# Overview

This is an automated framework that implemented for Web UI, Mobile App and API test automation with Behaviour-Driven
Development (BDD).

# Structure

![Image](structure.png)

# Features

* Supports Web UI, Mobile App and API
* Depends on WebDriverAgent when testing Mobile App (https://w3c.github.io/webdriver/webdriver-spec.html)
* Test data supports DataTable and csv files
* Re-executes failed scenarios
* Parameterization is supported in test data
* Generates allure report
* Takes the screenshot when failed by hooks
* Video recording is supported for mobile testing

# Pre-condition

## Install sqljdbc42.jar to Maven repository

1. Open a command window, go to directory: /src/main/resources/lib
2. Run command: mvn install:install-file -Dfile=sqljdbc42.jar -Dpackaging=jar -DgroupId=com.microsoft.sqlserver
   -DartifactId=sqljdbc4 -Dversion=4.2

## Install jave-1.0.2.jar to Maven repository

1. Open a command window, go to directory: /src/main/resources/lib
2. Run command: mvn install:install-file -Dfile=jave-1.0.2.jar -Dpackaging=jar -DgroupId=it.sauronsoftware
   -DartifactId=jave -Dversion=1.0.2

# Getting Started

Normally, the feature files and steps should be defined ahead before execution, the page objects should be defined as
well if UI relative.  
All the global variables can be defined in properties file.

## API

For API test automation, what we need to do are:

1. Defining csv files in /resources/api/testData folder, like below:

```csv
Module    ,CaseNo,CaseName                 ,URL                                               ,Method,Params,RequestBodyFileName                     ,HttpCode,ResponseBodyFileName,Validations                    ,byParameter,Variables,isFile,template
MobileFarm,1     ,Get device info          ,/automation/api/device/${desiredCapabilities.udid},Get   ,      ,                                        ,200     ,                    ,{'status': 'equals(available)'},           ,         ,      ,
MobileFarm,2     ,Start use device         ,/automation/api/device/${desiredCapabilities.udid},Post  ,      ,                                        ,200     ,                    ,                               ,           ,         ,      ,
MobileFarm,3     ,upload file to mobilefarm,/automation/api/v1/spaces/artifacts/0             ,Post  ,      ,src/test/resources/app/android/VodQA.apk,200     ,                    ,                               ,           ,id       ,1     ,
```

2. Define api properties, testng xml and runner files

There are several matchers that can help you to assert the result:

|Matcher              |Sample                         |Comments                                          |
|---------------------|-------------------------------|--------------------------------------------------|
|equals               |{‘Status’:’success’}           |{“Status”: “success”}
|startsWith           |{‘name’:’startsWith(test)’}    |{“name”: “test name”}
|contains             |{‘Message’:’contains(pass)’}   |{”Message”: “test passed”}
|exists               |{‘Data.PageSize’:’exists()’}
|isNotEmpty           |{‘Data.PageSize’:’isNotEmpty()’}
|isNull               |{‘Data.name’:’isNull()’}|{“Data”: {“name”: null}}
|isNotNull            |{‘Data.name’:’isNotNull()’}
|isAnArray            |{‘Data.Products’:’ isAnArray()’}|{“Data”: {“Products”: []}}
|hasItemInArray       |{‘Data.Products’:’ hasItemInArray()’}
|arraySizeEquals      |{‘Data.Products’:’arraySizeEquals(10)’}
|eachItemInArrayEquals|{‘Data.Products.Category’:’eachItemInArrayEquals(Kids)’}|{“Data”: {“Products”: [{“Category”: “Kids”}]}}
|eachItemInArrayContains|{‘Data.Products.Name’:’eachItemInArrayContains(Kids)’}

*if there are some dynamic values in parameters or json body, just define variables in properties file and use
`${variable_name}` to invoke it.*

*if the request header does not contain Authorization,enter the Headers line in the CSV file with {Authorization=without}*

## Mobile and Web

For Mobile and Web test automation, what we need to do are:

1. Define the feature file, steps and pages classes
2. Define mobile properties, testng xml and runner files

# Getting Execution

## Pre-condition

### Mobile

# If device is iOS 15, then need to use appium 1.22

# If device is iOS 14.x or lower version, need to use appium 1.21

# IOS screen recording video is a special format, need to use VLC media player to play.

# The property appiumProvider will determine how to start Appium Server

# nodejs: The framework will invoke nodejs start appium server automatically. In this case

# You need to set correct node path to DRIVER_PATH and appium path to APPIUM_PATH on

# src/main/java/com/welab/automation/framework/utils/entity/app/DeviceAppEntity.java

# desktop: start the appium server manually. and the framework only connect to this appium.

* Start the Appium server and related device is connected

### Web

* Start the selenium standalone in /src/main/resources/driver with command, like

```shell
java -Dwebdriver.chrome.driver=/path/to/chromedriver -jar selenium-server-standalone-3.141.59.jar
```  

## Run tests

To get the project set up just run maven command:

```shell

# -Dplatform is required, it could be [chrome, firefox, safari, mobile, api]
# -Dmobile is optional, it could be [android, ios, cloudandroid, cloudios]
mvn test -Denv=%env% -Dplatform=%platform% -Dmobile=%mobile% -DtestngXmlFile=%testngXmlFile% 
```

Here you need assign your testing env (like qa/dev/prod), platform (like chrome/firefox/android/ios) and TestNG xml
file.

Examples:

```shell
# run on mobilefarm environment
mvn test -Denv=mobilefarm -Dplatform=api -DtestngXmlFile="src/main/resources/suite/demo/mobileFarm-api.xml"
# run on android platform, local environment
mvn test -Denv=local -Dplatform=mobile -Dmobile=android -DtestngXmlFile="src/main/resources/suite/demo/mobile.xml"
# run the same scenario on android and chrome browser, sit environment
mvn test -Denv=sit -Dplatform=chrome -Dmobile=android -DtestngXmlFile="src/main/resources/suite/demo/mobile.xml"
# run on chrome browser, qa environment
mvn test -Denv=qa -Dplatform=chrome -DtestngXmlFile="src/main/resources/suite/demo/testng-web.xml"

#run API test without cucumber framework
mvn clean compile test -PWelab-Wealth-Sit -DtestngXmlFile="testng-welab.xml"
```

It will:

* Read assigned env properties under resources/platform_env.properties
* Start a relative driver and service if needed
* Generate the allure report automatically in TestReport folder

## Retry tests if failed

1. Configure the parameter "retryTimes" in properties file
2. Add the retry listener to the testng xml file, like

```shell
<listeners>
  <listener class-name="com.welab.automation.framework.listener.RetryListener"/>
</listeners>
```

## Video recording

Configure the parameter "recordVideo" in properties file

```shell
recordVideo=true to record video
recordVideo=false will not record video
```

## first run android tests on Windows

if using nodejs installed appium, change the appium path on
src/main/java/com/welab/automation/framework/utils/entity/app/DeviceAppEntity.java:
public static final String DRIVER_PATH = "{node_path}/node.exe"; public static final String APPIUM_PATH = "
{appium_path}/build/lib/main.js"; if using appium desktop, change the
src/main/java/com/welab/automation/framework/driver/AndroidDriverFactory.java:
public RemoteWebDriver getDriver() { try{ URL url = new URL("http://0.0.0.0:4273/wb/hub"); RemoteWebDriver driver = new
AndroidDriver(url, CapabilitiesBuilder.getAndroidLocalCapabilities()); return driver; } catch (Exception e){
System.out.println(e.getMessage()); return null; } }

check the properties on src/main/resources/mobile_local.properties, especially for android.platformVersion

## Setup python environment for find element by picture
1. install python3,and set environment for python3
2. pip3 install opencv-python
3. pip3 install opencv-contrib-python

## Setup record video for MAC
brew install ffmpeg