package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.*;
import com.welab.automation.framework.utils.PropertiesReader;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

public class DriverTest {
  DriverFactory driverFactory;
  RemoteWebDriver chromeDriver;
  RemoteWebDriver firefoxDriver;
  RemoteWebDriver safariDriver;
  BaseDriver baseDriver = new BaseDriver();

  @BeforeClass
  public void beforeClass() {
    GlobalVar.GLOBAL_VARIABLES.put("hub", "localhost:4444");
    System.setProperty("env", "qa");
  }

  @BeforeGroups("chrome")
  public void beforeGetChromeDriver() {
    System.setProperty("platform", "chrome");
    driverFactory = new ChromeDriverFactory();
  }

  @Test(groups = "chrome")
  public void shouldGetChromeWebDriver() {
    chromeDriver = driverFactory.getDriver();
    Assert.assertNotNull(chromeDriver);
  }

  @AfterGroups("chrome")
  public void afterGetChromeDriver() {
    chromeDriver.quit();
  }

  @BeforeGroups("firefox")
  public void beforeGetFirefoxDriver() {
    System.setProperty("platform", "firefox");
    driverFactory = new FirefoxDriverFactory();
  }

  @Test(groups = "firefox")
  public void shouldGetFirefoxWebDriver() {
    firefoxDriver = driverFactory.getDriver();
    Assert.assertNotNull(firefoxDriver);
  }

  @AfterGroups("firefox")
  public void afterGetFirefoxDriver() {
    firefoxDriver.quit();
  }

  @BeforeGroups("safari")
  public void beforeGetSafariDriver() {
    System.setProperty("platform", "safari");
    driverFactory = new SafariDriverFactory();
  }

  @Test(groups = "safari")
  public void shouldGetSafariWebDriver() {
    safariDriver = driverFactory.getDriver();
    Assert.assertNotNull(safariDriver);
  }

  @AfterGroups("safari")
  public void afterGetSafariDriver() {
    safariDriver.quit();
  }

  @Test
  public void shouldInitChromeWebDriver() throws IOException {
    System.setProperty("platform", "chrome");
    baseDriver.initDriver();
    RemoteWebDriver webDriver = BaseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    BaseDriver.closeDriver();
  }

  @Test
  public void shouldInitFirefoxWebDriver() throws IOException {
    System.setProperty("platform", "firefox");
    baseDriver.initDriver();
    RemoteWebDriver webDriver = BaseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    BaseDriver.closeDriver();
  }

  @Test
  public void shouldInitSafariWebDriver() throws IOException {
    System.setProperty("platform", "safari");
    baseDriver.initDriver();
    RemoteWebDriver webDriver = BaseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    BaseDriver.closeDriver();
  }

  @Test
  public void shouldInitAndroidWebDriver() throws IOException {
    System.setProperty("platform", "android");
    System.setProperty("env", "local");
    Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
    GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
    baseDriver.initDriver();
    RemoteWebDriver webDriver = baseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    baseDriver.closeDriver();
  }

  @Test
  public void shouldInitIOSWebDriver() throws IOException {
    System.setProperty("platform", "ios");
    System.setProperty("env", "local");
    Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
    GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
    baseDriver.initDriver();
    RemoteWebDriver webDriver = baseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    baseDriver.closeDriver();
  }
}
