package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BasePageTest {
  RemoteWebDriver driver;
  WebBasePage webBasePage;
  String pageName = "Demo page";

  @BeforeClass
  public void initEnv() {
    GlobalVar.GLOBAL_VARIABLES.put("hub", "localhost:4444");
    System.setProperty("env", "qa");
    System.setProperty("platform", "chrome");
  }

  @BeforeMethod
  public void initDriver() throws IOException {
    BaseDriver baseDriver = new BaseDriver();
    baseDriver.initDriver();
    driver = BaseDriver.getWebDriver();
    webBasePage = new WebBasePage();
  }

  @AfterMethod
  public void quitDriver() {
    BaseDriver.closeDriver();
  }

  @Test
  public void testGetElementAttribute() {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.id("kw"));
    String nameStr = webBasePage.getElementAttribute(ele, "name");
    Assert.assertEquals(nameStr, "wd");
  }

  @Test
  public void testGetText() {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.className("hot-refresh-text"));
    String nameStr = webBasePage.getElementText(ele);
    Assert.assertEquals(nameStr, "换一换");
  }

  @Test
  public void testSetText() throws InterruptedException {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.id("kw"));
    webBasePage.clearAndSendKeys(ele, "selenium");
    Thread.sleep(2000);
    WebElement listEle = driver.findElements(By.className("bdsug-overflow")).get(0);
    String actualTxt = webBasePage.getElementText(listEle);
    Assert.assertEquals(actualTxt, "selenium是什么意思");
  }

  @Test
  public void testVerifyContainsText() {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.className("hot-refresh-text"));
    boolean isContainsText = webBasePage.verifyContainsText(ele, "换一换", false);
    Assert.assertTrue(isContainsText);
  }

  @Test
  public void testVerifyNotContainsText() {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.className("hot-refresh-text"));
    boolean isContainsText = webBasePage.verifyNotContainsText(ele, "test", false);
    Assert.assertTrue(isContainsText);
  }

  @Test
  public void testVerifyElementNotExist() {
    webBasePage.navigateTo("http://www.baidu.com");
    By by = By.className("not-exist");
    boolean isNotExisted = webBasePage.verifyElementNotExist(by);
    Assert.assertTrue(isNotExisted);
  }

  @Test
  public void testVerifyElementExist() {
    webBasePage.navigateTo("http://www.baidu.com");
    By by = By.className("hot-refresh-text");
    boolean isExisted = webBasePage.verifyElementExist(by);
    Assert.assertTrue(isExisted);
  }

  @Test
  public void testSelectByIndex() {
    webBasePage.navigateTo("http://sahitest.com/demo/selectTest.htm");
    WebElement select = driver.findElement(By.id("s1Id"));
    webBasePage.selectByIndex(select, 1);
    String text = webBasePage.getSelectedOption(select);
    Assert.assertEquals(text, "o1");
  }

  @Test
  public void testSelectByVisibleText() {
    webBasePage.navigateTo("http://sahitest.com/demo/selectTest.htm");
    WebElement select = driver.findElement(By.id("s2Id"));
    webBasePage.selectByVisibleText(select, "o2");
    String text = webBasePage.getSelectedOption(select);
    Assert.assertEquals(text, "o2");
  }

  @Test
  public void testSelectByValue() {
    webBasePage.navigateTo("http://sahitest.com/demo/selectTest.htm");
    WebElement select = driver.findElement(By.id("s3Id"));
    webBasePage.selectByValue(select, "o4val");
    String text = webBasePage.getSelectedOption(select);
    Assert.assertEquals(text, "With spaces");
  }

  @Test
  public void testSelectMultipleByVisibleText() {
    webBasePage.navigateTo("http://sahitest.com/demo/selectTest.htm");
    WebElement select = driver.findElement(By.id("s4Id"));
    String options = "o1,o3";
    webBasePage.selectMultipleByVisibleText(select, options);
    List<String> expectedOption = new ArrayList<>();
    expectedOption.add("o1");
    expectedOption.add("o3");
    List<String> selectedOptions = webBasePage.getSelectedOptions(select);
    Assert.assertEquals(selectedOptions, expectedOption);
  }

  @Test
  public void testClickLink() {
    webBasePage.navigateTo("https://sahitest.com/demo/linkTest.htm");
    webBasePage.clickLink("linkByContent");
    String url = driver.getCurrentUrl();
    Assert.assertEquals(url, "https://sahitest.com/demo/linkByContent.htm");
  }

  @Test
  public void testHighlightElement() {
    webBasePage.navigateTo("http://www.baidu.com");
    WebElement ele = driver.findElement(By.id("kw"));
    webBasePage.highLightElement(ele);
    String style = webBasePage.getElementAttribute(ele, "style");
    Assert.assertEquals(style, "background: yellow; border: 2px solid red;");
  }
}
