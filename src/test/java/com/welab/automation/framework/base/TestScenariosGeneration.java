
package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.api.FeatureCreator;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

public class TestScenariosGeneration {
  FeatureCreator featureCreator;

  @Test
  private void generateFeatureAllCases() throws IOException {
    featureCreator = new FeatureCreator("");
    Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
    GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
    featureCreator.generatFeatureAllCases(GlobalVar.TEST_DATA_FILE_PATH, "templateDefault.feature");
  }
}
