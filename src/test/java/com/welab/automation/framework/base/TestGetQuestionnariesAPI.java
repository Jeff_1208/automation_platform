package com.welab.automation.framework.base;

import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.framework.common.ProxyUtils;
import com.welab.automation.framework.utils.entity.api.JsonDataProvider;
import com.welab.automation.framework.utils.entity.api.JsonEntity;
import com.welab.automation.framework.utils.entity.api.TestCaseUtils;
import com.welab.automation.projects.demo.WelabAPITest.IWealthQuestionnairesAPI;
import io.qameta.allure.Allure;
import io.qameta.allure.Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Feature("WelabQuestionnaires")
public class TestGetQuestionnariesAPI extends BaseRunner {
    private Logger logger = LoggerFactory.getLogger(TestGetQuestionnariesAPI.class);
    private IWealthQuestionnairesAPI iWealthQuestionnairesAPI = ProxyUtils.create(IWealthQuestionnairesAPI.class);
    @BeforeClass
    public void BeforeClass()
    {
        super.BeforeClass();
        JsonDataProvider.DATAFILE= "APIConfig/WealthQuestionnaires/TestGetQuestionnairesAPI.json";
    }

    @Test(dataProvider="json_data",dataProviderClass= JsonDataProvider.class)
    public void tc01_getWealthQuestionnaires(String rowID, String description, JsonEntity jsonEntity)
    {
        Allure.description(rowID.concat("-").concat(description));
        logger.info("rowID: {},description: {}",rowID,description);
        jsonEntity.addCaseLink();
        response = iWealthQuestionnairesAPI.getQuestionnariesCRPQ();
        TestCaseUtils.validate(response,jsonEntity);
    }
}
