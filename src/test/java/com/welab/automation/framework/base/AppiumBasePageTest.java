package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class AppiumBasePageTest {
  RemoteWebDriver driver;
  AppiumBasePage appiumBasePage;
  BaseDriver baseDriver;
  String pageName = "Demo page";

  @BeforeClass
  public void initEnv() {
    GlobalVar.GLOBAL_VARIABLES.put("hub", "localhost:4444");
    System.setProperty("env", "debug");
    System.setProperty("platform", "cloudandroid");
  }

  @BeforeMethod
  public void initDriver() throws IOException {
    baseDriver = new BaseDriver();
    baseDriver.initDriver();
    driver = BaseDriver.getWebDriver();
    appiumBasePage = new AppiumBasePage();
  }

  @AfterMethod
  public void quitDriver() {
    BaseDriver.closeDriver();
  }

  @Test
  public void testNotification() {
    appiumBasePage.scrollUp();
    appiumBasePage.openNotification();
    //    appiumBasePage.findElement_n(appiumBasePage.)
    BasePage eleObj = new BasePage();
    eleObj.verifyElementExist(By.xpath("//android.widget.Button[@text='Manage notifications']"));
    eleObj.verifyElementExist(By.id("com.android.systemui:id/notification_manager_btn"));
    //    appiumBasePage.findElement_n(By.id("com.android.systemui:id/notification_manager_btn"));
    //    appiumBasePage.takeScreenShot("notification bar");
  }
}
