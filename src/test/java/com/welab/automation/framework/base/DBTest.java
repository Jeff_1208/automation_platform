package com.welab.automation.framework.base;

import com.welab.automation.framework.db.DBSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import static com.welab.automation.framework.db.DBUtil.*;

public class DBTest {
  private static final Logger logger = LoggerFactory.getLogger(DBTest.class);
  DBSingleton dbSingleton;
  Connection connection;


  @BeforeClass
  public void initEnv() {
    System.setProperty("env", "qa");
    System.setProperty("platform", "chrome");
  }

  @BeforeMethod
  public void beforeMethod() {
    dbSingleton = DBSingleton.getInstance();
    connection = dbSingleton.getConnection();
  }

  @Test
  public void testQueryData() {
    String sql = "select send_to, code from otp order by update_time desc limit 1";
    List<Map<String, String>> results = queryData(connection, sql);
    for (Map<String, String> result : results) {
      logger.info(String.valueOf(result));
    }
    Assert.assertNotNull(results);
  }

  @Test
  public void testInsertData() {
    String sql = "INSERT INTO student(id, name, subject) VALUES (1005, 'Test5', 'Maths');";
    boolean inserted = insertData(connection, sql);
    Assert.assertTrue(inserted);
  }

  @Test
  public void testUpdateData() {
    String sql = "UPDATE student SET subject='Chinese' where id=1004;";
    boolean updated = updateData(connection, sql);
    Assert.assertTrue(updated);
  }

  @Test
  public void testDeleteData() {
    String sql = "DELETE FROM student where id=1005;";
    boolean deleted = deleteData(connection, sql);
    Assert.assertTrue(deleted);
  }

  @Test
  public void testGetColumnValue() {
    String sql = "SELECT * FROM student WHERE id=1004;";
    List<Map<String, String>> results = queryData(connection, sql);
    String columnValue = getColumnValue(results, "subject");
    Assert.assertEquals(columnValue, "Chinese");
  }

  @Test
  public void testVerifyColumnValue() {
    String sql = "SELECT * FROM student WHERE id=1004;";
    List<Map<String, String>> results = queryData(connection, sql);
    boolean isCorrect = verifyColumnValue(results, "subject", "Chinese");
    Assert.assertTrue(isCorrect);
  }

  @Test
  public void testGetColumnValues() {
    String sql = "select send_to, code from otp order by update_time desc limit 1";
    List<Map<String, String>> results = queryData(connection, sql);
    String[] columnValues = getColumnValues(results, "code");
    logger.info(columnValues[0]);
    //String[] expectedValues = {"Computer", "Science", "History", "Chinese"};
    //Assert.assertEquals(columnValues, expectedValues);
  }

  @Test
  public void testVerifyColumnValues() {
    String sql = "SELECT * FROM student;";
    List<Map<String, String>> results = queryData(connection, sql);
    String[] expectedValues = {"Science", "Computer", "History", "Chinese"};
    boolean isCorrect = verifyColumnValues(results, "subject", expectedValues);
    Assert.assertTrue(isCorrect);
  }

  @Test
  public void testGetSingleRow() {
    String sql = "SELECT * FROM student WHERE id=1004;";
    List<Map<String, String>> results = queryData(connection, sql);
    Map<String, String> singleRow = getSingleRow(results);
    Assert.assertNotNull(singleRow);
  }

  @AfterMethod
  public void afterMethod() {
    dbSingleton.disconnectFromDB();
  }
}
