package com.welab.automation.framework.base;

import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.driver.ChromeDriverFactory;
import com.welab.automation.framework.driver.DriverFactory;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import java.io.IOException;

public class CloudDriverTest {
  DriverFactory driverFactory;
  BaseDriver baseDriver = new BaseDriver();

  @BeforeClass
  public void beforeClass() {
    //    System.setProperty("hostPort", "localhost:4444");
    System.setProperty("env", "qa");
  }

  @BeforeGroups()
  public void beforeGetDriver() {
    System.setProperty("platform", "cloudandroid");
    driverFactory = new ChromeDriverFactory();
  }

  @Test
  public void shouldInitWebDriver() throws IOException {
    System.setProperty("platform", "cloudandroid");
    baseDriver.initDriver();
    RemoteWebDriver webDriver = baseDriver.getWebDriver();
    Assert.assertNotNull(webDriver);
    baseDriver.closeDriver();
  }
}
