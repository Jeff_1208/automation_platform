package com.welab.automation.framework.base;

import com.welab.automation.framework.common.ProxyUtils;
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.framework.utils.entity.api.JsonDataProvider;
import com.welab.automation.framework.utils.entity.api.JsonEntity;
import com.welab.automation.framework.utils.entity.api.TestCaseUtils;
import com.welab.automation.projects.demo.WelabAPITest.IWealthAccountAPI;
import io.cucumber.java.en.Given;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.qameta.allure.Allure;
import io.qameta.allure.Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Feature("WelabAccount")
public class TestGetAccountStatusAPI extends BaseRunner {
    private final Logger logger=LoggerFactory.getLogger(TestGetAccountStatusAPI.class);
    private IWealthAccountAPI iWealthAccountAPI = ProxyUtils.create(IWealthAccountAPI.class);

    @BeforeClass
    public void BeforeClass()
    {
        super.BeforeClass();
        JsonDataProvider.DATAFILE="APIConfig/WealthAccount/TestGetAccountStatusAPI.json";
    }

    @Test(dataProvider="json_data",dataProviderClass= JsonDataProvider.class)
    public void tc01_getWealthAccoutStatus(String rowID, String description, JsonEntity jsonEntity)
    {
        Allure.description(rowID.concat("-").concat(description));
        logger.info("rowID: {},description: {}",rowID,description);
        jsonEntity.addCaseLink();
        response = iWealthAccountAPI.getAccountStatus();
        TestCaseUtils.validate(response,jsonEntity);
    }
}
