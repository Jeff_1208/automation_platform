package com.welab.automation.framework.base;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;

public class TestSOAPAPI {
  @Test
  public void postMethod() throws Exception {
    // put the request body in test.xml file.
    FileInputStream fileInputStream = new FileInputStream(new File("test.xml"));
    RestAssured.baseURI = "https://gpsehi.sta-wlab.com";

    Response response =
        RestAssured.given()
            .header("Content-Type", "text/xml")
            .and()
            .body(IOUtils.toString(fileInputStream, "UTF-8"))
            .when()
            .post("/v1/ehi/service/getTransaction.asmx")
            .then()
            .statusCode(200)
            .and()
            .log()
            .all()
            .extract()
            .response();

    XmlPath jsXpath = new XmlPath(response.asString()); // Converting string into xml path to assert
    //    String rate = jsXpath.getString("");
    System.out.println(response.asString());
  }
}
