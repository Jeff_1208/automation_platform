package com.welab.automation.framework.base.example.entity;

public class User {
  private int userID;
  private String userName;

  public User(int userID, String userName) {
    this.userID = userID;
    this.userName = userName;
  }

  public void setUserID(int userID) {
    this.userID = userID;
  }

  public int getUserID() {
    return userID;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return this.userName;
  }
}
