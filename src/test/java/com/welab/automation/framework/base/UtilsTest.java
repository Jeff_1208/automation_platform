package com.welab.automation.framework.base;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.base.example.entity.User;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.api.CSVFileReader;
import com.welab.automation.framework.utils.entity.api.TestCaseEntity;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.welab.automation.framework.utils.FileUtil.*;
import static com.welab.automation.framework.utils.JsonUtil.*;
import static com.welab.automation.framework.utils.TimeUtils.checkNewerDateTime;
import static com.welab.automation.framework.utils.TimeUtils.dateTranslate;

public class UtilsTest {
  @Test
  public void testReadJsonFile() {
    String filepath = "src/test/resources/file/objectExample.json";
    String josnStr = readJsonFile(filepath);
    String expectedJson =
        "{\r\n"
            + "  \"name\": \"test1\",\r\n"
            + "  \"hobby\": [\r\n"
            + "    {\r\n"
            + "      \"sport\": \"football\"\r\n"
            + "    },\r\n"
            + "    {\r\n"
            + "      \"other\": \"music\"\r\n"
            + "    }\r\n"
            + "  ]\r\n"
            + "}";
    Assert.assertEquals(josnStr, expectedJson);
  }

  @Test
  public void testReadJsonObjectFileToMap() {
    String filepath = "src/test/resources/file/objectExample.json";
    String objectStr = readJsonObjectFileToMap(filepath).toString();
    String expectedStr = "{name=test1, hobby=[{sport=football}, {other=music}]}";
    Assert.assertEquals(objectStr, expectedStr);
  }

  @Test
  public void testReadJsonArrayFileToList() {
    String filepath = "src/test/resources/file/arrayExample.json";
    String objectStr = readJsonArrayFileToList(filepath).toString();
    String expectedStr = "[{name=test1, age=25.0}, {name=test2, age=35.0}]";
    Assert.assertEquals(objectStr, expectedStr);
  }

  @Test
  public void testWriteObj() {
    String filepath = "src/test/resources/file/writeExample.txt";
    writeObj(filepath, "write to file example");
  }

  @Test
  public void testReadFileWithWrap() {
    String filepath = "src/test/resources/file/arrayExample.json";
    String fileStr = readFileWithWrap(filepath);
    String expectedStr =
        "[\r\n"
            + "  {\r\n"
            + "    \"name\": \"test1\",\r\n"
            + "    \"age\": 25\r\n"
            + "  },\r\n"
            + "  {\r\n"
            + "    \"name\": \"test2\",\r\n"
            + "    \"age\": 35\r\n"
            + "  }\r\n"
            + "]\r\n";
    Assert.assertEquals(fileStr, expectedStr);
  }

  @Test
  public void testReadFileWithoutWrap() {
    String filepath = "src/test/resources/file/arrayExample.json";
    String fileStr = readFileWithoutWrap(filepath);
    String expectedStr =
        "[  {    \"name\": \"test1\",    \"age\": 25  },  {    \"name\": \"test2\",    \"age\": 35  }]";
    Assert.assertEquals(fileStr, expectedStr);
  }

  @Test
  public void testCreateFile() {
    String filepath = "src/test/resources/file/createExample.xls";
    boolean isCreated = createFile(filepath);
    Assert.assertTrue(isCreated);
  }

  @Test
  public void testCopyFile() {
    String sourceFile = "src/test/resources/file/objectExample.json";
    String targetFile = "src/test/resources/file/objectExample_copy.json";
    boolean isCopied = copyFile(sourceFile, targetFile);
    Assert.assertTrue(isCopied);
  }

  @Test
  public void testDeleteDirectoryAndChildren() {
    String path = "target/allure-results";
    boolean isDeleted = deleteDirectoryAndChildren(path);
    Assert.assertTrue(isDeleted);
  }

  @Test
  public void testDeleteFile() {
    String filePath = "src/test/resources/file/objectExample_copy.json";
    boolean isDeleted = deleteFile(filePath);
    Assert.assertTrue(isDeleted);
  }

  @Test
  public void testDeleteFolderOrFile() {
    String path = "src/test/resources/file/objectExample_copy.json";
    boolean isDeleted = deleteFolderOrFile(path);
    Assert.assertTrue(isDeleted);
  }

  @Test
  public void testGetActualValueByResponseKey() {
    String filepath = "src/test/resources/file/objectExample.json";
    Map<String, Object> map = readJsonObjectFileToMap(filepath);
    JSONObject jsonObject = mapToJsonObj(map);
    String sport = getActualValueByResponseKey(jsonObject, "hobby[0].sport");
    Assert.assertEquals(sport, "football");
  }

  @Test
  public void testMapToJsonObject() {
    String filepath = "src/test/resources/file/objectExample.json";
    Map<String, Object> map = readJsonObjectFileToMap(filepath);
    JSONObject jsonObject = mapToJsonObj(map);
    String expectedJson =
        "{\"name\":\"test1\",\"hobby\":[{\"sport\":\"football\"},{\"other\":\"music\"}]}";
    Assert.assertEquals(jsonObject.toString(), expectedJson);
  }

  @Test
  public void testJsonObjectToMap() {
    String filepath = "src/test/resources/file/objectExample.json";
    String objectStr = readJsonFile(filepath);
    Map<String, Object> map = jsonObjectToMap(objectStr);
    String expectedStr = "{name=test1, hobby=[{sport=football}, {other=music}]}";
    Assert.assertEquals(map.toString(), expectedStr);
  }

  @Test
  public void testArrayListToJsonArray() {
    String filepath = "src/test/resources/file/arrayExample.json";
    List<ArrayList> arrayList = readJsonArrayFileToList(filepath);
    String expectedStr = "[{\"name\":\"test1\",\"age\":25.0},{\"name\":\"test2\",\"age\":35.0}]";
    JSONArray jsonArray = listToJsonArray(arrayList);
    Assert.assertEquals(jsonArray.toString(), expectedStr);
  }

  @Test
  public void testJsonArrayToList() {
    String filepath = "src/test/resources/file/arrayExample.json";
    String jsonStr = readJsonFile(filepath);
    List<ArrayList> list = jsonArrayToList(jsonStr);
    String expectedStr = "[{name=test1, age=25.0}, {name=test2, age=35.0}]";
    Assert.assertEquals(list.toString(), expectedStr);
  }

  @Test
  public void testObjectToJsonObject() {
    User user = new User(1001, "test1");
    JSONObject jsonObject = objectToJsonObject(user);
    String expectedObject = "{\"userName\":\"test1\",\"userID\":1001}";
    Assert.assertEquals(jsonObject.toString(), expectedObject);
  }

  @Test
  public void testJsonObjectToObject() {
    String jsonStr = "{\r\n\"userName\":\"test1\",\r\n\"userID\":1001\r\n}";
    User user = jsonObjectToObject(jsonStr, User.class);
    int userId = user.getUserID();
    String userName = user.getUserName();
    Assert.assertEquals(userId, 1001);
    Assert.assertEquals(userName, "test1");
  }

  @Test
  public void testCheckField() {
    String filepath = "src/test/resources/file/objectExample.json";
    Map<String, Object> map = readJsonObjectFileToMap(filepath);
    JSONObject jsonObject = mapToJsonObj(map);
    String expectedStr = "{\"name\":\"test1\", \"hobby[0].sport\":\"football\"}";
    boolean isCorrect = checkField(jsonObject, expectedStr);
    Assert.assertTrue(isCorrect);
  }

  @Test
  public void testIsJsonObject() {
    String content = "{\"test\":\"value\"}";
    boolean isJsonObj = isJsonObject(content);
    Assert.assertTrue(isJsonObj);
  }

  @Test
  public void testIsJsonArray() {
    String content = "[\"test1\",\"test2\"]";
    boolean isJsonArray = isJsonArray(content);
    Assert.assertTrue(isJsonArray);
  }

  @Test
  public void testGetJsonType() {
    String content = "[\"test1\",\"test2\"]";
    String type = getJsonType(content);
    Assert.assertEquals(type, "JSONArray");
  }

  @Test
  public void testGetProperty() {
    System.setProperty("env", "stg");
    System.setProperty("platform", "safari");
    String url = PropertiesReader.getInstance().getProperty("url");
    Assert.assertEquals(url, "http://www.google.com");
  }

  @Test
  public void testGetAllProperties() {
    System.setProperty("env", "stg");
    System.setProperty("platform", "safari");
    String url = PropertiesReader.getInstance().getAllProperties().toString();
    String expectedProperties =
        "{retryTimes=1, headless=true, hub=localhost:4444, url=http://www.google.com}";
    Assert.assertEquals(url, expectedProperties);
  }

  @Test
  public void testDateTranslate() {
    String dateStr = dateTranslate("September 2, 2021 15:24:35", "yyyy-MM-dd HH:mm:ss");
    Assert.assertEquals(dateStr, "2021-09-02 15:24:35");
  }

  @Test
  public void testYamlToJson() throws IOException {
    JSONObject data = yamlToJsonObj("src/test/resources/file/yamlExample.yml");
    String jsonStr =
        "{\"languages\":[\"Ruby\",\"Perl\"],\"websites\":{\"YAML\":\"yaml.org\",\"Ruby\":\"ruby-lang.org\"}}";
    Assert.assertEquals(data.toString(), jsonStr);
  }

  @Test
  public void testGetCsvFileList() {
    String path = "src/test/resources/file";
    ArrayList<String> csvFiles = getCsvFileList(path);
    ArrayList<String> expectedFiles = new ArrayList<>();
    expectedFiles.add("D:\\Welab\\welabbank.automation\\src\\test\\resources\\file\\git.csv");
    Assert.assertEquals(csvFiles, expectedFiles);
  }

  @Test
  public void testReadCsvFile() throws IOException {
    String path = "src/test/resources/file";
    CSVFileReader csvFileReader = new CSVFileReader();
    List<TestCaseEntity> caseList = csvFileReader.readCSV(path);
    Assert.assertNotNull(caseList);
  }

  @Test
  public void testGetCsvFileHeaders() {
    String path = "src/test/resources/file/git.csv";
    String[] headers = getCSVHeaders(path);
    String[] expectedHeader = {
      "Module",
      "CaseNo",
      "CaseName",
      "URL",
      "Method",
      "Params",
      "RequestBodyFileName",
      "HttpCode",
      "ResponseBodyFileName",
      "Validations",
      "Template",
      "Variables"
    };
    Assert.assertEquals(headers, expectedHeader);
  }

  @Test
  public void testGetAllCsvFileDataWithoutHeaders() {
    String path = "src/test/resources/file/git.csv";
    ArrayList<String[]> content = getAllCSVDataWithoutHeaders(path);
    Assert.assertEquals(content.get(0)[1], "1");
  }

  @Test
  public void testGetAllCsvFileDataWitHeaders() {
    String path = "src/test/resources/file/git.csv";
    ArrayList<String[]> content = getAllCSVDataWithHeaders(path);
    Assert.assertEquals(content.get(0)[1], "CaseNo");
  }

  @Test
  public void testGetCsvFileRowDataWithoutHeaders() {
    String path = "src/test/resources/file/git.csv";
    String[] rowData = getCSVRowDataWithoutHeaders(path, 2);
    String[] expectedRow = {
      "GitHub",
      "2",
      "get repository02",
      "/orgs/swlxm2002/repos",
      "GET",
      "",
      "",
      "200",
      "",
      "{'name':'hasItemInArray(demo1)'}",
      "",
      ""
    };
    Assert.assertEquals(rowData, expectedRow);
  }

  @Test
  public void testGetCsvFileRowDataWithHeaders() {
    String path = "src/test/resources/file/git.csv";
    String[] rowData = getCSVRowDataWithHeaders(path, 2);
    String[] expectedRow = {
      "GitHub",
      "1",
      "get repository01",
      "/orgs/swlxm2002/repos",
      "GET",
      "",
      "",
      "200",
      "",
      "{'name':'hasItemInArray(demo)'}",
      "",
      ""
    };
    Assert.assertEquals(rowData, expectedRow);
  }

  @Test
  public void testGetCsvFileCellDataWithoutHeaders() {
    String path = "src/test/resources/file/git.csv";
    String cellData = getCSVCellDataWithoutHeaders(path, 2, "CaseName");
    Assert.assertEquals(cellData, "get repository02");
  }

  @Test
  public void testGetCsvFileCellDataWithHeaders() {
    String path = "src/test/resources/file/git.csv";
    String cellData = getCSVCellDataWithHeaders(path, 2, 3);
    Assert.assertEquals(cellData, "get repository01");
  }

  @Test
  public void testWriteCsvFileData() {
    String path = "src/test/resources/file/writeTest.csv";
    ArrayList<String[]> rowData = new ArrayList<>();
    rowData.add(new String[] {"test1", "test2"});
    rowData.add(new String[] {"test3", "test4"});
    writeCSVData(path, rowData);
    String cellData = getCSVCellDataWithHeaders(path, 2, 2);
    Assert.assertEquals(cellData, "test4");
  }

  @Test
  public void testWriteCsvFileRowData() {
    String path = "src/test/resources/file/writeTest.csv";
    String[] rowData = {"row2 column1", "row2 column 2"};
    writeCSVRowData(path, 2, rowData);
    String[] actualRowData = getCSVRowDataWithHeaders(path, 2);
    Assert.assertEquals(actualRowData, rowData);
  }

  @Test
  public void testWriteCsvFileCellData() {
    String path = "src/test/resources/file/writeTest.csv";
    String cellData = "cell test";
    writeCSVCellData(path, 5, 5, cellData);
    String actualCellData = getCSVCellDataWithHeaders(path, 4, 5);
    Assert.assertEquals(actualCellData, cellData);
  }

  @Test
  public void testCheckNewerDateTime() {
    String dateTime1 = "2021-09-14 10:44:04";
    String dateTime2 = "2021-09-14 10:44:44";
    boolean isNewer = checkNewerDateTime(dateTime1, dateTime2);
    Assert.assertTrue(isNewer);
  }
}
