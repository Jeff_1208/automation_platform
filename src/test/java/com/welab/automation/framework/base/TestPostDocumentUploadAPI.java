package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.ProxyUtils;
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.framework.utils.entity.api.JsonDataProvider;
import com.welab.automation.framework.utils.entity.api.JsonEntity;
import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import com.welab.automation.framework.utils.entity.api.TestCaseUtils;
import com.welab.automation.projects.demo.WelabAPITest.IWealthDocumentAPI;
import io.qameta.allure.Allure;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestPostDocumentUploadAPI extends BaseRunner {
    private IWealthDocumentAPI iWealthDocumentAPI = ProxyUtils.create(IWealthDocumentAPI.class);

    @BeforeClass
    public void BeforeClass()
    {
        super.BeforeClass();
        JsonDataProvider.DATAFILE="APIConfig/WealthDocuments/TestPostDocumentUploadAPI.json";
    }

    @BeforeMethod
    public void BeforeMethod()
    {
        String docType="ADDRESS_PROOF";
        String fileName=GlobalVar.faker.regexify("[0-9a-zA-Z]{16}")+".pdf";
        response = iWealthDocumentAPI.getDocumentsPreUpload(docType,fileName);
        String encryptedKey=response.jsonPath().getString("data.publicKey");
        String invtAccDocId=response.jsonPath().getString("data.invtAccDocId");
        GlobalVar.GLOBAL_VARIABLES.put("encryptedKey",encryptedKey);
        GlobalVar.GLOBAL_VARIABLES.put("invtAccDocId",invtAccDocId);
    }

    @Test(dataProvider="json_data",dataProviderClass= JsonDataProvider.class)
    public void tc01_postWealthDocuments(String rowID, String description, JsonEntity jsonEntity)
    {
        Allure.description(rowID.concat("-").concat(description));
        logger.info("rowID: {},description: {}",rowID,description);
        jsonEntity.addCaseLink();
        String objBody=jsonEntity.getObjBody().toJSONString();
        response = iWealthDocumentAPI.postDocumentsUpload(objBody);
        TestCaseUtils.validate(response,jsonEntity);
    }
}
