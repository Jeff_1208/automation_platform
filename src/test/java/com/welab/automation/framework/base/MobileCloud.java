package com.welab.automation.framework.base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static java.time.Duration.ofMinutes;
import static java.time.Duration.ofSeconds;
import static java.util.Optional.ofNullable;
import static org.openqa.selenium.By.className;

public class MobileCloud {

  private static final String PROJECT_NAME = "EPM-TSTF";
  private static final String API_KEY =
      "GdG/Wa5lj5syOmzE68JydisZjp/xh5pbMGK5/BR5n+NZSoUubFIXw+v4v4qpBN5XlyflGHeZQ6DCvkdNK8FROERJByPRo4FlDGSTpOrVgLDCKUIYxW7SJOFykfZYaZLJ45VvXg0b3dyOPM8o0UsKxqGjNL0/QyfFGYJZ6rPDcNQOkffVEMRjKFyFQp8sh5sr3Q5GUydJRGJS9+Ga3u8m9Ugh/xca07qVT0HRhLuRuCwJt9nWCPACwMCxK+r2KdEiavo663z1TmtbnZXHypYzcKBd/iNdsCZbd9RluoiBn12Z4+xOm/f6FvzJz5eZDCCKhUXowDMiclthkeN8lA9GbqfsaCmwhJEaxDQYV8rz8Xaiy4OvePrxHNL0Jflu7YdIfh6WBz3mjuwIc6fv9ii43fwrYYcxXFWcDwl3bfP+tqJxyokiQWqp0eKUNH/wIJ+VY96giDNMnN4Z+VhzA03kOmrmceJsXl7RODdBNa+9orJfddzGZIj9brp00xEjOaFgyqmYNxcQGERZLmU4wb0IcRleJqv0ekBqXvreNTniBCz2jBQ6uPLTLoexTDYMdIEexuUcVBaMNvfgnxGgHg++E4Gq44h/sx4pP2pc6uXqfk25L1rbYFkkeGdEie7Um1G74lw4s/Q0LIuKIoBnrf+RzawuzbzQG8xvuSGaJkHA06p6hiT/fEh/8wd8lODpagWDrmSvAJKmvom2gmUcmXHzRK+oy1Izr6ISzsSsJ86nbNYZIZTRQHyeiquXsDYfTDYnENcMONt4HBvFd8uSvEc+mJIOhLkZoo7+hJJVaOHkHq/GT0gjSfOz655FNlBKFUzuxkOmCgY+2UrblRYwYanFCgs+dYX2WG29Bz6iNqevkXGm4M2CuEOTdzvf9tQcKgdGO8gaAHrKYB+m3Q9/awN+RX8";
  private static final String APPIUM_HUB = "mobilecloud.test.com";
  private static final String PLATFORM_NAME = "iOS";
  private static final String PLATFORM_VERSION = "14.3";
  private static final String BROWSER_NAME = "safari";
  private static final String DEVICE_NAME = "IPHONE 13,1";

  private final DesiredCapabilities capabilities;

  private AppiumDriver driver = null;

  public MobileCloud() {
    capabilities = new DesiredCapabilities();
    capabilities.setCapability("platformName", PLATFORM_NAME);
    capabilities.setCapability("platformVersion", PLATFORM_VERSION);
    capabilities.setCapability("browserName", BROWSER_NAME);
    capabilities.setCapability("deviceName", DEVICE_NAME);
  }

  @BeforeMethod
  public void before() throws MalformedURLException, UnsupportedEncodingException {
    driver =
        new IOSDriver(
            new URL(
                format(
                    "https://%s:%s@%s/wd/hub",
                    PROJECT_NAME, URLEncoder.encode(API_KEY, "UTF-8"), APPIUM_HUB)),
            capabilities);

    // For devices with low performance
    driver
        .manage()
        .timeouts()
        .pageLoadTimeout(5, TimeUnit.MINUTES)
        .implicitlyWait(90, TimeUnit.SECONDS);
  }

  @Test
  public void demoTest() {
    final String testUrl = "https://www.test.com/";

    Assert.assertTrue(driver.isBrowser(), format("Focus is not on '%s'", BROWSER_NAME));

    driver.get(testUrl);

    new FluentWait<>(driver)
        .withMessage("Page was not loaded")
        .pollingEvery(ofSeconds(1))
        .withTimeout(ofMinutes(1))
        .until(driver -> driver.findElements(className("header__logo")).size() > 0);

    Assert.assertEquals(testUrl, driver.getCurrentUrl(), "Current url is incorrect");
    Assert.assertEquals(
        "test | Enterprise Software Development, Design & Consulting",
        driver.getTitle(),
        "Page title is incorrect");
  }

  @AfterMethod
  public void after() {
    ofNullable(driver).ifPresent(RemoteWebDriver::quit);
  }
}
