package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class WebBasePageTest {
  RemoteWebDriver driver;
  WebBasePage webBasePage;
  BaseDriver baseDriver;
  String pageName = "Demo page";

  @BeforeClass
  public void initEnv() {
    GlobalVar.GLOBAL_VARIABLES.put("hub", "localhost:4444");
    System.setProperty("env", "qa");
    System.setProperty("platform", "chrome");
  }

  @BeforeMethod
  public void initDriver() throws IOException {
    BaseDriver baseDriver = new BaseDriver();
    baseDriver.initDriver();
    driver = BaseDriver.getWebDriver();
    webBasePage = new WebBasePage();
    webBasePage.navigateTo("https://www.baidu.com");
  }

  @AfterMethod
  public void quitDriver() {
    BaseDriver.closeDriver();
  }

  @Test
  public void testPageBack() throws InterruptedException {
    WebElement searchTxt = driver.findElement(By.id("kw"));
    webBasePage.clearAndSendKeys(searchTxt, "selenium");
    WebElement searchBtn = driver.findElement(By.id("su"));
    webBasePage.clickElement(searchBtn);
    Thread.sleep(2000);
    webBasePage.pageBack(pageName);
    WebElement ele = driver.findElement(By.className("hot-refresh-text"));
    String nameStr = webBasePage.getElementText(ele);
    Assert.assertEquals(nameStr, "换一换");
  }

  @Test
  public void testGetTitle() {
    String title = webBasePage.getTabTitle(pageName);
    Assert.assertEquals(title, "百度一下，你就知道");
  }

  @Test
  public void testSwitchToNewTab() throws InterruptedException {
    WebElement linkEle = driver.findElement(By.linkText("hao123"));
    webBasePage.clickElement(linkEle);
    Thread.sleep(2000);
    boolean flag = webBasePage.switchToTab("hao123");
    Assert.assertTrue(flag);
    String title = webBasePage.getTabTitle("hao123");
    Assert.assertEquals(title, "hao123_上网从这里开始");
  }

  @Test
  public void testSwitchToOldTab() throws InterruptedException {
    WebElement linkEle = driver.findElement(By.linkText("hao123"));
    webBasePage.clickElement(linkEle);
    Thread.sleep(2000);
    boolean flag = webBasePage.switchToTab("百度一下");
    Assert.assertTrue(flag);
    String title = webBasePage.getTabTitle(pageName);
    Assert.assertEquals(title, "百度一下，你就知道");
  }

  @Test
  public void testCloseNewTab() throws InterruptedException {
    WebElement linkEle = driver.findElement(By.linkText("hao123"));
    webBasePage.clickElement(linkEle);
    Thread.sleep(2000);
    boolean flag = webBasePage.closeTab("hao123");
    Assert.assertTrue(flag);
  }

  @Test
  public void testCloseOldTab() throws InterruptedException {
    WebElement linkEle = driver.findElement(By.linkText("hao123"));
    webBasePage.clickElement(linkEle);
    Thread.sleep(2000);
    boolean flag = webBasePage.closeTab("百度一下");
    Assert.assertTrue(flag);
  }

  @Test
  public void testSwitchToAlert() {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("alert('Test alert')");
    Alert alert = webBasePage.switchToAlert();
    Assert.assertNotNull(alert);
    alert.accept();
  }

  @Test
  public void testGetAlertText() {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("alert('Test alert')");
    Alert alert = webBasePage.switchToAlert();
    String text = webBasePage.getAlertText(alert);
    Assert.assertEquals(text, "Test alert");
    alert.accept();
  }

  @Test
  public void testVerifyCurrentUrlCorrect() {
    String url = "https://www.baidu.com";
    boolean flag = webBasePage.verifyCurrentUrlCorrect(url);
    Assert.assertTrue(flag);
  }
}
