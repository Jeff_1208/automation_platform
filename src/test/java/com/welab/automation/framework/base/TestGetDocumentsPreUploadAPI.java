package com.welab.automation.framework.base;

import com.welab.automation.framework.common.ProxyUtils;
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.framework.utils.entity.api.JsonDataProvider;
import com.welab.automation.framework.utils.entity.api.JsonEntity;
import com.welab.automation.framework.utils.entity.api.TestCaseUtils;
import com.welab.automation.projects.demo.WelabAPITest.IWealthDocumentAPI;
import io.qameta.allure.Allure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestGetDocumentsPreUploadAPI extends BaseRunner {
    private final Logger logger= LoggerFactory.getLogger(TestGetAccountStatusAPI.class);
    private IWealthDocumentAPI iWealthDocumentAPI = ProxyUtils.create(IWealthDocumentAPI.class);

    @BeforeClass
    public void BeforeClass()
    {
        super.BeforeClass();
        JsonDataProvider.DATAFILE="APIConfig/WealthDocuments/TestGetDocumentsPreUploadAPI.json";
    }

    @Test(dataProvider="json_data",dataProviderClass= JsonDataProvider.class)
    public void tc01_getWealthDocuments(String rowID, String description, JsonEntity jsonEntity)
    {
        Allure.description(rowID.concat("-").concat(description));
        logger.info("rowID: {},description: {}",rowID,description);
        jsonEntity.addCaseLink();
        String docType=jsonEntity.getJsonObject().getString("docType");
        String fileName=jsonEntity.getJsonObject().getString("fileName");
        response = iWealthDocumentAPI.getDocumentsPreUpload(docType,fileName);
        TestCaseUtils.validate(response,jsonEntity);
    }

}
