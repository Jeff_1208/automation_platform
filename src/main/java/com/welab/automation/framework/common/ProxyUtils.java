package com.welab.automation.framework.common;

import com.welab.automation.framework.common.annotation.*;
import com.welab.automation.framework.utils.entity.api.*;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.enums.HttpType;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ProxyUtils {
    private static Logger logger = LoggerFactory.getLogger(ProxyUtils.class);

    public static <T> T create(Class<T> clazz) {

        // get interface host address
        Annotation annotation = clazz.getAnnotation(Server.class);
        if (annotation == null) {
            throw new RuntimeException(String.format("The interface class %s is not configured with the @server annotation",
                    clazz.getName()));
        }

        String CurrentProxy =ConfigUtils.getCurrentProxy().trim();
        String host = CurrentProxy +clazz.getAnnotation(Server.class).value().toString();
        String suffixRoute = clazz.getAnnotation(Server.class).value().toString();
        HttpUtils httpUtils = new HttpUtils(host);

         return (T) Proxy.newProxyInstance(clazz.getClassLoader(),
                new Class[]{clazz},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Annotation[] annotations = method.getAnnotations();
                        if (annotations.length == 0) {
                            throw new RuntimeException(String.format("%s method didn't config request annotation，ex:@POST、@GET...",
                                    method.getName()));
                        }

                        HttpType httpType = null;
                        String path = "";
                        String paramsPath = "";
                        String description = "";
                        String Body = "";
                        TestStep testStep = new TestStep();

                        // Currently only one annotation needs to be parsed
                        for (Annotation annotation : annotations) {
                            if (annotation instanceof Post) {
                                httpType = HttpType.POST;
                                path = ((Post) annotation).path();
                                testStep.setIsUploadFile(Boolean.valueOf(((Post) annotation).isUpload()));
                                description = ((Post) annotation).description();
                            } else if (annotation instanceof Put) {
                                httpType = HttpType.PUT;
                                path = ((Put) annotation).path();
                                description = ((Put) annotation).description();
                            } else if (annotation instanceof Patch) {
                                httpType = HttpType.PATCH;
                                path = ((Patch) annotation).path();
                                description = ((Patch) annotation).description();
                            } else if (annotation instanceof Delete) {
                                httpType = HttpType.DELETE;
                                path = ((Delete) annotation).path();
                                description = ((Delete) annotation).description();
                            } else if (annotation instanceof Get) {
                                httpType = HttpType.GET;
                                path = ((Get) annotation).path();
                                description = ((Get) annotation).description();
                            } else if (annotation instanceof Headers) {
                                testStep.setHeaderByAnnotation((Headers) annotation);
                            } else {
                                throw new RuntimeException(String.format("not support this function %s config annotation parameter %s",
                                        method.getName(),
                                        annotations[0].annotationType()));
                            }
                        }
                        GlobalVar.GLOBAL_VARIABLES.put("memthod",httpType.getValue());

                        // Comments corresponding to parameters on methods
                        Annotation[][] parameters = method.getParameterAnnotations();
                        Integer length = parameters.length;
                        testStep.setBody("");
                        boolean removeId = false;
                        if (length != 0) {
                            Map<String, Object> paramMap = new HashMap<>();
                            Map<String, Object> formParamMap = new HashMap<>();
                            Map<String, Object> bodyMap = new HashMap<>();
                            MultipartParameter multipartParameter = null;
                            for (Integer i = 0; i < length; i++) {
                                // Parameter annotation type
                                Annotation[] annos = parameters[i];
                                if (annos.length == 0) {
                                    //How can an unannotated parameter be considered an unannotated parameter
                                    throw new RuntimeException(String.format("function %s Missing parameter annotation，ex:@Param",
                                            method.getName()));
                                }

                                if (annos[0] instanceof Param) {
                                    if (args[i] == null) {
                                        paramMap.put((((Param) annos[0]).value()), null);
                                    } else {
                                        String param = args[i].toString();
                                        if (!param.isEmpty()) {
                                            paramMap.put((((Param) annos[0]).value()), args[i]);
                                        }
                                    }

                                } else if (annos[0] instanceof PathVariable) {
                                    path = path.replaceFirst("\\{\\w*\\}", java.util.regex.Matcher.quoteReplacement(args[i].toString()));
                                } else if (annos[0] instanceof Body) {
                                    if(args[i]!=null)
                                      Body = args[i].toString();
                                    testStep.setBody(Body);
                                } else if (annos[0] instanceof ObjBody) {
                                    ObjBody anno = (ObjBody) annos[0];
                                    if (anno.isIgnoreNull() == false) {
                                        GsonUtil.initGsonContainNull();
                                    }

                                    Body = GsonUtil.toJson(args[i]);
                                    testStep.setBody(Body);
                                    if (anno.isIgnoreNull() == false) {
                                        GsonUtil.initGsonIgnoreNull();  // restore to convert instance to json ignore null
                                    }
                                } else if (annos[0] instanceof ObjParam) {
                                    Object arg = args[i];
                                    paramMap = GsonUtil.object2map(arg);

                                } else if (annos[0] instanceof Multipart) {
                                    if (args[i] != null) {
                                        Multipart anno = (Multipart) annos[0];
                                        multipartParameter = new MultipartParameter(anno);
                                        multipartParameter.setControlValue(args[i]);
                                        testStep.multipartList.add(multipartParameter);
                                    }
                                } else {
                                    throw new RuntimeException(String.format("not support this function %s config annotation parameter %s",
                                            method.getName(),
                                            annos[0].annotationType()));
                                }
                            }
                            testStep.setParams(paramMap);
                            paramsPath = testStep.getParamsPath();
                            logger.info("paramsPath is " + paramsPath);
                        } else {
                            Map<String, Object> map = new HashMap<>();
                            testStep.setParams(map);
                        }
                        testStep.setType(httpType);
                        testStep.setPath(path);

                        logger.info("path: " + path + ", description:" + description);
                        String route = suffixRoute.concat(path);
                        if(httpType.getValue()=="post" || httpType.getValue()=="put")
                        {
                            SignatureUtil signatureUtil =new SignatureUtil();
                            signatureUtil.generateHeaderSignature(testStep.getBody(),route,CurrentProxy,httpType.getValue());
                        }
                        testStep.updateGlobalHeader(); //update global header
                        Response response = httpUtils.request(testStep);
                        testStep.restoreGlobalHeader(); // restore global header
                        return response;
                    }
                });
    }


    @Step("Given the header using host,path, path parameter")
    private static void generateHeader(String host, String path, String paramsPath) {
        generateHeaderWithoutAuth();
    }


    @Step("Given the header using host,path, path parameter")
    public static void generateHeaderWithoutAuth() {
        Allure.addAttachment("request headers", GlobalVar.HEADERS.toString());
    }

    public static void setDefaultHeaderTn() {
//        GlobalVar.HEADERS.put("tn", ""); // this user id = 1
    }

}
