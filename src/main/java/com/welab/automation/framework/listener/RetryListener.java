

package com.welab.automation.framework.listener;

import com.welab.automation.framework.utils.entity.api.JsonEntity;
import com.welab.automation.framework.utils.entity.api.TestCaseUtils;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RetryListener extends TestListenerAdapter implements IAnnotationTransformer,IHookable {
  public void transform(
      ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
    IRetryAnalyzer iRetryAnalyzer = annotation.getRetryAnalyzer();
    if (iRetryAnalyzer == null) {
      annotation.setRetryAnalyzer(TestRetryAnalyzer.class);
    }
  }

  @Override
  public void onFinish(ITestContext testContext) {
    Iterator<ITestResult> listOfSkippedTests =
        testContext.getSkippedTests().getAllResults().iterator();
    while (listOfSkippedTests.hasNext()) {
      ITestResult skippedTest = listOfSkippedTests.next();
      ITestNGMethod method = skippedTest.getMethod();
      if (testContext.getFailedTests().getResults(method).size() > 0
          || testContext.getPassedTests().getResults(method).size() > 0) {
        skippedTest.setStatus(0);
        listOfSkippedTests.remove();
      }
    }
  }

  @Override
  public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
    JsonEntity jsonEntity = getJsonEntity(iTestResult);
    Map<String, List<String>> sqlExpression = null;
    //replace parameter
    if (jsonEntity != null) {
      jsonEntity.setJsonObject(TestCaseUtils.replaceParameter(jsonEntity.getJsonObject()));
      jsonEntity.updateHeader();
    }
    //execute test case
    iHookCallBack.runTestMethod(iTestResult);
  }

  public JsonEntity getJsonEntity(ITestResult testResult) {
    JsonEntity jsonEntity = null;
    Object[] parameters = testResult.getParameters();

    if (parameters.length > 0) {
      for (Object parameter : parameters) {
        if (parameter instanceof JsonEntity) {
          jsonEntity = (JsonEntity) parameter;
        }
      }
    }
    return jsonEntity;
  }

  @Override
  public void onTestFailure(ITestResult result) {
    TestRetryAnalyzer testRetryAnalyzer = (TestRetryAnalyzer) result.getMethod().getRetryAnalyzer();
    testRetryAnalyzer.reSetCount();
  }
}
