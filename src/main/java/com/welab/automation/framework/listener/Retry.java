package com.welab.automation.framework.listener;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.JsonUtil;
import org.slf4j.LoggerFactory;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.slf4j.Logger;
public class Retry implements IRetryAnalyzer {

    private static Logger logger =  LoggerFactory.getLogger(Retry.class);
    private int retryCount = 0;

    @Override
    public boolean retry(ITestResult result) {
        logger.info("====执行用例：" + result.getName() + ",第" + retryCount + "次失败！");
        logger.info("====执行用例：" + result.getMethod() + ",第" + retryCount + "次失败！");
        String retryTimes= GlobalVar.GLOBAL_VARIABLES.get("retryTimes");
        System.out.println("retryTimes: "+retryTimes);
        int maxRetryCount  = Integer.parseInt(retryTimes);
        if (retryCount < maxRetryCount) {
            retryCount++;
            return true;
        }
        retryCount = 1;
        return false;
    }
}
