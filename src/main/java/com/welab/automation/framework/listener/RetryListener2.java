package com.welab.automation.framework.listener;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class RetryListener2 extends TestListenerAdapter implements IAnnotationTransformer,IHookable  {
  @Override
  public void transform(ITestAnnotation testannotation, Class testClass, Constructor testConstructor, Method testMethod) {
    testannotation.setRetryAnalyzer(Retry.class);
  }

  @Override
  public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {

  }
}
