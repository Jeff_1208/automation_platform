
package com.welab.automation.framework.listener;

import com.welab.automation.framework.utils.PropertiesReader;
import org.testng.ITestResult;
import org.testng.util.RetryAnalyzerCount;

public class TestRetryAnalyzer extends RetryAnalyzerCount {
  private static int count =
      Integer.parseInt(PropertiesReader.getInstance().getProperty("retryTimes"));

  public TestRetryAnalyzer() {
    super.setCount(count);
  }

  @Override
  public boolean retryMethod(ITestResult result) {
    return true;
  }

  public void reSetCount() {
    super.setCount(count);
  }
}
