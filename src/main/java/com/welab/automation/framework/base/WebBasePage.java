package com.welab.automation.framework.base;

import atu.testrecorder.ATUTestRecorder;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.utils.TimeUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import static com.welab.automation.framework.GlobalVar.*;
import static com.welab.automation.framework.utils.FileUtil.*;
import static com.welab.automation.framework.utils.Utils.logFail;
import static com.welab.automation.framework.utils.Utils.logPass;

public class WebBasePage extends BasePage {
  protected static final Logger logger = LoggerFactory.getLogger(WebBasePage.class);

  protected RemoteWebDriver driver;

  public WebBasePage() {
    this.driver = BaseDriver.getWebDriver();
    super.driver = this.driver;
    PageFactory.initElements(new AjaxElementLocatorFactory(driver, WAIT_TIMEOUT_LOW_S), this);
  }
  /**
   * @Date 2022/1/30 10:02
   * @Author Joe Shen
   * @Description It will navigate to the url that you filled in
   * @param url
   * @return
   */
  public void navigateTo(String url) {
    try {
      String logDetails = String.format("Navigate to url: %s", url);
      driver.get(url);
      waitForDOMReady();
      logPass(logDetails);
    } catch (RuntimeException e) {
      String logDetails = String.format("Failed to navigate to url: %s", url);
      logFail(logDetails);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:05
   * @Author Joe Shen
   * @Description Wait for page load complete until the page title show up within the number of seconds you filled in
   * @param pageTitle
   * @param timeOut
   * @return
   */
  public void waitForPageLoadComplete(String pageTitle, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
    try {
      wait.until(ExpectedConditions.titleIs(pageTitle));
      logPass("Page is loaded completely", pageName);
    } catch (TimeoutException e) {
      String actualTitle = driver.getTitle();
      String logDetails =
          String.format(
              "Page is incorrect, expected is: %s, actual is: %s", pageTitle, actualTitle);
      logFail(logDetails, pageName);
      throw new RuntimeException(logDetails);
    }
  }
  /**
   * @Date 2022/1/30 10:06
   * @Author Joe Shen
   * @Description Switch to the tab that you filled in tab title
   * @param tabTitle
   * @return boolean
   */
  public boolean switchToTab(String tabTitle) {
    boolean flag = false;
    try {
      Set<String> handles = driver.getWindowHandles();
      for (String s : handles) {
        driver.switchTo().window(s);
        String currentTitle = driver.getTitle();
        if (currentTitle.contains(tabTitle)) {
          flag = true;
          String logDetails = String.format("Switch to tab: %s successfully!", tabTitle);
          logPass(logDetails);
          break;
        } else {
          continue;
        }
      }
      if (!flag) {
        String logDetails = String.format("Can not switch to tab: %s!", tabTitle);
        logFail(logDetails);
      }
    } catch (Exception e) {
      String logDetails = String.format("Can not switch to tab: %s!", tabTitle);
      logFail(logDetails);
      throw e;
    }
    return flag;
  }
  /**
   * @Date 2022/1/30 10:06
   * @Author Joe Shen
   * @Description Close the tab that you filled in tab title
   * @param tabTitle
   * @return boolean
   */
  public boolean closeTab(String tabTitle) {
    boolean flag = false;
    boolean currentTab = switchToTab(tabTitle);
    if (currentTab) {
      driver.close();
      flag = true;
      String logDetails = String.format("Close tab: %s successfully!", tabTitle);
      logPass(logDetails);
    } else {
      String logDetails = String.format("Can not find to tab: %s!", tabTitle);
      logFail(logDetails);
    }
    return flag;
  }
  /**
   * @Date 2022/1/30 10:06
   * @Author Joe Shen
   * @Description Get tab title using page name
   * @param pageName
   * @return title
   */
  public String getTabTitle(String pageName) {
    String title = null;
    try {
      title = driver.getTitle();
      String logDetails = String.format("The title is '%s'", title);
      logPass(logDetails, pageName);
    } catch (Exception e) {
      logFail("Failed to page refresh", pageName);
      throw e;
    }
    return title;
  }
  /**
   * @Date 2022/1/30 10:06
   * @Author Joe Shen
   * @Description Get current URL using page name
   * @param pageName
   * @return url
   */
  public String getCurrentUrl(String pageName) {
    String url = null;
    try {
      url = driver.getCurrentUrl();
      String logDetails = String.format("Current URL is '%s'", url);
      logPass(logDetails, pageName);
    } catch (Exception e) {
      logFail("Failed to get current URL", pageName);
      throw e;
    }
    return url;
  }
  /**
   * @Date 2022/1/30 10:07
   * @Author Joe Shen
   * @Description Verify current URL is correct
   * @param expectedUrl
   * @return boolean
   */
  public boolean verifyCurrentUrlCorrect(String expectedUrl) {
    boolean flag = false;
    StringBuffer expectedSf = new StringBuffer();
    expectedSf.append(expectedUrl);
    int expectedLen = expectedSf.length();
    if (expectedSf.charAt(expectedLen - 1) == '/') {
      expectedSf.deleteCharAt(expectedLen - 1);
    }
    try {
      String actualUrl = getCurrentUrl(pageName);
      StringBuffer actualSf = new StringBuffer();
      actualSf.append(actualUrl);
      int actualLen = actualSf.length();
      if (actualUrl.charAt(actualLen - 1) == '/') {
        actualSf.deleteCharAt(actualLen - 1);
      }
      if (actualSf.toString().equals(expectedSf.toString())) {
        flag = true;
        String logDetails = String.format("Current URL is as expected: %s", expectedUrl);
        logPass(logDetails, pageName);
      } else {
        String logDetails =
            String.format(
                "Current URL is incorrect, expected: %s, actual: %s", expectedUrl, actualUrl);
        logFail(logDetails, pageName);
      }
    } catch (Exception e) {
      String logDetails = String.format("Failed to verify expected current URL: %s", expectedUrl);
      logFail(logDetails, pageName);
      throw e;
    }
    return flag;
  }
  /**
   * @Date 2022/1/30 10:08
   * @Author Joe Shen
   * @Description Switch to alert box
   * @param
   * @return alert
   */
  public Alert switchToAlert() {
    Alert alert = null;
    try {
      alert = driver.switchTo().alert();
      String logDetails = "Switch to alert successfully";
      logPass(logDetails);
    } catch (Exception e) {
      String logDetails = "Can not switch to alert";
      logFail(logDetails);
      throw e;
    }
    return alert;
  }
  /**
   * @Date 2022/1/30 10:08
   * @Author Joe Shen
   * @Description Get the text in alert box
   * @param alert
   * @return text
   */
  public String getAlertText(Alert alert) {
    String text = null;
    try {
      text = alert.getText();
      String logDetails = String.format("Alert text is: %s", text);
      logPass(logDetails);
    } catch (Exception e) {
      String logDetails = "Can not get alert text";
      logFail(logDetails);
      throw e;
    }
    return text;
  }
  /**
   * @Date 2022/1/30 10:08
   * @Author Joe Shen
   * @Description Accept alert if possible
   * @param alert
   * @return boolean
   */
  public boolean acceptAlert(Alert alert) {
    boolean flag = false;
    try {
      alert.accept();
      flag = true;
      String logDetails = "The alert is accepted";
      logPass(logDetails);
    } catch (Exception e) {
      String logDetails = "Can not accept the alert";
      logFail(logDetails);
    }
    return flag;
  }
  /**
   * @Date 2022/1/30 10:09
   * @Author Joe Shen
   * @Description Dismiss alert if possible
   * @param alert
   * @return boolean
   */
  public boolean dismissAlert(Alert alert) {
    boolean flag = false;
    try {
      alert.dismiss();
      flag = true;
      String logDetails = "The alert is dismissed";
      logPass(logDetails);
    } catch (Exception e) {
      String logDetails = "Can not dismiss the alert";
      logFail(logDetails);
    }
    return flag;
  }
  /**
   * @Date 2022/1/30 10:11
   * @Author Joe Shen
   * @Description Scroll to bottom in the page that you filled in
   * @param pageName
   * @return
   */
  public void scrollToBottom(String pageName) {
    try {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      String logDetails = "Scroll to bottom successfully";
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg = "Failed to scroll to bottom";
      logFail(errorMsg, pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:11
   * @Author Joe Shen
   * @Description Scroll to top in the page that you filled in
   * @param pageName
   * @return
   */
  public void scrollToTop(String pageName) {
    try {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript("window.scrollTo(0, 0)");
      String logDetails = "Scroll to top successfully";
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg = "Failed to scroll to top";
      logFail(errorMsg, pageName);
      throw e;
    }
  }

  public void scrollByLocation(String pageName,int Y1, int Y2) {
    try {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript("window.scrollTo("+Y1+", "+Y2+")");
      String logDetails = "Scroll successfully";
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg = "Failed to scroll to "+Y2;
      logFail(errorMsg, pageName);
      throw e;
    }
  }

  public void scrollByJS(String pageName,String jsScript) {
    try {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript(jsScript);
      String logDetails = "Scroll successfully";
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg = "Failed to scroll by js";
      logFail(errorMsg, pageName);
      throw e;
    }
  }

  public void removeAttributeByJS(String jsScript,WebElement webElement,String filed) {
    try {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript(jsScript,webElement,filed);
      String logDetails = "remove " + filed + "Attribute successfully";
      logPass(logDetails);
    } catch (Exception e) {
      String errorMsg = "Failed to remove" + filed + "Attribute";
      logFail(errorMsg);
      throw e;
    }

  }

   /**
   * @Date 2022/1/30 10:11
   * @Author Joe Shen
   * @Description The page name you wrote will be refreshed
   * @param pageName
   * @return
   */
  public void pageRefresh(String pageName) {
    try {
      driver.navigate().refresh();
      waitForDOMReady();
      logPass("Page refresh", pageName);
    } catch (Exception e) {
      logFail("Failed to page refresh", pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:11
   * @Author Joe Shen
   * @Description Go forwards in the page name you wrote it
   * @param pageName
   * @return
   */
  public void pageForward(String pageName) {
    try {
      driver.navigate().forward();
      waitForDOMReady();
      logPass("Page forward", pageName);
    } catch (Exception e) {
      logFail("Failed to page forward", pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:12
   * @Author Joe Shen
   * @Description Go back in the page name you wrote it
   * @param pageName
   * @return
   */
  public void pageBack(String pageName) {
    try {
      driver.navigate().back();
      waitForDOMReady();
      logPass("Page back", pageName);
    } catch (Exception e) {
      logFail("Failed to page back", pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:12
   * @Author Joe Shen
   * @Description return document ready state
   * @param
   * @return boolean
   */
  public boolean isDOMReady() {
    return ((JavascriptExecutor) driver)
        .executeScript("return document.readyState")
        .equals("complete");
  }
  /**
   * @Date 2022/1/30 10:12
   * @Author Joe Shen
   * @Description Wait for document ready in WAIT_TIMEOUT_HIGH_S seconds
   * @param
   * @return
   */
  public void waitForDOMReady() {
    try {
      Wait<Object> wait =
          new FluentWait<Object>(driver)
              .withTimeout(Duration.ofSeconds(WAIT_TIMEOUT_HIGH_S))
              .pollingEvery(Duration.ofMillis(500))
              .ignoring(NoSuchElementException.class);
      wait.until(input -> isDOMReady());
    } catch (Exception e) {
      String logDetails = e.getMessage();
      logFail(logDetails);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:12
   * @Author Joe Shen
   * @Description
   * @param
   * @return logEntries
   */
  public LogEntries getBrowserLogs() {
    LogEntries logEntries = driver.manage().logs().get("browser");
    return logEntries;
  }
  /**
   * @Date 2022/1/30 10:12
   * @Author Joe Shen
   * @Description Click element by JS
   * @param element
   * @return
   */
  public void clickElementByJS(WebElement element) {
    try {
      waitUntilElementClickable(element);
      ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
      logPass("Click element by JS", pageName, element);
    } catch (Exception e) {
      logFail("Failed to Click element by JS", pageName, element);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:13
   * @Author Joe Shen
   * @Description Scroll to find the element that you filled in
   * @param element
   * @return
   */
  public void scrollToElement(WebElement element) {
    try {
      waitUntilElementVisible(element);
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript("arguments[0].scrollIntoView();", element);
      String logDetails = "Scroll to element successfully";
      logPass(logDetails, pageName, element);
    } catch (Exception e) {
      String errorMsg = "Failed to scroll to element";
      logFail(errorMsg, pageName, element);
    }
  }
  /**
   * @Date 2022/1/30 10:13
   * @Author Joe Shen
   * @Description Move the mouse to the specified element
   * @param element
   * @return
   */
  public void mouseOverOnElement(WebElement element) {
    try {
      waitUntilElementVisible(element);
      Actions action = new Actions(driver);
      action.moveToElement(element).build().perform();
      String logDetails = "Move to element successfully";
      logPass(logDetails, pageName, element);
    } catch (Exception e) {
      String errorMsg = "Failed to move to element";
      logFail(errorMsg, pageName, element);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:13
   * @Author Joe Shen
   * @Description Will highlight the element for easy viewing
   * @param element
   * @return
   */
  public void highLightElement(WebElement element) {
    try {
      waitUntilElementVisible(element);
      JavascriptExecutor js = (JavascriptExecutor) driver;
      js.executeScript(
          "arguments[0].setAttribute('style', arguments[1]);",
          element,
          "background: yellow; border: 2px solid red;");
      String logDetails = "Highlight the element";
      logPass(logDetails, pageName, element);
    } catch (Exception e) {
      String errorMsg = "Failed to highlight the element";
      logFail(errorMsg, pageName, element);
    }
  }
  /**
   * @Date 2022/1/30 10:13
   * @Author Joe Shen
   * @Description Drag and drop element from source element to target element
   * @param source
   * @param target
   * @return
   */
  public void dragAndDropElement(WebElement source, WebElement target) {
    try {
      Actions actions = new Actions(driver);
      actions.dragAndDrop(source, target).perform();
      String logDetails =
          String.format("Drag element %s to element %s successfully", source, target);
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg = String.format("Failed to drag element %s to element %s", source, target);
      logFail(errorMsg, pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:13
   * @Author Joe Shen
   * @Description Drag and drop element to x,y
   * @param element
   * @param xOffset
   * @param yOffset
   * @return
   */
  public void dragAndDropElement(WebElement element, int xOffset, int yOffset) {
    try {
      Actions actions = new Actions(driver);
      actions.dragAndDropBy(element, xOffset, yOffset).perform();
      String logDetails =
          String.format("Drag element %s to (%d, %d) successfully", element, xOffset, yOffset);
      logPass(logDetails, pageName);
    } catch (Exception e) {
      String errorMsg =
          String.format("Failed to drag element %s to (%d, %d)", element, xOffset, yOffset);
      logFail(errorMsg, pageName);
      throw e;
    }
  }
  /**
   * @Date 2022/1/30 10:14
   * @Author Joe Shen
   * @Description Recording is stored in the specified path
   * @param recorder
   * @param videoName
   * @return recorder
   */
  public static ATUTestRecorder startRecording(ATUTestRecorder recorder, String videoName) {
    createDirectory(TARGET_RECORDING_PATH);
    try {
      recorder = new ATUTestRecorder(TARGET_RECORDING_PATH, videoName, false);
      recorder.start();
      logger.info("* Start recording video...");
    } catch (Exception e) {
      logger.error("Failed to start video recording");
    }
    return recorder;
  }
  /**
   * @Date 2022/1/30 10:14
   * @Author Joe Shen
   * @Description Stop recording
   * @param recorder
   * @param videoName
   * @return
   */
  public static void stopRecording(ATUTestRecorder recorder, String videoName) {
    try {
      recorder.stop();
      logger.info("* End recording video...");
      String videoPath = Paths.get(TARGET_RECORDING_PATH + videoName).toAbsolutePath().toString();
      //      String videoPath = TARGET_RECORDING_PATH + videoName;
      String movVideo = videoPath + ".mov";
      String mp4Video = videoPath + ".mp4";
      //movToMp4(movVideo, mp4Video);
      movToMp4_2(movVideo, mp4Video);
      deleteFile(movVideo);
    } catch (Exception e) {
      logger.error("Failed to stop video recording");
    }
  }

  public void inputEnter(WebElement webElement) {
    webElement.sendKeys(Keys.ENTER);
    logger.info("input ENTER to element({}) on page: {}!", webElement, pageName);
  }

  public void clickElementWithAlert(WebElement element, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    try {
      clickElement(element);
    } catch (Exception e) {
      logger.info("alert exception: {}", e.getMessage());
    } finally {
      try {
        logger.info("wait for the alert");
        WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
        Alert alert =
            wait.until(
                (ExpectedCondition<Alert>)
                    webDriver -> {
                      try {
                        return driver.switchTo().alert();
                      } catch (NoAlertPresentException e) {
                        return null;
                      }
                    });
        alert.accept();
      } catch (WebDriverException e) {
        logger.info("alert accept exception: {}", e.getMessage());
      }
    }
  }

  public void returnBeforeWindow() {
    try {
      String handleBefore = driver.getWindowHandle();
      Set<String> getHandles = driver.getWindowHandles();
      Iterator<String> it = getHandles.iterator();
      while (it.hasNext()) {
        String win = it.next();
        if (win.equals(handleBefore)) {
          driver.switchTo().window(win);
          logger.info("Switch Window From " + handleBefore + " to " + win);
          break;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void changeWindows(String windowName) {
    try {
      Set<String> windowsSet = driver.getWindowHandles();
      for (String newHandle : windowsSet) {
        if (!newHandle.equals(windowName)) {
          driver.switchTo().window(newHandle);
          logger.info("Switch Window success From " + windowName + " to " + newHandle);
        }
      }
    } catch (Exception e) {
      logger.info("Handles switch  accept exception: {}", e.getMessage());
    }
  }

  public void longScreenShot(String screenShotFileName) {
    File target = new File(TARGET_SCREENSHOTS_PATH);
    if (!target.exists()) {target.mkdirs();}
     Screenshot screenshot =
             new AShot()
                     .shootingStrategy(ShootingStrategies.viewportPasting(1000))
                     .takeScreenshot(driver);
     try {
       ImageIO.write(screenshot.getImage(),"PNG",new File(TARGET_SCREENSHOTS_PATH + screenShotFileName + ".png"));
       logger.info("Long screenshot obtained successfully ：long screen Shot file Name is {}",screenShotFileName);
     } catch (IOException e) {
       logger.error("Long screenshot obtained fail");
       e.printStackTrace();
     }
  }

  public void screenshot( String screenShotFileName) {
    if (Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"))==true) {
      JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
      int maxWidth = Integer.parseInt(String.valueOf(javascriptExecutor.executeScript("return Math.max(document.body.scrollWidth,document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);")));
      logger.info("获取网页最大宽度:" + maxWidth);
      int maxHeight = Integer.parseInt(String.valueOf(javascriptExecutor.executeScript("return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);")));
      logger.info("获取网页最大高度:" + maxHeight);
      driver.manage().window().setSize(new Dimension(maxWidth, maxHeight));
      File srcfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
      try {
        FileUtils.copyFile(srcfile, new File(TARGET_SCREENSHOTS_PATH + screenShotFileName + ".png"));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"))==false){
      longScreenShot(screenShotFileName);
    }
  }

}
