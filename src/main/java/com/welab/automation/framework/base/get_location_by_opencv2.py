# -*- encoding=utf-8 -*-
__author__ = 'Jeff.xie'

import cv2
import sys

def get_location_percent(img_slider_path, image_background_path, x_percent, y_percent):
    xper = int(x_percent)
    yper = int(y_percent)
    img1=cv2.imread(image_background_path)  #大图
    img2=cv2.imread(img_slider_path)
    #使用SIFT算法获取图像特征的关键点和描述符
    sift=cv2.xfeatures2d.SIFT_create()
    kp1,des1=sift.detectAndCompute(img1,None)
    kp2,des2=sift.detectAndCompute(img2,None)

    #定义FLANN匹配器
    indexParams=dict(algorithm=0,trees=10)
    searchParams=dict(checks=50)
    flann=cv2.FlannBasedMatcher(indexParams,searchParams)
    #使用KNN算法实现图像匹配，并对匹配结果排序
    matches=flann.knnMatch(des1,des2,k=2)
    matches=sorted(matches,key=lambda x:x[0].distance)

    #去除错误匹配，0.5是系数，系数大小不同，匹配的结果页不同
    goodMatches=[]
    for m,n in matches:
        if m.distance<0.5*n.distance:
            goodMatches.append(m)
    #获取某个点的坐标位置
    #index是获取匹配结果的中位数
    index=int(len(goodMatches)/2)
    #queryIdx是目标图像的描述符索引
    x,y=kp1[goodMatches[0].queryIdx].pt  #获取小图片中心点
    #将坐标位置勾画在2.png图片上，并显示
    print(x)
    print(y)
    src_img = cv2.imread(image_background_path,cv2.IMREAD_GRAYSCALE)
    height,width = src_img.shape
    # print("width:",width)
    # print("height:",height)
    des_img = cv2.imread(img_slider_path,cv2.IMREAD_GRAYSCALE)
    des_height,des_width = des_img.shape
    # print("des_width:",des_width)
    # print("des_height:",des_height)

    x1 = int(x-des_width/2)
    y1 = int(y-des_height/2)
    x2 = int(x+des_width/2)
    y2 = int(y+des_height/2)
    click_x = x1+des_width*(xper/100)
    click_y = y1+des_width*(yper/100)
    # print("click_x",click_x)
    # print("click_y",click_y)
    x_loc_percent = int(click_x/width*100)
    y_loc_percent = int(click_y/height*100)
    print("x_loc_percent: "+str(x_loc_percent))
    print("y_loc_percent: "+str(y_loc_percent))
    return x_loc_percent,x_loc_percent


if __name__ == '__main__':
    img_slider_path = sys.argv[1]
    image_background_path = sys.argv[2]
    x_percent = sys.argv[3]
    y_percent = sys.argv[4]
    get_location_percent(img_slider_path, image_background_path,x_percent,y_percent)
    # get_location_percent("D:/WIFI1.png", "D:/Setting.png",50,50)
