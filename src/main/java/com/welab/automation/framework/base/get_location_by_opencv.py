# -*- coding=utf-8 -*-

__author__ = 'Jeff.xie'

import cv2
import sys

def _tran_canny(image):
    """消除噪声"""
    image = cv2.GaussianBlur(image, (3, 3), 0)
    return cv2.Canny(image, 50, 150)

def get_center_location(img_slider_path,image_background_path,x_percent,y_percent):

    # java传递过来的参数都是str类型，所以需要强转成int类型
    xper = int(x_percent)
    yper = int(y_percent)
    print(x_percent)
    # # 参数0是灰度模式
    image = cv2.imread(img_slider_path, 0)
    template = cv2.imread(image_background_path, 0)

    # 寻找最佳匹配
    res = cv2.matchTemplate(_tran_canny(image), _tran_canny(template), cv2.TM_CCOEFF_NORMED)
    # 最小值，最大值，并得到最小值, 最大值的索引
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    #获得背景图像高和宽
    src_img = cv2.imread(image_background_path,cv2.IMREAD_GRAYSCALE)
    h,w = src_img.shape

    #获得需要寻找图像高和宽
    des_img = cv2.imread(img_slider_path,cv2.IMREAD_GRAYSCALE)
    des_img_h,des_img_w = des_img.shape

    trows,tcols = image.shape[:2]  #获得图片的宽度,两种方式都可以
    top_left = max_loc[0]  # 横坐标
    # 展示圈出来的区域
    x, y = max_loc
    # max_loc获取x,y位置坐标，
    xLocation = x + int(des_img_w*xper/100)
    yLocation = y + int(des_img_h*yper/100)
    print("xLocation: "+str(xLocation))
    print("yLocation: "+str(yLocation))
    return xLocation,yLocation

def get_location_percent(img_slider_path,image_background_path,x_percent,y_percent):

    # java传递过来的参数都是str类型，所以需要强转成int类型
    xper = int(x_percent)
    yper = int(y_percent)
    # # 参数0是灰度模式
    image = cv2.imread(img_slider_path, 0)
    template = cv2.imread(image_background_path, 0)

    # 寻找最佳匹配
    res = cv2.matchTemplate(_tran_canny(image), _tran_canny(template), cv2.TM_CCOEFF_NORMED)
    # 最小值，最大值，并得到最小值, 最大值的索引
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    #获得背景图像高和宽
    src_img = cv2.imread(image_background_path,cv2.IMREAD_GRAYSCALE)
    h,w = src_img.shape
    print(h)
    print(w)

    #获得需要寻找图像高和宽
    des_img = cv2.imread(img_slider_path,cv2.IMREAD_GRAYSCALE)
    des_img_h,des_img_w = des_img.shape

    trows,tcols = image.shape[:2]  #获得图片的宽度,两种方式都可以
    top_left = max_loc[0]  # 横坐标
    # 展示圈出来的区域
    x, y = max_loc
    # max_loc获取x,y位置坐标，
    xLocation = x + int(des_img_w*xper/100)
    yLocation = y + int(des_img_h*yper/100)

    x_loc_percent= int(xLocation*100/w)
    y_loc_percent= int(yLocation*100/h)
    print("x_loc_percent: "+str(x_loc_percent))
    print("y_loc_percent: "+str(y_loc_percent))
    return x_loc_percent,x_loc_percent

if __name__ == '__main__':
    img_slider_path = sys.argv[1]
    image_background_path = sys.argv[2]
    x_percent = sys.argv[3]
    y_percent = sys.argv[4]
    get_location_percent(img_slider_path, image_background_path,x_percent,y_percent)


