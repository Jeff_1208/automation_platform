package com.welab.automation.framework.base;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.utils.TimeUtils;
import io.appium.java_client.*;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.screenrecording.CanRecordScreen;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Base64;
import java.util.HashMap;
import java.util.Set;

import static com.welab.automation.framework.GlobalVar.TARGET_RECORDING_PATH;
import static com.welab.automation.framework.GlobalVar.WAIT_TIMEOUT_LOW_S;
import static com.welab.automation.framework.utils.FileUtil.createDirectory;
import static com.welab.automation.framework.utils.Utils.logPass;
import static java.lang.String.format;

public class AppiumBasePage extends BasePage {
  protected static final Logger logger = LoggerFactory.getLogger(AppiumBasePage.class);

  // Common locators and their parts
  protected AppiumDriver<MobileElement> driver;
  protected static final String CLOSING = "')]";
  protected static final String NAME = "//*[contains(@name,'";
  protected static final String CLASS = "//*[contains(@class,'";
  protected static final String NAME_PATTERN = NAME + "%s" + CLOSING;

  /**
   * @Author Ayden @Description enum scroll position ,left scroll,right scroll and middle
   * scroll @Date 2022/1/29 @Param
   *
   * @return
   */
  public enum scrollPosition {
    LEFT,
    RIGHT,
    MID
  }

  public AppiumBasePage() {
    this.driver = (AppiumDriver<MobileElement>) BaseDriver.getMobileDriver();
    super.driver = this.driver;
    PageFactory.initElements(
        new AppiumFieldDecorator(driver, Duration.ofSeconds(WAIT_TIMEOUT_LOW_S)), this);
  }

  /**
   * @Author Ayden @Description Which fork to choose platform to By .For example,By.driver(Android)
   * or By.driver(ios) @Date 2022/1/29 @Param By androidBy, By iosBy
   *
   * @return androidBy or iosBy
   */
  public By fork(By androidBy, By iosBy) {
    return System.getProperty("platform").equalsIgnoreCase("android") ? androidBy : iosBy;
  }

  /**
   * @Author Ayden @Description Which fork to choose platform to xpath .For
   * example,By.xpath(Android) or By.xpath(ios) @Date 2022/1/29 @Param string androidXpath, String
   * iosXpath
   *
   * @return By.xpath(androidXpath), By.xpath(iosXpath)
   */
  public By fork(String androidXpath, String iosXpath) {
    return fork(By.xpath(androidXpath), By.xpath(iosXpath));
  }

  /**
   * @Author Ayden @Description fork pattren android(xpath,param) or ios(xpath,param) @Date
   * 2022/1/29 @Param String androidXpath, String androidParam, String iosXpath, String iosParam
   *
   * @return By.xpath(format(androidXpath, androidParam)), By.xpath(format(iosXpath, iosParam))
   */
  public By forkPattern(
      String androidXpath, String androidParam, String iosXpath, String iosParam) {
    return fork(By.xpath(format(androidXpath, androidParam)), By.xpath(format(iosXpath, iosParam)));
  }

  static Duration duration = Duration.ofSeconds(1);

  /**
   * @param touchPosition move some element might not able to scroll up the screen. So use this
   *     param to specify the touch point is left, right or mid(default) to avoid moving that
   *     element.
   */

  /**
   * @Author Ayden @Description Get the width and height of the window size.Simulate gestures to
   * scroll up the page, The width coordinate remains unchanged,the start of the height coordinate
   * is larger than the end coordinate @Date 2022/1/29 @Param scrollPosition... touchPosition
   *
   * @return
   */
  public void scrollUp(scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 2 / 3;
    final int end_y = height / 4;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
        new TouchAction(driver)
            .press(PointOption.point(x, start_y))
            .waitAction(WaitOptions.waitOptions(duration))
            .moveTo(PointOption.point(x, end_y))
            .release();
    action1.perform();
  }

  public void scrollUpByStartEndHeightPercent(int yPercentStartHeight, int yPercentEndHeight,scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * yPercentStartHeight / 100;
    final int end_y = height * yPercentEndHeight / 100;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public void scrollUpByStartEndPointPercent(int xPercentStartHeight, int yPercentStartHeight,
                                            int xPercentEndHeight, int yPercentEndHeight,scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int start_x = height * xPercentStartHeight / 100;
    int end_x = height * xPercentEndHeight / 100;
    final int start_y = height * yPercentStartHeight / 100;
    final int end_y = height * yPercentEndHeight / 100;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        start_x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        end_x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", start_x, start_y, end_x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(start_x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(end_x, end_y))
                    .release();
    action1.perform();
  }

  public void scrollUpByheightPercent(int yPercentHeight, scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 90 / 100;
    final int end_y = height *(90-yPercentHeight)/ 100;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }
  public void scrollUpFast(scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 2 / 3;
    final int end_y = height / 4;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(100)))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public void scrollUpFast(int mills,scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 2 / 3;
    final int end_y = height / 4;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(mills)))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  /**
   * @Author Peng @Description Click the center point of an element. For iOS, click some pop out
   * element might lead to "Object cannot be nil" error. Then can use this method to click.
   * element @Date 2022/02/17
   *
   * @param element
   */
  public void clickElementCenter(MobileElement element) {
    TouchAction action = new TouchAction(driver);
    action
        .press(PointOption.point(element.getCenter().getX(), element.getCenter().getY()))
        .release()
        .perform();
  }

  /**
   * @Author Ayden @Description Get the width and height of the window size.Simulate gestures to
   * scroll down the page, The width coordinate remains unchanged,the start of the height coordinate
   * is less than the end coordinate @Date 2022/1/29 @Param
   *
   * @return
   */
  public void scrollDown() {
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    TouchAction action2 =
        new TouchAction(driver)
            .press(PointOption.point(width / 2, height / 4))
            .waitAction(WaitOptions.waitOptions(duration))
            .moveTo(PointOption.point(width / 2, height * 3 / 4))
            .release();
    action2.perform();
  }

  public void scrollDownFast(scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height / 4;
    final int end_y = height * 2 / 3;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(20)))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public void scrollDownFast(int mills,scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height / 4;
    final int end_y = height * 2 / 3;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(mills)))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }
  /**
   * @Author Ayden @Description Get the width and height of the window size.Simulate gestures to
   * swipe right to left the page(pull right), The height coordinate remains unchanged,the start of
   * the width coordinate is larger than the end coordinate @Date 2022/1/29 @Param
   *
   * @return
   */
  public void swipeRightToLeft() {
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    logger.info("swipe from right to left");
    TouchAction action =
        new TouchAction(driver)
            .press(PointOption.point(width * 4 / 5, height / 2))
            .waitAction(WaitOptions.waitOptions(duration))
            .moveTo(PointOption.point(width / 5, height / 2))
            .release();
    action.perform();
  }

  /**
   * @Author Ayden @Description Determine the maximum number of times to pull up to find elements,
   * and the number of times each seek until the element is found pass locator. @Date
   * 2022/1/29 @Param By locator, int maxTime, int... timeOut
   *
   * @return MobileElement ele
   */
  public MobileElement scrollUpToFindElement(By locator, int maxTime, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
    MobileElement ele = null;
    int swipeCount = 1;
    while (true) {
      if (ele != null || swipeCount == maxTime) break;
      try {
        ele = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
      } catch (Exception e) {
        logger.error("Element does not find.");
        scrollUp();
        swipeCount++;
        logger.info("Swipe up to find element.");
      }
    }
    return ele;
  }

  /**
   * @Author Ayden @Description Determine the maximum number of times to pull up to find elements,
   * and the number of times each seek until the element is found pass MobileElement. @Date
   * 2022/1/29 @Param MobileElement ele, int maxTime, int... timeOut
   *
   * @return MobileElement ele2
   */
  public MobileElement scrollUpToFindElement(MobileElement ele, int maxTime, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
    MobileElement ele2 = null;
    int swipeCount = 1;
    while (true) {
      if (ele2 != null || swipeCount == maxTime) break;
      try {
        ele2 = (MobileElement) wait.until(ExpectedConditions.visibilityOf(ele));
      } catch (Exception e) {
        logger.error("Element does not find.");
        scrollUp();
        swipeCount++;
        logger.info("Swipe up to find element.");
      }
    }
    return ele2;
  }

  /**
   * @Author Ayden @Description Determine the maximum number of times to drop down to find elements,
   * and the number of times each seek until the element is found pass MobileElement. @Date
   * 2022/1/29 @Param MobileElement ele, int maxTime, int... timeOut
   *
   * @return MobileElement ele2
   */
  public MobileElement scrollDownToFindElement(MobileElement ele, int maxTime, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
    MobileElement ele2 = null;
    int swipeCount = 1;
    while (true) {
      if (ele2 != null || swipeCount == maxTime) break;
      try {
        ele2 = (MobileElement) wait.until(ExpectedConditions.visibilityOf(ele));
      } catch (Exception e) {
        logger.error("Element does not find.");
        scrollDown();
        swipeCount++;
        logger.info("Swipe down to find element.");
      }
    }
    return ele2;
  }

  /**
   * @Author Ayden @Description Get the width and height of the window size.Simulate gestures to
   * swipe left to right the page(pull left), The height coordinate remains unchanged,the start of
   * the width coordinate is less than the end coordinate @Date 2022/1/29 @Param
   *
   * @return
   */
  public void swipeLeftToRight() {
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    TouchAction action =
        new TouchAction(driver)
            .press(PointOption.point(width / 5, height / 2))
            .waitAction(WaitOptions.waitOptions(duration))
            .moveTo(PointOption.point(width * 4 / 5, height / 2))
            .release();
    action.perform();
  }

  public MobileElement findMobileElement(By by) {
    return driver.findElement(by);
  }

  // Android only
  public void openNotification() {
    AndroidDriver androidDriver = (AndroidDriver) driver;
    androidDriver.openNotifications();
    logger.info("Open notification!");
  }

  public void launchIosAPP(String bundleId) {
    HashMap<String, Object> args = new HashMap<>();
    args.put("bundleId", bundleId);
    driver.executeScript("mobile: launchApp", args);
  }

  public void activeIosAPP(String bundleId) {
    HashMap<String, Object> args = new HashMap<>();
    args.put("bundleId", bundleId);
    driver.executeScript("mobile: activateApp", args);
  }

  public void closeIosAPP(String bundleId) {
    HashMap<String, Object> args = new HashMap<>();
    args.put("bundleId", bundleId);
    driver.executeScript("mobile: terminateApp", args);
  }

  /**
   * @Author Ayden @Description start up Andriod App @Date 2022/1/29 @Param String packageName,
   * String activityName
   *
   * @return
   */
  public void openAndriodApp(String packageName, String activityName) {
    AndroidDriver androidDriver = (AndroidDriver) driver;
    Activity activity = new Activity(packageName, activityName);
    androidDriver.startActivity(activity);
  }

  /**
   * @Author Ayden @Description Activate the given app onto the Andriod device @Date
   * 2022/1/29 @Param String packageName
   *
   * @return
   */
  public void activeAndriodApp(String packageName) {
    driver.activateApp(packageName);
  }

  public void terminateApp(String packageName) {
    driver.terminateApp(packageName);
    logger.info("{} Application termination succeeded ",packageName);
  }

  /**
   * @Author Ayden @Description After entering the value, click to hide the keyboard @Date
   * 2022/1/29 @Param
   *
   * @return
   */
  public void hideKeyboard() {
    driver.hideKeyboard();
    logger.info("Hide keyboard!");
  }

  /**
   * @Author Ayden @Description First determine which platform, then call up the keyboard, enter the
   * sendKeys @Date 2022/1/29 @Param
   *
   * @return
   */
  public void inputEnter(MobileElement ele) {
    if (System.getProperty("mobile").equals("android")) driver.getKeyboard().pressKey(Keys.ENTER);
    else if (System.getProperty("mobile").equals("ios")) ele.sendKeys(Keys.ENTER);
    logger.info("input ENTER to element({}) on page: {}!", ele, pageName);
  }

  private Dimension screenSize;

  // Works on iOS
  public void showNotifications() {
    manageNotifications(true);
    logger.info("Show notification!");
  }

  // Works on iOS
  public void hideNotifications() {
    manageNotifications(false);
    logger.info("Hide notification!");
  }

  private void manageNotifications(Boolean show) {
    int yMargin = 5;
    int xMid = screenSize.width / 2;
    PointOption top = PointOption.point(xMid, yMargin);
    PointOption bottom = PointOption.point(xMid, screenSize.height - yMargin);

    TouchAction action = new TouchAction(driver);
    PointOption direct = show ? top : bottom;
    action.press(direct);
    action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)));
    PointOption directAfter = show ? bottom : top;
    action.press(directAfter);
    action.perform();
  }

  /**
   * @Author Ayden @Description click notification,position margin @Date 2022/1/29 @Param int
   * yMargin
   *
   * @return
   */
  public void clickNotification(int yMargin) {
    int width = driver.manage().window().getSize().width;
    int xMid = width / 2;
    PointOption target = PointOption.point(xMid, yMargin);
    TouchAction action = new TouchAction(driver);
    action.press(target);
    action.perform();
  }

  /**
   * @Author Ayden @Description Run app in background @Date 2022/1/29 @Param
   *
   * @return
   */
  public void runAppInBackground() {
    driver.runAppInBackground(Duration.ofSeconds(-1));
    logger.info("Run app in background!");
  }

  /**
   * @Author Ayden @Description Start recording video method. Determine whether it is the ios
   * platform or the Android platform, if the Android platform, call
   * AndroidStartScreenRecordingOptions else call IOSStartScreenRecordingOptions @Date
   * 2022/1/29 @Param
   *
   * @return
   */
  public void startRecording() {
    logger.info("* Start recording video...");
    if (!IS_IOS) {
      ((CanRecordScreen) BaseDriver.getMobileDriver())
          .startRecordingScreen(
              new AndroidStartScreenRecordingOptions().withVideoSize("1080x1920").withTimeLimit(Duration.ofMillis(900000)));
    } else {
      // if device is iOS 15, then need to use appium 1.22
      // if device is iOS 14.x or lower version, need to use appium 1.21
      // IOS screen recording video is a special format, need to use VLC media player to play.
      ((CanRecordScreen) BaseDriver.getMobileDriver())
          .startRecordingScreen(new IOSStartScreenRecordingOptions().withVideoScale("1080x1920").withTimeLimit(Duration.ofMillis(900000)));
    }
  }

  /**
   * @Author Ayden @Description Stop recording video method.Store the recorded video in a specified
   * path @Date 2022/1/29 @Param String videoName
   *
   * @return
   */
  public void stopRecording(String videoName) {
    logger.info("* End recording video...");
    String base64Video = ((CanRecordScreen) BaseDriver.getMobileDriver()).stopRecordingScreen();
    createDirectory(TARGET_RECORDING_PATH);
    String videoPath = TARGET_RECORDING_PATH + videoName + ".mov";
    try {
      Files.write(Paths.get(videoPath), Base64.getDecoder().decode(base64Video));
      logger.info("Video is saved successfully: " + videoPath);
    } catch (IOException e) {
      logger.error("Failed to save video: " + e.getLocalizedMessage());
    }
  }

  /**
   * @Author Ayden @Description Incoming x, y coordinates, click coordinates @Date 2022/1/29 @Param
   * int x, int y
   *
   * @return
   */
  public void clickCoordinates(int x, int y) {
    try {
      TouchAction action = new TouchAction(driver);
      PointOption pointOption = new PointOption();
      pointOption.withCoordinates(x, y);
      TapOptions tapOptions = new TapOptions();
      tapOptions.withPosition(pointOption);
      action.tap(tapOptions).perform().release();
      Thread.sleep(2000);
      logger.info("Click coordinates ({}, {}) successfully", x, y);
    } catch (Exception e) {
      logger.error("Failed to Click coordinates ({}, {})", x, y);
      e.printStackTrace();
    }
  }

  /**
   * @Author Ayden @Description push file to remote path @Date 2022/1/29 @Param String remotePath,
   * String fileName
   *
   * @return
   */
  public void pushFiles(String remotePath, String fileName) {
    try {
      AndroidDriver androidDriver = (AndroidDriver) this.driver;
      androidDriver.pushFile(remotePath, new File(fileName));
      logger.info("Push file {} to {} successfully", fileName, remotePath);
    } catch (Exception e) {
      logger.error("Failed to push file {} to {}!", fileName, remotePath);
      e.printStackTrace();
    }
  }

  /**
   * @Author Ayden @Description Determine whether the switch is selected, if not, click it @Date
   * 2022/1/29 @Param MobileElement ele
   *
   * @return
   */
  public void setSwithToChecked(MobileElement ele) {
    boolean ischecked = getEleCheckStatus(ele);
    if (!ischecked) {
      clickElement(ele);
    }
  }

  /**
   * @Author Ayden @Description Element to determine whether to be detected @Date 2022/1/29 @Param
   * MobileElement ele
   *
   * @return boolean parseBoolean(status)
   */
  public boolean getEleCheckStatus(MobileElement ele) {
    String status = ele.getAttribute("checked");
    return Boolean.parseBoolean(status);
  }

  /**
   * @Author Ayden @Description Pass in x, y location, click on the location element @Date
   * 2022/1/29 @Param int x, int y
   *
   * @return
   */
  public void clickByLocation(int x, int y) {
    TouchAction touchAction = new TouchAction(driver);
    PointOption pointOption = PointOption.point(x, y);
    touchAction.press(pointOption).release().perform();
    logger.info("click xlocation: " + x);
    logger.info("click ylocation: " + y);
  }

  public void longClickByLocation(int x, int y) {
    TouchAction touchAction = new TouchAction(driver);
    PointOption pointOption = PointOption.point(x, y);
    touchAction.longPress(pointOption).release().perform();
    logger.info("click xlocation: " + x);
    logger.info("click ylocation: " + y);
  }

  public void clickByLocationByPercent(double xPercent, double yPercent) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = (int)(width * xPercent / 100);
    int y = (int)(height * yPercent / 100);
    logger.info("click xPercent: " + xPercent);
    logger.info("click yPercent: " + yPercent);
    logger.info("screen width: " + width);
    logger.info("screen height: " + height);
    clickByLocation(x, y);
  }

  public void clickByLocationByPercent2(double xPercent, double yPercent) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = (int)(width * xPercent / 100);
    int y = (int)(height * yPercent / 100);
    logger.info("click xPercent: " + xPercent);
    logger.info("click yPercent: " + yPercent);
    clickByLocation(x, y);
  }

  public int getYPercentByElement(MobileElement ele){
    Point point = ele.getLocation();
    int y = point.getY();
    final int height = driver.manage().window().getSize().height;
    int yPercent =  100 * y / height;
    return yPercent + 1;
  }
  public int getXPercentByElement(MobileElement ele){
    Point point = ele.getLocation();
    int x = point.getX();
    final int width = driver.manage().window().getSize().width;
    int xPercent =  100 * x / width;
    return xPercent;
  }
  public void getKeyboard(String text) {
    driver.getKeyboard().sendKeys(text);
    logPass(" input text " + text);
  }


  public void swipeControl(By by1, By by) {
    // 获取控件开始位置的坐标轴
    Point start = this.findElement(by1).getLocation();
    int startX = start.x;
    int startY = start.y;
    // 获取控件坐标轴差
    Dimension q = this.findElement(by).getSize();
    int x = q.getWidth();
    int y = q.getHeight();
    // 计算出控件结束坐标
    int endX = x + startX;
    int endY = y + startY;
    // 计算中间点坐标
    int centreX = (endX + startX) / 2;
    int centreY = (endY + startY) / 2;

    TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
    PointOption pointOption1 = PointOption.point(centreX, centreY + 30);
    PointOption pointOption2 = PointOption.point(centreX, centreY - 30);
    Duration duration = Duration.ofMillis(500);
    WaitOptions waitOptions = WaitOptions.waitOptions(duration);
    touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
            .perform();
    // 向下滑动
//      case DOWN:
//        driver.swipe(centreX, centreY - 30, centreX, centreY + 30, 500);
//        break;
  }

  public void clickMobileElementByPicture(String picPath) throws URISyntaxException, IOException {
    File file = new File(picPath);
    String base64String = Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
    By by= MobileBy.image(base64String);
    findMobileElement(by).click();
  }

  @SneakyThrows
  public int getYlocation(String picturePath,int xPercent,int yPercent){
    takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
    Thread.sleep(1000);
    String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv.py";
    HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
    int x =map.get("xPer");
    int y =map.get("yPer") + 1;
    return y;
  }

  @SneakyThrows
  public int getYlocation2(String picturePath,int xPercent,int yPercent){
    takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
    Thread.sleep(1000);
    String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv2.py";
    HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
    int x =map.get("xPer");
    int y =map.get("yPer");
    return y;
  }

  @SneakyThrows
  public int[] clickByPicture(String picturePath,int xPercent,int yPercent){
    takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
    Thread.sleep(1000);
    String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv.py";
    HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
    int x =map.get("xPer");
    int y =map.get("yPer");
    clickByLocationByPercent(x,y);
    return inputLocation(x,y);
  }

  public int[] inputLocation(int xPercent,int yPercent){
    int[] location = new int[2];
    location[0]=xPercent;
    location[1]=yPercent;
    return location;
  }

  @SneakyThrows
  public int[] clickByPicture2(String picturePath,int xPercent,int yPercent){
    takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
    Thread.sleep(1000);
    String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv2.py";
    HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
    int x =map.get("xPer");
    int y =map.get("yPer");
    clickByLocationByPercent(x,y);
    return inputLocation(x,y);
  }

  public static boolean isWindows(){
    String osName = System.getProperty("os.name");
    if (osName.startsWith("Windows")) {
      return true;
    } else {
      return false;
    }
  }

  public static HashMap<String, Integer> run_opencv(String pythonCodePath,String picturePath, String PagePicturePath, int xPercent, int yPercent) {
    HashMap<String, Integer> location = new HashMap<>();
    try {
      String python_execute_path ="";
      if (isWindows()) {
        python_execute_path ="python "+pythonCodePath;
      } else {
        python_execute_path ="python3 "+pythonCodePath;
      }
      String cmds = String.format("%s %s %s %s %s", python_execute_path,picturePath,PagePicturePath,xPercent+"",yPercent+"");
      System.out.println("Executing python script for picture location.");
      System.out.println(cmds);
      Process pcs = Runtime.getRuntime().exec(cmds);
      pcs.waitFor();
      Thread.sleep(1000);

      String result = null;
      BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
      BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
      String lineStr = null;
      while ((lineStr = br.readLine()) != null) {
        result = lineStr;//Python 代码中print的数据就是返回值
        System.out.println(result);
        if(lineStr.contains("xLocation")) {
          int x =  Integer.parseInt(lineStr.split(":")[1].trim());
          location.put("x", x);
        }
        if(lineStr.contains("yLocation")) {
          int x =  Integer.parseInt(lineStr.split(":")[1].trim());
          location.put("y", x);
        }
        if(lineStr.contains("x_loc_percent")) {
          int x =  Integer.parseInt(lineStr.split(":")[1].trim());
          location.put("xPer", x);
        }
        if(lineStr.contains("y_loc_percent")) {
          int x =  Integer.parseInt(lineStr.split(":")[1].trim());
          location.put("yPer", x);
        }
      }
      br.close();
      in.close();
      System.out.println(location.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return location;
  }

  public void scrollInProgress(String y){

    int yy =Integer.valueOf(y);
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 2 / 3;
    final int end_y = 816;
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, yy))
                    .release();
    action1.perform();
  }

  public int getElementCenterY(MobileElement element) {
    return element.getCenter().getY();
  }

  public void scrollByLocation(int startPerX,int endPerX,int y){
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    final int start_x = width * startPerX / 100;
    final int end_x = width * endPerX / 100;
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(start_x, y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(end_x, y))
                    .release();
    action1.perform();
  }

  public void scrollByLocationPercentFromOnePointToOtherPoint(double startPerX, double startPerY, double endPerX, double endPerY){
    int width = driver.manage().window().getSize().width;
    int height = driver.manage().window().getSize().height;
    final int start_x = (int) (width * startPerX / 100);
    final int end_x = (int) (width * endPerX / 100);
    final int start_y = (int) (height * startPerY / 100);
    final int end_y = (int) (height * endPerY / 100);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(start_x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(end_x, end_y))
                    .release();
    action1.perform();
  }

  public MobileElement scrollUpEntireToFindElement(MobileElement ele, int maxTime, int... timeOut) {
    int actualTimeout = TimeUtils.setTimeOut(timeOut);
    WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
    MobileElement ele2 = null;
    int swipeCount = 1;
    while (true) {
      if (ele2 != null || swipeCount == maxTime) break;
      try {
        ele2 = (MobileElement) wait.until(ExpectedConditions.visibilityOf(ele));
      } catch (Exception e) {
        logger.error("Element does not find.");
        scrollUpEntireScreen();
        swipeCount++;
        logger.info("Swipe up to find element.");
      }
    }
    return ele2;
  }

  public void scrollUpEntireScreen(scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * 12 / 13;
    final int end_y = height / 15;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public void scrollLeft(scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 4;
    final int start_y = height * 10 / 13;
    final int end_y = height / 3;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public void contextWebview( ) {
    Set<String> contextNames = driver.getContextHandles();
    for (String contextName : contextNames) {
      logger.info("Context Names Found ："+contextName);
    }
    BasePage basePage=new BasePage();
    basePage.driver= (AppiumDriver<MobileElement>) driver.context((String) contextNames.toArray()[1]);
    logger.info("I switched from native to "+driver.getContextHandles());
  }

  public void returnToNayive() {
    driver.getContextHandles().forEach((context) -> {
      if (context.toString().contains("NATIVE_APP")) {
        BasePage basePage=new BasePage();
        basePage.driver=  (AppiumDriver<MobileElement>)  driver.context(context.toString());
      }
    });
  }

  public void scrollUpByPercent(int startY, int endY , scrollPosition... touchPosition) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int x = width / 2;
    final int start_y = height * startY / 100;
    final int end_y =  height * endY / 100;
    if (0 != touchPosition.length) {
      if (touchPosition[0].equals(scrollPosition.LEFT)) {
        x = width / 10;
      } else if (touchPosition[0].equals(scrollPosition.RIGHT)) {
        x = width * 9 / 10;
      }
    }
    logger.info("scroll from ({}, {}) to ({}, {})", x, start_y, x, end_y);
    TouchAction action1 =
            new TouchAction(driver)
                    .press(PointOption.point(x, start_y))
                    .waitAction(WaitOptions.waitOptions(duration))
                    .moveTo(PointOption.point(x, end_y))
                    .release();
    action1.perform();
  }

  public boolean isIOS() {
    if (System.getProperty("mobile")==null) return false;
    if (System.getProperty("mobile").equalsIgnoreCase("ios")) return true;
    return false;
  }

  public boolean isAndroid() {
    if (System.getProperty("mobile")==null) return false;
    if (System.getProperty("mobile").equalsIgnoreCase("android")) return true;
    return false;
  }

  public boolean isIphoneSE(){
    if(isAndroid()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("ios.type");
    if(type==null || type.length() < 1) return false;
    if(type.contains("iPhone SE")) return true;
    return false;
  }

  public boolean isIphone7Plus(){
    if(isAndroid()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("ios.type");
    if(type==null || type.length() < 1) return false;
    if(type.contains("iPhone 7 Plus")) return true;
    return false;
  }

  public boolean isSamsungA3460(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    if(type.contains("SM_A3460")) return true;
    return false;
  }

  @SneakyThrows
  public void scrollUpByTimes( int num){
    for (int i = 0; i < num; i++) {
      scrollUp();
      Thread.sleep(10);
    }
  }
  public boolean isIphoneXR(){
    if(isAndroid()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("ios.type");
    if(type==null || type.length() < 1) return false;
    if(type.contains("iPhone XR")) return true;
    return false;
  }

  public boolean isIphone13(){
    if(isAndroid()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("ios.type");
    if(type==null || type.length() < 1) return false;
    if(type.contains("iPhone 13")) return true;
    return false;
  }
  public void slipeByLocationPercent(int percentX1, int percentY1, int percentX2, int percentY2) {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    int y1 = height * percentY1 / 100;
    int y2 = height * percentY2 / 100;
    int start_x = width * percentX1 / 100;
    int end_x = width * percentX2 / 100;

    TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
    PointOption pointOption1 = PointOption.point(start_x, y1);
    PointOption pointOption2 = PointOption.point(end_x, y2);
    Duration duration = Duration.ofMillis(1000);
    WaitOptions waitOptions = WaitOptions.waitOptions(duration);
    touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
            .perform();
  }

  //  Redmi Note 12 Get the model number displayed as 22111317G
  public boolean isRedMiNote12(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    return type.contains("redmi Note 12");
  }

  public boolean isSamSungS20(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    return type.contains("SAMSUNG S20");
  }

  public boolean isXiaomiA3(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    return type.contains("Mi_A3");
  }

  public boolean isOnePlus(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    return type.contains("ONEPLUS_A6010");
  }
  public boolean isSamsungA5460(){
    if(isIOS()) return false;
    String type = GlobalVar.GLOBAL_VARIABLES.get("android.type");
    if(type==null || type.length() < 1) return false;
    return type.contains("SM_A5460");
  }

}
