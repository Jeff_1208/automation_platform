package com.welab.automation.framework.base;

import com.alibaba.fastjson.JSONObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RestAPI {
  public static String baseURI;
  public static String token;
  private static final Logger logger = LoggerFactory.getLogger(RestAPI.class);

  public static Response get(String api_path) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    Map<String, Object> headers = new HashMap<>();
    if (!token.equals("")) {
      headers.put("Authorization", "Bearer " + token);
    }
    logger.info(String.format("API: %s, Headers: %s", baseURI + api_path, headers));
    Response response = RestAssured.given().headers(headers).get();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static Response get(String api_path, Map<String, Object> headers) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    logger.info(String.format("API: %s, Headers: %s", api_path, headers));
    Response response = RestAssured.given().headers(headers).get();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static Response get(
      String api_path, Map<String, Object> headers, Map<String, Object> params) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    logger.info(
        String.format("API: %s, Headers: %s, Params: %s", baseURI + api_path, headers, params));
    Response response = RestAssured.given().headers(headers).params(params).get();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static Response post(String api_path, Object body) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    RestAssured.useRelaxedHTTPSValidation();
    logger.info(String.format("API: %s, Body: %s", baseURI + api_path, body));
    Response response = RestAssured.given().contentType(ContentType.JSON).body(body).post();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static Response post(String api_path, Map<String, Object> headers, Object body) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    RestAssured.useRelaxedHTTPSValidation();
    logger.info(String.format("API: %s, Headers: %s, Body: %s", baseURI + api_path, headers, body));
    Response response =
        RestAssured.given().contentType(ContentType.JSON).headers(headers).body(body).post();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static Response put(String api_path, Map<String, Object> headers, Object body) {
    RestAssured.baseURI = baseURI;
    RestAssured.basePath = api_path;
    RestAssured.useRelaxedHTTPSValidation();
    logger.info(String.format("API: %s, Headers: %s, Body: %s", baseURI + api_path, headers, body));
    Response response =
        RestAssured.given().contentType(ContentType.JSON).headers(headers).body(body).put();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    return response;
  }

  public static void main(String[] args) throws Exception {
    String url = "https://sit.connectatchangi.com";

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("email", "lisatest2021@sina.com");
    //    //    String resp = post(url, jsonObject);
    //    //    JSONObject js = JSONObject.parseObject(resp);
    //    //    System.out.println(js.getString("message"));
    //    Response resp = post_n(url, jsonObject);
    //    Assert.assertEquals(resp.statusCode(), 200);

    RestAPI.baseURI = url;
    //    Response response = post("/api/Users/magiclink", jsonObject);

    //    int statusCode = response.getStatusCode();
    //    Assert.assertEquals(statusCode, 200);
    //    File f = new File("expect.json");
    //    try {
    //      RestAssured.given()
    //          .contentType(ContentType.JSON)
    //          .body(jsonObject)
    //          .post(url + "/api/Users/magiclink")
    //          .then()
    //          .assertThat()
    //          .body(JsonSchemaValidator.matchesJsonSchema(f));
    //    } catch (Exception e) {
    //      //      Assert.fail("eeeee");
    //      logger.info("json schema checking failed!");
    //    }
  }
}
