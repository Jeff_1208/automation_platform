package com.welab.automation.framework.utils.entity.api;

public final class RequestHeader {

    public static final String MAC = "mac";
    public static final String TIME_SPAN = "timespan";
    public static final String EXPIRED = "expired";
    public static final String SIGNATURE = "signature";
    public static final String LANGUAGE_CODE = "languagecode";
    public static final String OCP_APIM_SUB_KEY = "Ocp-Apim-Subscription-Key";
    public static final String CLIENT_ID = "CLIENT_ID";
    public static final String CLIENT_SECRET = "CLIENT_SECRET";
    public static final String AUTHORIZATION = "Authorization";
    public static final String COOKIE="Cookie";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
}
