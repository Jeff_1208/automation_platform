package com.welab.automation.framework.utils.estatement;

public class Global {
	
//	public static final String   LOANS_STRING ="WeLab 私人貸款 WeLab Personal Loan";
	public static final String   LOANS_STRING ="GoFlexi";
	public static final String   IMPORTANT_NOTES ="重要提示";
	public static final String	 CURRENTLY_HELD_STRING = "目前持有 Currently Held";
	public static final String   TRANSACTION_HISTORY_START_STRING ="交易紀錄 Transaction History";
	public static final String   TOTAL_AMOUNT_STRING ="結存總額";
	public static final String   CORE_ACCOUNT_AMOUNT_STRING ="核心賬戶";
	public static final String   PERSONAL_LOAN_STRING ="私人貸款";
	
	public static final String  GOSAVE_INTEREST_STRING ="GoSave利息";
	public static final String	GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING = "本金轉至核心賬戶";
	public static final String	GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING = "利息轉至核心賬戶";
	public static final String	DEPOSIT_TO_GOSAVE_STRING = "存入GoSave";
	public static final String	WELAB_PERSONAL_LOAN_STRING = "私人貸款 WeLab Personal Loan";
	public static final String	BANK_CODE_STRING = "銀行代碼 Bank Code";
	public static final String	BRANCH_CODE_STRING = "分行編號 Branch Code";

	public static final String	BALANCE_FROM_PREVIOUS_STATEMENT_STRING = "承上結餘";
	public static final String	CLOSING_BALANCE_STRING = "戶口結餘";
	public static final String	DEPOSIT_INTEREST_STRING = "存款利息";
	public static final String	RECEIVE_MONEY_STRING = "收款";
	public static final String	SEND_MONEY_STRING = "付款";
	public static final String	DEBIT_CARD_SPENDING_STRING = "Debit Card 消費";
	public static final String	DEBIT_CARD_STRING = "Debit Card";
	public static final String	CASH_REBATE_STRING = "現金回贈";
	public static final String	ADD_MONEY_STRING = "存入款項";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING = "GoSave 轉至核心賬戶";
	public static final String	JETCO_ATM_WITHDRAWAL_STRING = "銀通櫃員機提款";
	public static final String	JETCO_ATM_REFUND_STRING = "銀通櫃員機退款";
	public static final String	JETCO_ATM_STRING = "銀通櫃員機";
	public static final String	OTHER_STRING = "其他";
	public static final String	OVERSEAS_ATM_WITHDRAWAL_STRING = "Cirrus/海外櫃員機提款";
	public static final String	OVERSEAS_ATM_REFUND_STRING = "Cirrus/海外櫃員機退款";
	public static final String	OVERSEAS_ATM_STRING = "海外櫃員機";
	public static final String	MONTHLY_REPAYMENT_STRING = "每月供款";
	public static final String	LOAN_DRAWDOWN_STRING = "提取貸款";
	public static final String	DEBIT_ARRANGEMENT_STRING = "Debit Arrangement";
	public static final String	ACCOUNT_CLOSED_STRING = "取消賬戶";
	
	public static final String	CORE_ACCOUNT_STRING = "核心賬戶";
	public static final String	MONTHLY_STATEMENT_STRING = "月結單";
	public static final String	YOUR_BANK_STATEMENT_STRING = "你的銀行月結單Your Bank Statement";
	
	public static final String	DATE_OF_ISSUE_STRING ="列印日期 Date of issue";
	public static final String	ACCOUNT_NUMBER_STRING ="賬戶號碼";
	public static final String	ACCOUNT_NUMBER_FULL_STRING ="賬戶號碼 Account Number :";

}
