package com.welab.automation.framework.utils.entity.api;

public final class EnvironmentEntity {

  public static final String PLATFORM = "platform";
  public static final String ENV = "env";
  public static final String MOBILE = "mobile";
}
