package com.welab.automation.framework.utils.api;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.Utils;
import com.welab.automation.framework.utils.entity.api.ConfigUtils;
import com.welab.automation.framework.utils.entity.api.RetryListener;
import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import static com.welab.automation.framework.GlobalVar.HEADERS;
import static org.apache.http.HttpHeaders.AUTHORIZATION;

@Listeners({RetryListener.class})
public class BaseRunner {
    protected static final Logger logger = LoggerFactory.getLogger(BaseRunner.class);
    FeatureCreator featureCreator;
    protected Response response =null;

    @BeforeSuite(alwaysRun = true)
    public void BeforeSuite() {
        Utils.clearTestReport(50);
    }

    @BeforeClass
    public void BeforeClass(String ...config)
    {
        generateHeaderForWealth();
        SignatureUtil signatureUtil = new SignatureUtil(config);
        signatureUtil.getPublicKeyReq();
        int offset = signatureUtil.getOffset();
        signatureUtil.createSignReq(offset);
        HEADERS.put(AUTHORIZATION, GlobalVar.GLOBAL_VARIABLES.get(AUTHORIZATION));
    }

    public void generateHeaderForWealth()
    {
        if(!GlobalVar.GLOBAL_VARIABLES.containsKey("CLIENT_ID"))
        {
            Map<String, String> globalVars = ConfigUtils.getPropertiesToMap();
            GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
        }
        GlobalVar.HEADERS.put("Content-Type","application/json");
        GlobalVar.HEADERS.put("Client-Id",GlobalVar.GLOBAL_VARIABLES.get("CLIENT_ID"));
        GlobalVar.HEADERS.put("Client-Secret",GlobalVar.GLOBAL_VARIABLES.get("CLIENT_SECRET"));
        GlobalVar.HEADERS.put("Accept-Encoding","gzip,deflate,br");
        GlobalVar.HEADERS.put("accept-language","en-US");
    }

    public void generateHeaderForLoans()
    {
        GlobalVar.HEADERS.put("Content-Type","application/json");
        GlobalVar.HEADERS.put("accept","*/*");
        GlobalVar.HEADERS.put("Accept-Language","en-US");
    }

    public void generateFeature()
    {
        String filepath="";
        String env = System.getProperty("env");
        featureCreator = new FeatureCreator(env);
        if(!env.isEmpty())
            filepath = GlobalVar.TEST_DATA_FILE_PATH+env+"/";
        else
            filepath = GlobalVar.TEST_DATA_FILE_PATH;
        try{
            featureCreator.generatFeatureAllCases(filepath,"templateDefault.feature");
        }catch(Exception ex)
        {
            logger.error("generate feature error");
        }
    }
    @AfterSuite
    public void AfterSuite () throws IOException
    {
        System.setProperty("platform", "api");
        System.setProperty("env", "welab");
        LocalDateTime now = LocalDateTime.now();
        String path = DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm-ss").format(now);
        Utils.generateAllureReport(path);
    }
}
