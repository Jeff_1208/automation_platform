package com.welab.automation.framework.utils.entity.api;

import lombok.Data;

@Data
public class ApiResult {
    private String path;
    private String method;
    private String result;
    private long duration;
}
