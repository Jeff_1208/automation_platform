package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.common.annotation.Multipart;
import lombok.Data;

@Data
public class MultipartParameter {
    private String controlName; // name
    private Object controlValue; // value
    private Boolean bFile; //file
    private String mineType; //reserve field

    public MultipartParameter() {

    }
    public MultipartParameter(boolean isFile,String controlName) {
        setBFile(isFile);
        setControlName(controlName);
    }
    public MultipartParameter(Multipart anno) {
        setBFile(anno.isFile());
        setControlName(anno.controlName());
//        setMineType(anno.mineType());
    }
}
