

package com.welab.automation.framework.utils.entity.app.android;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppAccessibility {
    private String caseNumber;

    private String indexItem;

    private String itemText;
}
