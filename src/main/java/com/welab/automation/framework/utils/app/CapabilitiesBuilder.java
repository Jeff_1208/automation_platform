package com.welab.automation.framework.utils.app;

import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.entity.api.EnvironmentEntity;
import com.welab.automation.framework.utils.entity.app.DeviceAppEntity;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static io.appium.java_client.remote.MobileCapabilityType.*;
import static org.openqa.selenium.remote.CapabilityType.PLATFORM_NAME;

public class CapabilitiesBuilder {
  private static final Logger logger = LoggerFactory.getLogger(BaseDriver.class);
  private static DesiredCapabilities capabilities;

  private CapabilitiesBuilder() {}

  public static DesiredCapabilities getAndroidCloudCapabilities(String deviceName) {
    JSONObject devices = FileUtil.getJsonObj(GlobalVar.DEVICE_CAPS_JSON_PATH);
    JSONObject devices_caps = devices.getJSONObject(deviceName);
    capabilities = new DesiredCapabilities(devices_caps);
    capabilities.setCapability("automationName", "UiAutomator2");
    capabilities.setCapability("keepDevice", true);
    capabilities.setCapability("noReset", true);
    setCommonCapabilities();
    logger.info(String.valueOf(capabilities));
    return capabilities;
  }

  public static void setCommonCapabilities() {
    if (System.getProperty("env").equalsIgnoreCase("stage")) {
      capabilities.setCapability("appPackage", "welab.bank.mobile.stage");
      capabilities.setCapability("appActivity", "welab.bank.mobile.stage");
    } else if (System.getProperty("env").equalsIgnoreCase("sit")) {
      capabilities.setCapability("appPackage", "welab.bank.sit");
      capabilities.setCapability("appActivity", "com.welabfrontend.MainActivity");
    } else if (System.getProperty("env").equalsIgnoreCase("")) {
      capabilities.setCapability("appPackage", "com.vodqareactnative");
      capabilities.setCapability("appActivity", "com.vodqareactnative.MainActivity");
    }
  }

  public static DesiredCapabilities getIOSCloudCapabilities(String deviceName) {
    JSONObject devices = FileUtil.getJsonObj(GlobalVar.DEVICE_CAPS_JSON_PATH);
    JSONObject devices_caps = devices.getJSONObject(deviceName);
    capabilities = new DesiredCapabilities(devices_caps);
    capabilities.setCapability("bundleId", GlobalVar.GLOBAL_VARIABLES.get("ios.bundleId"));
    capabilities.setCapability("automationName", "XCUITest");
    capabilities.setCapability("keepDevice", true);
    capabilities.setCapability("noReset", true);
    //    setCommonCapabilities();
    logger.info(String.valueOf(capabilities));
    return capabilities;
  }

  public static DesiredCapabilities getAndroidLocalCapabilities() {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    if (!TestDataReader.getAppPath().contains(".apk")) {
      capabilities.setCapability("appPackage", TestDataReader.getAppPackage());
      capabilities.setCapability("appActivity", TestDataReader.getAppActivity());
    } else {
      capabilities.setCapability(APP, System.getProperty("user.dir") + TestDataReader.getAppPath());
    }
    String language = System.getProperty("language");
    if (language!=null){
      if (language.equals("zh")){
        capabilities.setCapability("language", "zh");
        capabilities.setCapability("locale", "zh");
      }
      if (language.equals("en")){
        capabilities.setCapability("language", "GB");
        capabilities.setCapability("locale", "UK");
      }
    }
    capabilities = unlockAndroidPhone(capabilities);
    capabilities.setCapability("chromedriverExecutableDir",
            DeviceAppEntity.CHROMEDRIVEReXECUTABLEDIR);
    capabilities.setCapability(DEVICE_NAME, TestDataReader.getDeviceName());
    capabilities.setCapability(
        PLATFORM_NAME,
        System.getProperty(EnvironmentEntity.PLATFORM).equalsIgnoreCase("ios") ? "iOS" : "Android");
    capabilities.setCapability(PLATFORM_VERSION, TestDataReader.getVersion());
    capabilities.setCapability(AUTOMATION_NAME, TestDataReader.getAutomationName());
    capabilities.setCapability(NO_RESET, true);
    capabilities.setCapability(
        MobileCapabilityType.NEW_COMMAND_TIMEOUT, TestDataReader.getNewCommandTimeout());
    logger.info(String.valueOf(capabilities));
    return capabilities;
  }

  public static DesiredCapabilities unlockAndroidPhone(DesiredCapabilities capabilities){
    String password = GlobalVar.GLOBAL_VARIABLES.get("android.phone.password");
    String androidDeviceName = GlobalVar.GLOBAL_VARIABLES.get("android.deviceName");
    if(androidDeviceName.contains("9b2157cfaedb")){
      password="1234";
    }
    if(androidDeviceName.contains("RFCWA17MW0Y")){
      password="236589";
    }
    if(password != null && password.length() > 1){
      capabilities.setCapability("unlockType", "password");
      capabilities.setCapability("unlockKey", password);
    }
    return capabilities;
  }

  public static DesiredCapabilities getIosLocalCapabilities() {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("bundleId", TestDataReader.getBundleId());
    capabilities.setCapability(DEVICE_NAME, TestDataReader.getDeviceName());
    capabilities.setCapability(
        PLATFORM_NAME,
        System.getProperty(EnvironmentEntity.MOBILE).equalsIgnoreCase("ios") ? "iOS" : "Android");
    capabilities.setCapability(PLATFORM_VERSION, TestDataReader.getVersion());
    capabilities.setCapability(AUTOMATION_NAME, TestDataReader.getAutomationName());
    capabilities.setCapability(UDID, TestDataReader.getDeviceUDID());
    Optional.ofNullable(TestDataReader.getXcodeOrgId())
        .ifPresent(id -> capabilities.setCapability("xcodeOrgId", id));
    Optional.ofNullable(TestDataReader.getxcodeSigningId())
        .ifPresent(id -> capabilities.setCapability("xcodeSigningId", id));
    capabilities.setCapability(
        MobileCapabilityType.NEW_COMMAND_TIMEOUT, TestDataReader.getNewCommandTimeout());
    logger.info(String.valueOf(capabilities));
    return capabilities;
  }
}
