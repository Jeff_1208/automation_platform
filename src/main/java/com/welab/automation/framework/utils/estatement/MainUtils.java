package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.Ref;

import java.io.File;
import java.util.ArrayList;

public class MainUtils {

  private String result_file_path = "";
  private String fail_ref_path = "";
  private ArrayList<Ref> fail_refs = new ArrayList<>();

  public String get_result_file_path() {
    return result_file_path;
  }

  public static ArrayList<String> Spec_REF_list;
  public static ArrayList<String> Spec_FPS_account;

  public static void main(String[] args) {
    MainUtils m = new MainUtils();
    Estatement e = new Estatement();
    String folder_path = "";
    String path = "";

    folder_path = "D:/Project/e-Statement/all";
    folder_path = "D:/Project/e-Statement/estatement_pdf/2021_06_04_estatement";
    folder_path = "D:/Project/e-Statement/estatement_pdf/2021_10_07_estatement";
    path =
        "D:/Project/e-Statement/estatement_pdf/2021_09_07_estatement/8000019645_est_d6e5763e65881d9455816fc546f06ad2.pdf";
    path =
        "D:/Project/e-Statement/estatement_pdf/2021_10_07_estatement/8000024987_est_e3e611943b9f3f89509f3db77d1ea606.pdf";
    Spec_REF_list = Common.read_list_from_txt("/src/com/utils/Spec_REF.txt");
    Spec_FPS_account = Common.read_list_from_txt("/src/com/utils/Spec_FPS_account.txt");
    m.run_all_estatement(folder_path);
    //				m.estatement_test(path,e,Spec_REF_list);
  }

  @SuppressWarnings("static-access")
  public ArrayList<Estatement> run_all_estatement(String path) {
    fail_ref_path = path + Common.fail_refs_fileName;
    long start_time = System.currentTimeMillis();
    Spec_REF_list = Common.read_list_from_txt("/src/com/utils/Spec_REF.txt");
    Spec_FPS_account = Common.read_list_from_txt("/src/com/utils/Spec_FPS_account.txt");
    ArrayList<String> file_List = new ArrayList<>();
    ArrayList<Estatement> Estatement_List = new ArrayList<>();
    File file = new File(path);
    File[] fs = file.listFiles();
    for (File f : fs) {
      String file_name = f.getName();
      // if(file_name.endsWith(".pdf")&&f.getName().contains("800")) {
      if (file_name.endsWith(".pdf")) {
        file_List.add(f.getAbsolutePath().trim());
        Estatement e = new Estatement();
        // String customer_id = file_name.substring(0,10);
        if (f.getName().contains("800")) {
          String customer_id = get_customer_id(file_name);
          e.setCustomer_id(customer_id);
        } else {
          e.setCustomer_id(f.getName().trim());
        }
        e.setFile_name(file_name);
        e.setAbsolute_path(path);
        e.setAbsolute_filename(path + "/" + file_name);
        e = estatement_test(f.getAbsolutePath().trim(), e, Spec_REF_list);
        Estatement_List.add(e);
      }
    }

    get_results(Estatement_List, path); // 检查所有错误

    ResultUtils result_Utils = new ResultUtils(Spec_FPS_account);
    String filename = path + "/eStatement_result_" + Common.get_time_string() + ".xlsx";
    Estatement_List =
        result_Utils.Print_result_to_Excel(Estatement_List, path, fail_refs, filename);

    Generate_Picture_Report(); // 调用Python程序截图

    result_file_path = filename;
    long end_time = System.currentTimeMillis();
    long cost_time = end_time - start_time;
    System.out.println("cost time: " + cost_time);
    for (int i = 0; i < Estatement_List.size(); i++) {
      // System.out.println(Estatement_List.get(i).getCustomer_id()+":
      // "+Estatement_List.get(i).getComment());
    }
    return Estatement_List;
  }

  private void Generate_Picture_Report() {
    try {
      // Java_to_Python.run_python_2(fail_ref_path);
      JavaToPython.run_exe(fail_ref_path);
    } catch (Exception e) {
    }
  }

  public String get_customer_id(String filename) {
    if (filename.contains("800")) {
      int index = filename.indexOf("800");
      return filename.substring(index, index + 10);
    } else {
      return filename.substring(0, 10);
    }
  }

  public Estatement estatement_test(
      String filepath, Estatement estatement, ArrayList<String> Spec_REF_list) {
    PDF p = new PDF();
    String content = p.Get_PDF_Content(filepath);
    estatement = get_first_infomations(content, estatement);
    estatement = check_title_for_every_page(estatement, content);
    CoreAccountUtils core = new CoreAccountUtils(Spec_REF_list);
    estatement = core.get_core_account_transactions(content, estatement);
    GoSaveUtils goSave_Utils = new GoSaveUtils();
    estatement = goSave_Utils.get_go_save_exsit(content, estatement);
    estatement = goSave_Utils.get_go_save_transactions_history(content, estatement);

    LoansUtils loans_Utils = new LoansUtils();
    estatement = loans_Utils.get_loans_transactions(content, estatement);
    estatement = get_total_ammount(estatement);
    estatement = get_all_fail_ref(estatement);
    if (estatement.getAll_fail_Ref_list().size() > 0) {
      Ref file_ref_object = new Ref();
      file_ref_object.setCustomer_ID(estatement.getCustomer_id());
      file_ref_object.setFail_Ref_list(estatement.getAll_fail_Ref_list());
      file_ref_object.setFile_name(estatement.getFile_name());
      file_ref_object.setPath(estatement.getAbsolute_path());
      fail_refs.add(file_ref_object);
    }
    return estatement;
  }

  public Estatement get_all_fail_ref(Estatement e) {

    ArrayList<String> all_fail_ref_list = new ArrayList<>();

    if (e.getLoan_fail_trans_Ref_for_record() == null
        || e.getLoan_fail_trans_Ref_for_record().size() == 0) {
    } else {
      all_fail_ref_list.addAll(e.getLoan_fail_trans_Ref_for_record());
    }
    if (e.getFail_trans_Ref_list_for_desc() == null
        || e.getFail_trans_Ref_list_for_desc().size() == 0) {
    } else {
      all_fail_ref_list.addAll(e.getFail_trans_Ref_list_for_desc());
    }
    if (e.getFail_start_Ref_list() == null || e.getFail_start_Ref_list().size() == 0) {
    } else {
      all_fail_ref_list.addAll(e.getFail_start_Ref_list());
    }
    if (e.getWrong_single_amount_of_ref_list() == null
        || e.getWrong_single_amount_of_ref_list().size() == 0) {
    } else {
      all_fail_ref_list.addAll(e.getWrong_single_amount_of_ref_list());
    }
    if (e.getFail_trans_Ref_list_for_Invalid_trans() == null
        || e.getFail_trans_Ref_list_for_Invalid_trans().size() == 0) {
    } else {
      ArrayList<String> l = e.getFail_trans_Ref_list_for_Invalid_trans();
      if (!Spec_FPS_account.contains(e.getCustomer_id())) {
        if (l.size() > 10) {
          all_fail_ref_list.addAll(l.subList(0, 10));
        } else {
          all_fail_ref_list.addAll(e.getFail_trans_Ref_list_for_Invalid_trans());
        }
      }
    }
    e.setAll_fail_Ref_list(all_fail_ref_list);
    return e;
  }

  public String get_results(ArrayList<Estatement> Estatement_List, String path) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < Estatement_List.size(); i++) {
      Estatement e = Estatement_List.get(i);
      stringBuilder.append(Estatement_List.get(i).toResult());
      if ("-".equals(e.getLast_month_amount())) {
        stringBuilder.append(e.getCustomer_id() + ": Previous statement result : False" + "\n");
      }
      if (!e.isCore_account_result()) {
        stringBuilder.append(e.getCustomer_id() + ": Core Account result: False" + "\n");
      }
      if (!e.isGo_save_result()) {
        stringBuilder.append(e.getCustomer_id() + ": GoSave result: False" + "\n");
      }
      if (!e.isLoans_result()) {
        stringBuilder.append(e.getCustomer_id() + ": Loans result : False" + "\n");
      }
      if (!e.isIs_total_amount_equal()) {
        stringBuilder.append(e.getCustomer_id() + ": total amount result: False" + "\n");
      }
    }
    stringBuilder.append("\n\n");
    stringBuilder = print_customer_account_ID(Estatement_List, stringBuilder);
    String filepath = Common.save_result_to_txt(stringBuilder.toString(), path);
    return filepath;
  }

  public StringBuilder print_customer_account_ID(
      ArrayList<Estatement> Estatement_List, StringBuilder stringBuilder) {
    for (int i = 0; i < Estatement_List.size(); i++) {
      stringBuilder.append(
          Estatement_List.get(i).getCustomer_id()
              + "\t"
              + Estatement_List.get(i).getAccount_id()
              + "\n");
    }
    return stringBuilder;
  }

  public ArrayList<Estatement> run_test(String folder_path) {
    ArrayList<Estatement> estatement_List = run_all_estatement(folder_path);
    return estatement_List;
  }

  public Estatement check_title_for_every_page(Estatement e, String content) {
    boolean page_title_check = true;
    // Page 1 of 7
    String[] page_line_need_shuzu = {"Page", "of", e.getPage()};

    String[] content_list = content.split("\n");
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      if (Common.is_all_of_shuzu_item_in_line(line, page_line_need_shuzu)) {

        String p = Common.get_value_string_from_line(line, -1);
        String p2 = Common.get_value_string_from_line(line, -3);
        if (p2.contains(e.getPage())) {
          break;
        }

        if (p.contains(e.getPage())) {
          try {
            if (!(content_list[i + 1].contains(Global.YOUR_BANK_STATEMENT_STRING)
                && content_list[i + 1].contains(e.getWhich_month()))) {
              page_title_check = false;
              e.setIs_page_title_right(page_title_check);
              break;
            }
          } catch (ArrayIndexOutOfBoundsException e2) {
            // TODO: handle exception
            page_title_check = false;
            e.setIs_page_title_right(page_title_check);
            break;
          }

          if (!(content_list[i + 2].contains(Global.DATE_OF_ISSUE_STRING))) {
            page_title_check = false;
            e.setIs_page_title_right(page_title_check);
            break;
          }
          if (!(content_list[i + 3].contains(Global.ACCOUNT_NUMBER_STRING))) {
            page_title_check = false;
            e.setIs_page_title_right(page_title_check);
            break;
          }
          String[] temp_4 = {Global.BANK_CODE_STRING, Global.BRANCH_CODE_STRING, "390", "750"};
          if (!(Common.is_all_of_shuzu_item_in_line(content_list[i + 4], temp_4))) {
            page_title_check = false;
            e.setIs_page_title_right(page_title_check);
            break;
          }
        }
      }
    }
    e.setIs_page_title_right(page_title_check);
    return e;
  }

  public Estatement get_total_ammount(Estatement estatement) {
    double gosave_amount = Common.string_to_double(estatement.getGosave_total_calculate());
    double coreaccout_amount = Common.string_to_double(estatement.getCoreaccount_total_calculate());
    double total = gosave_amount + coreaccout_amount;
    estatement.setTotal_Amount_calculate(Common.double_to_String(Common.get_double_round(total)));
    if (estatement.getTotal_Amount_capture_title().contains("-")) {
      if (estatement.isIs_close_acount()) {
        estatement.setIs_total_amount_equal(true);
      } else {
        if (estatement.getTotal_Amount_capture_title().contains("-")
            && (Common.is_amount_zero(estatement.getTotal_Amount_calculate()))) {
          estatement.setIs_total_amount_equal(true);
        } else {
          estatement.setIs_total_amount_equal(false);
        }
      }
    } else if (Common.compare_amount_string_to_double(
        estatement.getTotal_Amount_calculate(), estatement.getTotal_Amount_capture_title())) {
      estatement.setIs_total_amount_equal(true);
    } else {
      estatement.setIs_total_amount_equal(false);
    }
    return estatement;
  }

  public Estatement get_first_infomations(String content, Estatement estatement) {
    // 取消賬戶 Account Closed
    if (content.contains(Global.ACCOUNT_CLOSED_STRING) && content.contains("Account Closed")) {
      estatement.setIs_close_acount(true);
    }

    String[] content_list = content.split("\n");
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      String[] first_info_want_find_shuzu_1 = {"(", ")", "202", Global.MONTHLY_STATEMENT_STRING};
      if (line.contains(Global.TRANSACTION_HISTORY_START_STRING)) {
        break;
      }

      // 賬戶號碼 Account Number : 1000963292
      if (line.contains(Global.ACCOUNT_NUMBER_FULL_STRING)) {
        estatement.setAccount_id(
            Common.get_string_value_from_string_with_start(line, "賬戶號碼 Account Number :"));
      }
      //			核心賬戶 Core Account 310.82
      if (line.contains(Global.CORE_ACCOUNT_STRING)) {
        String ammount_string =
            Common.get_string_value_from_string_with_start(line, "Core Account");
        estatement.setCoreaccount_total_capture_title(Common.string_move_special(ammount_string));
      }
      //			GoSave 6,500.00
      if (line.contains("GoSave")) {
        String ammount_string = Common.get_string_value_from_string_with_start(line, "GoSave");
        estatement.setGosave_total_capture_title(Common.string_move_special(ammount_string));
      }
      //			結存總額 Net Position 6,810.82
      if (line.contains(Global.TOTAL_AMOUNT_STRING)) {
        String ammount_string =
            Common.get_string_value_from_string_with_start(line, "Net Position");
        estatement.setTotal_Amount_capture_title(Common.string_move_special(ammount_string));
      }
      //			WeLab 私人貸款 WeLab Personal Loan 0.00	   更改为 GoFlexi
      if (line.contains(Global.PERSONAL_LOAN_STRING) || line.contains(Global.LOANS_STRING)) {
        if (line.contains(Global.PERSONAL_LOAN_STRING)) {
          String ammount_string =
              Common.get_string_value_from_string_with_start(line, "Personal Loan");
          estatement.setLaon_total_capture_title(Common.string_move_special(ammount_string));
        } else if (line.contains(Global.LOANS_STRING)) {
          String ammount_string =
              Common.get_string_value_from_string_with_start(line, Global.LOANS_STRING);
          estatement.setLaon_total_capture_title(Common.string_move_special(ammount_string));
        }
      }
      if (Common.is_all_of_shuzu_item_in_line(line, first_info_want_find_shuzu_1)
          && Common.is_one_of_shuzu_item_in_line(line, Common.month_list)) {
        String v = Common.get_string_value_from_string_with_start_and_end(line, "(", ")");
        estatement.setWhich_month(v);
        int YEAR = Integer.parseInt(Common.get_value_string_from_line(v, -1));
        String Month = Common.get_value_string_from_line(v, -2);
        if ("Jan".equals(Month)) {
          estatement.setLast_months_YEAR(YEAR - 1);
        } else {
          estatement.setLast_months_YEAR(YEAR);
        }
        estatement.setLast_Month(Common.get_last_month(Month));
        estatement.setNext_Month(Common.get_next_month(Month));
        estatement.setYEAR(YEAR);
        estatement.setMonth(Month);
      }
    }

    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      if (line.contains("Page 1 of")) {
        String p = Common.get_string_value_from_string_with_start(line, "Page 1 of");
        estatement.setPage(p);
        break;
      }
    }
    return estatement;
  }
}
