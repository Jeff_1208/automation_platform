package com.welab.automation.framework.utils.entity.api;

public final class RequestEntity {

    public static final String HOST = "host";
    public static final String PORT = "port";
}
