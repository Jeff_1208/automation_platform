

package com.welab.automation.framework.utils.api;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.JsonUtil;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.TimeUtils;
import com.welab.automation.framework.utils.entity.api.RequestHeader;
import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.welab.automation.framework.GlobalVar.HEADERS;
import static io.restassured.RestAssured.given;
import static org.apache.http.HttpHeaders.AUTHORIZATION;

public class SendRequest {
  private static Logger logger = LoggerFactory.getLogger(SendRequest.class);

  private RequestSpecification requestSpecification;

  @Step("and Given parameters When Sending Request")
  public Response sendRequest(
      String url, String method, String headers, String params, String bodyParam) throws Exception {
    SignatureUtil signatureUtil = new SignatureUtil();
    signatureUtil.getPublicKeyReq();
    int offset = signatureUtil.getOffset();
    signatureUtil.createSignReq(offset);

    String json = null;
    StringBuffer path = new StringBuffer();
    while (url.contains("${")) {
      String var = url.substring(url.indexOf("{") + 1, url.indexOf("}"));
      url = url.replace("${" + var + "}", GlobalVar.GLOBAL_VARIABLES.get(var));
    }
    String host = (null == TestDataReader.getHost()) ? "" : TestDataReader.getHost();
    if (null == TestDataReader.getPort() || TestDataReader.getPort().isEmpty())
      path.append(host + url);
    else path.append(host + ":" + TestDataReader.getPort() + url);

    logger.info("http url: {}", path.toString());

    if (method.toUpperCase().equals("PUT")
        || method.toUpperCase().equals("POST")
        || method.toUpperCase().equals("DELETE")) {
      json = FileUtil.readFileWithoutWrap(GlobalVar.REQUEST_JSON_FILE_PATH + bodyParam);
      json = checkParamsDependsOn(json);
      logger.info("http body: {}", json);
      /*
      if (params != null && json.equals("")) {
        json = null;
      }
       */
      signatureUtil.generateHeaderSignature(json, url, host, method.toLowerCase());
    }
    String timespan = TimeUtils.getTime();
    LinkedHashMap paramap = null;
    String newparamaps = "";
    if (params != null && !params.equals("")) {
      String[] entries = params.split("&");
      paramap = new LinkedHashMap();
      for (String s : entries) {
        if (s != null && !s.equals("")) {
          if (!s.contains("=")) {
            throw new Exception("please input valid params");
          } else if (s.contains("${")) {
            String var = s.substring(s.indexOf("{") + 1, s.indexOf("}"));
            s = s.replace("${" + var + "}", GlobalVar.GLOBAL_VARIABLES.get(var));
            String[] entry = s.split("=");
            paramap.put(entry[0], entry[1]);
          } else {
            String[] entry = s.split("=");
            String replaceValue = checkParamsDependsOn(entry[1]);
            paramap.put(entry[0], replaceValue);
          }
          newparamaps = newparamaps + s;
        }
      }
      path.append("?");
      path.append(newparamaps);
    }
    String sign = null; // SignUtilForSDC.createSDCSign(path.toString(), timespan);
    requestSpecification = given().relaxedHTTPSValidation().log().all();

    Optional.ofNullable(path.toString())
        .ifPresent(
            uri -> {
              requestSpecification.baseUri(uri);
            });
    logger.info("http method: {}", method);
    //    generateHeader(timespan, sign);
    if (null != TestDataReader.getOCPAPIMSubKey())
      HEADERS.put(RequestHeader.OCP_APIM_SUB_KEY, TestDataReader.getOCPAPIMSubKey());
    if (null != TestDataReader.getAuthorization())
      HEADERS.put(AUTHORIZATION, TestDataReader.getAuthorization());

    // some cases need specific headers, which is set in csv Headers with json file or json object
    // like
    // {"Accept-Language": "zh-hk"}
    if (!headers.isEmpty()) {
      updateHeaders(headers);
      logger.info("updated headers are" + HEADERS);
    }

    Optional.ofNullable(HEADERS)
        .ifPresent(
            header -> {
              requestSpecification.headers(header);
            });
    Optional.ofNullable(paramap)
        .ifPresent(
            paramMap -> {
              requestSpecification.params(paramMap);
            });

    Optional.ofNullable(json)
        .ifPresent(
            body -> {
              requestSpecification.body(body);
            });
    requestSpecification.contentType(ContentType.JSON);

    Response resp;
    try {
      if (url.contains("/v1/loan-application")) {
          if (url.endsWith("agreements")  || url.endsWith("schedule")) {
          Thread.sleep(10 * 1000);
        }
      }
      resp = send(method, requestSpecification);
      if ( url.equals("/v1/gs") ) {
        if (Objects.equals(resp.jsonPath().getString("data[0].joined"), "false")) {
          Matchers.sendGetPotIdRequest(resp.jsonPath().getString("data[0].id"));
        }
        if (Objects.equals(resp.jsonPath().getString("data[0].joined"), "true")) {
          GlobalVar.GLOBAL_VARIABLES.put("data.id[0]",resp.jsonPath().getString("data.id[0]"));
        }
      }
    } finally {
      if (!headers.isEmpty()) {
        // restore headers
        HEADERS.clear();
        new BaseRunner().generateHeaderForWealth();
      }
    }
    return resp;
  }

  @Step("and Given parameters When Sending Request")
  public Response sendRequestLoans(
          String url, String method, String headers, String params, String bodyParam) throws Exception {
    SignatureUtil signatureUtil = new SignatureUtil();
    String getLoansSake2Token = "/auth/realms/welab-loan-operations-portal/protocol/openid-connect/token";
    String loansSake2Url = "/api/v1/loan-applications";
    String json = null;
    StringBuffer path = new StringBuffer();
    System.out.println("url: "+url);
    while (url.contains("${")) {
      String var = url.substring(url.indexOf("{") + 1, url.indexOf("}"));
      url = url.replace("${" + var + "}", String.valueOf(GlobalVar.GLOBAL_VARIABLES.get(var)));
    }
    String host = null;
    if (url.equals(getLoansSake2Token)) {
       host = "https://keycloak.sta-wlab.net";
    }else if(url.contains(loansSake2Url)) {
       host = "https://ploan-portal.sta-wlab.net";
    }else{
       host = (null == TestDataReader.getHost()) ? "" : TestDataReader.getHost();
    }

    if (null == TestDataReader.getPort() || TestDataReader.getPort().isEmpty())
      path.append(host + url);
    else path.append(host + ":" + TestDataReader.getPort() + url);

    logger.info("http url: {}", path.toString());

    if (method.toUpperCase().equals("PUT")
            || method.toUpperCase().equals("POST")
            || method.toUpperCase().equals("DELETE")
            || method.toUpperCase().equals("PATCH")) {
      json = FileUtil.readFileWithoutWrap(GlobalVar.LOANS_REQUEST_JSON_FILE_PATH + bodyParam);
      json = checkParamsDependsOn(json);
      logger.info("http body: {}", json);
    }
    String timespan = TimeUtils.getTime();
    LinkedHashMap paramap = null;
    String newparamaps = "";
    if (params != null && !params.equals("")) {
      String[] entries = params.split("&");
      paramap = new LinkedHashMap();
      for (String s : entries) {
        if (s != null && !s.equals("")) {
          if (!s.contains("=")) {
            throw new Exception("please input valid params");
          } else if (s.contains("${")) {
            String var = s.substring(s.indexOf("{") + 1, s.indexOf("}"));
            s = s.replace("${" + var + "}", GlobalVar.GLOBAL_VARIABLES.get(var));
            String[] entry = s.split("=");
            paramap.put(entry[0], entry[1]);
          } else {
            String[] entry = s.split("=");
            String replaceValue = checkParamsDependsOn(entry[1]);
            paramap.put(entry[0], replaceValue);
          }
          newparamaps = newparamaps + s;
        }
      }
      path.append("?");
      path.append(newparamaps);
    }
    String sign = null; // SignUtilForSDC.createSDCSign(path.toString(), timespan);
    requestSpecification = given().relaxedHTTPSValidation().log().all();
    Optional.ofNullable(path.toString())
            .ifPresent(
                    uri -> {
                      requestSpecification.baseUri(uri);
                    });
    logger.info("http method: {}", method);

    if (url.contains("/loan-applications") && host.equals("https://ploan-portal.sta-wlab.net")) {
      String loanSake2Token = "Bearer " + GlobalVar.GLOBAL_VARIABLES.get("access_token");
      HEADERS.put("Authorization",loanSake2Token);
    }

    if (!headers.isEmpty()) {
      updateHeaders(headers);
      logger.info("updated headers are" + HEADERS);
    }
    Optional.ofNullable(HEADERS)
            .ifPresent(
                    header -> {
                      requestSpecification.headers(header);
                    });
    Optional.ofNullable(paramap)
            .ifPresent(
                    paramMap -> {
                      requestSpecification.params(paramMap);
                    });

    if (url.equals(getLoansSake2Token)) {
      requestSpecification.contentType("application/x-www-form-urlencoded;charset=UTF-8");
      json = null;
    }else {
      requestSpecification.contentType(ContentType.JSON);
    }
    Optional.ofNullable(json)
            .ifPresent(
                    body -> {
                      requestSpecification.body(body);
                    });

    Response resp = null;
    try {
      if(path.toString().endsWith("offers")){
        for (int i = 0; i < 20; i++) {
          resp = send(method, requestSpecification);
          JsonPath jp = resp.jsonPath();
          String data=jp.get("data")+"";
          if(data.length()>100){
            break;
          }
          Thread.sleep(2000);
        }
      }else{
        resp = send(method, requestSpecification);
      }
    } finally {
      if (!headers.isEmpty()) {
        // restore headers
        HEADERS.clear();
        new BaseRunner().generateHeaderForWealth();
      }
    }
    return resp;
  }

  public String checkParamsDependsOn(String params) {
    String replaceValue = null;
    if (params.startsWith("$")) {
      return replaceValue;
    } else if (params.contains("$")) {
      replaceValue = new JsonUtil(params).checkReplace(params);
      return replaceValue;
    }
    return params;
  }

  @Step("Given the header using host,path, path parameter")
  private void generateHeader(String timespan, String signature) {
    Optional.ofNullable(TestDataReader.getMAC())
        .ifPresent(mac -> HEADERS.put(RequestHeader.MAC, mac));
    Optional.ofNullable(TestDataReader.getExpired())
        .ifPresent(expired -> HEADERS.put(RequestHeader.EXPIRED, expired));
    Optional.ofNullable(TestDataReader.getClientId())
        .ifPresent(clientId -> HEADERS.put(RequestHeader.CLIENT_ID, clientId));
    Optional.ofNullable(TestDataReader.getClientSecret())
        .ifPresent(clientsecret -> HEADERS.put(RequestHeader.CLIENT_SECRET, clientsecret));
    Optional.ofNullable(TestDataReader.getLanguageCode())
        .ifPresent(lang -> HEADERS.put(RequestHeader.LANGUAGE_CODE, "en-US"));
    Optional.ofNullable(timespan).ifPresent(time -> HEADERS.put(RequestHeader.TIME_SPAN, time));
    Allure.addAttachment("request headers", GlobalVar.HEADERS.toString());
  }

  /** @param headers update Global HEADERS by header configuration by json string or json file. */
  @Step("Update headers by header config")
  private void updateHeaders(String headers) {
    String json;
    if (headers.endsWith(".json")) {
      json = FileUtil.readFileWithoutWrap(GlobalVar.REQUEST_JSON_FILE_PATH + headers);
    } else {
      json = headers;
    }

    Map<String, Object> headerObject = JsonUtil.jsonObjectToMap(json);
    if (headerObject != null) {
      Set<String> headerKeySet = headerObject.keySet();
      for (String key : headerKeySet) {
        String value = (String) headerObject.get(key);
        // not need to replace Global header, because it already exists,
        if (!value.startsWith("$")) {
          HEADERS.put(key, value);
          if (null  != HEADERS.get("Authorization") && HEADERS.get("Authorization").equals("without")) {
            HEADERS.remove("Authorization");
          }
        }
      }
    }
  }

  @Step("Update headers by header config")
  private void updateHeadersLoans(String headers)  throws Exception {

    headers = headers.trim();
    if(headers.startsWith("{")&&headers.endsWith("}")){
      headers=headers.substring(1,headers.length()-1);
    }
    if(headers.contains(",")){
      String[] parmas = headers.split(",");
      for (String s : parmas) {
        setParams(s);
      }
    }else{
      setParams(headers);
    }
  }

  public void setParams(String s) throws Exception{
    if (s != null && !s.equals("")) {
      if (!s.contains("=")) {
        throw new Exception("please input valid params");
      } else if (s.contains("${")) {
        s = UpdateParams(s);
        String[] entry = s.split("=");
        HEADERS.put(entry[0], entry[1]);
      } else {
        String[] entry = s.split("=");
        HEADERS.put(entry[0], entry[1]);
      }
    }
  }

  public String UpdateParams(String s){
    String var = s.substring(s.indexOf("{") + 1, s.indexOf("}"));
    s = s.replace("${" + var + "}", GlobalVar.GLOBAL_VARIABLES.get(var));
    return s;
  }


  @Step("show response message")
  public Response send(String method, RequestSpecification specification) {
    Response response = null;
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    switch (method.toUpperCase()) {
      case "POST":
        response = specification.post();
        break;
      case "GET":
        response = specification.get();
        break;
      case "PUT":
        response = specification.put();
        break;
      case "DELETE":
        response = specification.delete();
        break;
      case "PATCH":
        response = specification.patch();
        break;
      default:
        {
          logger.error("Please define http method in csv file");
          throw new IllegalStateException("Please define http method in csv file");
        }
    }
    logger.info("Http Code: {}", response.statusCode());
    stopWatch.stop();
    logger.info("Time Cost(ms): {}", stopWatch.getTime());
    if (response.statusCode() != 200 && response.statusCode() != 201)
      logger.trace("Response: {}", response.prettyPrint());
    logger.info("Response: {}", response.prettyPrint());
    return response;
  }

  public String escapeExprSpecialWord(String keyword) {
    if (StringUtils.isNotBlank(keyword)) {
      String[] fbsArr = {"\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|"};
      for (String key : fbsArr) {
        if (keyword.contains(key)) {
          keyword = keyword.replace(key, "\\" + key);
        }
      }
    }
    return keyword;
  }
}
