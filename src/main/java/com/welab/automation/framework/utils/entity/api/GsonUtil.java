package com.welab.automation.framework.utils.entity.api;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;

public class GsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(GsonUtil.class);
    public static Gson gson;
    private static GsonBuilder gsonBuilder;


    /**
     * override registerTypeAdapter destination is used to avoid
     * when obj convert to map, it will convert int type to number type, such as 0 is converted to 0.0
     *
     */
    static {
        constructBuilder();
        initGsonIgnoreNull();
    }

    private static GsonBuilder constructBuilder() {
        gsonBuilder = new GsonBuilder()
                .disableHtmlEscaping()
                .registerTypeAdapter(
                        new TypeToken<Map<String, Object>>() {
                        }.getType(),
                        new JsonDeserializer<Map<String, Object>>() {
                            @Override
                            public Map<String, Object> deserialize(
                                    JsonElement json, Type typeOfT,
                                    JsonDeserializationContext context) throws JsonParseException {
                                Boolean nullValueSwitch = ConfigUtils.getNULLValueSwitch();
                                Map<String, Object> map = new HashMap<String, Object>();
                                JsonObject jsonObject = json.getAsJsonObject();
                                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
                                for (Map.Entry<String, JsonElement> entry : entrySet) {
                                    String key = entry.getKey();
                                    JsonElement entryValue = entry.getValue();
                                    if (entryValue.isJsonNull()) {
                                        if (nullValueSwitch == true) {
                                            map.put(key, null);
                                        }
                                        continue;
                                    } else if (entryValue.isJsonArray()) {
                                        JsonArray asJsonArray = entry.getValue().getAsJsonArray();
                                        Iterator<JsonElement> iterator = asJsonArray.iterator();
                                        List list = new ArrayList();
                                        while (iterator.hasNext()) {
                                            JsonElement jsonElement = iterator.next();
                                            JsonPrimitive asJsonPrimitive = jsonElement.getAsJsonPrimitive();
                                            if (asJsonPrimitive.isString()) {
                                                list.add(asJsonArray.toString());
                                            } else if (asJsonPrimitive.isNumber()) {
                                                list.add(asJsonPrimitive.getAsNumber());
                                            } else if (asJsonPrimitive.isBoolean()) {
                                                list.add(asJsonPrimitive.getAsBoolean());
                                            }
                                        }
                                        map.put(key, list);
                                    } else if (entryValue.isJsonPrimitive()) {
                                        JsonPrimitive jsonPrimitive = entry.getValue().getAsJsonPrimitive();
                                        /**
                                         * converting JsonPrimitive to primitive
                                         */

                                        if (jsonPrimitive.isBoolean()) {
                                            map.put(key, jsonPrimitive.getAsBoolean());
                                        } else if (jsonPrimitive.isNumber()) {
                                            map.put(key, jsonPrimitive.getAsNumber());
                                        } else if (jsonPrimitive.isString()) {
                                            map.put(key, jsonPrimitive.getAsString());
                                        } else if (jsonPrimitive.isJsonArray()) {
                                            map.put(key, jsonPrimitive.getAsJsonArray());
                                        } else if (jsonPrimitive.isJsonNull()) {
                                            if (nullValueSwitch == true) {
                                                map.put(key, null);
                                            }
                                        } else {
                                            map.put(key, entryValue.getAsString());
                                        }
                                    } else {
                                        JsonObject asJsonObject = entry.getValue().getAsJsonObject();
                                        map.put(key, entryValue.getAsString());
                                    }
                                }
                                return map;
                            }
                        })
                .enableComplexMapKeySerialization().setPrettyPrinting();
        return gsonBuilder;
    }

    public static void initGsonIgnoreNull() {
        gson = GsonUtil.constructBuilder().create();

    }

    public static void initGsonContainNull() {
        gson = GsonUtil.gsonBuilder.serializeNulls().create();
    }


    public static Map<String, Object> object2map(Object obj) {
        String s1 = gson.toJson(obj);
        return json2map(s1);
    }

    /**
     * convert map to object
     *
     * @param map
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T map2Object(Map map, Class<T> tClass) {
        String toJson = gson.toJson(map);
        return gson.fromJson(toJson, tClass);
    }

    /**
     * Convert the json format to map format, and you can get the content through map.get("id")
     *
     * @param json
     * @return
     */
    public static Map<String, Object> json2map(String json) {
        try {
            return gson.fromJson(json, new TypeToken<Map<String, Object>>() {
            }.getType());
        } catch (Exception exception) {
            return null;
        }
    }


    public static String toJson(Object object) {
        String json = gson.toJson(object);
        return json;
    }

    /**
     * extract json data from json file
     *
     * @param file (including path)
     * @return JsonElement
     * @throws Exception
     */
    public static JsonObject extractDataJson(String file) throws Exception {
        JsonParser jsonParser = new JsonParser();
        return (JsonObject) jsonParser.parse(new FileReader(file));
    }

}
