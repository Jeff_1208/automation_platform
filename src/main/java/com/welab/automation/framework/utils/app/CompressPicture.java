package com.welab.automation.framework.utils.app;

import java.io.File;
import java.io.FileOutputStream;
import com.welab.automation.framework.GlobalVar;
import net.coobird.thumbnailator.Thumbnails;

public class CompressPicture {

	public static long getFileSize(String filename) {
		File file = new File(filename);
		if (!file.exists() || !file.isFile()) {
			System.out.println("文件不存在");
			return -1;
		}
		return file.length();
	}

	public  void compressPic(String filePath,String descFileDIr,double compressPer) throws Exception{
		String filename ="";
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			filename =filePath.substring(filePath.lastIndexOf("\\")+1);
		} else {
			filename =filePath.substring(filePath.lastIndexOf("/")+1);
		}
		System.out.println("filename: "+filename);
		Thumbnails.of(new File(filePath))
				.scale(compressPer) //图片大小（长宽）压缩 从0按照
				.outputQuality(1f) //图片质量压缩比例 从0-1，越接近1质量越好
				.toOutputStream(new FileOutputStream(descFileDIr+filename));
	}


	public String changeByDir(String dirPath, double compressPercent){
		File dir = new File(dirPath);
		if (!dir.isDirectory()) {
			System.err.println("is not a dir");
			return "";
		}
		File[] fileList = dir.listFiles();
		String outputFileDir = dir.getAbsolutePath() + "/out/";
		File outputFile = new File(outputFileDir);
		if (!outputFile.exists() && !outputFile.isDirectory()) {
			outputFile.mkdir();
		}
		for (int i = 0; i <fileList.length;i++) {
			File file = fileList[i];
			if(!isStringInCaseList(file.getName())){
				continue;
			}
			if (file.getName().endsWith(".png")||file.getName().endsWith(".jpg")) {
				try{
					compressPic(file.getAbsolutePath(),outputFileDir,compressPercent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return outputFileDir;
	}

	public boolean isStringInCaseList(String data){
		boolean result = false;
		if(GlobalVar.CHANNEL_PASS_CASE_NAME.size()==0) {
			return false;
		}
		for (int i = 0; i < GlobalVar.CHANNEL_PASS_CASE_NAME.size(); i++) {
			String caseName=GlobalVar.CHANNEL_PASS_CASE_NAME.get(i);
			if(data.toUpperCase().contains(caseName.toUpperCase())){
				result=true;
				break;
			}
		}
		return result;
	}

}
