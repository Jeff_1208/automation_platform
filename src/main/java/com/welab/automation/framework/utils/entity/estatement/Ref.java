package com.welab.automation.framework.utils.entity.estatement;

import java.util.ArrayList;

public class Ref {
	private int y;
	private int page;
	private String value;
	private String customer_ID;
	private String file_name;
	private String path;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	private ArrayList<String> Fail_Ref_list;
	
	public String getCustomer_ID() {
		return customer_ID;
	}
	public void setCustomer_ID(String customer_ID) {
		this.customer_ID = customer_ID;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public ArrayList<String> getFail_Ref_list() {
		return Fail_Ref_list;
	}
	public void setFail_Ref_list(ArrayList<String> fail_Ref_list) {
		Fail_Ref_list = fail_Ref_list;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
