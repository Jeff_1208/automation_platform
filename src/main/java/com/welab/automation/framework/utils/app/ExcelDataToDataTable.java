package com.welab.automation.framework.utils.app;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import io.cucumber.cucumberexpressions.Transformer;
import io.cucumber.datatable.DataTable;
import lombok.Getter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.welab.automation.framework.GlobalVar;


public class ExcelDataToDataTable implements Transformer<DataTable> {

	@Getter
	private final DataTable datatable;

	public ExcelDataToDataTable(String file, String sheet) {
		Path filePath = Paths.get(GlobalVar.APP_TEST_DATA_PATH,file);
		String absolutePath = filePath + "#" + sheet;
		this.datatable = transform(absolutePath);
	}

	@Override
	public DataTable transform(String filePath) {
		String paras[] = filePath.split("#");
		ExcelReader reader = new ExcelReader.ExcelReaderBuilder()
				.setFileLocation(paras[0])
				.setSheet(paras[1])
				.build();
		
		List<List<String>> excelData = getExcelData(reader);
		DataTable table = getDataTable(excelData);
		
		return table;
	}

	private DataTable getDataTable(List<List<String>> dataTableRows) {
		DataTable table = DataTable.create(dataTableRows);
		return table;
	}

	private List<List<String>> getExcelData(ExcelReader reader) {
		List<List<String>> excelData ;
		
		try {
			excelData = reader.getSheetDataAt();
		} catch (IOException | InvalidFormatException e) {
			throw new RuntimeException(e.getMessage());
		}
		return excelData;
	}

}
