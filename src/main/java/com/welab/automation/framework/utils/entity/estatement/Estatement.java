package com.welab.automation.framework.utils.entity.estatement;

import java.util.ArrayList;
import java.util.HashMap;

public class Estatement {

	private String Comment;
	private boolean result;
	private int YEAR;
	private int Last_months_YEAR;
	private double Interest;
	private double Interest_capture;
	private double Interest_diff;
	private String content;
	private String Page;
	private String TestCaseID;  
	private String Month;
	private String Last_Month;
	private String Next_Month;
	private String Total_Amount_calculate;
	private String Total_Amount_capture_title;
	private String File_name;
	private String absolute_path;
	private String absolute_filename;
	private String Account_id;
	private String Customer_id;
	private String last_month_amount;
	private String gosave_total_calculate;
	private String gosave_total_capture;
	private String gosave_total_capture_title;
	private String coreaccount_total_calculate;
	private String coreaccount_total_capture;
	private String coreaccount_total_capture_title;
	private String loan_total_from_bank;
	private String laon_total_calculate;
	private String laon_total_capture_title;
	private String which_month;
	private boolean is_check_interest=true;
	private boolean is_have_gosave_record;
	private boolean is_have_gosave_trans;
	private boolean is_have_loan_record;
	private boolean is_total_amount_equal;
	private boolean core_account_result;
	private boolean go_save_result;
	private boolean loans_result;
	private boolean is_account_close=false;
	private boolean is_have_next_month_tran=true;
	private boolean is_new_page_important_notes=false;
	private boolean is_page_title_right=false;
	private boolean is_have_importan_notes=false;
	private boolean is_right_loan_trans_record=false;
	private boolean is_close_acount=false;

	private ArrayList<GoSave> goSave_trans_history;
	private ArrayList<Loans> Loans_trans_history;
	private ArrayList<Transactions> transations_history;
	
	private HashMap<String, String> current_gosave_exist;
	private ArrayList<String> Fail_trans_Ref_list_for_desc;
	private ArrayList<String> Fail_trans_Ref_list_for_Invalid_trans;
	private ArrayList<String> goSave_Amount_list_in_coreaccount_trans_history;
	private ArrayList<String> GoSave_fail_trans_Ref_from_coreaccount_list;
	private ArrayList<String> GoSave_fail_trans_Ref_for_amount_single;
	private ArrayList<String> loan_fail_trans_Ref_for_amount_single;
	private ArrayList<String> loan_fail_trans_Ref_for_record;
	private ArrayList<String> fail_start_Ref_list;
	private ArrayList<String> wrong_single_amount_of_ref_list;
	private ArrayList<String> wrong_descrition_start_for_pay_and_receive_money_of_ref_list;
	private ArrayList<String> all_fail_Ref_list;

	public String getComment() {
		return Comment;
	}
	public void setComment(String comment) {
		Comment = comment;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public double getInterest() {
		return Interest;
	}
	public void setInterest(double interest) {
		Interest = interest;
	}
	public double getInterest_diff() {
		return Interest_diff;
	}
	public void setInterest_diff(double interest_diff) {
		Interest_diff = interest_diff;
	}
	public double getInterest_capture() {
		return Interest_capture;
	}
	public void setInterest_capture(double interest_capture) {
		Interest_capture = interest_capture;
	}
	public boolean isIs_have_next_month_tran() {
		return is_have_next_month_tran;
	}
	public void setIs_have_next_month_tran(boolean is_have_next_month_tran) {
		this.is_have_next_month_tran = is_have_next_month_tran;
	}
	public boolean isIs_check_interest() {
		return is_check_interest;
	}
	public void setIs_check_interest(boolean is_check_interest) {
		this.is_check_interest = is_check_interest;
	}
	public String getNext_Month() {
		return Next_Month;
	}
	public void setNext_Month(String next_Month) {
		Next_Month = next_Month;
	}
	public String getAbsolute_path() {
		return absolute_path;
	}
	public void setAbsolute_path(String absolute_path) {
		this.absolute_path = absolute_path;
	}
	public String getAbsolute_filename() {
		return absolute_filename;
	}
	public void setAbsolute_filename(String absolute_filename) {
		this.absolute_filename = absolute_filename;
	}
	public ArrayList<String> getFail_trans_Ref_list_for_Invalid_trans() {
		return Fail_trans_Ref_list_for_Invalid_trans;
	}
	public void setFail_trans_Ref_list_for_Invalid_trans(ArrayList<String> fail_trans_Ref_list_for_Invalid_trans) {
		Fail_trans_Ref_list_for_Invalid_trans = fail_trans_Ref_list_for_Invalid_trans;
	}
	public ArrayList<Loans> getLoans_trans_history() {
		return Loans_trans_history;
	}
	public void setLoans_trans_history(ArrayList<Loans> loans_trans_history) {
		Loans_trans_history = loans_trans_history;
	}
	public String getLaon_total_calculate() {
		return laon_total_calculate;
	}
	public boolean isIs_close_acount() {
		return is_close_acount;
	}
	public void setIs_close_acount(boolean is_close_acount) {
		this.is_close_acount = is_close_acount;
	}
	public void setLaon_total_calculate(String laon_total_calculate) {
		this.laon_total_calculate = laon_total_calculate;
	}
	public ArrayList<String> getLoan_fail_trans_Ref_for_record() {
		return loan_fail_trans_Ref_for_record;
	}
	public void setLoan_fail_trans_Ref_for_record(ArrayList<String> loan_fail_trans_Ref_for_record) {
		this.loan_fail_trans_Ref_for_record = loan_fail_trans_Ref_for_record;
	}
	public ArrayList<String> getLoan_fail_trans_Ref_for_amount_single() {
		return loan_fail_trans_Ref_for_amount_single;
	}
	public void setLoan_fail_trans_Ref_for_amount_single(ArrayList<String> loan_fail_trans_Ref_for_amount_single) {
		this.loan_fail_trans_Ref_for_amount_single = loan_fail_trans_Ref_for_amount_single;
	}
	public ArrayList<String> getGoSave_fail_trans_Ref_for_amount_single() {
		return GoSave_fail_trans_Ref_for_amount_single;
	}
	public void setGoSave_fail_trans_Ref_for_amount_single(ArrayList<String> goSave_fail_trans_Ref_for_amount_single) {
		GoSave_fail_trans_Ref_for_amount_single = goSave_fail_trans_Ref_for_amount_single;
	}
	public ArrayList<String> getWrong_single_amount_of_ref_list() {
		return wrong_single_amount_of_ref_list;
	}
	public void setWrong_single_amount_of_ref_list(ArrayList<String> wrong_single_amount_of_ref_list) {
		this.wrong_single_amount_of_ref_list = wrong_single_amount_of_ref_list;
	}

	public ArrayList<String> getWrong_descrition_start_for_pay_and_receive_money_of_ref_list() {
		return wrong_descrition_start_for_pay_and_receive_money_of_ref_list;
	}
	public void setWrong_descrition_start_for_pay_and_receive_money_of_ref_list(
			ArrayList<String> wrong_descrition_start_for_pay_and_receive_money_of_ref_list) {
		this.wrong_descrition_start_for_pay_and_receive_money_of_ref_list = wrong_descrition_start_for_pay_and_receive_money_of_ref_list;
	}
	public boolean isIs_have_importan_notes() {
		return is_have_importan_notes;
	}
	public void setIs_have_importan_notes(boolean is_have_importan_notes) {
		this.is_have_importan_notes = is_have_importan_notes;
	}
	public boolean isIs_page_title_right() {
		return is_page_title_right;
	}
	public void setIs_page_title_right(boolean is_page_title_right) {
		this.is_page_title_right = is_page_title_right;
	}
	public boolean isIs_new_page_important_notes() {
		return is_new_page_important_notes;
	}
	public void setIs_new_page_important_notes(boolean is_new_page_important_notes) {
		this.is_new_page_important_notes = is_new_page_important_notes;
	}
	public boolean isIs_right_loan_trans_record() {
		return is_right_loan_trans_record;
	}
	public void setIs_right_loan_trans_record(boolean is_right_loan_trans_record) {
		this.is_right_loan_trans_record = is_right_loan_trans_record;
	}
	public ArrayList<String> getFail_start_Ref_list() {
		return fail_start_Ref_list;
	}
	public void setFail_start_Ref_list(ArrayList<String> fail_start_Ref_list) {
		this.fail_start_Ref_list = fail_start_Ref_list;
	}
	public ArrayList<String> getGoSave_fail_trans_Ref_from_coreaccount_list() {
		return GoSave_fail_trans_Ref_from_coreaccount_list;
	}
	public void setGoSave_fail_trans_Ref_from_coreaccount_list(
			ArrayList<String> goSave_fail_trans_Ref_from_coreaccount_list) {
		GoSave_fail_trans_Ref_from_coreaccount_list = goSave_fail_trans_Ref_from_coreaccount_list;
	}
	public ArrayList<String> getGoSave_Amount_list_in_coreaccount_trans_history() {
		return goSave_Amount_list_in_coreaccount_trans_history;
	}
	public void setGoSave_Amount_list_in_coreaccount_trans_history(
			ArrayList<String> goSave_Amount_list_in_coreaccount_trans_history) {
		this.goSave_Amount_list_in_coreaccount_trans_history = goSave_Amount_list_in_coreaccount_trans_history;
	}
	public ArrayList<Transactions> getTransations_history() {
		return transations_history;
	}
	public void setTransations_history(ArrayList<Transactions> transations_history) {
		this.transations_history = transations_history;
	}
	public ArrayList<String> getFail_trans_Ref_list_for_desc() {
		return Fail_trans_Ref_list_for_desc;
	}
	public void setFail_trans_Ref_list_for_desc(ArrayList<String> fail_trans_Ref_list_for_desc) {
		Fail_trans_Ref_list_for_desc = fail_trans_Ref_list_for_desc;
	}
	public boolean isIs_account_close() {
		return is_account_close;
	}
	public void setIs_account_close(boolean is_account_close) {
		this.is_account_close = is_account_close;
	}
	public ArrayList<GoSave> getGoSave_trans_history() {
		return goSave_trans_history;
	}
	public void setGoSave_trans_history(ArrayList<GoSave> goSave_trans_history) {
		this.goSave_trans_history = goSave_trans_history;
	}
	
	public String getTestCaseID() {
		return TestCaseID;
	}
	public void setTestCaseID(String testCaseID) {
		TestCaseID = testCaseID;
	}
	
	public boolean isIs_have_gosave_trans() {
		return is_have_gosave_trans;
	}
	public void setIs_have_gosave_trans(boolean is_have_gosave_trans) {
		this.is_have_gosave_trans = is_have_gosave_trans;
	}

	public boolean isIs_total_amount_equal() {
		return is_total_amount_equal;
	}
	public void setIs_total_amount_equal(boolean is_total_amount_equal) {
		this.is_total_amount_equal = is_total_amount_equal;
	}
	public String getWhich_month() {
		return which_month;
	}
	public void setWhich_month(String which_month) {
		this.which_month = which_month;
	}
	public boolean isLoans_result() {
		return loans_result;
	}
	public void setLoans_result(boolean loans_result) {
		this.loans_result = loans_result;
	}
	public boolean isIs_have_loan_record() {
		return is_have_loan_record;
	}
	public void setIs_have_loan_record(boolean is_have_loan_record) {
		this.is_have_loan_record = is_have_loan_record;
	}
	public int getLast_months_YEAR() {
		return Last_months_YEAR;
	}
	public void setLast_months_YEAR(int last_months_YEAR) {
		Last_months_YEAR = last_months_YEAR;
	}
	public HashMap<String, String> getCurrent_gosave_exist() {
		return current_gosave_exist;
	}
	public void setCurrent_gosave_exist(HashMap<String, String> current_gosave_exist) {
		this.current_gosave_exist = current_gosave_exist;
	}
	public boolean isGo_save_result() {
		return go_save_result;
	}
	public void setGo_save_result(boolean go_save_result) {
		this.go_save_result = go_save_result;
	}
	public String getLast_Month() {
		return Last_Month;
	}
	public void setLast_Month(String last_Month) {
		Last_Month = last_Month;
	}
	public String getGosave_total_capture_title() {
		return gosave_total_capture_title;
	}
	public void setGosave_total_capture_title(String gosave_total_capture_title) {
		this.gosave_total_capture_title = gosave_total_capture_title;
	}
	public String getCoreaccount_total_capture_title() {
		return coreaccount_total_capture_title;
	}
	public void setCoreaccount_total_capture_title(String coreaccount_total_capture_title) {
		this.coreaccount_total_capture_title = coreaccount_total_capture_title;
	}
	public String getLaon_total_capture_title() {
		return laon_total_capture_title;
	}
	public void setLaon_total_capture_title(String laon_total_capture_title) {
		this.laon_total_capture_title = laon_total_capture_title;
	}
	public int getYEAR() {
		return YEAR;
	}
	public void setYEAR(int yEAR) {
		YEAR = yEAR;
	}
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public String getTotal_Amount_calculate() {
		return Total_Amount_calculate;
	}
	public void setTotal_Amount_calculate(String total_Amount_calculate) {
		Total_Amount_calculate = total_Amount_calculate;
	}

	public String getTotal_Amount_capture_title() {
		return Total_Amount_capture_title;
	}
	public void setTotal_Amount_capture_title(String total_Amount_capture_title) {
		Total_Amount_capture_title = total_Amount_capture_title;
	}
	public String getFile_name() {
		return File_name;
	}
	public void setFile_name(String file_name) {
		File_name = file_name;
	}
	public String getAccount_id() {
		return Account_id;
	}
	public void setAccount_id(String account_id) {
		Account_id = account_id;
	}
	public String getCustomer_id() {
		return Customer_id;
	}
	public void setCustomer_id(String customer_id) {
		Customer_id = customer_id;
	}
	public String getLast_month_amount() {
		return last_month_amount;
	}
	public void setLast_month_amount(String last_month_amount) {
		this.last_month_amount = last_month_amount;
	}
	public String getGosave_total_calculate() {
		return gosave_total_calculate;
	}
	public void setGosave_total_calculate(String gosave_total_calculate) {
		this.gosave_total_calculate = gosave_total_calculate;
	}
	public String getGosave_total_capture() {
		return gosave_total_capture;
	}
	public void setGosave_total_capture(String gosave_total_capture) {
		this.gosave_total_capture = gosave_total_capture;
	}
	public String getCoreaccount_total_calculate() {
		return coreaccount_total_calculate;
	}
	public void setCoreaccount_total_calculate(String coreaccount_total_calculate) {
		this.coreaccount_total_calculate = coreaccount_total_calculate;
	}
	public String getCoreaccount_total_capture() {
		return coreaccount_total_capture;
	}
	public void setCoreaccount_total_capture(String coreaccount_total_capture) {
		this.coreaccount_total_capture = coreaccount_total_capture;
	}
	public String getLoan_total_from_bank() {
		return loan_total_from_bank;
	}
	public void setLoan_total_from_bank(String loan_total_from_bank) {
		this.loan_total_from_bank = loan_total_from_bank;
	}
	public boolean isIs_have_gosave_record() {
		return is_have_gosave_record;
	}
	public void setIs_have_gosave_record(boolean is_have_gosave_record) {
		this.is_have_gosave_record = is_have_gosave_record;
	}
	public boolean isCore_account_result() {
		return core_account_result;
	}
	public void setCore_account_result(boolean core_account_result) {
		this.core_account_result = core_account_result;
	}
	public String getPage() {
		return Page;
	}
	public void setPage(String page) {
		Page = page;
	}
	public ArrayList<String> getAll_fail_Ref_list() {
		return all_fail_Ref_list;
	}
	public void setAll_fail_Ref_list(ArrayList<String> all_fail_Ref_list) {
		this.all_fail_Ref_list = all_fail_Ref_list;
	}

	public String toResult() {
		return "客户ID      =" + Customer_id +"\n"+
				"账户ID      =" + Account_id +"\n"+
				"账单月份                  =" + which_month +"\n"+
				"核心账户(首页)        =" + coreaccount_total_capture_title +"\n"+
				"GoSave余额(首页)  =" + gosave_total_capture_title +"\n"+
				"结存总额(首页)        =" + Total_Amount_capture_title +"\n"+
				"私人贷款账户(首页) =" + laon_total_capture_title +"\n"+
				"上月余额     =" + last_month_amount +"\n"+
				"核心账户计算余额   =" + coreaccount_total_calculate +"\n"+
				"GoSave 计算余额   =" + gosave_total_calculate +"\n"+
				"Loans  计算余额   =" + laon_total_calculate +"\n"+
				"核心账户抓取余额(转账记录)   =" + coreaccount_total_capture +"\n"+
				"结存总额计算余额   =" + Total_Amount_calculate +"\n"+
				"********************************************************"+"\n"+
				"核心账户测试结果  =" + core_account_result +"\n"+
				"GoSave 测试结果  =" + go_save_result +"\n"+
				"Loans  测试结果  =" + loans_result +"\n"+
				"结存总额测试结果  =" + is_total_amount_equal +"\n"+
				"********************************************************"+"\n\n";
	}
	
	public String toResult1() {
		return "Customer_id=" + Customer_id +"\n"+
				"Account_id=" + Account_id +"\n"+
				"which_month=" + which_month +"\n"+
				"coreaccount_total_capture_title=" + coreaccount_total_capture_title +"\n"+
				"gosave_total_capture_title=" + gosave_total_capture_title +"\n"+
				"laon_total_capture_title=" + laon_total_capture_title +"\n"+
				"Total_Amount_capture_title=" + Total_Amount_capture_title +"\n"+
				"coreaccount_total_calculate=" + coreaccount_total_calculate +"\n"+
				"gosave_total_calculate=" + gosave_total_calculate +"\n"+
				"laon_total_calculate=" + laon_total_calculate +"\n"+
				"coreaccount_total_capture=" + coreaccount_total_capture +"\n"+
				"Total_Amount_calculate=" + Total_Amount_calculate +"\n"+
				"********************************************************"+"\n"+
				"core_account_result=" + core_account_result +"\n"+
				"go_save_result=" + go_save_result +"\n"+
				"loans_result=" + loans_result +"\n"+
				"is_total_amount_equal=" + is_total_amount_equal +"\n"+
				"********************************************************"+"\n\n";
	}

	@Override
	public String toString() {
		return "Estatement [YEAR=" + YEAR + ", Last_months_YEAR=" + Last_months_YEAR + ", Month=" + Month
				+ ", Last_Month=" + Last_Month + ", Total_Amount_calculate=" + Total_Amount_calculate
				+ ", Total_Amount_capture_title=" + Total_Amount_capture_title + ", File_name=" + File_name + ", Account_id="
				+ Account_id + ", Customer_id=" + Customer_id + ", last_month_amount=" + last_month_amount
				+ ", gosave_total_calculate=" + gosave_total_calculate + ", gosave_total_capture="
				+ gosave_total_capture + ", gosave_total_capture_title=" + gosave_total_capture_title
				+ ", coreaccount_total_calculate=" + coreaccount_total_calculate + ", coreaccount_total_capture="
				+ coreaccount_total_capture + ", coreaccount_total_capture_title=" + coreaccount_total_capture_title
				+ ", loan_total_from_bank=" + loan_total_from_bank + ", laon_total_calculate=" + laon_total_calculate
				+ ", laon_total_capture_title=" + laon_total_capture_title + ", which_month=" + which_month
				+ ", is_have_gosave_record=" + is_have_gosave_record + ", is_have_loan_record=" + is_have_loan_record
				+ ", is_total_amount_equal=" + is_total_amount_equal + ", core_account_result=" + core_account_result
				+ ", go_save_result=" + go_save_result + ", loans_result=" + loans_result + ",\n current_gosave_exist="
				+ current_gosave_exist + "]";
	}

}
