package com.welab.automation.framework.utils.app;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GenerateExcelReport {

    String[] sheetNames= {"Login","Card","Payment","Maintenance","GoSave"};
    private ArrayList<String> TestCaseList= new ArrayList<>();

    public  String get_date_string() {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public XSSFWorkbook create_result_report(String folder_path,String flag) {
        String filename = folder_path+"//Result_"+get_date_string()+".xlsx";
        FileOutputStream out;
        XSSFWorkbook workbook = null;
        try {
            workbook=new XSSFWorkbook();
            XSSFSheet sheet0 = workbook.createSheet();
            workbook.setSheetName(0,"Result");
            XSSFRow row = sheet0.createRow(0);
            XSSFCell c = row.createCell(0);
            c.setCellValue(flag);
            row.createCell(1).setCellValue(get_date_string());
            for(int i =0;i<sheetNames.length;i++) {
                workbook.createSheet();
                workbook.setSheetName(i+1,sheetNames[i]);
            }

            List<String> ScreenShotList = getScreenShotAbsolutePathList(folder_path,flag);
            WritePictureToExcel(ScreenShotList,workbook,flag);

            out = new FileOutputStream(filename);
            workbook.write(out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return workbook;
    }

    public List<String> getScreenShotAbsolutePathList(String Folder_path,String flag) {
        List<String> ScreenShotList = new ArrayList<>();
        File file = new File(Folder_path);
        File[] fs = file.listFiles();
        for(File f:fs) {
            if(f.getName().toUpperCase().contains(flag.toUpperCase())) {
                ScreenShotList.add(f.getAbsolutePath());
            }
        }
        return ScreenShotList;
    }

    public void WritePictureToExcel(List<String> filePaths,XSSFWorkbook workbook,String flag) {
        for(int i =0;i<sheetNames.length;i++) {
            String sheetName =sheetNames[i];
            int count=0;
            for(String s:filePaths) {
                XSSFSheet sheet =workbook.getSheet(sheetName);
                sheet.setColumnWidth(0, 5000);  //设置第一列的宽度
                String file_name = getFileNaneFromPath(s);
                if(file_name.toUpperCase().contains(sheetName.toUpperCase())) {
                    mergeCellForTestCaseName(sheet,file_name,flag);
                    PictureWriteToExcel(s,workbook,sheet,count);
                    count+=1;
                }
            }
        }
    }

    public String getFileNaneFromPath(String path){
        String fileName ="";
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            fileName =path.substring(path.lastIndexOf("\\")+1);
        } else {
            fileName =path.substring(path.lastIndexOf("/")+1);
        }
        return fileName;
    }

    public int getTestCaseIndex(String file_name) {
        String[] ss = file_name.split("_");
        return Integer.parseInt(ss[2]);
    }

    public String getTestCaseName(String file_name) {
        String[] ss = file_name.split("_");
        return ss[0]+"_"+ss[1]+"_"+ss[2];
    }

    public String getTestCaseName(String file_name,String flag) {
        int index = file_name.toUpperCase().indexOf(flag.toUpperCase())-1;
        return file_name.substring(0,index);
    }

    public XSSFSheet mergeCellForTestCaseName(XSSFSheet sheet,String file_name,String flag){
        String testCaseName =getTestCaseName(file_name,flag);
        if(TestCaseList.contains(testCaseName)){
            return sheet;
        }
        TestCaseList.add(testCaseName);
        int index =getTestCaseIndex(file_name);
        int start_row = (index-1)*30;
        int end_row = (index-1)*30+29;
        CellRangeAddress region = new CellRangeAddress(start_row, end_row, 0, 0);
        XSSFCell c=sheet.createRow(start_row).createCell(0);
        c.setCellValue(testCaseName);
        sheet.addMergedRegion(region);
        return sheet;
    }

    public static int getRowIndex(String s) {
        s = s.substring(s.lastIndexOf("\\")+1).toUpperCase();
        String flag =getPictureFlag(s);
        int index_end=s.indexOf(flag.toUpperCase());
        String v = s.substring(0,index_end);
        v = v.substring(v.lastIndexOf("_")+1);
        int value = Integer.parseInt(v)-1;
        return value;
    }

    public static int getColumnIndex(String s) {
        s = s.substring(s.lastIndexOf("\\")+1).toUpperCase();
        String flag =getPictureFlag(s);
        int index_end=s.indexOf(flag.toUpperCase());
        String v = s.substring(index_end+flag.length());
        v = v.substring(0,v.indexOf(".PNG"));
        int value = Integer.parseInt(v)-1;
        return value;
    }

    public static String getPictureFlag(String s) {
        if(s.toUpperCase().contains("ANDROID")) {
            return "_ANDROID_";
        }else if(s.toUpperCase().contains("IOS")){
            return "_IOS_";
        }else {
            return "_ANDROID_";
        }
    }

    public  void PictureWriteToExcel(String picture_path,XSSFWorkbook wb,XSSFSheet sheet,int count) {
        int row_index =getRowIndex(picture_path);
        int column_index =getColumnIndex(picture_path);
        FileOutputStream fileOut = null;
        BufferedImage bufferImg = null;//图片
        try {
            ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            bufferImg = ImageIO.read(new File(picture_path));
            ImageIO.write(bufferImg, "png", byteArrayOut);
            XSSFDrawing patriarch = sheet.createDrawingPatriarch();
            /**
             * 该构造函数有8个参数
             * 前四个参数是控制图片在单元格的位置，分别是图片距离单元格left，top，right，bottom的像素距离
             * 后四个参数，前两个表示图片左上角所在的cellNum和 rowNum，后两个参数对应的表示图片右下角所在的cellNum和 rowNum，
             * excel中的cellNum和rowNum的index都是从0开始的
             */
            XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0,
                    (short) column_index*5+1, row_index*30, (short) column_index*5+4+1, row_index*30+27);
            patriarch.createPicture(anchor, wb.addPicture(byteArrayOut
                    .toByteArray(), XSSFWorkbook.PICTURE_TYPE_JPEG));
        } catch (Exception io) {
            io.printStackTrace();
            System.out.println("io erorr : " + io.getMessage());
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
