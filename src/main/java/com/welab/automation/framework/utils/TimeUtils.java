
package com.welab.automation.framework.utils;

import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.*;

import static com.welab.automation.framework.GlobalVar.WAIT_TIMEOUT_MEDIUM_S;

public class TimeUtils {
  private static final Logger logger = LoggerFactory.getLogger(TimeUtils.class);

  private static Faker faker = new Faker();

  /** format : yyyyMMdd000000 */
  public static String YEAR_MONTH_DAY_MS = "yyyyMMdd000000"; // 20200304000000
  /** format: yyyy-MM-ddTHH:mm:ss */
  private static String YEAR_MONTH_DAY_T_HOUR_MIN_SEC = "yyyy-MM-dd'T'HH:mm:ss"; // 20200304000000

  private static String YEAR_MONTH_DAY_HOUR_MIN_SEC = "yyyy-MM-ddHH:mm:ss"; // 20200304000000
  private static String YEAR_MONTH_DAY_SPACE_HOUR_MIN_SEC = "yyyy-MM-dd HH:mm:ss"; // 20200304000000

  /** format: yyyy-MM-ddTHH:mm:ss */
  private static String YEAR_MONTH = "yyyy-MM"; // 20200304000000

  private static String TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  private static String YEAR_MONTH_DAY = "yyyy-MM-dd"; // 20200304000000
  private static String HOUR_MIN_SEC = "HH:mm:ss"; // 20200304000000

  public static ZonedDateTime getNowZoneTime() {
    return ZonedDateTime.now();
  }

  public static Instant getNowInstant() {
    return Instant.now();
  }

  public static String getStrNowZoneTime() {
    return ZonedDateTime.now().toString();
  }

  /**
   * yyyy-MM-dd HH:mm:ss
   *
   * @return yyyy-MM-dd HH:mm:ss
   */
  public static String getLocalFormatTime() {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_SPACE_HOUR_MIN_SEC);
    return dateFormat.format(new Date());
  }

  public static String getLocalFormatTimeWithT() {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_T_HOUR_MIN_SEC);
    return dateFormat.format(new Date());
  }

  public static String dateTranslate(String source, String setDateFormat) {
    DateFormat format = new SimpleDateFormat(setDateFormat);
    Date date = new Date(source);
    return format.format(date);
  }

  public static String generateTimestamp() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmssS");
    return dateFormat.format(new Date()).concat(String.valueOf(faker.random().nextInt(10000)));
  }

  public static String getLocalFormatTime(int day) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_SPACE_HOUR_MIN_SEC);
    Calendar calendar = Calendar.getInstance();
    String date;
    if (day == 0) date = dateFormat.format(new Date());
    else {
      calendar.add(Calendar.DAY_OF_MONTH, day);
      date = dateFormat.format(calendar.getTime());
    }
    return date;
  }

  public static String getLocalFormatMonth(int month) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH);
    Calendar calendar = Calendar.getInstance();
    String date;
    if (month == 0) date = dateFormat.format(new Date());
    else {
      calendar.add(Calendar.MONTH, month);
      date = dateFormat.format(calendar.getTime());
    }
    return date;
  }

  public static String getLocalFormatDay(int day) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY);
    Calendar calendar = Calendar.getInstance();
    String date;
    if (day == 0) date = dateFormat.format(new Date());
    else {
      calendar.add(Calendar.DAY_OF_MONTH, day);
      date = dateFormat.format(calendar.getTime());
    }
    return date;
  }

  public static String getZonedFormatDay(int day) {
    ZonedDateTime now = ZonedDateTime.now();
    if (day > 0) return now.plusDays(day).toInstant().toString();
    else if (day == 0) return now.toInstant().toString();
    else return now.minusDays(Math.abs(day)).toInstant().toString();
  }

  /**
   * yyyy-MM-dd
   *
   * @return
   */
  public static String getCurrentDay() {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY);
    return dateFormat.format(new Date());
  }

  public static String getFormatAddDay(int day, String dateFormat) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, day);
    Date date = calendar.getTime();
    DateFormat df = new SimpleDateFormat(dateFormat);
    String startDate = df.format(date);
    return startDate;
  }

  public static String getCurrentTime() {
    String formatStr = HOUR_MIN_SEC;
    SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
    return dateFormat.format(new Date());
  }

  public static String getTimeByAddHour(int hour) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.HOUR, hour);
    Date date = calendar.getTime();
    String formatStr = HOUR_MIN_SEC;
    SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
    return dateFormat.format(date);
  }

  public static String getTimeByAddHourMinute(int hour, int minute) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.HOUR, hour);
    calendar.add(Calendar.MINUTE, minute);
    Date date = calendar.getTime();
    String formatStr = HOUR_MIN_SEC;
    SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
    return dateFormat.format(date);
  }

  public static String getTimeByAddMinute(int minute) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, minute);
    Date date = calendar.getTime();
    String formatStr = HOUR_MIN_SEC;
    SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
    return dateFormat.format(date);
  }

  public static String getTimeByAddSecond(int second) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.SECOND, second);
    Date date = calendar.getTime();
    String formatStr = HOUR_MIN_SEC;
    SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
    return dateFormat.format(date);
  }

  public static String getYesterday() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -1);
    Date date = calendar.getTime();
    DateFormat df = new SimpleDateFormat(YEAR_MONTH_DAY);
    String startDate = df.format(date);
    return startDate;
  }

  /**
   * yyyy-MM-dd
   *
   * @return
   */
  public static String getTomorrow() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, 1);
    Date date = calendar.getTime();
    DateFormat df = new SimpleDateFormat(YEAR_MONTH_DAY);
    String endDate = df.format(date);
    return endDate;
  }

  public static List<String> getPreviewMonth() {
    String month = TimeUtils.getLocalFormatMonth(0);
    int m = Integer.valueOf(month.substring(5));
    List<String> list = new ArrayList<>();
    while (m > 0) {
      if (m >= 10) list.add(month.substring(0, 4) + "-" + m);
      else list.add(month.substring(0, 4) + "-0" + m);
      m--;
    }
    return list;
  }

  public static String getYear() {
    String month = TimeUtils.getLocalFormatMonth(0);
    return month.substring(0, 4);
  }

  public static String getMonth() {
    String month = TimeUtils.getLocalFormatMonth(0);
    return month.substring(5);
  }

  public static String getTimeSpan() {
    Date date = new Date();
    DateFormat format = new SimpleDateFormat(YEAR_MONTH_DAY_HOUR_MIN_SEC);
    String timespan = format.format(date);
    return timespan;
  }

  public static String getTimeWithFormat(Date date, String format) {
    Optional.ofNullable(date).ifPresent(d -> new Date());
    DateFormat dateFormat = new SimpleDateFormat(format);
    String timespan = dateFormat.format(date);
    return timespan;
  }

  public static String getUTCDate() {
    Calendar calendar = Calendar.getInstance();
    int offset = calendar.get(Calendar.ZONE_OFFSET);
    calendar.add(Calendar.MILLISECOND, -offset);
    Date date = calendar.getTime();
    DateFormat format = new SimpleDateFormat(YEAR_MONTH_DAY_HOUR_MIN_SEC);
    String timespan = format.format(date);
    return timespan;
  }

  /**
   * @param datetime dateTime
   * @param num minutes
   * @param type
   * @return
   */
  public static Date adjustDateByHour(String datetime, Integer num, int type) {
    try {
      Calendar Cal = Calendar.getInstance();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
      ParsePosition pos = new ParsePosition(0);
      Date originalDate = format.parse(datetime, pos);
      Cal.setTime(originalDate);
      if (type == 0) {
        Cal.add(Calendar.MINUTE, -num);
      } else {
        Cal.add(Calendar.MINUTE, num);
      }
      return Cal.getTime();
    } catch (NullPointerException e) {
      return null;
    }
  }

  /**
   * @param time dateTime
   * @return
   */
  public static String getAvailableSlot(Date time) {
    String formatStr = "HH:mm";
    SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
    Calendar newTime = Calendar.getInstance();
    newTime.setTime(time);
    return sdf.format(newTime.getTime());
  }

  public String convertDataForT(String inputData) throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
    return new SimpleDateFormat(YEAR_MONTH_DAY_SPACE_HOUR_MIN_SEC)
        .format(simpleDateFormat.parse(inputData));
  }

  public static String getTime() {
    DateFormat df = new SimpleDateFormat(YEAR_MONTH_DAY_HOUR_MIN_SEC);
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.HOUR, -8);
    String dateName = df.format(calendar.getTime());
    return dateName;
  }

  public static String getCurrentTimeAsString() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
    return ZonedDateTime.now().format(formatter);
  }

  public static int setTimeOut(int... timeOut) {
    int actualTimeout = WAIT_TIMEOUT_MEDIUM_S;
    if (timeOut.length != 0) {
      actualTimeout = timeOut[0];
    }
    return actualTimeout;
  }

  public static long generateUnixTimeStampPlus(long amountToAdd, TemporalUnit unit) {
    return Instant.now().plus(amountToAdd, unit).getEpochSecond();
  }

  public static boolean checkNewerDateTime(String oldDateTime, String newDateTime) {
    SimpleDateFormat format = new SimpleDateFormat(YEAR_MONTH_DAY_SPACE_HOUR_MIN_SEC);
    try {
      Date formatedOldDateTime = format.parse(oldDateTime);
      Date formatedNewDateTime = format.parse(newDateTime);
      if (formatedOldDateTime.compareTo(formatedNewDateTime) <= 0) {
        return true;
      } else {
        return false;
      }
    } catch (ParseException e) {
      logger.error("Parse error: {}", e.toString());
      throw new RuntimeException(e.toString());
    }
  }
}
