package com.welab.automation.framework.utils;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.entity.api.EnvironmentEntity;
import com.welab.automation.framework.utils.entity.api.RequestEntity;
import com.welab.automation.framework.utils.entity.api.RequestHeader;
import com.welab.automation.framework.utils.entity.app.DeviceAppEntity;
import org.apache.http.HttpHeaders;

public class TestDataReader {

  public static String getHost() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestEntity.HOST);
  }

  public static String getPort() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestEntity.PORT);
  }

  public static String getEnvironment() {
    return GlobalVar.GLOBAL_VARIABLES.get(EnvironmentEntity.ENV);
  }

  public static String getPlatform() {
    return GlobalVar.GLOBAL_VARIABLES.get(EnvironmentEntity.PLATFORM);
  }

  public static String getOCPAPIMSubKey() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.OCP_APIM_SUB_KEY);
  }

  public static String getExpired() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.EXPIRED);
  }

  public static String getClientId() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.CLIENT_ID);
  }

  public static String getCookie() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.COOKIE);
  }

  public static String getClientSecret() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.CLIENT_SECRET);
  }

  public static String getLanguageCode() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.LANGUAGE_CODE);
  }

  public static String getMAC() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.MAC);
  }

  public static String getSignature() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.SIGNATURE);
  }

  public static String getAuthorization() {
    return GlobalVar.GLOBAL_VARIABLES.get(HttpHeaders.AUTHORIZATION);
  }

  public static String getTimespan() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.TIME_SPAN);
  }

  public static String getAcceptLanguage() {
    return GlobalVar.GLOBAL_VARIABLES.get(RequestHeader.ACCEPT_LANGUAGE);
  }

  public static String getAutomationName() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.AUTOMATION_NAME);
  }

  public static String getBundleId() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.BUNDLE_ID);
  }

  public static String getVersion() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.PLATFORM_VERSION);
  }

  public static String getAppPackage() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.APP_PACKAGE);
  }

  public static String getAppPath() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.APP_PATH);
  }

  public static String getAppActivity() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.APP_ACTIVITY);
  }

  public static String getDeviceName() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.DEVICE_NAME);
  }

  public static String getLocalAddress() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LOCAL_ADDRESS);
  }

  public static String getAppiumPort() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LOCAL_APPIUM_PORT);
  }

  public static String getDriverPath() {
    return DeviceAppEntity.DRIVER_PATH;
  }

  public static String getAppiumPath() {
    return DeviceAppEntity.APPIUM_PATH;
  }

  public static String getDeviceUDID() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.UDID);
  }

  public static String getXcodeOrgId() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.XCODE_ORG_ID);
  }

  public static String getxcodeSigningId() {
    return GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.XCODE_SIGNING_ID);
  }

  public static int getLowerImplicitWait() {
    int defaultLowerImplicitWait = 0;
    if (GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LOWER_IMPLICIT_WAIT) != null) {
      return Integer.parseInt(GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LOWER_IMPLICIT_WAIT));
    }
    return defaultLowerImplicitWait;
  }

  public static int getImplicitWait() {
    int appiumDefaultImplicitWait = 0;
    if (GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.IMPLICIT_WAIT) != null) {
      return Integer.parseInt(GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.IMPLICIT_WAIT));
    }
    return appiumDefaultImplicitWait;
  }

  public static int getExplicitWait() {
    int defaultExplicitWait = 30;
    if (GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.EXPLICIT_WAIT) != null) {
      return Integer.parseInt(GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.EXPLICIT_WAIT));
    }
    return defaultExplicitWait;
  }

  public static int getLongWait() {
    int appiumDefaultLongWait = 60;
    if (GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LONG_WAIT) != null) {
      return Integer.parseInt(GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.LONG_WAIT));
    }
    return appiumDefaultLongWait;
  }

  public static int getNewCommandTimeout() {
    int appiumDefaultNewCommandTimeout = 60;
    if (GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.NEW_COMMAND_TIMEOUT) != null) {
      return Integer.parseInt(GlobalVar.GLOBAL_VARIABLES.get(DeviceAppEntity.NEW_COMMAND_TIMEOUT));
    }
    return appiumDefaultNewCommandTimeout;
  }
}
