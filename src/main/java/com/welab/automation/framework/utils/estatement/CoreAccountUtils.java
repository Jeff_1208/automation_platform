package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.Transactions;

import java.util.ArrayList;

public class CoreAccountUtils {
  String[] trans_type_shuzu = {
    Global.SEND_MONEY_STRING,
    Global.RECEIVE_MONEY_STRING,
    Global.DEBIT_CARD_SPENDING_STRING,
    Global.CASH_REBATE_STRING,
    Global.DEPOSIT_INTEREST_STRING,
    Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING,
    Global.DEPOSIT_TO_GOSAVE_STRING,
    Global.JETCO_ATM_STRING,
    Global.OTHER_STRING,
    Global.OVERSEAS_ATM_STRING,
    Global.DEBIT_ARRANGEMENT_STRING,
    Global.MONTHLY_REPAYMENT_STRING
  };
  String[] checK_FT_START_shuzu = {
    Global.SEND_MONEY_STRING,
    Global.RECEIVE_MONEY_STRING,
    Global.DEBIT_CARD_SPENDING_STRING,
    Global.CASH_REBATE_STRING,
    Global.JETCO_ATM_STRING,
    Global.OVERSEAS_ATM_STRING,
    Global.ADD_MONEY_STRING
  };
  String[] checK_AAAC_START_shuzu = {
    Global.DEPOSIT_TO_GOSAVE_STRING, Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING
  };

  private ArrayList<String> Spec_REF_list;

  public CoreAccountUtils(ArrayList<String> Spec_REF_list) {
    this.Spec_REF_list = Spec_REF_list;
  }

  // {"收款","付款","Debit Card 消費","現金回贈","存款利息","GoSave
  // 轉至核心賬戶","存入GoSave","銀通櫃員機提款","其他","Cirrus/海外櫃員機提款","Debit Arrangement"};
  public String get_type(String line) {
    String type = "";
    if (line.contains(Global.RECEIVE_MONEY_STRING)) {
      type = Global.RECEIVE_MONEY_STRING;
    } else if (line.contains(Global.SEND_MONEY_STRING)) {
      type = Global.SEND_MONEY_STRING;
    } else if (line.contains(Global.DEBIT_CARD_SPENDING_STRING)) {
      type = Global.DEBIT_CARD_SPENDING_STRING;
    } else if (line.contains(Global.CASH_REBATE_STRING)) {
      type = Global.CASH_REBATE_STRING;
    } else if (line.contains(Global.ADD_MONEY_STRING)) {
      type = Global.ADD_MONEY_STRING;
    } else if (line.contains(Global.DEPOSIT_INTEREST_STRING)) {
      type = Global.DEPOSIT_INTEREST_STRING;
    } else if (line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING)) {
      // System.out.println(line);
      type = Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING;
    } else if (line.contains(Global.DEPOSIT_TO_GOSAVE_STRING)) {
      type = Global.DEPOSIT_TO_GOSAVE_STRING;
    } else if (line.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) {
      type = Global.JETCO_ATM_WITHDRAWAL_STRING;
    } else if (line.contains(Global.JETCO_ATM_REFUND_STRING)) {
      type = Global.JETCO_ATM_REFUND_STRING;
    } else if (line.contains(Global.OTHER_STRING)) {
      type = Global.OTHER_STRING;
    } else if (line.contains(Global.OVERSEAS_ATM_WITHDRAWAL_STRING)) {
      type = Global.OVERSEAS_ATM_WITHDRAWAL_STRING;
    } else if (line.contains(Global.OVERSEAS_ATM_REFUND_STRING)) {
      type = Global.OVERSEAS_ATM_REFUND_STRING;
    } else if (line.contains(Global.DEBIT_ARRANGEMENT_STRING)) {
      type = Global.DEBIT_ARRANGEMENT_STRING;
    } else if (line.contains(Global.LOAN_DRAWDOWN_STRING)) {
      type = Global.LOAN_DRAWDOWN_STRING;
    } else if (line.contains(Global.MONTHLY_REPAYMENT_STRING)) {
      type = Global.MONTHLY_REPAYMENT_STRING;
    } else {
      type = "unexcept";
    }
    return type;
  }

  public Transactions check_type_for_transaction(
      String type, String[] content_list, int index, Transactions tran, Estatement e) {
    boolean result = false;
    // String[] need_check_type_shuzu = {"存款利息","存入款項","付款","收款","Debit Card
    // 消費","銀通櫃員機提款","銀通櫃員機退款","Cirrus/海外櫃員機提款","Cirrus/海外櫃員機退款","存入GoSave","GoSave 轉至核心賬戶"};

    if (type.contains(Global.DEPOSIT_INTEREST_STRING)) {
      result = is_have_flag_string(content_list, index, "Deposit interest");
    } else if (type.contains(Global.ADD_MONEY_STRING)) {
      result = is_have_flag_string(content_list, index, "Add money");
    } else if (type.contains(Global.SEND_MONEY_STRING)) {
      result = is_have_flag_string(content_list, index, "Send money");
    } else if (type.contains(Global.RECEIVE_MONEY_STRING)) {
      boolean result1 = is_have_flag_string(content_list, index, "Receive money");
      boolean result2 = is_have_flag_string(content_list, index, "reversal");
      result = result2 || result1;
    } else if (type.contains(Global.DEBIT_CARD_STRING)) {
      String[] debit_shuzu = {"Debit Card refund", "Debit Card spending"};
      result = is_have_flag_string_shuzu(content_list, index, debit_shuzu);
    } else if (type.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) {
      // String[] JETCO_shuzu ={"JETCO ATM refund","JETCO ATM withdrawal"};
      // result = is_have_flag_string_shuzu(content_list[index],JETCO_shuzu);
      result = is_have_flag_string(content_list, index, "JETCO ATM withdrawal");
    } else if (type.contains(Global.JETCO_ATM_REFUND_STRING)) {
      result = is_have_flag_string(content_list, index, "JETCO ATM refund");
    } else if (type.contains(Global.OVERSEAS_ATM_STRING)) {
      // String[] Overseas_shuzu ={"Cirrus/ Overseas ATM withdrawal","Cirrus/ Overseas ATM refund"};
      String[] Overseas_shuzu = {"Cirrus", "overseas", "ATM"};
      result = is_have_all_flag_inline(content_list, index, Overseas_shuzu);
    } else if (type.contains(Global.DEPOSIT_TO_GOSAVE_STRING)) {
      result = is_have_flag_string(content_list, index, "Deposit to GoSave");
    } else if (type.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING)) {
      result = is_have_flag_string(content_list, index, "GoSave transferred to");
    } else {
      result = true;
    }

    tran.setIs_right_type(result);
    return tran;
  }

  public boolean is_have_flag_string(String[] content_list, int index, String flag) {
    if (content_list[index].contains(flag)) {
      return true;
    } else if (content_list[index - 1].contains(flag)) {
      return true;
    } else if (content_list[index - 2].contains(flag)) {
      return true;
    } else if (content_list[index + 1].contains(flag)) {
      return true;
    } else {
      return false;
    }
  }

  public boolean is_have_flag_string_shuzu(String[] content_list, int index, String[] flag) {
    if (Common.is_one_of_shuzu_item_in_line(content_list[index], flag)) {
      return true;
    } else if (Common.is_one_of_shuzu_item_in_line(content_list[index - 1], flag)) {
      return true;
    } else if (Common.is_one_of_shuzu_item_in_line(content_list[index - 2], flag)) {
      return true;
    } else if (Common.is_one_of_shuzu_item_in_line(content_list[index + 1], flag)) {
      return true;
    } else {
      return false;
    }
  }

  public boolean is_have_all_flag_inline(String[] content_list, int index, String[] flag) {
    if (Common.is_all_of_shuzu_item_in_line(content_list[index], flag)) {
      return true;
    } else if (Common.is_all_of_shuzu_item_in_line(content_list[index - 1], flag)) {
      return true;
    } else if (Common.is_all_of_shuzu_item_in_line(content_list[index - 2], flag)) {
      return true;
    } else if (Common.is_all_of_shuzu_item_in_line(content_list[index + 1], flag)) {
      return true;
    } else {
      return false;
    }
  }

  public ArrayList<Transactions> get_Transactions(
      String[] content_list,
      int index,
      ArrayList<Transactions> trans_list,
      Estatement e,
      int trans_id) {
    Transactions tran = new Transactions();
    tran.setCore_account_trans_number(trans_id);
    tran.setAmount_row_content(content_list[index]);
    String amount = Common.get_value_string_from_line(content_list[index], -1);
    tran.setAmount(amount);
    tran.setDay_string(Common.get_days_String_value_from_line(content_list[index]));
    tran.setDay(Common.get_days_value_from_line(content_list[index]));
    tran.setMonth(Common.get_month_value_from_line(content_list[index]));
    tran.setYear(Common.get_year_value_from_line(content_list[index]));
    String type = get_type(content_list[index - 1]);
    // 检查  type是否正确
    tran = check_type_for_transaction(type, content_list, index, tran, e);

    if (type.contains(Global.SEND_MONEY_STRING)) {
      if (amount.contains("-")) {
        tran.setIs_recieve_money_have_right_fuhao(false);
      } else {
        tran.setIs_recieve_money_have_right_fuhao(true);
      }
    }
    if (type.contains(Global.RECEIVE_MONEY_STRING)) {
      if (amount.contains("-")) {
        tran.setIs_send_money_have_right_fuhao(true);
      } else {
        tran.setIs_send_money_have_right_fuhao(false);
      }
    }
    if (type.contains("GoSave")) {
      tran.setIs_goSave_trans(true);
    }
    if (type.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) {
      tran.setIs_JECTO_transaction(true);
      if (content_list[index].contains("CNY")) {
        boolean result = Common.check_foreign_currency_Amount_for_trans(content_list, index);
        tran.setCheck_foreign_currency_Amount(result);
      }
    }
    // tran =Common.get_Date_String_from_line(content_list[index], e,tran);
    tran.setDate(tran.getDay_string() + " " + tran.getMonth() + " " + tran.getYear());
    tran.setType(type);

    String ref = Common.get_Ref_for_spec_trans(content_list, index, "f:");
    if (ref == "NONE") {
      ref = Common.get_Ref_for_spec_trans(content_list, index, ":");
    }
    tran.setRef(ref);
    tran = check_trans_desc(content_list, index, e, tran, type); // 检查transation的   description

    trans_list.add(tran);
    return trans_list;
  }

  public ArrayList<Transactions> get_Transactions_spec(
      String[] content_list,
      int index,
      ArrayList<Transactions> trans_list,
      Estatement e,
      int trans_id) {
    //		Receive money Ref: FT21152TJW06
    //		付款 Send money to FUTU S********* I************ (**** K**** L**************
    //		1 Jun 2021 A**
    //		Send money Ref: FT211523QL9L -50,000.00
    Transactions tran = new Transactions();
    tran.setCore_account_trans_number(trans_id);
    tran.setAmount_row_content(content_list[index]);
    String amount = Common.get_value_string_from_line(content_list[index], -1);
    tran.setAmount(amount);
    tran.setDay_string(Common.get_days_String_value_from_line(content_list[index]));
    tran.setDay(Common.get_days_value_from_line(content_list[index - 1]));
    tran.setMonth(Common.get_month_value_from_line(content_list[index - 1]));
    tran.setYear(Common.get_year_value_from_line(content_list[index - 1]));
    String type = get_type(content_list[index - 3]);
    tran.setType(type);
    // tran=check_type_for_transaction(type,content_list,index,tran,e);//检查  type是否正确
    if (type.contains("GoSave")) {
      tran.setIs_goSave_trans(true);
    }
    if (type.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) {
      tran.setIs_JECTO_transaction(true);
      if (content_list[index].contains("CNY")) {
        boolean result = Common.check_foreign_currency_Amount_for_trans(content_list, index);
        tran.setCheck_foreign_currency_Amount(result);
      }
    }
    tran.setDate(tran.getDay_string() + " " + tran.getMonth() + " " + tran.getYear());
    tran.setRef(Common.get_Ref_for_spec_trans(content_list, index, "f:"));
    trans_list.add(tran);
    return trans_list;
  }

  public boolean compare_ref_for_ATM_revesal(
      Transactions tran, ArrayList<Transactions> trans_list, Estatement e) {
    boolean r = false;
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions t = trans_list.get(i);
      if (!tran.getType().contains(Global.DEBIT_CARD_SPENDING_STRING)) continue;
      if (tran.getCore_account_trans_number() == t.getCore_account_trans_number()) continue;
      if (compare_invalid_trans_amount(tran.getAmount(), t.getAmount())) {
        if (tran.getRef().contains(t.getRef())) {
          r = true;
        } else {
        }
      }
    }
    return r;
  }

  public Estatement check_single_for_amount(ArrayList<Transactions> trans_list, Estatement e) {
    String[] checK_single_should_be_found_shuzu = {
      Global.SEND_MONEY_STRING,
      Global.DEBIT_CARD_SPENDING_STRING,
      Global.DEPOSIT_TO_GOSAVE_STRING,
      Global.OVERSEAS_ATM_WITHDRAWAL_STRING
    };
    String[] checK_single_should_not_be_found_shuzu = {Global.RECEIVE_MONEY_STRING};
    ArrayList<String> fail_single_list = new ArrayList<>();
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions tran = trans_list.get(i);
      if (tran.isIs_invalid_trans()) {
        continue;
      }
      if (Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_single_should_be_found_shuzu)) {
        if (!(tran.getAmount().contains("-"))) {
          if (!(tran.getDescription()
              .contains(
                  "Send money by FPS"))) { // Invalid trans description是Send money by FPS，付款的金额符号可能为
                                           // 正，即不包含“-”
            if (!(tran.getDescription().contains("reversal"))) { // 退款的时候金额可以不包含“-”
              fail_single_list.add(tran.getRef());
            }
          }
        }
      }

      if (Common.is_one_of_shuzu_item_in_line(
          tran.getType(), checK_single_should_not_be_found_shuzu)) {
        if (tran.getAmount().contains("-")) {
          fail_single_list.add(tran.getRef());
        }
      }
      if (tran.getType().contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) { // 针对ATM机取款又退回的情况
        if ((!tran.getAmount().contains("-"))
            && (!compare_ref_for_ATM_revesal(tran, trans_list, e))) {
          fail_single_list.add(tran.getRef());
        }
      }
    }
    e.setWrong_single_amount_of_ref_list(fail_single_list);
    return e;
  }

  public Estatement check_Ref_Start_flag(ArrayList<Transactions> trans_list, Estatement e) {
    ArrayList<String> fail_ref_start_list = new ArrayList<>();
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions tran = trans_list.get(i);
      if (Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_FT_START_shuzu)) {
        if (tran.getRef() == null) {
        } else if (!(tran.getRef().startsWith("FT"))) {
          if (!tran.getRef().contains("money")) {
            fail_ref_start_list.add(tran.getRef());
          }
        }
      }
      if (Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_AAAC_START_shuzu)) {
        if (!(tran.getRef().startsWith("AAAC"))) {
          if (!tran.getRef().contains("money")) {
            fail_ref_start_list.add(tran.getRef());
          }
        }
      }
    }
    e.setFail_start_Ref_list(fail_ref_start_list);
    return e;
  }

  private Transactions check_trans_desc(
      String[] content_list, int index, Estatement e, Transactions tran, String type) {
    boolean desc_check_result = true;
    String desc = Common.get_string_value_from_string_with_start(content_list[index - 1], type);
    tran.setDescription(desc);
    String[] shuzu_temp = {Global.DEBIT_ARRANGEMENT_STRING, Global.DEBIT_CARD_SPENDING_STRING};
    if (desc.length() < 3) {
      if (!Common.is_one_of_shuzu_item_in_line(tran.getType(), shuzu_temp)) {
        desc_check_result = false;
      }
    }
    //  Reversal Transaction   Invalid transation 返回金额 的description
    // Send money by FPS //Receive Money by FPS
    if (Common.compareUpperStr(desc, "SEND MONEY BY FPS")
        || Common.compareUpperStr(desc, "RECEIVE MONEY BY FPS")) {
      tran.setIs_invalid_trans(true);
    }

    // Check 銀通櫃員機提款外币取款
    if (type.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)) {
      if (desc == null || desc.length() < 5) {
        desc_check_result = true;
      }
      if (desc.contains("CNY")) {
        desc_check_result = false;
        if (tran.getAmount_row_content().contains("CNY")) {
          desc_check_result = true;
        }
      }
    }
    String[] ss1 = {"related", "Receive money", "eversa", "FPS"};
    if (tran.getType().contains(Global.RECEIVE_MONEY_STRING)) {
      if (Common.is_one_of_shuzu_item_in_line(tran.getDescription(), ss1)) {
      } else {
        desc_check_result = false;
      }
    }
    if (tran.getType().contains(Global.DEPOSIT_INTEREST_STRING)) {
      if (!(tran.getDescription().contains("Deposit Interest")
          || tran.getDescription().contains("Deposit interest"))) {
        desc_check_result = false;
      }
    }

    String[] xianjinhuizeng_wanna_shuzu = {
      "WeLab Debit Card rebate",
      "18 District Campaign",
      "18 Districts Campaign",
      "Cash for all",
      "Referral Campaign Reward",
      "Campaign",
      "WeLab Bank rebate",
      "Interest Rate Boost",
      "Bring-a-friend",
      "Account Opening Reward",
      "Test code",
      "Bakery",
      "Rebate",
      "Spring Seasonal"
    };
    if (tran.getType().contains(Global.CASH_REBATE_STRING)) {
      if (!Common.is_one_of_shuzu_item_in_line(tran.getDescription(), xianjinhuizeng_wanna_shuzu)) {
        desc_check_result = false;
      }
    }
    if (tran.getType().contains(Global.ADD_MONEY_STRING)) {
      System.out.println(
          e.getFile_name()
              + ": "
              + tran.getRef()
              + " "
              + content_list[index - 1]
              + " "
              + " "
              + content_list[index]
              + tran.getDescription());
    }

    if (type.contains(Global.DEBIT_CARD_STRING)) {
      String tran_date =
          Common.get_date_string(tran.getYear(), tran.getMonth(), tran.getDay_string());
      String settlement_date_1 = Common.get_settlement_date_string(e.getYEAR(), e.getMonth());
      int diff_days = Common.get_days_diffs(tran_date, settlement_date_1, e, tran);
      if (diff_days >= 90) {
        if (!tran.getDescription().contains("#")) {
          desc_check_result = false;
        }
      } else {
        if (tran.getDescription().contains("#")) {
          desc_check_result = false;
        }
      }
    }
    if (desc_check_result) {
      // if(!Common.check_description(desc)) desc_check_result=false;
    }
    tran.setCheck_desc(desc_check_result);
    return tran;
  }

  public Estatement get_core_account_transactions(String content, Estatement estatement) {
    boolean core_account_flag = false;
    double coreaccount_total_calculate = 0;
    String[] coreaccount_want_find_shuzu_1 = {
      estatement.getMonth(), String.valueOf(estatement.getYEAR()), "."
    };
    String[] coreaccount_want_find_shuzu_2 = {
      estatement.getLast_Month(), String.valueOf(estatement.getLast_months_YEAR()), "."
    };
    String[] coreaccount_want_find_shuzu_3 = {
      estatement.getNext_Month(), "20", ".", String.valueOf(estatement.getYEAR())
    };
    String[] coreaccount_want_find_shuzu_for_long_before = {"20", "."};
    String[] donot_want_find_shuzu = {"Statement", "Financial", "APPLE.COM"};
    String[] number_shuzu = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    String[] add_want_find_shuzu = {"Receive money", "Send money", "Ref"};

    ArrayList<Transactions> trans_list = new ArrayList<>();
    int days = Common.get_this_month_days(estatement.getYEAR(), estatement.getMonth());
    String[] content_list = content.split("\n");
    boolean is_have_intetest = false;
    int trans_id = 0;
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      if (line.contains(Global.CURRENTLY_HELD_STRING)) {
        break;
      }
      // 1 Jan 2021 承上結餘 Balance From Previous Statement -
      if (line.contains(Global.BALANCE_FROM_PREVIOUS_STATEMENT_STRING)) {
        String v = Common.get_string_value_from_string_with_start(line, "Previous Statement");
        estatement.setLast_month_amount(Common.string_move_special(v));
        if (!(Common.string_move_special(v).equals("-"))) {
          coreaccount_total_calculate += Common.string_to_double(Common.string_move_special(v));
        }
        core_account_flag = true;
      }
      if (line.contains(Global.CLOSING_BALANCE_STRING)) {
        String v = Common.get_string_value_from_string_with_start(line, "Closing Balance");
        estatement.setCoreaccount_total_capture(Common.string_move_special(v));
        break;
      }

      // Deposit interest 存款利息
      if (line.contains(Global.DEPOSIT_INTEREST_STRING)) {
        is_have_intetest = true;
        String next_line = content_list[i + 1];
        if (next_line.contains("" + days)
            && next_line.contains(estatement.getMonth())
            && next_line.contains(".")) {
          estatement.setInterest_capture(Common.get_double_value(next_line, -1));
        }
      }

      if (!core_account_flag) {
        continue;
      }
      if (line.contains(Global.DEBIT_CARD_SPENDING_STRING)) {
        continue;
      } // 这一行没有金额，所有不抓取
      // 27 Jan 2021 取消賬戶 Account Closed
      if (line.contains(Global.ACCOUNT_CLOSED_STRING)) {
        estatement.setIs_account_close(true);
        continue;
      }

      if (Common.is_all_of_shuzu_item_in_line(line, coreaccount_want_find_shuzu_1)
          && Common.is_all_of_shuzu_item_not_in_line(line, donot_want_find_shuzu)) {
        trans_id += 1;
        trans_list = get_Transactions(content_list, i, trans_list, estatement, trans_id);
        coreaccount_total_calculate += Common.get_double_value(line, -1);
      } else if (Common.is_all_of_shuzu_item_in_line(line, coreaccount_want_find_shuzu_2)
          && Common.is_all_of_shuzu_item_not_in_line(line, donot_want_find_shuzu)) {
        // System.out.println(line);
        trans_id += 1;
        trans_list = get_Transactions(content_list, i, trans_list, estatement, trans_id);
        coreaccount_total_calculate += Common.get_double_value(line, -1);
      } else if (Common.is_all_of_shuzu_item_in_line(line, coreaccount_want_find_shuzu_3)
          && Common.is_all_of_shuzu_item_not_in_line(line, donot_want_find_shuzu)) {
        // 检查下个月的记录
        trans_id += 1;
        trans_list = get_Transactions(content_list, i, trans_list, estatement, trans_id);
        estatement.setIs_have_next_month_tran(false);
        coreaccount_total_calculate += Common.get_double_value(line, -1);
      } else if (Common.is_all_of_shuzu_item_in_line(
              line, coreaccount_want_find_shuzu_for_long_before)
          && Common.is_all_of_shuzu_item_not_in_line(line, donot_want_find_shuzu)
          && Common.is_one_of_shuzu_item_in_line(line, Common.month_list)) {
        // 检查很久之前的记录
        trans_id += 1;
        trans_list = get_Transactions(content_list, i, trans_list, estatement, trans_id);
        coreaccount_total_calculate += Common.get_double_value(line, -1);
      } else if (Common.is_one_of_shuzu_item_in_line(line, add_want_find_shuzu)
          && line.contains(".")
          && Common.is_one_of_shuzu_item_in_line(line, number_shuzu)
          && Common.is_all_of_shuzu_item_not_in_line(line, donot_want_find_shuzu)) {
        // System.out.println(line);
        String amout = Common.get_value_string_from_line(line, -1);
        if (Common.is_valued_number(amout)) {
          // 特殊的场景
          // Receive money Ref: FT2W10A5I6 MH5IN92ENRVA 20,000.00
          // Send money Ref: FT21033WQJ6T -1,000.00
          // Receive money ReIfN: FGT2105602TZV 0.10
          // Debit Card spending Ref: FT21159X9QBG 168.00
          // System.out.println(line);
          // System.out.println(Common.get_double_value(line, -1));
          coreaccount_total_calculate += Common.get_double_value(line, -1);
          trans_id += 1;
          trans_list = get_Transactions_spec(content_list, i, trans_list, estatement, trans_id);
        }
      }
    }

    // 添加核心账户的所有转账记录
    estatement.setTransations_history(trans_list);

    estatement = check_interest(trans_list, estatement);
    double interest_add = Common.compare_interest(estatement);
    if ((!is_have_intetest) && estatement.getInterest() >= 0.02) {
      System.out.println(
          estatement.getCustomer_id()
              + "\t interest error, need show interest: "
              + estatement.getInterest());
      estatement.setInterest_diff(interest_add);
      estatement.setIs_check_interest(false);
    }

    // 检测Ref的开头是否正确
    estatement = check_Ref_Start_flag(trans_list, estatement);
    // 检测invalid_transations
    // 必须放在Set_fail_Ref之前，因为后面需要用到这一块的数据
    estatement = check_invalid_transations(trans_list, estatement);
    // 检查desc是否正确
    estatement = Set_fail_Ref(estatement, trans_list);
    // 付款,Debit Card 消費,存入GoSave, 銀通櫃員機提款 , 海外櫃員機提款   的金额需要带有“-”    Send money to
    // 收款的金额不能带有“-”    Receive money from
    estatement = check_single_for_amount(trans_list, estatement);
    // 计算金额保留两位小数
    coreaccount_total_calculate = Common.get_double_round(coreaccount_total_calculate);
    estatement.setCoreaccount_total_calculate(coreaccount_total_calculate + "");
    // 对比计算结果是否相等
    if (Common.compare_amount_string_to_double(
        estatement.getCoreaccount_total_calculate(), estatement.getCoreaccount_total_capture())) {
      if (Common.compare_amount_string_to_double(
          estatement.getCoreaccount_total_calculate(),
          estatement.getCoreaccount_total_capture_title())) {
        estatement.setCore_account_result(true);
      }
    } else {
      estatement.setCore_account_result(false);
    }
    return estatement;
  }

  public Estatement check_invalid_transations(
      ArrayList<Transactions> trans_list, Estatement estatement) {
    ArrayList<String> fail_invalid_ref_list = new ArrayList<>();
    for (int i = 0; i < trans_list.size(); i++) {
      if (!trans_list.get(i).isIs_invalid_trans()) {
        continue;
      }
      if (!compare_invalid_trans(trans_list.get(i), trans_list, estatement)) {
        if (!Spec_REF_list.contains(trans_list.get(i).getRef().trim())) {
          fail_invalid_ref_list.add(trans_list.get(i).getRef());
        }
      }
    }
    estatement.setFail_trans_Ref_list_for_Invalid_trans(fail_invalid_ref_list);
    return estatement;
  }

  public boolean compare_invalid_trans(
      Transactions tran, ArrayList<Transactions> trans_list, Estatement e) {
    boolean r = false;
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions t = trans_list.get(i);
      if (!t.isIs_invalid_trans()) continue;
      if (compare_invalid_trans_amount(tran.getAmount(), t.getAmount())) {
        if (tran.getRef().contains(t.getRef())) {
          // System.out.println(e.getCustomer_id()+": "+tran.getRef());
          r = true;
        } else { // 针对手动操作invalid trans
        }
      }
    }
    return r;
  }

  public boolean compare_invalid_trans_amount(String amount1, String amount2) {
    boolean r = false;
    if (amount1.contains("-") && amount2.contains("-")) {
      return false;
    } else if ((!amount1.contains("-")) && amount2.contains("-")) {
      r = Common.compare_absolute_amount(amount1, amount2);
    } else if (amount1.contains("-") && (!amount2.contains("-"))) {
      r = Common.compare_absolute_amount(amount1, amount2);
    } else if ((!amount1.contains("-")) && (!amount2.contains("-"))) {
      return false;
    } else {
      return false;
    }
    return r;
  }

  private Estatement Set_fail_Ref(Estatement estatement, ArrayList<Transactions> trans_list) {
    ArrayList<String> gosave_amount_list = new ArrayList<>();
    ArrayList<String> fail_desc_Ref_list = new ArrayList<>();
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions tran = trans_list.get(i);
      if (trans_list.get(i).isIs_goSave_trans()) {
        gosave_amount_list.add(trans_list.get(i).getAmount());
      }
      if (tran.getType().contains(Global.SEND_MONEY_STRING)) {
        if ((tran.getAmount().contains("-"))) {
          if ((tran.getDescription().contains("Send money to"))) { // Send money to **  名字全部被注释
            String name =
                Common.get_string_value_from_string_with_start(
                    tran.getDescription(), "Send money to");
            name = Common.string_move_special(name);
            name = name.replace("*", "");
            if (name.length() < 1) {
              fail_desc_Ref_list.add(tran.getRef());
            }
          }
        }
      }
      if (!(trans_list.get(i).isCheck_desc())) { // 如果desc的结果的为假，则添加对应的ref到fail list中
        String REF = trans_list.get(i).getRef();
        if (!(REF.contains("NONE"))) {
          fail_desc_Ref_list.add(REF);
        }
      }
      if (!trans_list.get(i).isCheck_foreign_currency_Amount()) {
        fail_desc_Ref_list.add(trans_list.get(i).getRef());
      }
    }
    estatement.setFail_trans_Ref_list_for_desc(fail_desc_Ref_list);
    estatement.setGoSave_Amount_list_in_coreaccount_trans_history(gosave_amount_list);
    return estatement;
  }

  public Estatement check_interest(ArrayList<Transactions> trans_list, Estatement e) {
    int days = Common.get_this_month_days(e.getYEAR(), e.getMonth());
    int y = Common.get_year_days(e.getYEAR());
    double last_amount = 0.0;
    String chengshangyue = e.getLast_month_amount();
    if (chengshangyue.equals("-")) {
      last_amount = last_amount + get_last_month_records_amount(trans_list, e);
    } else {
      last_amount =
          Common.string_to_double(e.getLast_month_amount())
              + get_last_month_records_amount(trans_list, e);
    }
    ArrayList<Double> each_day_amount_list = new ArrayList<>();
    ArrayList<Double> current_held_each_day_amount_list = new ArrayList<>();
    for (int i = 1; i <= days; i++) {
      each_day_amount_list.add(get_amount_each_day(trans_list, i, e));
    }
    for (int i = 1; i <= days; i++) {
      double amount = last_amount;
      for (int j = 0; j < each_day_amount_list.size(); j++) {
        if (j > (i - 1)) break;
        amount += each_day_amount_list.get(j);
      }
      current_held_each_day_amount_list.add(amount);
    }
    double total1 = 0.0;
    for (int i = 0; i < current_held_each_day_amount_list.size(); i++) {
      total1 += current_held_each_day_amount_list.get(i);
    }
    double total_interest = total1 * 0.001 / y;

    if (total_interest >= 0.01) {
      e.setInterest(Common.get_double_round(total_interest));
    } else {
      e.setInterest(0.0);
    }
    return e;
  }

  public double interest(double a, int year_days) {
    // 'Amount = Sum of (daily account balance x (0.1%/total days of the year)) within the reporting
    // month
    return Common.get_double_round(a * 0.001 / year_days);
  }

  public double get_amount_each_day(ArrayList<Transactions> trans_list, int d, Estatement e) {
    double amount = 0.0;
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions tran = trans_list.get(i);
      int day = tran.getDay();
      String month = tran.getMonth();
      if (d == day && e.getMonth().equals(month)) {
        amount += Common.string_to_double(tran.getAmount());
      } else {
        continue;
      }
    }
    return Common.get_double_round(amount);
  }

  public double get_last_month_records_amount(ArrayList<Transactions> trans_list, Estatement e) {
    String last_Month = e.getLast_Month();
    double amount = 0.0;
    for (int i = 0; i < trans_list.size(); i++) {
      Transactions tran = trans_list.get(i);
      if (tran.getMonth().equals(last_Month)) {
        amount += Common.string_to_double(tran.getAmount());
      }
    }
    return amount;
  }
}
