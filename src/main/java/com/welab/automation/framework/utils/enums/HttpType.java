package com.welab.automation.framework.utils.enums;

public enum HttpType {
    POST("post"),
    PATCH("patch"),
    GET("get"),
    PUT("put"),
    DELETE("delete");


    private String value;

    HttpType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
