package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.Loans;

import java.util.ArrayList;

public class LoansUtils {

  public Estatement get_loans_transactions(String content, Estatement estatement) {
    boolean loans_flag = false;
    boolean gosave_start_flag = false;
    boolean is_have_loans = false;

    // 200,000.00 8 Jan 2021 12 Dec 2022 AA21008T0947 -183,439.99
    String[] want_find_shuzu = {",", "."};
    ArrayList<Loans> Loans_trans_history = new ArrayList<>();

    String[] content_list = content.split("\n");
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];

      // 目前持有 Currently Held
      if (line.contains(Global.CURRENTLY_HELD_STRING)) {
        gosave_start_flag = true;
      }
      if (line.contains(Global.PERSONAL_LOAN_STRING) && gosave_start_flag) {
        loans_flag = true;
      } else if (line.contains(Global.LOANS_STRING) && gosave_start_flag) {
        loans_flag = true;
      }
      if (line.contains(Global.IMPORTANT_NOTES)) {
        estatement = is_important_notes_new_page(content_list, i, estatement);
        break;
      }
      if (!loans_flag) {
        continue;
      }
      // 200,000.00 8 Jan 2021 12 Dec 2022 AA21008T0947 -183,439.99
      if (Common.is_all_of_shuzu_item_in_line(line, want_find_shuzu)) {
        is_have_loans = true;
        String[] loan_record = line.split(" ");
        Loans loan = new Loans();
        if (loan_record.length >= 9) {

          String current_loans_amount = Common.get_value_string_from_line(line, -1);
          String total_loans_amount = Common.get_value_string_from_line(line, -9);
          estatement.setIs_have_loan_record(is_have_loans);

          loan.setType(Common.string_move_special(content_list[i - 1]));
          loan.setTotal_Amount(total_loans_amount);
          loan.setCurrent_Amount(current_loans_amount);
          loan.setRef(Common.get_value_string_from_line(line, -2));

          loan.setStart_Day(Common.get_value_string_from_line(line, -8));
          loan.setStart_Month(Common.get_value_string_from_line(line, -7));
          loan.setStart_Year(Common.get_value_string_from_line(line, -6));
          loan.setEnd_Day(Common.get_value_string_from_line(line, -5));
          loan.setEnd_Month(Common.get_value_string_from_line(line, -4));
          loan.setEnd_Year(Common.get_value_string_from_line(line, -3));

          if (current_loans_amount.contains("-")) {
            loan.setIs_have_single_current_amount(true);
          } else {
            loan.setIs_have_single_current_amount(false);
          }
          String start_Date =
              Common.get_value_string_from_line(line, -8)
                  + " "
                  + Common.get_value_string_from_line(line, -7)
                  + " "
                  + Common.get_value_string_from_line(line, -6);
          loan.setStart_Date(start_Date);
          String end_Date =
              Common.get_value_string_from_line(line, -5)
                  + " "
                  + Common.get_value_string_from_line(line, -4)
                  + " "
                  + Common.get_value_string_from_line(line, -3);
          loan.setEnd_Date(end_Date);
        }

        if (loan_record.length <= 3) {
          String current_loans_amount = Common.get_value_string_from_line(line, -1);
          String total_loans_amount = Common.get_value_string_from_line(line, -3);
          estatement.setIs_have_loan_record(is_have_loans);

          loan.setType(Common.string_move_special(content_list[i - 1]));
          loan.setTotal_Amount(total_loans_amount);
          loan.setCurrent_Amount(current_loans_amount);
          loan.setRef(Common.get_value_string_from_line(line, -2));

          loan.setStart_Day(" ");
          loan.setStart_Month(" ");
          loan.setStart_Year(" ");
          loan.setEnd_Day(" ");
          loan.setEnd_Month(" ");
          loan.setEnd_Year(" ");
          loan.setStart_Date(" ");
          loan.setEnd_Date(" ");
          if (current_loans_amount.contains("-")) {
            loan.setIs_have_single_current_amount(true);
          } else {
            loan.setIs_have_single_current_amount(false);
          }
        }
        Loans_trans_history.add(loan);
      }
    }

    if (is_have_loans) {
      estatement.setLoans_trans_history(Loans_trans_history);
      estatement.setIs_have_loan_record(is_have_loans);
      estatement = check_Loans_record(Loans_trans_history, estatement);
      estatement = set_total_loans_calculate(estatement, Loans_trans_history);
      estatement = set_loans_fail_trans_Ref_for_amount_single(estatement, Loans_trans_history);
      if (Common.compare_amount_string_to_double(
          estatement.getLaon_total_calculate(), estatement.getLaon_total_capture_title())) {
        // if(estatement.getLaon_total_capture().equals(estatement.getLaon_total_capture_title())) {
        estatement.setLoans_result(true);
      } else {
        estatement.setLoans_result(false);
      }
    } else {
      estatement.setLoan_fail_trans_Ref_for_amount_single(new ArrayList<>());
      estatement.setIs_have_loan_record(is_have_loans);
      estatement.setLoan_fail_trans_Ref_for_record(new ArrayList<>());
      estatement.setLoans_result(true);
    }

    estatement = summary_all_of_ref_for_amount_single(estatement);
    return estatement;
  }

  public Estatement set_loans_fail_trans_Ref_for_amount_single(
      Estatement e, ArrayList<Loans> Loans_trans_history) {
    ArrayList<String> loans_ref_list_for_fail_amount_single = new ArrayList<>();
    if (e.isIs_have_loan_record()) {
      for (int i = 0; i < Loans_trans_history.size(); i++) {
        if (!Loans_trans_history.get(i).isIs_have_single_current_amount()) {
          loans_ref_list_for_fail_amount_single.add(Loans_trans_history.get(i).getRef());
        }
      }
      if (loans_ref_list_for_fail_amount_single.size() > 0) {
        e.setLoan_fail_trans_Ref_for_amount_single(loans_ref_list_for_fail_amount_single);
      } else {
        e.setLoan_fail_trans_Ref_for_amount_single(loans_ref_list_for_fail_amount_single);
      }
    }
    return e;
  }

  public Estatement summary_all_of_ref_for_amount_single(Estatement e) {
    ArrayList<String> all_of_ref_list_for_amount_single = e.getWrong_single_amount_of_ref_list();
    all_of_ref_list_for_amount_single.addAll(e.getGoSave_fail_trans_Ref_for_amount_single());
    all_of_ref_list_for_amount_single.addAll(e.getLoan_fail_trans_Ref_for_amount_single());
    e.setWrong_single_amount_of_ref_list(all_of_ref_list_for_amount_single);
    return e;
  }

  public Estatement set_total_loans_calculate(Estatement e, ArrayList<Loans> Loans_trans_history) {
    double total = 0.0;
    for (int i = 0; i < Loans_trans_history.size(); i++) {
      total += Common.string_to_double(Loans_trans_history.get(i).getCurrent_Amount());
    }
    e.setLaon_total_calculate(Common.double_to_String(total));
    return e;
  }

  public Estatement check_Loans_record(ArrayList<Loans> Loans_trans_history, Estatement e) {
    ArrayList<String> loan_fail_trans_Ref_for_record = new ArrayList<>();
    boolean r = true;
    for (int i = 0; i < Loans_trans_history.size(); i++) {
      Loans loan = Loans_trans_history.get(i);
      if (!(loan.getTotal_Amount().contains(".") && loan.getTotal_Amount().length() > 2)) {
        r = false;
      }
      if (!(loan.getType().length() > 2)) {
        r = false;
      }
      if (!(loan.getStart_Date().length() > 8)) {
        r = false;
      }
      if (!(loan.getEnd_Date().length() > 8)) {
        r = false;
      }
      if (!(loan.getRef().length() > 8)) {
        r = false;
      }
      if (!(loan.getCurrent_Amount().contains(".") && loan.getCurrent_Amount().length() > 2)) {
        r = false;
      }
      if (!r) {
        loan_fail_trans_Ref_for_record.add(loan.getRef());
        break;
      }
    }

    if (r) {
      e.setIs_right_loan_trans_record(true);

    } else {
      e.setIs_right_loan_trans_record(false);
    }
    e.setLoan_fail_trans_Ref_for_record(loan_fail_trans_Ref_for_record);
    return e;
  }

  public Estatement is_important_notes_new_page(
      String[] content_list, int index, Estatement estatement) {
    String line = content_list[index - 1];
    estatement.setIs_have_importan_notes(true);
    // 銀行代碼 Bank Code / 分行編號 Branch Code : 390 / 750
    if (line.contains(Global.BANK_CODE_STRING)
        && line.contains(Global.BRANCH_CODE_STRING)
        && line.contains("390")
        && line.contains("750")) {
      estatement.setIs_new_page_important_notes(true);
    } else {
      estatement.setIs_new_page_important_notes(false);
    }
    return estatement;
  }
}
