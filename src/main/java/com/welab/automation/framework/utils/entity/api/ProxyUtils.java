package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.enums.HttpType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;


public class ProxyUtils {
    private static Logger logger = LoggerFactory.getLogger(ProxyUtils.class);
    private static String host = "";
    public ProxyUtils(String host)
    {
        this.host =host;
    }
    public TestStep CreateTestStep(String path, String method, Map<String,Object> params,String body)
    {
        HttpType httpType=null;
        TestStep testStep = new TestStep();

//        HttpUtils httpUtils = new HttpUtils(host);
        switch (method.toUpperCase())
        {
            case "GET":
                httpType =HttpType.GET;
                break;
            case "PUT":
                httpType =HttpType.PUT;
                break;
            case "POST":
                httpType =HttpType.POST;
                break;
            case "DELETE":
                httpType =HttpType.DELETE;
                break;
            case "PATCH":
                httpType =HttpType.PATCH;
                break;
            default:
                logger.error("Please provide method");
                throw new RuntimeException(String.format("not support this function %s",
                        method.toUpperCase()));
        }
        testStep.setType(httpType);
        testStep.setPath(path);
        if(params==null)
            params = new HashMap<>();
        testStep.setParams(params);
        testStep.setBody(body);

        return testStep;
    }

    public TestStep createUplodaFileSteps(String path, String method,Map<String,Object> params,String filepath)
    {
        TestStep testStep = CreateTestStep(path,method,params,null);
        MultipartParameter multipartParameter= new MultipartParameter(true,"file");
        multipartParameter.setControlValue(filepath);
        testStep.multipartList.add(multipartParameter);
        testStep.setIsUploadFile(true);
        return testStep;
    }
    public Response SendRequest(String path, String method, Map<String,Object> params,String body,String filepath,boolean isUploadFile)
    {
        HttpUtils httpUtils = new HttpUtils(host);
        TestStep testStep = new TestStep();
        if(isUploadFile)
        {
            GlobalVar.HEADERS.put("Content-Type","multipart/form-data");
            testStep = createUplodaFileSteps(path, method,params,filepath);
        }
        else {
            GlobalVar.HEADERS.put("Content-Type","application/json;charset=utf-8");
            testStep = CreateTestStep(path, method, params, body);
        }
        Response response = httpUtils.request(testStep);
        return response;
    }

    public static void main(String[] args) throws Exception {
        GlobalVar.HEADERS.put("Authorization","Bearer 0vsbTctD4S8guX5+98WARQGXeJBsq8JvK2CXryGLFsHrumvGcJNGr6d1hDBC0nQ5lB41/0Wa3WNHBw8yCO10Cte7JKXSwvJ1gVvLckflEuAI73h8Y0BOM8FUWdkL4PfHuazcDrEdnqP2cUTuCE6bE4hhl3bR0tBVIqpU/mgkHLzz+gkZFgZx+OxySH31YPZhkSN54i5bzT6iYLEC+MY9R53X0qQXKFE9pH5a1YbirtuxH5r3H1VYm/WcFUuLEtMmhBrF+pSUeVzH+DMu0R9V8g90OFSXLxOcctkTXTdtOm/Io9S9Wn2bLa9c+Hle7siediC5MzebmFMQ9CPFBzhZ3Azq1OCy8KNIbrZENO6d0FLOCwbB5wNx2g1VjH6yPiYdaZoWR9ukt7Xbag0EumlxjPauEytpvbWa3FqwQPTWAyjb7yNKvKP6kQwZFc5k2J+146Yqd3HD3DvLpgWJfHiVfy5rzWHODAjwywml4PLc+cH/AOIarn4ar97Bdn36VOOnWTcz4Likx1QSdJqdJugxj7a6mW+zsQu0rJabrs6gP9cwiuu+n1znPh4L9TWgF3D4z63p8YuWSX3KLJ/hQdOC1HcPxLT3MZWAMLUcHVGvR8kZncQLA7NM20aN/uukBqJUaQsudCAem7hhmYyGiQY8Ab6cpmt5l7MkUSKC6+QHlolKudE1ePDvMM6IucHHPcsXol7cRa2sk3YoqO7MDOccUdV0RPO67VobCSnIXyQ7UNHydpw9d7hyZsI/DWztPbV41MhCR71eU5pbDTxL+iovBeKkv1DhkZ5yCPlJM3PWdQw02VPJQprISBMyL1VM6uSkxmwuoZ4gE6HLYuCGUuXU5wSoJ20kfMh3QOtkyeN49xASFFFMunN2ESwLNI5m2r3zGpfuhxkCmsE26g4UTAE87Xc");
        String host = "https://mobilecloud.test.com";
        ProxyUtils proxyUtils = new ProxyUtils(host);
        Map<String,Object> params = new HashMap<>();
        String path="/automation/api/device/HT79W1A05739";
        Response response = proxyUtils.SendRequest(path,"GET",params,"","",false);

//        String path="/automation/api/v1/spaces/artifacts/0";
//        String filepath="src/test/resources/app/android/VodQA.apk";
//        GlobalVar.HEADERS.put("X-File-Name","VodQA.apk");
//        GlobalVar.HEADERS.put("X-Content-Type","application/zip");
//        GlobalVar.HEADERS.put("X-Alias","v1.0.0");
//        GlobalVar.HEADERS.put("Content-Type","multipart/form-data");
//        Response response = proxyUtils.SendRequest(path,"POST",params,"",filepath,true);
    }
}

