package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.GlobalVar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigUtils {

    private static final Logger logger = LoggerFactory.getLogger(ConfigUtils.class);
    private static Properties props;
    private static String filename_ = GlobalVar.PG_CONFIGFILENAME;

    synchronized static private void loadProps(String fileName) {
        logger.info("start load properties file {}",fileName);
        props = new Properties();
        InputStream in = null;
        try {
            in = ConfigUtils.class.getClassLoader().getResourceAsStream(fileName);
            props.load(in);
        } catch (FileNotFoundException e) {
            logger.error(fileName + "file not find");
        } catch (IOException e) {
            logger.error("throw IOException");
        } finally {
            try {
                if (null != in) {
                    in.close();
                }
            } catch (IOException e) {
                logger.error("jdbc.properties file stream close exception");
            }
        }
        logger.info("load properties file complete...........\n");
    }

    public static void setProperty(String fileName, String keyname, String keyvalue) {
        URL url = ConfigUtils.class.getResource("/" + fileName);
        if (null == props) {
            loadProps(fileName); //Must be loaded before the output stream is instantiated
        }
        OutputStream fos = null;
        try {
            // the default way to open a file is to overwrite, which means that once the following sentence is executed, the contents of the original file are cleared
            // so load loadProps(fileName) first;Otherwise the original data of the file will be lost
            fos = new FileOutputStream(url.getFile());

            props.setProperty(keyname, keyvalue);
            // in a format suitable for loading into the Properties table using the load method,
            // writes the list of Properties (key and element pairs) in this Properties table to the output stream
            props.store(fos, "Update '" + keyname + "' value");
        } catch (IOException e) {
            logger.error("update property file error:" + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getProperty(String fileName, String key, String defaultValue) {
        if (null == props) {
            loadProps(fileName);
        }
        return props.getProperty(key, defaultValue);
    }

    public static Properties getAll(String fileName) {
        logger.info("file name：" + fileName);
        logger.info("props:" + props);
        if (null == props) {
            loadProps(fileName);
        } else {
            if (!props.propertyNames().equals(fileName)) {
                loadProps(fileName);
            }
        }
        return props;
    }

    public static Map<String,String> getPropertiesToMap()
    {
        Map<String, String> propertiesMap = new HashMap<>();
        if (null == props) {
            loadProps(filename_);
        }
        Enumeration<String> propertyNames = (Enumeration<String>) props.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String propertyKey = propertyNames.nextElement();
            String propertyValue = props.get(propertyKey).toString();
            propertiesMap.put(propertyKey, propertyValue);
        }
        return propertiesMap;
    }
    /**
     * put  *.properties's all the properties into map
     *
     * @return
     */
    public static Map<String, Object> getAllProperties(String fileName) {
        loadProps(fileName);
        Map<String, Object> allPropertiesMap = new HashMap<>();
        Enumeration<String> propertyNames = (Enumeration<String>) props.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String propertyKey = propertyNames.nextElement();
            Object propertyValue = props.get(propertyKey);
            allPropertiesMap.put(propertyKey, propertyValue);
        }
        props = null;
        return allPropertiesMap;
    }

    public static String getApiKey() {
        return getProperty(filename_, "api_key", "instore-is123");
    }

    public static String getNonceStr() {
        return getProperty(filename_, "nonce_str", "test-is123");
    }

    public static String getSecret() {
        return getProperty(filename_, "secret", "563fe3b3b4b4483191923bb1d4c55863");
    }

    public static String getAzureAPIM() {
        return getProperty(filename_, "AzureAPIM", "true");
    }

    public static String getMacAddress() {
        return getProperty(filename_, "macAddress", "macAddress");
    }

    public static String getDeviceId() {
        return getProperty(filename_, "deviceId", "deviceId");
    }

    public static String getProperty(String key) {
        return getProperty(filename_, key, "");
    }

    public static String getProperty(String key, String defaultValue) {
        return getProperty(filename_, key, defaultValue);
    }

    public static String getSubscriptionKey() {
        return getProperty(filename_, "subscriptionKey", "ac71fab6d89643b3b89196b2575d5167");
    }

    public static String getHostAddress() {
        return getProperty(filename_, "hostAddress", "localhost");
    }

    public static String getHostPort() {
        return getProperty(filename_, "hostPort", "hostPort");
    }

    public static String getCurrentProxy() {
        return getProperty(filename_, "host", "10.72.237.20:9023");
    }

    public static String getTokenSecret() {
        return getProperty(filename_, "tokensecret", "d206e2c06da944ca8bbff19dc2893d12");
    }

    /**
     * get CounterId
     *
     * @return
     */
    public static String getCounterId() {
        return getProperty(filename_, "CounterId", "2000263039");
    }


    /**
     * get CurrentVersion
     *
     * @return
     */
    public static String getCurrentVersion() {
        return getProperty(filename_, "CurrentVersion", "1.1.5.7");
    }

    /**
     * get UserAccount
     *
     * @return
     */
    public static String getUserAccount() {
        return getProperty(filename_, "UserAccount", "01537327");
    }

    /**
     * get CounterAccount
     *
     * @return
     */
    public static String getCounterAccount() {
        return getProperty(filename_, "CounterAccount", "2000263039");
    }

    /**
     * get UserPassword
     *
     * @return
     */
    public static String getUserPassword() {
        return getProperty(filename_, "UserPassword", "111111");
    }


    /**
     * get CounterPassword
     *
     * @return
     */
    public static String getCounterPassword() {
        return getProperty(filename_, "CounterPassword", "111111");
    }


    public static Boolean getNULLValueSwitch() {
        return Boolean.valueOf(getProperty(filename_, "NULLValueSwitch", "false"));
    }

    public static String getSwaggerUrl() {
        return getProperty(filename_, "swaggerUrl", "http://10.72.237.20:8023/swagger/docs/v1");
    }

}
