package com.welab.automation.framework.utils.entity.api;


import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.Headers;
import com.welab.automation.framework.utils.enums.HttpType;
import lombok.Data;
import org.apache.commons.collections.MapUtils;
import java.util.*;

@Data
public class TestStep {

    /**
     * 请求类型：post get
     */
    private HttpType type;
    /**
     * 接口
     */
    private String path;
    /**
     * 请求携带的参数，key、value格式
     */
    private Map<String, Object> params = new HashMap<>();

    /**
     * 消息体
     */
    private String body;

    private Object postBody;

    public Map<String, String> uploadFileMap = new HashMap<>();
    public List<MultipartParameter> multipartList = new ArrayList<>();
    public Map<String, Object> formParamMap = new HashMap<>();

    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> cookies = new HashMap<>();


    /**
     * it's used to store original global heads
     */
    Map<String,String> originalHeadMap = new HashMap<>();

    /**
     * it's used to store new header
     */
    Map<String,String> newAddedValueMap = new HashMap<>();

    private Boolean isUploadFile  = false;



    public Object getPostBody() {
        return postBody;
    }

    public void setPostBody(Object postBody) {
        this.postBody = postBody;
    }

    /**
     *  post body
     */
    private Map<String, Object> mapbody = new HashMap<>();

    public Map<String, Object> getMapbody() {
        return mapbody;
    }

    public void setMapbody(Map<String, Object> mapbody) {
        this.mapbody = mapbody;
    }
    public HttpType getType() {
        return type;
    }

    public void setType(HttpType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getParamsPath()
    {
        String paramsPath="";
        for(String key : params.keySet())
            paramsPath +=   key +  (params.get(key) == null ? "" : "=".concat(params.get(key).toString())) + "&";
        return "?" +paramsPath;
    }
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public boolean isUploadFile() {
        return isUploadFile;
    }

    public void setUploadFile(boolean uploadFile) {
        isUploadFile = uploadFile;
    }

    public void setHeaderByAnnotation(Headers headerByAnnotation) {
        String[] keys = headerByAnnotation.key();
        String[] values = headerByAnnotation.value();
        if (keys.length != 0 && values.length !=0 ) {
            for (int index = 0; index < keys.length; index++) {
                if (values[index].equalsIgnoreCase("multipart/form-data")) {
                    setIsUploadFile(true);
                }
                this.headers.put(keys[index], values[index]);
            }
        }
    }
    /**
     * update global header
     */
    public void updateGlobalHeader() {
        Iterator<String> keyIterator = this.headers.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            String newValue = this.headers.get(key);
            if(GlobalVar.HEADERS.containsKey(key)) {
                originalHeadMap.put(key, GlobalVar.HEADERS.get(key));
                GlobalVar.HEADERS.put(key,newValue);
            } else {
                newAddedValueMap.put(key, newValue);
                GlobalVar.HEADERS.put(key,newValue);
            }
        }
    }

    /**
     * restore global header
     */
    public void restoreGlobalHeader() {
        if (!MapUtils.isEmpty(this.originalHeadMap)) {
            GlobalVar.HEADERS.putAll(this.originalHeadMap);
            this.originalHeadMap.clear();
        }
        if (!MapUtils.isEmpty(this.newAddedValueMap)) {
            this.newAddedValueMap.forEach((key,value) -> {
               GlobalVar.HEADERS.remove(key);
            });
            this.newAddedValueMap.clear();
        }
    }


    @Override
    public String toString() {
        return "type=" + type +
                " , path=" + path;
    }
}
