package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.GoSave;
import com.welab.automation.framework.utils.entity.estatement.Transactions;

import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {
	public static final String[] month_list= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	public static final String fail_refs_fileName= "/Result_fail_Refs.xlsx";

	public static String get_month_number(String M) {
		for(int i=0;i<month_list.length;i++) {
			if(month_list[i].equals(M)) {
				int index = i+1;
				if(index>=10) {
					return index+"";
				}else {
					return "0"+index;
				}
			}
		}
		return "NONE";
	}
	
	public static String get_today_date_string() {
		//2021-07-27
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date).trim();
	}
	
	public static String get_settlement_date_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		String s = formatter.format(date);
		s= s.substring(0,s.length()-2);
		s = s+"01";
		return s;
	}
	
	
	public static String get_settlement_date_string(int Y,String M) {
		String m = get_month_number(M);
		int days= Common.get_this_month_days(Y, M);
		return Y+"-"+m.trim()+"-"+days;
	}
	
	public static String get_date_string(String Y,String M, String D) {
		String m = get_month_number(M);
		return Y.trim()+"-"+m.trim()+"-"+D.trim();
	}
	
	
	public static int  get_days_diffs(String date_star, String date_end, Estatement estatement, Transactions tran) {
		int i=0;
		DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date star = dft.parse(date_star);//开始时间
			Date endDay=dft.parse(date_end);//结束时间
			Date nextDay=star;
			while(nextDay.before(endDay)){//当明天不在结束时间之前是终止循环
				Calendar cld = Calendar.getInstance();
				cld.setTime(star);
				cld.add(Calendar.DATE, 1);
				star = cld.getTime();
				//获得下一天日期字符串
				nextDay = star; 
				i++;
			}
			//System.out.println("相差天数为："+i);
			
		} catch (Exception e) {
			//System.out.println(estatement.getCustomer_id()+": 日期解析异常" +tran.getRef());
			e.printStackTrace();
		}
		return i;
	}
	
	
	public static int get_year_days(int year) {
		if(year%400==0) {
			return 366;
		}else {
			if(year%4==0&&year%100==0) {
				return 365;
			}else if(year%4==0&&year%100!=0) {
				return 366;
			}else {
				return 365;
			}
		}
	}
	public static int get_this_month_days(int year,String Month) {
		String[] month_list_1= {"Jan","Mar","May","Jul","Aug","Oct","Dec"};
		String[] month_list_2= {"Apr","Jun","Sep","Nov"};
		if(is_one_of_shuzu_item_in_line(Month, month_list_1)) {
			return 31;
		}else if(is_one_of_shuzu_item_in_line(Month, month_list_2)) {
			return 30;
		}else if(Month.contains("Feb")) {
			if(year%400==0) {
				return 29;
			}else {
				if(year%4==0&&year%100==0) {
					return 28;
				}else if(year%4==0&&year%100!=0) {
					return 29;
				}else {
					return 28;
				}
			}
		}else {
			return -1;
		}
	}

	public static float get_float_round(float float_value) {
		return (float)(Math.round(float_value*100))/100;
	}
	public static boolean is_have_digit(String s) {
		boolean flag = false;
		//		Pattern p1 = Pattern.compile("[0-9]*");  //适用于纯数字判断
		Pattern p = Pattern.compile(".*\\d+.*");
		Matcher m = p.matcher(s);
		if(m.matches()) {
			flag=true;
		}
		return flag;
	}


	public static Transactions get_Date_String_from_line(String line,Estatement e,Transactions tran) {
		tran.setDate(tran.getDay_string()+" "+tran.getMonth()+" "+tran.getYear());
		return tran;
	}

	public static GoSave get_gosave_Date_from_line(String line, Estatement e, GoSave goSave) {
		String y1 = e.getYEAR()+"";
		String y2 = e.getLast_months_YEAR()+"";
		String value="";
		if(line.contains(y1)) {
			value=line.substring(0, line.indexOf(y1)+y1.length());
		}else {
			value=line.substring(0, line.indexOf(y2)+y2.length());
		}

		value =value.trim();
		String year= get_value_string_from_line(value, -1);
		String Month= get_value_string_from_line(value, -2);
		goSave.setGosave_trans_Date(value);
		goSave.setYear(year);
		goSave.setMonth(Month);

		String ref = value+" GoSave ID: "+goSave.getID();
		goSave.setGoSave_Trans_Ref(ref);

		return goSave;
	}

	public static double get_double_round(double float_value) {
		return (double)(Math.round(float_value*100))/100;
	}
	
	public static double compare_interest(Estatement e) {
		double i = e.getInterest() -e.getInterest_capture();
		i = get_double_round(Math.abs(i));
		return i;
	}

	public static boolean compare_absolute_amount(String amount1,String amount2) {
		boolean r = false;
		String s1 = remove_for_fushu(amount1);
		String s2 = remove_for_fushu(amount2);
		if(compare_amount_string_to_double(s1, s2)) {
			r=true;
		}
		return r;
	}

	public static String remove_for_fushu(String amount) {
		if(amount.contains("-")) {
			int index = amount.indexOf("-");
			if(index==0){
				return amount.substring(1);
			}else {
				return amount;
			}
		}else {
			return amount;
		}
	}

	public static ArrayList<String> Compate_list(Estatement estatement,ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> result = new ArrayList<>();
		for(int i =0;i<estatement.getTransations_history().size();i++) {
			if(estatement.getTransations_history().get(i).isIs_goSave_trans()) {
				String amout = estatement.getTransations_history().get(i).getAmount();
				String date = estatement.getTransations_history().get(i).getDate();
				String ref = estatement.getTransations_history().get(i).getRef();
				boolean this_trans_can_be_found =false;
				for(int j=0;j<gosave_trans_list.size();j++) {
					GoSave g = gosave_trans_list.get(j);
					if(g.getGosave_trans_Date().equals(date)) {
						String gosave_amount= g.getAmount();
						if(compare_absolute_amount(amout,gosave_amount)) {
							this_trans_can_be_found =true;
						}
					}
				}
				if(!this_trans_can_be_found) {
					if(!(ref.contains("NONE"))) {
						result.add(ref);
					}
				}
			}
		}
		return result;
	}


	public static ArrayList<String> Compate_list_according_gosave_tran(Estatement estatement,ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> result = new ArrayList<>();
		for(int i=0;i<gosave_trans_list.size();i++) {
			GoSave gosave = gosave_trans_list.get(i);
			//String Gosave_ID = gosave_trans_list.get(i).getID();
			String amout = gosave_trans_list.get(i).getAmount();
			String date = gosave_trans_list.get(i).getGosave_trans_Date();
			boolean is_this_gosave_trans_can_be_found_in_coreaccount =false;
			for(int j =0;j<estatement.getTransations_history().size();j++) {
				if(estatement.getTransations_history().get(j).isIs_goSave_trans()) {
					String core_amout = estatement.getTransations_history().get(j).getAmount();
					String core_date = estatement.getTransations_history().get(j).getDate();
					if(core_date.equals(date)) {
						if(compare_absolute_amount(amout,core_amout)) {
							is_this_gosave_trans_can_be_found_in_coreaccount =true;
						}
					}
				}
			}
			if(!is_this_gosave_trans_can_be_found_in_coreaccount) {

				if(!result.contains(gosave.getGoSave_Trans_Ref())) {
					result.add(gosave.getGoSave_Trans_Ref());
				}
			}
		}
		return result;
	}

	public static String get_last_month(String Month) {
		int index =0;
		String  m = "";
		if("Jan".equals(Month)) {
			m = "Dec";
		}else {
			for(int i=0;i<month_list.length;i++) {
				if(Month.equals(month_list[i])) {
					index=i;
					m=month_list[index-1];
					break;
				}
			}
		}
		return m;
	}

	public static String get_next_month(String Month) {
		int index =0;
		String  m = "";
		if("Dec".equals(Month)) {
			m = "Jan";
		}else {
			for(int i=0;i<month_list.length;i++) {
				if(Month.equals(month_list[i])) {
					index=i;
					m=month_list[index+1];
					break;
				}
			}
		}
		return m;
	}

	public static String get_time_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}

	public static String get_date_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}

	public static double get_double_value(String line,int index) {
		String v = get_value_string_from_line(line, index);
		if(!Common.is_valued_number(v)) return 0.0;
		return string_to_double(v);
	}

	public static int get_days_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[0];
		value = string_move_special(value);
		return Integer.parseInt(value);
	}
	public static String get_days_String_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[0];
		value = string_move_special(value);
		return value;
	}
	public static String get_month_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[1];
		return string_move_special(value);
	}
	public static String get_year_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[2];
		return string_move_special(value);
	}
	public static String get_value_string_from_line(String line,int index) {
		String[] ss=line.split(" ");
		String value = ss[ss.length+index];
		value = string_move_special(value);
		return value.trim();
	}

	public static boolean is_amount_zero(String amount1) {
		if(!is_valued_number(amount1)) {
			return false;
		}
		if(amount1==null) return false;
		if(string_to_double(amount1)==0.0) return true;
		return false;
	}

	public static boolean compare_amount_string_to_double(String amount1,String amount2) {
		if(!(is_valued_number(amount1)&&is_valued_number(amount2))) {
			return false;
		}
		if(amount1==null) return false;
		if(amount2==null) return false;
		boolean re = false;
		double a1 = string_to_double(amount1);
		double a2 = string_to_double(amount2);
		if(a1==a2) {
			re =true;
		}
		return re;
	}

	public static String string_move_special(String value) {
		String s = value.trim();
		s= s.replace("\n", "");
		s= s.replace("\t", "");
		s= s.replace("\r", "");
		s= s.replace(",", "");
		s= s.replace(" ", "");
		return s;
	}

	public static String get_string_value_from_string_with_start_and_end(String value,String start,String end) {
		int index_start=value.indexOf(start);
		int index_end=value.indexOf(end);
		String v = value.substring(index_start+start.length(),index_end);
		return v.trim();
	}

	public static String get_string_value_from_string_with_start(String value,String start) {
		int index_start=value.indexOf(start);
		String v = value.substring(index_start+start.length());
		return v.trim();
	}

	public static String get_Ref_for_spec_trans(String[] content_list,int index,String flag) {
		//String flag ="Ref:"
		String line1 = content_list[index+1];
		String line2 = content_list[index];
		String line3 = content_list[index+2];
		String ref="NONE";
		if(line1.contains(flag)) {
			//System.out.println("KKKK:"+line1);
			ref = process_ref_line(get_string_value_from_string_with_start(line1,flag));
		}else if(line2.contains(flag)){
			String v = get_string_value_from_string_with_start(line2,flag);
			if(v.contains(" ")) {
				String[] ss = v.split(" ");
				for(String s:ss) {
					if(s.trim().startsWith("FT")) {
						return s.trim();
					}
					if(s.trim().startsWith("AAAC")) {
						return s.trim();
					}
				}
				int myindex =v.lastIndexOf(" ");
				return v.substring(0,myindex).trim();
			}else {
				return v;
			}
			//return process_ref_line(get_string_value_from_string_with_start(line2,flag));
		}else if(line3.contains(flag)){
			return process_ref_line(get_string_value_from_string_with_start(line3,flag));
		}
		return ref;
	}
	
	public static String process_ref_line(String ref_line) {
		ref_line = ref_line.trim();
		if(ref_line.contains(" ")) {
			String[] ss = ref_line.split(" ");
			for(String s:ss) {
				if(s.trim().startsWith("FT")) {
					return s.trim();
				}
				if(s.trim().startsWith("AAAC")) {
					return s.trim();
				}
			}
			return ss[0].trim();
		}else {
			return ref_line;
		}
	}
		

	public static boolean is_valued_number(String value) {
		if(value==null) {
			return true;
		}
		if(!(value.contains("."))) {
			return false;
		}
		if(value.contains("-")) {
			int index = value.indexOf("-");
			if (index!=0){
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("null")
	public static double string_to_double(String value) {
		if(value==null) {
			return (Double) null;
		}
		double a=Double.parseDouble(value);
		return a;
	}

	public static String double_to_String(double value) {
		return get_double_round(value)+"";
	}

	public static boolean is_one_of_shuzu_item_in_line(String line,String[] list) {
		boolean value =false;
		for(int i=0;i<list.length;i++) {
			if(line.contains(list[i])) {
				return true;
			}
		}
		return value;
	}

	public static boolean compareUpperStr(String s1,String s2) {
		s1 = s1.toUpperCase();
		s2 = s2.toUpperCase();
		if(s1.contains(s2)) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean is_all_of_shuzu_item_not_in_line(String line,String[] list) {
		for(int i=0;i<list.length;i++) {
			if(line.contains(list[i])) {
				return false;
			}
		}
		return true;
	}

	public static boolean is_all_of_shuzu_item_in_line(String line,String[] list) {
		for(int i=0;i<list.length;i++) {
			if(!(line.contains(list[i]))) {
				return false;
			}
		}
		return true;
	}

	public static String save_result_to_txt (String string,String path) {
		String time_string=get_time_string();
		String filepath=path+"/eStatement_result_"+time_string+".txt";
		try {
			File file = new File(filepath);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filepath;
	}



	
	static ArrayList<String> read_list_from_txt(String path)  {
		ArrayList<String> list= new ArrayList<>();
		try {
			String project_path =System.getProperty("user.dir");
			File file = new File(project_path+path);
			if (!file.exists()) {
				//file.createNewFile();
				System.out.println("The file is not exist, create the file, but the file is empty");
				list.add("NNNNNNNNNNNNNN");
				return list;
			}
			//InputStream得到字节输入流
			InputStream instream = new FileInputStream(file);
			if(instream.toString().isEmpty()) {
				System.out.println("The file is empty");
			}

			if (instream != null) {
				//InputStreamReader 得到字节流
				InputStreamReader inputreader = new InputStreamReader(instream);
				@SuppressWarnings("resource")
				//BufferedReader 是把字节流读取成字符流
				BufferedReader buffreader = new BufferedReader(inputreader);
				String line="";// 分行读取
				while ((line = buffreader.readLine()) != null) {
					list.add(line.trim());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	//銀通櫃員機提款 RATE: CNY 100 : HKD 121.26
	//10 Aug 2021 -CNY 400 -121.26
	public static String get_foreign_currency_Amount_from_line(String line) {
		String value="";
		String flag="CNY";
		int index = 0;
		if(line.contains(flag)) {
			String[] ss = line.split(" ");
			for (int i = 0; i < ss.length; i++) {
				if(ss[i].contains(flag)) {
					index = i+1;
					break;
				}
			}
			return ss[index].trim();
		}
		return value;
	}
	
	public static boolean check_foreign_currency_Amount_for_trans(String[] content_list,int index) {
		boolean result= true;
		String v1= get_foreign_currency_Amount_from_line(content_list[index-1]);
		String v2= get_foreign_currency_Amount_from_line(content_list[index]);
		if(v1.contains(":")) {
			v1 = v1.replace(":", "");
		}
		if(v2.contains(":")) {
			v2 = v2.replace(":", "");
		}
		result = v1.equals(v2);
		if(!result) {
			//System.out.println("v1: "+v1+"\tv2: "+v2+"\tcontent_list[index]");
		}
		return result;
	}
	
	public static boolean check_description(String desc) {
		if(desc.length()<1) {
			return true;
		}
		boolean result = true;
		String REGEX ="^[?A-Za-z0-9*&#!\"‘:,'()\'//%+-. ]+$";
		Pattern p = Pattern.compile(REGEX);
		Matcher m = p.matcher(desc); // 获取 matcher 对象
		result= m.matches();
		if(!result) {
			//System.out.println(desc+": "+result);
		}
		return result;

	}
	
	public static void cal_num(String a, String b) {
		BigDecimal b1 = new BigDecimal(a);
		BigDecimal b2 = new BigDecimal(b);
		BigDecimal b3 = b1.add(b2);
	}
	public static void cal_num_by_float(String a, String b) {
		Float b1 = Float.parseFloat(a);
		Float b2 = Float.parseFloat(b);
		Float b3 = b1+b2;
	}

	public static void cal_num_by(double d, double e) {
		double b3 = d+e;
	}
}
