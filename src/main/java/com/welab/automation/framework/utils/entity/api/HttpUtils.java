package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.TimeUtils;
import com.welab.automation.framework.utils.enums.HttpType;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.DecoderConfig;
import io.restassured.config.RedirectConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Map;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.path.json.config.JsonPathConfig.NumberReturnType.DOUBLE;

public class HttpUtils {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private RestAssuredConfig restAssuredConfig;
  private Response response;
  private String baseURL;
  private String proxyurl;
  private int proxyport;

  public HttpUtils() {}

  /** constructor */
  public HttpUtils(String url) {
    baseURL = url;

    //        proxyurl = "10.72.160.114";
    //        proxyport = 8083;
    //        RestAssured.proxy(proxyurl,proxyport);
    //        logger.info("proxy host is "+proxyurl+" port is "+proxyport);

    // 根据需要进行设置
    restAssuredConfig =
        config()
            .redirect(new RedirectConfig().allowCircularRedirects(false))
            .decoderConfig(new DecoderConfig().defaultContentCharset("UTF-8"))
            .jsonConfig(jsonConfig().numberReturnType(DOUBLE));
  }

  /**
   * Get the URL for this request, carrying the parameters
   *
   * @param path
   * @param params
   */
  private String getRequestInfo(String path, Map<String, Object> params) {
    StringBuilder stringBuilder = new StringBuilder();

    for (String key : params.keySet()) {
      stringBuilder.append(key).append("=").append(params.get(key)).append("&");
    }

    if (stringBuilder.length() >= 1 && stringBuilder.toString().endsWith("&")) {
      stringBuilder = new StringBuilder(stringBuilder.substring(0, stringBuilder.length() - 1));
    }

    return getRequestInfo(path) + "?" + stringBuilder;
  }

  /**
   * Gets the response information for this request
   *
   * @param path
   */
  private String getRequestInfo(String path) {
    return RestAssured.baseURI + path;
  }

  /**
   * 获取本次请求的响应信息
   *
   * @param response
   */
  @Step("show response message")
  private void logResponseInfo(Response response) {
    logger.debug("get status line {} ", response.getStatusLine());
    Allure.addAttachment("response line", response.getStatusLine());
    String responseStr = response.asString();

    Integer truncateLength =
        GlobalVar.GLOBAL_VARIABLES.get("responseDataTruncatureLength") != null
            ? Integer.valueOf(
                GlobalVar.GLOBAL_VARIABLES.get("responseDataTruncatureLength").toString())
            : 10000;
    if (responseStr.length() > truncateLength) {
      logger.info(responseStr.substring(0, truncateLength));
    } else {
      logger.info(response.prettyPrint());
    }
    Allure.addAttachment("response content", responseStr);
  }

  /**
   * Load the request configuration
   *
   * @param testStep
   */
  private RequestSpecification getRequestSpecification(TestStep testStep) {
    RequestSpecification spec =
        new RequestSpecBuilder()
            .addHeaders(GlobalVar.HEADERS)
            .addCookies(GlobalVar.COOKIES)
            .setConfig(restAssuredConfig)
            .setBaseUri(baseURL)
            .setBasePath(testStep.getPath())
            .build();

    if (RestAssured.baseURI.startsWith("https://qa-zuul-b2c-instore.cn-xnp-cloud-pg.com.cn")) {
      return given()
          .relaxedHTTPSValidation("SSL")
          .spec(spec)
          .trustStore("/truststore_javanet.jks", "test1234");
    } else if (RestAssured.baseURI.contains("https")) {
      return given().relaxedHTTPSValidation("SSL").spec(spec).log().all();
    } else {
      return given().spec(spec).log().all();
    }
  }

  private Response uploadFile(TestStep testStep) {
    RequestSpecification requestSpecification =
        getRequestSpecification(testStep).queryParams(testStep.getParams());
    if (testStep.multipartList != null) {
      for (MultipartParameter multipartParameter : testStep.multipartList) {
        String controlName = multipartParameter.getControlName();
        Object controlValue = multipartParameter.getControlValue();
        Boolean isFile = multipartParameter.getBFile();
        if (isFile) {
          if (controlName.isEmpty()) {
            requestSpecification =
                requestSpecification.multiPart(new File(controlValue.toString()));
          } else {
            requestSpecification =
                requestSpecification.multiPart(controlName, new File(controlValue.toString()));
          }
        } else {
          requestSpecification = requestSpecification.multiPart(controlName, controlValue);
        }
      }
    }
    switch (testStep.getType()) {
      case POST:
        response = requestSpecification.post();
        break;
      case PUT:
        response = requestSpecification.put();
        break;
    }

    return response;
  }

  /**
   * Requests with parameters
   *
   * @param testStep
   * @param params
   * @param strbody
   * @return
   */
  @Step("and Given parameters When Sending Request")
  private Response request(TestStep testStep, Map<String, Object> params, String strbody) {
    HttpType httpType = testStep.getType();
    switch (httpType) {
      case GET:
        response = getRequestSpecification(testStep).params(params).get();
        break;
      case POST:
        if (strbody != null && !strbody.isEmpty()) {
          response = getRequestSpecification(testStep).queryParams(params).body(strbody).post();
        } else {
          if (testStep.getIsUploadFile()) { // upload file
            response = uploadFile(testStep);
          } else {
            response = getRequestSpecification(testStep).body(params).post();
          }
        }

        break;
      case DELETE:
        response = getRequestSpecification(testStep).params(params).delete();
        break;
      case PUT:
        if (strbody != null && !strbody.isEmpty())
          response = getRequestSpecification(testStep).params(params).body(strbody).put();
        else {
          if (testStep.getIsUploadFile()) { // upload file
            response = uploadFile(testStep);
          } else {
            response = getRequestSpecification(testStep).params(params).put();
          }
        }

        break;
      case PATCH:
        if (strbody != null && !strbody.isEmpty())
          response =
              getRequestSpecification(testStep).params(params).body(strbody).log().all().patch();
        else response = getRequestSpecification(testStep).params(params).log().all().patch();
        break;
      default:
        throw new RuntimeException(String.format("not support % request type", httpType));
    }

    return response;
  }

  /**
   * Requests with parameters body in map format
   *
   * @param testStep
   * @param params
   * @param postBody
   * @return
   */
  @Step("and Given parameters When Sending Request")
  private Response request(TestStep testStep, Map<String, Object> params, Object postBody) {
    HttpType httpType = testStep.getType();
    switch (httpType) {
      case GET:
        response = getRequestSpecification(testStep).params(params).get();
        break;
      case POST:
        if (postBody != null) {
          response =
              getRequestSpecification(testStep)
                  .queryParams(params)
                  .body(postBody)
                  .log()
                  .all()
                  .post();
        } else {
          response = getRequestSpecification(testStep).body(params).post();
        }
        break;
      case PATCH:
        if (postBody != null) {
          response =
              getRequestSpecification(testStep)
                  .log()
                  .all()
                  .queryParams(params)
                  .body(postBody)
                  .patch();
        } else {
          response = getRequestSpecification(testStep).body(params).log().all().patch();
        }
        break;
      case DELETE:
        response = getRequestSpecification(testStep).params(params).delete();
        break;
      case PUT:
        if (postBody != null)
          response = getRequestSpecification(testStep).params(params).body(postBody).put();
        else response = getRequestSpecification(testStep).params(params).put();
        break;
      default:
        throw new RuntimeException(String.format("not support % request type", httpType));
    }
    return response;
  }

  /**
   * body with map format params
   *
   * @param testStep
   * @param postBody
   * @return
   */
  @Step("and Given parameters When Sending Request")
  private Response request(TestStep testStep, Object postBody) {
    HttpType httpType = testStep.getType();
    switch (httpType) {
      case GET:
        response = getRequestSpecification(testStep).body(postBody).get();
        break;
      case POST:
        response = getRequestSpecification(testStep).body(postBody).post();
        break;
      case PATCH:
        response = getRequestSpecification(testStep).body(postBody).log().all().patch();
        break;
      case PUT:
        response = getRequestSpecification(testStep).body(postBody).put();
        break;
      default:
        throw new RuntimeException(String.format("not support % request type", httpType));
    }
    return response;
  }

  @Step("and Given parameters When Sending Request")
  private Response request(TestStep testStep, String params) {
    HttpType httpType = testStep.getType();
    switch (httpType) {
      case GET:
        response = getRequestSpecification(testStep).body(params).log().all().get();
        break;
      case POST:
        response = getRequestSpecification(testStep).body(params).post();
        break;
      case PATCH:
        response = getRequestSpecification(testStep).body(params).log().all().patch();
        break;
      case PUT:
        response = getRequestSpecification(testStep).body(params).put();
        break;
      default:
        throw new RuntimeException(String.format("not support % request type", httpType));
    }
    return response;
  }

  /**
   * Requests with no parameters
   *
   * @param testStep
   * @return
   */
  private Response doRequest(TestStep testStep) {
    HttpType httpType = testStep.getType();

    logger.info("[" + httpType + "]" + getRequestInfo(testStep.getPath()));
    try {
      switch (httpType) {
        case GET:
          response = getRequestSpecification(testStep).get();
          break;
        case POST:
          if (testStep.getIsUploadFile()) {
            response = uploadFile(testStep);
          } else {
            response = getRequestSpecification(testStep).post();
          }
          break;
        case PATCH:
          response = getRequestSpecification(testStep).patch();
          break;
        case PUT:
          if (testStep.getIsUploadFile()) {
            response = uploadFile(testStep);
          } else {
            response = getRequestSpecification(testStep).put();
          }
          break;
        case DELETE:
          response = getRequestSpecification(testStep).delete();
          break;
        default:
          throw new RuntimeException(String.format("not support % request type", httpType));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
    return response;
  }

  /**
   * request
   *
   * @param testStep
   */
  public Response request(TestStep testStep) {
    RestAssured.baseURI = baseURL;

    StopWatch stopWatch = new StopWatch();
    stopWatch.start();

    if (!checkRequest(testStep)) {
      return null;
    }

    if (testStep.getParams() != null && !testStep.getParams().isEmpty()) {
      if (testStep.getPostBody() != null) {
        response = request(testStep, testStep.getParams(), testStep.getPostBody());
      } else {
        response = request(testStep, testStep.getParams(), testStep.getBody());
      }
    } else if (testStep.getBody() != null && !testStep.getBody().isEmpty()) {
      response = request(testStep, testStep.getBody());
    } else if (testStep.getPostBody() != null) {
      response = request(testStep, testStep.getPostBody());
    } else {
      response = doRequest(testStep);
    }

    stopWatch.stop();
    logger.info("time cost {} ms", stopWatch.getTime());
    logResponseInfo(response);
    return response;
  }

  public Boolean checkRequest(TestStep testStep) {
    Boolean isValidate = true;
    String env = System.getProperty(GlobalVar.ENV);
    if (env != null && env.equals(GlobalVar.PRODUCTION_ENV)) {
      if (testStep.getType().equals(HttpType.GET)) {
        isValidate = true;
      } else {
        Allure.step(
            String.format(
                "Production env only can send get method request, current %s %s",
                testStep.getType().getValue(), baseURL));
        logger.info(
            "Production env only can send get method request,current {} {}",
            testStep.getType().getValue(),
            baseURL);
        isValidate = false;
      }
    }
    return isValidate;
  }

  public void recordRequest(TestStep testStep) {
    String time = TimeUtils.getLocalFormatTime();
    String url = RestAssured.baseURI;
    String path = testStep.getPath();
    String type = testStep.getType().getValue();
    Map parameters = testStep.getParams();
    Object postBody = testStep.getPostBody();

    StringBuffer text = new StringBuffer();
    text.append(time)
        .append(" ")
        .append(url)
        .append(path)
        .append(" ")
        .append(type)
        .append(" ")
        .append(parameters.isEmpty() || parameters == null ? "" : parameters.toString())
        .append(" ")
        .append(postBody == null ? "" : postBody.toString());

    String fileName =
        System.getProperty("user.dir").concat("/src/main/resources/config/request/requestData.txt");
    FileUtil.writeFile(text.toString(), fileName);
  }
}
