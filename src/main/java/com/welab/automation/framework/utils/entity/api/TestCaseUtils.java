package com.welab.automation.framework.utils.entity.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.welab.automation.framework.GlobalVar;
import io.qameta.allure.Allure;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * functional: it uses to extract variable from response and does response checking
 */
public class TestCaseUtils {
    private static final Logger logger = LoggerFactory.getLogger(TestCaseUtils.class);
    private static SoftAssertions softAssert = null;


    /**
     * extract variable that defines in the test case file from response
     * attention: it supports json style now
     *
     * @param jsonEntity
     * @param response   API's response
     * @return
     */
    public static Map<String, String> extractResponseVariable(Response response, JsonEntity jsonEntity) {
        /**
         "extract": {
         "cityId": "cityId",
         "authorization": "accessToken"
         }
         */
        JSONObject extractObject = jsonEntity.getExtractObject();
        Map<String, String> extractedMap = new HashMap<>();
        if (extractObject != null) {
            extractedMap = extractParamter(extractObject, response.jsonPath());
            GlobalVar.GLOBAL_VARIABLES.putAll(extractedMap);
        }
        return extractedMap;
    }

    public static void validate(Response response) {
        validate(response, null);
    }

    /**
     * response validate
     */

    public static void validate(Response response, JsonEntity jsonEntity) {
        softAssert = new SoftAssertions();
        if (response == null) {
            logger.info("API response is null, please check");
            return;
        }
        if (jsonEntity != null) {
            extractResponseVariable(response, jsonEntity);
            databaseDataBuilder(jsonEntity);   //  configDataBuilder;
            databaseDataBuilder(jsonEntity);   //  configDataBuilder;

            checkStatusCode(response, jsonEntity);
            checkContent(response, jsonEntity);
            checkIsNotEmpty(response, jsonEntity);
            notIncludeFieldNames(response, jsonEntity);
        } else {
            checkStatusCode(response);
            checkStatus(response);
        }
        softAssert.assertAll();
    }


    public static boolean checkResponseSuccess(Response response) {
        Boolean isSuccess = false;
        JsonPath jsonPath = response.jsonPath();
        if (jsonPath.get("Status") != null && jsonPath.get("Status").equals("success")) {
            isSuccess = true;
        }
        return isSuccess;
    }

    /**
     * verify response status
     *
     * @param response
     * @param jsonEntity
     */
    public static void checkStatusCode(Response response, JsonEntity jsonEntity) {
        JSONObject validateObject = jsonEntity.getValidateObject();
        if (validateObject.containsKey("statusCode")) {
            int expectedStatusCode = validateObject.getIntValue("statusCode");
            Allure.step(String.format("Then response status code %d should equal to expected code %d", response.getStatusCode(), expectedStatusCode));
            logger.info("Then response status code is {} and expected code is {}", response.getStatusCode(), expectedStatusCode);
            softAssert.assertThat(response.getStatusCode()).isEqualTo(expectedStatusCode);
        }
    }

    public static void checkStatusCode(Response response) {
        response.then().statusCode(response.getStatusCode());
    }

    public static void checkStatus(Response response) {
        softAssert = new SoftAssertions();
        JsonPath jsonPath = response.jsonPath();
        String actualStatus = jsonPath.getString(String.valueOf(GlobalVar.GLOBAL_VARIABLES.get("statusKey")));
        softAssert.assertThat(actualStatus).isEqualTo(String.valueOf(GlobalVar.GLOBAL_VARIABLES.get("statusValue")));
        softAssert.assertAll();
    }

    public static void checkContent(Response response, JsonEntity jsonEntity) {
        checkEqual(response, jsonEntity);
        checkExtentEqual(jsonEntity);
        checkContains(response, jsonEntity);
        checkNotContains(response, jsonEntity);
        checkGreatThan(response, jsonEntity);
        checkLessThan(response, jsonEntity);
        containsFields(response, jsonEntity);
    }

    public static void checkEqual(Response response, JsonEntity jsonEntity) {
        JSONObject contentObject = jsonEntity.getValidateObject();
        if (contentObject.containsKey("eq")) {
            JSONObject eqJsonObject = contentObject.getJSONObject("eq");
            Set<String> keySet = eqJsonObject.keySet();

            for (String key : keySet) {
                Object responseValue = response.jsonPath().get(key);
                Object expectedObject = eqJsonObject.get(key);

                Allure.step(String.format("Then verified key is: %s, response value: %s should equal with expected value: %s", key, responseValue, expectedObject));
                if (responseValue instanceof Integer && expectedObject instanceof String) {
                    Integer intValue = eqJsonObject.getIntValue(key);
                    softAssert.assertThat(responseValue).isEqualTo(intValue);
                } else if (responseValue instanceof Double) {
                    Double tempExpectedValue = eqJsonObject.getDouble(key);
                    softAssert.assertThat(responseValue).isEqualTo(tempExpectedValue);
                } else {
                    softAssert.assertThat(responseValue).isEqualTo(expectedObject);
                }
            }
        }
    }


    public static void checkExtentEqual(JsonEntity jsonEntity) {
        JSONObject validateObject = jsonEntity.getValidateObject();
        if (validateObject.containsKey("extent eq")) {
            JSONArray extentEqArray = validateObject.getJSONArray("extent eq");
            if (extentEqArray != null) {
                for (int i = 0; i < extentEqArray.size(); i++) {
                    JSONObject jsonObject = extentEqArray.getJSONObject(i);
                    Object actualValue = jsonObject.get("actualValue");
                    Object expectValue = jsonObject.get("expectValue");
                    String desc = jsonObject.getString("desc");
                    logger.info(String.format("Then actual value %s should equal with expected value %s", actualValue, expectValue));
                    Allure.step(String.format("Then actual value %s should equal with expected value %s", actualValue, expectValue));
                    if (actualValue instanceof Integer && expectValue instanceof String) {
                        String tempActualValue = jsonObject.getString("actualValue");
                        String tempExpectedValue = jsonObject.getString("actualValue");
                        softAssert.assertThat(tempExpectedValue).as(desc).isEqualTo(tempActualValue);
                    } else if (actualValue instanceof Double && expectValue instanceof Double) {
                        Double tempActualValue = jsonObject.getDouble("actualValue");
                        Double tempExpectedValue = jsonObject.getDouble("actualValue");
                        softAssert.assertThat(tempExpectedValue).as(desc).isEqualTo(tempActualValue);
                    } else {
                        softAssert.assertThat(expectValue).as(desc).isEqualTo(actualValue);
                    }
                }
            }
        }
    }


    private static void checkIsNotEmpty(Response response, JsonEntity jsonEntity) {
        JSONObject validateObject = jsonEntity.getValidateObject();
        if (validateObject.containsKey("notEmpty")) {
            JSONArray notEmptyJsonArray = validateObject.getJSONArray("notEmpty");
            int size = notEmptyJsonArray.size();
            JsonPath jsonPath = response.jsonPath();
            for (int index = 0; index < size; index++) {
                String key = notEmptyJsonArray.getString(index);
                Object objectResponse = jsonPath.get(key);
                if (objectResponse instanceof String) {
                    softAssert.assertThat(jsonPath.getString(key)).isNotEmpty();
                } else if (objectResponse instanceof List) {
                    softAssert.assertThat(jsonPath.getList(key)).isNotEmpty();
                } else if (objectResponse instanceof Map) {
                    softAssert.assertThat(jsonPath.getMap(key)).isNotEmpty();
                } else {
                    softAssert.assertThat(objectResponse).isNotNull();
                }
            }
        }
    }

    /**
     * assert one data is great than another
     */
    public static void checkGreatThan(Response response, JsonEntity jsonEntity) {
        JSONArray greatThanArray = jsonEntity.getGreatThanObject();
        JSONObject greatThanObject = null;

        if (greatThanArray != null) {
            for (int i = 0; i < greatThanArray.size(); i++) {
                greatThanObject = greatThanArray.getJSONObject(i);
                if (greatThanObject.getString("type").equalsIgnoreCase("jsonPath")) {
                    jsonPathGreaterThan(response, greatThanObject);
                } else {
                    variableGreatThan(greatThanObject);
                }
            }
        }
    }

    private static void variableGreatThan(JSONObject jsonObject) {
        String actualKey = "actualValue";
        String expectKey = "expectValue";
        if (jsonObject != null) {
            Object expectedObject = jsonObject.get(expectKey);
            if (expectedObject instanceof Integer) {
                logger.info("greater than method : actual value %d, expect value: %d");
                int actualValue = jsonObject.getIntValue(actualKey);
                int expectedValue = jsonObject.getIntValue(expectKey);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Long) {
                long actualValue = jsonObject.getLongValue(actualKey);
                long expectedValue = jsonObject.getLongValue(expectKey);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Float) {
                Float actualValue = jsonObject.getFloatValue(actualKey);
                Float expectedValue = jsonObject.getFloatValue(expectKey);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Double) {
                Double actualValue = jsonObject.getDoubleValue(actualKey);
                Double expectedValue = jsonObject.getDoubleValue(expectKey);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else {
                throw new RuntimeException("it doesn't support type for checkGreatThan method");
            }
        }
    }

    private static void jsonPathGreaterThan(Response response, JSONObject greatThanObject) {
        if (greatThanObject != null) {
            Set<String> keySet = greatThanObject.keySet();
            JsonPath jsonPath = response.jsonPath();
            String key = greatThanObject.getString("actualValue");
            Object responseValue = jsonPath.get(key);
            Object expectedObject = greatThanObject.get("expectValue");
            Allure.step(String.format("Then the response %s should greater than %s", key, expectedObject));
            if (expectedObject instanceof Long) {
                long actualValue = jsonPath.getLong(key);
                long expectedValue = greatThanObject.getLongValue(key);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Integer) {
                int actualValue = jsonPath.getInt(key);
                int expectedValue = greatThanObject.getIntValue(key);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Float) {
                Float actualValue = jsonPath.getFloat(key);
                Float expectedValue = greatThanObject.getFloatValue(key);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else if (expectedObject instanceof Double) {
                double actualValue = jsonPath.getDouble(key);
                double expectedValue = greatThanObject.getDoubleValue(key);
                softAssert.assertThat(actualValue).isGreaterThan(expectedValue);
            } else {
                throw new RuntimeException("it doesn't support type for checkGreatThan method");
            }
        }
    }

    /**
     * assert one data is great than  or equal to another value
     */
    public static void checkGreatThanOrEqual(Response response, JsonEntity jsonEntity) {
        JSONObject greatThanObject = jsonEntity.getGreatThanOrEqualObject();

        if (greatThanObject != null) {
            Set<String> keySet = greatThanObject.keySet();
            JsonPath jsonPath = response.jsonPath();
            for (String key : keySet) {
                Object responseValue = jsonPath.get(key);
                Object expectedObject = greatThanObject.get(key);
                Allure.step(String.format("Then the response %s should greater than %s", key, expectedObject));
                if (responseValue instanceof Integer) {
                    int actualValue = jsonPath.getInt(key);
                    int expectedValue = greatThanObject.getIntValue(key);
                    softAssert.assertThat(actualValue).isGreaterThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Long) {
                    long actualValue = jsonPath.getLong(key);
                    long expectedValue = greatThanObject.getLongValue(key);
                    softAssert.assertThat(actualValue).isGreaterThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Float) {
                    Float actualValue = jsonPath.getFloat(key);
                    Float expectedValue = greatThanObject.getFloatValue(key);
                    softAssert.assertThat(actualValue).isGreaterThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Double) {
                    double actualValue = jsonPath.getDouble(key);
                    double expectedValue = greatThanObject.getDoubleValue(key);
                    softAssert.assertThat(actualValue).isGreaterThanOrEqualTo(expectedValue);
                } else {
                    throw new RuntimeException("it doesn't support type for checkGreatThanOrEqual method");
                }
            }
        }
    }


    /**
     * assert one data is less than another
     */
    public static void checkLessThan(Response response, JsonEntity jsonEntity) {
        JSONObject greatThanObject = jsonEntity.getLessThanObject();

        if (greatThanObject != null) {
            Set<String> keySet = greatThanObject.keySet();
            JsonPath jsonPath = response.jsonPath();
            for (String key : keySet) {
                Object responseValue = jsonPath.get(key);
                Object expectedObject = greatThanObject.get(key);
                Allure.step(String.format("Then the response %s should greater than %s", key, expectedObject));
                if (responseValue instanceof Integer) {
                    int actualValue = jsonPath.getInt(key);
                    int expectedValue = greatThanObject.getIntValue(key);
                    softAssert.assertThat(actualValue).isLessThan(expectedValue);
                } else if (responseValue instanceof Long) {
                    long actualValue = jsonPath.getLong(key);
                    long expectedValue = greatThanObject.getLongValue(key);
                    softAssert.assertThat(actualValue).isLessThan(expectedValue);
                } else if (responseValue instanceof Float) {
                    Float actualValue = jsonPath.getFloat(key);
                    Float expectedValue = greatThanObject.getFloatValue(key);
                    softAssert.assertThat(actualValue).isLessThan(expectedValue);
                } else if (responseValue instanceof Double) {
                    double actualValue = jsonPath.getDouble(key);
                    double expectedValue = greatThanObject.getDoubleValue(key);
                    softAssert.assertThat(actualValue).isLessThan(expectedValue);
                } else {
                    throw new RuntimeException("it doesn't support type for checkLessThan method");
                }
            }
        }
    }

    /**
     * assert one data is less than or equal to another value
     */
    public static void checkLessThanOrEqual(Response response, JsonEntity jsonEntity) {
        JSONObject greatThanObject = jsonEntity.getLessThanOrEqualOobject();

        if (greatThanObject != null) {
            Set<String> keySet = greatThanObject.keySet();
            JsonPath jsonPath = response.jsonPath();
            for (String key : keySet) {
                Object responseValue = jsonPath.get(key);
                Object expectedObject = greatThanObject.get(key);
                Allure.step(String.format("Then the response %s should greater than %s", key, expectedObject));
                if (responseValue instanceof Integer) {
                    int actualValue = jsonPath.getInt(key);
                    int expectedValue = greatThanObject.getIntValue(key);
                    softAssert.assertThat(actualValue).isLessThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Long) {
                    long actualValue = jsonPath.getLong(key);
                    long expectedValue = greatThanObject.getLongValue(key);
                    softAssert.assertThat(actualValue).isLessThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Float) {
                    Float actualValue = jsonPath.getFloat(key);
                    Float expectedValue = greatThanObject.getFloatValue(key);
                    softAssert.assertThat(actualValue).isLessThanOrEqualTo(expectedValue);
                } else if (responseValue instanceof Double) {
                    double actualValue = jsonPath.getDouble(key);
                    double expectedValue = greatThanObject.getDoubleValue(key);
                    softAssert.assertThat(actualValue).isLessThanOrEqualTo(expectedValue);
                } else {
                    throw new RuntimeException("it doesn't support type for checkLessThanOrEqual method");
                }
            }
        }
    }


    /**
     * @param response
     * @param jsonEntity
     */
    public static void checkContains(Response response, JsonEntity jsonEntity) {
        JSONObject containObject = jsonEntity.getContainObject();
        if (containObject != null) {
            Set<String> keySet = containObject.keySet();
            for (String key : keySet) {
                JsonPath jsonPath = response.jsonPath();
                Object responseValue = jsonPath.get(key);
                Object expectedObject = containObject.get(key);
                if (responseValue == null) {
                    throw new RuntimeException(String.format("It cannot find key: %s in the response, please check!", key));
                }
                if (expectedObject instanceof Map) {
                    Map<Object, Object> actualValue = jsonPath.getMap(key);
                    softAssert.assertThat(actualValue).containsAllEntriesOf((Map<?, ?>) expectedObject);
                } else if (responseValue instanceof List) {
                    List<Object> actualValueList = jsonPath.getList(key);
                    if (expectedObject instanceof List) {
                        softAssert.assertThat(actualValueList).containsAll((List) expectedObject);
                    } else {
                        for (Object eachValue : actualValueList) {
                            if (eachValue instanceof String) {
                                String strEachValue = eachValue.toString();
                                softAssert.assertThat(strEachValue).containsIgnoringCase(expectedObject.toString());
                            }
                        }
                    }
                } else if (responseValue instanceof String) {
                    String actualValue = jsonPath.getString(key);
                    String expectedValue = containObject.getString(key);
                    softAssert.assertThat(actualValue).containsIgnoringCase(expectedValue);
                } else {
                    throw new RuntimeException("it doesn't support type for checkcontains method");
                }
            }
        }
    }

    /**
     * @param response
     * @param jsonEntity
     */
    public static void checkNotContains(Response response, JsonEntity jsonEntity) {
        JSONObject notContainObject = jsonEntity.getNotContainObject();
        if (notContainObject != null) {
            Set<String> keySet = notContainObject.keySet();
            for (String key : keySet) {
                JsonPath jsonPath = response.jsonPath();
                Object responseValue = jsonPath.get(key);
                Object expectedObject = notContainObject.get(key);
                if (responseValue == null) {
                    throw new RuntimeException(String.format("It cannot find key: %s in the response, please check!", key));
                }
                if (responseValue instanceof List) {
                    List<Object> actualValueList = jsonPath.getList(key);
                    if (expectedObject instanceof List) {
                        Iterator iterator = ((List) expectedObject).iterator();
                        softAssert.assertThat(actualValueList).doesNotContainAnyElementsOf((Iterable<?>) iterator);
                    } else {
                        for (Object eachValue : actualValueList) {
                            if (eachValue instanceof String) {
                                String strEachValue = eachValue.toString();
                                softAssert.assertThat(strEachValue).doesNotContain(expectedObject.toString());
                            }
                        }
                    }
                } else if (responseValue instanceof String) {
                    String actualValue = jsonPath.getString(key);
                    String expectedValue = notContainObject.getString(key);
                    softAssert.assertThat(actualValue).doesNotContain(expectedValue);
                } else {
                    throw new RuntimeException("it doesn't support type for checkcontains method");
                }
            }
        }
    }


    public static void databaseDataBuilder(JsonEntity jsonEntity) {
        JSONObject contentObject = jsonEntity.getValidateObject();
        jsonEntity.setJsonObject(TestCaseUtils.replaceParameter(jsonEntity.getJsonObject()));
    }

    private static Map<String, String> extractParamter(JSONObject jsonObject, JsonPath jsonPath) {
        Map<String, String> commonMap = new HashMap<>();
        Set<String> keySet = jsonObject.keySet();
        for (String key : keySet) {
            commonMap.put(key, jsonPath.get(jsonObject.getString(key)));
        }
        return commonMap;
    }

    public static JSONObject replaceParameter(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        jsonObject = replaceParameter(jsonObject, null);
        return jsonObject;
    }

    public static JSONObject replaceParameter(JSONObject jsonObject, Map<String, Object> tempHashMap) {
        String jsonString = jsonObject.toJSONString();
        String replaceParameter = replaceParameter(jsonString, tempHashMap);
        jsonObject = JSON.parseObject(replaceParameter);
        return jsonObject;
    }


    public static String extractVarByRegression(String parameter, String pattern, int postion) {
        String findedStr = null;
        if (parameter == null || pattern == null) {
            return "";
        }
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(parameter);
        int index = 1;
        while (matcher.find()) {
            findedStr = matcher.group(1);
            System.out.println(findedStr);
        }
        return findedStr;
    }

    /**
     * 替换参数，先从临时map中替换，如何临时map找不到值，则从全局运行变量找到值进行替换
     *
     * @param parameter
     * @param tempHashMap
     * @return
     */
    private static String replaceParameter(String parameter, Map<String, Object> tempHashMap) {
        StringBuffer sb = new StringBuffer(parameter);
        String pattern = "\\$\\{[\\w#-]+\\}";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(parameter);

        int beginIndex = 0;
        Object tempNewValue = null;
        while (matcher.find()) {
            String group = matcher.group();
            String originValue = group.substring(2, group.length() - 1);
            //  get value from temp hash map
            if (tempHashMap != null) {
                tempNewValue = tempHashMap.get(originValue);
            }

            //if user cannot get value from temp HashMap, then user can get value from global HashMap
            if (tempNewValue == null) {
                tempNewValue = GlobalVar.GLOBAL_VARIABLES.get(originValue);
            }

            if (tempNewValue != null) {
                beginIndex = sb.indexOf(group);
                sb.replace(beginIndex, beginIndex + group.length(), String.valueOf(tempNewValue));
            }

            tempNewValue = null;
        }

        return sb.toString();
    }


    public static void assertEqualIgnoreFields(Object object, String expectedJsonStr, String... comparedField) {
        String actualJsonStr = GsonUtil.toJson(object);
        assertEqualIgnoreFields(actualJsonStr, expectedJsonStr, comparedField);
    }

    public static void assertEqualWithFields(Object object, Map expectedMap, JsonEntity jsonEntity) {
        String actualJsonStr = GsonUtil.toJson(object);
        String expectedJsonStr = GsonUtil.toJson(expectedMap);
        JSONObject equalFields = jsonEntity.getValidateObject().getJSONObject("equal fields");
        assertEqualWithFields(actualJsonStr, expectedJsonStr, equalFields);
    }

    public static void assertEqualWithFields(Object object, List expectedMap, JsonEntity jsonEntity) {
        String actualJsonStr = GsonUtil.toJson(object);
        String expectedJsonStr = GsonUtil.toJson(expectedMap);
        JSONObject equalFields = jsonEntity.getValidateObject().getJSONObject("equal fields");
        assertEqualWithFields(actualJsonStr, expectedJsonStr, equalFields);
    }

    public static void assertEqualWithFields(Object object, Map expectedMap, JSONObject comparedField) {
        String actualJsonStr = GsonUtil.toJson(object);
        String expectedJsonStr = GsonUtil.toJson(expectedMap);
        assertEqualWithFields(actualJsonStr, expectedJsonStr, comparedField);
    }

    private static void assertEqualWithFields(String actualJsonStr, String expectedJsonStr, JSONObject comparedField) {
        softAssert = new SoftAssertions();
        JsonParser jsonParser = new JsonParser();
        JsonObject expectedJsonObject = null;
        JsonArray expectedJsonArray = null;

        JsonObject actualJsonElement = (JsonObject) jsonParser.parse(actualJsonStr);
        JsonElement expectedJsonElement = jsonParser.parse(expectedJsonStr);
        if (expectedJsonElement instanceof JsonObject) {
            expectedJsonObject = (JsonObject) expectedJsonElement;
        } else if (expectedJsonElement instanceof JsonArray) {
            expectedJsonArray = (JsonArray) expectedJsonElement;
        }
        if (comparedField == null) {
            softAssert.assertThat(actualJsonElement).isEqualTo(expectedJsonElement);
        } else {
            if (expectedJsonObject != null) {
                Iterator<String> compareFieldIterator = comparedField.keySet().iterator();
                while (compareFieldIterator.hasNext()) {
                    String actualKey = compareFieldIterator.next();
                    String expectedKey = comparedField.getString(actualKey);
                    JsonElement jsonElement1 = actualJsonElement.get(actualKey);
                    JsonElement jsonElement2 = expectedJsonObject.get(expectedKey);
                    softAssert.assertThat(jsonElement1).isEqualTo(jsonElement2);
                }
            }

            if (expectedJsonArray != null) {
                Iterator<String> compareFieldIterator = comparedField.keySet().iterator();
                while (compareFieldIterator.hasNext()) {
                    String actualKey = compareFieldIterator.next();
                    String expectedKey = comparedField.getString(actualKey);
                    List jsonElement1 = GsonUtil.gson.fromJson(actualJsonElement.getAsJsonArray(actualKey), List.class);

                    List list = GsonUtil.gson.fromJson(expectedJsonArray, List.class);
                    List mapListOneColumnValues = DataPool.getMapListOneColumnValues(list, expectedKey);
                    softAssert.assertThat(jsonElement1.size()).isEqualTo(mapListOneColumnValues.size());
                    softAssert.assertThat(jsonElement1).containsExactlyInAnyOrderElementsOf(mapListOneColumnValues);
                }
            }

        }
        softAssert.assertAll();
    }

    private static void assertEqualIgnoreFields(String actualJsonStr, String expectedJsonStr, String... comparedField) {
        softAssert = new SoftAssertions();
        JsonParser jsonParser = new JsonParser();
        JsonObject actualJsonElement = (JsonObject) jsonParser.parse(actualJsonStr);
        JsonObject expectedJsonElement = (JsonObject) jsonParser.parse(expectedJsonStr);
        for (String field : comparedField) {
            actualJsonElement.remove(field);
            expectedJsonElement.remove(field);
        }

        softAssert.assertThat(actualJsonElement).isEqualTo(expectedJsonElement);
        softAssert.assertAll();
    }

    public static void notIncludeFieldNames(Response response, JsonEntity jsonEntity) {
        boolean flag = true;
        Map data = new HashMap();
        String itSetValue = null;
        String itListValue = null;
        JSONObject contentObject = jsonEntity.getValidateObject();
        JsonPath jsonPath = response.jsonPath();
        if (contentObject.containsKey("notExist")) {
            JSONArray fieldNames = contentObject.getJSONArray("notExist");
            data = jsonPath.getMap("Data");
            Set keys = data.keySet();
            for (Iterator itList = fieldNames.iterator(); itList.hasNext(); ) {
                itListValue = (String) itList.next();
                for (Iterator itSet = keys.iterator(); itSet.hasNext(); ) {
                    itSetValue = (String) itSet.next();
                    if (itSetValue.equals(itListValue)) {
                        logger.info("Response include " + itListValue);
                        flag = false;
                        Allure.step(String.format("Response include %s", itListValue));
                    }
                }
            }
            softAssert.assertThat(flag).isEqualTo(true);
        }
    }

    public static void containsFields(Response response, JsonEntity jsonEntity) {
        JSONArray containFieldJsonArray = jsonEntity.getContainFieldObject();
        JsonPath jsonPath = response.jsonPath();
        if (containFieldJsonArray != null) {
            for (int index = 0; index < containFieldJsonArray.size(); index++) {
                JSONObject jsonObject = containFieldJsonArray.getJSONObject(index);
                String type = jsonObject.getString("type");
                if (type != null && type.equals("jsonPath")) {
                    String field = jsonObject.getString("field");
                    String message = jsonObject.getString("message");
                    softAssert.assertThat(((Object) jsonPath.get(field))).as(message == null ? "" : message).isNotNull();
                }

            }
        }
    }
}



