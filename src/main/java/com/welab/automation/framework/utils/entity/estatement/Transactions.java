package com.welab.automation.framework.utils.entity.estatement;


public class Transactions {
	private int core_account_trans_number;
	private String Date;
	private String Year;
	private String Month;
	private String day_string;
	private int day;
	private String Type;
	private String Description;
	private String Ref;
	private String Amount;
	private String Amount_row_content;
	private boolean check_foreign_currency_Amount=true;
	private boolean check_desc=true;
	private boolean is_goSave_trans=false;
	private boolean is_right_type=false;
	private boolean is_invalid_trans=false;
	private boolean is_send_money_have_right_fuhao;
	private boolean is_recieve_money_have_right_fuhao;
	private boolean is_JECTO_transaction=false;


	public boolean isCheck_foreign_currency_Amount() {
		return check_foreign_currency_Amount;
	}
	public void setCheck_foreign_currency_Amount(boolean check_foreign_currency_Amount) {
		this.check_foreign_currency_Amount = check_foreign_currency_Amount;
	}
	public String getDay_string() {
		return day_string;
	}
	public void setDay_string(String day_string) {
		this.day_string = day_string;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getCore_account_trans_number() {
		return core_account_trans_number;
	}
	public void setCore_account_trans_number(int core_account_trans_number) {
		this.core_account_trans_number = core_account_trans_number;
	}
	public String getAmount_row_content() {
		return Amount_row_content;
	}
	public void setAmount_row_content(String amount_row_content) {
		Amount_row_content = amount_row_content;
	}
	public boolean isIs_JECTO_transaction() {
		return is_JECTO_transaction;
	}
	public void setIs_JECTO_transaction(boolean is_JECTO_transaction) {
		this.is_JECTO_transaction = is_JECTO_transaction;
	}
	
	
	public boolean isIs_invalid_trans() {
		return is_invalid_trans;
	}
	public void setIs_invalid_trans(boolean is_invalid_trans) {
		this.is_invalid_trans = is_invalid_trans;
	}
	public boolean isIs_right_type() {
		return is_right_type;
	}
	public void setIs_right_type(boolean is_right_type) {
		this.is_right_type = is_right_type;
	}
	public String getYear() {
		return Year;
	}
	public void setYear(String year) {
		Year = year;
	}
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}
	public boolean isIs_send_money_have_right_fuhao() {
		return is_send_money_have_right_fuhao;
	}
	public void setIs_send_money_have_right_fuhao(boolean is_send_money_have_right_fuhao) {
		this.is_send_money_have_right_fuhao = is_send_money_have_right_fuhao;
	}
	public boolean isIs_recieve_money_have_right_fuhao() {
		return is_recieve_money_have_right_fuhao;
	}
	public void setIs_recieve_money_have_right_fuhao(boolean is_recieve_money_have_right_fuhao) {
		this.is_recieve_money_have_right_fuhao = is_recieve_money_have_right_fuhao;
	}
	public boolean isIs_goSave_trans() {
		return is_goSave_trans;
	}
	public void setIs_goSave_trans(boolean is_goSave_trans) {
		this.is_goSave_trans = is_goSave_trans;
	}
	public boolean isCheck_desc() {
		return check_desc;
	}
	public void setCheck_desc(boolean check_desc) {
		this.check_desc = check_desc;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getRef() {
		return Ref;
	}
	public void setRef(String ref) {
		Ref = ref;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	@Override
	public String toString() {
		return "Transactions [Date=" + Date + ", Year=" + Year + ", Month=" + Month + ", Type=" + Type
				+ ", Description=" + Description + ", Ref=" + Ref + ", Amount=" + Amount 
				+ ", check_desc=" + check_desc + ", is_goSave_trans=" + is_goSave_trans + "]";
	}
	
}
