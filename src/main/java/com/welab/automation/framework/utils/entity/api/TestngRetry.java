package com.welab.automation.framework.utils.entity.api;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;


public class TestngRetry implements IRetryAnalyzer {
    private int count = 0;
    private static final int totalCount = Integer.valueOf(ConfigUtils.getProperty("retryTimes", "0"));


    @Override
    public boolean retry(ITestResult result) {
        if (this.count < TestngRetry.totalCount) {
            this.count++;
            return true;
        }
        return false;
    }
}

