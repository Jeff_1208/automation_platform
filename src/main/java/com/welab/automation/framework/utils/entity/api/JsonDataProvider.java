package com.welab.automation.framework.utils.entity.api;

import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.utils.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

public class JsonDataProvider {
    private static final Logger logger = LoggerFactory.getLogger(JsonDataProvider.class);
    public static String DATAFILE = "";

    @DataProvider(name = "json_data")
    public static Iterator<Object[]> fetchData(Method method) throws Exception {
        Object result[][] = null;
        String methodName = method.getName();
        URL resourceUrl = JsonDataProvider.class.getClassLoader().getResource(JsonDataProvider.DATAFILE);
        String filePath = URLDecoder.decode(resourceUrl.getPath(), "utf-8");

        logger.debug(String.format("method name： %s", methodName));
        List<JSONObject> testDataList = JsonUtil.extractDataJson(filePath).getObject(methodName, List.class);
        String rowID, description, label;

        ArrayList<Object[]> jsonObjects = new ArrayList<>();

        for (int i = 0; i < testDataList.size(); i++) {
            JSONObject jsonObject = testDataList.get(i);
            rowID = jsonObject.getString("rowID");
            description = jsonObject.getString("description");
            JsonEntity entity = new JsonEntity.JsonEntityBuilder()
                    .withJsonObject(jsonObject).build();
            Object[] objectArr = new Object[]{rowID, description, entity};
            jsonObjects.add(objectArr);
        }

        return jsonObjects.iterator();
    }


    public static String lookupMessage(String filePath, String code) {
        Properties properties = new Properties();
        String message = null;
        try {
            properties.load(new FileReader(filePath));
            message = properties.getProperty(code);
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException.getStackTrace());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return message;
    }

    public static String getMessage(String filePath, String code) throws Exception {
        String absolutePath = "";
        String message = lookupMessage(absolutePath, code);
        if (message == null) {
            throw new Exception(String.format("The code:%s not found in %s file", code, filePath));
        }
        return message;
    }
}
