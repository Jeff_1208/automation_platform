package com.welab.automation.framework.utils.app;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ExcelReader {
  private String fileName;
  private String sheetName;
  private XSSFWorkbook book;

  private ExcelReader(ExcelReaderBuilder excelReaderBuilder) {
    this.fileName = excelReaderBuilder.fileName;
    this.sheetName = excelReaderBuilder.sheetName;
  }

  public static class ExcelReaderBuilder {
    private String fileName;
    private String sheetName;

    public ExcelReaderBuilder setFileLocation(String location) {
      this.fileName = location;
      return this;
    }

    public ExcelReaderBuilder setSheet(String sheetName) {
      this.sheetName = sheetName;
      return this;
    }

    public ExcelReader build() {
      return new ExcelReader(this);
    }
  }

  private XSSFWorkbook getWorkBook(String filePath) throws IOException, InvalidFormatException {
    OPCPackage pkg = OPCPackage.open(filePath);
    XSSFWorkbook wb = new XSSFWorkbook(pkg);
    return wb;
  }

  private XSSFSheet getWorkBookSheet(String fileName, String sheetName)
      throws InvalidFormatException, IOException {
    this.book = getWorkBook(fileName);
    return this.book.getSheet(sheetName);
  }

  public List<List<String>> getSheetData() throws IOException {
    XSSFSheet sheet;
    List<List<String>> outerList;

    try {
      sheet = getWorkBookSheet(fileName, sheetName);
      outerList = fetchSheetData(sheet);
    } catch (InvalidFormatException e) {
      throw new RuntimeException(e.getMessage());
    } finally {
      this.book.close();
    }
    return outerList;
  }

  public List<List<String>> getSheetDataAt() throws InvalidFormatException, IOException {
    XSSFSheet sheet;
    List<List<String>> outerList;

    try {
      sheet = getWorkBookSheet(fileName, sheetName);
      outerList = fetchSheetData(sheet);
    } catch (InvalidFormatException e) {
      throw new RuntimeException(e.getMessage());
    } finally {
      if (this.book != null) {
        this.book.close();
      }
    }
    return outerList;
  }

  private List<List<String>> fetchSheetData(XSSFSheet sheet) {
    List<List<String>> outerList = new LinkedList<>();
    prepareOuterList(sheet, outerList);
    return Collections.unmodifiableList(outerList);
  }

  private void prepareOuterList(XSSFSheet sheet, List<List<String>> outerList) {
    for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
      List<String> innerList = new LinkedList<>();
      XSSFRow xssfRow = sheet.getRow(i);

      for (int j = xssfRow.getFirstCellNum(); j < xssfRow.getLastCellNum(); j++) {
        prepareInnerList(innerList, xssfRow, j);
      }
      outerList.add(Collections.unmodifiableList(innerList));
    }
  }

  private void prepareInnerList(List<String> innerList, XSSFRow xssfRow, int j) {
    switch (xssfRow.getCell(j).getCellType()) {
      case BLANK:
        innerList.add("");
        break;

      case STRING:
        innerList.add(xssfRow.getCell(j).getStringCellValue());
        break;

      case NUMERIC:
        innerList.add(xssfRow.getCell(j).getNumericCellValue() + "");
        break;

      case BOOLEAN:
        innerList.add(xssfRow.getCell(j).getBooleanCellValue() + "");
        break;

      default:
        throw new IllegalArgumentException("Cannot read the column : " + j);
    }
  }
}
