package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.Ref;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

class ResultUtils {
  ArrayList<String> fail_Customer_id_list = new ArrayList<>();

  public static void main(String[] args) throws IOException {
    ResultUtils r = new ResultUtils();
    ArrayList<Estatement> Estatement_List = new ArrayList<>();
    ArrayList<Ref> refs_List = new ArrayList<>();
    String folder_path = "D:\\Project\\e-Statement\\estatement_pdf\\2021_03_03_estatement";
    String filename = folder_path + "/eStatement_result_" + Common.get_time_string() + ".xlsx";
    r.Print_result_to_Excel(Estatement_List, folder_path, refs_List, filename);
  }

  public ResultUtils() {}

  private ArrayList<String> Spec_FPS_account;

  public ResultUtils(ArrayList<String> Spec_FPS_account) {
    this.Spec_FPS_account = Spec_FPS_account;
  }

  String[] title_list = {
    "Customer_ID",
    "Account_ID",
    "月份",
    "核心账户(首页)",
    "GoSave余额(首页)",
    "结存总额(首页) ",
    "私人贷款账户(首页) ",
    "承上余额",
    "核心账户(转账)",
    "核心账户计算余额",
    "GoSave 计算余额",
    "结存总额计算余额",
    "Loans抓取余额",
    "核心账户测试结果",
    "下月转账记录",
    "Loans测试结果",
    "Interest利息",
    "检查Description",
    "检查Gosave记录",
    "检查Ref开头",
    "金额符号是否正常",
    "General Information",
    "Invaild transaction",
    "Comment",
    "Test Result"
  };

  public ArrayList<Estatement> Print_result_to_Excel(
      ArrayList<Estatement> Estatement_List,
      String folder_path,
      ArrayList<Ref> fail_refs,
      String filename) {

    // 创建工作薄对象
    FileOutputStream out;
    try {
      XSSFWorkbook workbook = new XSSFWorkbook(); // 这里也可以设置sheet的Name
      // 创建工作表对象
      XSSFSheet sheet = workbook.createSheet();
      for (int i = 0; i < title_list.length; i++) {
        sheet.setColumnWidth(i, 3500);
      }
      // 创建工作表的行
      XSSFRow row = sheet.createRow(0); // 设置第一行，从零开始
      for (int i = 0; i < title_list.length; i++) {
        XSSFCellStyle yellow_style = SetCellStyle(workbook, 2);
        XSSFCell c = row.createCell(i);
        c.setCellStyle(yellow_style);
        c.setCellValue(title_list[i]);
      }
      for (int i = 0; i < Estatement_List.size(); i++) {
        Estatement e = Estatement_List.get(i);
        XSSFRow r = sheet.createRow(i + 1);
        int cell_column = 0;
        StringBuffer stringBuffer = new StringBuffer();
        for (int j = 0; j < title_list.length; j++) {

          e = write_cell(r, cell_column, e, workbook, stringBuffer);
          cell_column += 1;
        }
      }

      int row_number = sheet.getLastRowNum();
      XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
      XSSFCell cell = sheet.createRow(row_number + 2).createCell(0);
      cell.setCellValue("注：当承上余额为 '-' 时，默认设置为0！");
      cell.setCellStyle(yellow_style_3);

      int row_num = row_number + 3;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue("Column R: 根据Ref No.找到etatement中transactions,确认description是否正确！");
      row_num += 1;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue(
              "Column S: 根据Ref No.找到etatement中GoSave transactions,确认在Gosave transaction中是否有对应的记录！");
      row_num += 1;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue(
              "Column T: 检查Ref 的开头， 其中\"收款\",\"付款\",\"Debit Card 消費\",\"現金回贈\",\"銀通櫃員機\",\"海外櫃員機\",\"存入款項\"为 FT ， 存入Gosave的Ref开头为 AAAC ！");
      row_num += 1;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue(
              "Column U: 检查金额的正负号是否正确， \"付款\",\"Debit Card 消費\",\"存入GoSave \",\" 銀通櫃員機提款\",\"海外櫃員機提款\" 的金额是否带有“-” ， 收款不能带有“-” ！");
      row_num += 1;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue(
              "Column V: 检查estatement中每一页的General Information(包括：你的銀行月結單Your Bank Statement,列印日期 Date of issue,賬戶號碼 Account Number,銀行代碼 Bank Code/分行編號 Branch Code: 390 / 750)是否正确,！");
      row_num += 1;
      sheet
          .createRow(row_num)
          .createCell(0)
          .setCellValue("Column W: 检查estatement中Invaild transaction(是否成对出现，金额是否分别为正负并且相等，ref是否相同)");

      workbook.setSheetName(0, "Result");

      for (int i = 0; i < fail_Customer_id_list.size(); i++) {
        workbook.createSheet();
        workbook.setSheetName(i + 1, fail_Customer_id_list.get(i));
      }
      out = new FileOutputStream(filename);
      workbook.write(out);
      out.close();
      create_refs_report(folder_path, fail_refs);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return Estatement_List;
  }

  public ArrayList<String> get_fail_Customer_id_list() {
    return fail_Customer_id_list;
  }

  public void create_result_report(String folder_path) {
    String filename = folder_path + "/Result_" + Common.get_date_string() + ".xlsx";
    // 创建工作薄对象
    FileOutputStream out;
    try {
      XSSFWorkbook workbook = new XSSFWorkbook(); // 这里也可以设置sheet的Name
      workbook.createSheet();
      workbook.setSheetName(0, "Result");
      for (int i = 0; i < fail_Customer_id_list.size(); i++) {
        workbook.createSheet();
        workbook.setSheetName(i + 1, fail_Customer_id_list.get(i));
      }
      out = new FileOutputStream(filename);
      workbook.write(out);
      out.close();
    } catch (Exception e) {
    }
  }

  public void create_refs_report(String folder_path, ArrayList<Ref> fail_refs) {
    String filename = folder_path + Common.fail_refs_fileName;
    // 创建工作薄对象
    FileOutputStream out;
    String[] ref_title = {"Customer_ID", "FileName", "Path", "Refs"};

    try {
      XSSFWorkbook workbook = new XSSFWorkbook(); // 这里也可以设置sheet的Name
      XSSFSheet sheet0 = workbook.createSheet();
      workbook.setSheetName(0, "Result");
      // 创建工作表的第一行
      XSSFRow row = sheet0.createRow(0); // 设置第一行，从零开始
      for (int i = 0; i < ref_title.length; i++) {
        XSSFCellStyle yellow_style = SetCellStyle(workbook, 2);
        XSSFCell c = row.createCell(i);
        c.setCellStyle(yellow_style);
        c.setCellValue(ref_title[i]);
      }
      for (int i = 0; i < fail_refs.size(); i++) {
        XSSFRow row1 = sheet0.createRow(i + 1);
        XSSFCell cell_ID = row1.createCell(0);
        XSSFCell cell_filename = row1.createCell(1);
        XSSFCell cell_path = row1.createCell(2);
        XSSFCell cell_Refs = row1.createCell(3);
        cell_ID.setCellValue(fail_refs.get(i).getCustomer_ID());
        cell_filename.setCellValue(fail_refs.get(i).getFile_name());
        cell_path.setCellValue(fail_refs.get(i).getPath());
        cell_Refs.setCellValue(fail_refs.get(i).getFail_Ref_list().toString());
      }
      out = new FileOutputStream(filename);
      workbook.write(out);
      out.close();
    } catch (Exception e) {
    }
  }

  public Estatement write_cell(
      XSSFRow r, int index, Estatement e, XSSFWorkbook workbook, StringBuffer stringBuffer) {
    XSSFCellStyle green_style = SetCellStyle(workbook, 1);
    XSSFCellStyle red_style = SetCellStyle(workbook, 0);
    XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
    XSSFCellStyle grey_style_4 = SetCellStyle(workbook, 4);
    XSSFCell c = r.createCell(index);

    String cell_content = "";
    // if(index==0) cell_content= e.getCustomer_id()+"/"+e.getFile_name();
    if (index == 0) cell_content = e.getCustomer_id();
    if (index == 1) cell_content = e.getAccount_id();
    if (index == 2) cell_content = e.getWhich_month();
    if (index == 3) cell_content = e.getCoreaccount_total_capture_title();
    if (index == 4) {
      cell_content = e.getGosave_total_capture_title();
      if (cell_content.length() < 2) {
        cell_content = "Not Applicable";
        c.setCellStyle(grey_style_4);
      } else {
        c.setCellStyle(green_style);
      }
    }
    if (index == 5) cell_content = e.getTotal_Amount_capture_title();
    if (index == 6) {
      cell_content = e.getLaon_total_capture_title();
      if (cell_content == null || cell_content.length() < 2) {
        cell_content = "Not Applicable";
      } else {

      }
    }
    // if(index==6) cell_content= e.getLaon_total_capture_title();
    if (index == 7) cell_content = e.getLast_month_amount();
    if (index == 8) cell_content = e.getCoreaccount_total_capture();
    if (index == 9) cell_content = e.getCoreaccount_total_calculate();
    if (index == 10) cell_content = e.getGosave_total_calculate();
    if (index == 11) cell_content = e.getTotal_Amount_calculate();
    if (index == 12) cell_content = e.getLaon_total_calculate();
    if (index == 13) cell_content = e.isCore_account_result() + "";
    if (index == 14) cell_content = e.isIs_have_next_month_tran() + "";
    if (index == 15) {
      if (!e.isIs_have_loan_record()) {
        cell_content = "true";
        c.setCellStyle(green_style);
      } else {
        if (e.getLoan_fail_trans_Ref_for_record().size() > 0) {
          cell_content = e.getLoan_fail_trans_Ref_for_record().toString();
          stringBuffer.append("贷款记录内容不正确;\n");
          c.setCellStyle(red_style);
        } else {
          cell_content = "true";
          c.setCellStyle(green_style);
        }
      }
    }
    if (index == 16) {
      cell_content = e.isIs_check_interest() + "";
      if (!e.isIs_check_interest()) {
        stringBuffer.append("利息异常,需要显示利息;\n");
      }
    }
    if (index == 17) {
      if (e.getFail_trans_Ref_list_for_desc().size() > 0) {
        stringBuffer.append("部分记录description错误;\n");
      }
      cell_content = e.getFail_trans_Ref_list_for_desc().toString();
    }

    if (index == 18) {
      ArrayList<String> s = e.getGoSave_fail_trans_Ref_from_coreaccount_list();
      if (s == null) {
        cell_content = "";
      } else {
        stringBuffer.append("Gosave记录和Core Accout中记录不匹配;\n");
        cell_content = s.toString();
      }
    }
    if (index == 19) {
      cell_content = e.getFail_start_Ref_list().toString();
      if (e.getFail_start_Ref_list().size() > 0) {
        stringBuffer.append("转账记录Ref的开头错误;\n");
      }
    }
    if (index == 20) {
      cell_content = e.getWrong_single_amount_of_ref_list().toString(); // 		"金额符号异常"
      if (e.getWrong_single_amount_of_ref_list().size() > 0) {
        stringBuffer.append("部分转账记录金额符号错误，需要检查是否带“-”;\n");
      }
    }
    if (index == 21) {
      cell_content = e.isIs_page_title_right() + ""; // "General Information"
      if (!e.isIs_page_title_right()) {
        stringBuffer.append("eSatement每页中的Title有错误;\n");
      }
    }
    if (index == 22) {
      if (!Spec_FPS_account.contains(e.getCustomer_id())) {
        cell_content = e.getFail_trans_Ref_list_for_Invalid_trans().toString(); // Invalid_trans
      }
      if (e.getFail_trans_Ref_list_for_Invalid_trans().size() > 0) {
        if (!Spec_FPS_account.contains(e.getCustomer_id())) {
          stringBuffer.append("Some FPS Sender/Receiver are missing;\n");
        }
      }
    }

    if (index == 23) {
      cell_content = stringBuffer.toString();
      if (cell_content.length() > 1) {
        int index_enter = cell_content.lastIndexOf("\n");
        cell_content = cell_content.substring(0, index_enter);
        c.setCellStyle(red_style);
        if (!fail_Customer_id_list.contains(e.getCustomer_id())) {
          fail_Customer_id_list.add(e.getCustomer_id());
        }
      } else {
        c.setCellStyle(green_style);
      }
    }
    if (index == 24) {
      cell_content = stringBuffer.toString();
      e.setComment(cell_content);
      if (cell_content.length() > 1) {
        cell_content = "Fail";
        e.setResult(false);
        c.setCellStyle(red_style);
      } else {
        c.setCellStyle(green_style);
        e.setResult(true);
        cell_content = "Pass";
      }
    }
    if (index < 3) {
      c.setCellStyle(yellow_style_3);
    }
    if (index == 3 || index == 5) {
      c.setCellStyle(green_style);
    }
    if (index == 5) {
      if (cell_content.contains("-") && (!e.isIs_close_acount())) {
        c.setCellStyle(red_style);
      } else {
        c.setCellStyle(green_style);
      }
      if (cell_content.contains("-") && (Common.is_amount_zero(e.getTotal_Amount_calculate()))) {
        c.setCellStyle(green_style);
      }
    }
    // 如果抓取的金额“-”在后面
    if (index == 6) {
      if (Common.is_valued_number(cell_content) || cell_content == null) {
        c.setCellStyle(green_style);
      } else if (cell_content.contains("Not Applicable")) {
        c.setCellStyle(grey_style_4);
      } else {
        c.setCellStyle(red_style);
      }
    }

    if (index == 7 || index == 8) {
      if (cell_content == null || cell_content.contains("-")) {
        c.setCellStyle(red_style);
      } else {
        c.setCellStyle(green_style);
      }
    }

    if (index == 9) {
      if (Common.compare_amount_string_to_double(
          e.getCoreaccount_total_calculate(), e.getCoreaccount_total_capture_title())) {
        c.setCellStyle(green_style);
      } else {
        String temp =
            "核心账户计算总金额为： "
                + e.getCoreaccount_total_calculate()
                + ", 抓取金额为： "
                + e.getCoreaccount_total_capture_title()
                + ", 金额不匹配;";
        stringBuffer.append(temp + "\n");
        c.setCellStyle(red_style);
      }
    }
    if (index == 10) {
      if (!e.isIs_have_gosave_record()) {
        c.setCellStyle(green_style);
      } else if (Common.compare_amount_string_to_double(
          e.getGosave_total_calculate(), e.getGosave_total_capture_title())) {
        c.setCellStyle(green_style);
      } else {
        stringBuffer.append("GoSave金额计算金额错误;\n");
        c.setCellStyle(red_style);
      }
    }
    if (index == 11) {
      if (e.isIs_total_amount_equal()) {
        c.setCellStyle(green_style);
      } else {
        c.setCellStyle(red_style);
      }
      if (e.isIs_account_close()) {
        c.setCellStyle(green_style);
      }
      if (e.getTotal_Amount_capture_title().contains("-")
          && (Common.is_amount_zero(e.getTotal_Amount_calculate()))) {
        c.setCellStyle(green_style);
      }
    }
    if (index == 12) {
      if (!e.isIs_have_loan_record()) {
        c.setCellStyle(green_style);
      } else if (Common.compare_amount_string_to_double(
          e.getLaon_total_calculate(), e.getLaon_total_capture_title())) {
        c.setCellStyle(green_style);
      } else {
        c.setCellStyle(red_style);
      }
    }
    if (index == 13 || index == 14 || index == 16) {
      set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
    }
    if (index == 17) {
      setColumnStyle(e.getFail_trans_Ref_list_for_desc(), green_style, red_style, c);
    }
    if (index == 18) {
      setColumnStyle(e.getGoSave_fail_trans_Ref_from_coreaccount_list(), green_style, red_style, c);
    }
    if (index == 19) {
      setColumnStyle(e.getFail_start_Ref_list(), green_style, red_style, c);
    }
    if (index == 20) {
      setColumnStyle(e.getWrong_single_amount_of_ref_list(), green_style, red_style, c);
    }
    if (index == 21) {
      set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
    }
    if (index == 22) {
      if (e.getFail_trans_Ref_list_for_Invalid_trans().size() > 0) {
        if (!Spec_FPS_account.contains(e.getCustomer_id())) {
          c.setCellStyle(red_style);
        } else {
          c.setCellStyle(green_style);
        }
      } else {
        c.setCellStyle(green_style);
      }
    }
    c.setCellValue(cell_content);
    return e;
  }

  private void set_cell_style_for_boolean_value(
      String cell_content, XSSFCellStyle green_style, XSSFCellStyle red_style, XSSFCell c) {
    if (cell_content.contains("false")) {
      c.setCellStyle(red_style);
    } else {
      c.setCellStyle(green_style);
    }
  }

  private void setColumnStyle(
      ArrayList<String> list, XSSFCellStyle green_style, XSSFCellStyle red_style, XSSFCell c) {
    if (list == null || list.size() == 0) {
      c.setCellStyle(green_style);
    } else {
      c.setCellStyle(red_style);
    }
  }

  public static XSSFCellStyle SetCellStyle(XSSFWorkbook Workbook, int flag) {
    XSSFCellStyle style = Workbook.createCellStyle();
    style.setBorderBottom(BorderStyle.THIN);
    style.setBorderTop(BorderStyle.THIN);
    style.setBorderRight(BorderStyle.THIN);
    style.setBorderLeft(BorderStyle.THIN);
    style.setAlignment(HorizontalAlignment.CENTER);

    if (flag == 0) {
      style.setFillForegroundColor(IndexedColors.RED.getIndex());
    }
    if (flag == 1) {
      style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
    }
    if (flag == 2) {
      XSSFFont font = Workbook.createFont();
      font.setFontName("黑体");
      font.setFontHeightInPoints((short) 12); // 设置字体大小
      style.setFont(font); // 选择需要用到的字体格式
      style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
    }
    if (flag == 3) {
      style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
      style.setAlignment(HorizontalAlignment.LEFT);
    }

    if (flag == 4) {
      style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
    }
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    return style;
  }
}
