package com.welab.automation.framework.utils.estatement;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class JavaToPython {
  public static void main(String[] args) {
    run_python_1();
  }

  public static void run_python_2(String path) {
    try {
      String project_path = System.getProperty("user.dir");
      String cmds =
          String.format(
              "python "
                  + project_path
                  + "\\src\\main\\java\\com\\welab\\automation\\framework\\utils\\estatement\\shot_screen_for_estatement02.py %s",
              path);
      // 执行CMD命令
      System.out.println("Executing python script file now.");
      Process pcs = Runtime.getRuntime().exec(cmds);
      pcs.waitFor();
      // 定义Python脚本的返回值
      String result = null;
      // 获取CMD的返回流
      BufferedInputStream in = new BufferedInputStream(pcs.getInputStream()); // 字符流转换字节流
      BufferedReader br = new BufferedReader(new InputStreamReader(in)); // 这里也可以输出文本日志
      String lineStr = null;
      while ((lineStr = br.readLine()) != null) {
        result = lineStr; // Python 代码中print的数据就是返回值
      }
      // 关闭输入流
      br.close();
      in.close();
      System.out.println(result);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void run_python_1() {

    String data = "Python";
    try {
      String cmds =
          String.format(
              "python C:/Users/jeff.xie/PycharmProjects/MyProject/study/test_for_java.py %s", data);
      Process pr = Runtime.getRuntime().exec(cmds);
      BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
      String line;
      System.out.println("in.readLine():" + in.readLine());
      while ((line = in.readLine()) != null) {
        System.out.println("KKKKKK: " + line); // Python 代码中print的数据就是返回值
      }
      in.close();
      pr.waitFor();
      System.out.println("end");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void run_exe(String path) {
    try {
      String[] cmds = {"src\\main\\resources\\estatement\\shot_screen_for_estatement02.exe", path};
      Process pr = Runtime.getRuntime().exec(cmds);
      pr.waitFor();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
