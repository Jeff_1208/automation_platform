package com.welab.automation.framework.utils.entity.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.GlobalVar;
import io.qameta.allure.Allure;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Data
public class JsonEntity {
    private final Logger logger = LoggerFactory.getLogger(JsonEntity.class);
    private String rowID;
    private String description;
    private JSONObject jsonObject;
    private JSONArray sqlJsonArray;


    public JSONObject getPostBody() {
        return this.jsonObject.getJSONObject("body");
    }

    public JSONArray getPostBodys() {
        return this.jsonObject.getJSONArray("body");
    }

    public JSONObject getObjBody() {
        return this.jsonObject.getJSONObject("objBody");
    }
    public JSONObject getObjBody(String objname) {
        return this.jsonObject.getJSONObject(objname);
    }

    public JSONArray getObjBodys() {
        return this.jsonObject.getJSONArray("objBody");
    }

    public JSONObject getObjParam() {
        return this.jsonObject.getJSONObject("objParam");
    }


    @Override
    public String toString() {
        return "";
    }

    private JsonEntity(JsonEntityBuilder builder) {
        jsonObject = builder.jsonObject;
    }

    private JsonEntity() {
    }

    public void addCaseLink() {
        addAllueCaseLink("caseID", "allure.link.tms.pattern","case");
        addAllueCaseLink("issueID", "allure.link.issue.pattern", "issue");
//        addFeature();
    }
    

    private void addAllueCaseLink(String caseIDDesc, String property, String caseMark) {
        String caseID = null;

        List<String> caseList = new ArrayList<>();
        Object caseIDObject = this.jsonObject.get(caseIDDesc);

        if (caseIDObject != null) {
            if (caseIDObject instanceof String) {
                caseID = this.jsonObject.getString(caseIDDesc);
                caseList.add(caseID);
            }
            if (caseIDObject instanceof JSONArray) {
                caseList = JSONArray.parseArray(this.jsonObject.getString(caseIDDesc), String.class);
            }
        }

        for (String eachCaseLink : caseList) {
            caseID = System.getProperty(property).replace("{}", eachCaseLink);
            Allure.link(String.format("%s : %s",caseMark, eachCaseLink), caseID);
        }
    }

    /**
     * add feature
     */
    private void addFeature() {
        String feature = String.valueOf(GlobalVar.GLOBAL_VARIABLES.get("label"));
        List<String> featureList = new ArrayList<>();
        Object caseIDObject = this.jsonObject.get(feature);

        if (caseIDObject != null) {
            if (caseIDObject instanceof String) {
                feature = this.jsonObject.getString(feature);
                featureList.add(feature);
            }

            if (caseIDObject instanceof JSONArray) {
                featureList = JSONArray.parseArray(this.jsonObject.getString(feature), String.class);
            }
        }

        for (String eachFeature : featureList) {
            Allure.feature(eachFeature);
        }
    }

    private String getEpicContent() {
        String epicName = null;
        epicName = this.jsonObject.getString("epic");
        return epicName;
    }

    public JSONObject getExtractObject() {
        JSONObject extractJsonObject = null;
        if (this.jsonObject.containsKey("extract")) {
            extractJsonObject = this.jsonObject.getJSONObject("extract");
        }
        return extractJsonObject;
    }

    public JSONObject getValidateObject() {
        JSONObject extractJsonObject = null;
        if (this.jsonObject.containsKey("validate")) {
            extractJsonObject = this.jsonObject.getJSONObject("validate");
        }
        return extractJsonObject;
    }

    public void updateHeader() {
        JSONObject headerObject = this.jsonObject.getJSONObject("header");
        if (headerObject != null) {
            Set<String> headKeySet = headerObject.keySet();
            for (String headKey : headKeySet) {
                String headValue = headerObject.getString(headKey);
                GlobalVar.HEADERS.put(headKey, headValue);
            }
        }
    }

    public void deleteHeader() {
        JSONObject headerObject = this.jsonObject.getJSONObject("header");
        if (headerObject != null) {
            Set<String> headKeySet = headerObject.keySet();
            for (String headKey : headKeySet) {
                GlobalVar.HEADERS.remove(headKey);
            }
        }
        GlobalVar.HEADERS.put("tn", ""); //
    }
    /*
     "sql": {
        "tableName": "SEC_USER",
        "rows": {
          "USER_ID": "Eva_test_user",
          "ROLE_ID": "${existedRoleId}",
          "USER_ACCOUNT": "33333",
          "USER_NAME": "3333",
          "USER_PASSWORD": "3333",
          "USER_HISTORY_PASSWORD": "333",
          "LAST_MODIFIED_PASSWORD_TIME": "1",
          "USER_STATE": "1",
          "LAST_MODIFIED_BY": "1",
          "LAST_MODIFIED_TIME": "1",
          "IS_DELETED": "0",
          "USER_ALLCOUNTER": "0"
        }
      },
     */


    /**
     * @return
     */
    public JSONArray getSqlJsonArray() {
        if (this.jsonObject.containsKey("sql")) {
             this.sqlJsonArray  = this.jsonObject.getJSONArray("sql");
        }
        return this.sqlJsonArray;
    }


//    public void executeSqlRequest() {
//        JSONArray sqlJsonArray = this.getSqlJsonArray();
//        if (sqlJsonArray != null) {
//            this.setJsonObject(TestCaseUtils.replaceParameter(this.jsonObject));
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            for (int i = 0;  i < sqlJsonArray.size(); i++) {
//                SqlRequest sqlRequest = sqlJsonArray.getObject(i, SqlRequest.class);
//                if (sqlRequest != null) {
//                    sqlRequest.execute();
//                } else {
//
//                    throw new RuntimeException("SQL does not conform to specifications, please check");
//                }
//             }
//        }
//    }
//    public Map<String, List<String>> getSqlExpression() {
//        Map<String, List<String>> sqlMap = new HashMap<>();
//
//        if (this.getSqlJsonArray() != null) {
//            List<String> insertSqlList = new ArrayList<>();
//            List<String> deleteSqlList = new ArrayList<>();
//            StringBuffer deleteBuffer;
//            StringBuffer insertBuffer;
//            String tableName = "";
//
//            for (int i = 0; i < this.sqlJsonArray.size(); i++) {
//                deleteBuffer = new StringBuffer();
//                insertBuffer = new StringBuffer();
//
//                JSONObject singleSqlJsonObject = TestCaseUtils.replaceParameter(this.sqlJsonArray.getJSONObject(i));
//                String jsonType = singleSqlJsonObject.getString("type");
//                tableName = singleSqlJsonObject.getString("tableName");
//                List<String> typeList = JSONObject.parseArray(jsonType, String.class);
//
//                // consist of insert sql expression
//                if (typeList.contains("insert")) {
//                    insertBuffer.append(String.format("insert into %s ", tableName));
//                    getInsertStringBuffer(singleSqlJsonObject, insertBuffer);
//                }
//
//                //consist of delete sql expression
//                if (typeList.contains("delete")) {
//                    deleteBuffer.append(String.format("delete from %s where 1=1", tableName));
//                    getDeleteStringBuffer(singleSqlJsonObject, deleteBuffer);
//                }
//
//                insertSqlList.add(insertBuffer.toString());
//                deleteSqlList.add(deleteBuffer.toString());
//            }
//            sqlMap.put("insert", insertSqlList);
//            sqlMap.put("delete", deleteSqlList);
//        }
//        return sqlMap;
//    }

    private StringBuffer getDeleteStringBuffer(JSONObject singleSqlJsonObject, StringBuffer deleteBuffer) {
        JSONObject whereJsonObject = singleSqlJsonObject.getJSONObject("where");
        if (whereJsonObject != null) {
            Set<String> whereConditionSet = whereJsonObject.keySet();
            for (String whereCondition : whereConditionSet) {
                deleteBuffer.append(String.format(" and %s='%s'", whereCondition, whereJsonObject.getString(whereCondition)));
            }
        }
        return deleteBuffer;
    }


    private StringBuffer getInsertStringBuffer(JSONObject singleSqlJsonObject, StringBuffer insertBuffer) {
        JSONObject rowsJsonObject = singleSqlJsonObject.getJSONObject("rows");
        if (rowsJsonObject != null) {
            Set<String> fieldSet = rowsJsonObject.keySet();
            StringBuffer valueBuffer = new StringBuffer(" values (");
            if (fieldSet.size() > 0) {
                insertBuffer.append("(");
            }
            for (String field : fieldSet) {
                insertBuffer.append(field).append(",");
                Object fieldObject = rowsJsonObject.get(field);
                if (fieldObject instanceof String) {
                    valueBuffer.append("'").append(rowsJsonObject.getString(field)).append("'").append(",");
                } else if (fieldObject instanceof Integer) {
                    valueBuffer.append(rowsJsonObject.getIntValue(field)).append(",");
                } else if (fieldObject instanceof Float) {
                    valueBuffer.append(rowsJsonObject.getFloatValue(field)).append(",");
                } else if (fieldObject instanceof Double) {
                    valueBuffer.append(rowsJsonObject.getDoubleValue(field)).append(",");
                } else {
                    throw new RuntimeException("not support type");
                }
            }

            insertBuffer.deleteCharAt(insertBuffer.length() - 1);
            insertBuffer.append(")");
            valueBuffer.deleteCharAt(valueBuffer.length() - 1);
            valueBuffer.append(")");
            insertBuffer.append(valueBuffer);
        }
        return insertBuffer;
    }


    public JSONObject getContainObject() {
        JSONObject containsJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            containsJsonObject = validateObject.getJSONObject("contains");
        }
        return containsJsonObject;
    }

    /**
     *
     * @return
     */
    public JSONArray getContainFieldObject() {
        JSONArray containsFieldJsonArray = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            containsFieldJsonArray = validateObject.getJSONArray("containsField");
        }
        return containsFieldJsonArray;
    }

    public JSONObject getNotContainObject() {
        JSONObject notContainsJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            notContainsJsonObject = validateObject.getJSONObject("notContains");
        }
        return notContainsJsonObject;
    }

    public JSONObject checkNotExistFieldNames() {
        JSONObject notExistFieldNamesJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            notExistFieldNamesJsonObject = validateObject.getJSONObject("FieldNames");
        }
        return notExistFieldNamesJsonObject;
    }

    public JSONObject getSqlValidateObject() {
        JSONObject sqlValidateObjec = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            sqlValidateObjec = validateObject.getJSONObject("sql");
        }
        return sqlValidateObjec;
    }

    public JSONArray getGreatThanObject() {
        JSONArray greatThanJsonArray = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            greatThanJsonArray = validateObject.getJSONArray("gt");
        }
        return greatThanJsonArray;
    }


    public JSONObject getGreatThanOrEqualObject() {
        JSONObject greatThanOrEqualJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            greatThanOrEqualJsonObject = validateObject.getJSONObject("gte");
        }
        return greatThanOrEqualJsonObject;
    }

    public JSONObject getLessThanObject() {
        JSONObject lessThanJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            lessThanJsonObject = validateObject.getJSONObject("lt");
        }
        return lessThanJsonObject;
    }

    public JSONObject getLessThanOrEqualOobject() {
        JSONObject lessThanOrEqualToJsonObject = null;
        JSONObject validateObject = this.getValidateObject();
        if (validateObject != null) {
            lessThanOrEqualToJsonObject = validateObject.getJSONObject("lte");
        }
        return lessThanOrEqualToJsonObject;
    }


    public static final class JsonEntityBuilder {
        private String rowID;
        private String description;
        private JSONObject jsonObject;

        public JsonEntityBuilder() {
        }

        public JsonEntityBuilder withRowID(String val) {
            rowID = val;
            return this;
        }

        public JsonEntityBuilder withDescription(String val) {
            description = val;
            return this;
        }

        public JsonEntityBuilder withJsonObject(JSONObject val) {
            jsonObject = val;
            return this;
        }


        public JsonEntity build() {
            return new JsonEntity(this);
        }
    }
}
