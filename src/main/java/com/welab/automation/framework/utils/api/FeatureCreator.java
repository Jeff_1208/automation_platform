

package com.welab.automation.framework.utils.api;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.entity.api.TestCaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Generate feature file from test data in excel */
public class FeatureCreator {

  private static Logger logger = LoggerFactory.getLogger(FeatureCreator.class);
  // generated file path
  private String path = GlobalVar.FEATURE_FILE_PATH;
  // template path
  private String templatePath = GlobalVar.TEMPLATE_FILE_PATH;
  private String testData = GlobalVar.TEST_DATA_FILE_PATH;
  private String filenameTemp = GlobalVar.FEATURE_FILE_PATH; // feature文件
  private CSVFileReader csvFileReader = new CSVFileReader();
  private static final String EXT = ".feature";

    public FeatureCreator(String env) {
        if (!env.isEmpty())
            filenameTemp = filenameTemp + "/" + GlobalVar.GLOBAL_VARIABLES.get("FUNCTION_TYPE") + "/";
    }

    /**
     * create feature file,copy text from template to this feature file
     *
     * @param fileName     filename
     * @param templateName content
     * @return if create success return true
     */
    public void createFile(String fileName, String templateName) {
        File file = null;
        System.out.println(fileName);
        file = new File(fileName);
        try {
            if (file.exists()) {
                if (file.delete()) {
                    logger.info("Delete file {} successfully", fileName);
                }
            }
            if (file.createNewFile()) {
                logger.info("Create new file {} successfully.", fileName);
            }
            logger.info("filenameTemp is:" + filenameTemp);
            String templateContent = readtxtFile(templatePath + templateName);
            String module = file.getName().split("\\.")[0];
            templateContent = templateContent.replace("<module>", module);
            writeToFeature(templateContent, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  public boolean generatFeatureByCaseNo(String dirPath, String templateName, int caseNo) {
    TestCaseEntity testCaseEntity = null;
    try {
      testCaseEntity = csvFileReader.readCsvIndex(dirPath, caseNo);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (testCaseEntity != null) {
      String featureName = testCaseEntity.getCaseName();
      if (featureName.isEmpty()) {
        try {
          throw new Exception("please give valid feature name");
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      // create file by caseName
      createFile(featureName, templateName);
    }
    if (testCaseEntity != null) {
      String featureName = testCaseEntity.getCaseName();
      if (featureName.isEmpty()) {
        try {
          throw new Exception("please give valid feature name");
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      String feature = filenameTemp + featureName + EXT;
      // create file by caseName
      String datalist = updateFeatureWithRequestEntity(testCaseEntity);
      appendToFeature(datalist, feature);
    }

    return true;
  }

  private void writeToFeature(String content, String featurePath) {
    File feature = null;
    feature = new File(featurePath);
    if (!feature.exists()) {
      try {
        if (feature.createNewFile()) {
          logger.info("Create feature file {} successfully.", featurePath);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try (FileWriter writer = new FileWriter(feature)) {
      writer.write(content);
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void appendToFeature(String content, String featurePath) {
    File feature = null;
    feature = new File(featurePath);
    if (!feature.exists()) {
      try {
        if (feature.createNewFile()) {
          logger.info("Create feature file {} successfully.", featurePath);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try (FileWriter writer = new FileWriter(feature, true); ) {
      writer.append(content);
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

    public Set<String> createFileByModule(List<TestCaseEntity> list, String templateName) {
        if (GlobalVar.GLOBAL_VARIABLES.get("is_loans") != null) {
            filenameTemp = GlobalVar.LOANS_FEATURE_FILE_PATH;
        }
        String finalTemplate = templateName;
        Set<String> moduleList = new HashSet<>();
        for (TestCaseEntity r : list) {
            String featureName = r.getModule();
            if (featureName.startsWith("#")) continue;
            String template = r.getTemplate();
            if (!template.isEmpty()) {
                finalTemplate = template + EXT;
            }
            if (!moduleList.contains(featureName)) {
                moduleList.add(featureName);
                System.out.println(filenameTemp);
                System.out.println(featureName);
                String feature = filenameTemp + featureName + EXT;
                createFile(feature, finalTemplate);
            }
        }
        return moduleList;
    }

  public boolean generatFeatureAllCases(String dirPath, String templateName) throws IOException {
    String finalTemplate = templateName;
    List<TestCaseEntity> list = null;
    try {
      list = csvFileReader.readCSV(dirPath);
    } catch (IOException e) {
      e.printStackTrace();
    }

    createFileByModule(list,templateName);

    for (TestCaseEntity l : list) {
      String featureName = l.getModule();
      if (featureName.startsWith("#")) continue;
      String feature = filenameTemp + featureName + EXT;
      // append datalist by module
      String datalist = updateFeatureWithRequestEntity(l);
      appendToFeature(datalist, feature);
    }
    return true;
  }

  public boolean generatFeatureCases(String sourceFile,String destinationFolder, String templateName) throws IOException {
    String finalTemplate = templateName;
    List<TestCaseEntity> list = null;
    try {
      list = csvFileReader.readCSVFile(sourceFile);
    } catch (IOException e) {
      e.printStackTrace();
    }

    for (TestCaseEntity r : list) {
      String featureName = r.getModule();
      if (featureName.startsWith("#")) continue;
      String template = r.getTemplate();
      // if template is not empty,then use this assigned template to generate feature file
      if (!template.isEmpty()) {
        finalTemplate = template + EXT;
      } else {
        finalTemplate = templateName;
      }
      String feature = filenameTemp + featureName + EXT;
      // create feature file then write with template.feature
      createFile(feature, finalTemplate);
    }

    for (TestCaseEntity l : list) {
      String featureName = l.getModule();
      if (featureName.startsWith("#")) continue;
      String feature = filenameTemp + featureName + EXT;
      // append datalist by module
      String datalist = updateFeatureWithRequestEntity(l);
      appendToFeature(datalist, feature);
    }
    return true;
  }

  private String updateFeatureWithRequestEntity(TestCaseEntity r) {
    String dataList = readtxtFile(templatePath + "dataTable.feature");
    String updateContent =
        dataList
            .replace("@module", r.getModule())
            .replace("@caseNo", r.getCaseNo())
            .replace("@url", r.getUrl())
            .replace("@variables", r.getVariables())
            .replace("@method", r.getMethod())
            .replace("@headers", r.getHeaders())
            .replace("@params", r.getParams())
            .replace("@code", r.getCode())
            .replace("@caseName", r.getCaseName())
            .replace("@bodyFile", r.getJsonFile())
            .replace("@response", r.getResponse())
            .replace("@byParameter", r.getByParameter())
            .replace("@isFile",r.getIsFile())
                .replace("@account",r.getAccount())
                .replace("@timeout",r.getTimeout())
            .replace("@validation", r.getValidation().replaceAll("\r|\n", ""))
                .replace("@sql",r.getSql());
    return updateContent;
  }

  /**
   * read context
   *
   * @param filename
   * @return
   */
  private String readtxtFile(String filename) {
    String lineTxt = null;
    StringBuffer stringBuffer = null;
    File file = new File(filename);
    if (file.isFile() && file.exists()) {
      try (InputStreamReader read = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
        BufferedReader bufferedReader = new BufferedReader(read);
        stringBuffer = new StringBuffer();
        while ((lineTxt = bufferedReader.readLine()) != null) {
          stringBuffer.append(lineTxt);
          stringBuffer.append(System.getProperty("line.separator"));
        }
      } catch (Exception e) {
        logger.info("readtxtFile:read file errorly");
        e.printStackTrace();
      }
    } else {
      logger.info("readtxtFile:can't find the file");
    }
    return String.valueOf(stringBuffer);
  }

  /**
   * clear text
   *
   * @param fileName
   */
  public void clearInfoForFile(String fileName) {
    File file = new File(fileName);
    try (FileWriter fileWriter = new FileWriter(file)) {
      fileWriter.write("");
      fileWriter.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) throws IOException {
    //            String inputFilePath =
    // "E:\\xcollection\\collectioncucumbertest\\src\\test\\resources\\template\\featuretemplate.txt";
    //            String outputFilePath =
    // "E:\\xcollection\\collectioncucumbertest\\src\\test\\resources\\feature\\" +
    // "myfile.feature";
    //            boolean b = copyTemplate(inputFilePath,outputFilePath);
    //            System.out.println("b:"+b);
    //        generatFeatureAllCases("testData.csv", "templateDefault.feature");
  }
}
