package com.welab.automation.framework.utils;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.projects.AndroidThread;
import com.welab.automation.projects.loans.pages.mobile.LoginPage;
import lombok.SneakyThrows;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;

import static com.welab.automation.framework.GlobalVar.TEST_REPORT_ZEPHYRSCALE_PATH;

public class ZephyrScaleUtils {
    private static AndroidThread androidThread;

    public static void startAndroidThread(){
        if (isRunBash() && !isRunAndroidCommand()){
            androidThread = new AndroidThread();
            androidThread.start();
        }
    }

    public void endAndroidThread(){
        if (isRunBash() && !isRunAndroidCommand()){
            androidThread.interrupt();
        }
    }

    public static boolean isRunAndroidCommand(){
        String flag = System.getProperty("mobile").toUpperCase();
        if (flag.contains("ANDROID")) return true;
        return false;
    }

    /**
     * need set runBash=true in properties file
     * @return
     */
    public static boolean isRunBash(){
        if(GlobalVar.GLOBAL_VARIABLES.get("runBash")!=null && GlobalVar.GLOBAL_VARIABLES.get("runBash").contains("true")){
            return true;
        }
        return false;
    }

    public boolean isMobile() {
        if (System.getProperty("mobile")==null) return false;
        return true;
    }

    public void storeAppVersion() {
        StringBuffer stringBuffer = new StringBuffer();
        if(isMobile()){
            stringBuffer.append("appVersion=" + GlobalVar.GLOBAL_VARIABLES.get("appVersion") + "\n");
            String mobileOs=System.getProperty("mobile");
            stringBuffer.append("mobileOs=" + mobileOs.toUpperCase() + "\n");
            stringBuffer.append("mobileVersion=" + GlobalVar.GLOBAL_VARIABLES.get(mobileOs + ".platformVersion") + "\n");
            stringBuffer.append("mobileType=" + GlobalVar.GLOBAL_VARIABLES.get(mobileOs + ".type") + "\n");
        }
        String projectPath = System.getProperty("user.dir");
        String absolutePath = projectPath + "/" +GlobalVar.GLOBAL_VARIABLES.get("TestReportPath");
        absolutePath = absolutePath.replace("\\", "/");
        stringBuffer.append("TestReportPath=" + GlobalVar.GLOBAL_VARIABLES.get("TestReportPath") + "\n");
        stringBuffer.append("TestReportAbsolutePath=" + absolutePath + "\n");
        for (String key : GlobalVar.CASE_RESULT.keySet()) {
            boolean value = GlobalVar.CASE_RESULT.get(key);
            stringBuffer.append(key+"=" + value + "\n");
        }
        try{
            FileWriter file = new FileWriter(TEST_REPORT_ZEPHYRSCALE_PATH + "/appversion.txt");
            String content = stringBuffer.toString();
            file.write(content);
            System.out.println(content);
            file.close();
        }catch (Exception e){
        }
    }

    public void getUserAndPasswordFromTxtAndPutInGlobalVar() {
        if(GlobalVar.GLOBAL_VARIABLES.get("isLoans")!=null && GlobalVar.GLOBAL_VARIABLES.get("isLoans").contains("true")){
            String data = deleteFirstLineForTxt();
            String user = data.split(",")[0].trim();
            String password = data.split(",")[1].trim();
            GlobalVar.GLOBAL_VARIABLES.put("userName",user);
            GlobalVar.GLOBAL_VARIABLES.put("password",password);
        }
    }

    public String deleteFirstLineForTxt(){
        String path = "src/main/resources/suite/loans/account.txt";
        String data = "";
        try {
            RandomAccessFile raf = null;
            raf = new RandomAccessFile(path, "rw");
            //Initial write position
            long writePosition = raf.getFilePointer();
            data = raf.readLine().replace("\n", "").replace("\r", "").trim();
            // Shift the next lines upwards.
            long readPosition = raf.getFilePointer();

            byte[] buff = new byte[1024];
            int n;
            while (-1 != (n = raf.read(buff))) {
                raf.seek(writePosition);
                raf.write(buff, 0, n);
                readPosition += n;
                writePosition += n;
                raf.seek(readPosition);
            }
            raf.setLength(writePosition);
            raf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public void putPhoneType() throws Exception {
        if((System.getProperty("mobile")!=null) && (System.getProperty("mobile").toUpperCase().contains("ANDROID"))){
            String type = getAndroidPhoneType();
            if(type.contains("22111317G")){type= "redmi Note 12";}
            if(type.contains("SM_G7810")){type= "SAMSUNG S20";}
            if(type != null){
                GlobalVar.GLOBAL_VARIABLES.put("android.type",type);
            }
        }
        if((System.getProperty("mobile")!=null) && (System.getProperty("mobile").toUpperCase().contains("IOS"))){
            String type = getIosPhoneType();
            if(type != null){
                GlobalVar.GLOBAL_VARIABLES.put("ios.type",type);
            }
        }
    }

    @SneakyThrows
    public String getAndroidPhoneType() throws URISyntaxException, IOException {
        String phoneType="";
        BufferedReader br = getCommandResponse("adb devices -l");
        String lineStr = null;
        while ((lineStr = br.readLine()) != null) {
            System.out.println(lineStr);
            if(lineStr.contains("model:")){
                int index = lineStr.indexOf("model:");
                String temp = lineStr.substring(index+"model:".length()).trim();
                phoneType = temp.split(" ")[0].trim();
            }
        }
        System.out.println("Android phoneType: "+phoneType);
        return phoneType;
    }

    @SneakyThrows
    public String getIosPhoneType() throws URISyntaxException, IOException {
        String phoneType = "";
        BufferedReader br = getCommandResponse("ideviceinfo -k ProductType");
        String lineStr = null;
        while ((lineStr = br.readLine()) != null) {
            phoneType = lineStr.replace("\n","").replace("\r","").trim();
        }
        HashMap<String, String> map = putIphoneType();
        System.out.println("IOS phoneType: "+phoneType);
        String type = map.get(phoneType.trim());
        System.out.println("Type: "+type);
        return type;
    }

    @SneakyThrows
    public BufferedReader getCommandResponse(String command) throws URISyntaxException, IOException{
        String cmds = String.format(command);
        System.out.println(cmds);
        Process pcs = Runtime.getRuntime().exec(cmds);
        pcs.waitFor();
        Thread.sleep(1000);
        BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
        BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
        return br;
    }

    public HashMap putIphoneType(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("iPhone3,1", "iPhone 4");
        map.put("iPhone3,2", "Verizon iPhone 4");
        map.put("iPhone3,3", "iPhone 4");
        map.put("iPhone4,1", "iPhone 4S");
        map.put("iPhone5,1", "iPhone 5");
        map.put("iPhone5,2", "iPhone 5 (GSM+CDMA)");
        map.put("iPhone5,3", "iPhone 5c (GSM)");
        map.put("iPhone5,4", "iPhone 5c (GSM+CDMA)");
        map.put("iPhone6,1", "iPhone 5s (GSM)");
        map.put("iPhone6,2", "iPhone 5s (GSM+CDMA)");
        map.put("iPhone7,1", "iPhone 6 Plus");
        map.put("iPhone7,2", "iPhone 6");
        map.put("iPhone8,1", "iPhone 6s");
        map.put("iPhone8,2", "iPhone 6s Plus");
        map.put("iPhone8,4", "iPhone SE");
        map.put("iPhone9,1", "iPhone 7");
        map.put("iPhone9,2", "iPhone 7 Plus");
        map.put("iPhone9,3", "iPhone 7");
        map.put("iPhone9,4", "iPhone 7 Plus");
        map.put("iPhone10,1", "iPhone 8");
        map.put("iPhone10,2", "iPhone 8 Plus");
        map.put("iPhone10,3", "iPhone X");
        map.put("iPhone10,4", "iPhone 8");
        map.put("iPhone10,5", "iPhone 8 Plus");
        map.put("iPhone10,6", "iPhone X");
        map.put("iPhone11,8", "iPhone XR");
        map.put("iPhone11,2", "iPhone XS");
        map.put("iPhone11,6", "iPhone XS Max");
        map.put("iPhone11,4", "iPhone XS Max");
        map.put("iPhone12,1", "iPhone 11");
        map.put("iPhone12,3", "iPhone 11 Pro");
        map.put("iPhone12,5", "iPhone 11 Pro Max");
        map.put("iPhone12,8", "iPhone SE");
        map.put("iPhone13,1", "iPhone 12 mini");
        map.put("iPhone13,2", "iPhone 12");
        map.put("iPhone13,3", "iPhone 12 Pro");
        map.put("iPhone13,4", "iPhone 12 Pro Max");
        map.put("iPhone14,2", "iPhone 13 Pro");
        map.put("iPhone14,3", "iPhone 13 Pro Max");
        map.put("iPhone14,4", "iPhone 13 mini");
        map.put("iPhone14,5", "iPhone 13");
        map.put("iPhone14,6", "iPhone SE");
        map.put("iPhone14,7", "iPhone 14");
        map.put("iPhone14,8", "iPhone 14 Plus");
        map.put("iPhone15,2", "iPhone 14 Pro");
        map.put("iPhone15,3", "iPhone 14 Pro Max");
        map.put("iPad1,1", "iPad");
        map.put("iPad1,2", "iPad 3G");
        map.put("iPad2,1", "iPad 2 (WiFi)");
        map.put("iPad2,2", "iPad 2");
        map.put("iPad2,3", "iPad 2 (CDMA)");
        map.put("iPad2,4", "iPad 2");
        map.put("iPad2,5", "iPad Mini (WiFi)");
        map.put("iPad2,6", "iPad Mini");
        map.put("iPad2,7", "iPad Mini (GSM+CDMA)");
        map.put("iPad3,1", "iPad 3 (WiFi)");
        map.put("iPad3,2", "iPad 3 (GSM+CDMA)");
        map.put("iPad3,3", "iPad 3");
        map.put("iPad3,4", "iPad 4 (WiFi)");
        map.put("iPad3,5", "iPad 4");
        map.put("iPad3,6", "iPad 4 (GSM+CDMA)");
        map.put("iPad4,1", "iPad Air (WiFi)");
        map.put("iPad4,2", "iPad Air (Cellular)");
        map.put("iPad4,3", "iPad Air");
        map.put("iPad4,4", "iPad Mini 2 (WiFi)");
        map.put("iPad4,5", "iPad Mini 2 (Cellular)");
        map.put("iPad4,6", "iPad Mini 2");
        map.put("iPad4,7", "iPad Mini 3");
        map.put("iPad4,8", "iPad Mini 3");
        map.put("iPad4,9", "iPad Mini 3");
        map.put("iPad5,1", "iPad Mini 4 (WiFi)");
        map.put("iPad5,2", "iPad Mini 4 (LTE)");
        map.put("iPad5,3", "iPad Air 2");
        map.put("iPad5,4", "iPad Air 2");
        map.put("iPad6,3", "iPad Pro 9.7");
        map.put("iPad6,4", "iPad Pro 9.7");
        map.put("iPad6,7", "iPad Pro 12.9");
        map.put("iPad6,8", "iPad Pro 12.9");
        map.put("iPad6,11", "iPad 5th");
        map.put("iPad6,12", "iPad 5th");
        map.put("iPad7,1", "iPad Pro 12.9 2nd");
        map.put("iPad7,2", "iPad Pro 12.9 2nd");
        map.put("iPad7,3", "iPad Pro 10.5");
        map.put("iPad7,4", "iPad Pro 10.5");
        map.put("iPad7,5", "iPad 6th");
        map.put("iPad7,6", "iPad 6th");
        map.put("iPad8,1", "iPad Pro 11");
        map.put("iPad8,2", "iPad Pro 11");
        map.put("iPad8,3", "iPad Pro 11");
        map.put("iPad8,4", "iPad Pro 11");
        map.put("iPad8,5", "iPad Pro 12.9 3rd");
        map.put("iPad8,6", "iPad Pro 12.9 3rd");
        map.put("iPad8,7", "iPad Pro 12.9 3rd");
        map.put("iPad8,8", "iPad Pro 12.9 3rd");
        map.put("iPad11,1", "iPad mini 5th");
        map.put("iPad11,2", "iPad mini 5th");
        map.put("iPad11,3", "iPad Air 3rd");
        map.put("iPad11,4", "iPad Air 3rd");
        map.put("iPad11,6", "iPad 8th");
        map.put("iPad11,7", "iPad 8th");
        map.put("iPad12,1", "iPad 9th");
        map.put("iPad12,2", "iPad 9th");
        map.put("iPad14,1", "iPad mini 6th");
        map.put("iPad14,2", "iPad mini 6th");
        map.put("iPod1,1", "iPod Touch 1G");
        map.put("iPod2,1", "iPod Touch 2G");
        map.put("iPod3,1", "iPod Touch 3G");
        map.put("iPod4,1", "iPod Touch 4G");
        map.put("iPod5,1", "iPod Touch (5 Gen)");
        map.put("iPod7,1", "iPod Touch (6 Gen)");
        map.put("iPod9,1", "iPod Touch (7 Gen)");
        map.put("i386", "Simulator");
        map.put("x86_64", "Simulator");
        return map;
    }

    public void checkZephyrScalePath(){
        File f = new File(TEST_REPORT_ZEPHYRSCALE_PATH);
        if(!f.exists()){
            f.mkdirs();
        }
    }

    public void storeLoansHkid() {
        checkZephyrScalePath();
        StringBuffer stringBuffer = new StringBuffer();
        try{
            FileWriter file = new FileWriter(TEST_REPORT_ZEPHYRSCALE_PATH + "/loans.txt");
            stringBuffer.append("hkid="+GlobalVar.GLOBAL_VARIABLES.get("hkid"));
            String content = stringBuffer.toString();
            file.write(content);
            System.out.println(content);
            file.close();
        }catch (Exception e){
        }
    }

    public String getLoansHkid(){
        String hkid = "";
        try{
            File f = new File(TEST_REPORT_ZEPHYRSCALE_PATH + "/loans.txt");
            FileReader bytes = new FileReader(f);
            BufferedReader chars = new BufferedReader(bytes);
            String row = "";
            while((row=chars.readLine())!=null) {
                System.out.println(row);
                row.replaceAll("\r", "").replaceAll("\n", "");
                if(row.contains("hkid")&row.contains("=")){
                    hkid = row.split("=")[1].trim();
                }
            }
        } catch (Exception e) {
        }
        if(hkid.trim().length()>3) {
            GlobalVar.GLOBAL_VARIABLES.put("hkid", hkid);
        }
        return hkid;
    }

    public static void updateForHkidInProperties(){
        try {
            String path = "src/main/resources/web_loans.properties";
            StringBuffer stringBuffer = new StringBuffer();
            File f = new File(path);
            FileReader bytes = new FileReader(f);
            BufferedReader chars = new BufferedReader(bytes);
            String row ="";
            boolean flag = false;
            while((row=chars.readLine())!=null) {
                System.out.println(row);
                if(row.contains("hkid")){
                    flag= true;
                    stringBuffer.append("hkid="+ GlobalVar.GLOBAL_VARIABLES.get("hkid")+"\n");
                }else {
                    stringBuffer.append(row+"\n");
                }
            }
            if(!flag){
                stringBuffer.append("hkid="+ GlobalVar.GLOBAL_VARIABLES.get("hkid"));
            }
            FileWriter file = new FileWriter(path,false);//true表示追加数据
            file.write(stringBuffer.toString());
            file.close();
        } catch (Exception e) {
        }
    }

}
