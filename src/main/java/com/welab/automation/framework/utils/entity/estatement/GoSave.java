package com.welab.automation.framework.utils.entity.estatement;


public class GoSave{
	//Core Account中GoSave转账记录
	private int gosave_trans_account_number;
	public int getGosave_trans_account_number() {
		return gosave_trans_account_number;
	}
	public void setGosave_trans_account_number(int gosave_trans_account_number) {
		this.gosave_trans_account_number = gosave_trans_account_number;
	}
	private String Year;
	private String Month;

	private String Core_Account_GoSave_Type;
	//GoSave现有记录， ID与交易记录公用
	private String ID;
	private String From;
	private String To;
	private String Rate;
	private String Start_Date;
	private String End_Date;
	private String Type;
	

	//GoSave交易记录
	private String GoSave_Trans_Ref;
	private String Description;
	private String Amount;
	private boolean is_amount_have_right_single;
	private String gosave_trans_Date;

	
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(String start_Date) {
		Start_Date = start_Date;
	}
	public String getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}
	public String getGoSave_Trans_Ref() {
		return GoSave_Trans_Ref;
	}
	public void setGoSave_Trans_Ref(String goSave_Trans_Ref) {
		GoSave_Trans_Ref = goSave_Trans_Ref;
	}
	public String getGosave_trans_Date() {
		return gosave_trans_Date;
	}
	public void setGosave_trans_Date(String gosave_trans_Date) {
		this.gosave_trans_Date = gosave_trans_Date;
	}
	
	public boolean isIs_amount_have_right_single() {
		return is_amount_have_right_single;
	}
	public void setIs_amount_have_right_single(boolean is_amount_have_right_single) {
		this.is_amount_have_right_single = is_amount_have_right_single;
	}
	public String getCore_Account_GoSave_Type() {
		return Core_Account_GoSave_Type;
	}
	public void setCore_Account_GoSave_Type(String core_Account_GoSave_Type) {
		Core_Account_GoSave_Type = core_Account_GoSave_Type;
	}
	public String getYear() {
		return Year;
	}
	public void setYear(String year) {
		Year = year;
	}
	public String getMonth() {
		return Month;
	}
	public void setMonth(String month) {
		Month = month;
	}	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getRate() {
		return Rate;
	}
	public void setRate(String rate) {
		Rate = rate;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}

	
}
