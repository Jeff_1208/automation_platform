package com.welab.automation.framework.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;


import static com.welab.automation.framework.utils.Utils.*;
import static com.welab.automation.framework.utils.entity.app.DeviceAppEntity.ANDROID_SDK;

public class ADB {

    public static String runCommand(String command){
        String output = null;
        try{
            Scanner scanner = new Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");
            if(scanner.hasNext()) output = scanner.next();
        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
        return output;
    }

    public static String command(String command){
        Path adbPath = Paths.get(ANDROID_SDK, "platform-tools", "adb");
        if(command.startsWith("adb")) {
            command = command.replace("adb", adbPath.toString());
            logPass("Execute adb command: " + command + " successful.");
        } else {
            logFail("Only support adb command currently. the command " + command + " will not be executed.");;
            throw new RuntimeException("This method is designed to run ADB commands only!");
        }

        String output = runCommand(command);
        if(output == null) return "";
        else return output.trim();
    }

    public static ArrayList getConnectedDevices(){
        ArrayList devices = new ArrayList();
        String output = command("adb devices");
        for(String line : output.split("\n")){
            line = line.trim();
            if(line.endsWith("device")) devices.add(line.replace("device", "").trim());
        }
        return devices;
    }

    public static void adbPushFile(String source, String target){
        command("adb push " + source + " " + target);
        logPass("adb push file form  " + source + " to " + target );
        broadCastFile(target);
        logPass("broad cast file " + target );
    }

    public static void adbDeleteFile(String target){
        command("adb shell rm  -rf "+target);
        logPass("adb delete files in  " + target);
    }

    private static void broadCastFile(String filePath){
        command("adb shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d  file://"+filePath);
    }
    public static void inputText(String text){
        command("adb shell input text "+text);
        logPass("adb shell input text " + text);
    }
}
