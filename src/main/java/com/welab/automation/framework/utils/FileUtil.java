
package com.welab.automation.framework.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.welab.automation.framework.GlobalVar;
import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.VideoAttributes;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.util.Strings;
import org.yaml.snakeyaml.Yaml;
import ws.schild.jave.MultimediaObject;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FileUtil {
  private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

  /**
   * This static method parse yaml file to json Object, so data can be use by JSON Tools like
   * Gson/Fastjson/JsonPath etc;
   *
   * <p>1. Deserialize yaml by SneakYAML, so we can support aliases and anchors in yaml. 2. Convert
   * the YAMLobject to JSONObject
   *
   * @param filePath A source YAML file
   * @return A JSONObject (defined by fastjson)
   */
  public static JSONObject yamlToJsonObj(String filePath) throws IOException {
    Yaml yaml = new Yaml();
    InputStream fileSteam = new FileInputStream(filePath);
    Object yamlObj = yaml.load(fileSteam);
    JSONObject jsonObject = (JSONObject) JSON.toJSON(yamlObj);
    return jsonObject;
  }

  private JsonReader getReaderByFileName(String jsonName) {
    try {
      logger.info("The json name is" + jsonName);
      String testDataResourcePath = PropertiesReader.getInstance().getProperty("appTestData");
      JsonReader reader = new JsonReader(new FileReader(testDataResourcePath + jsonName));
      return reader;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String readJsonFile(String fileName) {
    Gson gson = new Gson();
    String json = "";
    File file = new File(fileName);
    try (Reader reader = new InputStreamReader(new FileInputStream(file), "utf-8")) {

      int ch = 0;
      StringBuffer buffer = new StringBuffer();
      while ((ch = reader.read()) != -1) {
        buffer.append((char) ch);
      }
      json = buffer.toString();
      logger.info("The json file content is: {}", json);
      return json;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Map<String, Object> readJsonObjectFileToMap(String fileName) {
    String jsonStr = readJsonFile(fileName);
    Gson gson = new Gson();
    Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
    logger.info("The map is: {}", map);
    return map;
  }

  public static List<ArrayList> readJsonArrayFileToList(String fileName) {
    String jsonStr = readJsonFile(fileName);
    Gson gson = new Gson();
    List<ArrayList> list = gson.fromJson(jsonStr, ArrayList.class);
    logger.info("The array list is: {}", list);
    return list;
  }

  public static JSONObject getJsonObj(String filePath) {
    try {
      InputStream is = new FileInputStream(filePath);
      JSONObject jsonObject =
          JSONObject.parseObject(
              IOUtils.toString(is, StandardCharsets.UTF_8), Feature.OrderedField);
      return jsonObject;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void writeObj(String fileName, String content) {
    File file = new File(fileName);
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      writer.write(content);
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String readFileWithWrap(String fileName) {
    String lineTxt = null;
    StringBuilder stringBuilder = new StringBuilder();
    File file = new File(fileName);
    if (file.isFile() && file.exists()) {

      try (InputStreamReader read = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
        BufferedReader bufferedReader = new BufferedReader(read);
        while ((lineTxt = bufferedReader.readLine()) != null) {
          stringBuilder.append(lineTxt);
          stringBuilder.append(System.getProperty("line.separator"));
        }
      } catch (Exception e) {
        logger.error("Read file error");
        e.printStackTrace();
      }
    } else {
      logger.error("Can't find the given file");
    }
    return stringBuilder.toString();
  }

  public static String readFileWithoutWrap(String fileName) {
    String lineTxt = null;
    StringBuilder stringBuilder = new StringBuilder();
    File file = new File(fileName);
    if (file.isFile() && file.exists()) {
      try (InputStreamReader read = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
        BufferedReader bufferedReader = new BufferedReader(read);
        while ((lineTxt = bufferedReader.readLine()) != null) {
          stringBuilder.append(lineTxt);
        }
      } catch (Exception e) {
        logger.error("Read file error");
        e.printStackTrace();
      }
    } else {
      logger.error("Can't find the given file or just is json data");
    }
    return stringBuilder.toString();
  }

  public static void writeFile(String content, String filePath) {
    try {
      File file = new File(filePath);
      FileOutputStream fos = null;
      if (!file.exists()) {
        file.createNewFile();
        fos = new FileOutputStream(file, true);
      } else {
        fos = new FileOutputStream(file, true);
      }
      OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
      osw.write(content + "\r\n");
      osw.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static boolean createFile(String fileName) {
    File file = new File(fileName);
    if (!file.exists()) {
      try {
        if (file.createNewFile()) {
          logger.info("Create new file {} successfully", fileName);
        }
      } catch (Exception e) {
        logger.error("Can not create the file {}", fileName);
        e.printStackTrace();
      }
    }
    return file.exists();
  }

  public static boolean createDirectory(String directoryName) {
    File file = new File(directoryName);
    if (!file.exists()) {
      try {
        if (file.mkdir()) {
          logger.info("Create new directory {} successfully", directoryName);
        }
      } catch (Exception e) {
        logger.error("Can not create the directory {}", directoryName);
        e.printStackTrace();
      }
    }
    return file.exists();
  }

  public static boolean copyFile(String sourceFileName, String targetFileName) {
    File source = new File(sourceFileName);
    File target = new File(targetFileName);
    if (!source.exists() || !source.isFile()) {
      logger.error("Source {} does not exist or is not a file", sourceFileName);
      return false;
    }
    if (target.exists()) {
      target.delete();
    }
    boolean flag = false;
    try {
      Files.copy(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
      flag = target.exists();
      logger.info("Source file {} is copied to {}", sourceFileName, targetFileName);
    } catch (Exception e) {
      logger.error("Can not copy the file {} to {}", sourceFileName, targetFileName);
    }
    return flag;
  }

  public static boolean copyDirectory(String sourceFolderName, String targetFolderName) {
    File source = new File(sourceFolderName);
    File target = new File(targetFolderName);
    if (!source.exists() || !source.isDirectory()) {
      logger.error("Source {} does not exist or is not a directory", sourceFolderName);
      return false;
    }
    boolean flag = false;
    try {
      FileUtils.copyDirectory(source, target, true);
      flag = target.exists();
      logger.info("Source directory {} is copied to {}", sourceFolderName, targetFolderName);
    } catch (Exception e) {
      logger.error("Can not copy the directory {} to {}", sourceFolderName, targetFolderName);
    }
    return flag;
  }

  public static boolean deleteFile(String filePath) {
    File file = new File(filePath);
    if (!file.exists() || !file.isFile()) {
      logger.error("{} does not exist or is not a file", filePath);
      return false;
    }
    boolean isDeleted = false;
    try {
      file.delete();
      isDeleted = !file.exists();
      logger.info("Delete the file {} successfully", filePath);
    } catch (Exception e) {
      logger.error("Can not delete the file {}", filePath);
    }
    return isDeleted;
  }

  public static boolean deleteDirectoryAndChildren(String sPath) {
    File directory = new File(sPath);
    if (!directory.exists() || !directory.isDirectory()) {
      logger.error("{} does not exist or is not a directory", sPath);
      return false;
    }
    boolean flag = false;
    try {
      FileUtils.deleteDirectory(directory);
      flag = !directory.exists();
      logger.info("Delete the directory {} successfully", sPath);
    } catch (Exception e) {
      logger.error("Can not delete the directory {}", sPath);
    }
    return flag;
  }

  public static boolean deleteFolderOrFile(String path) {
    File file = new File(path);
    if (!file.exists()) {
      logger.error("{} does not exist", path);
      return false;
    } else {
      if (file.isFile()) {
        return deleteFile(path);
      } else {
        return deleteDirectoryAndChildren(path);
      }
    }
  }

  // get all the csv files under the folder
  public static ArrayList<String> getCsvFileList(String dirPath) {
    File dir = new File(dirPath);
    File[] fileList = dir.listFiles();
    ArrayList<String> strList = new ArrayList<String>();
    for (File f : fileList) {
      if ((f.isFile())
          && (".csv"
              .equals(f.getName().substring(f.getName().lastIndexOf("."), f.getName().length())))) {
        strList.add(f.getAbsolutePath());
      }
    }
    return strList;
  }

  public static String[] getCSVHeaders(String csvFile) {
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      reader.readHeaders();
      String[] headers = reader.getHeaders();
      reader.close();
      logger.info("Get CSV file {} headers successfully", csvFile);
      return headers;
    } catch (IOException e) {
      logger.error("Can not get the CSV file {} headers", csvFile);
      return null;
    }
  }

  public static ArrayList<String[]> getAllCSVDataWithoutHeaders(String csvFile) {
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      reader.readHeaders();
      ArrayList<String[]> list = new ArrayList<>();
      while (reader.readRecord()) {
        list.add(reader.getValues());
      }
      logger.info("Read CSV file {} content successfully", csvFile);
      reader.close();
      return list;
    } catch (IOException e) {
      logger.error("Can not read the CSV file {} content", csvFile);
      return null;
    }
  }

  public static ArrayList<String[]> getAllCSVDataWithHeaders(String csvFile) {
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      ArrayList<String[]> list = new ArrayList<>();
      while (reader.readRecord()) {
        list.add(reader.getValues());
      }
      logger.info("Read CSV file {} content successfully", csvFile);
      reader.close();
      return list;
    } catch (IOException e) {
      logger.error("Can not read the CSV file {} content", csvFile);
      return null;
    }
  }

  public static String[] getCSVRowDataWithoutHeaders(String csvFile, int row) {
    String[] rowData = null;
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      reader.readHeaders();
      int currentRow = 1;
      while (reader.readRecord()) {
        if (currentRow == row) {
          rowData = reader.getValues();
          break;
        }
        currentRow += 1;
      }
      if (rowData == null) {
        logger.error("Can not get the CSV file {} row {} data", csvFile, row);
      } else {
        logger.info("Get CSV file {} row {} data successfully", csvFile, row);
      }
      reader.close();
    } catch (IOException e) {
      logger.error("Can not get the CSV file {} row {} data", csvFile, row);
    }
    return rowData;
  }

  public static String[] getCSVRowDataWithHeaders(String csvFile, int row) {
    String[] rowData = null;
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      int currentRow = 1;
      while (reader.readRecord()) {
        if (currentRow == row) {
          rowData = reader.getValues();
          break;
        }
        currentRow += 1;
      }
      if (rowData == null) {
        logger.error("Can not get the CSV file {} row {} data", csvFile, row);
      } else {
        logger.info("Get CSV file {} row {} data successfully", csvFile, row);
      }
      reader.close();
    } catch (IOException e) {
      logger.error("Can not get the CSV file {} row {} data", csvFile, row);
    }
    return rowData;
  }

  public static String getCSVCellDataWithoutHeaders(String csvFile, int row, String headerName) {
    String cellData = null;
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      reader.readHeaders();
      int currentRow = 1;
      while (reader.readRecord()) {
        if (currentRow == row) {
          cellData = reader.get(headerName);
          break;
        }
        currentRow += 1;
      }
      if (cellData == null) {
        logger.error(
            "Can not get the CSV file {} cell {} data in row {}", csvFile, headerName, row);
      } else {
        logger.info(
            "The CSV file {} cell {} data in row {} is {}", csvFile, headerName, row, cellData);
      }
      reader.close();
    } catch (IOException e) {
      logger.error("Can not get the CSV file {} cell {} data in row {}", csvFile, headerName, row);
    }
    return cellData;
  }

  public static String getCSVCellDataWithHeaders(String csvFile, int row, int column) {
    String cellData = null;
    try {
      CsvReader reader = new CsvReader(csvFile, ',', Charset.forName("UTF-8"));
      int currentRow = 1;
      while (reader.readRecord()) {
        if (currentRow == row) {
          cellData = reader.get(column - 1);
          break;
        }
        currentRow += 1;
      }
      if (cellData == null) {
        logger.error("Can not get the CSV file {} cell [{},{}] data", csvFile, row, column);
      } else {
        logger.info("The CSV file {} cell [{},{}] data is {}", csvFile, row, column, cellData);
      }
      reader.close();
    } catch (IOException e) {
      logger.error("Can not get the CSV file {} cell [{},{}] data", csvFile, row, column);
    }
    return cellData;
  }

  public static void writeCSVData(String csvFile, ArrayList<String[]> data) {
    try {
      CsvWriter writer = new CsvWriter(csvFile, ',', Charset.forName("UTF-8"));
      for (int i = 0; i < data.size(); i++) {
        writer.writeRecord(data.get(i));
      }
      logger.info("The CSV file {} is written", csvFile);
      writer.close();
    } catch (IOException e) {
      logger.error("Can not write the CSV file {}", csvFile);
    }
  }

  public static void writeCSVRowData(String csvFile, int row, String[] rowData) {
    ArrayList<String[]> fileData = getAllCSVDataWithHeaders(csvFile);
    try {
      CsvWriter writer = new CsvWriter(csvFile, ',', Charset.forName("UTF-8"));
      int rowCount = fileData.size();
      if (rowCount < row) {
        for (int i = 0; i < rowCount; i++) {
          writer.writeRecord(fileData.get(i));
        }
        for (int j = rowCount; j < row - 1; j++) {
          writer.endRecord();
        }
        writer.writeRecord(rowData);
      } else {
        for (int i = 0; i < rowCount; i++) {
          if (i == row - 1) {
            writer.writeRecord(rowData);
          } else {
            writer.writeRecord(fileData.get(i));
          }
        }
      }
      logger.info("The CSV file {} row {} is written", csvFile, row);
      writer.close();
    } catch (IOException e) {
      logger.error("Can not write the CSV file {} row {}", csvFile, row);
    }
  }

  public static void writeCSVCellData(String csvFile, int row, int column, String cellData) {
    ArrayList<String[]> fileData = getAllCSVDataWithHeaders(csvFile);
    try {
      CsvWriter writer = new CsvWriter(csvFile, ',', Charset.forName("UTF-8"));
      int rowCount = fileData.size();
      if (rowCount < row) {
        for (int i = 0; i < rowCount; i++) {
          writer.writeRecord(fileData.get(i));
        }
        for (int j = rowCount; j < row - 1; j++) {
          writer.endRecord();
        }
        for (int k = 0; k < column - 1; k++) {
          writer.write(null);
        }
        writer.write(cellData);
      } else {
        for (int i = 0; i < rowCount; i++) {
          if (i == row - 1) {
            int columnCount = fileData.get(i).length;
            if (columnCount < column) {
              for (int j = 0; j < columnCount; j++) {
                writer.write(fileData.get(i)[j]);
              }
              for (int k = columnCount; k < column - 1; k++) {
                writer.write(null);
              }
              writer.write(cellData);
            } else {
              for (int j = 0; j < columnCount; j++) {
                if (j == column - 1) {
                  writer.write(cellData);
                } else {
                  writer.write(fileData.get(i)[j]);
                }
              }
            }
            writer.endRecord();
          } else {
            writer.writeRecord(fileData.get(i));
          }
        }
      }
      logger.info("The CSV file {} cell [{},{}] is written", csvFile, row, column);
      writer.close();
    } catch (IOException e) {
      logger.error("Can not write the CSV file {} cell [{},{}]", csvFile, row, column);
    }
  }

  public static String getFileDirectoryCreatedDateTime(File file) {
    if (file.exists()) {
      BasicFileAttributes attr;
      try {
        Path path = file.toPath();
        attr = Files.readAttributes(path, BasicFileAttributes.class);
      } catch (IOException e) {
        logger.error("Error occurs: {}", e.toString());
        throw new RuntimeException(e.toString());
      }
      Instant instant = attr.creationTime().toInstant();
      String createdTime =
          DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
              .withZone(ZoneId.systemDefault())
              .format(instant);
      return createdTime;
    } else {
      logger.error("The file or directory does not exist");
      return null;
    }
  }

  public static File movToMp4_2(String sourceFileName, String destFileName) {
    File source = new File(sourceFileName);
    File target = new File(destFileName);
    if (source.exists()) {
      try {
        ws.schild.jave.encode.AudioAttributes audio = new ws.schild.jave.encode.AudioAttributes();
        audio.setCodec("libmp3lame");
        audio.setBitRate(new Integer(800000));//设置比特率
        audio.setChannels(new Integer(1));//设置音频通道数
        audio.setSamplingRate(new Integer(44100));//设置采样率
        ws.schild.jave.encode.VideoAttributes video = new ws.schild.jave.encode.VideoAttributes();
        video.setCodec("mpeg4");
//            video.setCodec("libx264");
        video.setBitRate(new Integer(3200000));
        video.setFrameRate(new Integer(15));
        ws.schild.jave.encode.EncodingAttributes attrs = new ws.schild.jave.encode.EncodingAttributes();
        attrs.setOutputFormat("mp4");
        attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
        ws.schild.jave.Encoder encoder = new ws.schild.jave.Encoder();
        encoder.encode(new MultimediaObject(source), target, attrs);
        logger.info("Mov video is transferred to mp4 {}", destFileName);
      } catch (Exception e) {
        logger.error("Failed to transfer mov video to mp4 {}", sourceFileName);
        e.printStackTrace();
      }
    } else {
      logger.error("Mov video does not exist: {}", sourceFileName);
    }
    return target;
  }

  public static File movToMp4(String sourceFileName, String destFileName) {
    File source = new File(sourceFileName);
    File target = new File(destFileName);
    if (source.exists()) {
      try {
        AudioAttributes audio = new AudioAttributes();
        audio.setCodec("libmp3lame");
        audio.setBitRate(new Integer(56000));
        audio.setChannels(new Integer(1));
        audio.setSamplingRate(new Integer(22050));
        VideoAttributes video = new VideoAttributes();
        video.setCodec("mpeg4");
        video.setBitRate(new Integer(800000));
        video.setFrameRate(new Integer(15));
        EncodingAttributes attrs = new EncodingAttributes();
        attrs.setFormat("mp4");
        attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
        Encoder encoder = new Encoder();
        encoder.encode(source, target, attrs);
        logger.info("Mov video is transferred to mp4 {}", destFileName);
      } catch (Exception e) {
        logger.error("Failed to transfer mov video to mp4 {}", sourceFileName);
        e.printStackTrace();
      }
    } else {
      logger.error("Mov video does not exist: {}", sourceFileName);
    }
    return target;
  }


   public static String readPaymentExCsvFile(String tranStatus,String balanceStatus,String serviceType) {
     File chromeNewerDownFile = getChromeDefaultDownFile();
     String paymentCsvPath = chromeNewerDownFile.getPath();
     try {
       ArrayList<String[]> arrList = new ArrayList<String[]>();
       CsvReader reader = new CsvReader(paymentCsvPath, ',', StandardCharsets.UTF_8);
       while (reader.readRecord()) {
          arrList.add(reader.getValues());
       }
       reader.close();
       for (int row = 1; row < arrList.size(); row++) {
         if (arrList.get(row)[9].equals(tranStatus) && arrList.get(row)[10].equals(balanceStatus) && arrList.get(row)[3].equals(serviceType)) {
           return arrList.get(row)[1];
         }
       }
     } catch (Exception e) {
       e.printStackTrace();
     }
     return null;
   }

   public static File getChromeDefaultDownFile() {
     String chromeDefaultPath = GlobalVar.CHROME_PATH;
     List<File> exFiles = new ArrayList<>();
     if (!Strings.isNullOrEmpty(chromeDefaultPath)) {
       try {
         File downFiles = new File(chromeDefaultPath);
         if (downFiles.exists()) {
           File[] files = downFiles.listFiles();
           if (files != null) {
             for (File file : files) {
                if (file.getName().contains("payment_exception_record")) {
                  exFiles.add(file);
                }
             }
           }
         }

         if (exFiles.size() < 1) {
           return null;
         }

         String[] fileNameSplit = exFiles.get(0).getName().split("_");
         String[] createTime = fileNameSplit[3].split("\\.");
         BigDecimal newestData = new BigDecimal(createTime[0]);
         for (int i = 1; i < exFiles.size(); i++) {
           fileNameSplit = exFiles.get(i).getName().split("_");
           createTime = fileNameSplit[3].split("\\.");
           if (newestData.compareTo(new BigDecimal(createTime[0])) < 0) {
             newestData = new BigDecimal(createTime[0]);
           }
         }
         for (File file : exFiles) {
           if (file.getName().contains(String.valueOf(newestData))){
             logger.info("Read payment exception file is :" + file.getName());
             return file;
           }
         }
       }catch (Exception e) {
         logger.info("Read payment exception file error");
         e.printStackTrace();
       }
     }
     return null;
   }

}
