
package com.welab.automation.framework.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesReader {
  private static final Logger logger = LoggerFactory.getLogger(PropertiesReader.class);

  private static PropertiesReader propertiesReader;
  private Properties properties = new Properties();

  private PropertiesReader() {
    read();
  }

  public static synchronized PropertiesReader getInstance() {
    if (null == propertiesReader) {
      return new PropertiesReader();
    }
    return propertiesReader;
  }

  private void read() {
    String env = System.getProperty("env");
    String platform = System.getProperty("platform");
    logger.info(
        "Executing scripts for {} on {} environment...", platform.toUpperCase(), env.toUpperCase());
    String pref;
    switch (platform.toUpperCase()) {
      case "CHROME":
      case "FIREFOX":
      case "SAFARI":
        pref = "web";
        if (System.getProperty("mobile") != null) pref = "mobile";
        break;
      case "MOBILE":
        pref = "mobile";
        break;
      case "API":
        pref = "api";
        break;
      default:
        {
          logger.error("Please define platform in maven command");
          throw new IllegalArgumentException(
              "Please define platform in maven command with -Dplatform");
        }
    }
    try (BufferedReader bufferedReader =
        new BufferedReader(
            new FileReader("./src/main/resources/" + pref + "_" + env + ".properties"))) {
      properties.load(bufferedReader);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getProperty(String key) {
    return properties.getProperty(key);
  }

  public Map<String, String> getAllProperties() {
    Map<String, String> allPropertiesMap = new HashMap<>();
    Enumeration<String> propertyNames = (Enumeration<String>) properties.propertyNames();
    while (propertyNames.hasMoreElements()) {
      String propertyKey = propertyNames.nextElement();
      String propertyValue = properties.get(propertyKey).toString();
      allPropertiesMap.put(propertyKey, propertyValue);
    }
    logger.info("Get all properties Map {}", allPropertiesMap);
    return allPropertiesMap;
  }
}
