package com.welab.automation.framework.utils.entity.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class DataPool {
    private static Logger logger = LoggerFactory.getLogger(DataPool.class);
    /**
     * get List<Map> one column list
     *
     * @param mapList
     * @param key
     * @return
     */
    public static List getMapListOneColumnValues(List<Map<String, ?>> mapList, String key) {
        return mapList.stream()// stream over the list
                .map(m -> m.get(key)) // try to get the key "No"
                .filter(Objects::nonNull) // filter any null values in case it wasn't present
                .collect(Collectors.toList());  // join the values
    }
}
