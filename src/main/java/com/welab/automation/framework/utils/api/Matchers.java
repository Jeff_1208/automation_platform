
package com.welab.automation.framework.utils.api;

import com.welab.automation.framework.GlobalVar;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class Matchers {

  private static Logger logger = LoggerFactory.getLogger(Matchers.class);

  public static void checkField(JsonPath response, String path, String matcher) {
    int startIndex = matcher.indexOf("(");
    String expectedVal;
    if (startIndex >= 0)
      expectedVal = matcher.substring(startIndex + 1, matcher.length() - 1).trim();
    else expectedVal = matcher;
    if (expectedVal.startsWith("${")) {
      expectedVal = expectedVal.substring(2, expectedVal.length() - 1);
      expectedVal = GlobalVar.GLOBAL_VARIABLES.get(expectedVal);
    }
    if (matcher.startsWith("exists")) {
      assertThat(response.getObject(path, Object.class)).isNotNull();
      logger.info("The json path [{}] exists.", path);
    } else if (matcher.startsWith("hasItemInArray")) {
      assertThat(response.getList(path)).contains(expectedVal);
    } else if (matcher.startsWith("isAnArray")) {
      assertThat(response.getList(path)).hasSizeGreaterThanOrEqualTo(0);
      logger.info("The json path [{}] returns an array.", path);
    } else if (matcher.startsWith("equals")) {
      assertThat(response.getString(path)).isEqualTo(expectedVal);
      logger.info("The value returned by json path [{}] equals to [{}]", path, expectedVal);
    } else if (matcher.startsWith("startsWith")) {
      assertThat(response.getString(path)).startsWith(expectedVal);
      logger.info("The value returned by json path [{}] starts with [{}]", path, expectedVal);
    } else if (matcher.startsWith("isNotEmpty")) {
      assertThat(response.getObject(path, Object.class)).isNotEqualTo("");
      logger.info("The value returned by json path [{}] is not empty.", path);
    } else if (matcher.startsWith("isNull")) {
      assertThat(response.getObject(path, Object.class)).isNull();
      logger.info("The value returned by json path [{}] is null.", path);
    } else if (matcher.startsWith("isNotNull")) {
      assertThat(response.getString(path)).isNotNull();
      logger.info("The value returned by json path [{}] is not null.", path);
    } else if (matcher.startsWith("contains")) {
      assertThat(response.getString(path)).contains(expectedVal);
      logger.info("The value returned by json path [{}] contains [{}]", path, expectedVal);
    } else if (matcher.startsWith("arraySizeEquals")) {
      int size = response.getList(path).size();
      assertThat(size).isEqualTo(Integer.parseInt(expectedVal));
      logger.info("The size of array returned by json path [{}] is {}", path, expectedVal);
    } else if (matcher.startsWith("eachItemInArrayEquals")) {
      List<String> items = response.getList(path, String.class);
      for (String item : items) {
        assertThat(item).isEqualTo(expectedVal);
      }
      logger.info(
          "Each item in array (returned by json path [{}]) equals to [{}]", path, expectedVal);
    } else if (matcher.startsWith("eachItemInArrayContains")) {
      List<String> items = response.getList(path, String.class);
      for (String item : items) {
        assertThat(item).containsIgnoringCase(expectedVal);
      }
      logger.info(
          "Each item in array (returned by json path [{}]) equals to [{}]", path, expectedVal);
    } else if (matcher.startsWith("stringLength")) {
      assertThat(response.getString(path)).hasSize(Integer.parseInt(expectedVal));
      logger.info("The value returned by json path [{}] {} size equals [{}].", path, response.getString(path).length(), Integer.parseInt(expectedVal));
    } else {
      assertThat(response.getString(path)).isEqualTo(expectedVal);
      logger.info("The value returned by json path [{}] equals to [{}].", path, expectedVal);
    }
  }

  public static void sendGetPotIdRequest(String productId) throws Exception {
    Map<String, String> prepareData = new
            HashMap<String, String>();
    prepareData.put("TEST_ACCOUNT_SPECIFIED", GlobalVar.GLOBAL_VARIABLES.get("TEST_ACCOUNT"));
    prepareData.put("TEST_PASSWORD_SPECIFIED", GlobalVar.GLOBAL_VARIABLES.get("TEST_PASSWORD"));
    String path = "/v1/gs/" + productId + "/deposit/";
    String params = "";
    for (String key : prepareData.keySet()) {
      GlobalVar.GLOBAL_VARIABLES.put(key, prepareData.get(key));
    }
    String env = System.getProperty("env");
    String platform = System.getProperty("platform");
    String propertiesFileName = platform+"_"+env;
    File file = new File("./src/main/resources/" + propertiesFileName + ".properties");
    if (!file.exists())
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    Properties proterties = new Properties();
    try {
      proterties.load(new FileInputStream(file));
    } catch (IOException e) {
      e.printStackTrace();
    }
    GlobalVar.GLOBAL_VARIABLES.put("host", (String) proterties.get("host"));
    GlobalVar.HEADERS.put("Content-Type", (String) proterties.get("Content-Type"));
    GlobalVar.HEADERS.put("Client-Id", (String) proterties.get("CLIENT_ID"));
    GlobalVar.HEADERS.put("Client-Secret", (String) proterties.get("CLIENT_SECRET"));
    GlobalVar.HEADERS.put("User-Agent", (String) proterties.get("User-Agent"));
    GlobalVar.HEADERS.put("Accept-Language", (String) proterties.get("Accept-Language"));
    SendRequest SendRequest = new SendRequest();
    Map<String, String> Map = new HashMap<String, String>();
    Response response = SendRequest.sendRequest(path, "POST", "", params, "Welab/body/Deposit.json");
    assertThat(response.jsonPath().getString("data.depositAmount")).isEqualTo("100.0");
    GlobalVar.GLOBAL_VARIABLES.put("data.id[0]", String.valueOf(response.jsonPath().getInt("data.id")));

  }
}
