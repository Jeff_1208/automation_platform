
package com.welab.automation.framework.utils;

/**
 * Interface that allows to execute action and return value
 *
 * @param <R> type of returning value
 */
@FunctionalInterface
public interface Action<R> {
    R perform();
}
