package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.common.ProxyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class RetryListener extends TestListenerAdapter implements IAnnotationTransformer, IHookable {
    private final Logger logger = LoggerFactory.getLogger(RetryListener.class);
    private static ThreadLocal<String> pathThreadLocal = new ThreadLocal<String>();//全局静态变量
    private static ThreadLocal<String> methodThreadLocal = new ThreadLocal<String>();//全局静态变量
    private static ConcurrentHashMap<String, ApiResult> resultMap = new ConcurrentHashMap<>();

    private static final String SUCCESS = "success";
    private static final String FAIL = "fail";

    @SuppressWarnings("rawtypes")
    public void transform(ITestAnnotation annotation, Class testClass,
                          Constructor testConstructor, Method testMethod) {
        IRetryAnalyzer retry = annotation.getRetryAnalyzer();
        if (retry == null) {
            annotation.setRetryAnalyzer(TestngRetry.class);
        }
    }

    @Override
    public void onFinish(ITestContext testContext) {
//        new ExcelUtil().isUpdateResult(resultMap);
//        ArrayList<ITestResult> testsToBeRemoved = new ArrayList<ITestResult>();
//        Set<Integer> passedTestIds = new HashSet<Integer>();
//        for (ITestResult passedTest : testContext.getPassedTests().getAllResults()) {
//            passedTestIds.add(getId(passedTest));
//        }
//        Set<Integer> failedTestIds = new HashSet<Integer>();
//        for (ITestResult failedTest : testContext.getFailedTests().getAllResults()) {
//            int failedTestId = getId(failedTest);
//            if (failedTestIds.contains(failedTestId) || passedTestIds.contains(failedTestId)) {
//                testsToBeRemoved.add(failedTest);
//            } else {
//                failedTestIds.add(failedTestId);
//            }
//        }
//        for (Iterator<ITestResult> iterator = testContext.getFailedTests().getAllResults().iterator(); iterator.hasNext(); ) {
//            ITestResult testResult = iterator.next();
//            if (testsToBeRemoved.contains(testResult)) {
//                iterator.remove();
//            }
//        }
//
//        new ExcelUtil().updateResult(resultMap);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        super.onTestSuccess(iTestResult);
        getRunResult(iTestResult);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        super.onTestFailure(iTestResult);
        getRunResult(iTestResult);
    }


    private void getRunResult(ITestResult iTestResult) {
        String path = getPathThreadLocal();
        String method = getMethodThreadLocal();
        long usedTime = iTestResult.getEndMillis() - iTestResult.getStartMillis();
        if (!StringUtils.isEmpty(method) && !StringUtils.isEmpty(path)) {
            String key = method.concat("-").concat(path).replaceAll("\\{\\w*\\}", "{}");
            ApiResult apiResult;
            if (iTestResult.isSuccess()) {  //success
                if (resultMap.get(key) == null) {
                    apiResult = new ApiResult();
                    apiResult.setMethod(method);
                    apiResult.setPath(path);
                    apiResult.setResult(RetryListener.SUCCESS);
                    apiResult.setDuration(usedTime);
                    resultMap.put(key, apiResult);
                }
            } else { // fail
                if (resultMap.get(key) != null) {
                    apiResult = resultMap.get(key);
                    apiResult.setResult(RetryListener.FAIL);
                } else {
                    apiResult = new ApiResult();
                    apiResult.setMethod(method);
                    apiResult.setPath(path);
                    apiResult.setResult(RetryListener.SUCCESS);

                }
                apiResult.setDuration(usedTime);
                resultMap.put(key, apiResult);
            }
        }

    }


    private int getId(ITestResult result) {
        int id = result.getTestClass().getName().hashCode();
        id = id + result.getMethod().getMethodName().hashCode();
        id = id + (result.getParameters() != null ? Arrays.hashCode(result.getParameters()) : 0);
        return id;
    }

    public static String getPathThreadLocal() {
        return pathThreadLocal.get();
    }

    public static void setPathThreadLocal(String router) {
        RetryListener.pathThreadLocal.set(router);
    }


    public static void setMethodThreadLocal(String method) {
        RetryListener.methodThreadLocal.set(method);
    }

    public static String getMethodThreadLocal() {
        return methodThreadLocal.get();
    }

    @Override
    public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
        JsonEntity jsonEntity = getJsonEntity(iTestResult);
        Map<String, List<String>> sqlExpression = null;
        //replace parameter
        if (jsonEntity != null) {
            jsonEntity.setJsonObject(TestCaseUtils.replaceParameter(jsonEntity.getJsonObject()));
            jsonEntity.updateHeader();
        }
        //execute test case
        iHookCallBack.runTestMethod(iTestResult);
    }

    public JsonEntity getJsonEntity(ITestResult testResult) {
        JsonEntity jsonEntity = null;
        Object[] parameters = testResult.getParameters();

        if (parameters.length > 0) {
            for (Object parameter : parameters) {
                if (parameter instanceof JsonEntity) {
                    jsonEntity = (JsonEntity) parameter;
                }
            }
        }
        return jsonEntity;
    }
}

