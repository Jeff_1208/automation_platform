package com.welab.automation.framework.utils.entity.estatement;

public class Loans {
	
	private String Start_Year;
	private String Start_Month;
	private String Start_Day;
	private String End_Year;
	private String End_Month;
	private String End_Day;
	private String Total_Amount;
	private String Type;
	private String Start_Date;
	private String End_Date;
	private String Ref;
	private String current_Amount;
	private boolean is_have_single_current_amount;
	
	
	public boolean isIs_have_single_current_amount() {
		return is_have_single_current_amount;
	}
	public void setIs_have_single_current_amount(boolean is_have_single_current_amount) {
		this.is_have_single_current_amount = is_have_single_current_amount;
	}
	public String getStart_Year() {
		return Start_Year;
	}
	public void setStart_Year(String start_Year) {
		Start_Year = start_Year;
	}
	public String getStart_Month() {
		return Start_Month;
	}
	public void setStart_Month(String start_Month) {
		Start_Month = start_Month;
	}
	public String getStart_Day() {
		return Start_Day;
	}
	public void setStart_Day(String start_Day) {
		Start_Day = start_Day;
	}
	public String getEnd_Year() {
		return End_Year;
	}
	public void setEnd_Year(String end_Year) {
		End_Year = end_Year;
	}
	public String getEnd_Month() {
		return End_Month;
	}
	public void setEnd_Month(String end_Month) {
		End_Month = end_Month;
	}
	public String getEnd_Day() {
		return End_Day;
	}
	public void setEnd_Day(String end_Day) {
		End_Day = end_Day;
	}
	public String getTotal_Amount() {
		return Total_Amount;
	}
	public void setTotal_Amount(String total_Amount) {
		Total_Amount = total_Amount;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(String start_Date) {
		Start_Date = start_Date;
	}
	public String getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}
	public String getRef() {
		return Ref;
	}
	public void setRef(String ref) {
		Ref = ref;
	}
	public String getCurrent_Amount() {
		return current_Amount;
	}
	public void setCurrent_Amount(String current_Amount) {
		this.current_Amount = current_Amount;
	}
	@Override
	public String toString() {
		return "Loans [Start_Year=" + Start_Year + ", Start_Month=" + Start_Month + ", Start_Day=" + Start_Day
				+ ", End_Year=" + End_Year + ", End_Month=" + End_Month + ", End_Day=" + End_Day + ", Total_Amount="
				+ Total_Amount + ", Type=" + Type + ", Start_Date=" + Start_Date + ", End_Date=" + End_Date + ", Ref="
				+ Ref + ", current_Amount=" + current_Amount + ", getStart_Year()=" + getStart_Year()
				+ ", getStart_Month()=" + getStart_Month() + ", getStart_Day()=" + getStart_Day() + ", getEnd_Year()="
				+ getEnd_Year() + ", getEnd_Month()=" + getEnd_Month() + ", getEnd_Day()=" + getEnd_Day()
				+ ", getTotal_Amount()=" + getTotal_Amount() + ", getType()=" + getType() + ", getStart_Date()="
				+ getStart_Date() + ", getEnd_Date()=" + getEnd_Date() + ", getRef()=" + getRef()
				+ ", getCurrent_Amount()=" + getCurrent_Amount() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}



}
