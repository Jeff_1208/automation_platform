
package com.welab.automation.framework.utils.api;

import com.csvreader.CsvReader;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.entity.api.TestCaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class CSVFileReader {
    private static Logger logger = LoggerFactory.getLogger(FeatureCreator.class);

    public List<TestCaseEntity> readCSVFile(String filepath) throws IOException {
        List<TestCaseEntity> testDatas = new ArrayList();
        CsvReader reader = new CsvReader(filepath, ',', Charset.forName("UTF-8"));
        reader.readHeaders();
        ArrayList<String[]> list = new ArrayList<String[]>();
        while (reader.readRecord()) {
            list.add(reader.getValues());
        }
        for (int row = 0; row < list.size(); row++) {
            testDatas.add(createTestCaseEntity(row, list));
        }
        return testDatas;
    }

    public List<TestCaseEntity> readCSV(String dirPath) throws IOException {
        ArrayList<String> cvsFilesList = FileUtil.getCsvFileList(dirPath);
        List<TestCaseEntity> testDatas = new ArrayList();
        for (String csvFile : cvsFilesList) {
            testDatas.addAll(readCSVFile(csvFile));
        }
        return testDatas;
    }

    public TestCaseEntity createTestCaseEntity(int row, ArrayList<String[]> list) {
        TestCaseEntity testCaseEntity = new TestCaseEntity();
        testCaseEntity.setModule(list.get(row)[0] != null ? list.get(row)[0] : "");
        testCaseEntity.setCaseNo(list.get(row)[1] != null ? list.get(row)[1] : "");
        testCaseEntity.setCaseName(list.get(row)[2] != null ? list.get(row)[2] : "");
        testCaseEntity.setUrl(list.get(row)[3] != null ? list.get(row)[3] : "");
        testCaseEntity.setMethod(list.get(row)[4] != null ? list.get(row)[4] : "");
        testCaseEntity.setHeaders(list.get(row)[5] != null ? list.get(row)[5] : "");
        testCaseEntity.setParams(list.get(row)[6] != null ? list.get(row)[6] : "");
        testCaseEntity.setJsonFile(list.get(row)[7] != null ? list.get(row)[7] : "");
        testCaseEntity.setCode(list.get(row)[8] != null ? list.get(row)[8] : "");
        testCaseEntity.setResponse(list.get(row)[9] != null ? list.get(row)[9] : "");
        testCaseEntity.setValidation(list.get(row)[10] != null ? list.get(row)[10] : "");
        testCaseEntity.setByParameter(list.get(row)[11] != null ? list.get(row)[11] : "");
        testCaseEntity.setVariables(list.get(row)[12] != null ? list.get(row)[12] : "");
        testCaseEntity.setIsFile(list.get(row)[13] != null ? list.get(row)[13] : "");
        testCaseEntity.setTemplate(list.get(row)[14] != null ? list.get(row)[14] : "");
        if (list.get(row).length>15) {
            testCaseEntity.setAccount(list.get(row)[15] != null ? list.get(row)[15] : "");
            testCaseEntity.setTimeout(list.get(row)[16] != null ? list.get(row)[16] : "");
            testCaseEntity.setSql(list.get(row)[17] != null ? list.get(row)[17] : "");
        }
        else{
            testCaseEntity.setAccount("");
            testCaseEntity.setTimeout("");
        }

        logger.info(
                testCaseEntity.getCaseName()
                        + "||"
                        + testCaseEntity.getCaseNo()
                        + "||"
                        + testCaseEntity.getCaseName()
                        + "||"
                        + testCaseEntity.getUrl()
                        + "||"
                        + testCaseEntity.getMethod()
                        + "||"
                        + testCaseEntity.getParams()
                        + "||"
                        + testCaseEntity.getResponse());
        return testCaseEntity;

    }

    /**
     * get data by rowId
     *
     * @param fileDir
     * @param caseNo
     * @return ReqestEntity
     * @throws Exception
     */
    public TestCaseEntity readCsvIndex(String fileDir, int caseNo) throws Exception {
        TestCaseEntity reqestEntity = null;
        List<TestCaseEntity> list = readCSV(fileDir);
        for (TestCaseEntity req : list) {
            if (Integer.parseInt(req.getCaseNo().split("-")[1].trim()) == caseNo) {
                reqestEntity = req;
                break;
            }
        }
        if (reqestEntity == null) throw new Exception("test data not exist");
        logger.info(
                reqestEntity.getCaseName()
                        + "||"
                        + reqestEntity.getCaseNo()
                        + "||"
                        + reqestEntity.getCaseName()
                        + "||"
                        + reqestEntity.getUrl()
                        + "||"
                        + reqestEntity.getMethod()
                        + "||"
                        + reqestEntity.getParams()
                        + "||"
                        + reqestEntity.getResponse());
        return reqestEntity;
    }

    public static void main(String args[]) {
        //        try {
        //            readCSV("src/test/resources/api/testData/testData.csv");
        //        } catch (IOException e) {
        //            e.printStackTrace();
        //        }
    }
}
