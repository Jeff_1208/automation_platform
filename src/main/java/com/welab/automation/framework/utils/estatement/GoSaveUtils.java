package com.welab.automation.framework.utils.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.entity.estatement.GoSave;

import java.util.*;

public class GoSaveUtils {

  public String get_GoSave_id(String line) {
    String id = line.split(" ")[1].trim();
    return id.replace("-", "");
  }

  public Estatement get_go_save_transactions_history(String content, Estatement estatement) {
    boolean go_save_trans_flag = false;
    boolean is_have_go_save_transactions = false;
    ArrayList<GoSave> gosave_trans_list = new ArrayList<>();
    ArrayList<String> gosave_id_list = new ArrayList<>();
    int count = 0;

    String[] want_find_shuzu = {"GoSave", "."};
    String[] want_find_shuzu_need_one_1 = {
      "started", "matured",
    };
    String[] want_find_shuzu_need_one_2 = {"withdrawal", "Deposit"};
    String[] want_find_shuzu_need_one_3 = {"Transaction"};
    String[] want_find_shuzu_2 = {estatement.getMonth(), String.valueOf(estatement.getYEAR()), "."};
    String[] content_list = content.split("\n");
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      // GoSave 847- 交易紀錄 Transaction History
      if (line.contains(Global.TRANSACTION_HISTORY_START_STRING) && line.contains("GoSave")) {
        go_save_trans_flag = true;
        String gosave_id = get_GoSave_id(line);
        if (gosave_id_list.contains(gosave_id)) {
          // 暂时没有输出到报告中
          System.out.println(
              estatement.getCustomer_id() + " GoSave " + gosave_id + " didn't combine");
        } else {
          gosave_id_list.add(gosave_id);
        }
      }

      if (line.contains(Global.LOANS_STRING) || line.contains(Global.IMPORTANT_NOTES)) {
        break;
      }
      if (!go_save_trans_flag) {
        continue;
      }

      if (Common.is_all_of_shuzu_item_in_line(line, want_find_shuzu)
          && Common.is_one_of_shuzu_item_in_line(line, want_find_shuzu_need_one_1)) {
        count += 1;
        is_have_go_save_transactions = true;
        gosave_trans_list =
            set_one_gosave_trans(
                gosave_trans_list, line, -1, -3, estatement, content_list, i, count);
      }
      if (Common.is_all_of_shuzu_item_in_line(line, want_find_shuzu)
          && Common.is_one_of_shuzu_item_in_line(line, want_find_shuzu_need_one_2)) {
        count += 1;
        is_have_go_save_transactions = true;
        gosave_trans_list =
            set_one_gosave_trans(
                gosave_trans_list, line, -1, -4, estatement, content_list, i, count);
      }
      if (Common.is_all_of_shuzu_item_in_line(line, want_find_shuzu_2)
          && Common.is_one_of_shuzu_item_in_line(line, want_find_shuzu_need_one_3)) {
        is_have_go_save_transactions = true;
        count += 1;
        gosave_trans_list =
            set_one_gosave_trans(
                gosave_trans_list, line, -1, -2, estatement, content_list, i, count);
      }
    }

    if (is_have_go_save_transactions) {
      estatement.setGoSave_trans_history(gosave_trans_list);
      estatement.setIs_have_gosave_trans(is_have_go_save_transactions);
    } else {
      estatement.setGoSave_fail_trans_Ref_for_amount_single(new ArrayList<>());
      estatement.setIs_have_gosave_trans(false);
      return estatement;
    }

    // 检查核心账户中关于Gosave的记录，是否和Gosave的转账记录匹配
    estatement = Set_Gosave_fail_amout_list(estatement, gosave_trans_list);

    // 检查gosave交易记录是否有利息重复
    estatement = check_gosave_duplicate_interest(estatement, gosave_trans_list);

    // 检查Gosave转账记录金额符号是否正确
    estatement = set_GoSave_fail_trans_Ref_for_amount_single(estatement, gosave_trans_list);

    // System.out.println("fail:"+estatement.getGoSave_fail_trans_Ref_from_coreaccount_list());
    return estatement;
  }

  public Estatement check_gosave_duplicate_interest(
      Estatement e, ArrayList<GoSave> gosave_trans_list) {
    ArrayList<String> result = new ArrayList<>();

    for (int i = 0; i < gosave_trans_list.size(); i++) {
      boolean is_dulicate_interest = false;
      GoSave g1 = gosave_trans_list.get(i);
      for (int j = 0; j < gosave_trans_list.size(); j++) {
        GoSave g2 = gosave_trans_list.get(j);
        if ((g1.getID().equals(g2.getID()))
            && (!(g1.getGosave_trans_Date().equals(g2.getGosave_trans_Date())))) { // 记录日期不一样
          if (Common.compare_absolute_amount(g1.getAmount(), g2.getAmount())) {
            if (g1.getType().contains(Global.GOSAVE_INTEREST_STRING)
                && g2.getType().contains(Global.GOSAVE_INTEREST_STRING)) {
              is_dulicate_interest = true;
              // 暂时没有输出到报告中
              System.out.println(
                  e.getCustomer_id() + " duplicate interest: " + g1.getGoSave_Trans_Ref());
            }
          }
        }
        if (g1.getGosave_trans_account_number() != g2.getGosave_trans_account_number()) {
          if ((g1.getID().equals(g2.getID()))
              && ((g1.getGosave_trans_Date()
                  .equals(g2.getGosave_trans_Date())))) { // 记录日期一样,金额是一样的，不是比较绝对值
            if (g1.getAmount().equals(g2.getAmount())) {
              if (g1.getType().equals(g2.getType())) {
                is_dulicate_interest = true;
                // 暂时没有输出到报告中
                System.out.println(
                    e.getCustomer_id() + " duplicate interest: " + g1.getGoSave_Trans_Ref());
              }
            }
          }
        }
      }
      if (is_dulicate_interest) {
        if (!result.contains(g1.getGoSave_Trans_Ref())) {
          result.add(g1.getGoSave_Trans_Ref());
        }
      }
    }

    if (result.size() > 0) {
      ArrayList<String> r = e.getGoSave_fail_trans_Ref_from_coreaccount_list();
      if (r == null || r.size() == 0) {
        e.setGoSave_Amount_list_in_coreaccount_trans_history(result);
      } else if (r.size() > 0) {
        r.addAll(result);
        e.setGoSave_Amount_list_in_coreaccount_trans_history(r);
      }
    }
    return e;
  }

  public Estatement set_GoSave_fail_trans_Ref_for_amount_single(
      Estatement e, ArrayList<GoSave> gosave_trans_list) {
    ArrayList<String> gosave_trans_ref_list_for_fail_amount_single = new ArrayList<>();
    for (int i = 0; i < gosave_trans_list.size(); i++) {

      if (!Common.is_have_digit(gosave_trans_list.get(i).getID())) {
        // 暂时没有输出到报告中
        System.out.println(
            e.getCustomer_id()
                + " "
                + gosave_trans_list.get(i).getGosave_trans_Date()
                + " gosave transaction record description error!");
      }

      if (!gosave_trans_list.get(i).isIs_amount_have_right_single()) {
        gosave_trans_ref_list_for_fail_amount_single.add(
            gosave_trans_list.get(i).getGoSave_Trans_Ref());
      }
    }
    if (gosave_trans_ref_list_for_fail_amount_single.size() > 0) {
      e.setGoSave_fail_trans_Ref_for_amount_single(gosave_trans_ref_list_for_fail_amount_single);
    } else {
      e.setGoSave_fail_trans_Ref_for_amount_single(gosave_trans_ref_list_for_fail_amount_single);
    }
    return e;
  }

  private Estatement Set_Gosave_fail_amout_list(
      Estatement estatement, ArrayList<GoSave> gosave_trans_list) {
    ArrayList<String> gosave_trans_amount_list = new ArrayList<>();
    for (int i = 0; i < gosave_trans_list.size(); i++) {
      gosave_trans_amount_list.add(gosave_trans_list.get(i).getAmount());
    }
    ArrayList<String> result = Common.Compate_list(estatement, gosave_trans_list);
    ArrayList<String> result1 =
        Common.Compate_list_according_gosave_tran(estatement, gosave_trans_list);
    result.addAll(result1);
    if (result.size() > 0) {
      estatement.setGoSave_fail_trans_Ref_from_coreaccount_list(result);
    }
    return estatement;
  }

  //	GoSave利息	本金轉至核心賬戶
  public GoSave get_gosave_type(
      GoSave g, String[] content_list, int line_index, Estatement e, String gosave_amount) {
    // 这两种情况type只有一行，金额都为负数
    // 25 Jan 2021 INTEREST REPAYMENT GoSave 435 Time Deposit -301.83
    // 25 Jan 2021 Debit Arrangement GoSave 435 Time Deposit -99,999.00

    boolean is_right_amount_single = true;
    String[] checK_gosave_amount_need_single = {
      "INTEREST REPAYMENT",
      Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING,
      "Debit Arrangement",
      Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING
    };
    String[] checK_gosave_amount_not_need_single = {
      Global.GOSAVE_INTEREST_STRING, Global.DEPOSIT_TO_GOSAVE_STRING
    };

    String line = content_list[line_index - 1];
    String amount_line = content_list[line_index];
    String type = "";
    if (amount_line.contains("INTEREST REPAYMENT")) {
      type = "INTEREST REPAYMENT";
    } else if (amount_line.contains("Debit Arrangement")) {
      type = "Debit Arrangement";
    } else if (line.contains(Global.GOSAVE_INTEREST_STRING)) {
      type = Global.GOSAVE_INTEREST_STRING;
    } else if (line.contains(Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING)) {
      type = Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING;
    } else if (line.contains(Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING)) {
      type = Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING;
    } else if (line.contains(Global.DEPOSIT_TO_GOSAVE_STRING)) {
      type = Global.DEPOSIT_TO_GOSAVE_STRING;
    } else {
      type = line;
    }
    g.setType(type);

    if (Common.is_one_of_shuzu_item_in_line(type, checK_gosave_amount_need_single)) {
      if (!gosave_amount.contains("-")) {
        is_right_amount_single = false;
      }
    }
    if (Common.is_one_of_shuzu_item_in_line(type, checK_gosave_amount_not_need_single)) {
      if (gosave_amount.contains("-")) {
        is_right_amount_single = false;
      }
    }
    g.setIs_amount_have_right_single(is_right_amount_single);
    return g;
  }

  private ArrayList<GoSave> set_one_gosave_trans(
      ArrayList<GoSave> gosave_trans_list,
      String line,
      int amount_index,
      int id_index,
      Estatement e,
      String[] content_list,
      int line_index,
      int gosave_trans_account_number) {
    String gosave_amount = Common.get_value_string_from_line(line, amount_index);
    String gosave_id = Common.get_value_string_from_line(line, id_index);
    GoSave gosave = new GoSave();
    gosave.setGosave_trans_account_number(gosave_trans_account_number);
    gosave.setID(gosave_id);
    gosave.setAmount(gosave_amount);
    gosave = get_gosave_type(gosave, content_list, line_index, e, gosave_amount);
    gosave = Common.get_gosave_Date_from_line(line, e, gosave);
    gosave_trans_list.add(gosave);
    return gosave_trans_list;
  }

  public Estatement get_go_save_exsit(String content, Estatement estatement) {
    boolean go_save_flag = false;
    boolean is_have_go_save = false;
    double go_save_total_calculate = 0;
    HashMap<String, String> current_gosave_exist = new HashMap<>();
    String[] want_find_shuzu = {"GoSave", "p.a.", "%"};
    String[] content_list = content.split("\n");
    for (int i = 0; i < content_list.length; i++) {
      String line = content_list[i];
      if (line.contains(Global.CURRENTLY_HELD_STRING)) { // 目前持有 Currently Held
        go_save_flag = true;
      }
      if (line.contains(Global.TRANSACTION_HISTORY_START_STRING) && line.contains("GoSave")) {
        break;
      }
      if (line.contains(Global.LOANS_STRING) && go_save_flag) {
        break;
      }
      if (go_save_flag && line.contains(Global.IMPORTANT_NOTES)) {
        break;
      }
      if (!go_save_flag) {
        continue;
      }

      if (Common.is_all_of_shuzu_item_in_line(line, want_find_shuzu)) {
        is_have_go_save = true;
        double amount = Common.get_double_value(line, -1);
        String gosave_id = Common.get_value_string_from_line(line, -5);
        go_save_total_calculate += Common.get_double_value(line, -1);
        current_gosave_exist.put(gosave_id, Common.double_to_String(amount));
      }
    }

    if (is_have_go_save) {
      estatement.setIs_have_gosave_record(is_have_go_save);
    } else {
      estatement.setIs_have_gosave_record(false);
      estatement.setGosave_total_calculate("0");
      estatement.setGo_save_result(true);
      return estatement;
    }
    HashMap<String, String> map = sort_map(current_gosave_exist);
    estatement.setCurrent_gosave_exist(map);

    go_save_total_calculate = Common.get_double_round(go_save_total_calculate);
    estatement.setGosave_total_calculate(go_save_total_calculate + "");

    // print_current_gosave(map);//输出现有GoSave金额
    if (Common.compare_amount_string_to_double(
        estatement.getGosave_total_calculate(), estatement.getGosave_total_capture_title())) {
      estatement.setGo_save_result(true);
    } else {
      estatement.setGo_save_result(false);
    }
    return estatement;
  }

  public HashMap<String, String> sort_map(HashMap<String, String> map) {
    HashMap<String, String> new_map = new HashMap<String, String>();
    Set<String> set = map.keySet();
    ArrayList<String> arrayList = new ArrayList<String>(set);
    Collections.sort(arrayList); // 从小到大的排序
    for (int len = arrayList.size(), i = len - 1; i >= 0; i--) {
      new_map.put(arrayList.get(i), map.get(arrayList.get(i)));
    }
    return new_map;
  }

  @SuppressWarnings({"rawtypes"})
  public void print_current_gosave(HashMap<String, String> current_gosave_exist) {
    Iterator iter = current_gosave_exist.entrySet().iterator();
    while (iter.hasNext()) {
      Map.Entry entry = (Map.Entry) iter.next();
      Object key = entry.getKey();
      Object val = entry.getValue();
      System.out.println("GoSave " + key + " current amount: " + val);
    }
  }
}
