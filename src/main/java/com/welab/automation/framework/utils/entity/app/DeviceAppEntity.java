package com.welab.automation.framework.utils.entity.app;

import com.welab.automation.framework.utils.entity.api.EnvironmentEntity;

public final class DeviceAppEntity {

  public static final String APPS_PATH = "apps.path";
  public static final String APP_PATH = "ios.app";
  public static final String APP_ZIP = "ios.appZip";
  public static final String APP_PACKAGE =
      System.getProperty(EnvironmentEntity.MOBILE) + ".appPackage";
  public static final String APP_ACTIVITY =
      System.getProperty(EnvironmentEntity.MOBILE) + ".appActivity";
  public static final String DEVICE_NAME =
      System.getProperty(EnvironmentEntity.MOBILE) + ".deviceName";
  public static final String WDA = "ios.wda";
  public static final String UDID = "ios.udid";
  public static final String XCODE_ORG_ID = "ios.xcodeOrgId";
  public static final String XCODE_SIGNING_ID = "ios.xcodeSigningId";
  public static final String AUTOMATION_NAME =
      System.getProperty(EnvironmentEntity.MOBILE) + ".automationName";
  public static final String BUNDLE_ID = System.getProperty(EnvironmentEntity.MOBILE) + ".bundleId";
  public static final String PLATFORM_VERSION =
      System.getProperty(EnvironmentEntity.MOBILE) + ".platformVersion";
  public static final String LOCAL_ADDRESS = "local.ipAddress";
  public static final String LOCAL_APPIUM_PORT = "local.appiumPort";
  public static final String DRIVER_PATH = "C:/Program Files/nodejs/node.exe";
  public static final String APPIUM_PATH = "C:/Users/jeff.xie/AppData/Roaming/npm/node_modules/appium/build/lib/main.js";
  // for windows
  //public static final String DRIVER_PATH = "/usr/local/bin/node";
  //public static final String APPIUM_PATH = "/usr/local/lib/node_modules/appium/build/lib/main.js";
//  C:\Program Files\Appium\resources\app\node_modules\appium\node_modules
  public static final String LOWER_IMPLICIT_WAIT = "lowerImplicitWait";
  public static final String IMPLICIT_WAIT = "implicitWait";
  public static final String EXPLICIT_WAIT = "explicitWait";
  public static final String LONG_WAIT = "longWait";
  public static final String NEW_COMMAND_TIMEOUT = "newCommandTimeout";
  public static final String ANDROID_SDK = "/Users/robin_chen/Library/Android/sdk";
  public static final String CHROMEDRIVEReXECUTABLEDIR = "C:\\Program Files\\Appium\\resources\\app\\node_modules\\appium\\node_modules\\appium-chromedriver\\chromedriver\\win";
}
