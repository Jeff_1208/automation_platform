package com.welab.automation.framework.utils.app;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CSVAppReader {

  private String filePath;
  public static List<List<String>> records;

  public CSVAppReader(String filePath) {
    this.filePath = filePath;
  }

  public List<List<String>> DataSet(String filePath, final CSVFormat input_format)
      throws FileNotFoundException {
    InputStream inputstream = new FileInputStream(filePath);
    records = new LinkedList<>();
    try (CSVParser parser =
        new CSVParser(new InputStreamReader(inputstream), input_format.withSkipHeaderRecord())) {
      for (CSVRecord record : parser) {
        List<String> items = csvRecordToList(record);
        int size = items.size();

        // Don't add row if the line was empty.
        if (size > 1 || (size == 1 && items.get(0).length() > 0)) {
          records.add(items);
        }
      }

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    // remove the header
    return records.subList(1, records.size());
  }

  private static List<String> csvRecordToList(final CSVRecord record) {

    final List<String> list = new ArrayList<>();
    for (final String value : record) {
      list.add(value);
    }
    return list;
  }

  public List<List<String>> getRecords() throws FileNotFoundException {
    List<List<String>> outPutRecord;
    outPutRecord = DataSet(this.filePath, CSVFormat.DEFAULT.withDelimiter(';'));
    return outPutRecord;
  }
}
