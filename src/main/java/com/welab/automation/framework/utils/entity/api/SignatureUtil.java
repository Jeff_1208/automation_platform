package com.welab.automation.framework.utils.entity.api;

import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.api.SendRequestEx;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;
import java.util.*;

public class SignatureUtil {
    Logger logger = LoggerFactory.getLogger("SignatureUtil");

    String CLIENT_ID = "";
    String CLIENT_SECRET = "";
    String TEST_ACCOUNT = "";
    String TEST_PASSWORD = "";
    String version = "1";
    String publickey = "";
    String keyId = "";
    String signkey = "";

    /**
     *
     * @param config  username and password
     */
    public SignatureUtil(String ...config) {
        try {
            if (!GlobalVar.GLOBAL_VARIABLES.get("TEST_ACCOUNT_SPECIFIED").equals("")) {
                TEST_ACCOUNT = GlobalVar.GLOBAL_VARIABLES.get("TEST_ACCOUNT_SPECIFIED");
                TEST_PASSWORD = GlobalVar.GLOBAL_VARIABLES.get("TEST_PASSWORD_SPECIFIED");
            } else {
                TEST_ACCOUNT = GlobalVar.GLOBAL_VARIABLES.get("TEST_ACCOUNT");
                TEST_PASSWORD = GlobalVar.GLOBAL_VARIABLES.get("TEST_PASSWORD");
            }
        } catch (NullPointerException e) {

        }
        CLIENT_ID = GlobalVar.GLOBAL_VARIABLES.get("CLIENT_ID");
        CLIENT_SECRET = GlobalVar.GLOBAL_VARIABLES.get("CLIENT_SECRET");

        if (config.length > 1) {
            TEST_ACCOUNT = config[0];
            TEST_PASSWORD = config[1];
        }
    }

    public void generateReqHeader() {
        GlobalVar.HEADERS.put("Content-Type", "application/json");
        GlobalVar.HEADERS.put("Client-Id", CLIENT_ID);
        GlobalVar.HEADERS.put("Client-Secret", CLIENT_SECRET);
        GlobalVar.HEADERS.put("Accept-Encoding", "gzip,deflate,br");
    }

    public String generateDigest(String payload) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA-256");

            byte[] shaByteArr = mDigest.digest(payload.getBytes(Charset.forName("UTF-8")));
            logger.info("Result: " + Base64.getEncoder().encodeToString(shaByteArr));
            GlobalVar.HEADERS.put("Digest", "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr));
            GlobalVar.GLOBAL_VARIABLES.put("Digest", "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr));
            return "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr);
        } catch (Exception ex) {
            System.out.println("error");
        }
        return "";
    }

    public void generateHeaderSignature(String payload, String url, String host, String myMethod) {
        String digest = generateDigest(payload);
        Map<String, Object> headerHash = new HashMap<>();
        int timestamp = getOffset() + Math.round(System.currentTimeMillis() / 1000);
        headerHash = generateHeaderHash(timestamp, myMethod, url,
                digest, host.substring(8));
        Map<String, Object> configMap = new HashMap<>();
        configMap = generateConfig(GlobalVar.GLOBAL_VARIABLES.get("keyId"), timestamp, GlobalVar.GLOBAL_VARIABLES.get("signkey"));
        computeHttpSignature(configMap, headerHash);
        return;
    }

    public Map<String, Object> generateHeaderHash(int timestamp, String method, String targetUrl, String computerDigest, String host) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("(created)", timestamp);
        map.put("digest", computerDigest);
        map.put("(request-target)", method + ' ' + targetUrl);
        map.put("host", host);
        return map;
    }

    public Map<String, Object> generateConfig(String keyId, int timestamp, String secretKey) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("algorithm", "hs2019");
        map.put("KeyId", keyId);
        map.put("timestamp", timestamp);
        map.put("secretKey", secretKey);
        ArrayList list = new ArrayList<String>();
        list.add("(request-target)");
        list.add("(created)");
        list.add("host");
        list.add("digest");
        map.put("headers", list);
        return map;
    }

    public Map<String, Object> generateConfigEx(String keyId, int timestamp, String secretKey) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("algorithm", "hs2019");
        map.put("KeyId", "SbFG9ZBGxpsAWXEhzRuNdquBkWsx");
        map.put("timestamp", 1632448076);
        map.put("secretKey", "tCP5Up+r/oJ7+RLeI8KoSJh9IzSDGndIyDpIoF/hS/g=");
        ArrayList list = new ArrayList<String>();
        list.add("(request-target)");
        list.add("(created)");
        list.add("host");
        list.add("digest");
        map.put("headers", list);
        return map;
    }

    public String computeHttpSignature(Map<String, Object> config, Map<String, Object> headerHash) {
        String signingBase = "";
        ArrayList<String> list = new ArrayList<String>();
        list = (ArrayList) config.get("headers");
        for (String header : list) {
            if (signingBase != "") {
                signingBase += "\n";
            }
            signingBase += header.toLowerCase() + ": " + headerHash.get(header);
        }
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec((config.get("secretKey").toString()).getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            String hash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(signingBase.getBytes()));
            String result = "keyId=" + config.get("KeyId").toString() + ",algorithm=" + config.get("algorithm").toString() +
                    ",created=" + config.get("timestamp").toString() + ",headers=" + String.join(" ", (List) config.get("headers")) +
                    ",signature=" + hash;
            GlobalVar.HEADERS.put("Signature", result);
            return result;
        } catch (Exception ex) {
            System.out.println("error");
        }
        return "";
    }

    public void getPublicKeyReq() {
        String host = GlobalVar.GLOBAL_VARIABLES.get("host");
        logger.info("Global var is {}",GlobalVar.GLOBAL_VARIABLES.toString());
        SendRequestEx sendRequestEx = new SendRequestEx(host);
        Map<String, Object> params = new HashMap<>();
        String path = "/v1/internal/public-key";
        Response response = sendRequestEx.SendRequest(path, "GET", params, "", "", false);
        version = response.jsonPath().getString("data.version");
        GlobalVar.GLOBAL_VARIABLES.put("version", version);
        logger.info("Get Version is " + version);
        publickey = response.jsonPath().getString("data.publicKey");
        logger.info("Get PublicKey is \n" + publickey);
    }

    public int getOffset() {
        String host = GlobalVar.GLOBAL_VARIABLES.get("host");
        SendRequestEx sendRequestEx = new SendRequestEx(host);
        Map<String, Object> params = new HashMap<>();
        String path = "/v1/clock-skew";
        int timestamp = Math.round(System.currentTimeMillis() / 1000);
        params.put("time", timestamp);
        Response response = sendRequestEx.SendRequest(path, "GET", params, "", "", false);
        int offset = response.jsonPath().getInt("data.offset");
        logger.info("Get Offset is " + String.valueOf(offset));
        GlobalVar.GLOBAL_VARIABLES.put("offset", String.valueOf(offset));
        return offset;
    }

    public JSONObject createSignInPayload(int offset) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", TEST_ACCOUNT);
        jsonObject.put("authType", "PASSWORD");
        jsonObject.put("authValue", TEST_PASSWORD);
        int timestamp = Math.round(System.currentTimeMillis() / 1000);
        jsonObject.put("timestamp", offset + timestamp);
        logger.info("timestamp is" + String.valueOf(timestamp));
        jsonObject.put("salt", UUID.randomUUID().toString());
        logger.info("jsonObject is " + jsonObject.toJSONString());
        return jsonObject;
    }

    public String createCipherEx(String publicKey, int offset) {
        try {
            String publicKeyPEM = publicKey
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replaceAll(System.lineSeparator(), "")
                    .replace("-----END PUBLIC KEY-----", "");
            logger.info("public key is ： " + publicKeyPEM);

            byte[] pbks = Base64.getMimeDecoder().decode(publicKeyPEM);
            logger.info("new public key is ： " + Base64.getEncoder().encodeToString(pbks));

            X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(pbks);
            PublicKey newPbk = KeyFactory.getInstance("RSA").generatePublic(encodedKeySpec);
            OAEPParameterSpec oaepParameterSpec = new OAEPParameterSpec("SHA-256", "MGF1", new MGF1ParameterSpec("SHA-256"), PSource.PSpecified.DEFAULT);

            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
            cipher.init(Cipher.ENCRYPT_MODE, newPbk, oaepParameterSpec);

            JSONObject jsonObject = new JSONObject();
            jsonObject = createSignInPayload(offset);
            byte[] bytes = cipher.doFinal(jsonObject.toString().getBytes());
            logger.info("Result: " + Base64.getEncoder().encodeToString(bytes));

            return Base64.getEncoder().encodeToString(bytes);
        } catch (Exception ex) {
            logger.info("system error from createCipherEx");
        }
        return "";
    }

    public JSONObject createJsonBody(String credential) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("credentials", credential);
        jsonObject.put("authValue", "PASSWORD");
        jsonObject.put("version", version);
        return jsonObject;
    }

    public JSONObject createJsonBodyEx(String username, String password) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        return jsonObject;
    }

    public static RSAPublicKey createPublicKey(String publickey) throws Exception {

        String publicKeyPEM = publickey
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");
        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    public String createSignReqEx() {
        String host = "http://10.72.0.53:8080";
        SendRequestEx proxyUtils = new SendRequestEx(host);
        Map<String, Object> params = new HashMap<>();
        String path = "/login";
        String body = createJsonBodyEx(TEST_ACCOUNT, TEST_PASSWORD).toString();
        Response response = proxyUtils.SendRequest(path, "GET", params, body, "", false);
        keyId = response.jsonPath().getString("data.accessToken");
        GlobalVar.GLOBAL_VARIABLES.put("Authorization", "Bearer " + keyId);
        GlobalVar.GLOBAL_VARIABLES.put("keyId", keyId);
        logger.info("Get token is Bearer " + keyId);
        signkey = response.jsonPath().getString("data.signKey");
        GlobalVar.GLOBAL_VARIABLES.put("signkey", signkey);
        logger.info("Get signKey is " + signkey);
        return "";
    }

    public String createSignReq(int offset) {
        String host = GlobalVar.GLOBAL_VARIABLES.get("host");
        SendRequestEx sendRequestEx = new SendRequestEx(host);
        Map<String, Object> params = new HashMap<>();
        String path = "/v1/oauth/token";
        params.put("authType", "PASSWORD");
        String cipher = createCipherEx(publickey, offset);
        String body = createJsonBody(cipher).toString();
        Response response = sendRequestEx.SendRequest(path, "POST", params, body, "", false);
        String token = response.jsonPath().getString("data.accessToken");
        String refreshToken = response.jsonPath().getString("data.refreshToken");
        GlobalVar.GLOBAL_VARIABLES.put("Authorization", "Bearer " + token);
        GlobalVar.GLOBAL_VARIABLES.put("keyId", token);
        GlobalVar.GLOBAL_VARIABLES.put("refreshToken",refreshToken);
        logger.info("Get token is " + token);
        logger.info("Get refreshToken is " + refreshToken);
        String signkey = response.jsonPath().getString("data.signKey");
        GlobalVar.GLOBAL_VARIABLES.put("signkey", signkey);
        logger.info("Get signKey is " + signkey);
        return "";
    }

    public static void main(String[] args) {
        SignatureUtil signatureUtil = new SignatureUtil();
        Map<String, Object> map = signatureUtil.generateConfig("1", 1, "11");
        System.out.println("config string is " + map.toString());
        System.out.println("header is" + String.join(" ", (List) map.get("headers")));
    }
}
