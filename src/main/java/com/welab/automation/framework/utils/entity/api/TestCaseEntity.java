
package com.welab.automation.framework.utils.entity.api;

import com.welab.automation.framework.GlobalVar;
import lombok.Getter;
import lombok.Setter;
import org.aspectj.weaver.ast.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Mapping test data from excel to this entity */
@Getter
@Setter
public class TestCaseEntity {

  private String module = null;
  private String caseNo = null;
  private String caseName = null;
  private String url = null;
  private String method = null;
  private String headers = null;
  private String params;
  private String jsonFile = null;
  private String code = null;
  private String response = null;
  private String validation = null;
  private String byParameter = null;
  private String variables = null;
  private String isFile =null;

  private String success = null;
  private String message = null;
  private String msgCode = null;
  private String result = null;
  private String template = null;
  private String timeout = null;
  private String account = null;
  private String sql = null;
/***
  public void setParams(String params) {
    if (params.contains("$")) {
      String[] paramArr = params.split("&");
      for (String param : paramArr) {
        if (param.contains("$")) {
          String[] arr = param.split("=\\$\\{");
          String k = arr[0];
          String v = arr[1].substring(0, arr[1].length() - 1);
          String test = GlobalVar.GLOBAL_VARIABLES.get(v).toString();
          params = params.replace("${" + v + "}", GlobalVar.GLOBAL_VARIABLES.get(v).toString());
        }
      }
    }
    this.params = params;
  }***/
}
