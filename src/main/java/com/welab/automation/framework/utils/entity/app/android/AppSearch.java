

package com.welab.automation.framework.utils.entity.app.android;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppSearch {
    private String caseNumber;

    private String inputWord;

    private String searchWord;
}
