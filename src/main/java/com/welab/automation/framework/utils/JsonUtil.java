
package com.welab.automation.framework.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.api.SendRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.*;

public class JsonUtil {
  private static Logger logger = LoggerFactory.getLogger(SendRequest.class);
  private static String jsonBody;
  private static Faker faker = new Faker(new Locale("en", "US"));

  public JsonUtil(String body) {
    this.jsonBody = body;
  }

  public static boolean compareJsons(String json1, String json2) {
    return true;
  }

  public static JSONObject mapToJsonObj(Map<String, Object> map) {
    JSONObject resultJson = new JSONObject();
    Iterator it = map.keySet().iterator();
    while (it.hasNext()) {
      String key = (String) it.next();
      resultJson.put(key, map.get(key));
    }
    logger.info("The JSON object is: {}", resultJson);
    return resultJson;
  }

  public static Map<String, Object> jsonObjectToMap(String jsonStr) {
    Gson gson = new Gson();
    Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
    logger.info("The map is: {}", map);
    return map;
  }

  public static JSONArray listToJsonArray(List<ArrayList> list) {
    JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(list));
    logger.info("The JSON array is: {}", jsonArray);
    return jsonArray;
  }

  public static List<ArrayList> jsonArrayToList(String jsonStr) {
    Gson gson = new Gson();
    List<ArrayList> list = gson.fromJson(jsonStr, ArrayList.class);
    logger.info("The list is: {}", list);
    return list;
  }

  public static JSONObject objectToJsonObject(Object object) {
    JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(object));
    logger.info("The JSON object is: {}", jsonObject);
    return jsonObject;
  }

  public static <T> T jsonObjectToObject(String jsonStr, Class<T> tClass) {
    Gson gson = new Gson();
    T t = gson.fromJson(jsonStr, tClass);
    logger.info("Change JSON object to: {}", tClass);
    return t;
  }

  public static boolean checkField(JSONObject actualResponse, String expectedText) {

    Map map = JSONObject.parseObject(expectedText);
    Iterator iterator = map.entrySet().iterator();
    boolean isTrue = false;
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry) iterator.next();
      // get value by key from actual,and then compare value
      String key = String.valueOf(entry.getKey());
      String expectedValue = String.valueOf(entry.getValue());
      String actaulValue = getActualValueByResponseKey(actualResponse, key);
      isTrue = expectedValue.equals(actaulValue);
    }
    return isTrue;
  }

  public static String getActualValueByResponseKey(JSONObject actualRespose, String key) {

    String[] keys = key.split("\\.");
    int i = 0;
    for (; i < keys.length - 1; i++) {
      String s = keys[i];
      if (s.contains("[") && s.contains("]")) {
        int beginIndex = s.indexOf("[");
        int lastIndex = s.indexOf("]");
        String index = s.substring(beginIndex + 1, lastIndex);
        // data>coin[]
        actualRespose =
            actualRespose
                .getJSONArray(s.substring(0, beginIndex))
                .getJSONObject(Integer.parseInt(index));

      } else {
        actualRespose = actualRespose.getJSONObject(s);
      }
    }
    String actaulValue = String.valueOf(actualRespose.get(keys[i]));
    return actaulValue;
  }

  public static JSONObject extractDataJson(String filePath) throws Exception {
    JSONObject jsonObject = JSON.parseObject(new FileInputStream(filePath), JSONObject.class);
    return jsonObject;
  }

    public static String checkReplace(String body) {
      Map<String, Object> map = JSONObject.parseObject(body);
      Iterator<Object> values = map.values().iterator();
      Iterator<String> keys = map.keySet().iterator();
      while (values.hasNext()) {
          String key = String.valueOf(keys.next());
          String value = String.valueOf(values.next());
          if (value.startsWith("$")) {
            String newValue = null;
            if (value.equals("${random}")) {
              if (key.equals("TmpId") || key.equals("TempId")) {
                newValue = UUID.randomUUID().toString();
              } else if (key.equals("cellPhone")) {
                newValue = faker.regexify("136\\d{8}");
              } else if (key.equals("customerCode")) {
                newValue = faker.regexify("[0-9]{18}");
              } else if (key.startsWith("customerName")) {
                newValue = faker.name().fullName();
              } else if (key.equals("JoinDate")) {
                newValue = TimeUtils.getTimeWithFormat(new Date(), TimeUtils.YEAR_MONTH_DAY_MS);
              } else if (key.equals("AgeRangeUpdateDate")) {
                newValue = TimeUtils.getLocalFormatTime();
              }
            } else {
              String subStr = value.substring(value.indexOf("{") + 1, value.length() - 1);
              try {
                newValue = GlobalVar.GLOBAL_VARIABLES.get(subStr).toString();
              } catch (NullPointerException ex) {
                logger.error("Cannot find global variable {}", subStr);
              }
            }
            jsonBody = jsonBody.replace(value,newValue);
          } else if (getJsonType(value).equals("JSONObject")&&value!="null") {
            checkReplace(value);
          } else if (getJsonType(value).equals("JSONArray")&&value!="null") {
            checkArrayReplace(value);
          }
      }
      return jsonBody;
    }

    private static void checkArrayReplace(String body) {
      JSONArray jsonArray = JSON.parseArray(body);
      for (int i = 0; i < jsonArray.size(); i++) {
        JSONObject json = jsonArray.getJSONObject(i);
        checkReplace(json.toJSONString());
      }
    }

  public static boolean isJsonObject(String content) {
    boolean flag = false;
    if (StringUtils.isNotEmpty(content) && !content.equals("{}")) {
      try {
        JSONObject.parseObject(content);
        flag = true;
      } catch (Exception e) {
        logger.error("{} is not a JSON object", content);
      }
    }
    return flag;
  }

  public static boolean isJsonArray(String content) {
    boolean flag = false;
    if (StringUtils.isNotEmpty(content) && !content.equals("[]")) {
      try {
        JSONArray.parseArray(content);
        flag = true;
      } catch (Exception e) {
        logger.error("{} is not a JSON array", content);
      }
    }
    return flag;
  }

  public static String getJsonType(String content) {
    String type = "";
    boolean isJsonObj = isJsonObject(content);
    if (isJsonObj) {
      type = "JSONObject";
    } else {
      boolean isJsonArray = isJsonArray(content);
      if (isJsonArray) {
        type = "JSONArray";
      }
    }
    return type;
  }
}
