package com.welab.automation.framework.db;

import com.welab.automation.framework.GlobalVar;

import java.sql.Connection;
import java.sql.DriverManager;

public class PostGreSqlFactory implements DBFactory {
  private static final String DRIVER = "org.postgresql.Driver";

  @Override
  public Connection connectDB(String dbName, String user, String password) {
    Connection con;
    try {
      Class.forName(DRIVER);
      String connectionUrl = GlobalVar.GLOBAL_VARIABLES.get("url") + dbName;
      con = DriverManager.getConnection(connectionUrl, user, password);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
    return con;
  }
}
