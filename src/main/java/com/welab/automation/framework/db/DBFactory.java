package com.welab.automation.framework.db;

import java.sql.Connection;

public interface DBFactory {
  Connection connectDB(String dbName, String user, String password);
}
