package com.welab.automation.framework.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqlServerFactory implements DBFactory {
  private static final String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

  @Override
  public Connection connectDB(String dbName, String user, String password) {
    Connection con;
    try {
      Class.forName(DRIVER);
      String connectionUrl = "jdbc:sqlserver://" + dbName;
      con = DriverManager.getConnection(connectionUrl, user, password);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
    return con;
  }
}
