package com.welab.automation.framework.db;

import com.welab.automation.framework.GlobalVar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class DBUtil {
  private static final Logger logger = LoggerFactory.getLogger(DBSingleton.class);

  public static List<Map<String, String>> queryData(Connection connection, String sql) {
    List<Map<String, String>> results = new ArrayList<>();
    String actionType = sql.substring(0, 6).toUpperCase();
    if (actionType.equals("SELECT")) {
      try {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        while (rs.next()) {
          Map<String, String> rowMap = new HashMap<>();
          for (int i = 1; i <= columnCount; i++) {
            rowMap.put(rsmd.getColumnName(i), rs.getString(i).trim());
          }
          results.add(rowMap);
        }
        if (results.size() == 0) {
          logger.error("Query result is null by: {}", sql);
        } else {
          logger.info("Query data successfully by: {}", sql);
        }
        rs.close();
        statement.close();
      } catch (SQLException e) {
        logger.error("Failed to query data by: {}", sql);
        throw new RuntimeException("Failed to query data with error: " + e.getMessage());
      }
      return results;
    } else {
      logger.error("It is not QUERY action: {}", actionType);
      return null;
    }
  }

  public static boolean insertData(Connection connection, String sql) {
    String actionType = sql.substring(0, 6).toUpperCase();
    if (actionType.equals("INSERT")) {
      return cudAction(connection, actionType, sql);
    } else {
      logger.error("It is not INSERT action: {}", actionType);
      return false;
    }
  }

  public static boolean updateData(Connection connection, String sql) {
    String actionType = sql.substring(0, 6).toUpperCase();
    if (actionType.equals("UPDATE")) {
      return cudAction(connection, actionType, sql);
    } else {
      logger.error("It is not UPDATE action: {}", actionType);
      return false;
    }
  }

  public static boolean deleteData(Connection connection, String sql) {
    String actionType = sql.substring(0, 6).toUpperCase();
    if (actionType.equals("DELETE")) {
      return cudAction(connection, actionType, sql);
    } else {
      logger.error("It is not DELETE action: {}", actionType);
      return false;
    }
  }

  private static boolean cudAction(Connection connection, String actionType, String sql) {
    boolean flag = false;
    try {
      Statement statement = connection.createStatement();
      int executeResult = statement.executeUpdate(sql);
      if (executeResult == 1) {
        flag = true;
        logger.info("{} data successfully by: {}", actionType, sql);
      } else {
        logger.error("Failed to {} data by: {}", actionType, sql);
      }
      statement.close();
      connection.commit();
      return flag;
    } catch (SQLException e) {
      logger.error("Failed to {} data by: {}", actionType, sql);
      throw new RuntimeException(e.getMessage());
    }
  }

  public static String getColumnValue(List<Map<String, String>> results, String columnName) {
    Set<String> keySet = results.get(0).keySet();
    if (keySet.contains(columnName)) {
      String columnValue = results.get(0).get(columnName);
      logger.info("Get column '{}' value '{}' successfully", columnName, columnValue);
      return columnValue;
    } else {
      logger.error("The column '{}' does not exist", columnName);
      return null;
    }
  }

  public static boolean verifyColumnValue(
      List<Map<String, String>> results, String columnName, String expectedValue) {
    String actualValue = getColumnValue(results, columnName);
    if (actualValue.equals(expectedValue)) {
      logger.info("Column '{}' value is as expected '{}'", columnName, actualValue);
      return true;
    } else {
      logger.error(
          "Column '{}' value is incorrect, expected is '{}', actual is '{}'",
          columnName,
          expectedValue,
          actualValue);
      return false;
    }
  }

  public static String[] getColumnValues(List<Map<String, String>> results, String columnName) {
    Set<String> keySet = results.get(0).keySet();
    if (keySet.contains(columnName)) {
      int rowSize = results.size();
      String[] columnValues = new String[rowSize];
      for (int i = 0; i < rowSize; i++) {
        String columnValue = results.get(i).get(columnName);
        columnValues[i] = columnValue;
      }
      logger.info("Get column '{}' values '{}' successfully", columnName, columnValues);
      return columnValues;
    } else {
      logger.error("The column '{}' does not exist", columnName);
      return null;
    }
  }

  public static boolean verifyColumnValues(
      List<Map<String, String>> results, String columnName, String[] expectedValues) {
    String[] actualValues = getColumnValues(results, columnName);
    Arrays.sort(actualValues);
    Arrays.sort(expectedValues);
    if (Arrays.equals(actualValues, expectedValues)) {
      logger.info("Column '{}' values are as expected '{}'", columnName, actualValues);
      return true;
    } else {
      logger.error(
          "Column '{}' values are incorrect, expected are '{}', actual are '{}'",
          columnName,
          expectedValues,
          actualValues);
      return false;
    }
  }

  public static Map<String, String> getSingleRow(List<Map<String, String>> results) {
    if (results.size() > 0) {
      logger.info("Get query single row successfully");
      return results.get(0);
    } else {
      logger.error("Query result is null.");
      return null;
    }
  }

  public static String queryOtp(Connection connection,String sql) {
    List<Map<String, String>> results = queryData(connection,sql);
    assert results != null;
    String sendTONum = results.get(0).get("send_to");
    if (sendTONum.equals("ben050@ben.ben") || sendTONum.equals("85288880050")) {
      logger.info(results.get(0).get("code"));
      return GlobalVar.GLOBAL_VARIABLES.put("code",results.get(0).get("code"));
    }
    return null;
  }
}
