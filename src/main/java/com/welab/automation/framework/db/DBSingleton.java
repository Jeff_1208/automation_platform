package com.welab.automation.framework.db;

import com.welab.automation.framework.utils.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DBSingleton {
  private static final Logger logger = LoggerFactory.getLogger(DBSingleton.class);
  private static DBSingleton dbSingleton;
  private static DBFactory dbFactory;
  private static Connection connection;

  private DBSingleton() {}

  public static synchronized DBSingleton getInstance() {
    if (null == dbSingleton) {
      dbSingleton = new DBSingleton();
    }
    return dbSingleton;
  }

  public synchronized Connection getConnection() {
    if (null == connection) {
      connectToDB();
    }
    return connection;
  }

  public void disconnectFromDB() {
    if (connection != null) {
      try {
        connection.close();
        connection = null;
        logger.info("DB is disconnected");
      } catch (Exception e) {
        logger.error("Failed to disconnect from DB with error: {}", e.getMessage());
      }
    }
  }

  private static void connectToDB() {
    PropertiesReader propertiesReader = PropertiesReader.getInstance();
    String dbType = propertiesReader.getProperty("dbType").toUpperCase();
    String dbName = propertiesReader.getProperty("dbName");
    String dbUser = propertiesReader.getProperty("dbUser");
    String dbPassword = propertiesReader.getProperty("dbPassword");
    switch (dbType) {
      case "POSTGRESQL":
        dbFactory = new PostGreSqlFactory();
        break;
      case "SQLSERVER":
        dbFactory = new SqlServerFactory();
        break;
      default:
        {
          logger.error("Cannot connect to db for type: {}", dbType);
          throw new IllegalArgumentException("Cannot connect to db for type: " + dbType);
        }
    }
    connection = dbFactory.connectDB(dbName, dbUser, dbPassword);
    logger.info("Connect to {} db successfully", dbType);
    try {
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      logger.error("Failed to set auto commit with error: {}", e.getMessage());
    }
  }
}
