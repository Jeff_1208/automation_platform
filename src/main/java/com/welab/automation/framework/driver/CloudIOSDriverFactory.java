package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.app.CapabilitiesBuilder;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class CloudIOSDriverFactory implements DriverFactory {
  private static final Logger logger = LoggerFactory.getLogger(BaseDriver.class);

  @Override
  public RemoteWebDriver getDriver() {
    String cloudDevice = PropertiesReader.getInstance().getProperty("cloudDevice");
    try {
      return new IOSDriver(
          new URL(
              String.format(
                  "https://%s:%s@%s/wd/hub",
                  GlobalVar.PROJECT_NAME,
                  URLEncoder.encode(
                      TestDataReader.getAuthorization().replace("Bearer ", ""), "UTF-8"),
                  GlobalVar.APPIUM_HUB)),
          CapabilitiesBuilder.getIOSCloudCapabilities(cloudDevice));
    } catch (UnsupportedEncodingException e) {
      logger.error(e.getMessage());
    } catch (MalformedURLException e) {
      logger.error(e.getMessage());
    }
    return null;
  }
}
