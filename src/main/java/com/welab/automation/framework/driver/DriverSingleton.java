
package com.welab.automation.framework.driver;

import com.welab.automation.framework.utils.TestDataReader;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static io.appium.java_client.service.local.flags.GeneralServerFlag.LOG_LEVEL;
import static io.appium.java_client.service.local.flags.GeneralServerFlag.SESSION_OVERRIDE;

public class DriverSingleton {
    private static final Logger logger = LoggerFactory.getLogger(DriverSingleton.class);
    private static DriverSingleton driverSingleton;
    private static WebDriver driver;
    private static AppiumDriverLocalService appiumDriverLocalService;
    private static boolean isAppFolderDeleted;

    private DriverSingleton() {
    }

    public static DriverSingleton getInstance() {
        if (null == driverSingleton) {
            driverSingleton = new DriverSingleton();
        }
        return driverSingleton;
    }

    private static void startAppiumService() {
        AtomicInteger port = new AtomicInteger();
        AppiumDriverLocalService service = null;
        service = new AppiumServiceBuilder()
                .usingAnyFreePort()
                .withIPAddress(TestDataReader.getLocalAddress())
                .withArgument(SESSION_OVERRIDE)
                .withArgument(LOG_LEVEL, "error")
                .usingDriverExecutable(new File(TestDataReader.getDriverPath()))
                .withAppiumJS(new File(TestDataReader.getAppiumPath()))
                .build();
        Optional.ofNullable(service).ifPresent(s -> {
            s.start();
            port.set(s.getUrl().getPort());
        });
        appiumDriverLocalService = service;
        logger.info("the appium service started on port {}", port);
    }

    public static AppiumDriverLocalService getAppiumService() {
        if(null == appiumDriverLocalService) startAppiumService();
        return appiumDriverLocalService;
    }

    public static void stopAppiumService() {
        Optional.ofNullable(appiumDriverLocalService).ifPresent(AppiumDriverLocalService::stop);
        logger.info("the appium service was stopped");
    }
}
