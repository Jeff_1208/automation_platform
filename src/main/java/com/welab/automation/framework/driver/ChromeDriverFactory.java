package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.welab.automation.framework.utils.FileUtil.copyFile;

public class ChromeDriverFactory implements DriverFactory {

  @Override
  public RemoteWebDriver getDriver() {
    WebDriverManager.chromedriver().setup();
    String sourceDrivePath = WebDriverManager.chromedriver().getDownloadedDriverPath();
    String targetDrivePath =
        System.getProperty("user.dir") + "/src/main/resources/driver/chromedriver.exe";
    copyFile(sourceDrivePath, targetDrivePath);
    ChromeOptions chromeOptions = new ChromeOptions();
    boolean headlessFlag = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"));
    chromeOptions.setHeadless(headlessFlag);
    chromeOptions.addArguments("--ignore-certificate-errors");
    chromeOptions.addArguments("--disable-gpu");
    chromeOptions.addArguments("--start-maximized");
    chromeOptions.addArguments("--lang=es-US");
    RemoteWebDriver driver = new RemoteWebDriver(BaseDriver.getURL(), chromeOptions);
    return driver;
  }
}
