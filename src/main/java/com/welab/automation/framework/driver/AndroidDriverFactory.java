package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.app.CapabilitiesBuilder;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class AndroidDriverFactory implements DriverFactory {
  @Override
  public RemoteWebDriver getDriver() {

    RemoteWebDriver driver;
    if ((GlobalVar.GLOBAL_VARIABLES.get("appiumProvider") != null)
        && GlobalVar.GLOBAL_VARIABLES.get("appiumProvider").equals("desktop")) {
      try {
        URL url = new URL("http://0.0.0.0:4723/wd/hub");
        driver = new AndroidDriver(url, CapabilitiesBuilder.getAndroidLocalCapabilities());
      } catch (Exception e) {
        System.out.println("init driver failed:" + e.getMessage());
        driver = null;
      }
    } else {
      AppiumDriverLocalService service = DriverSingleton.getInstance().getAppiumService();
      driver =
          new AndroidDriver(service.getUrl(), CapabilitiesBuilder.getAndroidLocalCapabilities());
    }

    assertThat(driver).isNotNull();

    return driver;
  }
}
