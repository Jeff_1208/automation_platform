package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.app.CapabilitiesBuilder;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class IOSDriverFactory implements DriverFactory {
  @Override
  public RemoteWebDriver getDriver() {
    // connect to appium service first
    // start appium service if it is not start manually
    RemoteWebDriver driver;
    if ((GlobalVar.GLOBAL_VARIABLES.get("appiumProvider") != null)
        && GlobalVar.GLOBAL_VARIABLES.get("appiumProvider").equals("desktop")) {
      try {
        URL url = new URL("http://0.0.0.0:4723/wd/hub");
        driver = new IOSDriver(url, CapabilitiesBuilder.getIosLocalCapabilities());
      } catch (Exception e) {
        System.out.println("init driver failed:" + e.getMessage());
        driver = null;
      }
    } else {
      AppiumDriverLocalService service = DriverSingleton.getInstance().getAppiumService();
      driver = new IOSDriver(service.getUrl(), CapabilitiesBuilder.getIosLocalCapabilities());
    }

    assertThat(driver)
        .overridingErrorMessage("appium ios driver init failed: <%s>", driver)
        .isNotNull();

    return driver;
  }
}
