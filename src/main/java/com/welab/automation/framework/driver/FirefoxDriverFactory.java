package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.welab.automation.framework.utils.FileUtil.copyFile;

public class FirefoxDriverFactory implements DriverFactory {

  @Override
  public RemoteWebDriver getDriver() {
    WebDriverManager.firefoxdriver().setup();
    String sourceDrivePath = WebDriverManager.firefoxdriver().getDownloadedDriverPath();
    String targetDrivePath =
        System.getProperty("user.dir") + "/src/main/resources/driver/geckodriver.exe";
    copyFile(sourceDrivePath, targetDrivePath);
    FirefoxOptions firefoxOptions = new FirefoxOptions();
    boolean headlessFlag = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"));
    firefoxOptions.setHeadless(headlessFlag);
    RemoteWebDriver driver = new RemoteWebDriver(BaseDriver.getURL(), firefoxOptions);
    driver.manage().window().maximize();
    return driver;
  }
}
