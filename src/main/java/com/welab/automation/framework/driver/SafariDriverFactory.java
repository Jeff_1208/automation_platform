package com.welab.automation.framework.driver;

import com.welab.automation.framework.GlobalVar;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

public class SafariDriverFactory implements DriverFactory {

  @Override
  public RemoteWebDriver getDriver() {
    SafariOptions safariOptions = new SafariOptions();
    boolean headlessFlag = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"));
    safariOptions.setCapability("headless", headlessFlag);
    safariOptions.setCapability("--lang", "es-US");
    WebDriver driver = new SafariDriver(safariOptions);
    return (RemoteWebDriver) driver;
  }
}
