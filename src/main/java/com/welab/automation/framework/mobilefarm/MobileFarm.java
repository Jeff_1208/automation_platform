package com.welab.automation.framework.mobilefarm;

import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.TimeUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class MobileFarm {
  private static final Logger logger = LoggerFactory.getLogger(BaseDriver.class);

  //  public static void getDeviceAvailableTimeSlot() {
  //    RestAssured.baseURI = GlobalVar.MOBILE_FARM_API_URL;
  //    RestAssured.basePath = "v2/bookings/{bid}/slots";
  //
  //    RestAssured.given().contentType(ContentType.JSON).pathParam("bid", 0).body(body).post();
  //  }

  public static void bookDevice(String deviceName) {
    RestAssured.baseURI = GlobalVar.MOBILE_FARM_API_URL;
    RestAssured.basePath = "v2/bookings/{bid}";

    JSONObject schemas = FileUtil.getJsonObj(GlobalVar.MOBILE_FARM_API_SCHEMA);
    JSONObject jsonObject = schemas.getJSONObject("bookDevice");
    jsonObject.replace("name", deviceName);
    JSONObject devices = FileUtil.getJsonObj(GlobalVar.DEVICE_JSON_PATH);
    JSONObject devices_caps = devices.getJSONObject(deviceName);
    jsonObject.getJSONArray("devices").add(devices_caps.getString("serialId"));

    // Get timestamp, current time+5mins
    long startTime = TimeUtils.generateUnixTimeStampPlus(5, ChronoUnit.MINUTES);
    long endTime = TimeUtils.generateUnixTimeStampPlus(65, ChronoUnit.MINUTES);
    jsonObject.replace("start", startTime);
    jsonObject.replace("end", endTime);
    logger.info(String.valueOf(jsonObject));

    Map<String, Object> headers = new HashMap<>();
    headers.put("Authorization", TestDataReader.getAuthorization());
    headers.put("Content-Type", ContentType.JSON);

    Response response =
        RestAssured.given().headers(headers).pathParam("bid", 0).body(jsonObject).post();
    logger.info(
        String.format(
            "Status code: %s, \nResponse: %s",
            response.getStatusCode(), response.body().asString()));
    Assert.assertEquals(response.getStatusCode(), 200, "Device not available, please check!");
  }

  public static void installApp() {}

  public static void uploadFile() {}
}
