Feature:  Welab app Maintenance

  Background: Login
    Given Open Stage WeLab App
#    And Enable skip Root Stage
#    Given Check upgrade page appears Stage
    When Login with user and password from properties
#    Then I can see the LoggedIn page

  @ACBN-T636 @114
  Scenario: Reg_Maintenance_0003 Update daily limit with notification
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0003_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0003_Android_02 |
    When I clicked the trading limit button
      | screenShotName | Reg_Maintenance_0003_Android_03 |
      | screenShotName1 | Reg_Maintenance_0003_Android_04 |
      | screenShotName2 | Reg_Maintenance_0003_Android_05 |
      | amount | 0 |
      | key | 123456|
    When Verify whether the modified limit is changed
      | amount | 0 |
    When I clicked the trading limit button
      | screenShotName | Reg_Maintenance_0003_Android_06 |
      | screenShotName1 | Reg_Maintenance_0003_Android_07 |
      | screenShotName2 | Reg_Maintenance_0003_Android_08 |
      | amount | 480000 |
      | key | 123456|
    When Verify whether the modified limit is changed
      | amount | 480000 |



    @ACBN-T662
    Scenario: Reg_Maintenance_0005 Change residential address with notification
      Then Navigate to My Accounts
        | screenShotName | Reg_Maintenance_0005_Android_01 |
      When Click on Settings
        | screenShotName | Reg_Maintenance_0005_Android_02 |
      Then Click personal information title
      Then through adb common input MSK password
        | screenShotName | Reg_Maintenance_0005_Android_03 |
      When I clicked the personal address modification
        | screenShotName | Reg_Maintenance_0005_Android_04 |
      Then I update Residential Address
        | screenShotName | Reg_Maintenance_0005_Android_05 |
        | screenShotName1 | Reg_Maintenance_0005_Android_06 |

  @ACBN-T664
  Scenario: Reg_Maintenance_0006 Change corresponding (mailing) address with notification
    Then Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0006_Android_01 |
    When Click on Settings
      | screenShotName | Reg_Maintenance_0006_Android_02 |
    Then Click personal information title
    Then through adb common input MSK password
      | screenShotName | Reg_Maintenance_0006_Android_03 |
    When I clicked the personal address modification
      | screenShotName | Reg_Maintenance_0006_Android_04 |
    Then I update Email Address
      | screenShotName | Reg_Maintenance_0006_Android_05 |
      | screenShotName1 | Reg_Maintenance_0006_Android_06 |


  @ACBN-T666
  Scenario: Reg_Maintenance_0007 Change employment status with notification
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0007_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0007_Android_02 |
    And I move eyeBtn to top right corner
    Then  Click personal information title
    Then  through adb common input MSK password
      | screenShotName | Reg_Maintenance_0007_Android_03 |
    Then  Modify  the employment details
      | screenShotName | Reg_Maintenance_0007_Android_04 |
      | screenShotName1 | Reg_Maintenance_0007_Android_05 |
      | screenShotName2 | Reg_Maintenance_0007_Android_06 |
      | screenShotName3 | Reg_Maintenance_0007_Android_07 |
      | screenShotName4 | Reg_Maintenance_0007_Android_08 |
      | screenShotName5 | Reg_Maintenance_0007_Android_09 |
      | screenShotName6 | Reg_Maintenance_0007_Android_10 |
      | screenShotName7 | Reg_Maintenance_0007_Android_11 |
    Then Verify that the modification is successful
      | screenShotName | Reg_Maintenance_0007_Android_12 |

#  Scenario Outline: Reg_Maintenance_0008 Change nationality info with notification
#    Then  Navigate to My Accounts
#      | screenShotName | Reg_Maintenance_0008_Android_01 |
#    When  Click on Settings
#      | screenShotName | Reg_Maintenance_0008_Android_02 |
#    Then  Click personal information title
#    Then  through adb common input MSK password
#      | screenShotName | Reg_Maintenance_0008_Android_03 |
#    Then  Modify nationality or region information <nationalityOrRegion> and <tax>
#      | screenShotName  | Reg_Maintenance_0008_Android_04 |
#      | screenShotName1 | Reg_Maintenance_0008_Android_05 |
#      | screenShotName2 | Reg_Maintenance_0008_Android_06 |
#      | screenShotName3 | Reg_Maintenance_0008_Android_07 |
#    Examples:
#      | nationalityOrRegion                 | tax                     |
#      | Hong Kong SAR,china,Australia       | ,123456789123456,123    |

  @ACBN-T668
  Scenario: Reg_Maintenance_0009 Support
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0009_Android_01 |
    And  Click on Support
      | screenShotName | Reg_Maintenance_0009_Android_02 |
      | screenShotName1 | Reg_Maintenance_0009_Android_03 |
      | screenShotName2 | Reg_Maintenance_0009_Android_04 |
#      | screenShotName3 | Reg_Maintenance_0009_Android_05 |
#      | screenShotName4 | Reg_Maintenance_0009_Android_06 |
#      | screenShotName5 | Reg_Maintenance_0009_Android_07 |
#      | screenShotName6 | Reg_Maintenance_0009_Android_08 |

  @ACBN-T670
  Scenario: Reg_Maintenance_0010 About Us
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0010_Android_01 |
    And  Click on About us
      | screenShotNameUp | Reg_Maintenance_0010_Android_02 |
      | screenShotNameMiddle | Reg_Maintenance_0010_Android_03 |
      | screenShotNameDown | Reg_Maintenance_0010_Android_04 |
    And  I click phone,email,bank url  and ig
      | screenShotName | Reg_Maintenance_0010_Android_05 |
      | screenShotName1 | Reg_Maintenance_0010_Android_06 |
    And  I click other prompts or notices and verify them
      | screenShotName | Reg_Maintenance_0010_Android_07 |
      | screenShotName1 | Reg_Maintenance_0010_Android_08 |
      | screenShotName2 | Reg_Maintenance_0010_Android_09 |
      | screenShotName3 | Reg_Maintenance_0010_Android_10 |
      | screenShotName4 | Reg_Maintenance_0010_Android_11 |
      | screenShotName5 | Reg_Maintenance_0010_Android_12 |
      | screenShotName6 | Reg_Maintenance_0010_Android_13 |
      | screenShotName7 | Reg_Maintenance_0010_Android_14 |


  @ACBN-T672
  Scenario: Reg_Maintenance_0012 Change Marketing Preferences
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0012_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0012_Android_02 |
    Then  Click marketing preferences title
      | screenShotName | Reg_Maintenance_0012_Android_03 |
    Then  Trigger Marketing Preferences flow and check notification
      | screenShotName | Reg_Maintenance_0012_Android_04 |
      | screenShotName01 | Reg_Maintenance_0012_Android_05 |

  @ACBN-T674
  Scenario Outline: Reg_Maintenance_0015 Tax Jurisdiction Checking
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0015_Android_01 |
    When Click on Settings
      | screenShotName | Reg_Maintenance_0015_Android_02 |
    Then Click personal information title
    Then through adb common input MSK password
      | screenShotName | Reg_Maintenance_0015_Android_03 |
    Then  Modify press through all the options <nationalityOrRegion> and <reason>
      | screenShotName  | Reg_Maintenance_0015_Android_04 |
      | screenShotName1 | Reg_Maintenance_0015_Android_05 |
      | screenShotName2 | Reg_Maintenance_0015_Android_06 |
      | screenShotName3 | Reg_Maintenance_0015_Android_07 |
      | screenShotName4 | Reg_Maintenance_0015_Android_08 |
    Then I delete Nationality
    Examples:
      | nationalityOrRegion                       | reason  |
      | American Samoa,Haiti,Afghanistan          | no tin  |

  @ACBN-T1155
  Scenario: Reg_Maintenance_0016 Update Account Opening Information
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0016_Android_01 |
    When Click on Settings
      | screenShotName | Reg_Maintenance_0016_Android_02 |
    Then Click personal information title
    Then through adb common input MSK password
      | screenShotName | Reg_Maintenance_0016_Android_03 |
    Then Update Account opening information Android
      | screenShotName | Reg_Maintenance_0016_Android_04 |
      | screenShotName1 | Reg_Maintenance_0016_Android_05 |
    Then I verify Account opening information
      | screenShotName | Reg_Maintenance_0016_Android_06 |

  @ACBN-T658 @113   @health
  Scenario: Reg_Maintenance_0002 change MSK PIN
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0002_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0002_Android_02 |
    When I clicked the Change mobile security key button
      | screenShotName | Reg_Maintenance_0002_Android_03 |
    Then through adb common input MSK password
      | screenShotName | Reg_Maintenance_0002_Android_04 |
    When I enter new key
      | screenShotName1 | Reg_Maintenance_0002_Android_05 |
      | screenShotName2 | Reg_Maintenance_0002_Android_06 |
#    When I restored my key
    When I clicked the Change mobile security key button
      | screenShotName | Reg_Maintenance_0002_Android_07 |
    Then I change msk back
      | screenShotName | Reg_Maintenance_0002_Android_08 |
      | screenShotName1 | Reg_Maintenance_0002_Android_09 |

  @ACBN-T652 @VCAT-51
  Scenario:Reg_Payment_0015 rebind device and limit change 10000
    And I verify Available balance
      | screenShotName  | Reg_Payment_0015_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I see send money limit
      |accountNumber|1000476928 |
      |holdname     |WILSON,Wil |
      |money        |12000      |
      | screenShotName    | Reg_Payment_0015_Android_02 |
      | screenShotName1   | Reg_Payment_0015_Android_03 |

  @ACBN-T660 @115
  Scenario: Reg_Maintenance_0004 rebind device daily limit to10001
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0004_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0004_Android_02 |
    When I clicked the trading limit button
      | screenShotName | Reg_Maintenance_0004_Android_03 |
      | screenShotName1 | Reg_Maintenance_0004_Android_04 |
      | screenShotName2 | Reg_Maintenance_0004_Android_05 |
      | amount | 100001 |
      | key | 123456|
    Then Verify rebind device daily limit
      | screenShotName | Reg_Maintenance_0004_Android_06 |

  @ACBN-T656
  Scenario: Reg_Maintenance_0001 Change password with notification
    Then  Navigate to My Accounts
      | screenShotName  | Reg_Maintenance_0001_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0001_Android_02 |
    When I clicked the change password button
      | screenShotName  | Reg_Maintenance_0001_Android_03 |
    When I entered my old password
      | screenShotName  | Reg_Maintenance_0001_Android_04 |
    When Click next
      | screenShotName  | Reg_Maintenance_0001_Android_05 |
    When Enter new password
      | screenShotName  | Reg_Maintenance_0001_Android_06 |
    When Click Done
      | screenShotName  | Reg_Maintenance_0001_Android_07 |
    Then through adb common input MSK password
      | screenShotName  | Reg_Maintenance_0001_Android_08 |
    When I clicked the Settings
      | screenShotName  | Reg_Maintenance_0001_Android_09 |
    When I clicked the log out
      | screenShotName  | Reg_Maintenance_0001_Android_10 |
    When Login again
      | screenShotName  | Reg_Maintenance_0001_Android_11 |
      | screenShotName1 | Reg_Maintenance_0001_Android_12 |
    Then I can see the Login page
      | screenShotName  | Reg_Maintenance_0001_Android_13 |
    Then I restored my password




