Feature: Welab app login

  Background: Open App
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage

  @health
  Scenario:  Reg_Login_005 IOS Balance enquiry and transaction history sequence
    Then get app version
      | screenShotName | Reg_Login_005_IOS_01 |
    When Login with user and password from properties
    Then I can see the LoggedIn page
    Then get screenShot
      | screenShotName | Reg_Login_005_IOS_02 |
    Then check transaction detail
      | screenShotName | Reg_Login_005_IOS_03 |
      | screenShotName1 | Reg_Login_005_IOS_04 |