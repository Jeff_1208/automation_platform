Feature: Payment

  Background: Login
    Given Open Stage WeLab App
    When Login with user and password from properties
    Then I can see the LoggedIn page

  @VCAT-43  @health
  Scenario: Reg_Payment_001 Outbound FPS transfer(welab bank) with notification
    And I verify Available balance
      | screenShotName | Reg_Payment_001_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by welab bank
      |accountNumber|1000476928|
      |holdname     |WILSON,Wil|
      |money        |10        |
      | screenShotName | Reg_Payment_001_Android_02 |
    And I verify send money detail by bank transfer
      | screenShotName | Reg_Payment_001_Android_03 |
    And I verify Available balance after send money
      | screenShotName | Reg_Payment_001_Android_04 |
      | screenShotName1 | Reg_Payment_001_Android_05 |
    And I verify success send money record
      | screenShotName | Reg_Payment_001_Android_06 |

  @VCAT-44
  Scenario: Reg_Payment_002 Outbound FPS transfer(other bank) with notification
    And I verify Available balance
      | screenShotName  | Reg_Payment_002_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by bank672
      |accountNumber|1000476928|
      |holdname     |WILSON,Wil|
      |money        |20       |
      | screenShotName  | Reg_Payment_002_Android_02 |
    And I verify send money detail by bank transfer
      | screenShotName  | Reg_Payment_002_Android_03 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_002_Android_04 |
    And I verify success send money record
      | screenShotName  | Reg_Payment_002_Android_05 |

  @VCAT-45
  Scenario Outline: Reg_Payment_008 Non-payee, pay max limit 300k HKD.
    And I verify Available balance
      | screenShotName | Reg_Payment_008_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to <FPSID>
    Then I can see Non-payee page
      | screenShotName | Reg_Payment_008_Android_02 |
    And I enter <amount>
    Then I can see transfer in progress
      | screenShotName | Reg_Payment_008_Android_03 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_008_Android_04 |
    Then I verify transactions
      | screenShotName  | Reg_Payment_008_Android_05 |
      | screenShotName1 | Reg_Payment_008_Android_06 |
    Examples:
      | FPSID    | amount |
      | 88880194 | 10000  |

  @VCAT-46
  Scenario Outline: Reg_Payment_009 new payee,pay max limit 300k HKD
    And I verify Available balance
      | screenShotName | Reg_Payment_009_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I move eyeBtn to top right corner
    And I verify no payee <FPSID>
    And I transfer money to <FPSID>
    Then I can see Non-payee page and add payee
      | screenShotName | Reg_Payment_009_Android_02 |
    And I enter <amount>
    Then I can see transfer in progress
      | screenShotName | Reg_Payment_009_Android_03 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_009_Android_04 |
    And I verify Available balance
      | screenShotName | Reg_Payment_009_Android_05 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to my payee
      | screenShotName | Reg_Payment_009_Android_06 |
    And I enter money <amount2>
    Then I can see transfer in progress
      | screenShotName | Reg_Payment_009_Android_07 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_009_Android_08 |
    Then I verify transactions
      | screenShotName  | Reg_Payment_009_Android_09 |
      | screenShotName1 | Reg_Payment_009_Android_10 |
    Examples:
      | FPSID    | amount | amount2 |
      | 88880194 | 10     | 10000   |

  @VCAT-50
  Scenario: Reg_Payment_014 Outbound FPS transfer directly to bank account with incorrect payee name
    And I verify Available balance
      | screenShotName  | Reg_Payment_014_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by welab bank
      |accountNumber|1000476928|
      |holdname     |WILSON    |
      |money        |100        |
      | screenShotName   | Reg_Payment_014_Android_02 |
    And I verify send money detail by bank transfer
      | screenShotName   | Reg_Payment_014_Android_03 |
    And I verify fail Available balance after send money
      | screenShotName   | Reg_Payment_014_Android_04 |
      | screenShotName1  | Reg_Payment_014_Android_05 |
    And I verify fail send money record
      | screenShotName   | Reg_Payment_014_Android_06 |
      | screenShotName1  | Reg_Payment_014_Android_07 |


  @VCAT-52
  Scenario Outline: Reg_Payment_0017 bill payment transaction
    And I verify Available balance
      | screenShotName | Reg_Payment_0017_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to <FPSID>
    Then I can see merchant Information
      | screenShotName | Reg_Payment_0017_Android_02 |
    And I enter <billnum> and <amount>
    Then I can see transfer in progress
      | screenShotName | Reg_Payment_0017_Android_03 |
    Then I verify transactions
      | screenShotName  | Reg_Payment_0017_Android_04 |
      | screenShotName1 | Reg_Payment_0017_Android_05 |
    Examples:
      | FPSID     | billnum                   | amount |
      | 166924688 | DBSBillnumbertestingTerry | 1      |

  @VCAT-66  @health
  Scenario: Reg_Payment_0010 Use EDDI to add money
    And I verify Available balance
      | screenShotName  | Reg_Payment_0010_Android_01 |
    And I goto Transfer page
    And I click add money Button at Transfer page
      | screenShotName  | Reg_Payment_0010_Android_02 |
    Then I enter money at Add money page
      | money   | 100 |
      | screenShotName  | Reg_Payment_0010_Android_03 |
    And I verify add money success
      | screenShotName  | Reg_Payment_0010_Android_04 |
    Then go to home page
    And I verify add Money Record
      | screenShotName  | Reg_Payment_0010_Android_05 |
    And I verify add Money Balance
      | screenShotName  | Reg_Payment_0010_Android_06 |

  @VCAT-67
  Scenario: Reg_Payment_0012 set up auto eDDI rules
    And I verify Available balance
      | screenShotName   | Reg_Payment_0012_Android_01 |
    And I goto Transfer page
    And I click add money Button at Transfer page
      | screenShotName   | Reg_Payment_0012_Android_02 |
    And I turn on Auto Reload Button
      | screenShotName   | Reg_Payment_0012_Android_03 |
    Then I select repeat
      | screenShotName   | Reg_Payment_0012_Android_04 |
      | screenShotName1  | Reg_Payment_0012_Android_05 |
    Then I enter money at Add money page with Repeat Rule
      | money   | 100 |
      | screenShotName   | Reg_Payment_0012_Android_06 |
      | screenShotName1  | Reg_Payment_0012_Android_07 |
    And I verify add money with Repeat Rule success
      | screenShotName   | Reg_Payment_0012_Android_08 |
    Then go to home page
    And I verify add Money Record
      | screenShotName   | Reg_Payment_0012_Android_09 |
    And I verify add Money Balance
      | screenShotName   | Reg_Payment_0012_Android_10 |

  @VCAT-68
  Scenario: Reg_Payment_0013 change /delect auto eDDI rules
    And I goto Transfer page
    Then I click settingBtn on TransferPage
      | screenShotName   | Reg_Payment_0013_Android_01 |
    And I click Auto Reload settings on Transfer settings page
      | screenShotName   | Reg_Payment_0013_Android_02 |
    Then I delete first Auto Reload rule
      | screenShotName   | Reg_Payment_0013_Android_03 |
      | screenShotName1  | Reg_Payment_0013_Android_04 |







