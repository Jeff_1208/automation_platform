package com.welab.automation.projects.channel.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.channel.pages.web.GoSavePortalPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import java.util.Map;

public class GosavePortalSteps extends WebBasePage {
  GoSavePortalPage goSavePortalPage;

  public GosavePortalSteps(){
    goSavePortalPage = new GoSavePortalPage();
  }

  @Given("^Open the gosave portal URL ([^\"]\\S*)$")
  public void openUrl(String url) {
    try {
      navigateTo(url);
    } catch (Exception e) {
      logger.error(e.toString());
    }
  }

  @And("^Enter the user name and password and login gosave portal$")
  public void loginGosavePortal(Map<String,String> loginData) {
    goSavePortalPage.loginGosavePortal(loginData.get("username"),loginData.get("password"));
  }

  @And("open Deposit Scheme")
  public void openDepositScheme() {
    goSavePortalPage.openDepositScheme();
  }

  @And("click Deposit Scheme Create")
  public void clickDepositSchemeCreate() {
    goSavePortalPage.clickDepositSchemeCreate();
  }

  @And("click Deposit Scheme View")
  public void clickDepositSchemeView() {
    goSavePortalPage.clickDepositSchemeView();
  }

  @And("click Deposit Scheme Active")
  public void clickDepositSchemActive() {
    goSavePortalPage.clickDepositSchemeActive();
  }

  @And("click Deposit Scheme Pending Approval")
  public void clickDepositSchemePendingApproval() {
    goSavePortalPage.clickDepositSchemePendingApproval();
  }

  @And("fill Data For Create New Deposit")
  public void fillDataForCreateNewDeposit(Map<String,String> data) {
    goSavePortalPage.fillDataForCreateNewDeposit(data.get("screenShotName"),data.get("screenShotName1"));
  }
  @And("verify Create New Deposit Successfully")
  public void verifyCreateNewDepositSuccessfully(Map<String,String> data) {
    goSavePortalPage.verifyCreateNewDepositSuccessfully(data.get("screenShotName"));
  }

  @And("logout From Gosave Portal")
  public void logoutFromGosavePortal() {
    goSavePortalPage.logoutFromGosavePortal();
  }

  @And("approve the deposit by checker")
  public void approveTheDepositByChecker(Map<String,String> data) {
    goSavePortalPage.approveTheDepositByChecker(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
  }

  @And("check the deposit record in view page after create deposit successfully")
  public void checkDepositRecordInViewPageAfterCreateDepositSuccessfully(Map<String,String> data) {
    goSavePortalPage.checkDepositRecordInViewPageAfterCreateDepositSuccessfully(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
  }

  @And("check the deposit record in active page after create deposit successfully")
  public void checkDepositRecordInActivePageAfterCreateDepositSuccessfully(Map<String,String> data) {
    goSavePortalPage.checkDepositRecordInActivePageAfterCreateDepositSuccessfully(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
  }


}
