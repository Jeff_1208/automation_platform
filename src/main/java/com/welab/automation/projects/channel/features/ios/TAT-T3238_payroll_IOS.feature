Feature: Payroll

  Background: Open App
    Given Open Stage WeLab App
#    And ios skip Root
    Given Check upgrade page appears Stage


  Scenario:  Reg_Test bind device for payroll IOS
    And skip Rebind Device Authentication IOS
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    And bind Device Authentications


  @ACBN-T1105
  Scenario:  Reg_Payroll_001 AC1a send money trigger biometric IOS
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then  go to my Account Page
      | screenShotName | Reg_payroll_001_IOS_01 |
    And open payroll page
      | screenShotName | Reg_payroll_001_IOS_02 |
    And open Transaction Page
    And input Amount And Swipe The Slide
      | amount         | 10                     |
      | screenShotName | Reg_payroll_001_IOS_03 |
    And check Transaction Result Page
      | screenShotName | Reg_payroll_001_IOS_04 |
    And check Payroll Page
      | screenShotName | Reg_payroll_001_IOS_05 |


  @ACBN-T1099
  Scenario:  Reg_Payroll_002 AC1b add money trigger biometric IOS
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then  go to my Account Page
      | screenShotName | Reg_payroll_002_IOS_01 |
    And open payroll page
      | screenShotName | Reg_payroll_002_IOS_02 |
    And open Add Money Page
    And input Amount And Confirm
      | amount         | 11                     |
      | screenShotName | Reg_payroll_002_IOS_03 |
    And check Transaction Result Page
      | screenShotName | Reg_payroll_002_IOS_04 |
    And check Payroll Page
      | screenShotName | Reg_payroll_002_IOS_05 |

  @ACBN-T1103
  Scenario:  Reg_Payroll_003 AC1d Export payroll IOS
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then  go to my Account Page
      | screenShotName | Reg_payroll_003_IOS_01 |
    And open payroll page
      | screenShotName | Reg_payroll_003_IOS_02 |
    And open Download Page
    And check Estatement Content
      | screenShotName | Reg_payroll_003_IOS_03 |

  @ACBN-T1104
  Scenario:  Reg_Payroll_004 AC4 Payroll account eStatement IOS
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then  go to my Account Page
      | screenShotName | Reg_payroll_004_IOS_01 |
    And open payroll page
      | screenShotName | Reg_payroll_004_IOS_02 |
    And open Download Page
    And check Estatement Content
      | screenShotName | Reg_payroll_004_IOS_03 |
    And download Estatement PDF
      | screenShotName | Reg_payroll_004_IOS_04 |
