Feature:  GoSave Portal

  Background: GoSave Portal

  @GOS-T26
  Scenario: Reg_gosave_portal_001 create a new deposit flow
    Given Open the gosave portal URL https://welabone-sta2.sta-wlab.net/bulgarians/home
    And Enter the user name and password and login gosave portal
      | username | go-save-maker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Create
    And fill Data For Create New Deposit
      | screenShotName               | Reg_gosave_portal_001_web_01 |
      | screenShotName1              | Reg_gosave_portal_001_web_02 |
    And verify Create New Deposit Successfully
      | screenShotName               | Reg_gosave_portal_001_web_03 |
    And logout From Gosave Portal
    And Enter the user name and password and login gosave portal
      | username | go-save-checker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Pending Approval
    And approve the deposit by checker
      | screenShotName               | Reg_gosave_portal_001_web_04 |
      | screenShotName1              | Reg_gosave_portal_001_web_05 |
      | screenShotName2              | Reg_gosave_portal_001_web_06 |

  @GOS-T27
  Scenario: Reg_gosave_portal_002 check the deposit record in view page after create deposit successfully
    Given Open the gosave portal URL https://welabone-sta2.sta-wlab.net/bulgarians/home
    And Enter the user name and password and login gosave portal
      | username | go-save-maker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Create
    And fill Data For Create New Deposit
      | screenShotName               | Reg_gosave_portal_002_web_01 |
      | screenShotName1              | Reg_gosave_portal_002_web_02 |
    And verify Create New Deposit Successfully
      | screenShotName               | Reg_gosave_portal_002_web_03 |
    And logout From Gosave Portal
    And Enter the user name and password and login gosave portal
      | username | go-save-checker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Pending Approval
    And approve the deposit by checker
      | screenShotName               | Reg_gosave_portal_002_web_04 |
      | screenShotName1              | Reg_gosave_portal_002_web_05 |
      | screenShotName2              | Reg_gosave_portal_002_web_06 |
    And click Deposit Scheme View
    And check the deposit record in view page after create deposit successfully
      | screenShotName               | Reg_gosave_portal_002_web_07 |
      | screenShotName1              | Reg_gosave_portal_002_web_08 |
      | screenShotName2              | Reg_gosave_portal_002_web_09 |

  @GOS-T28
  Scenario: Reg_gosave_portal_003 check the deposit record in active page after create deposit successfully
    Given Open the gosave portal URL https://welabone-sta2.sta-wlab.net/bulgarians/home
    And Enter the user name and password and login gosave portal
      | username | go-save-maker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Create
    And fill Data For Create New Deposit
      | screenShotName               | Reg_gosave_portal_003_web_01 |
      | screenShotName1              | Reg_gosave_portal_003_web_02 |
    And verify Create New Deposit Successfully
      | screenShotName               | Reg_gosave_portal_003_web_03 |
    And logout From Gosave Portal
    And Enter the user name and password and login gosave portal
      | username | go-save-checker-1 |
      | password | Aa123456 |
    And open Deposit Scheme
    And click Deposit Scheme Pending Approval
    And approve the deposit by checker
      | screenShotName               | Reg_gosave_portal_003_web_04 |
      | screenShotName1              | Reg_gosave_portal_003_web_05 |
      | screenShotName2              | Reg_gosave_portal_003_web_06 |
    And click Deposit Scheme Active
    And check the deposit record in active page after create deposit successfully
      | screenShotName               | Reg_gosave_portal_003_web_07 |
      | screenShotName1              | Reg_gosave_portal_003_web_08 |
      | screenShotName2              | Reg_gosave_portal_003_web_09 |