package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.channel.pages.CommonPage;
import com.welab.automation.projects.channel.pages.GoSave2Page;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class GoSave2Steps {
    GoSave2Page goSave2Page;
    CommonPage commonPage;
    public GoSave2Steps(){
        goSave2Page = new GoSave2Page();
        commonPage = new CommonPage();
    }

    @And("get total balance")
    public void getTotalBalance(Map<String, String> data) {
        goSave2Page.getTotalBalanceBeforeJoinPot(data.get("screenShotName"));
    }

    @Given("I go to GoSave2 Page")
    public void openGoSave2Page(Map<String, String> data) {
        goSave2Page.openGoSave2Page(data.get("screenShotName"),data.get("screenShotName01"));
    }

    @And("open my GoSave2 Page")
    public void openMyGoSave2Page(Map<String, String> data) {
        goSave2Page.openMyGoSave2Page(data.get("screenShotName"));
    }
    @And("click back button")
    public void clickBackButton() {
        goSave2Page.clickBackButton();
    }
    @And("go to main page")
    public void goToMainPage() {
        goSave2Page.goToMainPage();
    }

    @And("click go to gosave1")
    public void clickGoToGoSave1(Map<String, String> data) {
        goSave2Page.clickGoToGoSave1(data.get("screenShotName"));
    }
    @And("click go to my gosave detail")
    public void clickMyGoSaveDetail() {
        goSave2Page.clickMyGoSaveDetail();
    }

    @And("click Join button")
    public void clickAddPotButton() {
        goSave2Page.clickAddPotButton();
    }
    @And("click Join more button")
    public void clickJoinMoreButton() {
        goSave2Page.clickJoinMoreButton();
    }


    @And("click the gosave pot")
    public void clickGoSavePot() {
        goSave2Page.clickGoSavePot();
    }

    @And("click the gosave pot center")
    public void clickGoSavePotCenter() {
        goSave2Page.clickGoSavePotCenter();
    }
    @And("scroll to bottom")
    public void scrollToBottom() {
        goSave2Page.scrollToBottom();
    }

    @And("input amount for gosave")
    public void inputAmountForGoSave(Map<String, String> data) {
        goSave2Page.inputAmountForGoSave(data.get("amount"),data.get("screenShotName"));
    }

    @And("input gosave amount")
    public void inputGoSaveAmount(Map<String, String> data) {
        goSave2Page.inputGoSaveAmount(data.get("amount"));
    }
    @And("input gosave amount and check interest rate")
    public void inputGoSaveAmountAndCheckInterestRate(Map<String, String> data) {
        goSave2Page.inputGoSaveAmountAndCheckInterestRate(data.get("amount"),data.get("screenShotName"));
    }

    @And("confirm gosave")
    public void confirmGosave(Map<String, String> data) {
        goSave2Page.confirmGosave(data.get("screenShotName"));
    }
    @And("confirm gosave with promotion code")
    public void confirmGosaveWithPromotionCode(Map<String, String> data) {
        goSave2Page.confirmGosaveWithPromotionCode(data.get("screenShotName"), data.get("screenShotName1"));
    }
    @And("verify gosave")
    public void verifyGosave(Map<String, String> data) {
        goSave2Page.verifyGosave(data.get("screenShotName"));
    }

    @Then("verify total balance")
    public void verifyTotalBalance(Map<String, String> data) {
        goSave2Page.verifyTotalBalance(data.get("screenShotName"));
    }

    @And("withdrawal money before pot start")
    public void withdrawalMoneyBeforePotStart(Map<String, String> data) {
        goSave2Page.withdrawalMoneyBeforePotStart(data.get("amount"),data.get("screenShotName"));
    }

    @And("check gosave in progress")
    public void checkGoSaveInprogress(Map<String, String> data) {
        goSave2Page.checkGoSaveInprogress(data.get("screenShotName"),data.get("screenShotName1"));
    }
    @And("withdrawal money after pot start")
    public void withdrawalMoneyAfterPotStart(Map<String, String> data) {
        goSave2Page.withdrawalMoneyAfterPotStart(data.get("amount"));
    }

    @And("withdrawal after pot start")
    public void withdrawalAfterPotStart(Map<String, String> data) {
        goSave2Page.withdrawalAfterPotStart(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("view started pot")
    public void viewStartedPot(Map<String, String> data) {
        goSave2Page.viewStartedPot(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("view end pot")
    public void viewEndPot(Map<String, String> data) {
        goSave2Page.viewEndPot(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("go to end pot")
    public void goToEndPot() {
        goSave2Page.goToEndPot();
    }


    @And("Click Join Now Button")
    public void clickJoinNowButton(Map<String, String> data) {
        goSave2Page.clickJoinNowButton(data.get("screenShotName"));
    }

    @Then("I Enter Deposit amount")
    public void iEnterDepositAmount(Map<String, String> data) {
        assertThat(goSave2Page.enterDepositAmount(data.get("amount"),data.get("screenShotName"))).isTrue();
    }

    @And("I Click Confirm and accept Terms and Conditions")
    public void iClickConfirmAndAcceptTermsAndConditions(Map<String, String> data) {
        goSave2Page.clickConfirmAndAccept(data.get("screenShotName"));
    }

    @Then("I verify Success joined")
    public void iVerifySuccessJoined(Map<String, String> data) {
        assertThat(goSave2Page.verifySuccessJoined(data.get("screenShotName"))).isTrue();
    }


    @And("Go to My deposit Page")
    public void goToMyDepositPage(Map<String, String> data) {
        goSave2Page.goToMyDepositPage(data.get("screenShotName"));
    }

    @Then("I verify myDeposit detail")
    public void iVerifyMyDepositDetail(Map<String, String> data) {
        assertThat(goSave2Page.verifyMyDepositDetail(data.get("screenShotName"))).isTrue();
    }

    @Then("I back to home page verify myDeposit Record")
    public void iBackToHomePageVerifyMyDepositRecord(Map<String, String> data) {
        assertThat(goSave2Page.verifyMyDepositRecord(data.get("screenShotName"))).isTrue();
    }

    @And("I Enter Promotion Code")
    public void iEnterPromotionCode() {
        goSave2Page.enterPromotionCode();
    }

    @Then("Click Early Withdrawal")
    public void clickEarlyWithdrawal(Map<String, String> data) {
        goSave2Page.clickEarlyWithdrawal(data.get("screenShotName"),data.get("screenShotName01"));
    }

    @And("Click submit")
    public void clickSubmit(Map<String, String> data) {
        goSave2Page.clickSubmit(data.get("screenShotName"));
    }

    @And("Click goSave Done")
    public void clickDone(Map<String, String> data) {
        goSave2Page.clickDone(data.get("screenShotName"));
    }

    @Then("I verify early withDrawl record")
    public void iVerifyEarlyWithDrawlRecord(Map<String, String> data) {
        goSave2Page.verifyEarlyWithDrawlRecord(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @Then("Click Ended button")
    public void clickEndedButton(Map<String, String> data) {
        goSave2Page.clickEndedButton(data.get("screenShotName"));
    }

    @Then("I verify join time deposit fail Page")
    public void iVerifyJoinTimeDepositFailPage(Map<String, String> data) {
        assertThat(goSave2Page.joinTimeDepositFail(data.get("screenShotName"))).isTrue();
    }

    @And("the deposit amount exceeds the available balance IOS")
    public void theDepositamountExceedsTheAvailableBalanceIOS(Map<String, String> data) {
        goSave2Page.theDepositamountExceedsTheAvailableBalanceIOS(data.get("amount"), data.get("screenShotName"));
    }
    @And("the deposit amount exceeds the maximum deposit amount IOS")
    public void theDepositAmountExceedsTheMaximumDepositAmountIOS(Map<String, String> data) {
        goSave2Page.theDepositamountExceedsTheAvailableBalanceIOS(data.get("amount"), data.get("screenShotName"));
    }

    @And("the deposit amount below the minimum deposit amount IOS")
    public void theDepositAmountBelowTheMinimumDepositAmountIOS(Map<String, String> data) {
        goSave2Page.theDepositamountExceedsTheAvailableBalanceIOS(data.get("amount"), data.get("screenShotName"));
    }

    @And("join time deposit fail with posting restriction account IOS")
    public void joinTimeDepositFailWithPostingRestrictionAccount(Map<String, String> data) {
        goSave2Page.joinTimeDepositFailWithPostingRestrictionAccount(data.get("screenShotName"), data.get("screenShotName1"));
    }



    @And("check my deposit page and detail after join deposit successful IOS")
    public void checkMyDepositPageAndDetailAfterJoinDepositSuccessfulIOS(Map<String, String> data) {
        goSave2Page.checkMyDepositPageAndDetailAfterJoinDepositSuccessfulIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("check in progress page and detail after join deposit successful IOS")
    public void checkInProgressPageAndDetailAfterJoinDepositSuccessfulIOS(Map<String, String> data) {
        goSave2Page.checkInProgressPageAndDetailAfterJoinDepositSuccessfulIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("check the Maturity deposit IOS")
    public void checkTheMaturityDepositIOS(Map<String, String> data) {
        goSave2Page.checkTheMaturityDepositIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("I verify Total balance Android")
    public void iVerifyTotalBalanceAndroid(Map<String, String> data) {
        assertThat(goSave2Page.verifyTotalBalanceAndroid(data.get("screenShotName"))).isTrue();
    }

    @Then("Check the deposit amount exceeds the available balance")
    public void checkTheDepositAmountExceedsTheAvailableBalance(Map<String, String> data) {
        assertThat(goSave2Page.checkDepositAmountExceedsAvailableBalance(data.get("amount"), data.get("screenShotName"))).isTrue();
    }

    @Then("Check the deposit amount exceeds the maximum deposit amount")
    public void checkTheDepositAmountExceedsTheMaximumDepositAmount(Map<String, String> data) {
        assertThat(goSave2Page.checkDepositAmountExceedsMaximumDepositAmount(data.get("amount"), data.get("screenShotName"))).isTrue();
    }

    @And("Check the deposit amount below the minimum deposit amount")
    public void theDepositAmountBelowTheMinimumDepositAmount(Map<String, String> data) {
        assertThat(goSave2Page.checkDepositAmountBelowAvailableBalance(data.get("amount"), data.get("screenShotName"))).isTrue();
    }

    @Then("Check my deposit page and detail")
    public void checkMyDepositPageAndDetail(Map<String, String> data) {
        goSave2Page.checkMyDepositPageAndDetail(data.get("screenShotName"));
    }

    @Then("Check in progress page and detail")
    public void checkInProgressPageAndDetail(Map<String, String> data) {
        goSave2Page.checkInProgressPageAndDetail(data.get("screenShotName"), data.get("screenShotName01"));
    }


    @And("Check the Maturity deposit")
    public void checkTheMaturityDeposit(Map<String, String> data) {
        goSave2Page.checkTheMaturityDeposit(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @Then("check My Deposit Detail IOS")
    public void checkMyDepositDetailIOS(Map<String, String> data) {
        goSave2Page.checkMyDepositDetailIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }
}
