package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.cucumber.java.en.And;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.Synchronized;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class GoSave2Page extends AppiumBasePage {
  private double theAccountAvailableBalanceBeforeJoinPot=0.0;
  private double myGoSaveAmountBeforeJoinPot=0.0;
  private double totalBalanceAfterJoin=0.0;
  private double goSaveAmount =0.0;
  private double TotalBalance = 0.0;
  private double AvailableBalance = 0.0;
  private double goSaveTotalBalance = 0.0;
  private double myTotalAfterJoinPot=0.0;
  private String pageName = "GoSave Page";
  private String tranAmount;

  CommonPage commonPage;
  GoSavePage goSavePage;
  public GoSave2Page() {
    super.pageName = this.pageName;
    commonPage = new CommonPage();
    goSavePage = new GoSavePage();
  }

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "Home, tab, 1 of 4"),
          @AndroidBy(accessibility = "Home, tab, 1 of 5"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='主頁']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'Home, tab, 1 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"主页, tab, 1 of 5\""),
  })
  private MobileElement homeTab;

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "GoSave, tab, 2 of 4"),
          @AndroidBy(accessibility = "GoSave, tab, 2 of 5")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 4'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"定存, tab, 2 of 5\""),
          @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemGroupSavingStack, tab, 2 of 5'")
  })
  private MobileElement goSaveTab;

  @iOSXCUITFindBy(xpath = "//*[@text=\"GoSave 2.0 定期存款\"]")
  private MobileElement myGoSavePageTitle;

  @iOSXCUITFindBy(xpath = "//*[@text=\"我的定期\"]")
  private MobileElement MyGosaveButton;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"完成\"]")
  private MobileElement keyFinishButton;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Join']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='加入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Join'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '加入'")
  })
  private MobileElement JoinBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Deposit more']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='存入更多']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"存入更多\"])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Deposit more\"])[1]")
  })
  private MobileElement DepositMoreBtn;


  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提前取款\"])[2]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Withdraw\"])[2]")
  })
  private MobileElement withdrawalMoney;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"提取\"]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Withdraw\"])[5]")
  })
  private MobileElement extractMoney;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'In progress')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'進行中')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"進行中\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"In progress\"]")
  })
  private MobileElement goSaveInprogressTab;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Available balance')]"),
  })
  private MobileElement balanceOnDepositPage;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Maximum deposit is')]"),
  })
  private MobileElement maximumDepositTip;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Minimum deposit')]"),
  })
  private MobileElement minimumDepositTip;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='My deposit']"),
  })
  private MobileElement myDepositBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Deposit amount(HKD)']/following-sibling::android.widget.EditText[1]")
  })
  private MobileElement depositAmountInput;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Next']")
  })
  private MobileElement nextBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Confirm and accept Terms and Conditions']")
  })
  private MobileElement confirmAndAccept;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Deposit amount']/following-sibling::android.widget.TextView")
  })
  private MobileElement amountOnConfirmationPage;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Successfully joined']")
  })
  private MobileElement successJoined;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Successfully joined']/following-sibling::android.widget.TextView[1]")
  })
  private MobileElement successJoinedAmount;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Done']")
  })
  private MobileElement doneBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='View more GoSave history']")
  })
  private MobileElement moreHistory;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='My deposit details']")
  })
  private MobileElement myDepositDetails;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='GoSave 2.0']/preceding-sibling::android.widget.TextView[2]")
  })
  private MobileElement depositAmountOnTranDetail;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[2]")
  })
  private MobileElement traderNameAtHome;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Transaction detail']/../android.view.ViewGroup"),
          @AndroidBy(xpath = "//*[@text='交易詳情']/../android.view.ViewGroup")
  })
  private MobileElement backBtnInRecordPage;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@content-desc='Home, tab, 1 of 5']")
  })
  private MobileElement homeBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Apply']")
  })
  private MobileElement applyBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Ok!']")
  })
  private MobileElement OKBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.View[@text='Promotion code (optional)']/preceding-sibling::android.widget.EditText[1]")
  })
  private MobileElement promotionCodeInput;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.View/android.widget.TabWidget/android.view.View[2]")
  })
  private MobileElement endedBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.View/android.widget.TabWidget/android.view.View[1]")
  })
  private MobileElement inProgressBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Early Withdrawal']")
  })
  private MobileElement earlyWithdrawalBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Confrim withdrawal']")
  })
  private MobileElement withdrawalConfirmBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Submit']")
  })
  private MobileElement submitBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='fail to create deposit from T24']")
  })
  private MobileElement depositFailTip;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Back to GoSave']")
  })
  private MobileElement backToGoSaveBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Insufficent funds']")
  })
  private MobileElement insufficientFunds;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[3]"),
          @AndroidBy(xpath = "//*[@text='今日']/../../android.view.ViewGroup[3]")
  })
  private MobileElement secondTraderNameAtHome;

  @SneakyThrows
  public void clickBackButton(){
    if(isIphoneSE()){
      clickByLocationByPercent(8, 9);
    }else if(isIphone7Plus()){
      clickByLocationByPercent(6, 6);
    }else{
      clickByLocationByPercent(8, 9);
    }
    Thread.sleep(3000);
  }

  @SneakyThrows
  public void goToMainPage(){
    clickElement(homeTab);
    Thread.sleep(3000);
  }

  public double getTotalBalance() {
    double amount = goSavePage.getAccountAvailableBalance();
    return amount;
  }

  public void getTotalBalanceBeforeJoinPot(String screenShotName){
    theAccountAvailableBalanceBeforeJoinPot=getTotalBalance();
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void verifyTotalBalance(String screenShotName){
    Thread.sleep(3000);
    try {
      totalBalanceAfterJoin=getTotalBalance();
    }catch (Exception e){

    }
    takeScreenshot(screenShotName);
    double t = totalBalanceAfterJoin-theAccountAvailableBalanceBeforeJoinPot;
  }

  @SneakyThrows
  public void openGoSave2Page(String screenShotName, String screenShotName01) {
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
    waitUntilElementVisible(goSaveTab);
    goSaveTab.click();
    Thread.sleep(8000);
    takeScreenshot(screenShotName01);
  }

  @SneakyThrows
  public void openMyGoSave2Page(String screenShotName) {
    clickMyGosave();
    Thread.sleep(6000);
    takeScreenshot(screenShotName);
  }

  public void scrollToBottom(){
    for (int i = 0; i < 5; i++) {
      scrollUpFast();
    }
  }


  @SneakyThrows
  public void inputGoSaveAmountAndCheckInterestRate(String amount,String screenShotName) {
    clickInputGosaveAmount(amount);
    Thread.sleep(1000);
    takeScreenshot(screenShotName);
    clickFinishAndNext();
  }

  public void inputGoSaveAmount(String amount) {
    clickInputGosaveAmount(amount);
    clickFinishAndNext();
  }

  public void clickInputGosaveAmount(String amount){
    clickGoSaveAmountInput();
    commonPage.inputMSKByString(amount);
  }

  @SneakyThrows
  public void clickFinishAndNext(){
    clickElement(keyFinishButton);
    Thread.sleep(2000);
    clickNextButton();
    Thread.sleep(5000);
  }

  @SneakyThrows
  public void theDepositamountExceedsTheAvailableBalanceIOS(String amount,String screenShotName) {
    clickInputGosaveAmount(amount);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    Thread.sleep(3000);
  }

  public void clickMyGosave(){
    clickByLocationByPercent(88,6);
  }

  @SneakyThrows
  public void clickGoToGoSave1(String screenShotName) {
    clickByLocationByPercent(50,85);
    Thread.sleep(6000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void clickGoSavePot() {
    scrollUp();
    Thread.sleep(1000);
    clickDepositNowButton();
    Thread.sleep(6000);
  }

  public void clickDepositNowButton(){
    if(isIphoneXR()){
      clickByPicture2("src/main/resources/images/channel/gosave/DepositNowButton.png",50,25);
    }else{
      clickByPicture2("src/main/resources/images/channel/gosave/addNewGosave.png",50,50);
    }
  }

  public void clickGoSavePotCenter() {
    clickByLocationByPercent(50,50);
  }

  @SneakyThrows
  public void clickMyGoSaveDetail(){
    clickByLocationByPercent(50, 26);
    Thread.sleep(5000);
  }

  public void clickGoSaveAmountInput() {
    if(isIphoneSE()){
      clickByPicture2("src/main/resources/images/channel/gosave/goSaveInputAmount.png",50,10);
    }else if(isIphone13()){
      clickByPicture2("src/main/resources/images/channel/gosave/goSaveInputAmount2.png",50,40);
    }else{
      clickByPicture2("src/main/resources/images/channel/gosave/goSaveInputAmount2.png",50,40);
    }
  }
  @SneakyThrows
  public void clickNextButton() {
    //clickByLocationByPercent(50,67);
    clickByPicture("src/main/resources/images/channel/nextButton1.png",50,60);
  }

  public void clickNextButtonForInputAmount(){
    clickByLocationByPercent(50,61);
  }

  public void clickFinishButtonIphoneSE(){
    clickByLocationByPercent(50,84);
  }

  public void clickFinishButtonForGosave(){
    clickByLocationByPercent(50,92);
  }

  public void clickConfirmGasaveButton(){
    clickByLocationByPercent(50,92);
  }

  public void clickConfirmGosave(){
    clickByPicture2("src/main/resources/images/channel/gosave/confirmGoSave.png",50,15);
  }

  public void clickInputForPromotionIphoneSE(){
    clickByLocationByPercent(50, 68);
  }

  public void clickUseButtonForPromotionIphoneSE(){
    clickByLocationByPercent2(86, 34.5);
  }


  public void goToEndPot(){
    if(isIphoneSE()){
      clickByLocationByPercent(73,24);
    }else if(isIphone7Plus()){
      clickByLocationByPercent(73,17);
    }else{
      clickByLocationByPercent(73,24);
    }
  }
  public void clickFisrtEndPot(){
    if(isIphoneSE()){
      clickByLocationByPercent(50,38);
    }else if(isIphone7Plus()){
      clickByLocationByPercent(50,35);
    }else{
      clickByLocationByPercent(50,38);
    }
  }

  public void clickWithdrawalAmountButton(){
    clickByLocationByPercent(50,83);
  }
  public void clickConfirmWithdrawalButton(){
    clickByLocationByPercent(50,66);
  }
  public void clickSubmitButton(){
    clickByLocationByPercent(50,68);
  }

  @SneakyThrows
  public void confirmGosaveWithPromotionCode(String screenShotName,String screenShotName1){
    scrollDown();
    Thread.sleep(1000);
    takeScreenshot(screenShotName);
    inputPromotionCode(screenShotName1);
    clickConfirmButtonAndWait();
  }


  @SneakyThrows
  public void confirmGosave(String screenShotName) {
    scrollUp();
    Thread.sleep(1000);
    clickConfirmGosave();
    Thread.sleep(12*1000);
    takeScreenshot(screenShotName);
    clickConfirmButtonAndWait();
  }

  @SneakyThrows
  public void clickConfirmButtonAndWait(){
    scrollUp();
    Thread.sleep(1000);
    clickFinishButtonIphoneSE();
    Thread.sleep(8000);
  }

  @SneakyThrows
  public void joinTimeDepositFailWithPostingRestrictionAccount(String screenShotName,String screenShotName1){
    scrollUp();
    Thread.sleep(1000);
    takeScreenshot(screenShotName);
    clickConfirmGosave();
    Thread.sleep(12*1000);
    takeScreenshot(screenShotName1);
  }

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"numbers\"")
  private MobileElement switchToNumbers;

  @SneakyThrows
  public void inputPromotionCode(String screenShotName){
    clickInputForPromotionIphoneSE();
    commonPage.clickSoftKeyByNumberString("A");
    clickElement(switchToNumbers);
    commonPage.clickSoftKeyByNumberString("123");
    clickUseButtonForPromotionIphoneSE();
    clickElement(keyFinishButton);
    scrollDown();
    Thread.sleep(1000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void checkInProgressPageAndDetailAfterJoinDepositSuccessfulIOS(String screenShotName, String screenShotName1){
    viewStartedPot(screenShotName, screenShotName1);
  }

  @SneakyThrows
  public void checkMyDepositPageAndDetailAfterJoinDepositSuccessfulIOS(String screenShotName, String screenShotName1){
    viewStartedPot(screenShotName, screenShotName1);
  }

  @SneakyThrows
  public void checkTheMaturityDepositIOS(String screenShotName, String screenShotName1){
    goToEndPot();
    viewEndPot(screenShotName, screenShotName1);
  }

  @SneakyThrows
  public void clickAddPotButton(){
    Thread.sleep(1000);
    clickElement(JoinBtn);
    Thread.sleep(2000);
  }
  @SneakyThrows
  public void clickJoinMoreButton(){
    Thread.sleep(1000);
    clickElement(DepositMoreBtn);
    Thread.sleep(2000);
  }

  public double strToDouble(String data){
    if(data.contains(",")) {
      data = data.replace(",", "");
    }
    return Double.parseDouble(data);
  }
  @SneakyThrows
  public void inputAmountForGoSave(String amount,String screenShotName) {
    goSaveAmount=strToDouble(amount);
    Thread.sleep(3000);
    commonPage.inputMSKByString(amount);
    clickNextButtonForInputAmount();
    Thread.sleep(3000);
    clickConfirmGasaveButton();
    Thread.sleep(8000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void verifyGosave(String screenShotName){
    clickFinishButtonForGosave();
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void withdrawalMoneyBeforePotStart(String amount,String screenShotName) {
    Thread.sleep(3000);
    scrollUp();
    scrollUp();
    Thread.sleep(1000);
    clickElement(withdrawalMoney);
    goSaveAmount = 0 - strToDouble(amount);
    waitUntilElementClickable(extractMoney);
    clickElement(extractMoney);
    Thread.sleep(3000);
    commonPage.inputMSKByString(amount);
    clickNextButtonForInputAmount();
    Thread.sleep(6000);
    takeScreenshot(screenShotName);
    clickConfirmGasaveButton();
    Thread.sleep(8000);
  }

  @SneakyThrows
  public void checkGoSaveInprogress(String screenShotName,String screenShotName1){
    clickElement(goSaveInprogressTab);
    Thread.sleep(5000);
    clickGoSavePotCenter();
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
    scrollUp();
    Thread.sleep(1000);
    takeScreenshot(screenShotName1);
  }

  @SneakyThrows
  public void withdrawalMoneyAfterPotStart(String amount){
    goSavePage.WithdrawalMoneyAfterPotStart(amount);
  }

  @SneakyThrows
  public void viewStartedPot(String screenShotName,String screenShotName1){
    clickGoSavePotCenter();
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
    scrollUp();
    Thread.sleep(1000);
    takeScreenshot(screenShotName1);
  }

  @SneakyThrows
  public void viewEndPot(String screenShotName,String screenShotName1){
    Thread.sleep(3000);
    clickFisrtEndPot();
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
    scrollUp();
    Thread.sleep(1000);
    takeScreenshot(screenShotName1);
  }


  @SneakyThrows
  public void withdrawalAfterPotStart(String screenShotName,String screenShotName1){
    clickGoSavePotCenter();
    Thread.sleep(5000);
    scrollUp();
    scrollUp();
    Thread.sleep(1000);
    takeScreenshot(screenShotName);
    clickWithdrawalAmountButton();
    Thread.sleep(1000);
    clickConfirmWithdrawalButton();
    Thread.sleep(10*1000);
    clickSubmitButton();
    Thread.sleep(5*1000);
    takeScreenshot(screenShotName1);
    clickFinishButtonIphoneSE();
    Thread.sleep(5*1000);
  }


  @SneakyThrows
  public void clickJoinNowButton(String screenShotName) {
    Thread.sleep(3000);
    // first Join now Button
    if(isSamsungA5460()){
      clickByLocation(800, 1200);
    }else{
      clickByLocation(800, 1100);
    }
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public boolean enterDepositAmount(String amount, String screenShotName) {
    Thread.sleep(3000);
    clickElement(depositAmountInput);
    Thread.sleep(3000);
    clearAndSendKeys(depositAmountInput, amount);
    tranAmount = amount;
    Thread.sleep(3000);
    clickElement(balanceOnDepositPage);
    takeScreenshot(screenShotName);
    clickElement(nextBtn);
    Thread.sleep(5000);
    waitUntilElementVisible(amountOnConfirmationPage,10);
    return amountOnConfirmationPage.getText().contains(tranAmount);
  }

  @SneakyThrows
  public void clickConfirmAndAccept(String screenShotName) {
    Thread.sleep(5 * 1000);
    scrollUp();
    waitUntilElementClickable(confirmAndAccept, 3);
    takeScreenshot(screenShotName);
    clickElement(confirmAndAccept);
  }

  @SneakyThrows
  public boolean verifySuccessJoined(String screenShotName) {
    Thread.sleep(5000);
    waitUntilElementVisible(successJoined, 10);
    takeScreenshot(screenShotName);
    if (verifyElementExist(successJoined) && successJoinedAmount.getText().contains(tranAmount)) {
      clickElement(doneBtn);
      return true;
    }
    return false;
  }

  public void goToMyDepositPage(String screenShotName) {
    waitUntilElementClickable(myDepositBtn);
    clickElement(myDepositBtn);
    waitUntilElementVisible(endedBtn);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public boolean verifyMyDepositDetail(String screenShotName) {
    //  scrollUpFast();
    //  scrollUpEntireToFindElement(moreHistory, 20, 1);
    Thread.sleep(3000);
    //click Penultimate
    clickByLocation(800, 1000);
    takeScreenshot(screenShotName);
    //  return amountOnConfirmationPage.getText().contains(tranAmount);

    //  just check myDepositDetails,No Assert tranAmount
    return verifyElementExist(myDepositDetails);
  }

  public boolean verifyMyDepositRecord(String screenShotName) {
    scrollUp();
    waitUntilElementVisible(traderNameAtHome);
    clickElement(traderNameAtHome);
    waitUntilElementVisible(depositAmountOnTranDetail, 10);
    takeScreenshot(screenShotName);
    return depositAmountOnTranDetail.getText().contains(tranAmount);
  }

  @SneakyThrows
  public void enterPromotionCode() {
    Thread.sleep(3000);
    if(isSamsungA5460()){
      // click promotion code input
      clickByLocation(500,1350);
    }else{
      clickByLocation(500,1200);
    }
    sendCode("A123");
    Thread.sleep(3000);

    // click apply Button
    clickByLocation(935,680);
    Thread.sleep(3000);
    // click other place to input enter
    clickByLocation(60, 820);
    if (isShow(OKBtn, 2)) {
      clickElement(OKBtn);
    }
  }

  @SneakyThrows
  public void clickEarlyWithdrawal(String screenShotName, String screenShotName01) {
    Thread.sleep(5000);
    //click first Deposit
    clickByLocation(900, 660);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
    tranAmount = amountOnConfirmationPage.getText();
    scrollUp();
    clickElement(earlyWithdrawalBtn);
    waitUntilElementVisible(withdrawalConfirmBtn);
    takeScreenshot(screenShotName01);
    Thread.sleep(2000);
    clickElement(withdrawalConfirmBtn);
  }

  public void clickSubmit(String screenShotName) {
    waitUntilElementClickable(submitBtn,5);
    takeScreenshot(screenShotName);
    clickElement(submitBtn);
  }

  public void clickDone(String screenShotName) {
    waitUntilElementVisible(doneBtn);
    takeScreenshot(screenShotName);
    clickElement(doneBtn);
  }

  @SneakyThrows
  public void verifyEarlyWithDrawlRecord(String screenShotName, String screenShotName01) {
    scrollUp();
    waitUntilElementVisible(traderNameAtHome);
    takeScreenshot(screenShotName);
    clickElement(secondTraderNameAtHome);
    waitUntilElementVisible(depositAmountOnTranDetail);
    takeScreenshot(screenShotName01);
  }

  @SneakyThrows
  public void clickEndedButton(String screenShotName) {
    clickElement(endedBtn);
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public boolean joinTimeDepositFail(String screenShotName) {
    Thread.sleep(5000);
    waitUntilElementVisible(depositFailTip);
    takeScreenshot(screenShotName);
    return verifyElementExist(depositFailTip) && verifyElementExist(backToGoSaveBtn);
  }

  public boolean verifyTotalBalanceAndroid(String screenShotName) {
    waitUntilElementVisible(traderNameAtHome);
    scrollDown();
    takeScreenshot(screenShotName);
    return theAccountAvailableBalanceBeforeJoinPot == getTotalBalance() + goSavePage.strToDouble(tranAmount);
  }

  @SneakyThrows
  public boolean checkDepositAmountExceedsAvailableBalance(String amount, String screenShotName) {
    clickElement(depositAmountInput);
    Thread.sleep(3000);
    clearAndSendKeys(depositAmountInput, amount);
    clickElement(balanceOnDepositPage);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    return verifyElementExist(insufficientFunds);
  }

  @SneakyThrows
  public boolean checkDepositAmountExceedsMaximumDepositAmount(String amount, String screenShotName) {
    clickElement(depositAmountInput);
    Thread.sleep(3000);
    clearAndSendKeys(depositAmountInput, amount);
    clickElement(balanceOnDepositPage);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    return verifyElementExist(maximumDepositTip);
  }

  @SneakyThrows
  public boolean checkDepositAmountBelowAvailableBalance(String amount, String screenShotName) {
    clickElement(depositAmountInput);
    Thread.sleep(3000);
    clearAndSendKeys(depositAmountInput, amount);
    clickElement(balanceOnDepositPage);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    return verifyElementExist(minimumDepositTip);
  }


  public void checkMyDepositPageAndDetail(String screenShotName) {
    waitUntilElementVisible(myDepositBtn);
    takeScreenshot(screenShotName);
  }

  public void checkInProgressPageAndDetail(String screenShotName, String screenShotName01) {
    waitUntilElementVisible(endedBtn);
    clickByLocation(800, 1000);
    waitUntilElementVisible(myDepositDetails);
    takeScreenshot(screenShotName);
    scrollUp();
    takeScreenshot(screenShotName01);
  }

  @SneakyThrows
  public void checkTheMaturityDeposit(String screenShotName, String screenShotName01) {
    //  click first Record
    clickByLocation(800, 1800);
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
    scrollUp();
    Thread.sleep(2000);
    takeScreenshot(screenShotName01);
  }

  @SneakyThrows
  public void checkMyDepositDetailIOS(String screenShotName, String screenShotName01) {
    clickByLocationByPercent(50, 50);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
    scrollUp();
    Thread.sleep(2000);
    takeScreenshot(screenShotName01);
  }

}