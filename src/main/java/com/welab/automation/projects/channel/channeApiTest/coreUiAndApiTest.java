package com.welab.automation.projects.channel.channeApiTest;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.api.SendRequest;
import io.restassured.response.Response;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Lenovo
 * @title: channelUiAndApi
 * @projectName Au_list
 * @description: TODO
 * @date 2022/4/711:506169
 */
public class coreUiAndApiTest {
    Logger logger = LoggerFactory.getLogger("channelUiAndApi");
    private static Response response;

    @SneakyThrows
    public static void main(String[] args) {
        List<String> valueExpression = coreUiAndApiTest.gridFinder("qatest101", "Aa123456","7868");
//        List<String> valueExpression = coreUiAndApiTest.gridFinder("tonymcv022301", "Aa123456","3283");
//        List<String> valueExpression = coreUiAndApiTest.gridFinder("tester052", "Aa123456","6169");
//        List<String> valueExpression = coreUiAndApiTest.gridFinder("stb10517", "Aa123321","0051");
        //List<String> valueExpression = coreUiAndApiTest.gridFinder("tester200", "Aa123456","9878");
        //List<String> valueExpression = coreUiAndApiTest.gridFinder("tester235", "Aa123456","1038");
        System.out.println(valueExpression.get(0));
    }
    public static List<String> gridFinder(String name,String password ,String maskedPan) throws Exception {
        String path = "/v1/bank-accounts/core/cards";
        Map<String, String> prepareData = new HashMap<String, String>();
        prepareData.put("TEST_ACCOUNT_SPECIFIED",name);
        prepareData.put("TEST_PASSWORD_SPECIFIED",password);
        String params="";
        List<String> result=yearOfBirthAge(path,params,prepareData,maskedPan);
        return result;
    }
    public static List<String> yearOfBirthAge(String path,String params,Map<String, String> prepareData,String maskedPan) throws Exception {
        if (!prepareData.isEmpty()){
            for(String key : prepareData.keySet()){
                GlobalVar.GLOBAL_VARIABLES.put(key,prepareData.get(key));
            }
        }
        File file = new File("./src/main/resources/api_channel.properties");
        if (!file.exists()) file.createNewFile();
        Properties proterties = new Properties();
        proterties.load(new FileInputStream(file));
        GlobalVar.GLOBAL_VARIABLES.put("host", (String) proterties.get("host"));
        GlobalVar.HEADERS.put("Content-Type",(String) proterties.get("Content-Type"));
        GlobalVar.HEADERS.put("Client-Id",(String) proterties.get("CLIENT_ID"));
        GlobalVar.HEADERS.put("Client-Secret",(String) proterties.get("CLIENT_SECRET"));
        GlobalVar.HEADERS.put("Accept-Encoding",(String) proterties.get("Accept-Encoding"));
        GlobalVar.HEADERS.put("Accept-Language",(String) proterties.get("Accept-Language"));
        SendRequest SendRequest = new SendRequest();
        Map<String, String> Map = new HashMap<String, String>();
        response = SendRequest.sendRequest(path, "GET", "", params, "");
        List<String> listMaker = new ArrayList<>();
        List<Map<String, Object>> data = response.jsonPath().getList("data");
        for (int i=0;i<data.size();i++){
            String str= String.valueOf(data.get(i).get("publicToken"));
            if (str!="") {
                String masked= String.valueOf(data.get(i).get("maskedPan"));
                if (masked.contains(maskedPan)) {
                    listMaker.add(data.get(i).get("publicToken").toString());
                }
            }
        }
        return listMaker;
    }


}
