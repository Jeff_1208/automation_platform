Feature: Payment

  Background: Login
    Given Open Stage WeLab App
#    And Enable skip Root Stage
    When Login with user and password from properties
#    Then I can see the LoggedIn page

  @ACBN-T640 @VCAT-43  @health
  Scenario: Reg_Payment_001 Outbound FPS transfer(welab bank) with notification
    And I verify Available balance
      | screenShotName | Reg_Payment_001_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by welab bank
      |accountNumber|1000476928|
      |holdname     |WILSON,Wil|
      |money        |10        |
      | screenShotName | Reg_Payment_001_Android_02 |
    And I verify send money detail by bank transfer
      | screenShotName | Reg_Payment_001_Android_03 |
    And I verify success send money record
      | screenShotName | Reg_Payment_001_Android_04 |
    And I verify Available balance after send money
      | screenShotName | Reg_Payment_001_Android_05 |


  @ACBN-T642 @VCAT-44
  Scenario: Reg_Payment_002 Outbound FPS transfer(other bank) with notification
    And I verify Available balance
      | screenShotName  | Reg_Payment_002_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by bank672
      |accountNumber|1000476928|
      |holdname     |WILSON,Wil|
      |money        |20       |
      | screenShotName  | Reg_Payment_002_Android_02 |
    And I verify send money detail by bank transfer
      | screenShotName  | Reg_Payment_002_Android_03 |
    And I verify success send money record
      | screenShotName  | Reg_Payment_002_Android_04 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_002_Android_05 |


  @ACBN-T644 @VCAT-45
  Scenario Outline: Reg_Payment_008 Non-payee, pay max limit 300k HKD.
    And I verify Available balance
      | screenShotName | Reg_Payment_008_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I verify no payee <FPSID>
    And I transfer money to <FPSID>
    And I enter <amount>
    Then I verify send money detail by Fpsid
      | screenShotName | Reg_Payment_008_Android_02 |
    Then I verify FPSID transactions
      | screenShotName  | Reg_Payment_008_Android_03 |
      | screenShotName1 | Reg_Payment_008_Android_04 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_008_Android_05 |
    Examples:
      | FPSID    | amount |
      | 88880194 | 100000 |
#      | 88080101 | 100000 |

  @ACBN-T646 @VCAT-46
  Scenario Outline: Reg_Payment_009 Register a new Payee, do a large amount transfer max limit 500k HKD to new payee account 1) send 10dollars and add payee. 2) send 299990 to the same payee
    And I verify Available balance
      | screenShotName | Reg_Payment_009_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I verify no payee <FPSID>
    And I transfer money to <FPSID>
    Then I can see Non-payee page and add payee
      | screenShotName | Reg_Payment_009_Android_02 |
    And I enter <amount>
    Then I verify send money detail by Fpsid
      | screenShotName | Reg_Payment_009_Android_03 |
    Then I verify FPSID transactions
      | screenShotName  | Reg_Payment_009_Android_04 |
      | screenShotName1 | Reg_Payment_009_Android_05 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_009_Android_06 |
    And I verify Available balance
      | screenShotName | Reg_Payment_009_Android_07 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to my payee
      | screenShotName | Reg_Payment_009_Android_08 |
    And I enter money <amount2>
    Then I verify send money detail by Fpsid
      | screenShotName | Reg_Payment_009_Android_09 |
    Then I verify FPSID transactions
      | screenShotName  | Reg_Payment_009_Android_10 |
      | screenShotName1 | Reg_Payment_009_Android_11 |
    And I verify Available balance after send money
      | screenShotName  | Reg_Payment_009_Android_12 |
    Examples:
      | FPSID    | amount | amount2 |
      | 88880194 | 10     | 100000  |

  @ACBN-T650 @VCAT-50
  Scenario: Reg_Payment_0014 Outbound FPS transfer incorrect payee name
    And I verify Available balance
      | screenShotName  | Reg_Payment_0014_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I send money by welab bank
      |accountNumber|1000476928|
      |holdname     |WILSON    |
      |money        |100        |
      | screenShotName   | Reg_Payment_0014_Android_02 |
    And I verify send money fail detail by bank transfer
      | screenShotName   | Reg_Payment_0014_Android_03 |
    And I verify fail send money record
      | screenShotName   | Reg_Payment_0014_Android_04 |
      | screenShotName1  | Reg_Payment_0014_Android_05 |
    And I verify fail Available balance after send money
      | screenShotName   | Reg_Payment_0014_Android_06 |



#  @ACBN-T654 @VCAT-52
#  Scenario Outline: Reg_Payment_0017 bill payment transaction
#    And I verify Available balance
#      | screenShotName | Reg_Payment_0017_Android_01 |
#    And I move eyeBtn to top right corner
#    And I goto Transfer page
#    And I click Send Money at Transfer page
#    And I transfer money to <FPSID>
#    Then I can see merchant Information
#      | screenShotName | Reg_Payment_0017_Android_02 |
#    And I enter <billnum> and <amount>
#    Then I can see transfer in progress
#      | screenShotName | Reg_Payment_0017_Android_03 |
#    And I verify Available balance after send money
#      | screenShotName  | Reg_Payment_007_Android_04 |
#    Then I verify billnum transactions
#      | screenShotName  | Reg_Payment_0017_Android_05 |
#      | screenShotName1 | Reg_Payment_0017_Android_06 |
#    Examples:
#      | FPSID     | billnum                   | amount |
#      | 166924688 | DBSBillnumbertestingTerry | 1      |

  @ACBN-T629
  Scenario Outline: Reg_Payment_0018 Transfer Money via Email Address
    And I verify Available balance
      | screenShotName | Reg_Payment_0018_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to <FPSID>
    And I enter <amount>
    Then I verify send money detail by Fpsid
      | screenShotName | Reg_Payment_0018_Android_02 |
    Then I verify FPSID transactions
      | screenShotName  | Reg_Payment_0018_Android_03 |
      | screenShotName1 | Reg_Payment_0018_Android_04 |
    And I verify Available balance after send money
      | screenShotName | Reg_Payment_0018_Android_05  |

    Examples:
      | FPSID                   | amount |
      | v.tony.liang@welab.bank | 200    |
#      | ghhh@qq.com | 200    |

  @ACBN-T630
  Scenario Outline: Reg_Payment_0019 Transfer Money via FPS Proxy ID
    And I verify Available balance
      | screenShotName | Reg_Payment_0019_Android_01 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    And I transfer money to <FPSID>
    And I enter <amount>
    Then I verify send money detail by Fpsid
      | screenShotName | Reg_Payment_0019_Android_02 |
    Then I verify FPSID transactions
      | screenShotName  | Reg_Payment_0019_Android_03 |
      | screenShotName1 | Reg_Payment_0019_Android_04 |
    And I verify Available balance after send money
      | screenShotName | Reg_Payment_0019_Android_05  |

    Examples:
      | FPSID     | amount |
      | 167649193 | 300    |
#      | 799687421 | 300    |

  @ACBN-T631 @VCAT-66 @health
  Scenario: Reg_Payment_0010 Add Money to Welab Account via eDDI
    And I verify Available balance
      | screenShotName  | Reg_Payment_0010_Android_01 |
    And I move eyeBtn to top right corner
    And I goto Transfer page
    And I click add money Button at Transfer page
      | screenShotName  | Reg_Payment_0010_Android_02 |
    Then I enter money at Add money page
      | money   | 500 |
      | screenShotName  | Reg_Payment_0010_Android_03 |
    And I verify add money success
      | screenShotName  | Reg_Payment_0010_Android_04 |
    Then go to home page
    And I verify add Money Record
      | screenShotName  | Reg_Payment_0010_Android_05 |
    And I verify add Money Balance
      | screenShotName  | Reg_Payment_0010_Android_06 |

  @ACBN-T632 @VCAT-67
  Scenario: Reg_Payment_0012 Auto eDDI Setup and Add Money
    And I verify Available balance
      | screenShotName   | Reg_Payment_0012_Android_01 |
    And I goto Transfer page
    And I click add money Button at Transfer page
      | screenShotName   | Reg_Payment_0012_Android_02 |
    And I turn on Auto Reload Button
      | screenShotName   | Reg_Payment_0012_Android_03 |
    Then I select repeat
      | screenShotName   | Reg_Payment_0012_Android_04 |
      | screenShotName1  | Reg_Payment_0012_Android_05 |
    Then I enter money at Add money page with Repeat Rule
      | money   | 100 |
      | screenShotName   | Reg_Payment_0012_Android_06 |
      | screenShotName1  | Reg_Payment_0012_Android_07 |
    And I verify add money success
      | screenShotName   | Reg_Payment_0012_Android_08 |
    Then I click settingBtn on TransferPage
      | screenShotName   | Reg_Payment_0012_Android_09 |
    And I click Auto Reload settings on Transfer settings page
      | screenShotName   | Reg_Payment_0012_Android_10 |
    Then I verify setup Repeat Rule success
    Then go to home page
    And I verify add Money Record
      | screenShotName   | Reg_Payment_0012_Android_11 |
    And I verify add Money Balance
      | screenShotName   | Reg_Payment_0012_Android_12 |

  @ACBN-T648 @VCAT-68
  Scenario: Reg_Payment_0013 change /delect auto eDDI rules
    And I goto Transfer page
    Then I click settingBtn on TransferPage
      | screenShotName   | Reg_Payment_0013_Android_01 |
    And I click Auto Reload settings on Transfer settings page
      | screenShotName   | Reg_Payment_0013_Android_02 |
    Then I delete first Auto Reload rule
      | screenShotName   | Reg_Payment_0013_Android_03 |
      | screenShotName1  | Reg_Payment_0013_Android_04 |







