Feature: Welab app login

  Background: Login
    Given Open Stage WeLab App
    Given Check upgrade page appears Stage
    When Login with user and password from properties
    Then I can see the LoggedIn page

  @VCAT-12  @health
Scenario: Reg_login_005 learn about channel project test case and overflow
#    When Login with user and password
#      | user     | tester052  |
#      | password | Aa123456 |
#    Then I can see the LoggedIn page
    And View transaction details
      | screenShotName | Reg_login_005_Android_01 |
      | screenShotName1 | Reg_login_005_Android_02 |
      | screenShotName2 | Reg_login_005_Android_03 |
    And I'm checking the information in the purchase record
      | screenShotName | Reg_login_0c05_Android_ |