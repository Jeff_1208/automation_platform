package com.welab.automation.projects.channel.steps.gps;

import lombok.SneakyThrows;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GpsHttpRequest {

    private static String baseURI = "https://gpsehi.sta-wlab.com";
    private static String urlPath = "/v1/ehi/service/getTransaction.asmx";
    private static long currenttime;
    private static String Trans_link;
    private static String Txn_id_lock;
    private static String traceid_lifecycle;
    private static String Txn_id_confirm;
    private static String now;
    private static String details;
    private static String soapPath1="/src/main/resources/app/testData/channel/SoapData1.xml";
    private static String soapPath2="/src/main/resources/app/testData/channel/SoapData2.xml";
    private static String descSoapPath="/src/main/resources/app/testData/channel/Soap.xml";

    private static String Bill_Amt ;
    private static String Txn_Amt;
    private static String Bill_Ccy;
    private static String Txn_Ccy ;
    private static String POS_Data_DE22;
    private static String GPS_POS_Capability;
    private static String Token ;

    public static void main(String[] args) {
        // Bill_Amt	-10	交易金额
        // Txn_CCy	344	交易币种 HKD：344 USD：840
        // GPS_POS_Capability	6	交易类型 ATM:A | Online:6, M| X-pay:9|Unknown:0|POS:1,2,3,4,5,7,R,
        // Token	101276565	用户卡Token
        // Txn_Amt 和Txn_Ccy表示交易金额和货币，这是商品原始的交易金额，比如外币消费 USD 10
        // Bill_Amt 和Bill_Ccy表示账单金额和货币，这是经过费率转换后的港币金额，比如HKD 8， 并且Txn_Amt是正数金额
        //  POS_Data_DE22= "810"   online  POS_Data_DE22= "832"  pos
        String Bill_Amt= "-1001" ;
        String Txn_Ccy= "344" ;
        String GPS_POS_Capability = "A";
        //String Token = "101276565";
        String Token = "129350791";
        String Txn_Amt= "1001" ;
        String Bill_Ccy= "344" ;
        String POS_Data_DE22= "832" ;
        GpsHttpRequest gpsHttpRequest = new GpsHttpRequest(Bill_Amt, Txn_Ccy, GPS_POS_Capability,Token,Txn_Amt,Bill_Ccy,POS_Data_DE22);
        boolean result = gpsHttpRequest.runGpsApi();
        System.out.println("result: "+result);
    }

    public GpsHttpRequest(String Bill_Amt, String Txn_Ccy, String GPS_POS_Capability,String Token,String Txn_Amt, String Bill_Ccy, String POS_Data_DE22){
        currenttime =System.currentTimeMillis();
        Trans_link = currenttime/1000 + "";
        Txn_id_lock = currenttime/1000 + "";
        traceid_lifecycle = currenttime/1000 + "";
        Txn_id_confirm=currenttime + "";
        now = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
        this.Bill_Amt= Bill_Amt;
        this.Txn_Ccy= Txn_Ccy ;
        this.GPS_POS_Capability = GPS_POS_Capability;
        this.Token = Token;
        this.Txn_Amt= Txn_Amt ;
        this.Bill_Ccy= Bill_Ccy ;
        this.POS_Data_DE22= POS_Data_DE22 ;
    }

    public boolean runGpsApi(){
        details=now+GPS_POS_Capability;
        boolean result1 = soapRequest(soapPath1,descSoapPath);
        boolean result2 = soapRequest(soapPath2,descSoapPath);
        return result1&&result2;
    }


    @SneakyThrows
    public static boolean soapRequest(String soapPath,String descSoapPath){
        String soap=getSoapBody(soapPath);
        String project_path =System.getProperty("user.dir");
        String filepPath = project_path+descSoapPath;
        saveSoapToXml(soap,filepPath);
        TestSOAPAPI testSOAPAPI = new TestSOAPAPI();
        String response = testSOAPAPI.postMethod(baseURI,urlPath,filepPath);
        //System.out.println(isPass(response));
        return isPass(response);
    }

    public static boolean isPass(String response){
        int index1  = response.indexOf("<Responsestatus>")+"<Responsestatus>".length();
        int index2  = response.indexOf("</Responsestatus>");
        String result = response.substring(index1,index2);
        System.out.println("result: "+result);
        if(result.equals("00")){
            return true;
        }else{
            return false;
        }
    }

    public static String getSoapBody(String path){
        String soap=read_soap(path);
        soap = updateParams(soap);
        return soap;
    }
    public static String saveSoapToXml(String string,String filePath) {
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream outStream = new FileOutputStream(file);
            outStream.write(string.trim().getBytes());
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }


    public static String read_soap(String path)  {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            String project_path =System.getProperty("user.dir");
            File file = new File(project_path+path);
            InputStream instream = new FileInputStream(file);
            if (instream != null) {
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                String line="";
                while ((line = buffreader.readLine()) != null) {
                    stringBuffer.append(line.trim()+"\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    public static String updateParams(String soap){
        soap = soap.replace("{Txn_Amt}", Txn_Amt);
        soap = soap.replace("{Bill_Ccy}",Bill_Ccy);
        soap = soap.replace("{Bill_Amt}",Bill_Amt);
        soap = soap.replace("{Txn_Ccy}",Txn_Ccy);
        soap = soap.replace("{GPS_POS_Capability}",GPS_POS_Capability);
        soap = soap.replace("{Token}",Token);
        soap = soap.replace("{Trans_link}",Trans_link);
        soap = soap.replace("{Txn_id_lock}",Txn_id_lock);
        soap = soap.replace("{traceid_lifecycle}",traceid_lifecycle);
        soap = soap.replace("{Txn_id_confirm}",Txn_id_confirm);
        soap = soap.replace("{POS_Data_DE22}",POS_Data_DE22);
        soap = soap.replace("{details}",details);
        return soap;
    }
}
