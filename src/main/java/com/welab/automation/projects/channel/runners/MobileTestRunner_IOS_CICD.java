package com.welab.automation.projects.channel.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
            //"src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3047_goSave_iOS.feature",
            "src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3048_login_iOS.feature",
            "src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3049_payment_iOS.feature",
            "src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3050_regCard_iOS.feature",
            "src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3051_regMaintenance_iOS.feature",
            //"src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3238_payroll_IOS.feature",
    },

    glue = {"com/welab/automation/projects/channel/steps/mobile"},
    tags = "@health",
    monochrome = true)
public class MobileTestRunner_IOS_CICD extends TestRunner {}
