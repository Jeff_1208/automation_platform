Feature: Gosave

  Background: Login
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage

  @ACBN-T335
  Scenario:  Reg_GoSave_001 Join GoSave 2.0 & TD IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_001__IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_001__IOS_02 |
      | screenShotName01 | Reg_GoSave_001__IOS_03 |
    And click the gosave pot
    And input gosave amount
      | amount | 100 |
    And confirm gosave
      | screenShotName | Reg_GoSave_001__IOS_04 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_001__IOS_05 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_001__IOS_06 |

  @ACBN-T336
  Scenario:  Reg_GoSave_002 GoSave 2.0 MyDeposit IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_002_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_002_IOS_02 |
      | screenShotName01 | Reg_GoSave_002_IOS_03 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_002_IOS_04 |
    Then check My Deposit Detail IOS
      | screenShotName  | Reg_GoSave_002_IOS_05 |
      | screenShotName1 | Reg_GoSave_002_IOS_06 |
#  @health
#  Scenario Outline:  Reg_GoSave_001 IOS Join Pot
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_001_IOS_01 |
#      | screenShotName1 | Reg_GoSave_001_IOS_02 |
#    And I click the gosave pot
#    And I join gosave pot set <amount>
#    And I check gosave status
#      | screenShotName | Reg_GoSave_001_IOS_03 |
#    Then I verify the gosave balance
#      | screenShotName | Reg_GoSave_001_IOS_04 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_001_IOS_05 |
#    Examples:
#      | amount |
#      | 100    |
#
#  Scenario Outline:  Reg_GoSave_002 IOS Deposit more money for the joined pot before the pot kick start
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_002_IOS_01 |
#      | screenShotName1 | Reg_GoSave_002_IOS_02 |
#    And I check mygosave pending
#    And I join gosave pot more set <amount>
#    And I check gosave deposited more status
#      | screenShotName | Reg_GoSave_002_IOS_03 |
#    Then I verify the gosave balance
#      | screenShotName | Reg_GoSave_002_IOS_04 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_002_IOS_05 |
#    Examples:
#      | amount |
#      | 100    |
#
#  Scenario Outline:  Reg_GoSave_003 IOS Withdrawal before pot start
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_003_IOS_01 |
#      | screenShotName1 | Reg_GoSave_003_IOS_02 |
#    And I check mygosave pending
#    And I withdrawal money before pot start <amount>
#    And I check status withdrawal money before pot start
#      | screenShotName | Reg_GoSave_003_IOS_03 |
#    Then I verify the gosave balance
#      | screenShotName | Reg_GoSave_003_IOS_04 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_003_IOS_05 |
#    Examples:
#      | amount |
#      | 50    |
#
#  Scenario:  Reg_GoSave_005 IOS View started pot
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_005_IOS_01 |
#      | screenShotName1 | Reg_GoSave_005_IOS_02 |
#    And I check mygosave in progress
#    And I View started pot
#      | screenShotName | Reg_GoSave_005_IOS_03 |
#
#  Scenario Outline:  Reg_GoSave_006 IOS Withdrawal after pot start
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_006_IOS_01 |
#      | screenShotName1 | Reg_GoSave_006_IOS_02 |
#    And I check mygosave in progress
#    And I withdrawal money after pot start <amount>
#    And I check status withdrawal money after pot start
#      | screenShotName | Reg_GoSave_006_IOS_03 |
#    Then I verify the gosave balance after withdrawn in progress
#      | screenShotName | Reg_GoSave_006_IOS_04 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_006_IOS_05 |
#    Examples:
#      | amount |
#      | 50    |






#  @health
#  Scenario:  Reg_GoSave_001 IOS Join Pot 1.0
#    And get total balance
#      | screenShotName | Reg_GoSave_001_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_001_IOS_02 |
#    And scroll to bottom
#    And click go to gosave1
#      | screenShotName | Reg_GoSave_001_IOS_02 |
#    And click the gosave pot center
#    And click Join button
#    And input amount for gosave
#      | amount | 100 |
#    And confirm gosave
#      | screenShotName | Reg_GoSave_001_IOS_03 |
#    And verify gosave
#      | screenShotName | Reg_GoSave_001_IOS_04 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_001_IOS_05 |
#
#  Scenario:  Reg_GoSave_002 IOS Deposit more money for the joined pot before the pot kick start 1.0
#    And get total balance
#      | screenShotName | Reg_GoSave_002_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_002_IOS_02 |
#    And scroll to bottom
#    And click go to gosave1
#      | screenShotName | Reg_GoSave_002_IOS_02 |
#    And click the gosave pot center
#    And click Join more button
#    And input amount for gosave
#      | amount | 100 |
#    And confirm gosave
#      | screenShotName | Reg_GoSave_002_IOS_03 |
#    And verify gosave
#      | screenShotName | Reg_GoSave_002_IOS_04 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_002_IOS_05 |
#
#  Scenario:  Reg_GoSave_003 IOS Withdrawal before pot start 1.0
#    And get total balance
#      | screenShotName | Reg_GoSave_003_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_003_IOS_02 |
#    And scroll to bottom
#    And click go to gosave1
#      | screenShotName | Reg_GoSave_003_IOS_02 |
#    And click go to my gosave detail
#    And click the gosave pot center
#    And withdrawal money before pot start
#      | amount | 50 |
#      | screenShotName | Reg_GoSave_003_IOS_03 |
#    And verify gosave
#      | screenShotName | Reg_GoSave_003_IOS_04 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_003_IOS_05 |
#
#  Scenario:  Reg_GoSave_005 IOS View started pot 1.0
#    And get total balance
#      | screenShotName | Reg_GoSave_005_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_005_IOS_02 |
#    And scroll to bottom
#    And click go to gosave1
#      | screenShotName | Reg_GoSave_005_IOS_02 |
#    And click go to my gosave detail
#    And check gosave in progress
#      | screenShotName | Reg_GoSave_005_IOS_03 |
#      | screenShotName1 | Reg_GoSave_005_IOS_04 |
#
#  Scenario:  Reg_GoSave_006 IOS Withdrawal after pot start 1.0
#    And get total balance
#      | screenShotName | Reg_GoSave_003_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_003_IOS_02 |
#    And scroll to bottom
#    And click go to gosave1
#      | screenShotName | Reg_GoSave_003_IOS_02 |
#    And I check mygosave in progress
#    And withdrawal money after pot start
#      | amount | 50 |
#    And I check status withdrawal money after pot start
#      | screenShotName | Reg_GoSave_006_IOS_03 |
#    And verify gosave
#      | screenShotName | Reg_GoSave_006_IOS_04 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_003_IOS_05 |