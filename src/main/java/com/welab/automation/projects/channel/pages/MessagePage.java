package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.SneakyThrows;

import java.util.List;

public class MessagePage extends AppiumBasePage {

    private String pageName = "Message Page";
    public MessagePage() {
        super.pageName = pageName;
    }

    @SneakyThrows
    public void openAndroidMessage() {
        openAndriodApp("com.google.android.apps.messaging",".ui.ConversationListActivity");
        Thread.sleep(1000 *2 );
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Messages')]")
    private MobileElement messagePage;

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView[@id='android:id/list']/android.widget.LinearLayout")
    private List<MobileElement> personalMessagelist;

    @AndroidFindBy(accessibility = "Navigate up")
    private MobileElement personalMessageBackBtn;

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView[@id='android:id/list']/android.widget.FrameLayout[1]")
    private MobileElement lastUserMessage;

    @SneakyThrows
    public void getMessageContent() {
        waitUntilElementVisible(messagePage);
        Thread.sleep(1000);
        openAndroidMessage();
        Thread.sleep(1000);
        activeAndriodApp("welab.bank.mobile.stage");
        Thread.sleep(1000);
        clickElement(lastUserMessage);
        waitUntilElementVisible(personalMessageBackBtn);
        int lastindex = personalMessagelist.size();
        String messageContent = personalMessagelist.get(lastindex-1).getAttribute("name");
        System.out.println("messageContent: "+messageContent);
    }


}
