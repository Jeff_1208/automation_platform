Feature: Gosave

  Background: Login
    Given Open Stage WeLab App
#    And Enable skip Root Stage
#    When Login with user and password from properties
##    Then I can see the LoggedIn page
#
#  Scenario Outline:  Reg_GoSave_001 join a pot
#    And I verify Total balance and Available balance
#      | screenShotName | Reg_GoSave_001_Android_01 |
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_001_Android_02 |
#      | screenShotName1 | Reg_GoSave_001_Android_03 |
#    And I move eyeBtn to top right corner
#    And I click the gosave pot
#    And I join gosave pot set <amount>
#    And I check gosave status
#      | screenShotName | Reg_GoSave_001_Android_04 |
#    Then I verify the gosave balance
#      | screenShotName | Reg_GoSave_001_Android_05 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_001_Android_06 |
#    Examples:
#      | amount |
#      | 100    |
#
#  Scenario Outline:  Reg_GoSave_002 join a pot more
#    And I verify Total balance and Available balance
#      | screenShotName | Reg_GoSave_002_Android_01 |
#    Given I go to GoSave Page
#      | screenShotName | Reg_GoSave_002_Android_02 |
#      | screenShotName1 | Reg_GoSave_002_Android_03 |
#    And I move eyeBtn to top right corner
#    And I check mygosave pending
#    And I join gosave pot more set <amount>
#    And I check gosave deposited more status
#      | screenShotName | Reg_GoSave_002_Android_04 |
#    Then I verify the gosave balance
#      | screenShotName | Reg_GoSave_002_Android_05 |
#    Then I verify the total balance
#      | screenShotName | Reg_GoSave_002_Android_06 |
#    Examples:
#      | amount |
#      | 100    |
#
#  @VCAT-26
#  Scenario Outline: Reg_GoSave_003 Withdrawal before pot start
#    And I verify Total balance and Available balance
#      | screenShotName | Reg_GoSave_003_Android_01 |
#    And I goto Go save page
#      | screenShotName | Reg_GoSave_003_Android_02 |
#    And I move eyeBtn to top right corner
##    And I verify balance amount withdrawal
#    And I goto Pending car Page withDrawal set <amount>
#    And I verify withDrawal completed page
#      | screenShotName | Reg_GoSave_003_Android_03 |
#    Then I verify myTotal Balance After
#      | screenShotName | Reg_GoSave_003_Android_04 |
#    Then I verify the home page total balance
#      | screenShotName | Reg_GoSave_003_Android_05 |
##    And I verify balance amount withdrawal
#    Examples:
#      | amount |
#      | 900   |
#
#  @VCAT-30
#  Scenario: Reg_GoSave_005 view started pot
#    And I go to GoSave Page
#      | screenShotName | Reg_GoSave_005_Android_01 |
#    And I move eyeBtn to top right corner
#    Then I check myGoSave In progress
#      | screenShotName | Reg_GoSave_005_Android_02 |
#    Then I check GoSave Order deposit
#      | screenShotName | Reg_GoSave_005_Android_03 |
#
#
#
#  @VCAT-32
#  Scenario Outline: Reg_GoSave_006 Withdrawal after pot start
#    And I verify Total balance and Available balance
#      | screenShotName | Reg_GoSave_006_Android_01 |
#    And I goto Go save page
#      | screenShotName | Reg_GoSave_006_Android_02 |
#    And I move eyeBtn to top right corner
##    And I verify balance amount withdrawal
#    And I goto In progress car Page withDrawal set <amount>
#    And I verify withDrawal completed page
#      | screenShotName | Reg_GoSave_006_Android_03 |
#    Then I verify myTotal Balance After
#      | screenShotName | Reg_GoSave_006_Android_04 |
#    Then I verify the home page total balance
#      | screenShotName | Reg_GoSave_006_Android_05 |
##    And I verify balance amount withdrawal
#    Examples:
#      | amount |
#      | 100    |
#
#  Scenario Outline: This is a successful example of h5
#    And I verify Total balance and Available balance
#      | screenShotName | Reg_GoSave_001_Android_01 |
#    Given I goto Go save page
#      | screenShotName | Reg_GoSave_001_Android_02 |
#      | screenShotName1 | Reg_GoSave_001_Android_03 |
#    And I move eyeBtn to top right corner
#    And  I switch to h5
#    And I clicked join Now
#    And I Enter <amount>
#    And I click Next
#    Then I clicked OK and agreed to the terms and conditions
#    And I clicked the Finish button
#    And I switch back to the native app
#    Examples:
#      | amount |
#      | 100    |

  @GOS-T96
  Scenario: Reg_GoSave_001 Join GoSave flow
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_001_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_001_Android_02 |
      | screenShotName01 | Reg_GoSave_001_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_001_Android_04 |
    Then I Enter Deposit amount
      | amount         | 100                        |
      | screenShotName | Reg_GoSave_001_Android_05 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_001_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_001_Android_07 |
    And click back button
    And go to main page
    And I verify Total balance Android
      | screenShotName | Reg_GoSave_001_Android_08 |
    
  @GOS-T97
  Scenario: Reg_GoSave_002 the deposit amount exceeds the available balance
    When Login with user and password from properties
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_002_Android_01 |
      | screenShotName01 | Reg_GoSave_002_Android_02 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_002_Android_03 |
    Then Check the deposit amount exceeds the available balance
      | amount         | 99999999                  |
      | screenShotName | Reg_GoSave_002_Android_04 |

  @GOS-T98
  Scenario: Reg_GoSave_003 the deposit amount exceeds the maximum deposit amount
    When Login with user and password from properties
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_003_Android_01 |
      | screenShotName01 | Reg_GoSave_003_Android_02 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_003_Android_03 |
    Then Check the deposit amount exceeds the maximum deposit amount
      | amount         | 1000000                   |
      | screenShotName | Reg_GoSave_003_Android_04 |

  @GOS-T99
  Scenario: Reg_GoSave_004 the deposit amount below the minimum deposit amount
    When Login with user and password from properties
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_004_Android_01 |
      | screenShotName01 | Reg_GoSave_004_Android_02 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_004_Android_03 |
    And Check the deposit amount below the minimum deposit amount
      | amount         | 1                         |
      | screenShotName | Reg_GoSave_004_Android_04 |


  @GOS-T100
  Scenario: Reg_GoSave_005 Increase interest（without promotion）
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_005_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_005_Android_02 |
      | screenShotName01 | Reg_GoSave_005_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_005_Android_04 |
    Then I Enter Deposit amount
      | amount         | 90                        |
      | screenShotName | Reg_GoSave_005_Android_05 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_005_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_005_Android_07 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_005_Android_08 |
    And click back button
    And go to main page
    Then I back to home page verify myDeposit Record
      | screenShotName | Reg_GoSave_005_Android_09 |
    And click back button
    And I verify Total balance Android
      | screenShotName | Reg_GoSave_005_Android_10 |

  @GOS-T101
  Scenario: Reg_GoSave_006 Increase interest（with promotion）
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_006_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_006_Android_02 |
      | screenShotName01 | Reg_GoSave_006_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_006_Android_04 |
    Then I Enter Deposit amount
      | amount         | 120                        |
      | screenShotName | Reg_GoSave_006_Android_05 |
    And I Enter Promotion Code
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_006_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_006_Android_07 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_006_Android_08 |
    And click back button
    And go to main page
    Then I back to home page verify myDeposit Record
      | screenShotName | Reg_GoSave_006_Android_09 |
    And click back button
    And I verify Total balance Android
      | screenShotName | Reg_GoSave_006_Android_10 |
    
  @GOS-T102
  Scenario: Reg_GoSave_007 check my deposit page and detail after join deposit successful
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_007_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_007_Android_02 |
      | screenShotName01 | Reg_GoSave_007_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_007_Android_04 |
    Then I Enter Deposit amount
      | amount         | 130                        |
      | screenShotName | Reg_GoSave_007_Android_05 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_007_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_007_Android_07 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_007_Android_07 |
    Then Check my deposit page and detail
      | screenShotName | Reg_GoSave_007_Android_08 |

  @GOS-T103
  Scenario: Reg_GoSave_008 check in progress page and detail after join deposit successful
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_008_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_008_Android_02 |
      | screenShotName01 | Reg_GoSave_008_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_008_Android_04 |
    Then I Enter Deposit amount
      | amount         | 140                         |
      | screenShotName | Reg_GoSave_008_Android_05 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_008_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_008_Android_07 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_008_Android_07 |
    Then Check in progress page and detail
      | screenShotName   | Reg_GoSave_008_Android_08 |
      | screenShotName01 | Reg_GoSave_008_Android_09 |

  @GOS-T104 
  Scenario: Reg_GoSave_009 early withDrawl flow
    When Login with user and password from properties
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_009_Android_01 |
      | screenShotName01 | Reg_GoSave_009_Android_02 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_009_Android_03 |
    Then Click Early Withdrawal
      | screenShotName   | Reg_GoSave_009_Android_04 |
      | screenShotName01 | Reg_GoSave_009_Android_05 |
    And Click submit
      | screenShotName | Reg_GoSave_009_Android_06 |
    And Click goSave Done
      | screenShotName | Reg_GoSave_009_Android_07 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_009_Android_08 |
    Then Click Ended button
      | screenShotName | Reg_GoSave_009_Android_09 |
    And click back button
    And go to main page
    Then I verify early withDrawl record
      | screenShotName   | Reg_GoSave_009_Android_10 |
      | screenShotName01 | Reg_GoSave_009_Android_11 |

  @GOS-T105
  Scenario: Reg_GoSave_0010 check the Maturity deposit
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_0010_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_0010_Android_02 |
      | screenShotName01 | Reg_GoSave_0010_Android_03 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_0010_Android_04 |
    Then Click Ended button
      | screenShotName | Reg_GoSave_0010_Android_05 |
    And Check the Maturity deposit
      | screenShotName   | Reg_GoSave_0010_Android_06 |
      | screenShotName01 | Reg_GoSave_0010_Android_07 |

  @ACBN-T1153
  Scenario: Reg_GoSave_0012 Join GoSave 2.0 & TD
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_0012_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_0012_Android_02 |
      | screenShotName01 | Reg_GoSave_0012_Android_03 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_0012_Android_04 |
    Then I Enter Deposit amount
      | amount         | 100                        |
      | screenShotName | Reg_GoSave_0012_Android_05 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_0012_Android_06 |
    Then I verify Success joined
      | screenShotName | Reg_GoSave_0012_Android_07 |
    And click back button
    And go to main page
    And I verify Total balance Android
      | screenShotName | Reg_GoSave_0012_Android_08 |


  @ACBN-T1154
  Scenario: Reg_GoSave_0013 GoSave 2.0 MyDeposit
    When Login with user and password from properties
    And get total balance
      | screenShotName | Reg_GoSave_0013_Android_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_0013_Android_02 |
      | screenShotName01 | Reg_GoSave_0013_Android_03 |
    And Go to My deposit Page
      | screenShotName | Reg_GoSave_0013_Android_04 |
    Then Check my deposit page and detail
      | screenShotName | Reg_GoSave_0013_Android_05 |

  @GOS-T106
  Scenario: Reg_GoSave_0011 join time deposit fail with posting restriction account
    And Enable skip Root Stage
    And Login with user and password
      | user     | stress22755 |
      | password | abcD$123456 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_0011_Android_01 |
      | screenShotName01 | Reg_GoSave_0011_Android_02 |
    And Click Join Now Button
      | screenShotName | Reg_GoSave_0011_Android_03 |
    Then I Enter Deposit amount
      | amount         | 100                       |
      | screenShotName | Reg_GoSave_0011_Android_04 |
    And I Click Confirm and accept Terms and Conditions
      | screenShotName | Reg_GoSave_0011_Android_05 |
    Then I verify join time deposit fail Page
      | screenShotName | Reg_GoSave_0011_Android_06 |