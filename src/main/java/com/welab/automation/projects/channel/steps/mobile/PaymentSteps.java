package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.MaintenancePage;
import com.welab.automation.projects.channel.pages.PaymentPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PaymentSteps {
    PaymentPage paymentPage;
    MaintenancePage maintenancePage;
    public PaymentSteps(){
        paymentPage = new PaymentPage();
        maintenancePage = new MaintenancePage();
    }

    @Given("I go to payment Page")
    public void openPaymentPage(Map<String, String> data) {
        paymentPage.openPaymentPage(data.get("screenShotName"),data.get("screenShotName1"));
    }
    @Given("I go to payment Page without transaction")
    public void openPaymentPageWithoutTransaction(Map<String, String> data) {
        paymentPage.openPaymentPageWithoutTransaction(data.get("screenShotName"));
    }
    @Then("I go to payment setting Page")
    public void openPaymentSettingPage(Map<String, String> data) {
        paymentPage.openPaymentSettingPage(data.get("screenShotName"));
    }
    @Then("set fps setting")
    public void setFpsSettingPage(Map<String, String> data) {
        paymentPage.setFpsSettingPage(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }
    @And("select bank")
    public void selectBank(Map<String, String> data) {
        paymentPage.selectBank(data.get("screenShotName"));
    }

    @And("transaction input data ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void transactionInputData(String amount,String account, String name,Map<String, String> data) {
        paymentPage.transactionInputData(amount,account,name,data.get("screenShotName"));
    }
    @And("confirm transaction")
    public void confirmTransaction(Map<String, String> data) {
        paymentPage.confirmTransaction(data.get("screenShotName"));
    }
    @And("confirm bill number transaction")
    public void confirmBillNumberTransaction(Map<String, String> data) {
        paymentPage.confirmBillNumberTransaction(data.get("screenShotName"));
    }
    @And("confirm transaction with limit message")
    public void confirmTransactionWithLimitMessage(Map<String, String> data) {
        paymentPage.confirmTransactionWithLimitMessage(data.get("screenShotName"));
    }
    @And("confirm transaction without msk")
    public void confirmTransactionWithoutMsk(Map<String, String> data) {
        paymentPage.confirmTransactionWithoutMsk(data.get("screenShotName"));
    }
    @And("check available amount")
    public void checkAvailableAmount(Map<String, String> data) {
        paymentPage.checkAvailableAmount(data.get("screenShotName"));
    }
    @And("check transaction on home page")
    public void checkTransationOnHomePage(Map<String, String> data) {
        paymentPage.checkTransationOnHomePage(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("select 672 bank")
    public void select672Bank(Map<String, String> data) {
        paymentPage.select672Bank(data.get("screenShotName"));
    }

    @And("I goto Transfer page")
    public void gotoTransferPage() {
        paymentPage.clickTransferPage();
    }

    @And("I click Send Money at Transfer page")
    public void clickSentMoneyAtTransfer() {
        paymentPage.clicksendMoneyAtTransfer();
    }

    @And("^I transfer money to ([^\"]\\S*)$")
    public void transferMoneyToFPSid(String FPSID) {
        paymentPage.sendToFPSID(FPSID);
    }

    @Then("I can see merchant Information")
    public void merhantInfo(Map<String, String> data) {
        boolean flag = paymentPage.verifyMerhantInfo(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("^I enter ([^\"]\\S*) and ([^\"]\\S*)$")
    public void enterBillnumberAndAmonut(String billnum, String amount) {
        paymentPage.enterBillAndPay(billnum, amount);
    }

    @Then("I can see transfer in progress")
    public void transferInProgress(Map<String, String> data) {
        assertThat(paymentPage.transferInProgress(data.get("screenShotName"))).isTrue();
    }

    @Then("I verify transactions")
    public void verifyTransactions(Map<String, String> data) {
        paymentPage.verifyTransactions(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @And("I verify Available balance")
    public void verifyAvailableBalance(Map<String, String> data) {
        paymentPage.verifyAvailableBalance(data.get("screenShotName"));
    }
    @And("check fail transaction")
    public void checkTransactionFail(Map<String, String> data) {
        paymentPage.checkTransactionFail(data.get("screenShotName"));
    }

    @And("run fps transaction ([^\"]\\S*) ([^\"]\\S*)$")
    public void runFpsTransaction(String amount,String FpsId,Map<String, String> data) {
        paymentPage.runFpsTransaction(amount,FpsId,data.get("screenShotName"));
    }

    @And("run fps transaction Trigger Half Idv Check ([^\"]\\S*) ([^\"]\\S*)$")
    public void runFpsTransactionTriggerHalfIdvCheck(String amount,String FpsId,Map<String, String> data) {
        paymentPage.runFpsTransactionTriggerHalfIdvCheck(amount, FpsId,
                data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("run fps transaction with add payee ([^\"]\\S*) ([^\"]\\S*)$")
    public void runFPSwithAddPayee(String amount,String FpsId,Map<String, String> data) {
        paymentPage.runFPSwithAddPayee(amount,FpsId,data.get("screenShotName"));
    }

    @And("run fps transaction with select payee ([^\"]\\S*)$")
    public void runFPSwithSelectPayee(String amount,Map<String, String> data) {
        paymentPage.runFPSwithSelectPayee(amount,data.get("screenShotName"));
    }

    @And("delete payee")
    public void deletePayee() {
        paymentPage.deletePayee();
    }
    @Then("go to home page")
    public void goToHomePage() {
        paymentPage.goToHomePage();
    }
    @Then("I send money by welab bank")
    public void sendMoneyByWelabBank(Map<String, String> data) {
        paymentPage.clickBankTransfer();
        paymentPage.chooseWeBank();
        paymentPage.sendMoneyByBank(data.get("accountNumber"),data.get("holdname"),data.get("money"),data.get("screenShotName"));
    }

    @And("I verify send money detail by bank transfer")
    public void sendMoneyDetailByBank(Map<String, String> data){
        boolean flag = paymentPage.sendMoneySuccessDetailByBank(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I verify Available balance after send money")
    public void verifyAvailableBalanceByBank(Map<String, String> data){
        boolean flag = paymentPage.verifyAvailableBalanceByBank(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I verify fail Available balance after send money")
    public void verifyFailAvailableBalanceByBank(Map<String, String> data){
        boolean flag = paymentPage.verifyFailAvailableBalanceByBank(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I verify success send money record")
    public void verifySendMoneyRecordByBank(Map<String,String> data){
        boolean recordFlag = paymentPage.verifySendMoneyRecordByBankSuccess(data.get("screenShotName"));
        assertThat(recordFlag).isTrue();
    }

    @Then("I send money by bank672")
    public void sendMoneyByBank672(Map<String, String> data) {
        paymentPage.clickBankTransfer();
        paymentPage.chooseBank672();
        paymentPage.sendMoneyByBank(data.get("accountNumber"),data.get("holdname"),data.get("money"),data.get("screenShotName"));
    }

    @Then("run fps transaction with bill number ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void runTransactionInputDataWithBillNumber(String amount,String FpsId, String billNumber,Map<String, String> data) {
        paymentPage.runTransactionInputDataWithBillNumber(amount,FpsId,billNumber,data.get("screenShotName"));
    }
    @Then("I can see Non-payee page")
    public void Nonpayee(Map<String, String> data) {
        boolean flag = paymentPage.verifyNonePayeePage(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("^I enter ([^\"]\\S*)$")
    public void enterBillnumberAndAmonut(String amount) {
        paymentPage.enterAmountAndPay(amount);
    }

    @And("^I enter money ([^\"]\\S*)$")
    public void enterBillnumberAndAmonutWithPayee(String amount) {
        paymentPage.enterAmountAndPayWithPayee(amount);
    }

    @And("I verify fail send money record")
    public void verifySendMoneyFailRecordByBank(Map<String, String> data){
        paymentPage.verifySendMoneyFailRecordByBank(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("I can see Non-payee page and add payee")
    public void NonpayeeAndAddPayee(Map<String, String> data) {
        boolean flag = paymentPage.verifyNonePayeePage(data.get("screenShotName"));
        assertThat(flag).isTrue();
        paymentPage.AddPayee();
    }

    @And("^I verify no payee ([^\"]\\S*)$")
    public void verifyNoPayee(String FPSID) {
        paymentPage.clickMyPayeePage();
        paymentPage.verifyNoMyPayee(FPSID);
    }

    @And("^I transfer money to my payee")
    public void transferToMyPayee(Map<String, String> data) {
        paymentPage.clickMyPayeePage();
        paymentPage.clickNewPayee();
        boolean flag = paymentPage.payeeAlready(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I click twice back button to channel home page")
    public void backToChannelHomePage(Map<String,String>data){
        maintenancePage.clickSettings(data.get("screenShotName"));
        maintenancePage.clickMyAccountBtn(data.get("screenShotName1"));
    }

    @And("I see send money limit")
    public void verifySendMoneyLimit(Map<String, String> data){
        paymentPage.clickBankTransfer();
        paymentPage.chooseBank672();
        boolean flag =paymentPage.verifySendMoneyByBankLimit(data.get("accountNumber"),data.get("holdname"),data.get("money"),data.get("screenShotName"),data.get("screenShotName1"));
        assertThat(flag).isTrue();
    }


    @And("run add money ([^\"]\\S*)$")
    public void runAddMoney(String amount,Map<String, String> data) {
        paymentPage.runAddMoney(amount,data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("set up auto eDDI rules ([^\"]\\S*) ([^\"]\\S*)$")
    public void setUpAutoEddiRules(String amount, String remainAmount, Map<String, String> data) {
        paymentPage.setUpAutoEddiRules(amount,remainAmount,data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }

    @And("delect auto eDDI rules")
    public void deleteAutoEddiRules(Map<String, String> data) {
        paymentPage.deleteAutoEddiRules(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }

    @And("I click add money Button at Transfer page")
    public void clickAddMoneyBtn(Map<String, String> data){
        paymentPage.clickAddMoneyBtn(data.get("screenShotName"));
    }

    @Then("I enter money at Add money page")
    public void enterMoney(Map<String,String>data){
        boolean flag =paymentPage.enterMoney(data.get("money"),data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I verify add money success")
    public void addMoneySuccess(Map<String,String>data){
        boolean flag =paymentPage.addMoneySuccess(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @And("I verify add Money Record")
    public void iVerifyAddMoneyRecord(Map<String,String>data) {
        boolean addMoneyRecordFlag = paymentPage.verifyAddMoneyRecord(data.get("screenShotName"));
        assertThat(addMoneyRecordFlag).isTrue();
    }

    @And("I verify add Money Balance")
    public void iVerifyAddMoneyBalance(Map<String,String>data) {
        assertThat(paymentPage.verifyAddMoneyBalance(data.get("screenShotName"))).isTrue();
    }

    @And("I turn on Auto Reload Button")
    public void clickAutoReloadBtn(Map<String,String>data){
        paymentPage.clickAutoReloadBtn(data.get("screenShotName"));
    }

    @Then("I select repeat")
    public void selectRepeat(Map<String,String>data){
        paymentPage.selectRepeat(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("I enter money at Add money page with Repeat Rule")
    public void enterMoneyWithRepeat(Map<String,String>data){
        paymentPage.enterMoneyWithRepeatRule(data.get("money"),data.get("screenShotName"),data.get("screenShotName1"));

    }

    @And("I verify add money with Repeat Rule success")
    public void iVerifyAddMoneyWithRepeatRuleSuccess(Map<String,String>data) {
        boolean flag =paymentPage.verifyAddMoneyWithRepeatRuleSuccess(data.get("screenShotName"));
        assertThat(flag).isTrue();
    }

    @Then("I click settingBtn on TransferPage")
    public void iClickSettingBtnOnTransferPage(Map<String,String>data) {
        paymentPage.clickSettingBtnOnTransferPage(data.get("screenShotName"));
    }

    @And("I click Auto Reload settings on Transfer settings page")
    public void iClickAutoReloadSettings(Map<String,String>data) {
        paymentPage.clickAutoReloadSettings(data.get("screenShotName"));
    }

    @Then("I delete first Auto Reload rule")
    public void iDeleteFirstAutoReloadRule(Map<String,String>data) {
        boolean flag =paymentPage.clickFirstAutoReloadRule(data.get("screenShotName"));
        assertThat(flag).isTrue();
        paymentPage.deleteRule();
        maintenancePage.confirm(data.get("screenShotName1"));
    }

    @Then("I verify send money detail by Fpsid")
    public void iVerifySendMoneyDetailByFpsid(Map<String,String>data) {
        assertThat(paymentPage.sendMoneyDetailByFpsid(data.get("screenShotName"))).isTrue();
    }

    @And("I verify send money fail detail by bank transfer")
    public void iVerifySendMoneyFailDetailByBankTransfer(Map<String,String>data) {
        assertThat(paymentPage.sendMoneyFailDetailByBankTransfer(data.get("screenShotName"))).isTrue();
    }

    @Then("I verify FPSID transactions")
    public void verifFpsidTransactions(Map<String, String> data) {
        paymentPage.verifyFpsidTransactions(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("I verify billnum transactions")
    public void verifyBillNumTransactions(Map<String, String> data) {
        paymentPage.verifyBillNumTransactions(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("I verify setup Repeat Rule success")
    public void iVerifySetupRepeatRuleSuccess() {
        assertThat(paymentPage.verifySetupRepeatRuleSuccess()).isTrue();
    }

    @And("run Transaction By QR Code ([^\"]\\S*)$")
    public void runTransactionByQRCode(String amount,Map<String, String> data) {
        paymentPage.runTransactionByQRCode(amount,data.get("screenShotName"));
    }
}
