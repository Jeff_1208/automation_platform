Feature: Gosave2.0

  Background: Login
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage

#  Scenario:  Reg_GoSave_001 IOS Join Pot 2.0
#    And get total balance
#      | screenShotName | Reg_GoSave_001_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_001_IOS_02 |
#    And click back button
#    And click the gosave pot
#    And input gosave amount
#      | amount | 300000 |
#    And confirm gosave
#      | screenShotName | Reg_GoSave_001_IOS_03 |
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_001_IOS_04 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_001_IOS_05 |
#
#  Scenario:  Reg_GoSave_005 IOS View started pot 2.0
#    And get total balance
#      | screenShotName | Reg_GoSave_002_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_002_IOS_02 |
#    And view started pot
#      | screenShotName | Reg_GoSave_002_IOS_03 |
#      | screenShotName1 | Reg_GoSave_002_IOS_04 |
#
#  Scenario:  Reg_GoSave_006 IOS Withdrawal after pot start 2.0
#    And get total balance
#      | screenShotName | Reg_GoSave_003_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_003_IOS_02 |
#    And withdrawal after pot start
#      | screenShotName | Reg_GoSave_003_IOS_03 |
#      | screenShotName1 | Reg_GoSave_003_IOS_04 |
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_003_IOS_05 |
#    And click back button
#    And go to main page
#    Then verify total balance
#      | screenShotName | Reg_GoSave_003_IOS_06 |
#
#  Scenario:  Reg_GoSave_007 IOS View end pot 2.0
#    And get total balance
#      | screenShotName | Reg_GoSave_004_IOS_01 |
#    Given I go to GoSave2 Page
#    And open my GoSave2 Page
#      | screenShotName | Reg_GoSave_004_IOS_02 |
#    And go to end pot
#    And view end pot
#      | screenShotName | Reg_GoSave_004_IOS_03 |
#      | screenShotName1 | Reg_GoSave_004_IOS_04 |


  @GOS-T86
  Scenario:  Reg_GoSave_001 the deposit amount exceeds the available balance IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_001_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_001_IOS_02 |
      | screenShotName01 | Reg_GoSave_001_IOS_03 |
    And click the gosave pot
    And the deposit amount exceeds the available balance IOS
      | amount         | 99999999              |
      | screenShotName | Reg_GoSave_001_IOS_04 |

  @GOS-T89
  Scenario:  Reg_GoSave_002 Increase interest（without promotion）IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_002_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_002_IOS_02 |
      | screenShotName01 | Reg_GoSave_002_IOS_03 |
    And click the gosave pot
    And input gosave amount and check interest rate
      | amount         | 100                   |
      | screenShotName | Reg_GoSave_002_IOS_04 |
    And confirm gosave
      | screenShotName | Reg_GoSave_002_IOS_05 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_002_IOS_06 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_002_IOS_07 |


  @GOS-T90
  Scenario:  Reg_GoSave_003 Increase interest（with promotion）IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_003_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_003_IOS_02 |
      | screenShotName01 | Reg_GoSave_003_IOS_03 |
    And click the gosave pot
    And input gosave amount and check interest rate
      | amount         | 1000                  |
      | screenShotName | Reg_GoSave_003_IOS_04 |
    And confirm gosave with promotion code
      | screenShotName  | Reg_GoSave_003_IOS_05 |
      | screenShotName1 | Reg_GoSave_003_IOS_06 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_003_IOS_07 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_003_IOS_08 |


  @GOS-T93
  Scenario:  Reg_GoSave_004 early withdrawl flow IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_004_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_004_IOS_02 |
      | screenShotName01 | Reg_GoSave_004_IOS_03 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_004_IOS_02 |
    And withdrawal after pot start
      | screenShotName | Reg_GoSave_004_IOS_03 |
      | screenShotName1 | Reg_GoSave_004_IOS_04 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_004_IOS_05 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_004_IOS_06 |

  @GOS-T107
  Scenario:  Reg_GoSave_006 Join GoSave flow IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_006_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_006_IOS_02 |
      | screenShotName01 | Reg_GoSave_006_IOS_03 |
    And click the gosave pot
    And input gosave amount
      | amount | 1000 |
    And confirm gosave
      | screenShotName | Reg_GoSave_006_IOS_04 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_006_IOS_05 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_006_IOS_06 |

  @GOS-T108
  Scenario:  Reg_GoSave_007 the deposit amount exceeds the maximum deposit amount IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_007_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_007_IOS_02 |
      | screenShotName01 | Reg_GoSave_007_IOS_03 |
    And click the gosave pot
    And the deposit amount exceeds the maximum deposit amount IOS
      | amount         | 1000000               |
      | screenShotName | Reg_GoSave_007_IOS_04 |

  @GOS-T109
  Scenario:  Reg_GoSave_008 the deposit amount below the minimum deposit amount IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_008_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_008_IOS_02 |
      | screenShotName01 | Reg_GoSave_008_IOS_03 |
    And click the gosave pot
    And the deposit amount below the minimum deposit amount IOS
      | amount         | 1                     |
      | screenShotName | Reg_GoSave_008_IOS_04 |

  @GOS-T110
  Scenario:  Reg_GoSave_009 check my deposit page and detail after join deposit successful IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_009_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_009_IOS_02 |
      | screenShotName01 | Reg_GoSave_009_IOS_03 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_009_IOS_04 |
    And click back button
    And click the gosave pot
    And input gosave amount
      | amount | 109 |
    And confirm gosave
      | screenShotName | Reg_GoSave_009_IOS_05 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_009_IOS_06 |
    And click back button
    And go to main page
    Then verify total balance
      | screenShotName | Reg_GoSave_009_IOS_07 |

  @GOS-T111
  Scenario:  Reg_GoSave_0010 check in progress page and detail after join deposit successful IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_0010_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName  | Reg_GoSave_0010_IOS_02 |
      | screenShotName01 | Reg_GoSave_0010_IOS_03 |
    And click the gosave pot
    And input gosave amount
      | amount | 110 |
    And confirm gosave
      | screenShotName | Reg_GoSave_0010_IOS_04 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_0010_IOS_05 |
    And check in progress page and detail after join deposit successful IOS
      | screenShotName  | Reg_GoSave_0010_IOS_06 |
      | screenShotName1 | Reg_GoSave_0010_IOS_07 |


  @GOS-T112
  Scenario:  Reg_GoSave_0011 check the Maturity deposit IOS
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_0011_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_0011_IOS_02 |
      | screenShotName01 | Reg_GoSave_0011_IOS_03 |
    And open my GoSave2 Page
      | screenShotName | Reg_GoSave_0011_IOS_04 |
    And check the Maturity deposit IOS
      | screenShotName  | Reg_GoSave_0011_IOS_05 |
      | screenShotName1 | Reg_GoSave_0011_IOS_06 |

  @GOS-T95
  Scenario:  Reg_GoSave_005 join time deposit fail with posting restriction account IOS
    And ios skip Root
    When Login with user and password
      | user     | stress22758 |
      | password | abcD$123456 |
    Then I can see the LoggedIn page
    And get total balance
      | screenShotName | Reg_GoSave_005_IOS_01 |
    Given I go to GoSave2 Page
      | screenShotName   | Reg_GoSave_005_IOS_02 |
      | screenShotName01 | Reg_GoSave_005_IOS_03 |
    And click the gosave pot
    And input gosave amount and check interest rate
      | amount         | 100                   |
      | screenShotName | Reg_GoSave_005_IOS_04 |
    And join time deposit fail with posting restriction account IOS
      | screenShotName  | Reg_GoSave_005_IOS_05 |
      | screenShotName1 | Reg_GoSave_005_IOS_06 |

