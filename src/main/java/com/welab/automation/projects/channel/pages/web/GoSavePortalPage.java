package com.welab.automation.projects.channel.pages.web;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.channel.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GoSavePortalPage extends WebBasePage {
    private final String pageName = "GoSave Portal Page";

    public GoSavePortalPage() {
        super.pageName = this.pageName;
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"username\"]")
    private WebElement user;

    @FindBy(how = How.XPATH, using = "//*[@id=\"password\"]")
    private WebElement pwd;

    @FindBy(how = How.XPATH, using = "//*[@id=\"kc-login\"]")
    private WebElement kcLogin;

    @FindBy(how = How.XPATH, using = "//span[text()='Home']")
    private WebElement Home;

    @FindBy(how = How.XPATH, using = "//span[text()='Deposit Scheme']")
    private WebElement DepositScheme;

    @FindBy(how = How.XPATH, using = "//span[text()='Create']")
    private WebElement DepositSchemeCreate;

    @FindBy(how = How.XPATH, using = "//span[text()='View']")
    private WebElement DepositSchemeView;

    @FindBy(how = How.XPATH, using = "//span[text()='Active']")
    private WebElement DepositSchemeActive;

    @FindBy(how = How.XPATH, using = "//span[text()='Pending Approval']")
    private WebElement DepositSchemePendingApproval;

    @SneakyThrows
    public void loginGosavePortal(String username,String password) {
        Thread.sleep(1000);
        andSendKeys(user,username);
        Thread.sleep(1000);
        andSendKeys(pwd,password);
        Thread.sleep(1000);
        clickElement(kcLogin);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void openDepositScheme() {
        waitUntilElementVisible(Home);
        Thread.sleep(1000);
        clickElement(DepositScheme);
    }

    @SneakyThrows
    public void clickDepositSchemeCreate() {
        waitUntilElementVisible(DepositSchemeCreate);
        Thread.sleep(1000);
        clickElement(DepositSchemeCreate);
    }

    @SneakyThrows
    public void clickDepositSchemeView() {
        waitUntilElementVisible(DepositSchemeView);
        Thread.sleep(1000);
        clickElement(DepositSchemeView);
    }

    @SneakyThrows
    public void clickDepositSchemeActive() {
        waitUntilElementVisible(DepositSchemeActive);
        Thread.sleep(1000);
        clickElement(DepositSchemeActive);
    }

    @SneakyThrows
    public void clickDepositSchemePendingApproval() {
        waitUntilElementVisible(DepositSchemePendingApproval);
        Thread.sleep(1000);
        clickElement(DepositSchemePendingApproval);
    }

    @FindBy(how = How.XPATH, using = "//button[text()='--Select--']")
    private WebElement TenorUnitSelect;

    @FindBy(how = How.XPATH, using = "//nb-option[text()='DAY']")
    private WebElement TenorUnitSelectDay;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tenor\"]")
    private WebElement TenorInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"interestRate\"]")
    private WebElement interestRateInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"penaltyFeeRate\"]")
    private WebElement penaltyFeeRateInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"minimalDepositAmount\"]")
    private WebElement minimalDepositAmountInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"maximumDepositAmount\"]")
    private WebElement maximumDepositAmountInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"capacity\"]")
    private WebElement maximumNumberOfCustomerInput;

    @FindBy(how = How.XPATH, using = "//button[text()='Submit']")
    private WebElement SubmitButton;

    //@FindBy(how = How.XPATH, using = "//span[contains(@text,'created successfully')]")
    @FindBy(how = How.XPATH, using = "//span[text()=' created successfully']")
    private WebElement createdSuccessfully;

    //@FindAll ({
    //        @FindBy(how = How.CLASS_NAME, using = "user-container"),
    //        @FindBy(how = How.CSS, using = "body > app-root > ngx-welabone-theme > nb-layout > div > div > nb-layout-header > nav > ngx-header > div:nth-child(2) > nb-actions > nb-action.user-action > nb-user"),
    //})
    @FindBy(how = How.CLASS_NAME, using = "user-container")  //OK
    private WebElement userNameButton;

    @FindBy(how = How.XPATH, using = "//span[text()='Logout']")
    private WebElement LogoutButton;

    @SneakyThrows
    public void logoutFromGosavePortal(){
        clickElement(userNameButton);
        waitUntilElementVisible(LogoutButton);
        Thread.sleep(100);
        clickElement(LogoutButton);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void fillDataForCreateNewDeposit(String screenShotName,String screenShotName1){
        Thread.sleep(3000);
        clickElement(TenorUnitSelect);
        Thread.sleep(1000);
        clickElement(TenorUnitSelectDay);

        Thread.sleep(100);
        andSendKeys(TenorInput, "2");
        Thread.sleep(100);
        andSendKeys(interestRateInput, "1");

        Thread.sleep(100);
        andSendKeys(penaltyFeeRateInput, "1");

        Thread.sleep(100);
        andSendKeys(minimalDepositAmountInput, "10");
        Thread.sleep(100);
        andSendKeys(maximumDepositAmountInput, "999999");
        Thread.sleep(100);
        andSendKeys(maximumNumberOfCustomerInput, "10");

        takeScreenshot(screenShotName);
        scrollToBottom(pageName);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickElement(SubmitButton);
    }

    @SneakyThrows
    public void verifyCreateNewDepositSuccessfully(String screenShotName){
        waitUntilElementVisible(createdSuccessfully);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @FindBy(how = How.XPATH, using = "//nb-icon[@icon='search']")
    private List<WebElement> searchButtonToApprovedList;

    @FindBy(how = How.XPATH, using = "//datatable-body[@class=\"datatable-body\"]/datatable-selection/datatable-scroller/datatable-row-wrapper[1]/datatable-body-row/div[2]/datatable-body-cell[1]/div/button[1]")
    private WebElement searchButtonToApproved;

    @FindBy(how = How.XPATH, using = "//*[text()='View Deposit Scheme']")
    private WebElement ViewDepositDetail;

    @FindBy(how = How.XPATH, using = "//*[text()=' approved successfully']")
    private WebElement approveSuccessfully;

    @FindBy(how = How.XPATH, using = "//button[text()='Approve']")
    private WebElement ApproveButton;

    @SneakyThrows
    public void approveTheDepositByChecker(String screenShotName, String screenShotName1, String screenShotName2){
        waitUntilElementVisible(searchButtonToApproved);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        clickElement(searchButtonToApproved);
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
        scrollToBottom(pageName);
        clickElement(ApproveButton);
        waitUntilElementVisible(approveSuccessfully);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void openDepositAndViewDetail(String screenShotName, String screenShotName1){
        waitListPage(screenShotName);
        openOneDespositCheckDetail(screenShotName1);
    }

    @SneakyThrows
    public void waitListPage(String screenShotName){
        waitUntilElementVisible(searchButtonToApproved);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openOneDespositCheckDetail(String screenShotName){
        clickElement(searchButtonToApproved);
        waitUntilElementVisible(ViewDepositDetail);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        scrollToBottom(pageName);
    }


    @SneakyThrows
    public void checkDepositRecordInViewPageAfterCreateDepositSuccessfully(String screenShotName, String screenShotName1, String screenShotName2) {
        openDepositAndViewDetail(screenShotName, screenShotName1);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
    }

    @FindBy(how = How.XPATH, using = "//div[@role=\"table\"]/datatable-header/div/div[2]/datatable-header-cell[2]/div/span[2]")
    private WebElement sortButton;

    @FindBy(how = How.XPATH, using = "//div[@role=\"table\"]/datatable-header/div/div[2]/datatable-header-cell[2]/div/span[2]")
    private WebElement sortAscButton;

    @SneakyThrows
    public void checkDepositRecordInActivePageAfterCreateDepositSuccessfully(String screenShotName, String screenShotName1, String screenShotName2) {
        waitListPage(screenShotName);
        clickElement(sortButton);
        Thread.sleep(2000);
        clickElement(sortAscButton);
        Thread.sleep(2000);
        openOneDespositCheckDetail(screenShotName1);
    }
}

