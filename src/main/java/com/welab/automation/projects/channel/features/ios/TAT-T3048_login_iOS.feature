Feature: Welab app login

  Background: Open App
    Given Open Stage WeLab App
    #And ios skip Root
    And Enable skip Root Stage
    Given Check upgrade page appears Stage

#  Scenario: update package for IOS from test flight
#    And update package for IOS

  @health
  Scenario:  Reg_Test bind device IOS
    And skip Rebind Device Authentication IOS
    When Login with user and password from properties
    And bind Device Authentications

  @ACBN-T2 @health
  Scenario:  Reg_Login_001 IOS Normal login (username + password)
    Then get app version
      | screenShotName | Reg_Login_001_IOS_01 |
    When Login with user and password from properties
    Then I can see the LoggedIn page
    Then get screenShot
      | screenShotName | Reg_Login_001_IOS_02 |

  @health  @ACBN-T637
  Scenario:  Reg_Login_005 IOS Balance enquiry and transaction history sequence
    Then get app version
      | screenShotName | Reg_Login_005_IOS_01 |
    When Login with user and password from properties
    Then I can see the LoggedIn page
    Then get screenShot
      | screenShotName | Reg_Login_005_IOS_02 |
    Then check transaction detail
      | screenShotName | Reg_Login_005_IOS_03 |
      | screenShotName1 | Reg_Login_005_IOS_04 |