Feature: Welab app Maintenance MCV

  Background: Login
    Given Open Stage WeLab App
    Given Click got it and Login
    When Login with user and password from properties
    Then I can see the home page

  Scenario Outline: Reg_Maintenance_0003 Increase daily limit with notification Decrease daily limit to 0
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0003_IOS_01 |
    Then  go to my Setting Page
    Then  update daily amount limit <limitAmount1>
      | screenShotName  | Reg_Maintenance_0003_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0003_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0003_IOS_04 |
    Then get screenShot
      | screenShotName | Reg_Maintenance_0003_IOS_05 |
    #Then  go to my Setting Page
    Then update daily amount limit <limitAmount2>
      | screenShotName  | Reg_Maintenance_0003_IOS_06 |
      | screenShotName1 | Reg_Maintenance_0003_IOS_07 |
      | screenShotName2 | Reg_Maintenance_0003_IOS_08 |
    Then get screenShot
      | screenShotName | Reg_Maintenance_0003_IOS_09 |
    Examples:
      | limitAmount1 |  limitAmount2 |
      | 0            |  480000       |

  Scenario: Reg_Maintenance_0005 Change residential address with notification for MCV
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0005_IOS_01 |
    Then  go to my Setting Page
    Then  go to personal Page
    Then  click Address Edit Button for MCV
    Then  update Residential Building For Mcv
      | screenShotName  | Reg_Maintenance_0005_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0005_IOS_03 |

  Scenario: Reg_Maintenance_0006 Change Email address with notification for MCV
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0006_IOS_01 |
    Then  go to my Setting Page
    Then  go to personal Page
    Then  click Address Edit Button for MCV
    Then  update Email Building For Mcv
      | screenShotName  | Reg_Maintenance_0006_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0006_IOS_03 |

  Scenario: Reg_Maintenance_0016 Update Account Opening Information IOS
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0016_IOS_01 |
    Then  go to my Setting Page
    Then  go to personal Page
    Then  click Account Opening Edit Button For MCV
      | screenShotName  | Reg_Maintenance_0016_IOS_02 |
    Then  update For Open Account IOS
      | screenShotName  | Reg_Maintenance_0016_IOS_03 |
      | screenShotName1 | Reg_Maintenance_0016_IOS_04 |
      | screenShotName2 | Reg_Maintenance_0016_IOS_05 |

  Scenario: Reg_Maintenance_0007 Change employment status with notification
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0007_IOS_01 |
    Then  go to my Setting Page
    Then  go to personal Page
    Then  open Work Info Page For MCV
      | screenShotName | Reg_Maintenance_0007_IOS_02 |
    Then  update work information for mcv
      | screenShotName  | Reg_Maintenance_0007_IOS_03 |
      | screenShotName1 | Reg_Maintenance_0007_IOS_04 |
      | screenShotName2 | Reg_Maintenance_0007_IOS_05 |

#  Scenario: Reg_Maintenance_0008 Change nationality info with notification
#    Then  go to my Account Page
#      | screenShotName | Reg_Maintenance_0008_IOS_01 |
#    Then  go to my Setting Page
#    Then  go to personal Page
#    Then  update nation infomation
#      | screenShotName  | Reg_Maintenance_0008_IOS_02 |
#      | screenShotName1 | Reg_Maintenance_0008_IOS_03 |
#      | screenShotName2 | Reg_Maintenance_0008_IOS_04 |
#      | screenShotName3 | Reg_Maintenance_0008_IOS_05 |

  Scenario: Reg_Maintenance_0009 Support
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0009_IOS_01 |
    Then  test support
      | screenShotName  | Reg_Maintenance_0009_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0009_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0009_IOS_04 |

  Scenario: Reg_Maintenance_0010 About Us
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0010_IOS_01 |
    Then  about us
      | screenShotName  | Reg_Maintenance_0010_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0010_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0010_IOS_04 |
      | screenShotName3 | Reg_Maintenance_0010_IOS_05 |
      | screenShotName4 | Reg_Maintenance_0010_IOS_06 |

  Scenario: Reg_Maintenance_0012 Marketing Preferences
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0012_IOS_01 |
    Then  go to my Setting Page
    Then test Marketing Preferences
      | screenShotName  | Reg_Maintenance_0012_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0012_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0012_IOS_04 |
      | screenShotName3 | Reg_Maintenance_0012_IOS_05 |
      | screenShotName4 | Reg_Maintenance_0012_IOS_06 |

  Scenario: Reg_Maintenance_0015 Change nationality info with notification
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0015_IOS_01 |
    Then  go to my Setting Page
    Then  go to personal Page
    Then  tax jurisdiction checking
      | screenShotName  | Reg_Maintenance_0015_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0015_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0015_IOS_04 |
      | screenShotName3 | Reg_Maintenance_0015_IOS_05 |
      | screenShotName4 | Reg_Maintenance_0015_IOS_06 |
      | screenShotName5 | Reg_Maintenance_0015_IOS_07 |

  Scenario: Reg_Maintenance_0002 Change MSK PIN with notification
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0002_IOS_01 |
    Then  go to my Setting Page
    Then  change MSK
      | screenShotName  | Reg_Maintenance_0002_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0002_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0002_IOS_04 |
#    Then  change MSK back
#      | screenShotName | Reg_Maintenance_0002_IOS_05 |

  # need test this case after change msk, wait amount limit message
  Scenario Outline:  Reg_Payment_0015 Transfer to other users and the amount exceeds 10K
    Given I go to payment Page
      | screenShotName | Reg_Payment_0015_IOS_01 |
      | screenShotName1 | Reg_Payment_0015_IOS_02 |
    Then run fps transaction <amount> <FpsId>
      | screenShotName | Reg_Payment_0015_IOS_03|
    Then confirm transaction with limit message
      | screenShotName | Reg_Payment_0015_IOS_04|
    Examples:
      | amount | FpsId    |
      | 12000  | 88880194 |
#      | 12000  | 88880194 |

  Scenario Outline: Reg_Maintenance_0004 Increase daily limit with notificatio Decrease daily limit to 10001
    Then  go to my Account Page
      | screenShotName | Reg_Maintenance_0004_IOS_01 |
    Then  go to my Setting Page
    Then  update daily amount limit with message <limitAmount>
      | screenShotName  | Reg_Maintenance_0004_IOS_02 |
      | screenShotName1 | Reg_Maintenance_0004_IOS_03 |
      | screenShotName2 | Reg_Maintenance_0004_IOS_04 |
    Then get screenShot
      | screenShotName | Reg_Maintenance_0004_IOS_05 |
    Examples:
      | limitAmount |
      | 100001      |

#  Scenario: Reg_Maintenance_0001 Change password with notification
#    Then  go to my Account Page
#      | screenShotName | Reg_Maintenance_0001_IOS_01 |
#    Then  go to my Setting Page
#    Then  change password
#      | screenShotName | Reg_Maintenance_0001_IOS_02 |
#      | screenShotName1 | Reg_Maintenance_0001_IOS_03 |
#      | screenShotName2 | Reg_Maintenance_0001_IOS_04 |
#    Then  change password back
#      | screenShotName | Reg_Maintenance_0001_IOS_05 |
