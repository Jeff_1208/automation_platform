package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.PayrollPage;
import com.welab.automation.projects.channel.pages.SampleAppPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

public class PayrollSteps {

    PayrollPage payrollPage;

    public PayrollSteps() {
        payrollPage = new PayrollPage();
    }

    @And("open payroll page")
    public void openPayrollPage(Map<String, String> data) {
        payrollPage.openPayrollPage(data.get("screenShotName"));
    }

    @And("open Transaction Page")
    public void openTransactionPage() {
        payrollPage.openTransactionPage();
    }

    @And("open Add Money Page")
    public void openAddMoneyPage() {
        payrollPage.openAddMoneyPage();
    }

    @And("open Download Page")
    public void openDownloadPage() {
        payrollPage.openDownloadPage();
    }

    @And("input Amount And Swipe The Slide")
    public void inputAmountAndSwipeTheSlide(Map<String, String> data) {
        payrollPage.inputAmountAndSwipeTheSlide(data.get("amount"),data.get("screenShotName"));
    }
    @And("input Amount And Confirm")
    public void inputAmountAndConfirm(Map<String, String> data) {
        payrollPage.inputAmountAndConfirm(data.get("amount"),data.get("screenShotName"));
    }

    @And("check Transaction Result Page")
    public void checkTransactionResultPage(Map<String, String> data) {
        payrollPage.checkTransactionResultPage(data.get("screenShotName"));
    }

    @And("check Payroll Page")
    public void checkPayrollPage(Map<String, String> data) {
        payrollPage.checkPayrollPage(data.get("screenShotName"));
    }


    @And("check Estatement Content")
    public void checkEstatementContent(Map<String, String> data) {
        payrollPage.checkEstatementContent(data.get("screenShotName"));
    }

    @And("download Estatement PDF")
    public void downloadEstatementPDF(Map<String, String> data) {
        payrollPage.downloadEstatementPDF(data.get("screenShotName"));
    }

    @And("go to payroll page")
    public void goToPayrollPage(Map<String, String> data) {
        payrollPage.goToPayrollPage(data.get("screenShotName"));
    }

    @And("go to Download Page")
    public void goToDownloadPage() {
        payrollPage.goToDownloadPage();
    }

    @Then("I click Send Money at Payroll page")
    public void iClickSendMoneyAtPayrollPage(Map<String, String> data) {
        payrollPage.clickSendMoney(data.get("screenShotName"));
    }

    @Then("Send money at payroll page")
    public void SendMoneyAtPayrollPage(Map<String, String> data) {
        payrollPage.SendMoneyAtPayrollPage(data.get("amount"),data.get("screenShotName"));
    }

    @And("verify Transaction Result Page")
    public void verifyTransactionResultPage(Map<String, String> data) {
        payrollPage.verifyTransactionResultPage(data.get("screenShotName"));
    }

    @Then("Add money at payroll page")
    public void AddMoneyAtPayrollPage(Map<String, String> data) {
        payrollPage.AddMoneyAtPayrollPage(data.get("amount"),data.get("screenShotName"));
    }

    @And("check add money Success")
    public void checkAddMoneySuccess(Map<String, String> data) {
        payrollPage.checkAddMoneySuccess(data.get("screenShotName"));
    }

    @And("download Estatement PDF Android")
    public void downloadEstatementPDFAndroid(Map<String, String> data) {
        payrollPage.downloadEstatementPDFAndroid(data.get("screenShotName"));
    }

}
