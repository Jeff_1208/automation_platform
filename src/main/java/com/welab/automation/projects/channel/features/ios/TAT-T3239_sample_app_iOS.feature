Feature: Sample App2App

  Background: Open App
#    Given Open Stage WeLab App
    And open HKAB Merchant Sample App

  @jeff2 @ACBN-T319
  Scenario:  Reg_Sample_APP_001 IOS App2App Merchant Payment Happy flow with Editable Amount
    And set Server Certificate Options
      | screenShotName | Reg_Sample_App_001_IOS_01 |
    And set Negative Testing Options
      | screenShotName | Reg_Sample_App_001_IOS_02 |
    And set FPS Ios
      | screenShotName | Reg_Sample_App_001_IOS_03 |
    And set Additional Data
      | screenShotName | Reg_Sample_App_001_IOS_04 |
    And set Products
      | screenShotName | Reg_Sample_App_001_IOS_05 |
    And set Transaction Detail
      | screenShotName | Reg_Sample_App_001_IOS_06 |
    And set Server Certificate Options
      | screenShotName | Reg_Sample_App_001_IOS_07 |
    And click Pay With Callback
      | screenShotName | Reg_Sample_App_001_IOS_08 |

