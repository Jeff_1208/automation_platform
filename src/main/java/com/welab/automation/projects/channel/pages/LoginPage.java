package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import lombok.Getter;
import lombok.SneakyThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class LoginPage extends AppiumBasePage {

  private String pageName = "Login Page";
  private static final String[] login_text = {"Login","登入"};
  private static final String cancelText = "Cancel";
  private int customWait = 60;
  private int cancelMessageWait = 5;

    CommonPage commonPage;
    CardPage cardPage;
    public LoginPage() {
        super.pageName = pageName;
        commonPage = new CommonPage();
        cardPage = new CardPage();
        if (System.getProperty("mobile").equalsIgnoreCase("cloudios"))
            ((AppiumDriver<MobileElement>) BaseDriver.getMobileDriver()).activateApp("welab.bank.sit");
    }

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"不，谢谢\"]")
  private MobileElement noThanksBtn;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登录'"),
 })
  protected MobileElement loginTxt;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='我明白啦']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Got it''"),
          @iOSXCUITBy(iOSNsPredicate = "name == '我明白啦'"),
  })
  private MobileElement gotIt;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Username']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='用戶名稱']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"用户名\" AND name == \"用户名\" AND value == \"用户名\""),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"用户名\"]"),
  })
  private MobileElement userNameTxt;

  @AndroidFindBy(accessibility = "username-input")
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"用户名\" AND name == \"用户名\" AND value == \"用户名\""),
  })
  private MobileElement userNameInput;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Password']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='密碼']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"密码\"]"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"密码\" AND name == \"密码\" AND value == \"密码\""),
  })
  private MobileElement passwordTxt;

  @AndroidFindBy(accessibility = "password-input")
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"密码\" AND name == \"密码\" AND value == \"密码\""),
  })
  private MobileElement passwordInput;

  @AndroidFindBy(xpath = "//*[@text='Welcome back']")
  private MobileElement welcomeBack;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"login-screen\"]/android.view.ViewGroup[3]/android.view.ViewGroup[8]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"登入\" AND name == \"text\""),
          @iOSXCUITBy(iOSNsPredicate = "label == \"登录\" AND name == \"text\""),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"登入\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@text=\"登入\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"登录\"]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"登录\"])[2]"),
  })
  private MobileElement loginButton;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"你好\""),
    })
    private MobileElement hello;
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"请输入正确的用户名/密码\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"請輸入正確嘅用戶名稱/密碼\"]"),
  })
  private MobileElement loginUsernamePasswordErrorMessage;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
  private MobileElement notification;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement next;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start Testing'")
  private MobileElement start;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Ask App not to Track'")
  private MobileElement noTracking;

//  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]")
//  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Total balance')]")
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text, '總結餘')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Total balance')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用余额')]"),
  })
  public MobileElement totalBalance;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'WeLab')]"),
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[contains(@name, 'WeLab')]"),
  })
  private MobileElement openAppEle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'month GoSave Time Deposit')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Other transaction')]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'4個月GoSave定期存款')]"),
            @AndroidBy(xpath = "//android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]"),
    })
    private MobileElement transaction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup"),
//            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[2]")
    })

    private MobileElement transactionRecord;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> textView;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transaction detail']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement returnButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Recent transactions']"),
            @AndroidBy(xpath = "//*[@text='最近交易']"),
    })
    private MobileElement transactions;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='See all']"),
//            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//*[@text='查看更多']"),
    })
    private MobileElement seeAll;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private List<MobileElement> getList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"),
    })
    private List<MobileElement> textElement;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[3]"),
    })
    private MobileElement reference;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> referenceNumber;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc='login-screen']/android.view.ViewGroup[3]/android.widget.TextView[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Open account']/../android.widget.TextView[2]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"開戶\"]/../XCUIElementTypeStaticText"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"开户\"]/../XCUIElementTypeStaticText"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Open account\"]/../XCUIElementTypeStaticText[1]"),
    })
    private MobileElement appVersion;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"I am rebound\"]"),
    })
    private MobileElement helloAlert;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"OK\"]"),
    })
    private MobileElement helloOKBtn;

    @SneakyThrows
    public void checkHelloAlert(){
        Thread.sleep(3000);
        if(isShow(helloAlert,5)){
            clickElement(helloOKBtn);
        }
        checkLaterButton();
        checkTransactionDetailPage();
        checkWelabBankInstructionPage();
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"WeLab Bank 全港獨家慳息承諾\"]")
    private MobileElement welabBankInstructionPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"交易詳情\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"交易详情\" AND name == \"text\""),
    })
    private MobileElement transactionDetailPage;

    @AndroidFindAll({
            @AndroidBy(accessibility = "Home, tab, 1 of 4"),
            @AndroidBy(accessibility = "Home, tab, 1 of 5"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='主頁']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"主页, tab, 1 of 5\""),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'Home, tab, 1 of 5'")
    })
    private MobileElement homeTab;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"系統遇到問題\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"系统遇到问题\" AND name == \"text\""),
    })
    private MobileElement systemIssueAlert;
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"取消\" AND name == \"text\"")
    private MobileElement systemIssueAlertCancelButton;

    public void checkSystemIssue(){
        if(isShow(systemIssueAlert,2)){
            clickElement(systemIssueAlertCancelButton);
        }
    }

    public void checkTransactionDetailPage(){
        if(isAndroid()) return;
        if(isShow(transactionDetailPage,1)){
            clickByLocationByPercent(6,6);
        }
    }

    public void checkWelabBankInstructionPage(){
        if(isAndroid()) return;
        if(isShow(welabBankInstructionPage,1)){
            clickByLocationByPercent(6,6);
        }
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"以后\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"稍後再說\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"稍后再说\""),
    })
    private MobileElement laterButton;
    public void checkLaterButton(){
        if(isAndroid()) return;
        if(isShow(laterButton,1)){
            clickElement(laterButton);
        }
    }

    @SneakyThrows
    public void IcanSeeHomePage() {
        checkHelloAlert();
        waitUntilElementVisible(homeTab);
        Thread.sleep(1000);
    }

  public String getLoggedInPageText() {
    checkHelloAlert();
    waitUntilElementVisible(totalBalance,30);
    if (isElementDisplayed(totalBalance) != null) {
      return totalBalance.getText();
    }
    return "Can't find total balance element.";
  }

  public void storeAppVersion() {
     StringBuffer stringBuffer = new StringBuffer();
     waitUntilElementVisible(appVersion);
     GlobalVar.GLOBAL_VARIABLES.put("appVersion", appVersion.getText());
  }

  public void loginWithUserAndPasswordFromProperties() {
    storeAppVersion();
    String platform =  System.getProperty("mobile");
    String user= GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
    String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
    clickElement(userNameTxt);
    clearAndSendKeys(userNameInput, user);
    hideAndroidKeyboard();
    clickElement(passwordTxt);
    clearAndSendKeys(passwordInput, password);
    //inputEnter(passwordInput);
    hideAndroidKeyboard();
    clickHelloOnLoginPage();
    if(isShow(loginButton, 2)){
        clickElement(loginButton);
    }
    checkLoginErrorMessage();
  }

  @SneakyThrows
  public void clickHelloOnLoginPage(){
        if(isAndroid()) return;
        clickElement(hello);
        Thread.sleep(1000);
  }

  @SneakyThrows
  public void checkLoginErrorMessage(){
        if(isAndroid()) return;
        if(isShow(loginUsernamePasswordErrorMessage,2)){
            Thread.sleep(3);
            clickElement(loginButton);
        }
//        if(isShow(verifyNow,4)){
//            clickElement(verifyNow);
//            if(isShow(cancel,4)){
//                clickElement(cancel);
//            }
//        }

  }

  public void loginWithCredential(String user, String password) {
    logger.info("Login with credential: " + user + ", " + password);
    clickElement(userNameTxt);
    clearAndSendKeys(userNameInput, user);
    hideAndroidKeyboard();
    clickElement(passwordTxt);
    clearAndSendKeys(passwordInput, password);
    inputEnter(passwordInput);
    hideAndroidKeyboard();
    clickElement(loginButton);
    checkLater();
  }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"Close Message\"]"),
    })
    private MobileElement closeMesssageButton;

  public void checkLater(){
        if(isIOS()){
            if(isShow(laterButton,15)){
                clickElement(laterButton);
            }
            if(isShow(closeMesssageButton,1)){
                clickElement(closeMesssageButton);
            }
        }
  }
    public String getLoginPageText(Map<String, String> data) {
        waitUntilElementVisible(totalBalance,60);
        takeScreenshot(data.get("screenShotName"));
        if (isElementDisplayed(totalBalance) != null) {
            return totalBalance.getText();
        }
        return "Can't find total balance element.";
    }

    @SneakyThrows
    public void LoginAgain( Map<String, String> data) {
        Thread.sleep(1000 * 5);
        String platform =  System.getProperty("mobile");
        String user=GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        logger.info("Login with credential: " + user + ", " + password);
        takeScreenshot(data.get("screenShotName"));
        clickElement(userNameTxt);
        clearAndSendKeys(userNameInput, user);
        hideAndroidKeyboard();
        clickElement(passwordTxt);
        clearAndSendKeys(passwordInput, password);
        inputEnter(passwordInput);
        hideAndroidKeyboard();
        takeScreenshot(data.get("screenShotName1"));
        clickElement(loginButton);
    }
    public void hideAndroidKeyboard(){
      try {
          if (System.getProperty("mobile").contains("android")) hideKeyboard();
      }catch (Exception e){
      }
  }

  @SneakyThrows
  public String getLoginIconText() {
    if (System.getProperty("mobile").contains("ios")) {
      Thread.sleep(1000 * 5);
    }
    waitUntilElementVisible(loginTxt);
    return loginTxt.getText();
  }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@text=\"不允许\"]"),
            @iOSXCUITBy(xpath = "//*[@name=\"不允许\"]"),
    })
    private MobileElement notAllow;
    @iOSXCUITFindBy(xpath = "//*[@name=\"取消\"]")
    private MobileElement cancel;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"立刻验证\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"立即驗證\"]"),
    })
    private MobileElement verifyNow;


  @SneakyThrows
  public void waitAppOpen() {
      Thread.sleep(1000 * 5);

      if(commonPage.isIOS()) {
//          checkLaterButton();
//          if(isShow(notAllow, 1)){clickElement(notAllow);}
//          if(isShow(cancel, 1)){clickElement(cancel);}
//          if (isShow(noThanksBtn,2)){
//              clickElement(noThanksBtn);
//          }
          if (isShow(userNameTxt,2)){
          }else{
              clickElement(loginTxt);
          }
      }
      if(commonPage.isAndroid()) {
          commonPage.checkWelcomeLogin();
      }
  }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='OK']"),
            @AndroidBy(xpath = "//*[@text='確定']")
    })
    private MobileElement alertForAppCarriesRisks;

    public void checkAlertForAppCarriesRisks() {
        if(isShow(alertForAppCarriesRisks,2)){
            clickElement(alertForAppCarriesRisks);
        }
    }

  public String getLoginButtonText() {
    waitUntilElementVisible(loginButton);
    return totalBalance.getText();
  }

    @SneakyThrows
    public void clickTransactionDetails(String screenShotName,String screenShotName1) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
//        scrollUp();
        Thread.sleep(5000);
        clickElement(transactionRecord);
        Thread.sleep(9000);
        takeScreenshot(screenShotName1);
    }
    public boolean viewTransactionDetails() {
        List<String> list=new ArrayList<>();
        waitUntilElementVisible(reference,6000);
        boolean detailText=true;
        for (MobileElement text : textView) {
            list.add(text.getText());
        }
        for (MobileElement text : referenceNumber) {
            list.add(text.getText());
        }
        for (String str : list) {
            if (str==""){
                return detailText=false;
            }
        }
        return detailText;
    }
    @SneakyThrows
    public void clickReturnButton(String screenShotName) {
        clickElement(returnButton);
        Thread.sleep(9000);
        takeScreenshot(screenShotName);
    }
    public void Pullup() {
        waitUntilElementVisible(transaction,600000000);
        for (int i=0;i<=100;i++){
            if (!verifyElementExist(transactions)){
                scrollUp();
            }
        }
    }
    @SneakyThrows
    public Boolean getListInformation(String screenShotName){
        List<String> list=new ArrayList<>();
        for (int i=1;i<=getList.size();i++){
            String str=null;
            int a = i + 3;
            if (a >= 10) {
                str = screenShotName + a;
                takeScreenshot(str);
            }
            if (a < 10) {
                str = screenShotName + "0" + a;
                takeScreenshot(str);
            }

            for (MobileElement text : textElement) {
                if (!list.contains(text.getText())) {
                    list.add(text.getText());
                }
                if (isValidDate(text.getText())) {
                    if (!isValidDate(text.getText())) {
                        return false;
                    }
                }
                if (isValidSimpleDateFormat(text.getText())) {
                    if (!isValidSimpleDateFormat(text.getText())) {
                        return false;
                    }
                }
            }
            if (verifyElementExist(seeAll)){
                if (seeAll.getText().equals("See all") ||seeAll.getText().equals("查看更多")) {
                    break;
                }
            }
            scrollUp();
        }
        return listAfter(list);
    }
    public boolean listAfter(List<String> listMaker) throws ParseException, InterruptedException {
        listMaker.remove(0);
        Thread.sleep(3000);
        boolean than=true;
        SimpleDateFormat dateFormat =null;
        if (listMaker.size()!=0){
            if (isValidSimpleDateFormat(listMaker.get(0))){
                dateFormat = new SimpleDateFormat("yyyy年MM月dd日", Locale.ENGLISH);
            }
            if (isValidDate(listMaker.get(0))){
                dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
            }
        }
        for (int i = 1; i < listMaker.size()-1; i++) {
            Date date = dateFormat.parse(listMaker.get(i));
            Date datestr = dateFormat.parse(listMaker.get(i-1));
            if (date.after(datestr)){
                return than=false;
            }
        }
        return than;
    }
    public static boolean isValidDate(String str) {
        boolean convertSuccess=true;
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        try {
            dateFormat.parse(str);
        } catch (ParseException e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }
    public static boolean isValidSimpleDateFormat(String str) {
        boolean convertSuccess=true;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日", Locale.ENGLISH);
        try {
            dateFormat.parse(str);
        } catch (ParseException e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }

    public void getAppVersion(String screenShotName) {
      waitUntilElementVisible(appVersion);
      appVersion.getText();
      System.out.println("appVersion.getText(): "+appVersion.getText());
      takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void getScreenShot(String screenShotName) {
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkTransactionDetail(String screenShotName,String screenShotName1) {
      scrollUp();
      Thread.sleep(1000);
      takeScreenshot(screenShotName);
      clickElement(cardPage.transactionDetails);
      Thread.sleep(8000);
      takeScreenshot(screenShotName1);
    }


}
