Feature: WeLab app Maintenance CH

  Background: Login
    Given Open Stage WeLab App
    Given Check upgrade page appears Stage
    When Login with user and password from properties
    Then I can see the LoggedIn page
  @114
  Scenario: Reg_Maintenance_0003 Increase daily limit with notification Decrease daily limit to 0
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0003_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Maintenance_0003_Android_02 |
    When I clicked the trading limit button
      | screenShotName1 | Reg_Maintenance_0003_Android_03 |
      | screenShotName2 | Reg_Maintenance_0003_Android_04 |
      | amount | 0 |
      | key | 123456|
      | screenShotName3 | Reg_Maintenance_0003_Android_05 |
      | screenShotName4 | Reg_Maintenance_0003_Android_06 |
      | screenShotName5 | Reg_Maintenance_0003_Android_07 |
    When Verify whether the modified limit is changed
      | amount | 0 |

  @115
  Scenario: Reg_Maintenance_0004 Increase daily limit with notification Decrease daily limit to 30001
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0004_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Maintenance_0004_Android_02 |
    When I clicked the trading limit button
      | screenShotName1 | Reg_Maintenance_0004_Android_03 |
      | screenShotName2 | Reg_Maintenance_0004_Android_04 |
      | amount | 10001 |
      | key | 123456|
      | screenShotName3 | Reg_Maintenance_0004_Android_05 |
      | screenShotName4 | Reg_Maintenance_0004_Android_06 |
      | screenShotName5 | Reg_Maintenance_0004_Android_07 |
    When Verify whether the modified limit is changed
      | amount | 30001 |
 #** Scenario: Reg_Maintenance_0005 Change residential address with notification
#    Then  Navigate to My Accounts
#      | screenShotName | Reg_Maintenance_0005_Android_01 |
#    When  Click on Settings
#      | screenShotName1 | Reg_Maintenance_0005_Android_02 |
 #   When I clicked on my personal information
 #     | screenShotName1 | Reg_Maintenance_0005_Android_03 |
#    When I clicked the personal address modification
#      | screenShotName2 | Reg_Maintenance_0005_Android_04 |
#    When I enter special characters
#      | screenShotName5 | Reg_Maintenance_0005_Android_ |

  Scenario: Reg_Maintenance_0007 Change employment status with notification
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0007_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Maintenance_0007_Android_02 |
    Then  Click personal information title
    Then  through adb common input MSK password
      | screenShotName | Reg_Maintenance_0007_Android_03 |
    Then  Modify  the employment details
      | screenShotName | Reg_Maintenance_0007_Android_04 |
      | screenShotName1 | Reg_Maintenance_0007_Android_05 |
      | screenShotName2 | Reg_Maintenance_0007_Android_06 |
      | screenShotName3 | Reg_Maintenance_0007_Android_07 |
      | screenShotName4 | Reg_Maintenance_0007_Android_08 |
      | screenShotName5 | Reg_Maintenance_0007_Android_09 |
      | screenShotName6 | Reg_Maintenance_0007_Android_10 |
      | screenShotName7 | Reg_Maintenance_0007_Android_11 |
    Then Verify that the modification is successful
      | screenShotName | Reg_Maintenance_0007_Android_12 |


  Scenario Outline: Reg_Maintenance_0008 Change nationality info with notification
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0008_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0008_Android_02 |
    Then  Click personal information title
    Then  through adb common input MSK password
      | screenShotName | Reg_Maintenance_0008_Android_03 |
    Then  Modify nationality or region information <nationalityOrRegion> and <tax>
      | screenShotName  | Reg_Maintenance_0008_Android_04 |
      | screenShotName1 | Reg_Maintenance_0008_Android_05 |
      | screenShotName2 | Reg_Maintenance_0008_Android_06 |
      | screenShotName3 | Reg_Maintenance_0008_Android_07 |
    Examples:
      | nationalityOrRegion                 | tax                     |
      | 香港特別行政區,中國,澳洲                | ,123456789123456,123    |

  Scenario: Reg_Maintenance_0009 Support slide normal
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0009_Android_01 |
    And  Click on Support
      | screenShotName | Reg_Maintenance_0009_Android_02 |
      | screenShotName1 | Reg_Maintenance_0009_Android_03 |
      | screenShotName2 | Reg_Maintenance_0009_Android_04 |
      | screenShotName3 | Reg_Maintenance_0009_Android_05 |
      | screenShotName4 | Reg_Maintenance_0009_Android_06 |
      | screenShotName5 | Reg_Maintenance_0009_Android_07 |
      | screenShotName6 | Reg_Maintenance_0009_Android_08 |

  Scenario: Reg_Maintenance_0010 About Us Page Verify
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0010_Android_01 |
    And  Click on About us
      | screenShotNameUp | Reg_Maintenance_0010_Android_02 |
      | screenShotNameMiddle | Reg_Maintenance_0010_Android_03 |
      | screenShotNameDown | Reg_Maintenance_0010_Android_04 |
    And  I click phone,email,bank url  and ig
      | screenShotNamePhone | Reg_Maintenance_0010_Android_05 |
      | screenShotNameEmail | Reg_Maintenance_0010_Android_06 |
      | screenShotNameBank | Reg_Maintenance_0010_Android_07 |
      | screenShotNameIg | Reg_Maintenance_0010_Android_08 |
    And  I click other prompts or notices and verify them
      | screenShotName | Reg_Maintenance_0010_Android_09 |
      | screenShotName1 | Reg_Maintenance_0010_Android_10 |
      | screenShotName2 | Reg_Maintenance_0010_Android_11 |
      | screenShotName3 | Reg_Maintenance_0010_Android_12 |
      | screenShotName4 | Reg_Maintenance_0010_Android_13 |
      | screenShotName5 | Reg_Maintenance_0010_Android_14 |
      | screenShotName6 | Reg_Maintenance_0010_Android_15 |
      | screenShotName7 | Reg_Maintenance_0010_Android_16 |


  Scenario: Reg_Maintenance_0012 Change Marketing Preferences
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0012_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0012_Android_02 |
    Then  Click marketing preferences title
      | screenShotName | Reg_Maintenance_0012_Android_03 |
    Then  Trigger Marketing Preferences flow and check notification
      | screenShotName | Reg_Maintenance_0012_Android_04 |


  Scenario Outline: Reg_Maintenance_0015 Tax Jurisdiction Checking
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0015_Android_01 |
    When  Click on Settings
      | screenShotName | Reg_Maintenance_0015_Android_02 |
    Then Click personal information title
    Then  through adb common input MSK password
      | screenShotName | Reg_Maintenance_0015_Android_03 |
    Then Modify press through all the options <nationalityOrRegion> and <reason>
      | screenShotName  | Reg_Maintenance_0015_Android_04 |
      | screenShotName1 | Reg_Maintenance_0015_Android_05 |
      | screenShotName2 | Reg_Maintenance_0015_Android_06 |
      | screenShotName3 | Reg_Maintenance_0015_Android_07 |
      | screenShotName4 | Reg_Maintenance_0015_Android_08 |
    Examples:
      | nationalityOrRegion                       | reason  |
      | 海地,美屬薩摩亞,阿富汗                        | no tin  |

  @113  @health
  Scenario: Reg_Maintenance_0002 Able to change MSK PIN successfully receive notification successfully
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0002_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Maintenance_0002_Android_02 |
    When I clicked the Change mobile security key button
      | screenShotName | Reg_Maintenance_0002_Android_03 |
    When I enterI enter input MSK key
      | screenShotName | Reg_Maintenance_0002_Android_04 |
    When I enter new key
      | screenShotName1 | Reg_Maintenance_0002_Android_05 |
      | screenShotName2 | Reg_Maintenance_0002_Android_06 |
#    When I restored my key
    When I clicked the Change mobile security key button
      | screenShotName | Reg_Maintenance_0002_Android_07 |
    When  I enterI enter input MSK key
      | screenShotName | Reg_Maintenance_0002_Android_08 |

  @VCAT-51
  Scenario:Reg_Payment_0015 new device, transaction limit 10000-CH
    Then  Navigate to My Accounts
      | screenShotName  | Reg_Payment_0015_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Payment_0015_Android_02 |
    When I clicked the Change mobile security key button
      | screenShotName  | Reg_Payment_0015_Android_03 |
    When I enterI enter input MSK key
      | screenShotName  | Reg_Payment_0015_Android_04 |
    When I enter new key
      | screenShotName1 | Reg_Payment_0015_Android_05 |
      | screenShotName2 | Reg_Payment_0015_Android_06 |
    And I click twice back button to channel home page
      | screenShotName  | Reg_Payment_0015_Android_07 |
      | screenShotName1 | Reg_Payment_0015_Android_08 |
    And I verify Available balance
      | screenShotName  | Reg_Payment_0015_Android_09 |
    And I goto Transfer page
    And I click Send Money at Transfer page
    Then I see send money limit
      |accountNumber|1000476928 |
      |holdname     |WILSON,Wil |
      |money        |10001      |
      | screenShotName    | Reg_Payment_0015_Android_10 |
      | screenShotName1   | Reg_Payment_0015_Android_11 |

  @112
  Scenario: Reg_Maintenance_0001 Change password with notification
    Then  Navigate to My Accounts
      | screenShotName | Reg_Maintenance_0001_Android_01 |
    When  Click on Settings
      | screenShotName1 | Reg_Maintenance_0001_Android_02 |
    When I clicked the change password button
      | screenShotName | Reg_Maintenance_0001_Android_03 |
    When I entered my old password
      | screenShotName | Reg_Maintenance_0001_Android_04|
      | password | Aa123456|
    When Click next
      | screenShotName | Reg_Maintenance_0001_Android_05|
    When Enter new password
      | screenShotName | Reg_Maintenance_0001_Android_06|
      | newPassword | Aa1234567|
    When Click Done
      | screenShotName | Reg_Maintenance_0001_Android_07|
    When input Key
      | screenShotName | Reg_Maintenance_0001_Android_08|
      | key | 123456|
    When I clicked the Settings
      | screenShotName| Reg_Maintenance_0001_Android_09|
    When I clicked the log out
    When Login again
      | user     | tester052  |
      | password | Aa1234567   |
      | screenShotName| Reg_Maintenance_0001_Android_10|
      | screenShotName1| Reg_Maintenance_0001_Android_11|
    Then I can see the Login page
      | screenShotName| Reg_Maintenance_0001_Android_12|
    Then I restored my password
