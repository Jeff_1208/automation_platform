package com.welab.automation.projects.channel.steps.mobile;


import com.welab.automation.framework.utils.ImageComparison;
import com.welab.automation.framework.utils.comparison.model.ImageComparisonResult;
import com.welab.automation.projects.channel.pages.CommonPage;
import com.welab.automation.projects.channel.pages.LoginPage;
import com.welab.automation.projects.channel.pages.MaintenancePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;
import lombok.SneakyThrows;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.welab.automation.framework.GlobalVar.TARGET_SCREENSHOTS_PATH;
import static com.welab.automation.framework.utils.comparison.model.ImageComparisonState.MATCH;
import static org.assertj.core.api.Assertions.assertThat;

public class MaintenanceSteps {

    MaintenancePage maintenancePage;
    CommonPage commonPage;
    LoginPage loginPage;
    public MaintenanceSteps(){
        maintenancePage = new MaintenancePage();
        commonPage = new CommonPage();
        loginPage = new LoginPage();
    }
    private static final String[] login_text = {"Login","登入"};
    private static final String[] total_balance_text = {"Total balance","總結餘"};
    private static final String expect_pending_desc =
            "We're working hard to process your application and will inform you via in-app notification within 2 business days.";

    @Then("Navigate to My Accounts")
    public void navigateAccounts(Map<String, String> data) {
        maintenancePage.clickAccountsBtn(data.get("screenShotName"));
    }

    @And("Click on About us")
    public void navigateAboutUsPage(Map<String, String> data) {
        assertThat(maintenancePage.clickAboutUsTitle(data.get("screenShotNameUp"))).isTrue();
        maintenancePage.verifySlidePage(data.get("screenShotNameMiddle"),data.get("screenShotNameDown"));
    }

    @When("Click on Settings")
    public void navigateSettings(Map<String, String> data) {
        maintenancePage.clickSettingTitle(data.get("screenShotName"));
    }

    @Then("Click personal information title")
    public void clickPersonalInformation() {
        maintenancePage.clickPersonalInformation();
    }

    @And("Click on Support")
    public void navigateSupport(Map<String, String> data) {
        maintenancePage.clickSupportTitles(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
//        List<String> screenShotNameList = new ArrayList<>();
//        for (int i = 0; i < data.values().size() ; i++) {
//            if ( i != 0) {
//                screenShotNameList.add(data.get("screenShotName" + i ));
//            }else
//                screenShotNameList.add(data.get("screenShotName" ));
//        }
//        maintenancePage.clickSupportTitle(screenShotNameList);
//        ImageComparison imageComparison =
//                new ImageComparison(
//                        TARGET_SCREENSHOTS_PATH + screenShotNameList.get(1) + ".png",
//                        TARGET_SCREENSHOTS_PATH + screenShotNameList.get(2) + ".png")
//                        .setAllowingPercentOfDifferentPixels(10);
//        File file = new File("target/screenshots/" + screenShotNameList.get(2) + ".png");
//        ImageComparisonResult imageComparisonResult =
//                imageComparison.compareImages().writeResultTo(file);
//        assertThat(imageComparison.getActual()).isNotNull();
//        assertThat(imageComparison.getExpected()).isNotNull();
//        Allure.step("compare result is match");
//        assertThat(imageComparisonResult.getImageComparisonState()).isEqualTo(MATCH);
    }

    @And("I click phone,email,bank url  and ig")
    @SneakyThrows
    public void verifyPhoneEmailBankUrlIg(Map<String, String> data) {
        String weLabPhoneNum = maintenancePage.clickVerifyPhoneLink(data.get("screenShotName"));
        Thread.sleep(2*1000);
        assertThat(weLabPhoneNum).isNotEmpty();
        Thread.sleep(2*1000);
        assertThat(maintenancePage.getEmailLink().getText()).isNotEmpty();
        maintenancePage.clickVerifyBankUrl(data.get("screenShotName1"));
        Thread.sleep(2*1000);
        assertThat(maintenancePage.getInstagramLink().getText()).isNotEmpty();
    }

    @And("I click other prompts or notices and verify them")
    public void verifyOtherNotices(Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 0; i < data.values().size() ; i++) {
            if ( i != 0) {
                screenShotNameList.add(data.get("screenShotName" + i ));
            }else
                screenShotNameList.add(data.get("screenShotName" ));
        }
        maintenancePage.clickVerifyAllNotices(screenShotNameList);
    }

    @Then("through adb common input MSK password")
    public void inputMSKPassword(Map<String, String> data) {
        maintenancePage.adbSendMsk(data.get("screenShotName"));
    }

    @Then("Modify  the employment details")
    public void modifyEmployment(Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 0; i < data.values().size() ; i++) {
            if ( i != 0) {
                screenShotNameList.add(data.get("screenShotName" + i ));
            }else
                screenShotNameList.add(data.get("screenShotName" ));
        }
        maintenancePage.modifyPersonalInformation(screenShotNameList);
    }

    @Then("Verify that the modification is successful")
    public void verifyEmployment(Map<String, String> data){
        maintenancePage.verifyEmploymentList(data.get("screenShotName"));
    }

    @Then("Modify nationality or region information ([^\"][\\S\\s]*) and ([^\"]\\S*)$")
    public void modifyNationOrRegion(String nationalityName,String taxNumber,Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 0; i < data.values().size() ; i++) {
            if ( i != 0) {
                screenShotNameList.add(data.get("screenShotName" + i ));
            }else
                screenShotNameList.add(data.get("screenShotName" ));
        }
        maintenancePage.modifyInfoNationOrRegion(nationalityName,taxNumber,screenShotNameList);
        maintenancePage.verifyInfoNationOrRegion(taxNumber,screenShotNameList);
    }
    @Then("I clicked the change password button")
    public void passwordButton(Map<String, String> data) {
        maintenancePage.clickChangePassword(data.get("screenShotName" ));
    }
    @When("I entered my old password")
    public void enteredPassword(Map<String, String> data) {
        maintenancePage.inputTextPassword(data.get("screenShotName" ));
    }
    @Then("Click next")
    public void clickNext(Map<String, String> data) {
        maintenancePage.clickNext(data.get("screenShotName" ));
    }
    @Then("Enter new password")
    public void newPassword(Map<String, String> data) {
        maintenancePage.inputNewPassword(data.get("screenShotName" ));
    }
    @Then("^Click Done")
    public void doneButton(Map<String, String> data) {
        maintenancePage.clickDone(data.get("screenShotName" ));
    }
    @Then("input Key")
    public void inputKey(Map<String, String> data) {
        maintenancePage.inputKey( data.get("screenShotName"));
    }
    @Then("I clicked the Settings")
    public void clickedSettings(Map<String, String> data) {
        maintenancePage.clickSettings(data.get("screenShotName" ));
    }
    @Then("I clicked the log out")
    public void clickLogOut(Map<String, String> data) {
        maintenancePage.clickLogOut(data.get("screenShotName" ));
    }
    @Then("Login again")
    public void LoginAgain(Map<String, String> data) {
        loginPage.LoginAgain(data);
    }
    @Then("I can see the Login page")
    public void verifyOnLoginPage(Map<String, String> data) throws InterruptedException {
        String checkedTxt = loginPage.getLoginPageText(data);
        assertThat(commonPage.oneOfArrayItemInStr(checkedTxt,total_balance_text)).isTrue();
    }
    @Then("I clicked the Change mobile security key button")
    public void clickedChangeMobileKey(Map<String, String> data) {
        maintenancePage.changeMobileKey(data.get("screenShotName" ));
    }
    @Then("I enterI enter input MSK key")
    public void enterKey(Map<String, String> data) {
        maintenancePage.enterKey(data.get("screenShotName" ));
    }
    @Then("I enter new key")
    public void enterNewKey(Map<String, String> data) {
        maintenancePage.enterNewKey(data);
    }

    @Then("I restored my key")
    public void restoredKey() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("key","123456");
        data.put("pwd","234567");
        maintenancePage.changeMobileKey(data.get("screenShotName" ));
        maintenancePage.enterKey(data.get("screenShotName" ));
        maintenancePage.enterNewKey(data);
    }
    @Then("I restored my password")
    public void restoredPassword() {
        Map<String, String> data = new HashMap<String, String>();
        maintenancePage.clickAccountsBtn(data.get("screenShotName"));
        maintenancePage.clickSettingTitle(data.get("screenShotName1"));
        maintenancePage.clickChangePassword(data.get("screenShotName" ));
        maintenancePage.inputPassword(data.get("screenShotName" ));
        maintenancePage.clickNext(data.get("screenShotName" ));
        maintenancePage.NewPassword(data.get("screenShotName" ));
        maintenancePage.clickDone(data.get("screenShotName" ));
        maintenancePage.inputKey(data.get("screenShotName"));
    }
    @Then("I clicked the trading limit button")
    public void clickedTrading(Map<String, String> data) {
        boolean str=maintenancePage.getAmountText(data.get("amount"));
        if (!str) {
            maintenancePage.dailyTransfer(data.get("screenShotName"));
            maintenancePage.sendKeysEnterAmount(data.get("screenShotName1"), data.get("amount"));
            maintenancePage.setDone(data.get("screenShotName2"));

            commonPage.waiteSendMsk();
//            maintenancePage.confirm(data.get("screenShotName4"));
//            maintenancePage.inputKey( data.get("screenShotName5"));
        }
    }
    @Then("I entered the maximum amount")
    public void enteredAmount(Map<String, String> data) {
        maintenancePage.sendKeysEnterAmount(data.get("screenShotName" ),data.get("amount" ));

    }
    @Then("I Click the submit button")
    public void clicksubmit(Map<String, String> data) {
        maintenancePage.setDone(data.get("screenShotName" ));
        maintenancePage.confirm(data.get("screenShotName1" ));
    }
    @Then("Verify whether the modified limit is changed")
    public void VerifyModified(Map<String, String> data) {
        maintenancePage.getAmountText(data.get("amount"));
    }
    @Then("I clicked on my personal information")
    public void personalInformation(Map<String, String> data) {
        maintenancePage.clickDailyTransferLimitransfer(data.get("screenShotName" ));
    }
    @Then("I clicked the personal address modification")
    public void addressModification(Map<String, String> data) {
        maintenancePage.clickUpdateAddress(data.get("screenShotName" ));
    }

    @Then("change password")
    public void changePassword(Map<String, String> data) {
        maintenancePage.changePassword(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }
    @Then("change password back")
    public void changePasswordBack(Map<String, String> data) {
        maintenancePage.changePasswordBack(data.get("screenShotName"));
    }
    @Then("go to my Account Page")
    public void goToMyAccountPage(Map<String, String> data) {
        maintenancePage.goToMyAccountPage(data.get("screenShotName" ));
    }
    @Then("go to my Setting Page")
    public void goToSettingPage() {
        maintenancePage.goToSettingPage();
    }
    @Then("go to personal Page")
    public void goToPersonalPage() {
        maintenancePage.goToPersonalPage();
    }
    @Then("change MSK")
    public void changeMsk(Map<String, String> data) {
        maintenancePage.changeMsk(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }
    @Then("change MSK back")
    public void changeMskBack(Map<String, String> data) {
        maintenancePage.changeMskBack(data.get("screenShotName"));
    }
    @Then("update daily amount limit ([^\"]\\S*)$")
    public void updateDailyAmountLimit(String limitAmout,Map<String, String> data) {
        maintenancePage.updateDailyAmountLimit(limitAmout,data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }

    @Then("update daily amount limit with message ([^\"]\\S*)$")
    public void updateDailyAmountLimitWithMessage(String limitAmout,Map<String, String> data) {
        maintenancePage.updateDailyAmountLimitWithMessage(limitAmout,data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }

    @Then("Click marketing preferences title")
    public void marketingPreferences(Map<String, String> data) {
        maintenancePage.clickMarketingPreferencesTitle(data.get("screenShotName"));
    }

    @Then("Trigger Marketing Preferences flow and check notification")
    public void triggerMarketingPreferences(Map<String, String> data) {
//        maintenancePage.changeNotificationOptions(data.get("screenShotName"));
        maintenancePage.changeNotification(data.get("screenShotName"),data.get("screenShotName01"));
    }

    @Then("test support")
    public void testSupport(Map<String, String> data) {
        maintenancePage.testSupport(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }

    @Then("about us")
    public void aboutUs(Map<String, String> data) {
        maintenancePage.aboutUs(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"));
    }
    @Then("test Marketing Preferences")
    public void testMarketingPreferences(Map<String, String> data) {
        maintenancePage.testMarketingPreferences(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"));
    }

    @Then("Modify press through all the options ([^\"][\\S\\s]*) and ([^\"][\\S\\s]*)$")
    public void modifyAllReasons(String nationalityName,String reason,Map<String, String> data) {
        maintenancePage.ModifyPressThroughAllTheOptions(nationalityName, reason, data);
    }

    @Then("update address")
    public void updateAddress(Map<String, String> data) {
        maintenancePage.updateAddress(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName5"));
    }

    @Then("update email address")
    public void updateEmailAddress(Map<String, String> data) {
        maintenancePage.updateEmailAddress(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName5"));
    }

    @Then("update work infomation")
    public void updateWorkInfomation(Map<String, String> data) {
        maintenancePage.updateWorkInfomation(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"));
    }

    @Then("open Work Info Page For MCV")
    public void openWorkInfoPageForMCV(Map<String, String> data) {
        maintenancePage.openWorkInfoPageForMCV(data.get("screenShotName"));
    }

    @Then("update work information for mcv")
    public void updateWorkInfomationForMCV(Map<String, String> data) {
        maintenancePage.updateWorkInfomationForMCV(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"));
    }
    @Then("update nation infomation")
    public void updateNationInfomation(Map<String, String> data) {
        maintenancePage.updateNationInfomation(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"));
    }
    @Then("tax jurisdiction checking")
    public void taxJurisdictionChecking(Map<String, String> data) {
        maintenancePage.taxJurisdictionChecking(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName4"));
    }

    @Then("I update Residential Address")
    public void iUpdateResidentialAddress(Map<String, String> data) {
        assertThat(maintenancePage.updateResidentialAddress(data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }

    @Then("I update Email Address")
    public void iUpdateEmailAddress(Map<String, String> data) {
        assertThat(maintenancePage.verifyUpdateEmailAddress(data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }

    @Then("I change msk back")
    public void iChangeMskBack(Map<String, String> data) {
        maintenancePage.changeMSKPasswordBack(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("Verify rebind device daily limit")
    public void verifyRebindDeviceDailyLimit(Map<String, String> data) {
        assertThat(maintenancePage.verifyRebindDeviceDailyLimit(data.get("screenShotName"))).isTrue();
    }

    @Then("I delete Nationality")
    public void iDeleteNationality() {
        maintenancePage.deleteNationality();
    }

    @Then("update Account Opening Information IOS")
    public void updateAccountOpeningInformationIOS(Map<String, String> data) {
        maintenancePage.updateAccountOpeningInformationIOS(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("update For Open Account IOS")
    public void updateForOpenAccount(Map<String, String> data) {
        maintenancePage.updateForOpenAccount(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"));
    }

    @Then("click Account Opening Edit Button For MCV")
    public void clickAccountOpeningEditButtonForMCV(Map<String, String> data) {
        maintenancePage.clickAccountOpeningEditButtonForMCV(data.get("screenShotName"));
    }
    @Then("Update Account opening information Android")
    public void updateAccountOpeningInformationAndroid(Map<String, String> data) {
        maintenancePage.updateAccountOpeningInformationAndroid(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("I verify Account opening information")
    public void iVerifyAccountOpeningInformation(Map<String, String> data) {
        maintenancePage.verifyAccountOpeningInformation(data.get("screenShotName"));
    }

    @Then("click Address Edit Button for MCV")
    public void clickAddressEditButtonForMcv() {
        maintenancePage.clickAddressEditButtonForMcv();
    }

    @Then("update Residential Building For Mcv")
    public void updateResidentialBuildingForMcv(Map<String, String> data) {
        maintenancePage.updateResidentialBuildingForMcv(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("update Email Building For Mcv")
    public void updateEmailBuildingForMcv(Map<String, String> data) {
        maintenancePage.updateEmailBuildingForMcv(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("I update Residential Address for AOS MCV")
    public void iUpdateResidentialAddressForAOSMCV(Map<String, String> data) {
        assertThat(maintenancePage.updateResidentialAddressForAOSMCV(data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }

    @Then("I update Email Address for AOS MCV")
    public void iUpdateEmailAddressForAOSMCV(Map<String, String> data) {
        assertThat(maintenancePage.updateEmailAddressForAOSMCV(data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }

    @Then("Update Account opening information AOS MCV")
    public void updateAccountOpeningInformationAndroidMCV(Map<String, String> data) {
        maintenancePage.updateAccountOpeningInformationAOSMCV(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("Modify nationalityOrRegion for MCV")
    public void modifyNationalityForMCV(Map<String, String> data) {
        maintenancePage.modifyNationalityForMCV(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("nationalityName"), data.get("screenShotName2"),data.get("screenShotName3"));
    }

}
