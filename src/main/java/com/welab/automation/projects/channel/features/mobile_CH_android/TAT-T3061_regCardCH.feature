Feature: Reg Card

  Background: Login
    Given Open Stage WeLab App
    Given Check upgrade page appears Stage
    When Login with user and password from properties
    Then I can see the LoggedIn page

  @VCAT-63 @health
  Scenario:  Reg_Card_001 Under WeLab Debit Card section, Input MSK PIN to show card details
    And I verify Total balance and Available balance
      | screenShotName  | Reg_Card_001_Android_01 |
    And I click the debit card
      | screenShotName2 | Reg_Card_001_Android_02 |
    And I click Card details and verify details
      | screenShotName3 | Reg_Card_001_Android_03 |
      | screenShotName4 | Reg_Card_001_Android_04 |
    Then get card information
      | screenShotName | Reg_Card_001_Android_05 |


  @VCAT-64
  Scenario Outline: Reg_Card_002 Set card limit - daily ATM withdrawal limit
    And I verify Total balance and Available balance
      | screenShotName  | Reg_Card_02_Android_01 |
    And I click the debit card
      | screenShotName2 | Reg_Card_002_Android_02 |
    And I click Daily card transaction limits
      | screenShotName3  | Reg_Card_002_Android_03 |
    And I change transaction limit and withdrawal limit set <transactionLimit> and <withdrawalLimit>
      | screenShotName4  | Reg_Card_002_Android_04 |
      | screenShotName5  | Reg_Card_002_Android_05 |
      | screenShotName6  | Reg_Card_002_Android_06 |
    Examples:
      |transactionLimit |withdrawalLimit|
      |      1000       |     1000      |

  @health
  Scenario Outline:  Reg_Card_003 Android Mastercard transaction 1. Online under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName  | Reg_Card_003_Android_01 |
      | screenShotName1 | Reg_Card_003_Android_02 |
    Then go to debit card
      | screenShotName  | Reg_Card_003_Android_03 |
      | screenShotName1 | Reg_Card_003_Android_04 |
    Then set limit large android
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_003_Android_03 |
      | screenShotName1 | Reg_Card_003_Android_04 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_003_Android_05 |
      | screenShotName1 | Reg_Card_003_Android_06 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability | Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10033   | 344     | 6                  | 10033   | 344      | 810           | -133      | 133      |

  Scenario Outline:  Reg_Card_004 Android Mastercard transaction 2. POS under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName  | Reg_Card_004_Android_01 |
      | screenShotName1 | Reg_Card_004_Android_02 |
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_004_Android_03 |
      | screenShotName1 | Reg_Card_004_Android_04 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_004_Android_05 |
      | screenShotName1 | Reg_Card_004_Android_06 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability | Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10044   | 344     | 6                  | 10044   | 344      | 032           | -144      | 144      |

  @VCAT-67
  Scenario: Reg_Card_006 Report card lost and notification
    And I verify Total balance and Available balance
      | screenShotName  | Reg_Card_006_Android_01 |
    And I click the debit card
      | screenShotName2 | Reg_Card_006_Android_02 |
    And I get debit card number
    And I click Lost card
      | screenShotName3 | Reg_Card_006_Android_03 |
    And I click Confirm button
    And I verify report card lost successfully
      | screenShotName4 | Reg_Card_006_Android_04 |
