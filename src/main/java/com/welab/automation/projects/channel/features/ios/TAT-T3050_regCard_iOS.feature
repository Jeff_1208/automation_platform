Feature: Reg Card

  Background: Login
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage
    When Login with user and password from properties
    Then I can see the LoggedIn page

  @ACBN-T237  @health
  Scenario:  Reg_Card_001 IOS Under WeLab Debi Card section, Input MSK PIN to show card details
    Then go to debit card
      | screenShotName | Reg_Card_001_IOS_01 |
      | screenShotName1 | Reg_Card_001_IOS_02 |
    Then click card info and input msk
      | screenShotName | Reg_Card_001_IOS_03|
      | screenShotName1 | Reg_Card_001_IOS_04|
    Then get card information
      | screenShotName | Reg_Card_001_IOS_05 |

  @ACBN-T238
  Scenario Outline:  Reg_Card_002 IOS Set card limit - daily ATM withdrawal limit
    Then go to debit card
      | screenShotName | Reg_Card_002_IOS_01 |
      | screenShotName1 | Reg_Card_002_IOS_02 |
    Then set limit
      | screenShotName | Reg_Card_002_IOS_03 |
    When run gps request with error <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check notification
      | screenShotName | Reg_Card_002_IOS_04 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability  | Txn_Amt | Bill_Ccy | POS_Data_DE22 |
      | -1009    | 344     | 6                   | 1009    | 344      | 810           |

  @health @ACBN-T675
  Scenario Outline:  Reg_Card_003 IOS Mastercard transaction 1. Online under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName | Reg_Card_003_IOS_01 |
      | screenShotName1 | Reg_Card_003_IOS_02 |
    Then go to debit card
      | screenShotName | Reg_Card_003_IOS_03 |
      | screenShotName1 | Reg_Card_003_IOS_04 |
    Then set limit large
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName | Reg_Card_003_IOS_03 |
      | screenShotName1 | Reg_Card_003_IOS_04 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName | Reg_Card_003_IOS_05 |
      | screenShotName1 | Reg_Card_003_IOS_06 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability | Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10003   | 344     | 6                  | 10003    | 344      | 810          | -103      | 103      |

  @ACBN-T677
  Scenario Outline:  Reg_Card_004 IOS Mastercard transaction 2. POS under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName | Reg_Card_004_IOS_01 |
      | screenShotName1 | Reg_Card_004_IOS_02 |
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName | Reg_Card_004_IOS_03 |
      | screenShotName1 | Reg_Card_004_IOS_04 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName | Reg_Card_004_IOS_05 |
      | screenShotName1 | Reg_Card_004_IOS_06 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability |  Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10004   | 344     | 6                  |  10004   | 344      | 032           | -104      | 104      |

  @ACBN-T334
  Scenario:  Reg_Card_005 IOS Reset Card PIN Flow
    Then go to debit card
      | screenShotName  | Reg_Card_005_IOS_01 |
      | screenShotName1 | Reg_Card_005_IOS_02 |
    Then reset card pin flow
      | screenShotName  | Reg_Card_005_IOS_03 |
      | screenShotName1 | Reg_Card_005_IOS_04 |

  @ACBN-T330
  Scenario:  Reg_Card_008 IOS Insights page information display
    Then go to debit card
      | screenShotName  | Reg_Card_008_IOS_01 |
      | screenShotName1 | Reg_Card_008_IOS_02 |
    Then Insights page information display IOS
      | screenShotName  | Reg_Card_008_IOS_03 |
      | screenShotName1 | Reg_Card_008_IOS_04 |

  @ACBN-T332
  Scenario:  Reg_Card_006 IOS Report card lost and notification
    Then go to debit card
      | screenShotName | Reg_Card_006_IOS_01 |
      | screenShotName1 | Reg_Card_006_IOS_02 |
    Then report card lost and notification
      | screenShotName | Reg_Card_006_IOS_03|
      | screenShotName1 | Reg_Card_006_IOS_04|

  @ACBN-T2371
  Scenario:  Reg_Card_001 IOS Under WeLab Debi Card section, Input MSK PIN to show card details1
    Then go to debit card
      | screenShotName | Reg_Card_001_IOS_01 |
      | screenShotName1 | Reg_Card_001_IOS_02 |
    Then click card info and input msk
      | screenShotName | Reg_Card_001_IOS_03|
      | screenShotName1 | Reg_Card_001_IOS_04|
    Then get card information
      | screenShotName | Reg_Card_001_IOS_05 |

  @ACBN-T333
  Scenario:  Reg_Card_007 IOS Begin Debit card activation flow
    Then go to debit card
      | screenShotName | Reg_Card_007_IOS_01 |
      | screenShotName1 | Reg_Card_007_IOS_02 |
    Then debit card activation
      | screenShotName | Reg_Card_007_IOS_03|
      | screenShotName1 | Reg_Card_007_IOS_04|
      | screenShotName2 | Reg_Card_007_IOS_05|
      | screenShotName3 | Reg_Card_007_IOS_06|
      | screenShotName4 | Reg_Card_007_IOS_07|
      | screenShotName5 | Reg_Card_007_IOS_08|
      | screenShotName6 | Reg_Card_007_IOS_09|
