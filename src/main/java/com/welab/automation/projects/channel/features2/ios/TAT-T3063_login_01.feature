Feature: Welab app login

  Background: Open App
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage

  @health
  Scenario:  Reg_Login_001 IOS Normal login (username + password)
    Then get app version
      | screenShotName | Reg_Login_001_IOS_01 |
    When Login with user and password from properties
    Then I can see the LoggedIn page
    Then get screenShot
      | screenShotName | Reg_Login_001_IOS_02 |