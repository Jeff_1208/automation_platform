package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.GoSavePage;
import com.welab.automation.projects.channel.pages.MessagePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class FpsSteps {

    MessagePage messagePage;
    public FpsSteps(){
        messagePage = new MessagePage();
    }

    @When("Open Message App")
    public void openAndroidMessageApp(){
        messagePage.openAndroidMessage();
    }
    @And("Get Message Content")
    public void getMessageContent(){
        messagePage.getMessageContent();
    }

}
