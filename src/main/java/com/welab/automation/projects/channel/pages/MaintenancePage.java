package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.utils.ADB;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.welab.automation.framework.utils.Utils.logPass;
import static org.assertj.core.api.Assertions.assertThat;


public class MaintenancePage extends AppiumBasePage {
    private String pageName = "About Us Page";
    private String receiverAll = "Great, we'll be sending you marketing updates via Email, Phone Call, SMS and Push Notification.";
    private String receiverAllCH = "我哋會透過電郵、電話、短訊及推送通知將推廣資訊傳送畀你。";
    private String noLongerReceiverAll = "You'll no longer receive our marketing updates via Email, Phone Call, SMS and Push Notification.";
    private String noLongerReceiverAllCH = "你唔會再透過電郵、電話、短訊及推送通知收到我哋嘅推廣資訊。";
    private int forwardOkX;
    private int afterOkX;

    CommonPage commonPage;
    GoSavePage goSavePage;
    public MaintenancePage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        goSavePage = new GoSavePage();
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Total balance (HKD)']/../../../preceding-sibling::android.view.ViewGroup/android.view.ViewGroup[4]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '總結餘 (HKD)']/../../../preceding-sibling::android.view.ViewGroup/android.view.ViewGroup[4]")
    })
    private MobileElement accountsBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'About us']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '關於我們']")
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"關於我們\"]")
    private MobileElement aboutUsTitle;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'WeLab Bank is here to show you how.')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text ,'WeLab Bank 都會為大家實現')]")
    })
    private MobileElement aboutUsPageTips;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[@text = 'Made with love in Hong Kong']"
    )
    private MobileElement aboutUsBottomMost;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'WeLab Bank')]/following-sibling::android.view.ViewGroup[3]/android.widget.TextView"
    )
    private MobileElement phoneNumLink;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'WeLab Bank')]/following-sibling::android.view.ViewGroup[5]/android.widget.TextView"
    )
    private MobileElement emailLink;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'WeLab Bank')]/following-sibling::android.view.ViewGroup[7]/android.widget.TextView"
    )
    private MobileElement bankUrlLink;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'WeLab Bank')]/following-sibling::android.view.ViewGroup[9]/android.widget.TextView"
    )
    private MobileElement instagramLink;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.EditText[@resource-id='com.android.dialer:id/digits']"
    )
    private MobileElement phoneNumber;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.TextView[@resource-id='com.google.android.gm:id/welcome_tour_title']"
    )
    private MobileElement welcomeEmailText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.View[contains(@text,'Beware of bogus calls')]"),
            @AndroidBy(xpath = "//android.view.View[contains(@text,'請提防偽冒銀行發出的來電')]")
    })
    private MobileElement bankUrlPageText;

    @Getter
    @AndroidFindBy(
            xpath = "//android.view.View[@resource-id='__next']/android.view.View[2]/android.view.View[1]"
    )
    private MobileElement weLabLogoText;

    @AndroidFindBy(
            xpath = "//android.view.View[@resource-id = '__next']/../../../../android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"
    )
    private MobileElement xBtn;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement outerConfirm;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Confirm']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '確認']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '確認修改']")
    })
    @iOSXCUITFindBy(xpath = "//*[@name=\"確認\"]")
    private MobileElement confirmBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name=\"確認\"]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确认\"])[2]"),
    })
    private MobileElement confirmAttentionBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text ='Open source acknowledgements']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '開源碼致謝名單']")
    })
    private MobileElement openSourceLink;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.EditText[@resource-id=\"com.android.chrome:id/url_bar\"]"
    )
    private MobileElement insUrl;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Version')]/../../android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    })
    private List<MobileElement> aboutUsNotice;

    @AndroidFindBy(
            xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/" +
                    "android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView\n" )
    private MobileElement noticePageTitle;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement licenseTitle;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text ='Support']/../../android.view.ViewGroup"),
            @AndroidBy( xpath = "//android.widget.TextView[@text ='協助']/../../android.view.ViewGroup")
    })
    private MobileElement supportTitle;

    @AndroidFindBy(
            xpath = "//android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.View[1]/android.view.View[2]"
    )
    private MobileElement supportCenter;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.view.View[@content-desc=\"Why doesn't WeLab Bank have branches?\"]"),
            @AndroidBy( xpath = "//android.view.View[@content-desc=\"點解WeLab Bank冇實體分行？\"]"),
            @AndroidBy( xpath = "//android.view.View[@text=\"Why doesn't WeLab Bank have branches?\"]"),
            @AndroidBy( xpath = "//android.view.View[@text=\"點解WeLab Bank冇實體分行？\"]"),
    })
    private MobileElement weLabBankFirstQuestion;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[contains(@text,'our digital branch')]"),
            @AndroidBy( xpath = "//android.widget.TextView[contains(@text,'數碼分行')]"),
            @AndroidBy( xpath = "//*[contains(@text,'our digital branch')]"),
            @AndroidBy( xpath = "//*[contains(@text,'數碼分行')]")
    })
    private MobileElement branchPageAssert;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup"
    )
    private MobileElement leftArrowBtn;

    @AndroidFindBy(
            xpath = "//android.view.View[@text = 'View all\uF061']"
    )
    private List<MobileElement> viewAllLinks;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup"
    )
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"設定\"]")
    private MobileElement settingTitle;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement intoSettingTitle;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement intoPersonInformationTitle;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement intoEmployTitle;


    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Personal information']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '個人資料']")
    })
    private MobileElement personalInformation;

    @AndroidFindAll({
            @AndroidBy(  xpath = "//android.widget.TextView[@text = 'Mobile security key']" ),
            @AndroidBy(  xpath = "//android.widget.TextView[@text = '流動保安編碼認證']")
    })
    private MobileElement MSKTitle;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Employment information']/following-sibling::android.view.ViewGroup" ),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '工作資料']/following-sibling::android.view.ViewGroup")
    })
    private MobileElement employmentEditBtn;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Nationality / region']/following-sibling::android.view.ViewGroup"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '國籍/地區']/following-sibling::android.view.ViewGroup")
    })
    private MobileElement nationalEditBtn;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Employment information']" ),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '工作資料']")
    })
    private MobileElement employmentTitle;

    @AndroidFindBy(
            xpath = "//android.widget.TextView/following-sibling::android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText"
    )
    private List<MobileElement> intoEmploymentList;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement intoEmploymentTitle;

    @AndroidFindBy(
            xpath = "//android.widget.TextView/following-sibling::android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText"
    )
    private List<MobileElement> modifiedEmploymentList;


    @AndroidFindBy(
            xpath = "//android.widget.ScrollView//android.view.ViewGroup/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"
    )
    private List<MobileElement> allEmployInfoList;


    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = '確認']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'OK']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '好']")
    })
    private MobileElement okBtn;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Nationality / region']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '國籍/地區']")
    })
    private MobileElement nationalTitle;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Edit nationality/region/tax jurisdiction']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '修改國籍/地區/稅務地區']")
    })
    private MobileElement intoNationalTitle;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.EditText"
    )
    private MobileElement searchNationalityEdit;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup[2]/android.widget.EditText"
    )
    private MobileElement searchBtn;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup"
    )
    private List<MobileElement> oldNationalityList;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup[2]/android.widget.EditText/../../android.view.ViewGroup/following-sibling::android.widget.ScrollView//android.widget.TextView"
    )
    private MobileElement searchResult;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text= 'Tax information']/../android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy( xpath = "//android.widget.TextView[@text= '稅務資料']/../android.view.ViewGroup/android.widget.TextView")
    })
    private List<MobileElement> logoText;


    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText"
    )
    private List<MobileElement> taxIdentificationNumberList;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[1]"
    )
    private MobileElement modifiedNaList;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView"
    )
    private List<MobileElement> confirmSuccessNationality;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Tax jurisdiction']/following-sibling::android.widget.TextView"),
            @AndroidBy( xpath = "//android.widget.TextView[@text= '稅務地區']/following-sibling::android.widget.TextView")
    })
    private List<MobileElement> confirmSuccessTaxNumber;


    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Change password']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text='更改密碼']")
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"更改密碼\"]")
    private MobileElement changePassword;

    @AndroidFindBy(
            xpath = "//android.widget.EditText[@content-desc=\"password-input\"]"
    )
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeSecureTextField[@name=\"password-input\"]")
    private MobileElement passwordInput;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Next']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text='確定']")
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]")
    private MobileElement next;

    @AndroidFindBy(xpath = "(//android.widget.EditText[@content-desc=\"password-input\"])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeSecureTextField[@name=\"password-input\"])[1]")
    private MobileElement newPassword;

    @AndroidFindBy(xpath = "(//android.widget.EditText[@content-desc=\"password-input\"])[2]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeSecureTextField[@name=\"password-input\"])[2]")
    private MobileElement newPassword2;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Done']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確定']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確認']")
    })
    private MobileElement done;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Settings']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='設定']")
    })
    private MobileElement Settings;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='My account']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='我的賬戶']")
    })
    private MobileElement myAccountBtn;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Log out']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text='登出']")
    })
    private MobileElement logOut;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Change mobile security key']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text='更改流動保安編碼']")
    })
    private MobileElement changeMobileKey;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Daily transfer limit']"),
//            @AndroidBy( xpath = "//android.widget.TextView[@text='每日交易限額']")
            @AndroidBy( xpath = "//*[contains(@text,'HKD')]")
    })
    private MobileElement dailyTransferLimitransfer;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText"
    )
    private MobileElement enterAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Confirm']"),
            @AndroidBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup[2]/android.widget.TextView")
    })
    private MobileElement confirm;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Daily transfer limit']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='每日交易限額']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView"),
    })
    private MobileElement dailyTransferLimitAmount;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[@text='My account']"
    )
    private MobileElement myAmount;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='Address information']/following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy( xpath = "//android.widget.TextView[@text='更改地址']/following-sibling::android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement updateAddress;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']/following-sibling::android.view.ViewGroup[2]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']"),
//            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText']"),
    })
    private MobileElement estateStreet;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']/following-sibling::android.view.ViewGroup[4]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']"),
//            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.EditText"),
    })
    private MobileElement building;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"請設定你嘅登入密碼，並需符合：\"]")
    private MobileElement newPasswordPage;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[starts-with(@name, '我的賬戶’)])[1]")
    private MobileElement myAccountPageTitle;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[contains(@name, \"每日交易限額\")]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[contains(@name, \"每日交易限额\")]"),
    })
    private MobileElement settingPageTitle;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[starts-with(@name, '個人資料’)])[1]")
    private MobileElement personalDataPageTitle;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Marketing preferences']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '推廣資訊']")
    })
    private MobileElement marketingPreferencesTitle;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'Email received']"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '電郵接收']")
    })
    private MobileElement emailBtn;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"
    )
    private MobileElement intoPreferencesTitle;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup"
    )
    private MobileElement byAllBtn;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]"
    )
    private MobileElement byAllBtnOff;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup/android.view.ViewGroup[7]/android.widget.TextView[2]"
    )
    private MobileElement updatedHintText;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text = 'By all channels']/following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy( xpath = "//android.widget.TextView[@text = '所有途徑接收']/following-sibling::android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> otherUpdateBtnStatus;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"遇到難題？有我哋幫手！\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"歡迎嚟到WeLab Bank 支援中心，請問有咩幫到你？\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"協助\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"协助\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"协助\""),
    })
    private MobileElement supportPageTitle;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"遇到難題？有我哋幫手！\"]")
    private MobileElement supportTitleEle;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"wecare@welab.bank\"]")
    private MobileElement supportEmail;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"+852 3898 6988\"]")
    private MobileElement supportNumber;
    @AndroidFindBy(xpath = "//*[@text='OK']")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"好\"]")
    private MobileElement OkBtn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '你可以喺呢到選擇是否接收推廣資訊及接收嘅方法')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '您可以在此处选择是否接收推广信息及接收方式')]"),
    })
    private MobileElement PromotionalInfomationPageTitle;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "/XCUIElementTypeOther[@name=\"繼續\"]"),
            @iOSXCUITBy(xpath = "/XCUIElementTypeOther[@name=\"继续\"]"),
    })
    private MobileElement continueBtn;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeButton[@name=\"取消\"])[2]")
    private MobileElement cancelBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"現有密碼\" AND name == \"現有密碼\" AND value == \"現有密碼\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"现有密码\" AND name == \"现有密码\" AND value == \"现有密码\""),
    })
    private MobileElement currentPasswordPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"新密碼\" AND name == \"新密碼\" AND value == \"新密碼\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"新密码\"]"),
    })
    private MobileElement newPasswordInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "label == \"再輸入新密碼\" AND name == \"再輸入新密碼\" AND value == \"再輸入新密碼\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"再输入新密码\"]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"再輸入新密碼\"])[3]"),
    })
    private MobileElement newPasswordAgainInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"新嘅流動保安編碼\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"新的流动安全编码\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"新的流动保安编码\" AND name == \"text\""),
    })
    private MobileElement newMskPasswordPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"每日交易限額\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"每日交易限額\"])[4]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"此限額包括你嘅轉數快轉賬及銀行轉賬\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"此限额包括您的转数快转账及银行转账\""),
    })
    private MobileElement dailyAmountLimitPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"最高限額 HKD 600,000.00\"]/../XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"最高限额 HKD 600,000.00\"]/../XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 10,000.00 設為最高限額\"])[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 10,000.00 设为最高限额\"])[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement dailyAmountInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"請輸入不同限額\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"请输入不同限额\"]"),
    })
    private MobileElement pleaseInputDiffLimitLable;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name=\"此金額超出最高限額\"]"),
            @iOSXCUITBy(xpath = "//*[@name=\"此金额超出最高限额\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"綁定手機嘅24小時內，你嘅每日交易限額被限制為最多HKD 10,000.00。請喺轉賬時降低交易金額或遲啲先進行大筆轉賬以避免轉賬被拒絕。\""),
    })
    private MobileElement overLargeAmountLimitMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"確認\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确认\" AND name == \"text\""),
    })
    private MobileElement confirmButtonForUpdateLimit;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name=\"工作資料\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"工作资料\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "//*[@name=\"工作信息\"]"),
    })
    private MobileElement workInfoPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"每月收入\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"每月收入\"])[4]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"月收入\"]"),
    })
    private MobileElement monthAmountSelector;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< HKD 8,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 8,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem1;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"HKD 8,000 - HKD 20,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 8,000 - 港币 20,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem2;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"HKD 20,001 - HKD 40,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 20,001 - 港币 40,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem3;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"HKD 40,001 - HKD 60,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 40,001 - 港币 60,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem4;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"HKD 60,001 - HKD 80,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 60,001 - 港币 80,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem5;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"HKD 80,001 - HKD 100,000\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"< 港币 80,001 - 港币 100,000\"])[1]"),
    })
    private MobileElement monthAmountSelectorItem6;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"修改國籍/地區/稅務地區\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"修改国籍/地区/税务地区\" AND name == \"text\""),
    })
    private MobileElement nationPageTitle;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Return\"]")
    private MobileElement softKeyReturn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"無稅務編碼\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"无税务编号\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"没有税务编号\" AND name == \"text\""),
    })
    private MobileElement noTaxPageTitle;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我居住嘅司法管轄區並不發出稅務編號予其居民\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我居住的司法管辖区并不向其居民发出税务编号\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我居住的司法管辖区不会发出税务编号予其居民\"])[1]"),
    })
    private MobileElement noTaxReasonItem1;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"不需要提供稅務編號\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"不需要提供税务编号\"])[1]"),
    })
    private MobileElement noTaxReasonItem2;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我無法取得稅務編號 【請提供原因】\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我无法取得税务编号 【请提供原因】\"])[2]"),
    })
    private MobileElement noTaxReasonItem3;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Enter your mobile security key']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '請輸入6位數字嘅流動保安編碼。']"),
    })
    private MobileElement enterKey;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Forgot mobile security key?']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '忘記流動保安編碼？']"),
    })
    private MobileElement forgotKey;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    private MobileElement backBtn;

    @iOSXCUITFindBy(xpath = "//*[@text=\"國籍/地區\"]")
    private MobileElement countryArea;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='English residential address']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement englishResidentialAddress;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Mailing address']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement englishMailAddress;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.EditText"),
    })
    private MobileElement mailingAddressStreet;
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'includes alphanumeric')]")
    })
    private MobileElement setNewPasswordText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Mailing address']/following-sibling::android.view.ViewGroup[5]/android.widget.EditText"),
    })
    private MobileElement mailingBuilding;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']/following-sibling::android.view.ViewGroup[5]/android.widget.EditText"),
    })
    private MobileElement estateStreetMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address']/following-sibling::android.view.ViewGroup[7]/android.widget.EditText"),
    })
    private MobileElement buildingMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Address information']/following-sibling::android.widget.TextView[11]")
    })
    private MobileElement addressInformation;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Delivery address']/following-sibling::android.view.ViewGroup[6]/android.widget.EditText"),
    })
    private MobileElement deliveryAddressStreetMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.EditText"),
    })
    private MobileElement streetMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.EditText"),
    })
    private MobileElement buildMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Mailing address']/following-sibling::android.widget.TextView[12]")
    })
    private MobileElement mailAddressStreetMCV;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Reasons to open an account in Hong Kong']/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> ReasonInfoList;

    @SneakyThrows
    public void clickAccountsBtn(String screenShotName) {
        Thread.sleep(5*1000);
        waitUntilElementClickable(accountsBtn);
        clickElement(accountsBtn);
        Thread.sleep(2*1000);
        waitUntilElementClickable(aboutUsTitle);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public boolean clickAboutUsTitle(String screenShotNameUp) {
        waitUntilElementClickable(aboutUsTitle);
        clickElement(aboutUsTitle);
        waitUntilElementClickable(aboutUsPageTips);
        takeScreenshot(screenShotNameUp);
        Thread.sleep(2000);
        return verifyElementExist(aboutUsPageTips);
    }

    @SneakyThrows
    public void verifySlidePage(String screenShotNameMiddle,String screenShotNameDown) {
        scrollUp();
        takeScreenshot(screenShotNameMiddle);
        scrollUpToFindElement(aboutUsBottomMost,5,2);
        assertThat(aboutUsBottomMost.getText()).isEqualTo("Made with love in Hong Kong");
        Thread.sleep(5*1000);
        takeScreenshot(screenShotNameDown);
        scrollDownToFindElement(aboutUsPageTips,5,2);
        Thread.sleep(2*1000);
    }

    @SneakyThrows
    public String clickVerifyPhoneLink(String screenShotName) {
        waitUntilElementClickable(phoneNumLink);
        String weLabPhoneNumber = phoneNumLink.getText();
        takeScreenshot(screenShotName);
        return weLabPhoneNumber;
    }

    @SneakyThrows
    public void clickVerifyBankUrl(String screenShotName1) {
        Thread.sleep(2000);
        terminateApp("com.google.android.gm");
        activeAndriodApp("welab.bank.mobile.stage");
        waitUntilElementClickable(bankUrlLink);
        clickElement(bankUrlLink);
        Thread.sleep(5000);
        takeScreenshot(screenShotName1);
        Thread.sleep(2000);
//        clickElement(xBtn);
        clickElement(backBtn);
    }


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"I don't have a TIN\"]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"I don't have a TIN\"]/../android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '我没有稅務編碼']")
    })
    private List<MobileElement> notTinBtn;

    @AndroidFindBy(
            xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"
    )
    private List<MobileElement> reasonListBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"Other (please specify)\"]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '其他 【請提供原因】']"),
    })
    private MobileElement otherReasonBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Other (please specify)']/following-sibling::android.view.ViewGroup[1]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='其他 【請提供原因】']/following-sibling::android.view.ViewGroup[1]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup[2]/android.widget.EditText"),
    })
    private MobileElement otherReasonInputText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Tax jurisdiction']/preceding-sibling::android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '稅務地區']/preceding-sibling::android.widget.TextView"),
    })
    private List<MobileElement> confirmNationality;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"修改地址\" AND name == \"text\"")
    private MobileElement udpateAddressPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名稱\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名稱\"])[2]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名称\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名称\"])[2]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement streetNameInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名稱\"])[8]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名稱\"])[6]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名称\"])[8]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"屋苑 / 街道名称\"])[6]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement emailStreetNameInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認修改\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定修改\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确定修改\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确认修改\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@value=\"確定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@value=\"确定\"]"),
    })
    private MobileElement confirmUpdateBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[3]"),
    })
    private MobileElement confirmButtonForSourceOfIncome;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"收入来源\"])[4]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"收入來源\"])[4]"),
    })
    private MobileElement sourceOfIncome;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"其他\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"其他\"])[2]"),
    })
    private MobileElement otherItem;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Add tax jurisdiction']"),
            @AndroidBy(xpath = "//*[@text='税務地區']"),
    })
    private MobileElement addTaxJurisdiction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='The jurisdiction does not issue TINs to its residents']")
    })
    private MobileElement reason1;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='TIN is not required by the local authority']")
    })
    private MobileElement reason2;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='I am unable to obtain a TIN (Please elaborate)']")
    })
    private MobileElement reason3;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[starts-with(@text,'Please don')]"),
    })
    private MobileElement exceedMaximum;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Haiti']/following-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement deleteHaitiBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='American Samoa']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='American Samoa']/../android.view.ViewGroup[1]")
    })
    private MobileElement deleteAmericanBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Yes']")
    })
    private MobileElement yesBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Account opening information']/following-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement accountOpeningInformationEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Change account opening info']/following-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement changeAccountOpeningInfo;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Purpose of')]/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> PurposeInfoList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Expected monthly incoming funds']/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> inComingInfoList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Expected monthly outgoing')]/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> outGoingInfoList;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Account opening information']/following-sibling::android.widget.TextView")
    })
    private List<MobileElement> modifyAccountList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Change account opening info']/following-sibling::android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")
    })
    private List<MobileElement> intoAccountList;

    @AndroidFindBy(xpath = "//*[@text='Mailing address']/following-sibling::android.view.ViewGroup[3]/android.widget.EditText")
    private MobileElement RedMiMailingAddressStreet;

    @SneakyThrows
    public void clickVerifyAllNotices(List<String> screenShotNameList) {
        logger.info("Show screenShotNameList {}",screenShotNameList);
        Thread.sleep(1000);
        terminateApp("com.android.chrome");
        activeAndriodApp("welab.bank.mobile.stage");
        scrollUpToFindElement(openSourceLink, 3, 2);
        Thread.sleep(2000);
        List<MobileElement> noticeList = aboutUsNotice;
        logger.info("Show noticeList :{}",noticeList.size());
        for (int i = 0 ; i < noticeList.size(); i ++) {
            Thread.sleep(2000);
            waitUntilElementClickable(noticeList.get(i));
            String noticeText = noticeList.get(i).getText();
            Thread.sleep(3000);
            clickElement(noticeList.get(i));
            Thread.sleep(2000);
            if ("Open source acknowledgements".equals(noticeText)) {
                assertThat("licenseAcknowledgement").isEqualToIgnoringCase(licenseTitle.getText().trim());
                Thread.sleep(5000);
                takeScreenshot(screenShotNameList.get(i));
            }else if ("開源碼致謝名單".equals(noticeText)){
                    assertThat("licenseAcknowledgement").isEqualToIgnoringCase(licenseTitle.getText().trim());
                    Thread.sleep(5000);
                    takeScreenshot(screenShotNameList.get(i));
            }else {
                assertThat(noticePageTitle.getText().trim()).isEqualToIgnoringCase(noticeText);
                Thread.sleep(5000);
                takeScreenshot(screenShotNameList.get(i));
//                clickElement(xBtn);
                clickElement(backBtn);
                Thread.sleep(3000);
            }
        }
    }

    @SneakyThrows
    public void clickSupportTitle(List<String> screenShotName) {
        Thread.sleep(2*1000);
        clickElement(supportTitle);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName.get(0));
        assertThat(verifyElementExist(weLabBankFirstQuestion)).isTrue();
        for (int i=1;i<20;i++){
            scrollUp();
        }
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName.get(1));
        scrollUp();
        Thread.sleep(3*1000);
        takeScreenshot(screenShotName.get(2));
        Thread.sleep(3*1000);
        clickElement(supportTitle);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName.get(3));
        Thread.sleep(3*1000);

//        Thread.sleep(2*1000);
//        clickElement(supportTitle);
//        waitUntilElementClickable(supportCenter);
//        takeScreenshot(screenShotName.get(0));
//        Thread.sleep(5*1000);
//        takeScreenshot(screenShotName.get(1));
//        clickElement(weLabBankFirstQuestion);
//        assertThat(branchPageAssert.getText()).isNotEmpty();
//        Thread.sleep(5*1000);
//        takeScreenshot(screenShotName.get(2));
//        //clickElement(leftArrowBtn);
//        clickElement(xBtn);
//        Thread.sleep(2*1000);
//        clickElement(supportTitle);
//        Thread.sleep(5*1000);
//        takeScreenshot(screenShotName.get(3));
//        Thread.sleep(3*1000);
//        for (int i = 0; i < 20; i++) {
//            scrollUp();
//        }
//        Thread.sleep(5*1000);
//        takeScreenshot(screenShotName.get(4));
//        scrollUp();
//        Thread.sleep(3*1000);
//        takeScreenshot(screenShotName.get(5));
//        Thread.sleep(3*1000);
//        clickElement(xBtn);
//        Thread.sleep(3*1000);
//        takeScreenshot(screenShotName.get(6));
//        Thread.sleep(3*1000);
    }

    @SneakyThrows
    public void clickSettingTitle(String screenShotName) {
        Thread.sleep(3 * 1000);
        waitUntilElementClickable(Settings);
        clickElement(Settings);
        Thread.sleep(3 * 1000);
        assertThat(intoSettingTitle.getText()).isNotEmpty();
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void clickPersonalInformation() {
        Thread.sleep(2 * 1000);
        if(isSamsungA3460()){
            scrollUp();
            scrollUp();
            scrollUp();
        }else{
            scrollUpToFindElement(personalInformation, 3, 1);
        }
        clickElement(personalInformation);
    }

    @SneakyThrows
    public void adbSendMsk(String screenShotName) {
        Thread.sleep(5 * 1000);
        waitUntilElementVisible(enterKey);
        takeScreenshot(screenShotName);
        Thread.sleep(5*1000);
        commonPage.waiteSendMsk();
        Thread.sleep(2 * 1000);
    }

    @SneakyThrows
    public void modifyPersonalInformation(List<String> screenShotName) {
        Thread.sleep(5 * 1000);
        assertThat(intoPersonInformationTitle.getText()).isNotEmpty();
        scrollUp();
        scrollUpToFindElement(employmentTitle, 3, 1);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName.get(0));
        clickElement(employmentEditBtn);
        waitUntilElementVisible(intoEmploymentTitle);
        Thread.sleep(2 * 1000);
        assertThat(intoEmployTitle.getText()).isNotEmpty();
        takeScreenshot(screenShotName.get(1));
        clickElement(intoEmploymentList.get(0));
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName.get(2));
        randomClickListElement(allEmployInfoList, 1);

        logger.info("intoEmploymentList Size is {}", intoEmploymentList.size());

        if (intoEmploymentList.size() <= 3) {
            clickElement(intoEmploymentList.get(1));
            Thread.sleep(3 * 1000);
            takeScreenshot(screenShotName.get(3));
            randomClickListElement(allEmployInfoList, 1);
            Thread.sleep(2 * 1000);
            logger.info("intoEmploymentList Size is {}", intoEmploymentList.size());
            if (intoEmploymentList.size() <= 3) {
                Thread.sleep(3 * 1000);
                takeScreenshot(screenShotName.get(4));
                clickElement(intoEmploymentList.get(2));
            } else {
                Thread.sleep(2 * 1000);
                clickElement(intoEmploymentList.get(2));
                takeScreenshot(screenShotName.get(4));
                randomClickListElement(allEmployInfoList, 4);
                Thread.sleep(3 * 1000);
                takeScreenshot(screenShotName.get(5));
                clickElement(okBtn);
                clickElement(intoEmploymentList.get(3));
                takeScreenshot(screenShotName.get(6));
            }
            randomClickListElement(allEmployInfoList, 3);

        } else {
            for (int i = 1; i < intoEmploymentList.size(); i++) {
                clickElement(intoEmploymentList.get(i));
                Thread.sleep(2 * 1000);
                switch (i) {
                    case 1:
                        takeScreenshot(screenShotName.get(3));
                        randomClickListElement(allEmployInfoList, 1);
                        break;
                    case 2:
                        takeScreenshot(screenShotName.get(4));
                        randomClickListElement(allEmployInfoList, 1);
                        Thread.sleep(5*1000);
                        if (intoEmploymentList.size() > 5) {
                            clickElement(intoEmploymentList.get(3));
                            takeScreenshot(screenShotName.get(5));
                            randomClickListElement(allEmployInfoList, 1);
                            clickElement(intoEmploymentList.get(4));
                            takeScreenshot(screenShotName.get(6));
                            randomClickListElement(allEmployInfoList, 1);
                            clickElement(intoEmploymentList.get(5));
                            takeScreenshot(screenShotName.get(7));
                            randomClickListElement(allEmployInfoList, 1);
                        } else {
                            clickElement(intoEmploymentList.get(3));
                            takeScreenshot(screenShotName.get(5));
                            randomClickListElement(allEmployInfoList, 1);
                            clickElement(intoEmploymentList.get(4));
                            takeScreenshot(screenShotName.get(6));
                            randomClickListElement(allEmployInfoList, 4);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        Thread.sleep(2 * 1000);
        clickElement(okBtn);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName.get(7));
    }

    public void randomClickListElement(List<MobileElement> randomList, int number) {
        if (number >= 2) {
            int randomOne = new Random().nextInt(randomList.size());
            clickElement(randomList.get(randomOne));
            int randomTwo = 0;
            while (number > 0) {
                randomTwo = new Random().nextInt(randomList.size());
                if (randomTwo != randomOne) {
                    number--;
                }
            }
            clickElement(randomList.get(randomTwo));
        } else {
            int randomOne = new Random().nextInt(randomList.size());
            clickElement(randomList.get(randomOne));
        }
    }

    @SneakyThrows
    public void verifyEmploymentList(String screenShotName) {
        logger.info("intoEmploymentList Size is {}", intoEmploymentList.size());
        clickElement(confirmBtn);
        Thread.sleep(10 * 1000);
        waitUntilElementVisible(employmentTitle);
        for (int i = 0; i < intoEmploymentList.size(); i++) {
            assertThat(modifiedEmploymentList.get(i*2+1).getText()).isEqualTo(intoEmploymentList.get(i).getText());
        }
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void modifyInfoNationOrRegion(String nationalityName, String taxNumber,List<String> screenShotName) {
        String[] nationalityValues = nationalityName.split(",", 10);
        String[] taxNumberValues = taxNumber.split(",", 10);
        clickNationAndDeleted(screenShotName);
        int circleSearchTime = 0;
        Thread.sleep(3*1000);
        if (nationalityValues.length > 0) {
            for (int i = 0; i < nationalityValues.length; i++) {
                logger.info("nationalityValues.length: {}",nationalityValues.length);
                andSendKeys(searchBtn, nationalityValues[i]);
                Thread.sleep(2 * 1000);
                commonPage.androidInputEnter();
                clickElement(searchResult);
                clickElement(okBtn);
                Thread.sleep(2 * 1000);
                if (nationalityValues[i].equals("Hong Kong SAR") || nationalityValues[i].equals("香港特別行政區")){
                    clickElement(searchNationalityEdit);
                    Thread.sleep(2 * 1000);
                    clickElement(searchBtn);
                    circleSearchTime++;
                    continue;
                }else {
//                    scrollUp();
                    if ((i == 1 && nationalityValues[i-1].equals("Hong Kong SAR")) || (i == 1 && nationalityValues[i-1].equals("香港特別行政區"))) {
                        andSendKeys(taxIdentificationNumberList.get(1), taxNumberValues[i]);
                        commonPage.androidInputEnter();
                        clickElement(searchNationalityEdit);
                        Thread.sleep(2 * 1000);
                        clickElement(searchBtn);
                        circleSearchTime++;
                        continue;
                    }
                    if (i >= 2 && taxIdentificationNumberList.size() == 2) {
                        clearInput(taxIdentificationNumberList.get(i-1));
                        andSendKeys(taxIdentificationNumberList.get(i-1), taxNumberValues[i]);
                        commonPage.androidInputEnter();
                        clickElement(logoText.get(i + 1));
                    }else if (i >= 2 && taxIdentificationNumberList.size() == 3) {
                        andSendKeys(taxIdentificationNumberList.get(i), taxNumberValues[i]);
                        commonPage.androidInputEnter();
                        clickElement(logoText.get(i + 1));
                    } else {
                        clearInput(taxIdentificationNumberList.get(i+1));
                        andSendKeys(taxIdentificationNumberList.get(i+1), taxNumberValues[i]);
                        commonPage.androidInputEnter();
                    }
                }
                if (circleSearchTime < nationalityValues.length - 1) {
                    clickElement(searchNationalityEdit);
                    Thread.sleep(2 * 1000);
                    clickElement(searchBtn);
                    circleSearchTime++;
                }
            }
        }
    }

    @SneakyThrows
    public void verifyInfoNationOrRegion(String taxNumber,List<String> screenShotName) {
        Thread.sleep(3*1000);
        scrollDown();
        String[] modifyNationalityList = modifiedNaList.getText().split(";");
        scrollUp();
        clickElement(outerConfirm);
        Thread.sleep(6 * 1000);
        waitUntilElementVisible(nationalTitle);
        scrollUp();
        Thread.sleep(2*1000);
        scrollUp();
        Thread.sleep(2*1000);
        for (int i = 0; i < modifyNationalityList.length; i++) {
            assertThat(confirmSuccessNationality.get(i+1).getText().trim()).isEqualTo(modifyNationalityList[i].trim());
            assertThat(confirmSuccessTaxNumber.get(i).getText().trim()).isEqualTo(modifyNationalityList[i].trim());
        }
        takeScreenshot(screenShotName.get(3));
    }

    @SneakyThrows
    public void clickChangePassword(String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(changePassword);
        Thread.sleep(2*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void inputTextPassword(String screenShotName) {
        Thread.sleep(2*1000);
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        andSendKeys(passwordInput,password);
        inputEnter(passwordInput);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void inputPassword(String screenShotName) {
        Thread.sleep(2*1000);
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        andSendKeys(passwordInput,password);
        inputEnter(passwordInput);
        Thread.sleep(2*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void clickNext(String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(next);
        Thread.sleep(2*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void inputNewPassword(String screenShotName) {
        Thread.sleep(5*1000);
        clickElement(newPassword);
        Thread.sleep(5*1000);
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        andSendKeys(newPassword,password);
        Thread.sleep(2*1000);
        inputEnter(newPassword);
        Thread.sleep(2*1000);
        clickElement(newPassword2);
        scrollUp();
        Thread.sleep(2*1000);
        clickElement(setNewPasswordText);
        andSendKeys(newPassword2,password);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
        Thread.sleep(5*1000);
    }

    public void hideAndroidKeyboard(){
        try {
            if (System.getProperty("mobile").contains("android")) hideKeyboard();
        }catch (Exception e){
        }
    }

    @SneakyThrows
    public void NewPassword(String screenShotName) {
        Thread.sleep(5*1000);
        clickElement(newPassword);
        Thread.sleep(5*1000);
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        andSendKeys(newPassword,password);
        Thread.sleep(2*1000);
        hideAndroidKeyboard();
        Thread.sleep(2*1000);
        clickElement(newPassword2);
        scrollDown();
        Thread.sleep(2*1000);
        clickElement(setNewPasswordText);
        andSendKeys(newPassword2,password);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
//        hideAndroidKeyboard();
        Thread.sleep(5*1000);
    }
    @SneakyThrows
    public void clickDone(String screenShotName) {
        Thread.sleep(2*1000);
        if(isShow(done,2)){
            clickElement(done);
        }
        Thread.sleep(5*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void inputKey(String screenShotName) {
        waitUntilElementVisible(MSKTitle);
        Thread.sleep(5*1000);
        commonPage.waiteSendMsk();
        Thread.sleep(5*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void clickSettings(String screenShotName) {
        Thread.sleep(10*1000);
        clickElement(Settings);
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void clickLogOut(String screenShotName) {
        Thread.sleep(5*1000);
        clickElement(logOut);
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void changeMobileKey(String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(changeMobileKey);
        Thread.sleep(2*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public void enterKey (String screenShotName) {
        Thread.sleep(2*1000);
        Thread.sleep(5*1000);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
        commonPage.waiteSendMsk();
        Thread.sleep(5*1000);
    }
    @SneakyThrows
    public void enterNewKey(Map<String, String> data) {
        Thread.sleep(2*1000);
        commonPage.enterNewMsk();
        Thread.sleep(5*1000);
        if (data.get("screenShotName1")!=null) {
            takeScreenshot(data.get("screenShotName1"));
        }
        Thread.sleep(5*1000);
        Thread.sleep(5*1000);
        if (data.get("screenShotName2")!=null) {
            takeScreenshot(data.get("screenShotName2"));
        }
    }
    public boolean ElementExist(By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    @SneakyThrows
    public void dailyTransfer (String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(dailyTransferLimitransfer);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void sendKeysEnterAmount (String screenShotName,String money) {
        Thread.sleep(2*1000);
        clickElement(enterAmount);
        Thread.sleep(2*1000);
        clearAndSendKeys(enterAmount,money);
        inputEnter(enterAmount);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void setDone (String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(done);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }
    @SneakyThrows
    public void confirm (String screenShotName) {
        Thread.sleep(2*1000);
        if (verifyElementExist(confirm)) {
            clickElement(confirm);
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName);
        }
    }
    @SneakyThrows
    public boolean  getAmountText (String expect) {
        boolean flag=false;
        Thread.sleep(5*1000);
        String str= getElementText(dailyTransferLimitAmount);
        str = str.substring(0,str.indexOf("."));
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.replaceAll("").trim().equals(expect)){
             flag=true;
        }
        return flag;
    }

    @SneakyThrows
    public void clickDailyTransferLimitransfer (String screenShotName) {
        Thread.sleep(2*1000);
        scrollUpToFindElement(personalInformation, 3, 1);
        clickElement(personalInformation);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
        Thread.sleep(2*1000);
        sendMsk();
    }
    @SneakyThrows
    public void clickUpdateAddress(String screenShotName) {
        Thread.sleep(3*1000);
        clickElement(updateAddress);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }

    public void clickMyAccountBtn(){
        clickByLocationByPercent(93,7);
    }

    public void clickSetting(){
        if(isIphoneSE()){
            //clickByLocationByPercent(20,51); //iphone se
            clickByPicture2("src/main/resources/images/channel/maintenance/settingItem.png", 50, 20);  //iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(20,46); //iphone 7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,39);
        }else if(isIphone13()){
            clickByPicture2("src/main/resources/images/channel/maintenance/settingItem.png", 50, 20);  //iphone se
        }else{
            clickByPicture2("src/main/resources/images/channel/maintenance/settingItem.png", 50, 20);  //iphone se
        }
    }

    public void clickChangePasswordItem(){
        //clickByLocationByPercent(20,59);
        clickByPicture2("src/main/resources/images/channel/maintenance/updatePassword.png", 50, 30);
    }

    public void clickPasswordInput(){
        clickElement(currentPasswordPage);
    }

    public void clickNewPasswordInput(){
        clickElement(newPasswordInput);
    }
    public void clickNewPasswordInput2(){
        clickElement(newPasswordAgainInput);
    }

    @SneakyThrows
    public void goToMyAccountPage(String screenShotName){
        clickMyAccountBtn();
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void goToSettingPage(){
        clickSetting();
        Thread.sleep(10000);
        waitUntilElementVisible(settingPageTitle);
    }

    @SneakyThrows
    public void goToPersonalPage(){
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        clickPersonalItem();
        if(!isShow(commonPage.MSKpage, 3))   {
            clickPersonalItem();
        }
        Thread.sleep(3000);
        commonPage.inputMSKByString("123456");
        Thread.sleep(8000);
    }

    public void clickPersonalItem(){
        if(isIphoneSE()){
            clickByPicture2("src/main/resources/images/channel/maintenance/PersonalItem.png",50,15);  //iphone se
        }else if(isIphone7Plus()){
            clickByPicture2("src/main/resources/images/channel/maintenance/PersonalItem.png",50,50);  //iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,75.5);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,74.1);
        }else{
            clickByPicture2("src/main/resources/images/channel/maintenance/PersonalItem.png",50,15);  //iphone se
        }
    }

    @SneakyThrows
    public void changePassword(String screenShotName1,String screenShotName2,String screenShotName3){
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        String newPassword=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        clickChangePasswordItem();
        Thread.sleep(2000);
        inputOldPassword(password,screenShotName1);
        inputIosNewPassword(newPassword,screenShotName2);
        commonPage.waitMskPage();
        commonPage.inputMSKByString("123456");
        waitUntilElementVisible(settingPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void changePasswordBack(String screenShotName){
        String platform =  System.getProperty("mobile");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        String newPasswordValue=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        Thread.sleep(5000);
        clickChangePasswordItem();
        Thread.sleep(2000);
        clickPasswordInput();
        Thread.sleep(1000);
        andSendKeys(passwordInput,newPasswordValue);
        inputEnter(passwordInput);
        clickElement(next);
        waitUntilElementVisible(newPasswordInput);

        clickNewPasswordInput();
        Thread.sleep(1000);
        andSendKeys(newPassword,password);
        inputEnter(newPassword);
        Thread.sleep(1000);

        clickNewPasswordInput2();
        andSendKeys(newPassword2,password);
        Thread.sleep(1000);
        clickElement(next);
        Thread.sleep(2000);
        commonPage.waitMskPage();
        commonPage.inputMSKByString("123456");
        waitUntilElementVisible(settingPageTitle);
        Thread.sleep(200);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void inputOldPassword(String password,String screenShotName){
        clickPasswordInput();
        Thread.sleep(1000);
        andSendKeys(passwordInput,password);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        inputEnter(passwordInput);
        Thread.sleep(1000);
        clickElement(next);
    }

    @SneakyThrows
    public void inputIosNewPassword(String password,String screenShotName){
        waitUntilElementVisible(newPasswordInput);
        clickNewPasswordInput();
        Thread.sleep(1000);
        andSendKeys(newPassword,password);
        inputEnter(newPassword);
        Thread.sleep(1000);
        clickNewPasswordInput2();
        andSendKeys(newPassword2,password);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickElement(next);
        Thread.sleep(2000);
    }

    public void clickUpdateMskItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(20,77);  //iphone se
        }else if(isIphone7Plus()){
            clickByPicture2("src/main/resources/images/channel/maintenance/updateMsk.png", 50, 50);  //iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,58.9);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,62.5);
        }else{
            clickByPicture2("src/main/resources/images/channel/maintenance/updateMsk.png", 50, 50);
        }
    }

    public void clickDailyAmoutLimitItem(){
        if(isIphoneSE()){
            clickByPicture("src/main/resources/images/channel/maintenance/dailyLimit2.png", 50, 50);//iphone se
        }else if(isIphone7Plus()){
            clickByPicture2("src/main/resources/images/channel/maintenance/dailyLimit2.png", 50, 15);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,34.5);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,36.6);
        }else{
            clickByPicture("src/main/resources/images/channel/maintenance/dailyLimit2.png", 50, 50);//iphone se
        }
    }

    @SneakyThrows
    public void changeMsk(String screenShotName1,String screenShotName2,String screenShotName3){
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        String mskNewPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskNewPassword");
        clickUpdateMskItem();
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
        commonPage.inputMSKByString(mskPassword);
        waitUntilElementVisible(newMskPasswordPage);
        commonPage.inputMSKByString(mskNewPassword);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        commonPage.inputMSKByString(mskNewPassword);
        Thread.sleep(5000);
        if(!isIphone13()) {
            waitUntilElementVisible(cancelBtn);
            clickElement(cancelBtn);
            waitUntilElementVisible(continueBtn);
            clickElement(continueBtn);
        }
        waitUntilElementVisible(settingPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void changeMskBack(String screenShotName){
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        String mskNewPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskNewPassword");

        Thread.sleep(8000);
        scrollDown();
        Thread.sleep(1000);
        clickUpdateMskItem();
        Thread.sleep(3000);
        commonPage.inputMSKByString(mskNewPassword);
        waitUntilElementVisible(newMskPasswordPage);
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(2000);
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(5000);
        if(!isIphone13()) {
            waitUntilElementVisible(cancelBtn);
            clickElement(cancelBtn);
            waitUntilElementVisible(continueBtn);
            clickElement(continueBtn);
        }
        waitUntilElementVisible(settingPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkIsSameValue(String limitAmout){
        if(isShow(pleaseInputDiffLimitLable, 3)){
            int amount = Integer.parseInt(limitAmout);
            int newAmount = amount + (int) (Math.random() * 100);
            commonPage.inputDeleteByNumber(6);
            andSendKeys(dailyAmountInput, newAmount+"");
            Thread.sleep(1000);
        }
    }

    @SneakyThrows
    public void updateDailyAmountLimit(String limitAmout,String screenShotName1,String screenShotName2,String screenShotName3){
        updateLimitAmoutInputSteps(limitAmout,screenShotName1,screenShotName2);
//        if(isShow(pleaseAttention,3)){
//            clickElement(confirmAttentionBtn);
//            Thread.sleep(1000);
//        }
        checkIsSameValue(limitAmout);
        if(isShow(confirmAttentionBtn,3)){
            clickElement(confirmAttentionBtn);
            Thread.sleep(1000);
        }
        commonPage.waitMskPage();
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(1000);
        waitUntilElementVisible(settingPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void updateLimitAmoutInputSteps(String limitAmout,String screenShotName1,String screenShotName2){
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickDailyAmoutLimitItem();
        Thread.sleep(3000);
        waitUntilElementVisible(dailyAmountLimitPage);
        clearInput(dailyAmountInput);
        Thread.sleep(1000);
        andSendKeys(dailyAmountInput, limitAmout);
        Thread.sleep(1000);
        //waitUntilElementVisible(overLargeAmountLimitMessage,10);
        takeScreenshot(screenShotName2);
        //if(isShow(overLargeAmountLimitMessage,3)) {
        //    Thread.sleep(1000);
        // takeScreenshot(screenShotName2);
        //}

//        if (isShow(pleaseInputDiffLimitLable, 2)) {
//            commonPage.inputDeleteByNumber(6);
//            limitAmout = ((int) (Math.random() * 10000) + 480000) + "";
//            andSendKeys(dailyAmountInput, limitAmout);
//            Thread.sleep(1000);
//        }
//        Thread.sleep(1000);
//        takeScreenshot(screenShotName2);
//        clickElement(confirmBtn);
//        Thread.sleep(1000);
    }

    @SneakyThrows
    public void updateLimitAmoutInputStepsWithMessage(String limitAmout,String screenShotName1,String screenShotName2){
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickDailyAmoutLimitItem();
        Thread.sleep(3000);
        waitUntilElementVisible(dailyAmountLimitPage);
        clearAndSendKeys(dailyAmountInput, limitAmout);
        Thread.sleep(1000);
        if(isShow(overLargeAmountLimitMessage,10)){
            takeScreenshot(screenShotName2);
        }else{
            clickElement(confirmButtonForUpdateLimit);
            waitUntilElementVisible(overLargeAmountLimitMessage,10);
            takeScreenshot(screenShotName2);
        }

    }

    @SneakyThrows
    public void updateDailyAmountLimitWithMessage(String limitAmout,String screenShotName1,String screenShotName2,String screenShotName3) {
        updateLimitAmoutInputStepsWithMessage(limitAmout,screenShotName1,screenShotName2);
        Thread.sleep(2000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void clickMarketingPreferencesTitle(String screenShotName) {
        Thread.sleep(3*1000);
        scrollUp();
        clickElement(marketingPreferencesTitle);
//        waitUntilElementVisible(emailBtn);
//        assertThat(intoPreferencesTitle.getText()).isEqualTo(marketingPreferencesTitle.getText());
        takeScreenshot(screenShotName);
    }


    @SneakyThrows
    public void changeNotification(String screenShotName,String screenShotName01) {
        Thread.sleep(3*1000);
        for (MobileElement updateBtnStatus : otherUpdateBtnStatus) {
            clickElement(updateBtnStatus);
            waitOkBtnAndClick();
        }
        takeScreenshot(screenShotName);
        for (MobileElement updateBtnStatus : otherUpdateBtnStatus) {
            clickElement(updateBtnStatus);
            waitOkBtnAndClick();
        }
        takeScreenshot(screenShotName01);
}


    @SneakyThrows
    public void changeNotificationOptions(String screenShotName) {
        Thread.sleep(3*1000);
        if (verifyElementExist(byAllBtn)) {
            if (System.getProperty("language").contains("zh")) {
                clickByAll(noLongerReceiverAllCH);
            }else
                clickByAll(noLongerReceiverAll);
        }
        forwardOkX = otherUpdateBtnStatus.get(0).getLocation().x;
        if (System.getProperty("language").contains("zh")) {
            clickByAll(receiverAllCH);
        }else{
            clickByAll(receiverAll);
        }
        afterOkX = otherUpdateBtnStatus.get(0).getLocation().x;
        assertionsAllCoordinate(forwardOkX);
        if (System.getProperty("language").contains("zh")) {
            clickByAll(noLongerReceiverAllCH);
        }else
            clickByAll(noLongerReceiverAll);
        assertionsAllCoordinate(afterOkX);
        for (int i = 0; i < otherUpdateBtnStatus.size(); i++) {
            switch (i) {
                case 0:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電郵", "電郵接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Email", "onEmailBtn");
                    break;
                case 1:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電話", "電話接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Phone Call", "onPhoneBtn");
                    break;
                case 2:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "短訊", "短訊接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "SMS", "onSMSBtn");
                    break;
                case 3:
                    if (System.getProperty("language").contains("zh")) {
                        clickSingle(otherUpdateBtnStatus.get(i), "推送通知", "推送通知");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Push Notification", "onPushBtn");
                    break;
            }
        }
        if (System.getProperty("language").contains("zh")) {
            clickByAll(noLongerReceiverAllCH);
        }else
            clickByAll(noLongerReceiverAll);
        assertionsAllCoordinate(afterOkX);
        for (int i = 0; i < otherUpdateBtnStatus.size(); i= i+2) {
            switch (i) {
                case 0:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電郵", "電郵接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Email", "onEmailBtn");
                    break;
                case 2:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "短訊", "短訊接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "SMS", "onSMSBtn");
                    break;
            }
        }
        if (System.getProperty("language").contains("zh")) {
            clickByAll(noLongerReceiverAllCH);
        }else
            clickByAll(noLongerReceiverAll);
        assertionsAllCoordinate(afterOkX);
        for (int i = 1; i < otherUpdateBtnStatus.size(); i= i+2) {
            switch (i) {
                case 1:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電話", "電話接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Phone Call", "onPhoneBtn");
                    break;
                case 3:
                    if (System.getProperty("language").contains("zh")) {
                        clickSingle(otherUpdateBtnStatus.get(i), "推送通知", "推送通知");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Push Notification", "onPushBtn");
                    break;
            }
        }
        if (System.getProperty("language").contains("zh")) {
            clickByAll(noLongerReceiverAllCH);
        }else
            clickByAll(noLongerReceiverAll);
        assertionsAllCoordinate(afterOkX);
        for (int i = 0; i < otherUpdateBtnStatus.size() ; i++) {
            switch (i) {
                case 0:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電郵", "電郵接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Email", "onEmailBtn");
                    break;
                case 1:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電話", "電話接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Phone Call", "onPhoneBtn");
                    break;
                case 2:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "短訊", "短訊接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "SMS", "onSMSBtn");
                    break;
                case 3:
                    if (System.getProperty("language").contains("zh")) {
                        clickSingle(otherUpdateBtnStatus.get(i), "推送通知", "推送通知");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Push Notification", "onPushBtn");
                    break;
            }
        }
        for (int i = otherUpdateBtnStatus.size() - 1; i >= 0 ; i--) {
            switch (i) {
                case 3:
                    if (System.getProperty("language").contains("zh")) {
                        clickSingle(otherUpdateBtnStatus.get(i), "推送通知", "关闭推送通知");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Push Notification", "offPushBtn");
                    break;
                case 2:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "短訊", "关闭短訊接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "SMS", "offSMSBtn");
                    break;
                case 1:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電話", "关闭電話接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Phone Call", "offPhoneBtn");
                    break;
                case 0:
                    if (System.getProperty("language").contains("zh")){
                        clickSingle(otherUpdateBtnStatus.get(i), "電郵", "关闭電郵接收");
                    }else
                        clickSingle(otherUpdateBtnStatus.get(i), "Email", "offEmailBtn");
                    break;
            }
        }
        takeScreenshot(screenShotName);
    }

    public void clickByAll(String assertionsText) {
        if (verifyElementExist(byAllBtn)) {
            clickElement(byAllBtn);
        }else {
            clickElement(byAllBtnOff);
        }
        waitUntilElementVisible(updatedHintText);
        assertThat(updatedHintText.getText()).isEqualTo(assertionsText);
        clickElement(okBtn);
    }

    @SneakyThrows
    public void clickSingle(MobileElement singleBtn,String responseText,String assertionsText) {
        String onSingleBtn = "Great, we'll be sending you marketing updates via " + responseText + '.';
        String onSingleBtnCH = "我哋會透過" + responseText + "將推廣資訊傳送畀你。";
        String onSinglePushBtn = "Great, we'll be sending you marketing updates " + responseText + '.';
        String offSingleBtn = "You'll no longer receive our marketing updates via " + responseText + '.';
        String offSingleBtnCH = "你唔會再透過" + responseText + "收到我哋嘅推廣資訊。";
        clickElement(singleBtn);
        switch (assertionsText) {
            case "onEmailBtn":
            case "onPhoneBtn":
            case "onSMSBtn":
                assertThat(updatedHintText.getText()).isEqualTo(onSingleBtn);
                clickElement(okBtn);
                Thread.sleep(1000);
                assertionsCoordinate(singleBtn,forwardOkX);
                break;
            case "onPushBtn":
                assertThat(updatedHintText.getText()).isEqualTo(onSinglePushBtn);
                clickElement(okBtn);
                Thread.sleep(1000);
                assertionsCoordinate(singleBtn,forwardOkX);
                break;
            case "offEmailBtn":
            case "offPhoneBtn":
            case "offSMSBtn":
            case "offPushBtn":
                assertThat(updatedHintText.getText()).isEqualTo(offSingleBtn);
                clickElement(okBtn);
                Thread.sleep(2*1000);
                assertionsCoordinate(singleBtn,afterOkX);
                break;
            case "電郵接收":
            case "電話接收":
            case "短訊接收":
            case "推送通知":
                assertThat(updatedHintText.getText()).isEqualTo(onSingleBtnCH);
                clickElement(okBtn);
                Thread.sleep(1000);
                assertionsCoordinate(singleBtn,forwardOkX);
                break;
            case "关闭電郵接收":
            case "关闭電話接收":
            case "关闭短訊接收":
            case "关闭推送通知":
                assertThat(updatedHintText.getText()).isEqualTo(offSingleBtnCH);
                clickElement(okBtn);
                Thread.sleep(2*1000);
                assertionsCoordinate(singleBtn,afterOkX);
                break;
        }
    }


    @SneakyThrows
    public void assertionsAllCoordinate(int coordinateNum) {
        for (MobileElement updateBtnStatus : otherUpdateBtnStatus) {
            Thread.sleep(1000);
            int x = updateBtnStatus.getLocation().x;
            if (x > coordinateNum) {
                assertThat(x - coordinateNum >= 50).isTrue();
            }else
                assertThat(x - coordinateNum <= -50).isTrue();

        }
    }

    @SneakyThrows
    public void assertionsCoordinate(MobileElement singleBtn,int coordinateNum) {
        Thread.sleep(2*1000);
        int x = singleBtn.getLocation().x;
        if (x > coordinateNum) {
            assertThat(x - coordinateNum >= 50).isTrue();
        }else
            assertThat(x - coordinateNum <= -50).isTrue();
    }

    @SneakyThrows
    public void selectNoTinRegionForSamsung(String nationalityName,String reason,List<String> screenShotName){
        String[] nationalityValues = nationalityName.split(",", 10);
        clickNationAndDeleted(screenShotName);
        clickElement(searchNationalityEdit);
        waitUntilElementVisible(okBtn);
        clickElement(searchBtn);
        andSendKeys(searchBtn, nationalityValues[0]);
        inputEnter(searchBtn);
        clickElement(searchResult);
        Thread.sleep(2 * 1000);
        clickElement(okBtn);

        logger.info("notTinBtn.size(): "+notTinBtn.size());
        Thread.sleep(5 * 1000);
        clickElement(notTinBtn.get(0));
        Thread.sleep(200);
        clickElement(reason1);
        clickElement(confirmBtn);

        Thread.sleep(2000);
        clickElement(confirmBtn);
        Thread.sleep(5000);
        takeScreenshot(screenShotName.get(3));
    }

    @SneakyThrows
    public void ModifyPressThroughAllTheOptions(String nationalityName,String reason,Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 0; i < data.values().size(); i++) {
            if (i != 0) {
                screenShotNameList.add(data.get("screenShotName" + i));
            } else
                screenShotNameList.add(data.get("screenShotName"));
        }
        String reasonAct = reason + (int) (Math.random() * 100);

        if(isSamsungA3460()){
            selectNoTinRegionForSamsung(nationalityName, reasonAct, screenShotNameList);
        }else{
            selectNoTinRegion(nationalityName, reasonAct, screenShotNameList);
            verifyNoTinRegion(nationalityName, screenShotNameList);
        }
    }

    @SneakyThrows
    public void selectNoTinRegion(String nationalityName,String reason,List<String> screenShotName) {
        String[] nationalityValues = nationalityName.split(",", 10);
        clickNationAndDeleted(screenShotName);
        for (int i = 0; i < nationalityValues.length; i++) {
            clickElement(searchNationalityEdit);
            waitUntilElementVisible(okBtn);
            clickElement(searchBtn);
            andSendKeys(searchBtn, nationalityValues[i]);
            inputEnter(searchBtn);
            clickElement(searchResult);
            Thread.sleep(2 * 1000);
            clickElement(okBtn);
        }
        logger.info("notTinBtn.size(): "+notTinBtn.size());
        Thread.sleep(5 * 1000);
        clickElement(notTinBtn.get(0));
        Thread.sleep(200);
        clickElement(reason1);
        clickElement(confirmBtn);

        Thread.sleep(5 * 1000);
        clickElement(notTinBtn.get(1));
        clickElement(reason2);
        Thread.sleep(2 * 1000);
        clickElement(confirmBtn);

        Thread.sleep(5 * 1000);
        scrollUp();
        Thread.sleep(5 * 1000);
        if(isOnePlus()){
            clickElement(notTinBtn.get(2));
        }else{
            clickElement(notTinBtn.get(1));
        }
        clickElement(reason3);
        clickElement(otherReasonBtn);
        clickElement(otherReasonInputText);
        andSendKeys(otherReasonInputText, reason);
        clickElement(confirmBtn);
        Thread.sleep(5000);
        clickElement(confirmBtn);

        Thread.sleep(5000);
        takeScreenshot(screenShotName.get(3));
    }

    @SneakyThrows
    public void verifyNoTinRegion(String nationalityName,List<String> screenShotName) {
        String[] nationalityValues = nationalityName.split(",", 10);
        List<String>  nationalityValuesList = new ArrayList<>();
        for (String s : nationalityValues) {
            nationalityValuesList.add(s.trim());
        }
        Thread.sleep(3*1000);
        Thread.sleep(2*1000);
        scrollUp();
        for (int i = 0; i < nationalityValuesList.size(); i++) {
            assertThat(nationalityValuesList.get(i).equals(confirmSuccessTaxNumber.get(i).getText().trim())).isTrue();
        }
        takeScreenshot(screenShotName.get(4));
    }


    @SneakyThrows
    public void clickNationAndDeleted(List<String> screenShotName) {
        Thread.sleep(4 * 1000);
        assertThat(intoPersonInformationTitle.getText()).isNotEmpty();
        takeScreenshot(screenShotName.get(0));
        scrollUpToFindElement(nationalTitle, 3, 1);
        scrollUp();
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName.get(1));
        Thread.sleep(4 * 1000);
        clickElement(nationalEditBtn);
        Thread.sleep(3 * 1000);
        assertThat(intoNationalTitle.getText()).isNotEmpty();
        takeScreenshot(screenShotName.get(2));
        clickElement(searchNationalityEdit);
        waitUntilElementVisible(okBtn);
        clickElement(searchBtn);
        logger.info("oldNationalityList.size() : {}", oldNationalityList.size());
        int oldNationalityNum = oldNationalityList.size();
        if (oldNationalityNum > 1) {
            for (int i = 1; i < oldNationalityNum; i++) {
                Thread.sleep(500);
                clickElement(oldNationalityList.get(oldNationalityNum - i));
                logger.info("oldNationalityList.size() : {} {}", oldNationalityList.size(), i);
            }
        }
//        clickElement(okBtn);
        Thread.sleep(3*1000);
//        clickElement(searchNationalityEdit);
//        waitUntilElementVisible(okBtn);
//        clickElement(searchBtn);
    }

    public void clickSupportItem(){
        if(isIphoneSE()){
            clickByPicture("src/main/resources/images/channel/maintenance/support.png", 50, 50);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(20,69);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,58);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,63.3);
        }else{
            clickByPicture("src/main/resources/images/channel/maintenance/support.png", 50, 50);
        }
    }

    @SneakyThrows
    public void testSupport(String screenShotName1,String screenShotName2,String screenShotName3){
        Thread.sleep(1000);
        clickSupportItem();
        Thread.sleep(5000);
        waitUntilElementVisible(supportPageTitle);
        //waitUntilElementVisible(supportEmail);
        //waitUntilElementVisible(supportNumber);
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        for (int i = 0; i < 10; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
    }

    public void clickPromotionalInfomationItem(){
        if(isIphoneSE()){
            clickByPicture2("src/main/resources/images/channel/maintenance/Marketing.png", 50, 10);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(20,55);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,59.7);
        }else if(isIphone13()){
            clickByPicture2("src/main/resources/images/channel/maintenance/Marketing.png", 50, 10);
        }else{
            clickByPicture2("src/main/resources/images/channel/maintenance/Marketing.png", 50, 10);
        }
    }

    public void clickPromotionalInfomationItemEmailReceive(){
        if(isIphoneSE()){
            clickByLocationByPercent(88,51);//iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(88,45);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(88,39.2);
        }else if(isIphone13()){
            clickByLocationByPercent2(88,41);
        }else{
            clickByLocationByPercent(88,51);//iphone se
        }
    }
    public void clickPromotionalInfomationItemTelephoneReceive(){
        if(isIphoneSE()){
            clickByLocationByPercent(88,62);//iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(88,56);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(88,48.2);
        }else if(isIphone13()){
            clickByLocationByPercent2(88,49.6);
        }else{
            clickByLocationByPercent(88,62);//iphone se
        }
    }
    public void clickPromotionalInfomationItemMessageReceive(){
        if(isIphoneSE()){
            clickByLocationByPercent(88,73);//iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(88,66);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(88,57);
        }else if(isIphone13()){
            clickByLocationByPercent2(88,58.6);
        }else{
            clickByLocationByPercent(88,73);//iphone se
        }
    }
    public void clickPromotionalInfomationItemPushReceive(){
        if(isIphoneSE()){
            clickByLocationByPercent(88,83);//iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(88,76);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(88,65.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(88,66.9);
        }else{
            clickByLocationByPercent(88,83);//iphone se
        }
    }

    @SneakyThrows
    public void testMarketingPreferences(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5) {
        Thread.sleep(5000);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        clickPromotionalInfomationItem();
        Thread.sleep(3000);
        waitUntilElementVisible(PromotionalInfomationPageTitle);
        takeScreenshot(screenShotName1);

        clickPromotionalInfomationItemEmailReceive();
        waitOkBtnAndClick();
        clickPromotionalInfomationItemTelephoneReceive();
        waitOkBtnAndClick();
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickPromotionalInfomationItemMessageReceive();
        waitOkBtnAndClick();
        clickPromotionalInfomationItemPushReceive();
        waitOkBtnAndClick();
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);

        clickPromotionalInfomationItemEmailReceive();
        waitOkBtnAndClick();
        Thread.sleep(1000);
        takeScreenshot(screenShotName4);
        clickPromotionalInfomationItemTelephoneReceive();
        waitOkBtnAndClick();
        clickPromotionalInfomationItemMessageReceive();
        waitOkBtnAndClick();
        clickPromotionalInfomationItemPushReceive();
        waitOkBtnAndClick();
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
    }

    public void waitOkBtnAndClick(){
        if(isShow(OkBtn,5)){
            clickElement(OkBtn);
        }
    }

    @SneakyThrows
    public void clickUpdateAddressEditBtn(){
//        int y=41;
//        for (int i = 0; i < 5; i++) {
//            clickWorkInfoEditBtn(y);
//            if(isShow(udpateAddressPageTitle,3)) break;
//            y =+ 2;
//        }
        if(isIphoneSE()){
            scrollUp();
            Thread.sleep(2000);
            clickByLocationByPercent(87, 60);//iphone se
        }else if(isIphone7Plus()){
            scrollUp();
            Thread.sleep(2000);
            clickByLocationByPercent(87, 51);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(87,76.9);
        }else if(isIphone13()){
            clickByLocationByPercent2(87,82);
        }else{
            clickByLocationByPercent(87, 60);//iphone se
        }
        //int yPercent = getYlocation("src/main/resources/images/channel/maintenance/UpdateAddress2.png",96,30);
        //clickByLocationByPercent(87, yPercent);
    }

    @SneakyThrows
    public void updateAddress(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5,String screenShotName6){
        Thread.sleep(5000);
        clickUpdateAddressEditBtnAndCheck(screenShotName1);
        updateAddressDetail(streetNameInput,screenShotName2,screenShotName3,screenShotName4,screenShotName5);
        scrollDown();
        Thread.sleep(1000);
        //clickUpdateAddressEditBtnAndCheck(screenShotName6);
    }

    @SneakyThrows
    public void updateAddressDetail(MobileElement addressInputEle,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5){
        //String streetNewName =generateRandomString()+"@#*";
        String streetNewName =generateRandomString();
        clearAndSendKeys(addressInputEle,streetNewName);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        inputEnter(addressInputEle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickElement(confirmUpdateBtn);
        commonPage.inputMSKByString("123456");
        Thread.sleep(3000);
        takeScreenshot(screenShotName4);
        Thread.sleep(2000);
        takeScreenshot(screenShotName5);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void clickUpdateAddressEditBtnAndCheck(String screenShotName){
        clickUpdateAddressEditBtn();
        waitUntilElementVisible(udpateAddressPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickUpdateAddressEditBtnAndCheckForEmailAddress(String screenShotName){
        clickUpdateAddressEditBtn();
        waitUntilElementVisible(udpateAddressPageTitle);
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void updateEmailAddress(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5,String screenShotName6) {
        Thread.sleep(5*1000);
        clickUpdateAddressEditBtnAndCheckForEmailAddress(screenShotName1);
        updateAddressDetail(emailStreetNameInput,screenShotName2,screenShotName3,screenShotName4,screenShotName5);
        Thread.sleep(5*1000);
        scrollDown();
        Thread.sleep(2000);
        clickUpdateAddressEditBtnAndCheckForEmailAddress(screenShotName6);
    }

    public String generateRandomString() {
        String result = "";
        for (int i = 0; i <10; i++) {
            int intVal = (int) (Math.random()*26+97);
            result += (char) intVal;
        }
        return result;
    }

    public void clickWorkInfoEditBtn(){
        clickByLocationByPercent(87,53);
    }

    @SneakyThrows
    public void scrollUpToNationLocation(){
        for (int i = 0; i < 6; i++) {
            scrollUp();
        }
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void updateNationInfomation(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4) {
        Thread.sleep(3000);
        scrollUpToNationLocation();
        takeScreenshot(screenShotName1);
        clickNationEditItemWhenChinaSelected();
        Thread.sleep(2000);
        waitUntilElementVisible(nationPageTitle);
        commonPage.moveEyeBtnToMiddleRight();
        Thread.sleep(1000);
        clickEditNationSelector();
        Thread.sleep(2000);
        takeScreenshot(screenShotName2);
        clickToDeleteChinaItemFromSelector();
        Thread.sleep(2000);
        takeScreenshot(screenShotName3);
        clickConfirmBtnForSelectNation();
        Thread.sleep(2000);
        waitUntilElementVisible(confirmUpdateBtn);
        clickElement(confirmUpdateBtn);
        Thread.sleep(10000);
        takeScreenshot(screenShotName4);

        addChinaNation();
    }

    @SneakyThrows
    public void addChinaNation(){
        scrollUp();
        Thread.sleep(1000);
        //clickNationEditItemWhenJustHkSelected();
        clickNationEditItemWhenChinaSelected();
        Thread.sleep(2000);
        waitUntilElementVisible(nationPageTitle);
        commonPage.moveEyeBtnToMiddleRight();
        Thread.sleep(1000);
        clickEditNationSelector();
        Thread.sleep(2000);
        clickToAddChinaItemFromSelector();
        Thread.sleep(2000);
        clickConfirmBtnForSelectNation();
        Thread.sleep(3000);
        clickTaxInput();

        Thread.sleep(1000);

        commonPage.clickSoftKeyMore();
        Thread.sleep(1000);
        commonPage.inputMSKByString("123456789012345");
        commonPage.clickSoftKeyReturn();
        waitUntilElementVisible(confirmUpdateBtn);
        clickElement(confirmUpdateBtn);
        Thread.sleep(10000);
    }

    public void clickTaxInput(){
        //clickByLocationByPercent(50,76);
        clickByPicture2("src/main/resources/images/channel/maintenance/taxNumberInput.png", 50, 30);
    }

    public void clickNationEditItemWhenChinaSelected(){
        int yPercent = 0;
        if(isIphoneSE()){
            yPercent = getYlocation("src/main/resources/images/channel/maintenance/CountryArea2.png",50,50);
        }else if(isIphone7Plus()){
            //yPercent = getYlocation("src/main/resources/images/channel/maintenance/countryArea.png",50,5);
            yPercent = 56;
        }else if(isIphone13()){
            yPercent = 62;
        }else{
            yPercent = getYlocation("src/main/resources/images/channel/maintenance/CountryArea2.png",50,50);
        }
        logger.info("yPercent: ", yPercent);
        logger.info("yPercent: ", yPercent);
        clickByLocationByPercent2(87, yPercent);
//        int y=42;
//        for (int i = 0; i < 5; i++) {
//            clickWorkInfoEditBtn(y);
//            if(isShow(nationPageTitle,3)) break;
//            y =+ 2;
//        }
    }

    public void clickEditNationSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(92,25);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(92,23);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(92,21.6);
        }else if(isIphone13()){
            clickByLocationByPercent2(92,21.6);
        }else{
            clickByLocationByPercent(92,25);
        }
    }
    public void clickToDeleteChinaItemFromSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(69,18);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(59,18);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(60.9,17.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(60.9,17.3);
        }else{
            clickByLocationByPercent(69,18);
        }
    }

    public void clickToAddChinaItemFromSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,39);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,36);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,29.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,29.4);
        }else{
            clickByLocationByPercent(50,39);
        }
    }

    @SneakyThrows
    public void openWorkInfoPageForMCV(String screenShotName){
        Thread.sleep(5000);
        scrollUp();
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        openWorkInfoPageForMCVByPicture();
        Thread.sleep(1000);
        waitUntilElementVisible(workInfoPageTitle);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }
    public void clickConfirmBtnForSelectNation(){
        clickElement(confirmBtnForSelectNation);
    }

    @SneakyThrows
    public void updateWorkInfomationForMCV(String screenShotName1,String screenShotName2,String screenShotName3) {

        clickElement(monthAmountSelector);
        Thread.sleep(1000);
        clickElement(monthAmountSelectorItem5);
        Thread.sleep(2000);
        clickElement(sourceOfIncome);
        Thread.sleep(200);
        clickElement(otherItem);
        Thread.sleep(200);
        takeScreenshot(screenShotName1);
        clickElement(confirmButtonForSourceOfIncome);
        Thread.sleep(200);
        clickElement(confirmUpdateBtn);
//        if(isClickable(confirmUpdateBtn,3)){
//            clickElement(monthAmountSelector);
//            Thread.sleep(1000);
//            clickElement(monthAmountSelectorItem4);
//            Thread.sleep(2000);
//            takeScreenshot(screenShotName1);
//            clickElement(confirmUpdateBtn);
//        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName2);
        Thread.sleep(3000);
        clickWorkInfoEditBtn();
        Thread.sleep(1000);
        if(isShow(workInfoPageTitle,4)){
            Thread.sleep(2000);
            takeScreenshot(screenShotName3);
        }
    }
    @SneakyThrows
    public void updateWorkInfomation(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4) {
        Thread.sleep(5000);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        getWorkInfoPage(screenShotName1);
        clickElement(monthAmountSelector);
        Thread.sleep(1000);
        clickElement(monthAmountSelectorItem5);
        Thread.sleep(2000);
//        clickElement(sourceOfIncome);
//        Thread.sleep(200);
//        clickElement(otherItem);
//        Thread.sleep(200);
//        takeScreenshot(screenShotName2);
//        clickElement(confirmUpdateBtn);
        if(isClickable(confirmUpdateBtn,3)){
            clickElement(monthAmountSelector);
            Thread.sleep(1000);
            clickElement(monthAmountSelectorItem4);
            Thread.sleep(2000);
            takeScreenshot(screenShotName2);
            clickElement(confirmUpdateBtn);
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName3);
        Thread.sleep(3000);
        clickWorkInfoEditBtn();
        Thread.sleep(1000);
        if(isShow(workInfoPageTitle,4)){
            Thread.sleep(2000);
            takeScreenshot(screenShotName4);
        }
    }

    @SneakyThrows
    public void getWorkInfoPage(String screenShotName){

        openWorkInfoPage();

        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openWorkInfoPage(){
        //clickByPicture("src/main/resources/images/channel/maintenance/WorkInfoEditBtn.png",95,50);
        //iphone 7 plus
        //int yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoItem.png",50,10);
        //clickByLocationByPercent(87, yPercent);

        //iphone se
        double yPercent = 0;
        if(isIphoneSE()){
            scrollUp();
            Thread.sleep(1000);
            yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoItem.png",50,10);
        }else if(isIphone13()){
            yPercent = 71.2;
        }else{
            yPercent = 71.2;
        }
        clickByLocationByPercent(87, yPercent);
        Thread.sleep(1000);

        if(isShow(workInfoPageTitle,2)){
        }else{
            clickByLocationByPercent(87, yPercent + 1);
            if(isShow(workInfoPageTitle,2)){
            }else{
                clickByLocationByPercent(87, yPercent + 2);
            }
        }
        waitUntilElementVisible(workInfoPageTitle);
    }

    @SneakyThrows
    public void openWorkInfoPageForMCVByPicture(){
        double yPercent = 0;
        if(isIphoneSE()){
            scrollUp();
            Thread.sleep(1000);
            yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoItem.png",50,10);
        }else if(isIphone13()){
            yPercent = 65;
            //yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoEdit_Iphone13.png",50,10);
        }else{
            yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoEdit_Iphone13.png",50,10);
        }
        clickByLocationByPercent(87, yPercent);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"確認\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确认\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确定\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確定\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確認\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确认\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确定\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確定\""),
    })
    private MobileElement confirmBtnForSelectNation;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"阿富汗\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"阿富汗\"])[3]"),
    })
    private MobileElement afghanistanItem;

    public void clickToAddAfghanistanItemForSelector(){
        clickElement(afghanistanItem);
    }

    public void clickToDeleteAlghanistanItemFromSelector(){
        clickElement(afghanistanItem);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"阿富汗 稅務編碼 我没有稅務編碼\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"阿富汗 税务编码 我没有税务编号\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"阿富汗 税务编号 我没有税务编码\"])[2]"),
    })
    private MobileElement afghanistanIdItem;

    public void clickNoTaxBtn(){

        int window_height = driver.manage().window().getSize().height;
        int height = afghanistanIdItem.getSize().getHeight();
        int width = afghanistanIdItem.getSize().getHeight();
        int xLocation = afghanistanIdItem.getLocation().getX();
        int yLocation = afghanistanIdItem.getLocation().getY();
        int centerX = afghanistanIdItem.getCenter().getX();
        int centerY = afghanistanIdItem.getCenter().getY();
        logger.info("window_height" + window_height);
        logger.info("height" + height);
        logger.info("width" + width);
        logger.info("xLocation" + xLocation);
        logger.info("yLocation" + yLocation);
        logger.info("centerX" + centerX);
        logger.info("centerY" + centerY);
        int eleHeight = yLocation + (int)(height*0.79);
        int yPercent = (int)eleHeight * 100 /window_height; //44.2
        logger.info("yPercent" + yPercent);
        clickByLocationByPercent(13,yPercent);
        /*
        if(isIphoneSE()){
            clickByLocationByPercent(13,54);//iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(13,58.7);//iphone 7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(12.6,64);
        }else if(isIphone13()){
            clickByLocationByPercent2(12.6,62);
        }else{
            clickByLocationByPercent(13,54);//iphone se
        }
        */
        //clickByPicture("src/main/resources/images/channel/maintenance/noTaxItem.png",50,50);
    }

    @SneakyThrows
    public void taxJurisdictionChecking(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5,String screenShotName6){
        Thread.sleep(3000);
        scrollUpToNationLocation();

        clickNationEditItemWhenChinaSelected();
        Thread.sleep(2000);
        waitUntilElementVisible(nationPageTitle);
        commonPage.moveEyeBtnToMiddleRight();
        Thread.sleep(1000);
        clickEditNationSelector();
        Thread.sleep(2000);
        clickToAddAfghanistanItemForSelector();
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        clickConfirmBtnForSelectNation();
        Thread.sleep(2000);
        scrollUp();
        scrollUp();
        Thread.sleep(2000);

        clickNoTaxBtn();
        Thread.sleep(2000);
        waitUntilElementVisible(noTaxPageTitle);
        Thread.sleep(1000);
        clickElement(noTaxReasonItem1);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickElement(noTaxReasonItem2);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickElement(noTaxReasonItem3);
        //clickNoTaxReasomItem3();
        Thread.sleep(1000);
        takeScreenshot(screenShotName4);
        clickNoTaxReasonConfirmBtn();
        Thread.sleep(1000);

        waitUntilElementVisible(confirmUpdateBtn);
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
        clickElement(confirmUpdateBtn);
        Thread.sleep(10000);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName6);

        deleteAfghanistan();
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[2]"),
    })
    private MobileElement confirmNoTaxReasonBtn;

    public void clickNoTaxReasonConfirmBtn(){
        clickElement(confirmNoTaxReasonBtn);
//        if(isIphoneSE()||isIphone7Plus()){
//            clickByLocationByPercent(50,93); //iphone7 plus,iphone se
//        }else if(isIphoneXR()||isIphone13()){
//            clickByLocationByPercent(50,90);
//        }else{
//            clickByLocationByPercent(50,93);//iphone se
//        }
    }

    public void clickNationEditItemWhenAfghanistanSelected(){
        if(isIphoneSE()){
            clickByLocationByPercent(87,39); //iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(87,44.5); //iphone 7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(87.5,54.6);
        }else if(isIphone13()){
            clickByLocationByPercent2(87.5,51.8);
        }else{
            clickByLocationByPercent(87,39); //iphone se
        }
    }

    @SneakyThrows
    public void deleteAfghanistan(){
        scrollUp();
        Thread.sleep(1000);
        //clickNationEditItemWhenChinaSelected();
        clickNationEditItemWhenAfghanistanSelected();

        Thread.sleep(2000);
        waitUntilElementVisible(nationPageTitle);
        commonPage.moveEyeBtnToMiddleRight();
        Thread.sleep(1000);
        clickEditNationSelector();
        Thread.sleep(2000);
        clickToDeleteAlghanistanItemFromSelector();
        Thread.sleep(2000);
        clickConfirmBtnForSelectNation();
        Thread.sleep(2000);
        waitUntilElementVisible(confirmUpdateBtn);
        clickElement(confirmUpdateBtn);
        Thread.sleep(10000);
    }

    @SneakyThrows
    public void clickMyAccountBtn(String screenShotName) {
        Thread.sleep(2*1000);
        clickElement(myAccountBtn);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }

    public void clickTopRightToExit(){
        clickByLocationByPercent(95,6);
    }

    public void activeWelabBankStageApp(){
        activeIosAPP("welab.bank.mobile.stage");
    }
    public void clickAboutUsItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(25,89);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(25,78);
        }else if(isIphoneXR()||isIphone13()){
            clickByLocationByPercent2(20,66.9);
        }else{
            clickByLocationByPercent(25,89);
        }
    }
    public void clickAboutUsPhoneNumber(){
        if(isIphoneSE()){
            clickByLocationByPercent(30,74);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(30,66);
        }else{
            clickByLocationByPercent(30,74);
        }
    }

    public void clickPhoneNumberToCancel(){
        clickByLocationByPercent(50,96);
    }

    public void clickAboutUsEmail(){
        if(isIphoneSE()){
            clickByLocationByPercent(30,79);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(30,72);
        }else{
            clickByLocationByPercent(30,79);
        }
    }

    public void clickAboutUsWebLink(){
        if(isIphoneSE()){
            clickByLocationByPercent(30,87);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(30,78);
        }else{
            clickByLocationByPercent(30,87);
        }
    }

    @SneakyThrows
    public void aboutUs(String screenShotName1,String screenShotName2,String screenShotName3,String screenShotName4,String screenShotName5) {
        Thread.sleep(1000);
        clickAboutUsItem();
        Thread.sleep(5000);
        takeScreenshot(screenShotName1);

        clickAboutUsPhoneNumber();
        Thread.sleep(2000);
        takeScreenshot(screenShotName2);
        clickPhoneNumberToCancel();
        Thread.sleep(2000);
        closeIosAPP("com.apple.mobilephone");
        Thread.sleep(1000);

        activeWelabBankStageApp();
        Thread.sleep(3000);
        clickAboutUsEmail();
        Thread.sleep(10000);
        takeScreenshot(screenShotName3);
        closeIosAPP("com.apple.mobilemail");
        Thread.sleep(1000);

        activeWelabBankStageApp();
        Thread.sleep(10000);
        clickAboutUsWebLink();
        Thread.sleep(10000);
        takeScreenshot(screenShotName4);
        clickTopRightToExit();
        Thread.sleep(5000);

        scrollUp();
        scrollUp();
        scrollUp();
        Thread.sleep(2000);
        takeScreenshot(screenShotName5);
    }
    public boolean ElementExist(MobileElement forgotKey) {
        try {
            MobileElement forgotKey1 = forgotKey;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @SneakyThrows
    public boolean updateResidentialAddress(String screenShotName,String screenShotName1) {
        String estateStreetStr = generateRandomString();
        clearAndSendKeys(estateStreet,estateStreetStr);
        inputEnter(estateStreet);
        Thread.sleep(2 * 1000);
        String buildingStr = generateRandomString();
        clearAndSendKeys(building,buildingStr);
        inputEnter(building);
        takeScreenshot(screenShotName);
        clickElement(confirmBtn);
        commonPage.waiteSendMsk();
        Thread.sleep(6000);
        waitUntilElementVisible(englishResidentialAddress);
        takeScreenshot(screenShotName1);
        return englishResidentialAddress.getText().contains(estateStreetStr);
    }

    @SneakyThrows
    public boolean verifyUpdateEmailAddress(String screenShotName,String screenShotName1) {
        Thread.sleep(5 * 1000);
        String estateStreetStr = generateRandomString();
        if(isRedMiNote12()){
            scrollUp();
            clickElement(RedMiMailingAddressStreet);
            clearAndSendKeys(RedMiMailingAddressStreet,estateStreetStr);
        }else {
            clickElement(building);
            Thread.sleep(5 * 1000);
            scrollUpByPercent(33, 5);
            scrollUpByPercent(33, 5);
            clickElement(mailingAddressStreet);
            clearAndSendKeys(mailingAddressStreet, estateStreetStr);
        }

        Thread.sleep(5 * 1000);
        takeScreenshot(screenShotName);
        clickElement(confirmBtn);
        Thread.sleep(2 * 1000);
        commonPage.waiteSendMsk();
        Thread.sleep(10000);
        waitUntilElementVisible(englishResidentialAddress);
        scrollUp();
        Thread.sleep(3 * 1000);
        waitUntilElementVisible(englishMailAddress);
        takeScreenshot(screenShotName1);
        return englishMailAddress.getText().contains(estateStreetStr);
    }

    @SneakyThrows
    public void changeMSKPasswordBack(String screenShotName,String screenShotName1) {
        Thread.sleep(5*1000);
        sendNewMsk();
        Thread.sleep(5*1000);
        sendMsk();
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName);
        sendMsk();
        Thread.sleep(5*1000);
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public boolean verifyRebindDeviceDailyLimit(String screenShotName) {
        takeScreenshot(screenShotName);
        Thread.sleep(5000);
        return verifyElementExist(exceedMaximum);
    }

    @SneakyThrows
    public void deleteNationality() {
        if(isSamsungA3460()){
            deleteNationalityForSamsungA3460();
            return;
        }
        Thread.sleep(3000);
        clickElement(nationalEditBtn);
        Thread.sleep(3000);
        clickElement(deleteHaitiBtn);
        Thread.sleep(3000);
        clickElement(yesBtn);

        Thread.sleep(3000);
        clickElement(deleteAmericanBtn);
        Thread.sleep(3000);
        clickElement(yesBtn);
        Thread.sleep(3000);
        clickElement(confirmBtn);
    }

    @SneakyThrows
    public void deleteNationalityForSamsungA3460() {
        Thread.sleep(3000);
        clickElement(nationalEditBtn);
        Thread.sleep(3000);
        clickElement(deleteAmericanBtn);
        Thread.sleep(3000);
        clickElement(yesBtn);
        Thread.sleep(3000);
        clickElement(confirmBtn);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void clickSupportTitles(String screenShotName,String screenShotName1,String screenShotName2) {
        Thread.sleep(2*1000);
        clickElement(supportTitle);
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName);
        scrollUp();
        scrollUp();
        takeScreenshot(screenShotName1);
//        for (int i = 0; i < 10; i++) {
//            scrollUpFast();
//        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName2);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"修改賬戶資料\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"修改账户资料\" AND name == \"text\""),
    })
    private MobileElement updateAccountPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"開戶目的\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"開戶原因\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"开户目的\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"开户原因\"])[3]"),
    })
    private MobileElement OpeningAccountPurpose;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"儲蓄\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"储蓄\"])[2]"),
    })
    private MobileElement updateAccountReason;
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"确定\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確定\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确定\" AND name == \"确定\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確定\" AND name == \"確定\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"確認\" AND name == \"確認\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"确认\" AND name == \"确认\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确认\"]"),
    })
    private MobileElement updateAccountReasonConfirm;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確認修改\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确认修改\"])[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确认\"]"),
    })
    private MobileElement OpeningAccountPurposeConfirm;

    @SneakyThrows
    public void clickAccountOpeningEditButtonForMCV(String screenShotName){
        scrollUp();
        scrollUp();
        Thread.sleep(200);
        takeScreenshot(screenShotName);
        clickUpdateAccountOpeningEidtButtonForMCV();
    }

    @SneakyThrows
    public void clickUpdateAccountOpeningEidtButtonForMCV(){
        if(isIphone13()){
            int yPercent = getYlocation2("src/main/resources/images/channel/maintenance/openAccountItem.png",50,10);
            clickByLocationByPercent(87, yPercent -1);
        }else if(isIphoneSE()){
            int yPercent = getYlocation2("src/main/resources/images/channel/maintenance/openAccountItem.png",50,10);
            clickByLocationByPercent(87, yPercent -2);
        }else{
            int yPercent = getYlocation2("src/main/resources/images/channel/maintenance/openAccountItem.png",50,10);
            clickByLocationByPercent(87, yPercent -1);
        }
    }
    public void clickUpdateAccountOpeningEidtButton(){
        if(isIphoneSE()){
            clickByLocationByPercent2(87, 61.8); //iphone se
        }else if(isIphone7Plus()){
            clickByLocationByPercent(87, 50);//iphone7 plus
        }else if(isIphoneXR()){
            clickByLocationByPercent2(87.5,48.2);
        }else if(isIphone13()){
            clickByLocationByPercent2(87.5,74.8);
        }else{
            clickByLocationByPercent2(87, 61.8); //iphone se
        }
    }

    @SneakyThrows
    public void updateAccountOpeningInformationIOS(String screenShotName, String screenShotName1,
                                                String screenShotName2, String screenShotName3) {
        if(isIphoneXR()||isIphone13()){
            scrollUp();
        }else{
            scrollUp();
            scrollUp();
        }
        takeScreenshot(screenShotName);
        clickUpdateAccountOpeningEidtButton();
        updateForOpenAccount(screenShotName1, screenShotName2, screenShotName3);
    }

    @SneakyThrows
    public void updateForOpenAccount(String screenShotName1, String screenShotName2, String screenShotName3){
        waitUntilElementVisible(updateAccountPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickElement(OpeningAccountPurpose);
        clickElement(updateAccountReason);
        clickElement(updateAccountReasonConfirm);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickElement(OpeningAccountPurposeConfirm);
        Thread.sleep(6000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void updateAccountOpeningInformationAndroid(String screenShotName,String screenShotName1) {
        Thread.sleep(2000);
        scrollUpToFindElement(accountOpeningInformationEdit,3,2);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(accountOpeningInformationEdit);

        Thread.sleep(2000);
        intoAccountList.get(0).click();
        randomClickListElement(PurposeInfoList,3);
        Thread.sleep(2000);
        clickElement(okBtn);
        Thread.sleep(2000);
        intoAccountList.get(1).click();
        randomClickListElement(inComingInfoList,1);
        Thread.sleep(2000);
        intoAccountList.get(2).click();
        randomClickListElement(outGoingInfoList,1);
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void verifyAccountOpeningInformation(String screenShotName) {
        logger.info("intoAccountList Size is {}", intoAccountList.size());
        clickElement(confirmBtn);
        waitUntilElementVisible(accountOpeningInformationEdit);
        for (int i = 0; i < intoAccountList.size(); i++) {
            assertThat(modifyAccountList.get(i*2+1).getText()).isEqualTo(intoAccountList.get(i).getText());
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    public String generateRandomString(int length) {
        String result = "";
        for (int i = 0; i <length; i++) {
            int intVal = (int) (Math.random()*26+97);
            result += (char) intVal;
        }
        return result;
    }

    @SneakyThrows
    public void clickAddressEditButtonForMcv(){
        clickUpdateAddressEditBtn();
//        if(isIphone13()){
//            clickByLocationByPercent2(87,82);
//        }else{
//            clickByLocationByPercent(87, 60);
//        }
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"大廈名稱\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"大厦名称\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement mcvAccountResidentialBuilding;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"街道名稱\"])[8]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"街道名称\"])[8]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"大廈名稱 / 座數\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField\n"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"大厦名称 / 座数\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField\n"),
    })
    private MobileElement mcvAccountEmailBuilding;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"分區\"])[3]")
    private MobileElement areaSelectorForMcvEmailAddress;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Aberdeen\"])[2]")
    private MobileElement selectorItemForMcvEmailAddress;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"大廈名稱\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"大厦名称\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"大厦名称\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"大廈名稱\"]"),
    })
    private MobileElement buildingLabel;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确定\"]"),
    })
    private MobileElement confirmUpdateAddressForMcv;

    @SneakyThrows
    public void updateResidentialBuildingForMcv(String screenShotName, String screenShotName1){
        String address = generateRandomString(3);
        logger.info("address: "+ address);
        waitUntilElementVisible(buildingLabel);
        if(isIphoneSE()){
            scrollUp();
            Thread.sleep(1000);
        }
        clickElement(mcvAccountResidentialBuilding);
        Thread.sleep(100);
        clearInput(mcvAccountResidentialBuilding);
        Thread.sleep(100);
        commonPage.clickSoftKeyByNumberString(address);
        Thread.sleep(100);
        takeScreenshot(screenShotName);
        inputMskForUpdateAddressMcv(screenShotName1);
    }

    @SneakyThrows
    public void updateEmailBuildingForMcv(String screenShotName, String screenShotName1){
        String address = generateRandomString(3);
        logger.info("address: "+ address);
        waitUntilElementVisible(buildingLabel);
        scrollUp();
        scrollUp();
        scrollUp();
        Thread.sleep(200);
//        clickElement(areaSelectorForMcvEmailAddress);
//        Thread.sleep(1000);
//        clickElement(selectorItemForMcvEmailAddress);
//        Thread.sleep(1000);
        clickElement(mcvAccountEmailBuilding);
        Thread.sleep(100);
        clearInput(mcvAccountEmailBuilding);
        Thread.sleep(100);
        commonPage.clickSoftKeyByNumberString(address);
        Thread.sleep(100);
        takeScreenshot(screenShotName);
        inputMskForUpdateAddressMcv(screenShotName1);
    }
    @SneakyThrows
    public void inputMskForUpdateAddressMcv(String screenShotName){
        Thread.sleep(100);
        clickElement(confirmUpdateAddressForMcv);
        Thread.sleep(1000);
        commonPage.waitMskPage();
        commonPage.inputMSKByString("123456");
        Thread.sleep(3000);
        scrollUp();
        Thread.sleep(200);
        takeScreenshot(screenShotName);
        Thread.sleep(200);
    }

    @SneakyThrows
    public boolean updateResidentialAddressForAOSMCV(String screenShotName, String screenShotName1) {
        String estateStreetStr = generateRandomString();
        clickElement(estateStreetMCV);
        clearAndSendKeys(streetMCV,estateStreetStr);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(confirmBtn);
        commonPage.waiteSendMsk();
        Thread.sleep(6000);
        scrollDown();
        scrollUpToFindElement(addressInformation,2,1);
        waitUntilElementVisible(addressInformation);
        takeScreenshot(screenShotName1);
        return addressInformation.getText().contains(estateStreetStr);
    }

    @SneakyThrows
    public boolean updateEmailAddressForAOSMCV(String screenShotName, String screenShotName1) {
        Thread.sleep(5 * 1000);
        String estateStreetStr = generateRandomString();
        scrollUp();
        scrollUp();
        clickElement(deliveryAddressStreetMCV);
        scrollUpByPercent(33, 5);
        scrollUpByPercent(33, 5);
        scrollUpByPercent(33, 5);
        clearAndSendKeys(buildMCV, estateStreetStr);
        Thread.sleep(5 * 1000);
        takeScreenshot(screenShotName);
        clickElement(confirmBtn);
        Thread.sleep(2 * 1000);
        commonPage.waiteSendMsk();
        Thread.sleep(10000);
        waitUntilElementVisible(addressInformation);
        scrollDown();
        scrollUpToFindElement(mailAddressStreetMCV,2,1);
        Thread.sleep(3 * 1000);
        waitUntilElementVisible(mailAddressStreetMCV);
        takeScreenshot(screenShotName1);
        return mailAddressStreetMCV.getText().contains(estateStreetStr);
    }

    @SneakyThrows
    public void updateAccountOpeningInformationAOSMCV(String screenShotName, String screenShotName1) {
        Thread.sleep(2000);
        scrollUpToFindElement(accountOpeningInformationEdit,3,2);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(accountOpeningInformationEdit);

        Thread.sleep(2000);
        intoAccountList.get(0).click();
        randomClickListElement(PurposeInfoList,3);
        Thread.sleep(2000);
        clickElement(okBtn);
        intoAccountList.get(1).click();
        randomClickListElement(ReasonInfoList,2);
        Thread.sleep(2000);
        clickElement(okBtn);
        Thread.sleep(2000);
        intoAccountList.get(2).click();
        randomClickListElement(inComingInfoList,1);
        Thread.sleep(2000);
        intoAccountList.get(3).click();
        randomClickListElement(outGoingInfoList,1);
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void modifyNationalityForMCV(String screenShotName, String screenShotName1, String nationalityName,
                                        String screenShotName2, String screenShotName3) {
        Thread.sleep(4 * 1000);
        scrollUpToFindElement(nationalTitle, 6, 1);
        scrollUp();
        Thread.sleep(2 * 1000);
        Thread.sleep(4 * 1000);
        takeScreenshot(screenShotName);
        clickElement(nationalEditBtn);

        Thread.sleep(3 * 1000);
        clickElement(searchNationalityEdit);
        waitUntilElementVisible(okBtn);
        takeScreenshot(screenShotName1);
        clickElement(searchBtn);
        clickElement(oldNationalityList.get(1));
        clickElement(searchNationalityEdit);
        waitUntilElementVisible(okBtn);
        clickElement(searchBtn);
        andSendKeys(searchBtn, nationalityName);
        inputEnter(searchBtn);
        clickElement(searchResult);
        takeScreenshot(screenShotName2);
        Thread.sleep(2 * 1000);
        clickElement(okBtn);

        Thread.sleep(5 * 1000);
        scrollUp();
        clickElement(notTinBtn.get(0));
        Thread.sleep(200);
        clickElement(reason1);
        clickElement(confirmBtn);
        takeScreenshot(screenShotName3);
        Thread.sleep(2000);
        clickElement(confirmBtn);
        Thread.sleep(5000);
        deleteNationalityForSamsungA3460();
    }

}
