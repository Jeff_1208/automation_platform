package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.SampleAppPage;
import io.cucumber.java.en.And;
import java.util.Map;

public class SampleAppSteps {

    SampleAppPage sampleAppPage;
    public SampleAppSteps() {
        sampleAppPage = new SampleAppPage();
    }

    @And("set Server Certificate Options")
    public void setServerCertificateOptions(Map<String, String> data) {
        sampleAppPage.setServerCertificateOptions(data.get("screenShotName"));
    }

    @And("set Negative Testing Options")
    public void setNegativeTestingOptions(Map<String, String> data) {
        sampleAppPage.setNegativeTestingOptions(data.get("screenShotName"));
    }

    @And("set FPS Ios")
    public void setFPSIos(Map<String, String> data) {
        sampleAppPage.setFPSIos(data.get("screenShotName"));
    }

    @And("set Additional Data")
    public void setAdditionalData(Map<String, String> data) {
        sampleAppPage.setAdditionalData(data.get("screenShotName"));
    }

    @And("set Products")
    public void getTotalBalance(Map<String, String> data) {
        sampleAppPage.setProducts(data.get("screenShotName"));
    }

    @And("set Transaction Detail")
    public void setTransactionDetail(Map<String, String> data) {
        sampleAppPage.setTransactionDetail(data.get("screenShotName"));
    }

    @And("click Pay With Callback")
    public void clickPayWithCallback(Map<String, String> data) {
        sampleAppPage.clickPayWithCallback(data.get("screenShotName"));
    }

}
