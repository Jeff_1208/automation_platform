package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.CommonPage;
import com.welab.automation.projects.channel.pages.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;

public class ChannelCommonStepsDef {

  LoginPage loginPage;
  CommonPage commonPage;
  public ChannelCommonStepsDef(){
    loginPage = new LoginPage();
    commonPage = new CommonPage();
  }

  private static final String[] login_text = {"Login","登入"};
  private static final String[] total_balance_text = {"Total balance","總結餘"};
  private static final String expect_pending_desc =
      "We're working hard to process your application and will inform you via in-app notification within 2 business days.";

  @When("Login with user and password")
  public void loginWithUserAndPassword(Map<String, String> data) {
    loginPage.loginWithCredential(data.get("user"), data.get("password"));
  }

  @When("Login with user and password from properties")
  public void loginWithUserAndPasswordFromProperties() {
    loginPage.loginWithUserAndPasswordFromProperties();
  }
  @Given("Open Stage WeLab App")
  public void openWeLabApp() throws Exception {
    loginPage.waitAppOpen();
  }

  @And("Enable skip Root Stage")
  public void enableSkipRoot() throws InterruptedException {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      commonPage.enableSkipRoot();
    }
  }
  @And("ios skip Root")
  public void enableSkipRootIos() throws InterruptedException {
    commonPage.skipRootIos();
  }

  @And("skip Rebind Device Authentication IOS")
  public void skipRebindDeviceAuthenticationIOS() throws InterruptedException {
    commonPage.skipRebindDeviceAuthenticationIOS();
  }

  @And("register pin")
  public void skipRootAndRegisterPin() throws InterruptedException {
      commonPage.registerPin();
  }

  @And("goto My Account page")
  public void gotoMyAccountPage() {
    commonPage.gotoMyAccount();
  }

  @Then("I can see the home page")
  public void IcanSeeHomePage() {
    loginPage.IcanSeeHomePage();
  }

  @Then("I can see the LoggedIn page")
  public void verifyOnLoggedInPage() throws InterruptedException {
    String checkedTxt = loginPage.getLoggedInPageText();
    assertThat(commonPage.oneOfArrayItemInStr(checkedTxt,total_balance_text));
  }

  @Given("Check upgrade page appears Stage")
  public void checkUpgradePageAppears() {
    //commonPage.checkLogin();
    //commonPage.checkUpgrade();
  }

  @Given("Click got it and Login")
  public void clickGotItAndLogin() {
//    commonPage.checkLogin();
    commonPage.checkUpgrade();
  }

  @Then("I can see pending approval screen")
  public void iCanSeePendingApproval() {
    String approvalTxt = commonPage.getPendingApprovalDesc();
    assertThat(approvalTxt).isEqualTo(expect_pending_desc);
  }

  @And("I skip guidance")
  public void skipGuidance() {
    commonPage.skipGuidance();
  }

  @And("I go to Verify personal info")
  public void goToVerifyPersonalInfo() {
    commonPage.skipToOpenAccount();
  }

  @When("I click X icon")
  public void iClickXIcon() {
    commonPage.clickXIcon();
  }

  @Then("I can see pending page")
  public void verifyPendingPage(Map<String, String> data) {
    String titleText = commonPage.getElementText(commonPage.getThanksTitle()).trim();
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
  }

  @When("I click back icon")
  public void iClickBackIcon() {
    commonPage.clickBackIcon();
  }

  @And("I move eyeBtn to top right corner")
  public void iMoveEyeBtnToTopRight() {
    // if the mobile platform is iOS, then no eyeBtn.
    if (System.getProperty("mobile").equals("android")) {
      commonPage.moveEyeBtnToTopRight();
    }
  }

  @And("Enable skip Root Stage and change language to CH")
  public void enableSkipRootchange() throws InterruptedException {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      boolean flag =commonPage.changeLanguageToCH();
      assertThat(flag).isTrue();
      commonPage.enableSkipRoot();
    }
  }

  @And("open HKAB Merchant Sample App")
  public void openHKABMerchantSampleApp() {
    commonPage.openHKABMerchantSampleApp();
  }

  @And("bind Device Authentications")
  public void bindDeviceAuthenticationIOS() throws InterruptedException {
    commonPage.bindDeviceAuthenticationIOS();
  }

  @And("bind Device Authentications Android")
  public void bindDeviceAuthenticationAndroid(){
    commonPage.bindDeviceAuthenticationAndroid();
  }

  @And("skip Rebind Device Authentication Android")
  public void skipRebindDeviceAuthenticationAndroid() {
    commonPage.skipRebindDeviceAuthenticationAndroid();
  }

  @And("update package for IOS")
  public void updatePackageForIOS() {
    commonPage.updatePackageForIOS();
  }

}
