Feature: Welab app login

  Background: Login
    Given Open Stage WeLab App
#    And Enable skip Root Stage
#    Given Check upgrade page appears Stage

#  @health
#  Scenario:  Reg_Login_001 bind Device Android
#    And skip Rebind Device Authentication Android
#    When Login with user and password from properties
#    And bind Device Authentications Android

  @ACBN-T628  @health
  Scenario:  Reg_Login_001 login in by username and password
    When Login with user and password from properties
    Then I can see the LoggedIn page
    Then get screenShot
      | screenShotName | Reg_Login_001_Android_01 |

    @VCAT-7 @ACBN-T638
  Scenario:  Reg_login_005 Balance enquiry and transaction history sequence
    When Login with user and password from properties
    Then I can see the LoggedIn page
    And View transaction details
      | screenShotName | Reg_login_005_Android_01 |
      | screenShotName1 | Reg_login_005_Android_02 |
#      | screenShotName2 | Reg_login_005_Android_03 |
#    And I'm checking the information in the purchase record
#      | screenShotName | Reg_login_005_Android_ |