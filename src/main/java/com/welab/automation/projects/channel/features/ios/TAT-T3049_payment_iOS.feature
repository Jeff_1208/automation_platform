Feature: Payment

  Background: Login
    Given Open Stage WeLab App
    And Enable skip Root Stage
    Given Check upgrade page appears Stage
    When Login with user and password from properties
    Then I can see the LoggedIn page

  @health  @ACBN-T639
  Scenario Outline:  Reg_Payment_001 IOS Outbound FPS transfer(welab bank) with notification
    Given I go to payment Page
      | screenShotName | Reg_Payment_001_IOS_01 |
      | screenShotName1 | Reg_Payment_001_IOS_02 |
    Then select bank
      | screenShotName | Reg_Payment_001_IOS_03|
    Then transaction input data <amount> <account> <name>
      | screenShotName | Reg_Payment_001_IOS_04|
    Then confirm transaction
      | screenShotName | Reg_Payment_001_IOS_05|
    Then check available amount
      | screenShotName | Reg_Payment_001_IOS_06|
    Then check transaction on home page
      | screenShotName | Reg_Payment_001_IOS_07|
      | screenShotName1 | Reg_Payment_001_IOS_08|
    Examples:
      | amount | account    | name        |
      | 100    | 1000476928 | WILSON,Wil  |

  @ACBN-T641
  Scenario Outline:  Reg_Payment_002 IOS Outbound FPS transfer(other bank) with notification
    Given I go to payment Page
      | screenShotName | Reg_Payment_002_IOS_01 |
      | screenShotName1 | Reg_Payment_002_IOS_02 |
    Then select 672 bank
      | screenShotName | Reg_Payment_002_IOS_03|
    Then transaction input data <amount> <account> <name>
      | screenShotName | Reg_Payment_002_IOS_04|
    Then confirm transaction
      | screenShotName | Reg_Payment_002_IOS_05|
    Then check available amount
      | screenShotName | Reg_Payment_002_IOS_06|
    Then check transaction on home page
      | screenShotName | Reg_Payment_002_IOS_07|
      | screenShotName1 | Reg_Payment_002_IOS_08|
    Examples:
      | amount | account   | name     |
      | 100    | 123456789 | test672  |

#  @jeff
#  Scenario:  Reg_Payment_007 IOS FPS ID reg and De-reg with push notification
#    Given I go to payment Page
#      | screenShotName | Reg_Payment_007_IOS_01 |
#      | screenShotName1 | Reg_Payment_007_IOS_02 |
#    Then I go to payment setting Page
#      | screenShotName | Reg_Payment_007_IOS_03 |
#    Then set fps setting
#      | screenShotName | Reg_Payment_007_IOS_04|
#      | screenShotName1 | Reg_Payment_007_IOS_05|
#      | screenShotName2 | Reg_Payment_007_IOS_06|

#  @ACBN-T226
#  Scenario Outline:  Reg_Payment_0018 IOS Transfer Money via Email Address
#    Given I go to payment Page
#      | screenShotName  | Reg_Payment_0018_IOS_01 |
#      | screenShotName1 | Reg_Payment_0018_IOS_02 |
#    Then run fps transaction <amount> <FpsId>
#      | screenShotName | Reg_Payment_0018_IOS_03|
#    Then confirm transaction
#      | screenShotName | Reg_Payment_0018_IOS_04|
#    Then check available amount
#      | screenShotName | Reg_Payment_0018_IOS_05|
#    Then check transaction on home page
#      | screenShotName  | Reg_Payment_0018_IOS_06 |
#      | screenShotName1 | Reg_Payment_0018_IOS_07|
#    Examples:
#      | amount | FpsId       |
#      | 118    | v.tony.liang@welab.bank |
##      | 118    | ghhh@qq.com |

#  @ACBN-T227
#  Scenario Outline:  Reg_Payment_0019 IOS Transfer Money via FPS Proxy ID
#    Given I go to payment Page
#      | screenShotName  | Reg_Payment_0019_IOS_01 |
#      | screenShotName1 | Reg_Payment_0019_IOS_02 |
#    Then run fps transaction <amount> <FpsId>
#      | screenShotName | Reg_Payment_0019_IOS_03 |
#    Then confirm transaction
#      | screenShotName | Reg_Payment_0019_IOS_04 |
#    Then check available amount
#      | screenShotName | Reg_Payment_0019_IOS_05 |
#    Then check transaction on home page
#      | screenShotName  | Reg_Payment_0019_IOS_06 |
#      | screenShotName1 | Reg_Payment_0019_IOS_07 |
#    Examples:
#      | amount | FpsId     |
#      | 119    | 167649193 |
##      | 119    | 799687421 |

  @ACBN-T643
  Scenario Outline:  Reg_Payment_008 IOS Non-payee, pay max limit 300k HKD
    Given I go to payment Page
      | screenShotName | Reg_Payment_008_IOS_01 |
      | screenShotName1 | Reg_Payment_008_IOS_02 |
    Then run fps transaction <amount> <FpsId>
      | screenShotName | Reg_Payment_008_IOS_03|
    Then confirm transaction
      | screenShotName | Reg_Payment_008_IOS_04|
    Then check available amount
      | screenShotName | Reg_Payment_008_IOS_05|
    Then check transaction on home page
      | screenShotName | Reg_Payment_008_IOS_06|
      | screenShotName1 | Reg_Payment_008_IOS_07|
    Examples:
      | amount | FpsId    |
      | 100008 | 88880194 |
#      | 100008 | 88880194 |

  @ACBN-T645
  Scenario Outline:  Reg_Payment_009 IOS Register a new Payee, do a large amount transfer max limit 500k HKD to new payee account
    Given I go to payment Page
      | screenShotName | Reg_Payment_009_IOS_01 |
      | screenShotName1 | Reg_Payment_009_IOS_02 |
    Then delete payee
    Then run fps transaction with add payee <amount> <FpsId>
      | screenShotName | Reg_Payment_009_IOS_03|
    Then confirm transaction
      | screenShotName | Reg_Payment_009_IOS_04|
    Then run fps transaction with select payee <amount2>
      | screenShotName | Reg_Payment_009_IOS_05|
    Then confirm transaction
      | screenShotName | Reg_Payment_009_IOS_06|
    Then delete payee
    Then go to home page
    Then check available amount
      | screenShotName | Reg_Payment_009_IOS_07|
    Then check transaction on home page
      | screenShotName | Reg_Payment_009_IOS_08|
      | screenShotName1 | Reg_Payment_009_IOS_09|
    Examples:
      | amount | FpsId    | amount2 |
      | 100    | 88880194 | 100009  |
#      | 100    | 88880194 | 100009  |

  @ACBN-T235 @health
  Scenario Outline:  Reg_Payment_0010 Use EDDI to add money
    Given I go to payment Page without transaction
      | screenShotName | Reg_Payment_0010_IOS_01 |
    Then run add money <amount>
      | screenShotName | Reg_Payment_0010_IOS_02|
      | screenShotName1 | Reg_Payment_0010_IOS_03|
    Then check available amount
      | screenShotName | Reg_Payment_0010_IOS_04|
    Examples:
      | amount |
      | 100    |

  @ACBN-T236
  Scenario Outline:  Reg_Payment_0012 set up auto eDDI rules
    Given I go to payment Page without transaction
      | screenShotName | Reg_Payment_0012_IOS_01 |
    Then set up auto eDDI rules <amount> <remainAmount>
      | screenShotName | Reg_Payment_0012_IOS_02|
      | screenShotName1 | Reg_Payment_0012_IOS_03|
      | screenShotName2 | Reg_Payment_0012_IOS_04|
    Examples:
      | amount | remainAmount  |
      | 100    | 12000         |

  @ACBN-T647
  Scenario: Reg_Payment_0013 change delect  auto eDDI rules
    Given I go to payment Page without transaction
      | screenShotName | Reg_Payment_0013_IOS_01 |
    Then delect auto eDDI rules
      | screenShotName | Reg_Payment_0013_IOS_02|
      | screenShotName1 | Reg_Payment_0013_IOS_03|
      | screenShotName2 | Reg_Payment_0013_IOS_04|

  @ACBN-T649
  Scenario Outline:  Reg_Payment_0014 IOS Outbound FPS transfer directly to bank account with incorrect payee name
    Given I go to payment Page
      | screenShotName | Reg_Payment_0014_IOS_01 |
      | screenShotName1 | Reg_Payment_0014_IOS_02 |
    Then select bank
      | screenShotName | Reg_Payment_0014_IOS_03|
    Then transaction input data <amount> <account> <name>
      | screenShotName | Reg_Payment_0014_IOS_04|
    Then check fail transaction
      | screenShotName | Reg_Payment_0014_IOS_05|
    Examples:
      | amount | account    | name        |
      | 100    | 1000476928 | WILSON,errorName  |

#  @ACBN-T653
#  Scenario Outline:  Reg_Payment_0017 IOS Payment with bill number
#    Given I go to payment Page
#      | screenShotName | Reg_Payment_0017_IOS_01 |
#      | screenShotName1 | Reg_Payment_0017_IOS_02 |
#    Then run fps transaction with bill number <amount> <FpsId> <billNumber>
#      | screenShotName | Reg_Payment_0017_IOS_03|
#    Then confirm bill number transaction
#      | screenShotName | Reg_Payment_0017_IOS_04|
#    Then check available amount
#      | screenShotName | Reg_Payment_0017_IOS_05|
#    Then check transaction on home page
#      | screenShotName | Reg_Payment_0017_IOS_06|
#      | screenShotName1 | Reg_Payment_0017_IOS_07|
#    Examples:
#      | amount | FpsId     | billNumber                 |
#      | 100    | 166924688 | DBSBillnumbertestingTerry  |

  @ACBN-T232
  Scenario Outline:  Reg_Payment_0020 Transfer money through scan Bill Payment QR Code with MSK IOS
    Given I go to payment Page
      | screenShotName | Reg_Payment_0020_IOS_01 |
      | screenShotName1 | Reg_Payment_0020_IOS_02 |
    Then run Transaction By QR Code <amount>
      | screenShotName | Reg_Payment_0020_IOS_03|
    Then confirm transaction
      | screenShotName | Reg_Payment_0020_IOS_04|
    Then check available amount
      | screenShotName | Reg_Payment_0020_IOS_05|
    Then check transaction on home page
      | screenShotName | Reg_Payment_0020_IOS_06|
      | screenShotName1 | Reg_Payment_0020_IOS_07|
    Examples:
      | amount |
      | 22     |


#  Scenario Outline:  Reg_Payment_0021 fps high risk transaction IOS by mobile
#    Given I go to payment Page
#      | screenShotName  | Reg_Payment_0021_IOS_01 |
#      | screenShotName1 | Reg_Payment_0021_IOS_02 |
#    Then run fps transaction Trigger Half Idv Check <amount> <FpsId>
#      | screenShotName  | Reg_Payment_0021_IOS_03 |
#      | screenShotName1 | Reg_Payment_0021_IOS_04 |
#    Examples:
#      | amount | FpsId    |
#      | 21     | 88880600 |

#  Scenario Outline:  Reg_Payment_0022 fps high risk transaction IOS by FPS ID
#    Given I go to payment Page
#      | screenShotName  | Reg_Payment_0022_IOS_01 |
#      | screenShotName1 | Reg_Payment_0022_IOS_02 |
#    Then run fps transaction Trigger Half Idv Check <amount> <FpsId>
#      | screenShotName  | Reg_Payment_0022_IOS_03 |
#      | screenShotName1 | Reg_Payment_0022_IOS_04 |
#    Examples:
#      | amount | FpsId     |
#      | 22     | 799932892 |

#  Scenario Outline:  Reg_Payment_0023 fps high risk transaction IOS by Email
#    Given I go to payment Page
#      | screenShotName  | Reg_Payment_0023_IOS_01 |
#      | screenShotName1 | Reg_Payment_0023_IOS_02 |
#    Then run fps transaction Trigger Half Idv Check <amount> <FpsId>
#      | screenShotName  | Reg_Payment_0023_IOS_03 |
#      | screenShotName1 | Reg_Payment_0023_IOS_04 |
#    Examples:
#      | amount | FpsId          |
#      | 23     | ben602@ben.ben |
