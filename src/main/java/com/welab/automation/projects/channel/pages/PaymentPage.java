package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.utils.ADB;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.cucumber.java.en.Then;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;


public class PaymentPage extends AppiumBasePage {

    private double theAccountAvailableBalanceBeforeTrans;
    private double transactionAmount = 0.0;
    private String transactionText="";
    private String pageName = "Payment Page";

    private String traderName;
    private double TransactionAmount=0.00;
    private String FPSid;
    private String billNumber;
    private String SendMoneyAccount;
    private double theAccountAvailableBalanceAfterTrans = 0.0;
    CommonPage commonPage;
    GoSavePage goSavePage;
    CardPage cardPage;
    public PaymentPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        goSavePage = new GoSavePage();
        cardPage = new CardPage();
    }

    @Getter
    @AndroidFindAll({
            @AndroidBy( accessibility = "Transfer, tab, 3 of 4"),
            @AndroidBy( accessibility = "Transfer, tab, 3 of 5"),
            @AndroidBy( xpath ="//android.widget.Button[3]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"存款与转账, tab, 3 of 5\""),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemTransferStack, tab, 3 of 5'")
    })
    private MobileElement transferTab;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"存款與轉賬\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"存款与转账\" AND name == \"text\""),
    })
    private MobileElement transactionPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"轉賬\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"转账\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"轉賬\"]"),
    })
    private MobileElement transactionItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"存款\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"存款\" AND name == \"text\""),
    })
    private MobileElement addMoneyItem;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '轉數快QR')]\""),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '转数快QR')]\""),
    })
    private MobileElement fastTransactionQrItem;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"收款人手機號碼 / 電郵地址 / 轉數快識別碼\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField\n")
    private MobileElement transactionAccountInput;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"我的收款人\"]")
    private MobileElement myPayeeItem;
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"我的收款人\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"我的收款人\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"搜尋\"])[1]"),
    })
    private MobileElement myPayeePageTitle;
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"你未有登記收款人\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"您还未登记收款人\" AND name == \"text\""),
    })
    private MobileElement youNotHavePayee;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"我的收款人\"])[1]/XCUIElementTypeOther[2]")
    private MobileElement myPayeePageSettingBtn;
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Confirm')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'確認')]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确认\"]"),
    })
    private MobileElement confirmDeleteBtn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"銀行轉賬\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"银行转账\"]"),
    })
    private MobileElement bankTransaction;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"收款人名称\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"收款人名稱\"]"),
    })
    private MobileElement theAccountNameInput;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"下一步\"])[2]\n")
    private MobileElement nextStepBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"轉賬\"])[5]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@value=\"转账\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"转账\" AND name == \"text\""),
    })
    private MobileElement transactionDetailPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"滑動確認\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"滑动确认\"]"),
    })

    private MobileElement slideConfirmBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"完成\"]"),
            @iOSXCUITBy(xpath = "//*[@name=\"完成\"]"),
    })
    private MobileElement FinishBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"搜尋\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"搜索\"]"),
    })
    private MobileElement searchBankInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"流動保安編碼認證\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"流动安全验证码认证\" AND name == \"text\""),
    })
    private MobileElement MSKpage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"交易詳情\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"交易详情\"]"),
    })
    private MobileElement transactionDetailPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '轉賬申請失敗')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '转账申请失败')]"),
    })
    private MobileElement transactionsFailItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, \"轉賬成功\")]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, \"转账成功\")]"),
    })
    private MobileElement transactionsSuccessItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"收款人手機號碼 / 電郵地址 / 轉數快識別碼\"])[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"收款人手機號碼 / 電郵地址 / 轉數快識別碼\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"收款人手机号码 / 电子邮件地址 / 转数快识别码\"]"),
    })
    private MobileElement FpsInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"收款人手機號碼 / 電郵地址 / 轉數快識別碼\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"收款人手机号码 / 电子邮件地址 / 转数快识别码\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement FpsInputField;
//    (//XCUIElementTypeOther[@name="收款人手機號碼 / 電郵地址 / 轉數快識別碼"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"[test braze] check out gosave best rates\"]"),
    })
    private MobileElement gosaveAlertOnTransactionPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement traderNameAtHome;

    @AndroidFindBy(
            xpath =
                    "//android.widget.ScrollView//android.view.ViewGroup[2]//android.view.ViewGroup[2]/../android.widget.TextView")
    private MobileElement amountAtHome;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Send money']"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]")
    })
    private MobileElement sendMoneyAtTransfer;

    @AndroidFindBy(xpath = "//android.widget.EditText")
    private MobileElement FPSIDEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Next']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='下一步']"),
            @AndroidBy(xpath = "//android.widget.ScrollView/../android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement sendMoneyNext;

    @AndroidFindBy(xpath = "//android.widget.TextView[2]/../android.widget.TextView[1]")
    private MobileElement traderNameAtBill;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    private MobileElement FPSidAtBill;

    @AndroidFindBy(xpath = "//android.widget.TextView[2]/..//android.widget.EditText")
    private MobileElement billNumAtBill;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.EditText")
    private MobileElement amountAtBill;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/../android.view.View"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Slide to pay')]/.."),
    })
    private MobileElement slideToPay;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Transfer in progress')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬申請已提交')]")
    })
    private MobileElement InProgress;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]")
    private MobileElement amountAtProgress;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[4]")
    private MobileElement traderNameAtProgress;

    @AndroidFindBy(xpath = "//android.widget.TextView[6]")
    private MobileElement billNumAtProgress;

    @AndroidFindBy(xpath = "//android.widget.TextView[8]")
    private MobileElement FPSIDAtProgress;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/../android.view.ViewGroup/android.widget.TextView")
    private MobileElement doneAtProgress;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='HKD']/preceding-sibling::android.widget.TextView[1]")
    })    private MobileElement amountAtTrans;

    @AndroidFindBy(xpath = "//android.widget.TextView[3]")
    private MobileElement traderNameAtTrans;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Bill/merchant reference number']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='賬單/商戶參考號碼']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement billNameAtTrans;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='FPS identifier']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='轉數快收款人']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement FPSidAtTrans;

    @AndroidFindBy(
            xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
    private MobileElement eyeBtn;


    @AndroidFindAll({
            @AndroidBy( accessibility = "Home, tab, 1 of 4"),
            @AndroidBy( accessibility = "Home, tab, 1 of 5"),
            @AndroidBy( accessibility = "主頁, tab, 1 of 5"),
            @AndroidBy( accessibility = "主頁, tab, 1 of 5")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"主页, tab, 1 of 5\""),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'")
    })
    private MobileElement homeTab;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"GOLDSON G*** +852-88880194\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"搜尋\"])[1]/../XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther[1]"),
    })
    private MobileElement myExistPayeeItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"輸入你嘅訊息\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"输入你的信息\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"輸入你嘅訊息\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"输入你的信息\""),
    })
    private MobileElement inputYourInfoEle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Done\"]")
    private MobileElement softKeyDone;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"添加為我的常用收款人\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"添加为我的常用收款人\"])[2]"),
    })
    private MobileElement addMyPayee;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Bank transfer']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='銀行轉賬']")
    })
    private MobileElement bankTransfer;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Search']"),
            @AndroidBy(xpath = "//*[@text='搜尋']")
    })
    private MobileElement searchInput;


    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.widget.EditText")
    private MobileElement accountNumberInput;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[4]/android.widget.EditText")
    private MobileElement holdNameInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.widget.EditText"),
    })
    private MobileElement amountInput;

    @AndroidFindBy(xpath = "//android.widget.TextView[7]")
    private MobileElement progressAccount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Notifications']/../android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='通知']/../android.view.ViewGroup")
    })
    private MobileElement backBtnInNoticePage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'Hi')]/../../android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'你好')]/../../android.view.ViewGroup[2]")
    })
    private MobileElement noticeBtn;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    private MobileElement firstNotice;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[3]")
    private MobileElement firstNoticeText;

    @AndroidFindBy(xpath = "//android.widget.TextView[9]")
    private MobileElement idNumberAtTrans;

    //@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"賬單 / 商戶繳付\"])[5]")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"賬單 / 商戶繳付\"]")
    private MobileElement merchanrPageTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"水平滚动条, 1页\"]")
    private MobileElement lineInput;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"賬單／商戶參考號碼\"]")
    private MobileElement billNumberInput;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[1]/android.widget.TextView[2]")
    private MobileElement FPSidAtPayee;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Add to My Payee']"),
            @AndroidBy(xpath = "//*[@text='添加為我的常用收款人']")
    })
    private MobileElement NonePayee;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[3]"),
            @AndroidBy(xpath = "//*[@text='今日']/../../android.view.ViewGroup[3]")
    })
    private MobileElement SeocndTraderNameAtHome;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transaction detail']/../android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text='交易詳情']/../android.view.ViewGroup")
    })
    private MobileElement backBtnInRecordPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='My payees']")
    })
    private MobileElement myPayeePage;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView//android.view.ViewGroup[2]//android.widget.TextView")
    private MobileElement FPSidAtMyPayee;

    @AndroidFindBy(
            xpath =
                    "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]")
    private MobileElement deleteBtnAtMyPayee;

    @AndroidFindBy(
            xpath = "//android.widget.EditText/../../android.view.ViewGroup[1]/android.view.ViewGroup[1]")
    private MobileElement backBtnAtMyPayee;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"綁定手機嘅24小時內，你嘅每日交易限額將暫時限制為HKD 10,000.00或者你之前設定的每日交易限額(以較低者為準)。如你嘅轉賬金額較高，請暫時降低你嘅轉賬金額。 如你嘅轉賬金額低於HKD 10,000.00，請提高交易限額以完成轉賬。\"")
    private MobileElement outstripLimitMessage;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"超過交易限額\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"超过交易限额\"])[1]"),
    })
    private MobileElement outstripLimitAmountTitle;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"修改限額\"]")
    private MobileElement updateLimitAmountBtn;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"取消\"]")
    private MobileElement cancelBtn;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"交易設定\"])[5]")
    private MobileElement transferSettingPageTitle;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"轉數快設定\"]")
    private MobileElement fpsSettingItem;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"自動入錢設定\"]")
    private MobileElement addMoneySettingItem;
    //XCUIElementTypeOther[@name="每日交易限額 HKD 10,000.00"]
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[starts-with(@name, '每日交易限額')]")
    private MobileElement dailyLimitAmountItem;
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"轉數快設定\" AND name == \"text\"")
    private MobileElement fpsSettingPageTitle;
    //XCUIElementTypeOther[@name="轉數快識別碼 799088570 已登記"]
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[starts-with(@name, '轉數快識別碼')]")
    private MobileElement fpsIdentity;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"刪除\"]")
    private MobileElement deleteBtn;

    private String regEx = "[^0-9]+";
    private Pattern pattern = Pattern.compile(regEx);
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Exceed daily transfer limit']"),
            @AndroidBy(xpath = "//*[@text='超過交易限額']"),
            @AndroidBy(xpath = "//android.view.ViewGroup[7]/android.widget.TextView[1]")
    })
    private MobileElement limitTextTitle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Exceed daily transfer limit']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='超過交易限額']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.view.ViewGroup[7]/android.widget.TextView[2]")
    })    private MobileElement limitText;

    private String limitTitle = "Exceed daily transfer limit";
    private String limitTitleCH = "超過交易限額";

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Add money']"),
            @AndroidBy(xpath = "//*[@text='存款']"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement addMoneyBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Confirm']"),
            @AndroidBy(xpath = "//*[@text='確定']")
    })
    private MobileElement confirmBtn;
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]")
    })
    private MobileElement repeatText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Transfer in progress')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬處理中')]")
    })
    private MobileElement Processing;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Done']"),
            @AndroidBy(xpath = "//*[@text='完成']")
    })
    private MobileElement doneBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Receive money']"),
            @AndroidBy(xpath = "//*[@text='收款']")
    })
    private MobileElement receiveMoney;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Auto Reload']/following-sibling::android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text='自動入錢']/following-sibling::android.view.ViewGroup")
    })
    private MobileElement autoReloadBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Repeat']"),
            @AndroidBy(xpath = "//*[@text='重複']")
    })
    private MobileElement repeatBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Done']"),
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup")
    })
    private MobileElement doneOnRepeat;

    @AndroidFindAll({
            @AndroidBy(xpath = " //*[@text='Transfer']/following-sibling::android.view.ViewGroup"),
            @AndroidBy(xpath = " //*[@text='存款與轉賬']/following-sibling::android.view.ViewGroup")
    })
    private MobileElement settingBtnOnTransfer;

    @AndroidFindAll({
            @AndroidBy(xpath = " //*[@text='Auto Reload settings']"),
            @AndroidBy(xpath = " //*[@text='自動入錢設定']")    })
    private MobileElement autoReloadSetings;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]")
    })
    private MobileElement firstAutoReloadRule;

    @AndroidFindAll({
            @AndroidBy(xpath="//*[@text='Delete this rule']"),
            @AndroidBy(xpath="//*[@text='删除此規則']")
    })
    private MobileElement deleteRuleBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='390 Welab Bank Limited']")
    })
    private MobileElement WeBank390;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='672 BANK 672']"),
            @AndroidBy(xpath = "//*[@text='672 672 銀行']")
    })
    private MobileElement Bank672;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
    private MobileElement amountAtTransSuccess;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Account number')]/preceding-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'戶口號碼')]/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement traderNameAtTransSuccess;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Account number')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'戶口號碼')]/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement idNumberAtTransSuccess;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    private MobileElement FPSidAtMyPayeeAlready;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Add to My Payee']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='添加為我的常用收款人']")
    })
    private MobileElement myPaeeText;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]")
    private MobileElement amountAtProgressing;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transfer unsuccessful']"),
            @AndroidBy(xpath = "//*[@text='轉賬申請失敗']")
    })
    private MobileElement transferUnsuccessful;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Try again']"),
            @AndroidBy(xpath = "//*[@text='重試']"),
    })
    private MobileElement tryAgain;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'sent money')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬成功')]"),
    })
    private MobileElement sendMoneySuccess;

    private String transactionTextSuccess="You've sent money!";

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'sent money')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬成功')]/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement sendMoneyAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Pay to']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='收款人']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement payTo;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Receiving bank']/following-sibling::android.widget.TextView[2]"),
            @AndroidBy(xpath = "//*[@text='收款銀行']/following-sibling::android.widget.TextView[2]")
    })
    private MobileElement receivingBankAccount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Account number']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='戶口號碼']/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement accountNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='FPS Proxy ID']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='轉數快識別代碼']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement fpsidOnDetail;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'ve added')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬成功')]/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement addMoney;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'instruction submitted')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'指示已提交')]"),
    })
    private MobileElement sendMoneySubmit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'instruction submitted')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'指示已提交')]/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement submitMoneyAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Transfer in progress')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'轉賬申請已提交')]/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement inProgressAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='FPS Proxy ID']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='轉數快識別代碼']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement fpsProxyIDAtTrans;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Add HKD')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'入錢 HKD')]")
    })
    private MobileElement ruleAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Auto Reload settings']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='自動入錢設定']/preceding-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement backBtnOnAutoReload;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transfer settings']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='交易設定']/preceding-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement backBtnOnTransferSettings;

    @SneakyThrows
    public void openPaymentSettingPage(String screenShotName) {
        waitUntilElementVisible(transferTab);
        Thread.sleep(1000);
        clickmyPayeePageSettingBtn();
        Thread.sleep(1000);
        waitUntilElementVisible(transferSettingPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void setFpsSettingPage(String screenShotName,String screenShotName1,String screenShotName2){
        clickElement(fpsSettingItem);
        waitUntilElementVisible(fpsSettingPageTitle);
        waitUntilElementVisible(fpsIdentity);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        String fpsContent = fpsIdentity.getText();
        System.out.println(fpsContent);
        boolean isRegisterFps = fpsContent.contains("已登記");
        clickFpsSettingSwitchBtn();
        if(isRegisterFps){
            confirmFpsSetting("未登記");
            takeScreenshot(screenShotName1);
            clickFpsSettingSwitchBtn();
            Thread.sleep(10*1000);
        }else{
            Thread.sleep(10*1000);
            takeScreenshot(screenShotName1);
            clickFpsSettingSwitchBtn();
            confirmFpsSetting("未登記");
        }
        takeScreenshot(screenShotName2);
    }

    @SneakyThrows
    public void confirmFpsSetting(String flag){
        if(isShow(deleteBtn,30)){
            clickElement(deleteBtn);
        }
        Thread.sleep(10*10000);
        String fpsContent = fpsIdentity.getText();
        boolean isRegisterFps = fpsContent.contains(flag);
        if(!isRegisterFps){
            assertThat(false);
        }
    }

    public void clickFpsSettingSwitchBtn(){
        clickByLocationByPercent(82,83);
    }

    @SneakyThrows
    public void openPaymentPage(String screenShotName,String screenShotName1) {
        waitUntilElementVisible(transferTab);
        Thread.sleep(1000);
        theAccountAvailableBalanceBeforeTrans=goSavePage.getAccountAvailableBalance();
        System.out.println("theAccountAvailableBalanceBeforeTrans: "+theAccountAvailableBalanceBeforeTrans);
        waitUntilElementClickable(transferTab);
        takeScreenshot(screenShotName);
        scrollUp();
        Thread.sleep(1000 * 10);
        waitUntilElementVisible(cardPage.transactionDetails);
        transactionText = cardPage.transactionDetails.getText();
        takeScreenshot(screenShotName1);

        transferTab.click();
        Thread.sleep(2000);
        waitUntilElementVisible(transactionPageTitle);
        //waitUntilElementClickable(transactionItem, 60);
        waitUntilElementVisible(transactionItem);
    }

    @SneakyThrows
    public void openPaymentPageWithoutTransaction(String screenShotName) {
        waitUntilElementVisible(transferTab);
        Thread.sleep(1000);
        theAccountAvailableBalanceBeforeTrans=goSavePage.getAccountAvailableBalance();
        System.out.println("theAccountAvailableBalanceBeforeTrans: "+theAccountAvailableBalanceBeforeTrans);
        waitUntilElementClickable(transferTab);
        Thread.sleep(2000);
        transferTab.click();
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        waitUntilElementVisible(transactionPageTitle);
    }

    public void inputBankAccount(String account){
        commonPage.clickSoftKeyByNumberString(account);
    }
    public void inputAmount(String amount){
        commonPage.clickSoftKeyByNumberString(amount);
    }
    public void inputMSK(){
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        commonPage.inputMSKByString(mskPassword);
    }

    public void clickFirstBankItem(){
        clickElement(welabBank);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"390 Welab Bank Limited\"]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"390 Welab Bank Limited\"])[2]"),
    })
    private MobileElement welabBank;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"672 672 銀行\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"672 672 银行\"]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"672 672 銀行\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"672 672 銀行\"])[3]"),
    })
    private MobileElement bank672;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"收款人銀行\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"收款人银行\" AND name == \"text\""),
    })
    private MobileElement bankListTitle;


    public void click672BankItem(){
        //clickElement(bank672);
        int height = driver.manage().window().getSize().getHeight();
        int ele_height = bankListTitle.getLocation().getY();
        int yPercent = ele_height * 100/ height + 15 ;
        clickByLocationByPercent(50, yPercent);
    }


    public void slipeConfirmBtnWithImage() {
        int yPercent = 0;
        String picturePath = "";
        if(isIphoneSE()){
            picturePath = "src/main/resources/images/channel/payment/slide1.png";
            yPercent=getYlocation(picturePath,50,70);
        }else if(isIphone7Plus()){
            picturePath = "src/main/resources/images/channel/payment/Slide.png";
            yPercent=getYlocation(picturePath,50,70);
        }else if(isIphone13()){
            picturePath = "src/main/resources/images/channel/payment/slideIphone13.png";
            yPercent=getYlocation2(picturePath,50,10);
        }else{
            picturePath = "src/main/resources/images/channel/payment/slide1.png";
            yPercent=getYlocation(picturePath,50,70);
        }
        // x =  18 ~ 95 %   // y = 64 %
        scrollByLocationPercentFromOnePointToOtherPoint(18, yPercent, 95, yPercent);
    }

    public void slipeConfirmBtn() {
        /*
        int yPercent = 0;
        String picturePath = "";
        if(isIphoneSE()){
            picturePath = "src/main/resources/images/channel/payment/slide1.png";
            yPercent=getYlocation(picturePath,50,70);
        }else if(isIphone7Plus()){
            picturePath = "src/main/resources/images/channel/payment/Slide.png";
            yPercent=getYlocation(picturePath,50,70);
        }else if(isIphone13()){
            picturePath = "src/main/resources/images/channel/payment/slideIphone13.png";
            yPercent=getYlocation2(picturePath,50,10);
        }else{
            picturePath = "src/main/resources/images/channel/payment/slide1.png";
            yPercent=getYlocation(picturePath,50,70);
        }
        // x =  18 ~ 95 %   // y = 64 %
        scrollByLocationPercentFromOnePointToOtherPoint(18, yPercent, 95, yPercent);
        */
        slipeConfirmBtnByEle();
    }

    public void slipeConfirmBtnByEle() {
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        //int y = getIOSAttributePoints(slideConfirmBtn,"y") + height * 3 /100;
        int y = getElementCenterY(slideConfirmBtn);
        System.out.println("height: "+height);
        System.out.println("y: "+y);
        final int start_x = width * 18 / 100;
        final int end_x = width * 95 / 100;
        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        PointOption pointOption1 = PointOption.point(start_x, y);
        PointOption pointOption2 = PointOption.point(end_x, y);
        Duration duration = Duration.ofMillis(1000);
        WaitOptions waitOptions = WaitOptions.waitOptions(duration);
        touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
                .perform();
    }

    @SneakyThrows
    public void selectBank(String screenShotName){
        waitUntilElementVisible(transactionPageTitle);
        clickElement(transactionItem);
        Thread.sleep(3000);
        checkGoSaveAlertOnTransactionPage();
        clickElement(bankTransaction);
        Thread.sleep(2000);
        clickFirstBankItem();
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void select672Bank(String screenShotName){
        waitUntilElementVisible(transactionPageTitle);
        clickElement(transactionItem);
        Thread.sleep(2000);
        checkGoSaveAlertOnTransactionPage();
        clickElement(bankTransaction);
        Thread.sleep(2000);
        andSendKeys(searchBankInput,"672");
        Thread.sleep(2000);
        click672BankItem();
        click672BankItem();
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void transactionInputData(String amount,String account,String name,String screenShotName){
        inputBankAccount(account);
        andSendKeys(theAccountNameInput,name);
        inputEnter(theAccountNameInput);
        clickElement(nextStepBtn);
        transactionAmount = goSavePage.strToDouble(amount);
        waitUntilElementVisible(transactionDetailPageTitle);
        inputAmount(amount);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
        Thread.sleep(3000);
    }

    public void inputAmountAndSlideConfirm(String amount,String screenShotName){
        inputAmount(amount);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
    }

    @SneakyThrows
    public void confirmTransaction(String screenShotName){
        Thread.sleep(1000);
        waitUntilElementVisible(MSKpage);
        inputMSK();
        Thread.sleep(5000);
        //waitUntilElementVisible(FinishBtn);
        Thread.sleep(8000);
        //waitUntilElementVisible(transactionsSuccessItem);
        waitUntilElementVisible(FinishBtn);
        takeScreenshot(screenShotName);
        clickElement(FinishBtn);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void confirmTransactionWithLimitMessage(String screenShotName){
        Thread.sleep(3000);
        waitUntilElementVisible(outstripLimitAmountTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        waitUntilElementVisible(cancelBtn);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void confirmBillNumberTransaction(String screenShotName){
        Thread.sleep(1000);
        waitUntilElementVisible(MSKpage);
        inputMSK();
        Thread.sleep(5000);
        waitUntilElementVisible(FinishBtn);
        Thread.sleep(200);
        //waitUntilElementVisible(billNumberTransactionsSuccessItem);
        takeScreenshot(screenShotName);
        clickElement(FinishBtn);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public boolean verifySendMoneyFailRecordByBank(String screenShotName, String screenShotName1) {
        Thread.sleep(5000);
        clickElement(doneBtn);
        Thread.sleep(5000);
        clickHomePageRecord();
        Thread.sleep(5000);
        takeScreenshot(screenShotName);

        String regEx = "[^0-9]+";
        Pattern pattern = Pattern.compile(regEx);
        String SecondRecordAmountStr = pattern.split(amountAtTransSuccess.getText())[1];
        String SecondRecordNameStr = traderNameAtTransSuccess.getText();
        String SecondRecordAccountNumber = idNumberAtTransSuccess.getText();
        clickElement(backBtnInRecordPage);
        Thread.sleep(1000);
        scrollUp();
        Thread.sleep(1000);
        clickElement(SeocndTraderNameAtHome);
        Thread.sleep(5000);
        takeScreenshot(screenShotName1);
        String FirstRecordAmountStr = pattern.split(amountAtTransSuccess.getText())[1];
        String FirstRecordNameStr = traderNameAtTransSuccess.getText();
        String FirstRecordAccountNumber = idNumberAtTransSuccess.getText();


        assertThat(FirstRecordAmountStr).isEqualTo(SecondRecordAmountStr);
        assertThat(FirstRecordNameStr).isEqualTo(SecondRecordNameStr);
        assertThat(FirstRecordAccountNumber).isEqualTo(SecondRecordAccountNumber);
        if(traderName.equals(FirstRecordNameStr) && goSavePage.strToDouble(FirstRecordAmountStr)==TransactionAmount&&FirstRecordAccountNumber.equals(SendMoneyAccount)){
            double FirstMoney = goSavePage.strToDouble(amountAtTrans.getText());
            double SecondMoney = goSavePage.strToDouble(amountAtTrans.getText());
            return FirstMoney + SecondMoney == 0;
        }
        return false;
    }



    @SneakyThrows
    public void confirmTransactionWithoutMsk(String screenShotName){
        Thread.sleep(3000);
        waitUntilElementVisible(FinishBtn);
        Thread.sleep(5000);
        waitUntilElementVisible(transactionsSuccessItem);
        takeScreenshot(screenShotName);
        clickElement(FinishBtn);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void checkTransactionFail(String screenShotName){
        Thread.sleep(3000);
        waitUntilElementVisible(MSKpage);
        inputMSK();
        Thread.sleep(5000);
        waitUntilElementVisible(FinishBtn);
        Thread.sleep(10000);
        waitUntilElementVisible(transactionsFailItem);
        clickElement(FinishBtn);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkAvailableAmount(String screenShotName){
        Thread.sleep(5*1000);
        scrollDown();
        double availableAmount = goSavePage.getAccountAvailableBalance();
        scrollUp();
        Thread.sleep(1000);
        waitUntilElementVisible(cardPage.transactionDetails);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        double calcTotalBalance = theAccountAvailableBalanceBeforeTrans - transactionAmount;
        transactionAmount =0.0;
        assertThat(availableAmount).isEqualTo(calcTotalBalance);
    }

    @SneakyThrows
    public void checkTransationOnHomePage(String screenShotName,String screenShotName1){
        if(!GlobalVar.IS_CHECK_TRANSACTION) return;
        scrollDown();
        scrollDown();
        Thread.sleep(1000 * 5);
        scrollUp();
        waitUntilElementVisible(cardPage.transactionDetails);
        Thread.sleep(1000 * 2);
        takeScreenshot(screenShotName);
        clickTodayLastTransaction();
        waitUntilElementVisible(transactionDetailPage);
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
    }

    public void clickTodayLastTransaction(){
        //the last transaction  under below  currentlyTransactions  13% height
        int eleHeight = getIOSAttributePoints(cardPage.currentlyTransactions,"y");
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int x = width * 25 / 100;
        int y = height * 13 / 100 +eleHeight;
        clickByLocation(x,y);
    }

    public int getIOSAttributePoints(WebElement element,String aName){
        WebElement seekBar = element;
        String rect=seekBar.getAttribute("rect");
        if(aName=="y"){
            String strx= rect.split(",")[0].toString().split(":")[1];
            return (Integer.parseInt(strx));
        }
        else if(aName=="x"){
            String stry= rect.split(",")[1].toString().split(":")[1];
            return (Integer.parseInt(stry));
        }
        else if (aName.toLowerCase()=="width"){
            String strx= rect.split(",")[2].toString().split(":")[1];
            return (Integer.parseInt(strx));
        }
        return 0;
    }

    public String get(String text){
        System.out.println("text: "+text);
        text = text.substring(text.indexOf("(HKD)")+5).trim();
        String lastTransactionId = text.split(" ")[0].trim();
        return lastTransactionId;
    }
    @SneakyThrows
    public void clickTransferPage() {
        Thread.sleep(3 * 1000);
        waitUntilElementClickable(transferTab);
        transferTab.click();
    }
    @SneakyThrows
    public void clicksendMoneyAtTransfer() {
        Thread.sleep(2 * 1000);
        waitUntilElementClickable(sendMoneyAtTransfer);
        sendMoneyAtTransfer.click();
    }
    @SneakyThrows
    public void sendToFPSID(String FPSID) {
        FPSid = FPSID;
        Thread.sleep(2000);
        clearAndSendKeys(FPSIDEdit, FPSID);
        clickElement(sendMoneyNext);
    }

    @SneakyThrows
    public void enterBillAndPay(String billnum, String amount) {
        traderName = traderNameAtBill.getText();
        billNumber = billnum;
        TransactionAmount = Double.parseDouble(amount);
        clearAndSendKeys(billNumAtBill, billnum);
        inputEnter(billNumAtBill);
        Thread.sleep(3000);
        clearAndSendKeys(amountAtBill, amount);
        inputEnter(amountInput);
        Thread.sleep(3000);
        slideToPay();
        Thread.sleep(3000);
        commonPage.waiteSendMsk();
    }

    @SneakyThrows
    public boolean transferInProgress(String screenShotName) {
        takeScreenshot(screenShotName);
        if(isShow(sendMoneySubmit,2)){
            String progressMoneyAmount=submitMoneyAmount.getText().split("-")[1].split("HKD")[0];
            if(goSavePage.strToDouble(progressMoneyAmount)==TransactionAmount&&fpsidOnDetail.getText().contains(FPSid)) {
                Thread.sleep(2000);
                clickElement(doneBtn);
                return true;
            }
        }else{
            Thread.sleep(5000);
            String progressMoneyAmount=sendMoneyAmount.getText().split("-")[1].split("HKD")[0];
            if(verifyElementExist(sendMoneySuccess)){
                if(goSavePage.strToDouble(progressMoneyAmount)==TransactionAmount&&
                        fpsidOnDetail.getText().contains(FPSid))
                {
                    Thread.sleep(2000);
                    clickElement(doneBtn);
                    return true;
                }
            }
        }
        return false;
    }

    @SneakyThrows
    public void verifyTransactions(String screenShotName, String screenShotName1) {
        waitUntilElementVisible(traderNameAtHome);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        clickElement(traderNameAtHome);
        if (FPSidAtTrans.getText().contains("-")) {
            assertThat(FPSidAtTrans.getText().split("-")[1]).isEqualTo(FPSid);
        } else {
            assertThat(billNameAtTrans.getText()).isEqualTo(billNumber);
            assertThat(FPSidAtTrans.getText()).isEqualTo(FPSid);
        }
        assertThat(Double.parseDouble(amountAtTransSuccess.getText().replaceAll(",", "").split("[^0-9]+")[1]))
                .isEqualTo(TransactionAmount);
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
        clickElement(backBtnInRecordPage);
    }

    @SneakyThrows
    public boolean verifyMerhantInfo(String screenShotName) {
        boolean flag = FPSidAtBill.getText().contains(FPSid);
        traderName = traderNameAtBill.getText();
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        return flag;
    }

    @SneakyThrows
    public void verifyAvailableBalance(String screenShotName) {
        Thread.sleep(3000);
        theAccountAvailableBalanceBeforeTrans = goSavePage.getAccountAvailableBalance();
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void slideToPay() {
        int x = slideToPay.getLocation().x;

        int y = slideToPay.getLocation().y;
        int width = slideToPay.getRect().width;
        int x_start = x + 80;
        int x_end = x + width + 800;
        int y_co = y + 2;
        TouchAction action =
                new TouchAction(driver)
                        .longPress(PointOption.point(x_start, y_co))
                        .moveTo(PointOption.point(x_end, y_co))
                        .release();
        action.perform();
        Thread.sleep(1000 * 10);
    }

    @SneakyThrows
    public void runFpsTransaction(String amount,String FpsId,String screenShotName){
        transactionAmount = goSavePage.strToDouble(amount);
        Thread.sleep(1000);
        clickElement(transactionItem);
        Thread.sleep(2000);
        checkGoSaveAlertOnTransactionPage();
        clickElement(FpsInput);
        andSendKeys(FpsInputField,FpsId);
        inputEnter(FpsInput);
        Thread.sleep(1000);
        clickElement(nextStepBtn);
        Thread.sleep(5000);
        //waitUntilElementVisible(transactionDetailPageTitle);
        inputAmountAndSlideConfirm(amount,screenShotName);
    }

    @SneakyThrows
    public void clickAddPayee(){
        // x = 7%  get addMyPayee height,add 1 % to click middle of height
        scrollUpToFindElement(addMyPayee,2,2);
        int addPayeeHeight = getIOSAttributePoints(addMyPayee,"y");
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        final int y = width * 1 / 100+addPayeeHeight;
        final int x = width * 7 / 100;
        clickByLocation(x,y);
    }

    @SneakyThrows
    public  void runFPSwithAddPayee(String amount,String FpsId,String screenShotName){
        transactionAmount = transactionAmount + goSavePage.strToDouble(amount);
        clickElement(transactionItem);
        Thread.sleep(2000);
        checkGoSaveAlertOnTransactionPage();
        clickElement(FpsInput);
        andSendKeys(FpsInputField,FpsId);
        inputEnter(FpsInput);
        Thread.sleep(1000);
        clickElement(nextStepBtn);
        Thread.sleep(5000);
        //waitUntilElementVisible(transactionDetailPageTitle);
        inputAmount(amount);
        Thread.sleep(1000);
        scrollUpByStartEndHeightPercent(50,20);
        Thread.sleep(1000);
        clickElement(inputYourInfoEle);
        Thread.sleep(1000);
        if(isShow(softKeyDone, 3)){
            clickElement(softKeyDone);
        }
        //clickPageToHideKeyBoard();
        Thread.sleep(1000);
        clickAddPayee();
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
    }

    public void clickPageToHideKeyBoard(){
        clickByLocationByPercent(15,33);
    }

    public void clickDeletePayeeBtn(){
        if(GlobalVar.IS_USE_OPENCV){
            clickByPicture2("src/main/resources/images/channel/payment/PayeeDeleteBtn.png",50,50);
        }else{
            clickByLocationByPercent(90,26);
        }
    }
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"我的收款人\"])[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther")
    private MobileElement myPayeeSettingBtn;
    public void clickmyPayeePageSettingBtn(){
        //clickElement(myPayeeSettingBtn);
        if(GlobalVar.IS_USE_OPENCV){
            clickByPicture2("src/main/resources/images/channel/payment/PayeeSettingBtn.png",50,50);
        }else{
            clickByLocationByPercent(90,6);
        }
    }
    public void clickPayeeItem(){
        clickByLocationByPercent(40,26);
        //clickByPicture2("src/main/resources/images/channel/payment/FirstPayeeItem.png",50,50);
    }
    @SneakyThrows
    public void goToTransactionPage(){
        transferTab.click();
        Thread.sleep(2000);
        waitUntilElementVisible(transactionItem);
        clickElement(transactionItem);
        Thread.sleep(2000);
        waitUntilElementVisible(myPayeeItem);
    }

    @SneakyThrows
    public  void runFPSwithSelectPayee(String amount,String screenShotName){
        transactionAmount = transactionAmount + goSavePage.strToDouble(amount);
        goToTransactionPage();
        clickElement(myPayeeItem);
        waitUntilElementVisible(myPayeePageTitle);
        Thread.sleep(3000);
        clickPayeeItem();
        Thread.sleep(1000);
        waitUntilElementVisible(transactionDetailPageTitle);
        Thread.sleep(1000);
        inputAmount(amount);
        clickPageToHideKeyBoard();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
    }

    public void moveEyeBtnToMiddleRight() {
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        TouchAction action = new TouchAction(driver);
        action.press(PointOption.point(width*88/100, height*26/100))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(width, height/2))
                .release()
                .perform();
        action.press(PointOption.point(width*92/100, height*22/100))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(width, height/2))
                .release()
                .perform();
    }

    @SneakyThrows
    public void goToHomePage() {
        Thread.sleep(5000);
        waitUntilElementVisible(homeTab);
        clickElement(homeTab);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void deletePayee(){
        goToTransactionPage();
        clickElement(myPayeeItem);
        Thread.sleep(1000);
        waitUntilElementVisible(myPayeePageTitle);
        if(!isShow(youNotHavePayee,3)){
            moveEyeBtnToMiddleRight();
            clickmyPayeePageSettingBtn();
            Thread.sleep(1000);
            clickDeletePayeeBtn();
            Thread.sleep(2000);
            waitUntilElementVisible(confirmDeleteBtn);
            clickElement(confirmDeleteBtn);
            Thread.sleep(3000);
        }
        goSavePage.clickTopLeftBtnToExit();
        Thread.sleep(3000);
        goSavePage.clickTopLeftBtnToExit();
        Thread.sleep(3000);

    }

    public void clickBankTransfer(){
        waitUntilElementClickable(bankTransfer);
        clickElement(bankTransfer);
    }

    @SneakyThrows
    public void chooseWeBank(){
        Thread.sleep(2000);
        clearAndSendKeys(searchInput,"390");
        inputEnter(searchInput);
        Thread.sleep(2000);
        clickElement(WeBank390);
    }

    @SneakyThrows
    public void sendMoneyByBank(String account,String name,String money,String screenShotName){
        TransactionAmount = goSavePage.strToDouble(money);
        traderName = name;
        SendMoneyAccount = account;
        Thread.sleep(2000);
        clearAndSendKeys(accountNumberInput,account);
        clearAndSendKeys(holdNameInput,name);
        if (isShow(sendMoneyNext)){
            clickElement(sendMoneyNext);
        }else {
            hideAndroidKeyboard();
            waitUntilElementClickable(sendMoneyNext);
            clickElement(sendMoneyNext);
        }
        Thread.sleep(2000);
        clearAndSendKeys(amountInput,money);
        takeScreenshot(screenShotName);
        slideToPay();
        Thread.sleep(3000);
        commonPage.waiteSendMsk();
    }

    @SneakyThrows
    public boolean sendMoneyDetailByBank(String screenShotName){
        Thread.sleep(2000);
        boolean flag = verifyElementExist(InProgress);
        assertThat(flag).isTrue();
        takeScreenshot(screenShotName);
        String progressMoneyAmount=amountAtProgress.getText().split("-")[1].split("HKD")[0];
        if(flag){
            if(goSavePage.strToDouble(progressMoneyAmount)==TransactionAmount&&
                    traderNameAtProgress.getText().equals(traderName)&&
                    progressAccount.getText().equals(SendMoneyAccount))
            {
                waitUntilElementClickable(doneAtProgress);
                clickElement(doneAtProgress);
                return true;
            }
        }

        return false;
    }

    @SneakyThrows
    public boolean verifyAvailableBalanceByBank(String screenShotName) {
        Thread.sleep(2000);
        if(isShow(backBtnInRecordPage,2)){
            clickElement(backBtnInRecordPage);
        }
        Thread.sleep(2000);
        scrollDown();
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        theAccountAvailableBalanceAfterTrans=goSavePage.getAccountAvailableBalance();
        logger.info(theAccountAvailableBalanceAfterTrans+"AvBalanceAfterSendMoney");
        logger.info(theAccountAvailableBalanceBeforeTrans+"AvailableBalance");
        logger.info(TransactionAmount+"sendMoneyAmount");
        return theAccountAvailableBalanceAfterTrans == theAccountAvailableBalanceBeforeTrans - TransactionAmount;
    }


    @SneakyThrows
    public boolean verifyFailAvailableBalanceByBank(String screenShotName) {
        Thread.sleep(2000);
        clickElement(backBtnInRecordPage);
        Thread.sleep(2000);
        scrollDown();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        theAccountAvailableBalanceAfterTrans=goSavePage.getAccountAvailableBalance();
        logger.info(theAccountAvailableBalanceAfterTrans+"AvBalanceAfterSendMoney");
        logger.info(theAccountAvailableBalanceBeforeTrans+"AvailableBalance");
        logger.info(TransactionAmount+"sendMoneyAmount");
        return theAccountAvailableBalanceAfterTrans == theAccountAvailableBalanceBeforeTrans;
    }


    @SneakyThrows
    public void chooseBank672(){
        Thread.sleep(2000);
        waitUntilElementClickable(searchInput);
        clearAndSendKeys(searchInput,"672");
        inputEnter(searchInput);
        Thread.sleep(2000);
        clickElement(Bank672);
    }

    @SneakyThrows
    public void checkGoSaveAlertOnTransactionPage(){
        if(isShow(gosaveAlertOnTransactionPage,2)){
            clickElement(gosaveAlertOnTransactionPage);
        }
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void runTransactionInputDataWithBillNumber(String amount, String FpsId,String billNumber,String screenShotName){
        transactionAmount = transactionAmount + goSavePage.strToDouble(amount);
        clickElement(transactionItem);
        Thread.sleep(2000);
        waitUntilElementVisible(FpsInput);
        checkGoSaveAlertOnTransactionPage();
        if(isIphone13()){
            clickElement(FpsInput);
            Thread.sleep(1000);
            andSendKeys(FpsInputField,FpsId);
            inputEnter(FpsInputField);
        }else{
            andSendKeys(FpsInput,FpsId);
            inputEnter(FpsInput);
        }
        Thread.sleep(1000);
        clickElement(nextStepBtn);
        Thread.sleep(5000);
        waitUntilElementVisible(merchanrPageTitle);
        inputBillNumberBySoftkey(billNumber);//DBSBillnumbertestingTerry
        clickPageToHideKeyBoard();
        clickAmountInput();
        inputAmount(amount);
        clickPageToHideKeyBoard();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
    }

    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '金額 (HKD) 戶口結餘: HKD')]")
    private MobileElement amountInputForPayment;
    public void clickAmountInput(){
        //clickByLocationByPercent(20,46);
        //clickByPicture2("src/main/resources/images/channel/payment/AmountInput.png",50,20);
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int y = getElementCenterY(amountInputForPayment);
        int yy = getYPercentByElement(amountInputForPayment);
        int xx = getXPercentByElement(amountInputForPayment);
        int yPercent = (int) y*100/ height - 2;
        Dimension d = amountInputForPayment.getSize();
        int eleWidth = d.width;
        int eleHeight = d.height;
        logger.info("width: "+width);
        logger.info("height: "+height);
        logger.info("getElementCenterY: "+yPercent);
        logger.info("getYPercentByElement: "+yy);
        logger.info("getXPercentByElement: "+xx);
        logger.info("eleWidth: "+eleWidth);
        logger.info("eleHeight: "+eleHeight);
        clickByLocationByPercent(50,yPercent);
    }
    public void inputBillNumberBySoftkey(String billNumber){
        commonPage.clickShift();
        commonPage.clickSoftKeyByNumberString(billNumber);
    }

    @SneakyThrows
    public void enterAmountAndPay(String amount) {
        Thread.sleep(3000);
        TransactionAmount = Double.parseDouble(amount);
        clearAndSendKeys(amountInput, amount);
        inputEnter(amountInput);
        Thread.sleep(3000);
        slideToPay();
        Thread.sleep(3000);
        commonPage.waiteSendMsk();
    }

    @SneakyThrows
    public void enterAmountAndPayWithPayee(String amount) {
        TransactionAmount = Double.parseDouble(amount);
        Thread.sleep(3000);
        clearAndSendKeys(amountAtBill, amount);
        inputEnter(amountAtBill);
        slideToPay();
        if(TransactionAmount>=10000){
            commonPage.waiteSendMsk();
        }
    }

    @SneakyThrows
    public boolean verifyNonePayeePage(String screenShotName) {
        Thread.sleep(3000);
        hideKeyboard();
        boolean flag =verifyElementExist(NonePayee);
        takeScreenshot(screenShotName);
        return flag;
    }
    @SneakyThrows
    public boolean payeeAlready(String screenShotName) {
        boolean flag =FPSidAtMyPayeeAlready.getText().split("-")[1].contains(FPSid);
        Thread.sleep(3000);
        hideKeyboard();
        assertThat(verifyElementExist(myPaeeText)).isFalse();
        takeScreenshot(screenShotName);
        return flag;
    }

    public void AddPayee() {
        clickElement(NonePayee);
    }

    @SneakyThrows
    public void clickMyPayeePage() {
        Thread.sleep(1000 * 3);
        waitUntilElementClickable(myPayeePage);
        clickElement(myPayeePage);
    }

    @SneakyThrows
    public void verifyNoMyPayee(String FPSID) {
        FPSid = FPSID;
        clearAndSendKeys(FPSIDEdit, FPSID);
        inputEnter(FPSIDEdit);
        if (verifyElementExist(FPSidAtMyPayee)) {
            if (FPSidAtMyPayee.getText().split("-")[1].contains(FPSID)) {
                slideToDeleteMyPayee();
                clickElement(deleteBtnAtMyPayee);
                waitUntilElementClickable(confirmDeleteBtn);
                clickElement(confirmDeleteBtn);
            }
        } else {
        }
        Thread.sleep(1000 * 3);
        clickElement(backBtnAtMyPayee);
    }

    public void searchFPSIDatMyPayee(String FPSID) {
        clickElement(NonePayee);
    }

    @SneakyThrows
    public void slideToDeleteMyPayee() {
        int x = FPSidAtMyPayee.getLocation().x;
        int y = FPSidAtMyPayee.getLocation().y;
        int x_start = x + 200;
        int x_end = x - 300;
        int y_co = y + 2;
        TouchAction action =
                new TouchAction(driver)
                        .longPress(PointOption.point(x_start, y_co))
                        .moveTo(PointOption.point(x_end, y_co))
                        .release();
        action.perform();
        Thread.sleep(1000 * 8);
    }

    @SneakyThrows
    public void clickNewPayee() {
        Thread.sleep(1000 * 3);
        clearAndSendKeys(FPSIDEdit, FPSid);
        inputEnter(FPSIDEdit);
        Thread.sleep(1000 * 5);

//        waitUntilElementClickable(FPSidAtMyPayee);
//        clickElement(FPSidAtMyPayee);
        clickElement(FPSidAtMyPayee);
    }

    @SneakyThrows
    public boolean verifySendMoneyByBankLimit(String account, String name, String money, String screenShotName, String screenShotName1) {
        Thread.sleep(2000);
        clearAndSendKeys(accountNumberInput, account);
        inputEnter(accountNumberInput);
        clearAndSendKeys(holdNameInput, name);
        inputEnter(holdNameInput);

        waitUntilElementClickable(sendMoneyNext);
        clickElement(sendMoneyNext);
        Thread.sleep(2000);
        clearAndSendKeys(amountInput, money);
        inputEnter(amountInput);
        takeScreenshot(screenShotName);
        slideToPay();
        takeScreenshot(screenShotName1);

        return goSavePage.strToDouble(getLimitMoney()) <= goSavePage.strToDouble(money);
    }


    public String getLimitMoney(){
        String limitMoney;
        if(limitTextTitle.getText().startsWith("Exceed")){
            limitMoney = limitText.getText().split("HKD")[1].split("Please")[0].split("\\.")[0];
        }else{
            limitMoney = limitText.getText().split("HKD")[1].split("。 請")[0].split("\\.")[0];
        }
        return limitMoney;
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"存款\"])[5]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"存款\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@value=\"存款\"]"),
    })
    private MobileElement addMoneyPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"金額 (HKD)\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"金额 (HKD)\"])[2]"),
    })
    private MobileElement addMoneyInput;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[2]"),
    })

    private MobileElement confirmAddMoneyBtn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '轉賬成功')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '转账成功')]"),
    })
    private MobileElement addMoneySuccessMessage;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"完成\"])[2]")
    private MobileElement addMoneyFinish;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"重複 - 選擇規則 -\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"重复 - 选择规则 -\"]"),
    })
    private MobileElement selectRuleItem;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[4]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[4]"),
    })
    private MobileElement selectRuleConfirmBtn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"戶口結餘低於此水平時入錢 (HKD)\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"账户余额低于此水平时充值 (HKD)\"])[4]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement remainAmountInput;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[3]"),
    })
    private MobileElement remainAmountConfirmBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"自動入錢設定\"])[3]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"自动充值设置\" AND name == \"text\""),
    })
    private MobileElement addMoneyRulePageTitle;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"存款\"])[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther")
    private MobileElement addAmountRuleEditBtn;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"自動入錢設定\"])[1]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"自动入钱设置\"])[1]"),
    })
    private MobileElement autoAddMoneySettingItem;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"刪除此規則\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"删除此规则\" AND name == \"text\""),
    })
    private MobileElement deleteTheRuleBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确定\"]"),
    })
    private MobileElement deleteRuleConfirmBtn;

    @SneakyThrows
    public void runAddMoney(String amount,String screenShotName1,String screenShotName2){
        goToAddMoneyAndInputMoney(amount,screenShotName1);

        clickElement(confirmAddMoneyBtn);
        commonPage.inputMSK();
        Thread.sleep(10000);
        waitUntilElementVisible(addMoneySuccessMessage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickElement(addMoneyFinish);
        Thread.sleep(2000);
        clickElement(homeTab);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void goToAddMoneyAndInputMoney(String amount,String screenShotName1){
        transactionAmount = 0 - goSavePage.strToDouble(amount);
        waitUntilElementVisible(addMoneyItem);
        Thread.sleep(1000);
        clickElement(addMoneyItem);
        waitUntilElementVisible(addMoneyPageTitle);
        Thread.sleep(2000);
        andSendKeys(addMoneyInput,amount);
        inputEnter(addMoneyInput);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
    }

    public void clickAddMoneyRuleSwithBtn(){
        /**
        if(isIphoneSE()){
            clickByLocationByPercent(83, 56);  //iphone se
        }else if(isIphone7Plus()){
            int yPercent = getYlocation2("src/main/resources/images/channel/payment/AddMoneySwitch2.png",170,25);
            clickByLocationByPercent(83, yPercent);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(83,44.2);
        }else if(isIphone13()){
            clickByLocationByPercent2(83,47.1);
        }else{
            clickByLocationByPercent(83, 56);  //iphone se
        }**/
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int y = getElementCenterY(addMoneySwitchButton);
        int yPercent = (int)y*100/height;
        clickByLocationByPercent(83, yPercent);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"自動入錢 設定戶口自動增值，為下次俾錢時做足準備\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"自动充值 设定账户自动增值，为下次付钱时做好准备\"]"),
    })
    private MobileElement addMoneySwitchButton;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確定\"])[5]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确定\"])[5]"),
    })
    private MobileElement confirmButtonForAddMoneyRule;

    public void clickConfirmForRuleTypes(){
        clickElement(confirmButtonForAddMoneyRule);
    }
    public void clickAddMoneyFirstRule(){
        if(GlobalVar.IS_USE_OPENCV){
            clickByPicture2("src/main/resources/images/channel/payment/AddMoneyFirstRule.png",50,50);
        }else{
            clickByLocationByPercent(50,32);
        }
    }

    @SneakyThrows
    public void setUpAutoEddiRules(String amount,String remainAmount,String screenShotName1,String screenShotName2,String screenShotName3) {
        goToAddMoneyAndInputMoney(amount,screenShotName1);

        clickAddMoneyRuleSwithBtn();
        Thread.sleep(2000);
        waitUntilElementVisible(selectRuleItem);
        clickElement(selectRuleItem);
        Thread.sleep(2000);
        clickConfirmForRuleTypes();
        Thread.sleep(2000);
        andSendKeys(remainAmountInput,remainAmount);
        inputEnter(remainAmountInput);
        Thread.sleep(1000);
        clickElement(remainAmountConfirmBtn);
        Thread.sleep(1000);
        waitUntilElementVisible(addMoneyPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickElement(confirmAddMoneyBtn);
        Thread.sleep(5000);
        commonPage.inputMSK();
        Thread.sleep(1000);
        waitUntilElementVisible(addMoneyRulePageTitle);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void deleteAutoEddiRules(String screenShotName1,String screenShotName2,String screenShotName3) {
        waitUntilElementVisible(addMoneyItem);
        Thread.sleep(1000);
        clickElement(addMoneyItem);
        waitUntilElementVisible(addMoneyPageTitle);
        Thread.sleep(1000);
        clickElement(addAmountRuleEditBtn);
        Thread.sleep(1000);
        waitUntilElementVisible(autoAddMoneySettingItem);
        clickElement(autoAddMoneySettingItem);
        Thread.sleep(2000);
        waitUntilElementVisible(addMoneyRulePageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickAddMoneyFirstRule();
        Thread.sleep(2000);
        waitUntilElementVisible(deleteTheRuleBtn);
        clickElement(deleteTheRuleBtn);
        Thread.sleep(1000);
        waitUntilElementVisible(deleteRuleConfirmBtn);
        clickElement(deleteRuleConfirmBtn);
        Thread.sleep(3000);
        takeScreenshot(screenShotName2);
        waitUntilElementVisible(addMoneyRulePageTitle);
        Thread.sleep(10000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void clickAddMoneyBtn(String screenShotName) {
        Thread.sleep(2000);
        waitUntilElementClickable(addMoneyBtn);
        clickElement(addMoneyBtn);
        Thread.sleep(10000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public boolean enterMoney(String money,String screenShotName) {
        clearAndSendKeys(amountInput,money);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        TransactionAmount=goSavePage.strToDouble(money);
        Thread.sleep(5000);
        if(repeatText.getText().equals("自動入錢")||repeatText.getText().equals("Auto Reload")){
            clickElement(confirmBtn);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean addMoneySuccess(String screenShotName) {
        Thread.sleep(5000);
        commonPage.waiteSendMsk();
        takeScreenshot(screenShotName);
        Thread.sleep(5000);
        String addMoneyAmount;
        Thread.sleep(5000);
        if(isShow(InProgress,2)){
            addMoneyAmount = inProgressAmount.getText().split("\\+")[1].split("HKD")[0];
        } else if (verifyElementExist(addMoney)) {
            addMoneyAmount = amountAtProgressing.getText().split("\\+")[1].split("HKD")[0];
        } else {
            return false;
        }
        if (goSavePage.strToDouble(addMoneyAmount) == TransactionAmount) {
            Thread.sleep(2000);
            clickElement(doneBtn);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifyAddMoneyRecord(String screenShotName) {
        waitUntilElementClickable(goSavePage.getTheAccountAvailableBalance());
        Thread.sleep(5000);

        clickHomePageRecord();

        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        double recordAmount = getRecordAmount();
        if(recordAmount==TransactionAmount && verifyElementExist(receiveMoney)){
            clickElement(backBtnInRecordPage);
            Thread.sleep(2000);
            scrollDown();
            return true;
        }
        return false;
    }

    public double getRecordAmount(){
        String recordAmountStrFirst = pattern.split(amountAtTrans.getText())[1].concat(".");
        String recordAmountStr =recordAmountStrFirst.concat(pattern.split(amountAtTrans.getText())[2]) ;
//        String recordAmountStr = amountAtTrans.getText().split("\\+")[1];
        return goSavePage.strToDouble(recordAmountStr);
    }

    @SneakyThrows
    public boolean verifyAddMoneyBalance(String screenShotName) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        double balance =goSavePage.getAccountAvailableBalance();
        return theAccountAvailableBalanceBeforeTrans == balance - TransactionAmount;
    }

    @SneakyThrows
    public void clickAutoReloadBtn(String screenShotName) {
        clickElement(autoReloadBtn);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void selectRepeat(String screenShotName,String screenShotName1) {
        clickElement(repeatBtn);
        Thread.sleep(5000);
        clickElement(repeatBtn);
        swipeControl(By.xpath("//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView"),
                By.xpath("//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.TextView"));
        takeScreenshot(screenShotName);
        clickElement(doneOnRepeat);
        clickElement(confirmBtn);
        Thread.sleep(5000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void enterMoneyWithRepeatRule(String money,String screenShotName,String screenShotName1) {
        clearAndSendKeys(amountInput, money);
        inputEnter(amountInput);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        TransactionAmount = goSavePage.strToDouble(money);
        Thread.sleep(5000);
        clickElement(confirmBtn);
        Thread.sleep(5000);
        commonPage.waiteSendMsk();
        Thread.sleep(5000);
    }

    @SneakyThrows
    public boolean verifyAddMoneyWithRepeatRuleSuccess(String screenShotName) {
        Thread.sleep(5000);
        commonPage.waiteSendMsk();
        Thread.sleep(5000);
        commonPage.waiteSendMsk();
        waitUntilElementVisible(Processing);
        takeScreenshot(screenShotName);
        String addMoneyAmount = amountAtProgressing.getText().split("\\+")[1].split("HKD")[0];
        if (goSavePage.strToDouble(addMoneyAmount) == TransactionAmount) {
            clickElement(doneBtn);
            Thread.sleep(2000);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public void clickSettingBtnOnTransferPage(String screenShotName) {
        Thread.sleep(5000);
        waitUntilElementClickable(settingBtnOnTransfer);
        clickElement(settingBtnOnTransfer);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickAutoReloadSettings(String screenShotName) {
        Thread.sleep(5000);
        clickElement(autoReloadSetings);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        Thread.sleep(10000);
    }

    @SneakyThrows
    public boolean clickFirstAutoReloadRule(String screenShotName) {
        if(verifyElementExist(firstAutoReloadRule)){
            clickElement(firstAutoReloadRule);
            Thread.sleep(2000);
            takeScreenshot(screenShotName);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public void deleteRule() {
        Thread.sleep(2000);
        clickElement(deleteRuleBtn);
    }

    public void hideAndroidKeyboard() {
        try {
            if (System.getProperty("mobile").contains("android")) hideKeyboard();
        } catch (Exception e) {
        }
    }


    @SneakyThrows
    public boolean sendMoneySuccessDetailByBank(String screenShotName) {
        Thread.sleep(3000);
        waitUntilElementVisible(doneAtProgress);
        takeScreenshot(screenShotName);
        String progressMoneyAmount;
        if (isShow(sendMoneySubmit)) {
            progressMoneyAmount = submitMoneyAmount.getText().split("-")[1].split("HKD")[0];
        } else if(verifyElementExist(sendMoneySuccess)){
            progressMoneyAmount = sendMoneyAmount.getText().split("-")[1].split("HKD")[0];
        }else {
            return false;
        }
        if (goSavePage.strToDouble(progressMoneyAmount) == TransactionAmount &&
                payTo.getText().equals(traderName) &&
                receivingBankAccount.getText().equals(SendMoneyAccount)) {
            waitUntilElementClickable(doneAtProgress);
            clickElement(doneAtProgress);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifySendMoneyRecordByBankSuccess(String screenShotName){
        Thread.sleep(5000);
        scrollUp();
        Thread.sleep(1000);
        clickElement(traderNameAtHome);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);

        String recordAmountStrFirst = pattern.split(amountAtTransSuccess.getText())[1].concat(".");
        String recordAmountStr =recordAmountStrFirst.concat(pattern.split(amountAtTransSuccess.getText())[2]) ;
        WebElement  nameEle =driver.findElementByXPath("//*[@text='" +traderName+"']");
        boolean flag = verifyElementExist(nameEle);
        String recordAccountNumber = accountNumber.getText();
        return flag && goSavePage.strToDouble(recordAmountStr) == TransactionAmount && recordAccountNumber.equals(SendMoneyAccount);
    }

    @SneakyThrows
    public boolean sendMoneyDetailByFpsid(String screenShotName) {
        Thread.sleep(5000);
        waitUntilElementVisible(doneAtProgress);
        takeScreenshot(screenShotName);
        String progressMoneyAmount;
        Thread.sleep(5000);
        if (isShow(sendMoneySubmit, 2)) {
            progressMoneyAmount = submitMoneyAmount.getText().split("-")[1].split("HKD")[0];
        } else if (verifyElementExist(sendMoneySuccess)) {
            progressMoneyAmount = sendMoneyAmount.getText().split("-")[1].split("HKD")[0];
        } else {
            return false;
        }
        if (goSavePage.strToDouble(progressMoneyAmount) == TransactionAmount && fpsidOnDetail.getText().contains(FPSid)) {
            Thread.sleep(2000);
            clickElement(doneBtn);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean sendMoneyFailDetailByBankTransfer(String screenShotName) {
        waitUntilElementVisible(payTo);
        takeScreenshot(screenShotName);
        if (verifyElementExist(sendMoneySubmit) || verifyElementExist(transferUnsuccessful)) {
            return payTo.getText().equals(traderName) && receivingBankAccount.getText().equals(SendMoneyAccount);
        }
        return false;
    }


    @SneakyThrows
    public void verifyFpsidTransactions(String screenShotName, String screenShotName1) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);

        clickHomePageRecord();
        Thread.sleep(2000);
        assertThat(FPSidAtTrans.getText().contains(FPSid)).isTrue();
        assertThat(Double.parseDouble(amountAtTransSuccess.getText().replaceAll(",", "").split("[^0-9]+")[1]))
                .isEqualTo(TransactionAmount);
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void verifyBillNumTransactions(String screenShotName, String screenShotName1) {
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        clickHomePageRecord();

        Thread.sleep(2000);
        assertThat(billNameAtTrans.getText()).isEqualTo(billNumber);
        assertThat(fpsProxyIDAtTrans.getText()).isEqualTo(FPSid);

        assertThat(Double.parseDouble(amountAtTransSuccess.getText().replaceAll(",", "").split("[^0-9]+")[1]))
                .isEqualTo(TransactionAmount);
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
        clickElement(backBtnInRecordPage);
    }

    @SneakyThrows
    public boolean verifySetupRepeatRuleSuccess() {
        Thread.sleep(3000);
        if (ruleAmount.getText().contains(String.valueOf(TransactionAmount))) {
            Thread.sleep(2000);
            clickElement(backBtnOnAutoReload);
            Thread.sleep(2000);
            clickElement(backBtnOnTransferSettings);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public void clickHomePageRecord(){
        Thread.sleep(3000);
        scrollUp();
        Thread.sleep(2000);
        clickElement(traderNameAtHome);
        Thread.sleep(3000);
        clickElement(backBtnInRecordPage);
        Thread.sleep(3000);
        clickElement(traderNameAtHome);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"轉數快QR Code\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"转数快QR Code\""),
            @iOSXCUITBy(iOSNsPredicate = "//XCUIElementTypeOther[@name=\"转数快QR Code\"]"),
    })
    private MobileElement QRCodeItem;

    public void clickImportPhotoLibrary(){
        if(isIphoneSE()){
            clickByLocationByPercent(50, 93);
        } if(isIphone13()){
            clickByLocationByPercent(50, 91);
        }else{
            clickByLocationByPercent(50, 93);
        }
    }

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"相簿\"")
    private MobileElement photoLibrary;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"个人收藏\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"喜好項目\""),
    })
    private MobileElement personalCollection;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"照片, 个人收藏, 12月01日, 09:19\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 12月01日, 09:19\" AND name == \"照片, 个人收藏, 12月01日, 09:19\" AND type == \"XCUIElementTypeImage\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 12月01日, 上午9:19\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 2023年12月01日, 09:19\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"照片, 个人收藏, 12月01日, 上午9:19\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 12月01日, 上午9:19\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 2023年12月01日, 上午9:19\"]"),
    })
    private MobileElement qrCodePicture;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"截屏, 个人收藏, 12月05日, 上午9:38\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 2023年12月05日, 09:38\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 12月05日, 09:38\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"截屏, 个人收藏, 12月05日, 上午9:38\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"照片, 个人收藏, 12月05日, 09:38\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 12月05日, 上午9:38\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 2023年12月05日, 上午9:38\"]"),
    })
    private MobileElement qrCodePicture2;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"截屏, 个人收藏, 12月05日, 14:23\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"截屏, 个人收藏, 2023年12月05日, 14:23\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"照片, 个人收藏, 12月05日, 下午2:23\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"截屏, 个人收藏, 12月05日, 14:23\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"照片, 个人收藏, 12月05日, 下午2:23\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 12月05日, 下午2:23\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeImage[@name=\"相片, 喜好項目, 2023年12月05日, 下午2:23\"]"),
    })
    private MobileElement qrCodePicture3;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"转账\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"轉賬\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"轉賬\" "),
            @iOSXCUITBy(iOSNsPredicate = "label == \"转账\" "),
    })
    private MobileElement transactionPage;
    @SneakyThrows
    public void runTransactionByQRCode(String amount,String screenShotName){
        transactionAmount = goSavePage.strToDouble(amount);
        clickElement(QRCodeItem);
        Thread.sleep(2000);
        clickImportPhotoLibrary();
        Thread.sleep(1000);
        clickElement(photoLibrary);
        Thread.sleep(1000);
        clickElement(personalCollection);
        Thread.sleep(1000);
        clickElement(qrCodePicture2);
        Thread.sleep(3000);
        waitUntilElementVisible(transactionPage);
        Thread.sleep(1000);
        commonPage.inputMSKByString(amount);
        slipeConfirmBtn();
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"自拍驗證\""),
    })
    private MobileElement halfIdvCheckPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name,'警示: 根據香港警務處在')]"),
    })
    private MobileElement highRiskMessage;
    //警示: 根據香港警務處在「防騙視伏器」上提供的防騙資訊，此「轉數快」收款識別代號(手機號碼/電郵地址/快速支付系統識別碼)被列為「高危有伏」。 請您在進行交易前再次查證收款人實屬可靠。 你可按“取消”終止此次交易

    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '金額 (HKD) 戶口結餘')]")
    private MobileElement highRiskPageAmountInput;

    public void clickHighRiskPageAmountInput() {
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int y = getIOSAttributePoints(inputYourInfoEle,"y");
        System.out.println("height: "+height);
        System.out.println("y: "+y);
        y = y - (height * 10/100);
        clickByLocation(width/2, y);
    }
    @SneakyThrows
    public void runFpsTransactionTriggerHalfIdvCheck(String amount, String FpsId,
                                                     String screenShotName, String screenShotName1){
        Thread.sleep(1000);
        clickElement(transactionItem);
        Thread.sleep(2000);
        andSendKeys(FpsInput,FpsId);
        inputEnter(FpsInput);
        Thread.sleep(1000);
        clickElement(nextStepBtn);
        waitUntilElementVisible(inputYourInfoEle);
        waitUntilElementVisible(highRiskMessage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        //clickElement(highRiskPageAmountInput);
        clickHighRiskPageAmountInput();
        inputAmount(amount);
        slipeConfirmBtn();
        waitUntilElementVisible(halfIdvCheckPage);
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
    }
}
