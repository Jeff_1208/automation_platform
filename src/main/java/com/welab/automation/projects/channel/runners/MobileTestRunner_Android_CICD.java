package com.welab.automation.projects.channel.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(

    features = {
            "src/main/java/com/welab/automation/projects/channel/features/mobile_android/TAT-T3054_login.feature",
            "src/main/java/com/welab/automation/projects/channel/features/mobile_android/TAT-T3055_payment.feature",
            "src/main/java/com/welab/automation/projects/channel/features/mobile_android/TAT-T3056_regCard.feature",
            "src/main/java/com/welab/automation/projects/channel/features/mobile_android/TAT-T3057_regMaintenance.feature",
    },

    glue = {"com/welab/automation/projects/channel/steps/mobile"},
    tags = "@health",
    monochrome = true)
public class MobileTestRunner_Android_CICD extends TestRunner {}
