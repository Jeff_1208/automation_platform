package com.welab.automation.projects.channel.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/web"
    },
    glue = {"com/welab/automation/projects/channel/steps/web"},
    tags = "@jeff1",
    monochrome = true)
public class WebTestRunner extends TestRunner {}
