Feature: Payroll

  Background: Open App
    Given Open Stage WeLab App
    Given Check upgrade page appears Stage


  Scenario: Reg_Test bind device for payroll Android
    And skip Rebind Device Authentication Android
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    And bind Device Authentications Android


  @ACBN-T1410
  Scenario:  Reg_Payroll_001 AC1a send money trigger biometric Android
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then go to my Account Page
      | screenShotName | Reg_payroll_001_Android_01 |
    And go to payroll page
      | screenShotName | Reg_payroll_001_Android_02 |
    Then Send money at payroll page
      | amount         | 11                     |
      | screenShotName | Reg_payroll_001_Android_03 |
    And verify Transaction Result Page
      | screenShotName | Reg_payroll_001_Android_04 |
    And check Payroll Page
      | screenShotName | Reg_payroll_001_Android_05 |


  @ACBN-T1407
  Scenario:  Reg_Payroll_002 AC1b add money trigger biometric Android
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then go to my Account Page
      | screenShotName | Reg_payroll_002_Android_01 |
    And go to payroll page
      | screenShotName | Reg_payroll_002_Android_02 |
    Then Add money at payroll page
      | amount         | 12                     |
      | screenShotName | Reg_payroll_002_Android_03 |
    And check add money Success
      | screenShotName | Reg_payroll_002_Android_04 |
    And check Payroll Page
      | screenShotName | Reg_payroll_002_Android_05 |

  @ACBN-T1408
  Scenario:  Reg_Payroll_003 AC1d Export payroll Android
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then go to my Account Page
      | screenShotName | Reg_payroll_003_Android_01 |
    And go to payroll page
      | screenShotName | Reg_payroll_003_Android_02 |
    And go to Download Page
    And check Estatement Content
      | screenShotName | Reg_payroll_003_Android_03 |

  @ACBN-T1409
  Scenario:  Reg_Payroll_004 AC4 Payroll account eStatement Android
    When Login with user and password
      | user     | payroll00004 |
      | password | Aa123321     |
    Then I can see the LoggedIn page
    Then go to my Account Page
      | screenShotName | Reg_payroll_004_Android_01 |
    And go to payroll page
      | screenShotName | Reg_payroll_004_Android_02 |
    And go to Download Page
    And check Estatement Content
      | screenShotName | Reg_payroll_004_Android_03 |
    And download Estatement PDF Android
      | screenShotName | Reg_payroll_004_Android_04 |
