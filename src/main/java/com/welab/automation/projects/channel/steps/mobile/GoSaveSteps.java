package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.projects.channel.pages.CommonPage;
import com.welab.automation.projects.channel.pages.GoSavePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

public class GoSaveSteps {
    GoSavePage goSavePage;
    public GoSaveSteps(){
        goSavePage = new GoSavePage();
    }

    @Given("I go to GoSave Page")
    public void openWealthPage(Map<String, String> data) {
        goSavePage.openGoSavePage(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("I click the gosave pot")
    public void clickGoSavePot() {
        goSavePage.checkGoSaveDetail();
    }

    @And("I join gosave pot set ([^\"]\\S*)$")
    public void joinGoSavePot(String amount) {
        goSavePage.clickGoSaveJoin(amount);
    }

    @And("I join gosave pot more set ([^\"]\\S*)$")
    public void joinGoSavePotMore(String amount) {
        goSavePage.clickGoSaveJoinMore(amount);
    }

    @And("I check mygosave pending")
    public void checkMyGosavePending() {
        goSavePage.checkGoSavePending();
    }

    @And("I check mygosave in progress")
    public void checkMyGosaveInprogress() {
        goSavePage.goToGoSaveInprogress();
    }

    @And("I check gosave status")
    public void checkGoSaveStatus(Map<String, String> data) {
        goSavePage.checkGoSaveSuccessfullyDeposited(data.get("screenShotName"));
    }

    @And("I check gosave deposited more status")
    public void checkGoSaveDepositedMoreStatus(Map<String, String> data) {
        goSavePage.checkGoSaveSuccessfullyDepositedMore(data.get("screenShotName"));
    }

    @Then("I verify the gosave balance")
    public void verifyGoSaveBalance(Map<String, String> data) {
        goSavePage.verifyGoSaveAmount(data.get("screenShotName"));
    }
    @Then("I verify the gosave balance after withdrawn in progress")
    public void verifyGoSaveAmountAfterWithdrawInprogress(Map<String, String> data) {
        goSavePage.verifyGoSaveAmountAfterWithdrawInprogress(data.get("screenShotName"));
    }

    @Then("I verify the total balance")
    public void verifyTotalBalance(Map<String, String> data) {
        goSavePage.checkTheAcountTotalBalance(data.get("screenShotName"));
    }

    @And("I withdrawal money before pot start ([^\"]\\S*)$")
    public void WithdrawalMoneyBeforePotStart(String amount) {
        goSavePage.withdrawalBeforePotStartIOS(amount);
    }

    @And("I withdrawal money after pot start ([^\"]\\S*)$")
    public void WithdrawalMoneyAfterPotStart(String amount) {
        goSavePage.WithdrawalMoneyAfterPotStart(amount);
    }

    @And("I check status withdrawal money before pot start")
    public void checkStatusWithdrawalMoneyBeforePotStart (Map<String, String> data) {
        goSavePage.checkStatusWithdrawalMoneyBeforePotStart(data.get("screenShotName"));
    }

    @And("I check status withdrawal money after pot start")
    public void checkStatusWithdrawalMoneyAfterPotStart (Map<String, String> data) {
        goSavePage.checkStatusWithdrawalMoneyBeforePotStart(data.get("screenShotName"));
    }

    @And("I View started pot")
    public void viewStartedPot(Map<String, String> data) {
        goSavePage.viewStartedPot(data.get("screenShotName"));
    }

    @Then("I check myGoSave In progress")
    public void checkInProgress(Map<String, String> data){
        goSavePage.checkGoSavePending();
        goSavePage.clickInProgress(data.get("screenShotName"));
    }

    @Then("I check GoSave Order deposit")
    public void checkOrderDeposit(Map<String, String> data){
        goSavePage.checkOrderDeposit(data.get("screenShotName"));
    }

    @And("I verify Total balance and Available balance")
    public void verifyBalance(Map<String, String> data) {
        goSavePage.verifyBalance(data.get("screenShotName"));
    }

    @And("I goto Go save page")
    public void gotoGoSavePage(Map<String, String> data){
        goSavePage.openGoSave(data.get("screenShotName"));
    }

    @And("I verify balance amount withdrawal")
    public void iverifywithdrawalaccountbalance() {
        assertThat(goSavePage.totalbalance()).isTrue();
    }

    @And("^I goto Pending car Page withDrawal set ([^\"]\\S*)$")
    public void gotowithDrawal(String amount){
        boolean flag =goSavePage.gotoPenging();
        assertThat(flag).isTrue();
        goSavePage.clickWithDrawSetAmount(amount);
    }

    @And("I verify withDrawal completed page")
    public void verifyWithDrawalCompletedPage(Map<String, String> data){
        goSavePage.verifyWithDrawalCompletedPage(data.get("screenShotName"));
    }


    @And("I verify myTotal Balance After")
    public void verifyGoSaveBalabceAfterJoinPot(Map<String, String> data) {
        boolean balanceFlag=goSavePage.verifyGoSaveBalanceAfterJoinPot(data.get("screenShotName"));
        assertThat(balanceFlag).isTrue();
    }
    @And("^I goto In progress car Page withDrawal set ([^\"]\\S*)$")
    public void gotoInprogresswithDrawal(String amount) {
        goSavePage.clickGoSaveBalance();
        goSavePage.selectCar();
        goSavePage.clickInprogressWithDrawSetAmount(amount);
    }

    @Then("I verify the home page total balance")
    public void verifyHomePageTotalBalance(Map<String, String> data) {
        goSavePage.checkTheHomePageTotalBalance(data.get("screenShotName"));
    }
    @Then("I switch to h5")
    public void switchH5() {
        goSavePage.switchH5();
    }
    @Then("I clicked join Now")
    public void joinNow() {
        goSavePage.joinNow();
    }
    @Then("I Enter ([^\"]\\S*)$")
    public void enter(String amount) {
        goSavePage.enter(amount);
    }
    @Then("I click Next")
    public void clickNext() {
        goSavePage.clickNext();
    }
    @Then("I clicked OK and agreed to the terms and conditions")
    public void clickedConditions() {
        goSavePage.clickedConditions();
    }
    @Then("I clicked the Finish button")
    public void clickedFinish() {
        goSavePage.clickedConditions();
    }
    @Then("I switch back to the native app")
    public void switchNative() {
        goSavePage.switchNative();
    }

}
