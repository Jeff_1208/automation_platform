package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GoSavePage extends AppiumBasePage {
  private double theAccountAvailableBalanceBeforeJoinPot=0.0;
  private double myGoSaveAmountBeforeJoinPot=0.0;
  private double totalBalanceAfterJoin=0.0;
  private double goSaveAmount =0.0;
  private double TotalBalance = 0.0;
  private double AvailableBalance = 0.0;
  private double goSaveTotalBalance = 0.0;
  private double myTotalAfterJoinPot=0.0;
  private String pageName = "GoSave Page";
  CommonPage commonPage;
  public GoSavePage() {
    super.pageName = this.pageName;
    commonPage = new CommonPage();
  }

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "GoSave, tab, 2 of 4"),
          @AndroidBy(accessibility = "GoSave, tab, 2 of 5")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 4'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemGroupSavingStack, tab, 2 of 5'")
  })
  private MobileElement goSaveTab;

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "Home, tab, 1 of 4"),
          @AndroidBy(accessibility = "Home, tab, 1 of 5"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='主頁']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'Home, tab, 1 of 5'")
  })
  private MobileElement homeTab;

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "Transfer, tab, 3 of 4"),
          @AndroidBy(accessibility = "Transfer, tab, 3 of 5"),
          @AndroidBy(accessibility = "存款與轉賬, tab, 3 of 4"),
          @AndroidBy(accessibility = "存款與轉賬, tab, 3 of 5")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 4'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemTransferStack, tab, 3 of 5'")
  })
  private MobileElement transferTab;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'My GoSave')]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='我的GoSave']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[contains(@name, '我的GoSave總額 (HKD)')])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[contains(@name, 'My total GoSave balance (HKD)')])[1]"),
  })
  private MobileElement myGoSave;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'GoSave Time Deposit')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'GoSave定期存款')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"GoSave定期存款\" AND name == \"text\""),
          @iOSXCUITBy(iOSNsPredicate = "label == \"GoSave Time Deposit\" AND name == \"text\""),
  })
  private MobileElement goSaveTimeDeposit;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'My GoSave')]/../../android.view.ViewGroup[1]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='我的GoSave']/../../android.view.ViewGroup[1]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[starts-with(@name, '我的GoSave 總額: HKD’)])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[starts-with(@name, 'My total GoSave balance (HKD)’)])[1]")
  })
  private MobileElement myGoSaveHeaderBack;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='My total GoSave balance (HKD)']/../android.widget.TextView[2]"),
          @AndroidBy(xpath = "//*[@text='我的GoSave總額 (HKD)']/../android.widget.TextView[2]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '我的GoSave總額 (HKD)')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'My total GoSave balance (HKD)')]")
  })
  private MobileElement goSaveBalance;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'month GoSave Time Deposit')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'個月GoSave定期存款')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[contains(@name, '個月GoSave定期存款')]"),
          @iOSXCUITBy(xpath = "//*[contains(@name, 'month GoSave Time Deposit')]")
  })
  private MobileElement goSaveItem;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'month GoSave Time Deposit')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'個月GoSave定期存款')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[contains(@name, '個月GoSave定期存款')]"),
          @iOSXCUITBy(xpath = "//*[contains(@name, 'month GoSave Time Deposit')]")
  })
  private MobileElement goSaveDetailBtn;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, '個月GoSave定期存款')]")
  private MobileElement goSaveDetailBtnZh;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'month GoSave Time Deposit')]")
  private MobileElement goSaveDetailBtnEn;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"存款紀錄\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Deposit record\"]")
  })
  private MobileElement goSaveDetailDepositRecord;

  @AndroidFindAll({
          @AndroidBy( xpath = "//android.widget.TextView[contains(@text,'Start date')]/../android.view.ViewGroup[6]/android.widget.TextView[1]"),
          @AndroidBy( xpath = "//android.widget.TextView[contains(@text,'存款開始於')]/../android.view.ViewGroup[6]/android.widget.TextView[1]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"存款開始於\"]/../XCUIElementTypeOther[2]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Start date\"]/../XCUIElementTypeOther[2]")
  })
  private MobileElement goSaveStartDate;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Join']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='加入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Join'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '加入'")
  })
  private MobileElement JoinBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Deposit more']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='存入更多']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"存入更多\"])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Deposit more\"])[1]")
  })
  private MobileElement DepositMoreBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available balance HKD')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'可用結餘')]")
  })
  private MobileElement goSaveAvailableBalance;

  @AndroidFindAll({
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'Available balance HKD')]/../android.view.ViewGroup[2]/android.widget.EditText"),
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'可用結餘')]/../android.view.ViewGroup[2]/android.widget.EditText")
  })
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"水平滚动条, 1页\"])[2]")
  private MobileElement goSaveAmountInputEditText;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Next')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'下一步')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[contains(@name, '下一步')]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[contains(@name, 'Next')]")
  })
  private MobileElement goSaveNextBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'GoSave number')]/../android.widget.TextView"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'GoSave編號')]/../android.widget.TextView")
  })
  private List<MobileElement> goSaveIDBrothers;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"完成\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Done\"]")
  })
  private MobileElement withdrawalFinishBtn;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提前取款\"])[2]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Withdraw\"])[2]")
  })
  private MobileElement withdrawalMoney;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"提取\"]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Withdraw\"])[5]")
  })
  private MobileElement extractMoney;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Pending')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'等待開始')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"等待開始\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Pending\"]")
  })
  private MobileElement goSavePengdingTab;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'In progress')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'進行中')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"進行中\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"In progress\"]")
  })
  private MobileElement goSaveInprogressTab;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"1\"]")
  private MobileElement goSaveAKeyNumber1;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"5\"]")
  private MobileElement goSaveAKeyNumber5;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"0\"]")
  private MobileElement goSaveAKeyNumber0;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"GoSave編號\"]/../XCUIElementTypeOther"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"GoSave number\"]/../XCUIElementTypeOther")
  })
  private MobileElement goIosSaveID;

  @AndroidFindAll({
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'Confirm and accept Terms and Conditions')]"),
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'確定並同意條款及細則')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Confirm and accept Terms and Conditions\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定並同意條款及細則\"]")
  })
  private MobileElement ConfirmBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Ended')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'已結束')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"已結束\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Ended\"]")
  })
  private MobileElement goSaveEndedTab;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Successfully joined')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'成功上車')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"成功上車\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"Successfully joined\"]")
  })
  private MobileElement successfullyJoined;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Successfully joined')]/../android.widget.TextView[2]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'成功上車')]/../android.widget.TextView[2]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[contains(@name, '你已成功將 HKD')]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[contains(@name, 'Successfully joined')]")
  })
  private MobileElement successfullyDepositedAmount;


  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'OK')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'完成')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"完成\"])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"完成\"])[2]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"OK\"])[2]")
  })
  private MobileElement OkBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Deposit more')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'存入更多')]")
  })
  private MobileElement goSaveDepositMoreBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'My total GoSave balance (HKD)')]/../../../android.view.ViewGroup[2]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'我的GoSave總額 (HKD)')]/../../../android.view.ViewGroup[2]")
  })
  private MobileElement goToMyGoSaveBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = " //*[@text='My total GoSave balance (HKD)']"),
          @AndroidBy(xpath = " //*[@text='我的GoSave總額 (HKD)']")
  })
  private MobileElement totalBalance;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='In progress']"),
          @AndroidBy(xpath = "//*[@text='進行中']")
  })
  private MobileElement inProgress;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[1]/../../android.widget.TextView[1]")
  private MobileElement firstOrder;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Deposit record']"),
          @AndroidBy(xpath = "//*[@text='存款紀錄']")
  })
  private MobileElement depositRecord;

  @AndroidFindAll({
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'Successfully deposited more')]"),
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'成功存入更多')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"Successfully deposited more\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"成功存入更多\"]")
  })
  private MobileElement successfullyDepositedMore;

  @AndroidFindAll({
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'Successfully deposited')]/../android.widget.TextView[2]"),
          @AndroidBy(
                  xpath = "//android.widget.TextView[contains(@text,'你已成功將')]/../android.widget.TextView[2]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name, '你已成功')]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name, \"You've successfully\")]"),
  })
  private MobileElement successfullyDepositedMoreAmount;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Total balance')]/../android.widget.TextView[2]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'總結餘')]/../android.widget.TextView[2]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘 (HKD)')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用余额 (HKD)')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Available balance (HKD)')]")
  })
  private MobileElement theAccountTotalBalance;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available balance')]/../android.widget.TextView[2]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'可用結餘')]/../android.widget.TextView[2]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用余额')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Available balance')]")
  })
  private MobileElement theAccountAvailableBalance;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available balance')]"),
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'可用結餘')]")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用余额')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Available balance')]")
  })
  private MobileElement theAccountAvailableBalanceTitle;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"緊急資金需要\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Urgent needs of funds\"]")
  })
  private MobileElement extractMoneyReason;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"提交\"]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Done\"])[3]")
  })
  private MobileElement extractMoneySubmit;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Withdraw\"])[3]")
  })
  private MobileElement extractMoneyConfirm;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '本金金額')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Principal amount')]")
  })
  private MobileElement GosaveItemAmount;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'GoSave編號')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'GoSave number')]")
  })
  private MobileElement GosaveIDEle;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[starts-with(@name, '存款期')]"),
          @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Tenor')]")
  })
  private MobileElement GoSaveDepositPeriod;


  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.widget.EditText"),
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'可提取金額')]/../android.view.ViewGroup[2]")
  })
  private MobileElement withDrawInput;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Confirm']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='確認提取']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認提取\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Confirm\"]")
  })
  private MobileElement confirmBtn;

  @AndroidFindBy(xpath = "//android.widget.ImageView/../android.widget.TextView[1]")
  private MobileElement withDrawCompleted;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Done']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='完成']")
  })
  private MobileElement doneBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Withdraw']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='提前取款']")
  })
  private MobileElement withDrawBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Cancel']/../../android.view.ViewGroup[1]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='返回']/../../android.view.ViewGroup[1]")
  })
  private MobileElement withDrawConfirm;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Deposited amount HKD')]"),
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'已存金額')]")
  })
  private MobileElement withDarwalMaxMoney;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Urgent needs of funds']/../android.view.ViewGroup"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='緊急資金需要']")
  })
  private MobileElement improveBtn1;

  @AndroidFindAll({
          @AndroidBy( xpath ="//android.widget.TextView[contains(@text, 'Please select')]/../android.view.ViewGroup[11]"),
          @AndroidBy( xpath ="//android.widget.TextView[contains(@text, 'Please select')]/../android.view.ViewGroup[10]"),
          @AndroidBy( xpath ="//android.widget.TextView[contains(@text, '請選擇')]/../android.view.ViewGroup[11]"),
          @AndroidBy( xpath ="//android.widget.TextView[contains(@text, '請選擇')]/../android.view.ViewGroup[10]")
  })
  private MobileElement improveDone;

  @AndroidFindBy(xpath = "//android.widget.TextView[8]/../../../../android.view.ViewGroup")
  private List<MobileElement> carDetail;
  @AndroidFindAll({
          @AndroidBy( xpath ="//android.widget.TextView[@text='Processing withdrawal']"),
          @AndroidBy( xpath ="//android.widget.TextView[@text='提取處理中']")
  })
  private List<MobileElement> StatusLists;
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Cancel']/../../android.widget.TextView[2]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='返回']/../../android.widget.TextView[2]")
  })
  private MobileElement withDrawtimesleft;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='All available']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='提取全部']")
  })
  private MobileElement allAvailable;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Confirm withdrawal?\"])[1]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提早提款\"])[1]")
  })
  private MobileElement earlyWithdrawalMoneyPageTitle;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提取處理中\"])[2]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Processing withdrawal\"])[2]")
  })
  private MobileElement goSaveHangdlingBtn;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提早提款 GoSave開始後，你可以提早取款2次，而最後一次須為全額取款。 目前提早取款次數剩餘1次，你確定要提早取款？ 確定 返回\"])[2]"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Confirm withdrawal? After GoSave started, you can make up to 2 withdrawals, and the last withdrawal must be a full withdrawal. You have 1 withdrawal left, are you sure to withdraw? Withdraw Cancel\"])[2]")
  })
  private MobileElement earlyWithdrawalJustOneTimeMessage;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"返回\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Cancel\"]")
  })
  private MobileElement earlyWithdrawalMoneyCancelBtn;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確定\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Withdraw\"]")
  })
  private MobileElement earlyWithdrawalMoneyConfirmBtn;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"你已離開此GoSave定期\"]/../XCUIElementTypeStaticText[2]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"You've Left the Group\"]/../XCUIElementTypeStaticText[2]")
  })
  private MobileElement withdrawnGoSaveSecondTimeMessage2;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"提早提款\"])[1]/../../XCUIElementTypeOther"),
          @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Confirm withdrawal?\"])[1]/../../XCUIElementTypeOther")
  })
  private MobileElement earlyWithdrawalMessage;

  @AndroidFindAll({
          @AndroidBy( xpath ="//android.widget.TextView[starts-with(@text,'You have withdrawn HKD')]"),
          @AndroidBy( xpath ="//android.widget.TextView[starts-with(@text,'你已從')]")
  })
  private MobileElement moneyWithDrawCompleted;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available for Withdrawal')]"),
          @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text,'可提取金額')]")
  })
  private MobileElement withDarwalMaxMoneyInProgress;

  @AndroidFindAll({
          @AndroidBy( xpath ="//android.widget.TextView[@text='GoSave Time Deposit']"),
          @AndroidBy( xpath ="//android.widget.TextView[@text='GoSave定期存款']")
  })
  private MobileElement backInCarDetail;

  @SneakyThrows
  public void openGoSavePage(String screenShotName,String screenShotName1) {
    Thread.sleep(3000);
    waitUntilElementVisible(goSaveTab);
//    theAccountAvailableBalanceBeforeJoinPot=getAccountAvailableBalance();
//    System.out.println("theAccountAvailableBalanceBeforeJoinPot: "+theAccountAvailableBalanceBeforeJoinPot);
//    waitUntilElementClickable(goSaveTab);
    takeScreenshot(screenShotName);
    goSaveTab.click();
    Thread.sleep(2000);
    waitUntilElementClickable(goSaveTimeDeposit,60);
    myGoSaveAmountBeforeJoinPot=getGoSaveCurrentTotalBalance();
    Thread.sleep(2000);
    takeScreenshot(screenShotName1);
  }

  public void checkGoSaveDetail() {
    clickGoSaveDetail();
  }

  public void checkGoSavePending(){
    if(commonPage.isAndroid()){
      clickElement(goToMyGoSaveBtn);
    }else{
      if (GlobalVar.IS_USE_OPENCV) {
        clickByPicture2("src/main/resources/images/channel/gosave/MyGosaveDetail.png",50,50);
      } else {
        clickByLocationByPercent(50, 26);
      }
    }
    waitUntilElementVisible(myGoSave);
  }

  @SneakyThrows
  public void goToGoSaveInprogress(){
    checkGoSavePending();
    clickElement(goSaveInprogressTab);
    waitUntilElementVisible(goSaveDetailBtn);
    Thread.sleep(2000);
  }

  public boolean isHaveGoSaveItemInPending(){
    waitUntilElementVisible(goSavePengdingTab);
    if(isShow(goSaveItem,8)){
      return true;
    }
    return false;
  }

  public Double getGoSaveCurrentTotalBalance() {
    String goSaveTotalBalance = goSaveBalance.getText();
    if(commonPage.isIOS()){
      System.out.println("goSaveTotalBalance:"+goSaveTotalBalance);
      goSaveTotalBalance = getAmountString(goSaveTotalBalance,"(HKD)").trim();
    }
    Double totalBalance =strToDouble(goSaveTotalBalance);
    System.out.println("goSaveTotalBalance: "+goSaveTotalBalance);
    return totalBalance;
  }

  public void clickGoSaveDetail() {
    waitUntilElementClickable(goSaveDetailBtn);
    goSaveDetailBtn.click();
  }

  public String getGoSaveStartDate() {
    waitUntilElementVisible(goSaveStartDate);
    String startDate = goSaveStartDate.getText();
    System.out.println("startDate: "+startDate);
    return  startDate;
  }

  public void calcTotalBalanceAfterJoin(String amount) {
    totalBalanceAfterJoin = myGoSaveAmountBeforeJoinPot + strToDouble(amount);
  }

  public void calcTotalBalanceAfterWithdrawn(String amount) {
    totalBalanceAfterJoin = myGoSaveAmountBeforeJoinPot - strToDouble(amount);
  }

  public void clickGoSaveJoin(String amount) {
    goSaveAmount =strToDouble(amount);
    waitUntilElementClickable(JoinBtn);
    getGoSaveStartDate();
    JoinBtn.click();
    inputAmountForGoSave(amount);
  }

  @SneakyThrows
  public void inputAmountForGoSave(String amount){
    waitUntilElementVisible(goSaveAmountInputEditText);
    if(commonPage.isIOS()){
      clickElement(goSaveAKeyNumber1);
      clickElement(goSaveAKeyNumber0);
      clickElement(goSaveAKeyNumber0);
      inputEnter(goSaveAmountInputEditText);
      Thread.sleep(1000*2);
      final int width = driver.manage().window().getSize().width;
      final int height = driver.manage().window().getSize().height;
      int x = width / 2;
      int y = height *14/ 15;
      clickByLocation(x, y);
    }else if(commonPage.isAndroid()){
      String availableAmountString= goSaveAvailableBalance.getText();
      System.out.println("availableAmountString: "+availableAmountString);
      andSendKeys(goSaveAmountInputEditText,amount);
      clickElement(goSaveNextBtn);
    }

    calcTotalBalanceAfterJoin(amount);
    confirmGoSaveJoin(amount);
  }

  public void withdrawalBeforePotStartIOS(String amount) {
    boolean result = isHaveGoSaveItemInPending();
    assertThat(result).isTrue();
    System.out.println("have gosave record: "+result);
    if(result){
      clickElement(goSaveDetailBtn);
      scrollUpToFindElement(withdrawalMoney,3,2);
      clickElement(withdrawalMoney);
      goSaveAmount = 0 - strToDouble(amount);
      waitUntilElementClickable(extractMoney);
      clickElement(extractMoney);
      inputAmountForGoSaveWithdrawMoney(amount);
    }else{
      assertThat(false).isTrue();
    }
  }

  @SneakyThrows
  public void inputAmountForGoSaveWithdrawMoney(String amount){
    waitUntilElementVisible(goSaveAmountInputEditText);
    if(commonPage.isIOS()){
      clickElement(goSaveAKeyNumber5);
      clickElement(goSaveAKeyNumber0);
      inputEnter(goSaveAmountInputEditText);
      Thread.sleep(2000);
      if (GlobalVar.IS_USE_OPENCV) {
        clickByPicture2("src/main/resources/images/channel/gosave/WithdrawalConfirm.png",50,0);
      } else {
        clickByLocationByPercent(50, 94);
      }
    }
    calcTotalBalanceAfterWithdrawn(amount);
  }

  public void checkStatusWithdrawalMoneyBeforePotStart(String screenShotName){
    waitUntilElementVisible(withdrawalFinishBtn);
    takeScreenshot(screenShotName);
    clickElement(withdrawalFinishBtn);
  }

  public void clickGoSaveJoinMore(String amount) {
    boolean result = isHaveGoSaveItemInPending();
    assertThat(result).isTrue();
    System.out.println("have gosave record: "+result);
    if(result){
      clickElement(goSaveDetailBtn);
      goSaveAmount =strToDouble(amount);
      waitUntilElementClickable(DepositMoreBtn);
      DepositMoreBtn.click();
      inputAmountForGoSave(amount);
    }else{
      assertThat(false);
    }
  }

  public String getGoSaveID(){
    String goSaveID = goSaveIDBrothers.get(goSaveIDBrothers.size()-1).getText();
    System.out.println("goSaveIDBrothers.size(): "+goSaveIDBrothers.size());
    System.out.println("goSaveID: "+goSaveID);
    return goSaveID;
  }

  public String getIosGoSaveID(){
    String goSaveID = getElementText(goIosSaveID);
    System.out.println("goSaveID: "+goSaveID);
    return goSaveID;
  }

  public void confirmGoSaveJoin(String amount) {
    waitUntilElementVisible(ConfirmBtn);
    String goSaveID ="";
    if(commonPage.isAndroid()){
      goSaveID =getGoSaveID();
    }else{
      goSaveID =getIosGoSaveID();
    }
    clickElement(ConfirmBtn);
  }

  public void checkGoSaveSuccessfullyDeposited(String screenShotName) {
    waitUntilElementVisible(successfullyJoined);
    String depositedAmount = successfullyDepositedAmount.getText();
    System.out.println("depositedAmount: "+depositedAmount);
    takeScreenshot(screenShotName);
    clickElement(OkBtn);
  }

  public void checkGoSaveSuccessfullyDepositedMore(String screenShotName) {
    waitUntilElementVisible(successfullyDepositedMore);
    String depositedMoreAmount = successfullyDepositedMoreAmount.getText();
    System.out.println("depositedMoreAmount: "+depositedMoreAmount);
    takeScreenshot(screenShotName);
    clickElement(OkBtn);
  }

  @SneakyThrows
  public void verifyGoSaveAmount(String screenShotName) {
    //waitUntilElementVisible(myGoSave);
    if(!isShow(myGoSave,10)){
      clickTopLeftBtnToExit();
    }
    scrollDown();
    Thread.sleep(3000*5);
    scrollDown();
    Thread.sleep(3000);
    Double amount =getGoSaveCurrentTotalBalance();
//    System.out.println("totalBalanceAfterJoin: "+totalBalanceAfterJoin);
//    assertThat(amount).isEqualTo(totalBalanceAfterJoin);
    takeScreenshot(screenShotName);
    boolean balanceFlag=false;
    if(myGoSaveAmountBeforeJoinPot+goSaveAmount==amount){
      balanceFlag=true;
    }
    assertThat(balanceFlag).isTrue();
    if(commonPage.isAndroid()){
      clickElement(myGoSaveHeaderBack);
    }else{
      clickTopLeftBtnToExit();
    }
  }

  @SneakyThrows
  public void verifyGoSaveAmountAfterWithdrawInprogress(String screenShotName){
    for (int i = 0; i < 20; i++) {
      scrollDownFast(100);
    }
    Thread.sleep(5000);
    verifyGoSaveAmount(screenShotName);
  }

  public Double getAccountTotalBalance(){
    String theAccountTotalBalanceText = theAccountTotalBalance.getText();
    return strToDouble(theAccountTotalBalanceText);
  }

  @SneakyThrows
  public Double getAccountAvailableBalance(){
    waitUntilElementVisible(theAccountAvailableBalanceTitle,50);
    Thread.sleep(2000);
    String theAccountAvailableBalanceText = theAccountAvailableBalance.getText();
    if(commonPage.isAndroid()){
      return strToDouble(theAccountAvailableBalanceText);
    }else{
      return getIOSBalance(theAccountAvailableBalanceText);
    }
  }

  public double getIOSBalance(String data){
    System.out.println("data: "+data);
    //可用結餘 (HKD) 43,056,973.83
    if(data.contains("(HKD)")){
      data = data.split("(HKD)")[1].trim();
      data = data.split(" ")[1].trim();
    }
    if(data.contains("可用余额")){
      //  data: 可用余额 43,089,841.83
      data = data.split("可用余额")[1].trim();
    }
    data = data.split(" ")[0].trim();
    return strToDouble(data);
  }



  public double strToDouble(String data){
    if(data.contains(",")) {
      data = data.replace(",", "");
    }
    return Double.parseDouble(data);
  }

  public String getAmountString(String data,String flag){
    int i = data.indexOf(flag)+flag.length();
    data = data.substring(i).trim();
    System.out.println("data: "+data);
    return data.split(" ")[0].trim();
  }

  public String getAmountString(String data,String start,String end){
    int start_index =  data.indexOf(start);
    int end_index =  data.indexOf(end);
    return data.substring(start_index+start.length(),end_index).trim();
  }

  @SneakyThrows
  public void checkTheAcountTotalBalance(String screenShotName){
    waitUntilElementClickable(homeTab);
    clickElement(homeTab);
    Thread.sleep(2000);
//    waitUntilElementVisible(theAccountTotalBalance);
//    Double theAccountAvailableBalanceText = getAccountAvailableBalance();
//    Double calcTotalBalance = theAccountAvailableBalanceBeforeJoinPot -goSaveAmount;
//    System.out.println("theAccountAvailableBalanceBeforeJoinPot: "+theAccountAvailableBalanceBeforeJoinPot);
//    System.out.println("theAccountAvailableBalanceText: "+theAccountAvailableBalanceText);
//    System.out.println("calcTotalBalance: "+calcTotalBalance);
    takeScreenshot(screenShotName);
//    assertThat(theAccountAvailableBalanceText).isEqualTo(calcTotalBalance);
//    goSaveAmount =0.0;
  }


  public void clickWithdrawAllBtn() {
    if (GlobalVar.IS_USE_OPENCV) {
      clickByPicture2("src/main/resources/images/channel/gosave/WithdrawalAll.png",50,50);
    } else {
      clickByLocationByPercent(83, 48);
    }
  }
  public void clickConfirmBtn(){
    if(GlobalVar.IS_USE_OPENCV){
      clickByPicture2("src/main/resources/images/channel/gosave/WithdrawalConfirm.png",50,10);
    }else {
      clickByLocationByPercent(50, 60);
    }
  }
  public void clickGosaveItem() {
    if (GlobalVar.IS_USE_OPENCV) {
      clickByPicture2("src/main/resources/images/channel/gosave/GosaveDetail.png",50,50);
    } else {
      clickByLocationByPercent(50, 51);
    }
  }
  public void clickGosaveItemEn(){
    if(GlobalVar.IS_USE_OPENCV){
      clickByPicture2("src/main/resources/images/channel/gosave/GosaveDetail.png",50,50);
    }else {
      clickByLocationByPercent(50, 43);
    }
  }
  @SneakyThrows
  public boolean isFirstTimeToEarlyWithdrawalGoSaveItem(){
    boolean flag = true;
    boolean isEn = isShow(goSaveDetailBtnEn,3);
    //clickElement(goSaveDetailBtn);
    if(isEn){
      clickGosaveItemEn();
    }else {
      clickGosaveItem();
    }
    Thread.sleep(3000);
    scrollUp();
    Thread.sleep(1000);
    //scrollUpToFindElement(withdrawalMoney,3,2);
    if(isShow(goSaveHangdlingBtn,3)){
      clickTopLeftBtnToExit();
      Thread.sleep(4000);
      scrollDown();
      clickElement(goSaveDetailBtn);
      Thread.sleep(3000);
      scrollUp();
      Thread.sleep(1000);
    }
    if(!isShow(goSaveDetailDepositRecord,3)){
      if(isEn){
        clickGosaveItemEn();
      }else {
        clickGosaveItem();
      }
    }
    clickElement(withdrawalMoney);
    Thread.sleep(1000);
    waitUntilElementVisible(earlyWithdrawalMoneyPageTitle);
    if(isShow(earlyWithdrawalMessage,3)){
      String data  = earlyWithdrawalMessage.getText();
      System.out.println("earlyWithdrawalMessage: "+earlyWithdrawalMessage.getText());
      if(data.contains("目前提早取款次數剩餘1次")||data.contains("You have 1 withdrawal left")){
        return false;
      }
    }
    if(isShow(earlyWithdrawalJustOneTimeMessage,3)){
      flag = false;
    }
    System.out.println("is First time to withdraw gosave: "+flag);
    return flag;
  }

  @SneakyThrows
  public void WithdrawalMoneyAfterPotStart(String amount){
    boolean result = isHaveGoSaveItemInPending();
    assertThat(result).isTrue();
    System.out.println("have gosave record: "+result);

    for (int i = 0; i < 20; i++) {
      scrollUpFast(100);
    }
    Thread.sleep(5000);

    if(isFirstTimeToEarlyWithdrawalGoSaveItem()){
      goSaveAmount = 0-strToDouble(amount);
      waitUntilElementClickable(extractMoneyConfirm);
      clickElement(extractMoneyConfirm);
      inputAmountForGoSaveWithdrawMoney(amount);
      waitUntilElementClickable(extractMoneyReason);
      clickElement(extractMoneyReason);
      clickElement(extractMoneySubmit);
      calcTotalBalanceAfterWithdrawn(amount);
    }else{
      //clickElement(earlyWithdrawalMoneyConfirmBtn);
      clickElement(extractMoneyConfirm);
      Thread.sleep(1000);
      clickWithdrawAllBtn();
      Thread.sleep(2000);
      clickConfirmBtn();
      Thread.sleep(2000);
      waitUntilElementClickable(extractMoneyReason);
      clickElement(extractMoneyReason);
      clickElement(extractMoneySubmit);
      Thread.sleep(3000);
      if(isShow(withdrawnGoSaveSecondTimeMessage2,5)){
        System.out.println("withdrawnGoSaveSecondTimeMessage2: "+withdrawnGoSaveSecondTimeMessage2.getText());
      }
      waitUntilElementVisible(withdrawnGoSaveSecondTimeMessage2);
      String message = withdrawnGoSaveSecondTimeMessage2.getText();
      int start_index = message.indexOf("HKD")+"HKD".length();
      int end_index=0;
      if(message.contains("withdrawn")){
        end_index = message.indexOf(" from");
      }else{
        end_index = message.indexOf("至核心賬戶");
      }
      String withdrawAmountStr= message.substring(start_index,end_index).trim();
      System.out.println("withdranAmountStr: "+withdrawAmountStr);
      goSaveAmount  = 0 - strToDouble(withdrawAmountStr);
      calcTotalBalanceAfterWithdrawn(withdrawAmountStr);
      //XCUIElementTypeStaticText[@name="你已從 3個月GoSave定期存款4389 提取 HKD 100.00 至核心賬戶"]
      //XCUIElementTypeStaticText[@name="You have withdrawn HKD 100.00 from 3-month Gosave Time Deposit4429 to your Core Account"]
    }
  }

  public double  captureGosaveItemAmountInProgram(){
    waitUntilElementVisible(GosaveItemAmount);
    String amount = GosaveItemAmount.getText();
    amount = amount.substring(amount.indexOf("HKD")+"HKD".length()).trim();
    double amount_value = strToDouble(amount);
    return amount_value;
  }

  @SneakyThrows
  public void clickTopLeftBtnToExit(){
    Thread.sleep(2000);
    clickByLocationByPercent(6, 6);
//    if (GlobalVar.IS_USE_OPENCV) {
//      clickByPicture2("src/main/resources/images/channel/BackBtn.png",50,50);
//    } else {
//      Thread.sleep(2000);
//      clickByLocationByPercent(6, 6);
//    }
  }

  public void viewStartedPot(String screenShotName){
    boolean result = isHaveGoSaveItemInPending();
    assertThat(result).isTrue();
    System.out.println("have gosave record: "+result);
    if(result){
      clickElement(goSaveDetailBtn);
      double haveMoney =captureGosaveItemAmountInProgram();
      System.out.println("amout: "+haveMoney);
      String id = GosaveIDEle.getText();
      System.out.println("GosaveId: "+id);
      String Period = GoSaveDepositPeriod.getText();
      System.out.println("GoSave Deposit Period: "+Period);
      takeScreenshot(screenShotName);
      clickTopLeftBtnToExit();
    }else{
      assert false;
    }
  }

  @SneakyThrows
  public void clickInProgress(String screenShotName){
    waitUntilElementClickable(inProgress);
    clickElement(inProgress);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void checkOrderDeposit(String screenShotName){
    waitUntilElementClickable(firstOrder);
    clickElement(firstOrder);
    scrollUpToFindElement(depositRecord,10,3);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  public void verifyBalance(String screenShotName) {
//    TotalBalance = getAccountTotalBalance();
//    AvailableBalance = getAccountAvailableBalance();
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void openGoSave(String screenShotName) {
    Thread.sleep(3000);
    waitUntilElementVisible(goSaveTab);
    goSaveTab.click();
    Thread.sleep(3000);
    goSaveTotalBalance = getGoSaveCurrentTotalBalance();
    System.out.println("theAccountAvailableBalance: " + goSaveTotalBalance);
    takeScreenshot(screenShotName);
  }

  public boolean totalbalance() {
    if(goSaveAmount ==0.0){
      if (TotalBalance - AvailableBalance == goSaveTotalBalance) {
        return true;
      }
    }else{
      if (TotalBalance - AvailableBalance == myTotalAfterJoinPot && myTotalAfterJoinPot==goSaveTotalBalance -goSaveAmount) {
        return true;
      }
    }
    return false;
  }

  public boolean gotoPenging(){
    checkGoSavePending();
    boolean flag = isHaveGoSaveItemInPending();
    return flag;
  }

  @SneakyThrows
  public void clickWithDrawSetAmount(String amount){
    goSaveAmount = strToDouble(amount);
    clickGoSaveDetail();
    scrollUpToFindElement(withDrawBtn,5,2);
    clickElement(withDrawBtn);

    waitUntilElementClickable(withDrawConfirm);
    clickElement(withDrawConfirm);

    String withDarwalMaxMoneyStr = withDarwalMaxMoney.getText().split("HKD")[1];
    double money=strToDouble(withDarwalMaxMoneyStr);
    if(goSaveAmount > money){
      goSaveAmount =money-20.01;
      clearAndSendKeys(withDrawInput,String.valueOf(goSaveAmount));
      inputEnter(withDrawInput);
      Thread.sleep(2000);
      waitUntilElementClickable(confirmBtn);
      clickElement(confirmBtn);
      waitUntilElementClickable(improveBtn1);
      clickElement(improveBtn1);
      waitUntilElementClickable(improveDone);
      clickElement(improveDone);
    }else{
      clearAndSendKeys(withDrawInput,amount);
      inputEnter(withDrawInput);
      clickElement(confirmBtn);
      clickElement(confirmBtn);
    }
  }

  @SneakyThrows
  public void verifyWithDrawalCompletedPage(String screenShotName){
    Boolean flag = false;
    Thread.sleep(2000);
    System.out.println(withDrawCompleted.getText()+"withDrawCompleted.getText()");
    if (withDrawCompleted.getText().contains("Withdrawal completed")||withDrawCompleted.getText().contains("成功提取")) {
      if (withDrawCompleted.getText().contains("Withdrawal completed")){
        getMoneyWithDrawCompletedEn();
      }else{
        getMoneyWithDrawCompletedCH();
      }
      flag = true;
    } else if (withDrawCompleted.getText().contains("Processing")||withDrawCompleted.getText().contains("提取處理中")) {
      goSaveAmount = 0.0;
//      carStatus = 1;
      flag = true;
    } else if (withDrawCompleted.getText().contains("Left")||withDrawCompleted.getText().contains("你已離開")) {
      if (withDrawCompleted.getText().contains("Left")){
        getMoneyWithDrawCompletedEn();
      }else{
        getMoneyWithDrawCompletedCH();      }
      flag = true;
    }
//    amoutStr=goSaveTotalBalance -goSaveAmount;
    assertThat(flag).isTrue();
    takeScreenshot(screenShotName);
    Thread.sleep(2000);
    waitUntilElementClickable(doneBtn);
    clickElement(doneBtn);
  }

  public double getMoneyWithDrawCompletedCH(){
    goSaveAmount =strToDouble(moneyWithDrawCompleted.getText().split("HKD")[1].split("至")[0].replaceAll(" ",""));
    return goSaveAmount;
  }
  public double getMoneyWithDrawCompletedEn(){
    goSaveAmount =strToDouble(moneyWithDrawCompleted.getText().split("HKD")[1].split("from")[0].replaceAll(" ",""));
    return goSaveAmount;
  }

  @SneakyThrows
  public boolean verifyGoSaveBalanceAfterJoinPot(String screenShotName) {
    waitUntilElementVisible(myGoSave);
    Thread.sleep(3000);
    myTotalAfterJoinPot=strToDouble(goSaveBalance.getText());
    boolean flag=false;
    if(myTotalAfterJoinPot+goSaveAmount==goSaveTotalBalance){
      flag =true;
    }
//    assertThat(myTotalAfterJoinPot).isEqualTo(amoutStr);
    takeScreenshot(screenShotName);
    clickElement(myGoSaveHeaderBack);
    return flag;
  }
  public void clickGoSaveBalance() {
    waitUntilElementClickable(goSaveBalance);
    goSaveBalance.click();
  }
  @SneakyThrows
  public void clickInprogressWithDrawSetAmount(String amount) {
    Thread.sleep(2000);
    System.out.println(withDrawtimesleft.getText()+"vvvvvvvvvvvvvvvvvvvvvvvvv");
    if (withDrawtimesleft.getText().contains("You have 1 withdrawal left")||withDrawtimesleft.getText().contains("取款次數剩餘1次")) {
      waitUntilElementClickable(withDrawConfirm);
      clickElement(withDrawConfirm);
      waitUntilElementVisible(allAvailable);
      clickElement(allAvailable);
    }else{
      waitUntilElementClickable(withDrawConfirm);
      clickElement(withDrawConfirm);
      String withDarwalMaxMoneyStr = withDarwalMaxMoneyInProgress.getText().split("HKD")[1].replace(" ","");
      double money=strToDouble(withDarwalMaxMoneyStr);
      System.out.println(withDarwalMaxMoneyInProgress.getText());
      if(strToDouble(amount) >= money){
        goSaveAmount =money;
        clearAndSendKeys(withDrawInput,withDarwalMaxMoneyStr);
        inputEnter(withDrawInput);
        Thread.sleep(2000);
      }else{
        Thread.sleep(2000);
        clearAndSendKeys(withDrawInput,amount);
      }
//        goSaveAmount = Double.parseDouble(withDrawInput.getText());
    }
    Thread.sleep(2000);
    waitUntilElementClickable(confirmBtn);
    clickElement(confirmBtn);
    Thread.sleep(2000);
    clickElement(improveBtn1);
    Thread.sleep(2000);
    clickElement(improveDone);
  }

  @SneakyThrows
  public void checkTheHomePageTotalBalance(String screenShotName){
    Thread.sleep(2000);
    waitUntilElementClickable(homeTab);
    clickElement(homeTab);
//    waitUntilElementVisible(theAccountTotalBalance);
//    TotalBalance = getAccountTotalBalance();
//    AvailableBalance = getAccountAvailableBalance();
//    System.out.println("TotalBalance: " + TotalBalance + ",AvailableBalance:" + AvailableBalance);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void selectCar(){
    waitUntilElementClickable(inProgress);
    clickElement(inProgress);
    boolean carNumflag = true;
    if (carDetail.size() == 0) {
      assertThat(carNumflag).isFalse();
    } else {
      for (int i = 0; i < 20; i++) {
        if (StatusLists.size() >= 2) {
          if(i==0) {
            scrollInProgress("1000");
          }else{
            scrollInProgress("830");
          }
        } else {
          clickElement(carDetail.get(1));
          Thread.sleep(2000);
          scrollUp();
          if(StatusLists.size()==1){
            Thread.sleep(2000);
            clickElement(backInCarDetail);
            Thread.sleep(2000);
            clickElement(carDetail.get(2));
            Thread.sleep(2000);
            scrollUp();
            if(StatusLists.size()==1){
              Thread.sleep(2000);
              clickElement(backInCarDetail);
              Thread.sleep(2000);
              clickElement(carDetail.get(3));
              Thread.sleep(2000);
              scrollUp();
            }
          }
          clickElement(withDrawBtn);
          break;
        }
      }
    }
  }
  public void switchH5() {
    contextWebview();
  }
  @SneakyThrows
  public void joinNow() {
    Thread.sleep(3000);

    clickElement(driver.findElement(By.xpath("//button[@aria-disabled='false']")));
    Thread.sleep(3000);
  }
  @SneakyThrows
  public void enter(String amount) {
    Thread.sleep(3000);
    driver.switchTo();
    clickElement(driver.findElement(By.xpath("//input[@aria-required='false']")));
//    andSendKeys(depositAmount,amount);
    Thread.sleep(3000);
    andSendKeys(driver.findElement(By.xpath("//input[@aria-required='false']")),amount);
    Thread.sleep(3000);
  }
  @SneakyThrows
  public void clickNext() {
    Thread.sleep(3000);
    clickElement(driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/ngx-input-amount/div[2]/button")));
    Thread.sleep(3000);
  }

  @SneakyThrows
  public void clickedConditions() {
    Thread.sleep(3000);
    clickElement(driver.findElement(By.xpath("//button[@aria-disabled='false']")));
    Thread.sleep(3000);
  }
  @SneakyThrows
  public void clickComplete() {
    Thread.sleep(3000);
    clickElement(driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/ngx-input-amount/div[2]/button")));

    Thread.sleep(3000);
  }
  @SneakyThrows
  public void switchNative() {
    Thread.sleep(3000);
    returnToNayive();
    Thread.sleep(3000);
  }
}