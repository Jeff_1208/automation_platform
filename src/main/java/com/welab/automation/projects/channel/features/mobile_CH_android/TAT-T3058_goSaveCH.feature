Feature: Gosave

  Background: Login
    Given Open Stage WeLab App
    When Login with user and password from properties
#    Then I can see the LoggedIn page

  Scenario Outline:  Reg_GoSave_001 join a pot
    And I verify Total balance and Available balance
      | screenShotName | Reg_GoSave_001_Android_01 |
    Given I go to GoSave Page
      | screenShotName | Reg_GoSave_001_Android_02 |
      | screenShotName1 | Reg_GoSave_001_Android_03 |
    And I move eyeBtn to top right corner
    And I click the gosave pot
    And I join gosave pot set <amount>
    And I check gosave status
      | screenShotName | Reg_GoSave_001_Android_04 |
    Then I verify the gosave balance
      | screenShotName | Reg_GoSave_001_Android_05 |
    Then I verify the total balance
      | screenShotName | Reg_GoSave_001_Android_06 |
    Examples:
      | amount |
      | 100    |

  Scenario Outline:  Reg_GoSave_002 join a pot more
    And I verify Total balance and Available balance
      | screenShotName | Reg_GoSave_002_Android_01 |
    Given I go to GoSave Page
      | screenShotName | Reg_GoSave_002_Android_02 |
      | screenShotName1 | Reg_GoSave_002_Android_03 |
    And I move eyeBtn to top right corner
    And I check mygosave pending
    And I join gosave pot more set <amount>
    And I check gosave deposited more status
      | screenShotName | Reg_GoSave_002_Android_04 |
    Then I verify the gosave balance
      | screenShotName | Reg_GoSave_002_Android_05 |
    Then I verify the total balance
      | screenShotName | Reg_GoSave_002_Android_06 |
    Examples:
      | amount |
      | 100    |

  @VCAT-26
  Scenario Outline: Reg_GoSave_003 Withdrawal before pot start
    And I verify Total balance and Available balance
      | screenShotName | Reg_GoSave_003_Android_01 |
    And I goto Go save page
      | screenShotName | Reg_GoSave_003_Android_02 |
    And I move eyeBtn to top right corner
#    And I verify balance amount withdrawal
    And I goto Pending car Page withDrawal set <amount>
    And I verify withDrawal completed page
      | screenShotName | Reg_GoSave_003_Android_03 |
    Then I verify myTotal Balance After
      | screenShotName | Reg_GoSave_003_Android_04 |
    Then I verify the home page total balance
      | screenShotName | Reg_GoSave_003_Android_05 |
#    And I verify balance amount withdrawal
    Examples:
      | amount |
      | 900   |

  @VCAT-30
  Scenario: Reg_GoSave_005 view started pot
    And I go to GoSave Page
      | screenShotName | Reg_GoSave_005_Android_01 |
    And I move eyeBtn to top right corner
    Then I check myGoSave In progress
      | screenShotName | Reg_GoSave_005_Android_02 |
    Then I check GoSave Order deposit
      | screenShotName | Reg_GoSave_005_Android_03 |



  @VCAT-32
  Scenario Outline: Reg_GoSave_006 Withdrawal after pot start
    And I verify Total balance and Available balance
      | screenShotName | Reg_GoSave_006_Android_01 |
    And I goto Go save page
      | screenShotName | Reg_GoSave_006_Android_02 |
    And I move eyeBtn to top right corner
#    And I verify balance amount withdrawal
    And I goto In progress car Page withDrawal set <amount>
    And I verify withDrawal completed page
      | screenShotName | Reg_GoSave_006_Android_03 |
    Then I verify myTotal Balance After
      | screenShotName | Reg_GoSave_006_Android_04 |
    Then I verify the home page total balance
      | screenShotName | Reg_GoSave_006_Android_05 |
#    And I verify balance amount withdrawal
    Examples:
      | amount |
      | 100    |