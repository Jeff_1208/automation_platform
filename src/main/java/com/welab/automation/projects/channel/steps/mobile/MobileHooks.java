package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Locale;

import static com.welab.automation.framework.utils.TimeUtils.getCurrentTimeAsString;

public class MobileHooks extends AppiumBasePage {
  private static final Logger logger = LoggerFactory.getLogger(MobileHooks.class);
  BaseDriver baseDriver;
  boolean recordVideo = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("recordVideo"));

  @Before
  public void beforeScenario(Scenario scenario) throws IOException {
    logger.info("* Start running scenario: {}", scenario.getName());
    this.baseDriver = new BaseDriver();
    this.baseDriver.initMobileDriver();
    System.out.println(BaseDriver.getMobileDriver());
    if (recordVideo) {
      startRecording();
    }
    storeCaseInitResult(scenario);
  }

  @After
  public void afterScenario(Scenario scenario) {
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    String screenshotVideoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      stopRecording(screenshotVideoName);
    }

    if (scenario.isFailed()) {
      // Take screenshot to report
      final byte[] screenshot =
          ((TakesScreenshot) BaseDriver.getMobileDriver()).getScreenshotAs(OutputType.BYTES);
      scenario.attach(screenshot, "image/png", screenshotVideoName);
      // Take screenshots to project folder
      takeScreenshot(screenshotVideoName);
    }
    System.out.println("scenario.isFailed(): "+scenario.isFailed());
    if (System.getProperty("platform").toUpperCase().contains("MOBILE")) {
      //if (!scenario.isFailed()) {
        String channelScenarioName = scenario.getName();
        String testCaseName = channelScenarioName.split(" ")[0].trim();
        if(!GlobalVar.CHANNEL_PASS_CASE_NAME.contains(testCaseName)){
          GlobalVar.CHANNEL_PASS_CASE_NAME.add(testCaseName);
        }
      //}
    }

    storeCaseResult(scenario);

    logger.info("* End running scenario: {}", scenario.getName());
    logger.info("* End running scenario: {}", scenario.getSourceTagNames());
    logger.info("* End running scenario isFailed: {}", scenario.isFailed());
    BaseDriver.closeMobileDriver();
  }

  public void storeCaseInitResult(Scenario scenario){
    String tag = getCaseTag(scenario);
    if(tag != ""){
      tag = tag.substring(1);
      GlobalVar.CASE_RESULT.put(tag, false);
    }
  }

  public void storeCaseResult(Scenario scenario){
    String tag = getCaseTag(scenario);
    if(tag != ""){
      tag = tag.substring(1);
      GlobalVar.CASE_RESULT.put(tag, !scenario.isFailed());
    }
  }

  public String getCaseTag(Scenario scenario){
    Collection<String> tags = scenario.getSourceTagNames();
    String tag = "";
    for (String t: tags) {
      if(t.startsWith("@ACBN")){
        tag = t;
        break;
      }
      if(t.startsWith("@GOS")){
        tag = t;
        break;
      }
    }
    return tag;
  }
}
