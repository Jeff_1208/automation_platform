package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomRect;

public class SampleAppPage extends AppiumBasePage {
    private final String pageName = "Sample App Page";

    public SampleAppPage() {
        super.pageName = this.pageName;
    }


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Server Certificate Options\"]")
    private MobileElement serverCertificateOptions;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Server with Valid Hong Kong Post eCert\"]/../XCUIElementTypeButton")
    private MobileElement serverWithValidHongKongPosteCertCheckBox;

    public void selectCheckBox(MobileElement ele){
        String status = ele.getAttribute("name");
        if(status.contains("Unchecked")){
            clickElement(ele);
        }
    }

    @SneakyThrows
    public void setServerCertificateOptions(String screenShotName){
        Thread.sleep(3000);
        scrollDown();
        Thread.sleep(1000);
        clickElement(serverCertificateOptions);
        Thread.sleep(1000);
        selectCheckBox(serverWithValidHongKongPosteCertCheckBox);
        takeScreenshot(screenShotName);
        clickElement(serverCertificateOptions);
        Thread.sleep(1000);
    }


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Negative Testing Options\"]")
    private MobileElement negativeTestingOptions;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Invalid PRO\"]/../XCUIElementTypeButton")
    private MobileElement invalidPROCheckBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Invalid URL\"]/../XCUIElementTypeButton")
    private MobileElement invalidURLCheckBox;


    @SneakyThrows
    public void setNegativeTestingOptions(String screenShotName){
        clickElement(negativeTestingOptions);
        selectCheckBox(invalidPROCheckBox);
        takeScreenshot(screenShotName);
        clickElement(negativeTestingOptions);
        Thread.sleep(1000);
    }


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"FPS\"]")
    private MobileElement FPS;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"FPS Globally Unique Identifier\"]/../XCUIElementTypeButton")
    private MobileElement fpsGloballyUniqueIdentifierCheckBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"FPS Globally Unique Identifier\"]/../XCUIElementTypeTextField")
    private MobileElement fpsGloballyUniqueIdentifierInput;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Email Address\"]/../XCUIElementTypeButton")
    private MobileElement fpsEmailAddressCheckBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Email Address\"]/../XCUIElementTypeTextField")
    private MobileElement fpsEmailAddressInput;


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Editable Transaction Amount Indicatior\"]/../XCUIElementTypeButton")
    private MobileElement editableTransactionAmountIndicatiorCheckBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Editable Transaction Amount Indicatior\"]/../XCUIElementTypeTextField")
    private MobileElement editableTransactionAmountIndicatiorInput;

    public void scrollingToSubElement() {
        IOSElement ele = (IOSElement) driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"FPS Globally Unique Identifier\"]"));
        MobileElement radioGroup = ele
                .findElement(MobileBy
                        .AndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView("
                                + "new UiSelector().text(\"Email Address\"));"));
    }

    @SneakyThrows
    public void setFPSIos(String screenShotName){
        clickElement(FPS);
        Thread.sleep(1000);
        //selectCheckBox(fpsGloballyUniqueIdentifierCheckBox);
        //clearAndSendKeys(fpsGloballyUniqueIdentifierInput, "hk.com.hkicl");
//        AppiumDriver appiumDriver = (AppiumDriver) driver;
//        appiumDriver.context("");
//        appiumDriver.swipe(startx, starty, endx, starty, 1000);
        scrollingToSubElement();
        scrollUpByStartEndHeightPercent(70, 50);
        Thread.sleep(1000);
        //selectCheckBox(fpsEmailAddressCheckBox);
        clearAndSendKeys(fpsEmailAddressInput, "iclmrch01@hkicl.com.hk");
        takeScreenshot(screenShotName);
        scrollUpByStartEndHeightPercent(70, 50);
        Thread.sleep(1000);
        selectCheckBox(editableTransactionAmountIndicatiorCheckBox);
        clearAndSendKeys(editableTransactionAmountIndicatiorInput, "10");
        takeScreenshot(screenShotName);
        scrollDown();
        scrollDown();
        Thread.sleep(1000);
        clickElement(FPS);
        Thread.sleep(1000);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Additional Data\"]")
    private MobileElement additionalData;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Reference Label\"]/../XCUIElementTypeButton")
    private MobileElement referenceLabelCheckBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Reference Label\"]/../XCUIElementTypeTextField")
    private MobileElement referenceLabelInput;


    @SneakyThrows
    public void setAdditionalData(String screenShotName){
        clickElement(additionalData);
        selectCheckBox(referenceLabelCheckBox);
        clearAndSendKeys(fpsGloballyUniqueIdentifierInput, "abcdtest123");
        takeScreenshot(screenShotName);
        clickElement(additionalData);
        Thread.sleep(1000);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"Products\"]")
    private MobileElement products;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Apple\"]/../(//XCUIElementTypeButton[@name=\"Add to Cart\"])[1]")
    private MobileElement appleAddToCart;

    @SneakyThrows
    public void setProducts(String screenShotName){
        clickElement(products);
        clickElement(appleAddToCart);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(products);
        Thread.sleep(1000);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Transaction Detail\"]")
    private MobileElement transactionDetail;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"Transaction Detail\"]/following-sibling::XCUIElementTypeCell/XCUIElementTypeTextField")
    private MobileElement TransactionAmount;

    @SneakyThrows
    public void setTransactionDetail(String screenShotName){
        clickElement(transactionDetail);
        scrollUpByStartEndHeightPercent(70, 30);
        Thread.sleep(1000);
        clearAndSendKeys(TransactionAmount,"100");
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickElement(transactionDetail);
        Thread.sleep(1000);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"PAY (with callback)\"]")
    private MobileElement payWithCallback;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"PAY (without callback)\"]")
    private MobileElement payWithoutCallback;

    @SneakyThrows
    public void clickPayWithCallback(String screenShotName){
        clickElement(payWithCallback);
    }

    @SneakyThrows
    public void clickPayWithoutCallback(String screenShotName){
        clickElement(payWithoutCallback);
    }


}
