Feature: Welab app　Card　MCV

  Background: Login
    Given Open Stage WeLab App
#    And Enable skip Root Stage
    When Login with user and password from properties



  Scenario: Reg_Card_001 View Card detail
    Given get home page
      | screenShotName  | Reg_Card_001_Android_01 |
      | screenShotName1 | Reg_Card_001_Android_02 |
    And I click the debit card
      | screenShotName  | Reg_Card_001_Android_03 |
    And I click Card details
    When I enterI enter input MSK key
      | screenShotName  | Reg_Card_001_Android_04 |
    And I verify details
      | screenShotName  | Reg_Card_001_Android_05 |
    Then get card information
      | screenShotName  | Reg_Card_001_Android_06 |


#  Scenario:  Reg_Card_007 Active Card flow
#    Given get home page
#      | screenShotName  | Reg_Card_007_Android_01 |
#      | screenShotName1 | Reg_Card_007_Android_02 |
#    And I click the debit card
#      | screenShotName | Reg_Card_007_Android_03 |
#    Then activate Debit Card
#      | screenShotName | Reg_Card_007_Android_04|
#      | screenShotName1 | Reg_Card_007_Android_05|
#      | screenShotName2 | Reg_Card_007_Android_06|
#      | screenShotName3 | Reg_Card_007_Android_07|
#      | screenShotName4 | Reg_Card_007_Android_08|
#      | screenShotName5 | Reg_Card_007_Android_09|



#  Scenario: Reg_Card_008 Reset Card PIN Flow
#    Given get home page
#      | screenShotName  | Reg_Card_008_Android_01 |
#      | screenShotName1 | Reg_Card_008_Android_02 |
#    And I click the debit card
#      | screenShotName | Reg_Card_008_Android_03 |
#    Then I click reset card PIN
#      | screenShotName | Reg_Card_008_Android_04 |
#      | screenShotName1 | Reg_Card_008_Android_05 |


  Scenario Outline:  Reg_Card_003 Android Mastercard transaction 1. Online under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName  | Reg_Card_003_Android_01 |
      | screenShotName1 | Reg_Card_003_Android_02 |
    And I click the debit card
      | screenShotName | Reg_Card_003_Android_03 |
    Then set limit large android
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_003_Android_04 |
      | screenShotName1 | Reg_Card_003_Android_05 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_003_Android_06 |
      | screenShotName1 | Reg_Card_003_Android_07 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability | Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10033   | 344     | 6                  | 10033   | 344      | 810           | -133      | 133      |


  Scenario Outline:  Reg_Card_004 Android Mastercard transaction 2. POS under HKD 10,000 + over HKD 10,000
    Given get home page
      | screenShotName  | Reg_Card_004_Android_01 |
      | screenShotName1 | Reg_Card_004_Android_02 |
    When run gps request <Bill_Amt> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_004_Android_03 |
      | screenShotName1 | Reg_Card_004_Android_04 |
    When run gps request <Bill_Amt2> <Txn_Ccy> <GPS_POS_Capability> <Txn_Amt2> <Bill_Ccy> <POS_Data_DE22>
    Then check gps transation
      | screenShotName  | Reg_Card_004_Android_05 |
      | screenShotName1 | Reg_Card_004_Android_06 |
    Examples:
      | Bill_Amt | Txn_Ccy | GPS_POS_Capability | Txn_Amt | Bill_Ccy | POS_Data_DE22 | Bill_Amt2 | Txn_Amt2 |
      | -10044   | 344     | 6                  | 10044   | 344      | 032           | -144      | 144      |


  Scenario: Reg_Card_005 Insights page information display
    And I click the debit card
      | screenShotName | Reg_Card_005_Android_01 |
    Then Verify Insights page display
      | screenShotName | Reg_Card_005_Android_02 |
      | screenShotName1 | Reg_Card_005_Android_03 |



  Scenario: Reg_Card_002 Update Card limit for MCV
    And I click the debit card
      | screenShotName | Reg_Card_002_Android_01 |
    And I click Daily card transaction limits
      | screenShotName | Reg_Card_002_Android_02 |
    Then Change card limit for MCV
      | cardLimit | 1000 |
      | screenShotName | Reg_Card_002_Android_03 |
      | screenShotName1 | Reg_Card_002_Android_04 |



  Scenario: Reg_Card_006 Lost card Flow for MCV
    Given get home page
      | screenShotName  | Reg_Card_006_Android_01 |
      | screenShotName1 | Reg_Card_006_Android_02 |
    And I click the debit card
      | screenShotName | Reg_Card_006_Android_03 |
    Then Click lost card and verify lost success for MCV
      | screenShotName | Reg_Card_006_Android_04 |
      | screenShotName1 | Reg_Card_006_Android_05 |

