package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.SneakyThrows;

public class PayrollPage extends AppiumBasePage {
    private final String pageName = "Payroll Page";

    PaymentPage paymentPage;
    CommonPage commonPage;
    public PayrollPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        paymentPage = new PaymentPage();
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Payroll Account']")
    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '發薪賬戶')]")
    private MobileElement paySalaryAccountPage;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text,'Core Account')]")
    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '核心賬戶 金額')]")
    private MobileElement transferAmountPage;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text,'Ensure your Core Account has sufficient funds')]")
    @iOSXCUITFindBy(xpath = "//*[contains(@name, '請確保你嘅核心賬戶於即時或自動入錢時有⾜夠嘅資金')]")
    private MobileElement addMoneyPage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Download']")
    @iOSXCUITFindBy(xpath = "//*[contains(@name, '下載')]")
    private MobileElement downloadPage;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"流動保安編碼認證\" AND name == \"text\"")
    private MobileElement MSKpage;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'sent money!')]")
    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '轉賬成功')]")
    private MobileElement transfersuccessfullPage;

    @AndroidFindBy(xpath = "//*[@text='Payroll Account']")
    private MobileElement payrollAccount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='始终允许']"),
            @AndroidBy(xpath = "//*[@text='允许']")
    })
    private MobileElement allwaysAllow;

    @AndroidFindBy(xpath = "//*[@text='Ok']")
    private MobileElement okBtn;

    @AndroidFindBy(xpath = "//*[@text='Confirm']")
    private MobileElement confirmBtn;

    @AndroidFindBy(xpath = "//*[@text='Add money']")
    private MobileElement addMoney;

    @AndroidFindBy(xpath = "//[@text='Send money']")
    private MobileElement sendMoneyBtn;

    @AndroidFindBy(xpath = "//*[@text='Core Account']/../../android.view.ViewGroup[2]/android.widget.EditText")
    private MobileElement payrollInput;

    @AndroidFindBy(xpath = "//*[@text='Send money']")
    private MobileElement sendMoney;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'ve added')]")
    private MobileElement addMoneySuccess;

    @AndroidFindBy(xpath = "//*[@text='Done']")
    private MobileElement doneBtn;

    @AndroidFindBy(xpath = "//*[@text='Download']")
    private MobileElement download;

    public void inputMSK(){
        waitUntilElementVisible(MSKpage);
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        commonPage.inputMSKByString(mskPassword);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text,'The Payroll Account')]")
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"發薪賬戶將被關閉!\" AND name == \"text\"")
    private MobileElement payAccountWillClose;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text,'Got it')]")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"明白\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"明白\"])[2]"),
    })
    private MobileElement understandButton;

    @SneakyThrows
    public void openPayrollPage(String screenShotName) {
        if(isShow(payAccountWillClose,3)){
            clickElement(understandButton);
        }
        clickPaySalaryAccount();
        checkPayrollPage(screenShotName);
    }

    public void clickPaySalaryAccount(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,37);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,34);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,30);
        }else{
            clickByLocationByPercent(50,37);
        }
    }

    @SneakyThrows
    public void openTransactionPage(){
        clickTransactionButton();
        waitUntilElementVisible(transferAmountPage);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void openAddMoneyPage(){
        clickAddMoneyButton();
        waitUntilElementVisible(addMoneyPage);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void openDownloadPage(){
        clickDownloadButton();
        waitUntilElementVisible(downloadPage);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void inputAmountAndSwipeTheSlide(String amount, String screenShotName){
        clickAmountInput();
        Thread.sleep(1000);
        commonPage.inputMSKByString(amount);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        paymentPage.slipeConfirmBtnWithImage();
        inputMSK();
    }

    @SneakyThrows
    public void inputAmountAndConfirm(String amount, String screenShotName){
        clickAmountInput();
        Thread.sleep(1000);
        commonPage.inputMSKByString(amount);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickConfirmForInputAmount();
        inputMSK();
    }

    @SneakyThrows
    public void checkTransactionResultPage(String screenShotName){
        waitUntilElementVisible(transfersuccessfullPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickFinishButton();
    }

    @SneakyThrows
    public void checkPayrollPage(String screenShotName){
        waitUntilElementVisible(paySalaryAccountPage);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        Thread.sleep(3000);
    }

    public void clickConfirmForInputAmount(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,61);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,64);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,61.8);
        }else{
            clickByLocationByPercent(50,61);
        }
    }

    public void clickTransactionButton(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,31);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,28);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,24.4);
        }else{
            clickByLocationByPercent(50,31);
        }
    }

    public void clickAddMoneyButton(){
        if(isIphoneSE()){
            clickByLocationByPercent(21,31);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(19,28);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,24.4);
        }else{
            clickByLocationByPercent(21,31);
        }
    }

    public void clickDownloadButton(){
        if(isIphoneSE()){
            clickByLocationByPercent(69,31);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(79,28);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(79,24.4);
        }else{
            clickByLocationByPercent(69,31);
        }
    }

    @SneakyThrows
    public void checkEstatementContent(String screenShotName){
        clickTheMonthItem();
        Thread.sleep(6000);
        takeScreenshot(screenShotName);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '存储到')]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"存储到“文件”\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeCell[@name=\"儲存至檔案\"]/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]"),
    })
    private MobileElement saveToFiles;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"WeLab Bank STAGE\"]")
    private MobileElement savePathItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"存储\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"儲存\""),
    })
    private MobileElement storageButton;


    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"替换\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"取代\"]"),
    })
    private MobileElement replaceButton;

    @SneakyThrows
    public void downloadEstatementPDF(String screenShotName){
        clickDownloadButtonForEstatement();
        Thread.sleep(2000);
        scrollUpByStartEndHeightPercent(90,40);
        Thread.sleep(2000);
        clickElement(saveToFiles);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(storageButton);
        Thread.sleep(1000);
        if(isShow(replaceButton,3)){
            clickElement(replaceButton);
        }
    }

    public void clickTheMonthItem(){
        //second month
        if(isIphoneSE()){
            clickByLocationByPercent(50,30);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,27);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,23.7);
        }else{
            clickByLocationByPercent(50,30);
        }
    }

    public void clickDownloadButtonForEstatement(){
        //iphone SE and iphone7 plus OK
        clickByLocationByPercent(92,6);
    }

    public void clickAmountInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,32);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,29);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,25.8);
        }else{
            clickByLocationByPercent(50,32);
        }
    }

    public void clickFinishButton(){
        clickByLocationByPercent(50,92);
    }


    @SneakyThrows
    public void goToPayrollPage(String screenShotName) {
        Thread.sleep(3000);
        if (isShow(payAccountWillClose, 3)) {
            clickElement(understandButton);
        }
        Thread.sleep(3000);
        clickElement(payrollAccount);
        Thread.sleep(3000);
        checkPayrollPage(screenShotName);
    }

    @SneakyThrows
    public void clickSendMoney(String screenShotName) {
        Thread.sleep(2000);
        clickElement(sendMoneyBtn);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void SendMoneyAtPayrollPage(String amount, String screenShotName) {
        Thread.sleep(2000);
        clickElement(sendMoney);

        Thread.sleep(2000);
        waitUntilElementVisible(transferAmountPage);
        clickElement(payrollInput);
        andSendKeys(payrollInput, amount);
        inputEnter(payrollInput);
        takeScreenshot(screenShotName);

        paymentPage.slideToPay();
        Thread.sleep(2000);
        commonPage.waiteSendMsk();
    }

    @SneakyThrows
    public void verifyTransactionResultPage(String screenShotName) {
        waitUntilElementVisible(transfersuccessfullPage);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(doneBtn);
    }

    @SneakyThrows
    public void AddMoneyAtPayrollPage(String amount, String screenShotName) {
        Thread.sleep(2000);
        clickElement(addMoney);

        Thread.sleep(2000);
        waitUntilElementVisible(addMoneyPage);

        clickElement(payrollInput);
        andSendKeys(payrollInput, amount);
        inputEnter(payrollInput);
        takeScreenshot(screenShotName);

        Thread.sleep(2000);
        clickElement(confirmBtn);
        Thread.sleep(2000);
        commonPage.waiteSendMsk();
    }

    @SneakyThrows
    public void checkAddMoneySuccess(String screenShotName) {
        waitUntilElementVisible(addMoneySuccess);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(doneBtn);
    }

    @SneakyThrows
    public void goToDownloadPage() {
        Thread.sleep(2000);
        clickElement(download);
        waitUntilElementVisible(downloadPage);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void downloadEstatementPDFAndroid(String screenShotName) {
        clickDownloadButtonForEstatement();
        Thread.sleep(2000);
        if(isShow(allwaysAllow,3)){
            clickElement(allwaysAllow);
        }
        takeScreenshot(screenShotName);
        clickElement(okBtn);
    }


}
