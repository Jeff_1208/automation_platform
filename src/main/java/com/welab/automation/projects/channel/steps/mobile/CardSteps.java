package com.welab.automation.projects.channel.steps.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.projects.channel.pages.CardPage;
import com.welab.automation.projects.channel.steps.gps.GpsHttpRequest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CardSteps {

    CardPage cardPage;
    public CardSteps(){
        cardPage = new CardPage();
    }

    @Given("get home page")
    public void getHomePage(Map<String, String> data){
        cardPage.getHomePage(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @When("^run gps request ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void runGpsRequest(String Bill_Amt, String Txn_Ccy, String GPS_POS_Capability,String Txn_Amt, String Bill_Ccy, String POS_Data_DE22) {
        String token = GlobalVar.GLOBAL_VARIABLES.get("token");
        GpsHttpRequest gpsHttpRequest = new GpsHttpRequest(Bill_Amt, Txn_Ccy, GPS_POS_Capability,token,Txn_Amt,Bill_Ccy,POS_Data_DE22);
        boolean result = gpsHttpRequest.runGpsApi();
        assertThat(result).isTrue();
        System.out.println("result: "+result);
    }
    @When("^run gps request with error ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void runGpsRequestWithError(String Bill_Amt, String Txn_Ccy, String GPS_POS_Capability,String Txn_Amt, String Bill_Ccy, String POS_Data_DE22) {
        String token = GlobalVar.GLOBAL_VARIABLES.get("token");
        GpsHttpRequest gpsHttpRequest = new GpsHttpRequest(Bill_Amt, Txn_Ccy, GPS_POS_Capability,token,Txn_Amt,Bill_Ccy,POS_Data_DE22);
        boolean result = gpsHttpRequest.runGpsApi();
        assertThat(result).isFalse();
        System.out.println("result: "+result);
    }

    @Then("check gps transation")
    public void checkGpsTransaction(Map<String, String> data){
        cardPage.checkGpsTransaction(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("go to debit card")
    public void goToDebitCard(Map<String, String> data){
        cardPage.goToDebitCard(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("click card info and input msk")
    public void clickCardInfoAndinputMSK(Map<String, String> data){
        cardPage.clickCardInfoAndinputMSK(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("get card information")
    public void getCardInfomation(Map<String, String> data){
        cardPage.getCardInfomation(data.get("screenShotName"));
    }

    @Then("set limit")
    public void setLimit(Map<String, String> data){
        cardPage.setLimit(data.get("screenShotName"));
    }
    @Then("set limit large")
    public void setLimitLarge(){
        cardPage.setLimitLarge();
    }
    @Then("check notification")
    public void checkNotification(Map<String, String> data){
        cardPage.checkNotification(data.get("screenShotName"));
    }

    @And("I click the debit card")
    public void clickDebitCard(Map<String, String> data){
        cardPage.iClickDebitCard(data.get("screenShotName"));
    }

    @And("I click Card details")
    public void clickCardDetails(){
        cardPage.clickCardDetails();
    }

    @And("I click Daily card transaction limits")
    public void clickDailyCardTransactionLimits(Map<String, String> data){
        cardPage.clickDailyCardTransactionLimits(data.get("screenShotName"));
    }

    @And("I change transaction limit and withdrawal limit set ([^\"]\\S*) and ([^\"]\\S*)$")
    public void changeTransactionAndWithdrawalLimit(String transactionLimit, String withdrawalLimit, Map<String, String> data) {
//        NumberFormat nf = NumberFormat.getNumberInstance(Locale.CANADA);
        cardPage.changeDailyTransactionAndWithdrawalLimit(transactionLimit, withdrawalLimit, data.get("screenShotName"), data.get("screenShotName1"));
    }


    @Then("report card lost and notification")
    public void reportCardLost(Map<String, String> data){
        cardPage.reportCardLost(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("report card lost and notification for MCV")
    public void reportCardLostForMCV(Map<String, String> data){
        cardPage.reportCardLostForMCV(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("reset card pin flow")
    public void resetCardPinFlow(Map<String, String> data){
        cardPage.resetCardPinFlow(data.get("screenShotName"),data.get("screenShotName1"));
    }
    @Then("debit card activation")
    public void debitCardActivation(Map<String, String> data){
        cardPage.debitCardActivation(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),
                data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName5"),data.get("screenShotName6"));
    }

    @And("I get debit card number")
    public void iGetDebitCardNumber(){
        cardPage.getDebitCardNumber();
    }

    @And("I click Lost card")
    public void iClickLostCard(Map<String, String> data){
        assertThat(cardPage.clickLostCard(data.get("screenShotName"))).isTrue();
    }

    @And("I click Confirm button")
    public void iClickConfirmBtn(){
        cardPage.clickConfirmBtn();
    }

    @And("I verify report card lost successfully")
    public void iVerifyReportCardLostSuccess(Map<String, String> data){
        cardPage.verifyReportCardLostSuccess(data.get("screenShotName"));
    }
    @Then("set limit large android")
    public void setLimitLargeAndroid() {
        cardPage.setLimitLargeAndroid();
    }

    @And("I verify details")
    public void iVerifyCardDetails(Map<String, String> data){
        cardPage.verifyCardDetails(data.get("screenShotName"));
    }

    @Then("activate Debit Card")
    public void activateDebitCard(Map<String, String> data) {
        cardPage.activateDebitCard(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"),
                data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName5"));
    }

    @Then("I click reset card PIN")
    public void iClickResetCardPIN(Map<String, String> data) {
        assertThat(cardPage.clickResetCardPIN(data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }

    @Then("Insights page information display IOS")
    public void InsightsPageInformationDisplayIOS(Map<String, String> data){
        cardPage.InsightsPageInformationDisplayIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("Verify Insights page display")
    public void verifyInsightsPageDisplay(Map<String, String> data) {
        cardPage.verifyInsightsPageDisplay(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("Click lost card and verify lost success for MCV")
    public void clickLostCardAndVerifyLostSuccessForMCV(Map<String, String> data) {
        cardPage.clickAndVerifyLostCard(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("Change card limit for MCV")
    public void changeCardLimitForMCV(Map<String, String> data) {
        assertThat(cardPage.changeCardLimitForMCV(data.get("cardLimit"),data.get("screenShotName"),data.get("screenShotName1"))).isTrue();
    }
}
