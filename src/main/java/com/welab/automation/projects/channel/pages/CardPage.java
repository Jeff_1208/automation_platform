package com.welab.automation.projects.channel.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.channel.channeApiTest.channelAPI;
import com.welab.automation.projects.channel.channeApiTest.coreUiAndApiTest;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.SneakyThrows;
import static org.assertj.core.api.Assertions.assertThat;
import com.welab.automation.framework.GlobalVar;
import org.assertj.core.api.AssertionsForClassTypes;
import org.openqa.selenium.Point;

import java.util.List;

public class CardPage extends AppiumBasePage {
    private final String pageName = "Card Page";
    private boolean isHaveAddToAppleWalletBtn=true;
    CommonPage commonPage;
    String getWithdrawalLimitText;
    String getTransactionLimitText;
    public CardPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }

    private static String transactionText="";
    private static String cardNumbers;
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Recent transactions']"),
            @AndroidBy(xpath = "//android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.TextView")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 1 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 3 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 4 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 5 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 6 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 1 交易记录\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 2 交易记录\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 3 交易记录\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 4 交易记录\"]"),
    })
    public MobileElement currentlyTransactions;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 1 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 3 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 4 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 5 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 6 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 1 交易记录\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 2 交易记录\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 3 交易记录\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 处理中的交易 4 交易记录\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]/../XCUIElementTypeOther[2]"),
    })
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView//android.view.ViewGroup[2]//android.view.ViewGroup[2]/android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Recent transactions']")
    })
    public MobileElement transactionDetails;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Mobile security key']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='流動保安編碼認證']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"流動保安編碼認證\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@label=\"流动安全验证码认证\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"Mobile security key\" AND name == \"text\""),
    })
    public MobileElement MSKpage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"Reset card PIN\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"重設卡密碼\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"重置卡密码\" AND name == \"text\""),
    })
    private MobileElement resetPasswordPage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='WeLab Debit Card']")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"WeLab Debit Card\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"WeLab Debit Card\"])[5]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"WeLab Debit Card\"]"),
            @iOSXCUITBy(xpath = "//*[@name=\"WeLab Debit Card\"]"),
    })
    private MobileElement WelabDebitCardPageTitle;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Insights']")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Insights 卡資料\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"財務概覽 卡資料\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"财务概览 卡资料\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Insights Card details\"]")
    })
    private MobileElement cardInfomation;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 50,000.00\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 50,000.00\"])[3]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Maximum HKD 50,000.00\"])[3]")
    })
    private MobileElement debitCardTopLimit;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"請輸入不同限額\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"请输入不同限额\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"Please input different limit instead\"]")
    })
    private MobileElement plsInputDiffLimitAmount;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//(//XCUIElementTypeOther[@name=\"最高限額 HKD 50,000.00\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//(//XCUIElementTypeOther[@name=\"最高限额 HKD 50,000.00\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//(//XCUIElementTypeOther[@name=\"Maximum HKD 50,000.00\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField")
    })
    private MobileElement debitCardTopLimitInput;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"最高限額 HKD 20,000.00\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"最高限额 HKD 20,000.00\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"Maximum HKD 20,000.00\"]")
    })
    private MobileElement AtmCardTopLimit;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 20,000.00\"])[3]/XCUIElementTypeOther"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 20,000.00\"])[3]/XCUIElementTypeOther"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Maximum HKD 20,000.00\"])[3]/XCUIElementTypeOther")
    })
    private MobileElement AtmCardTopLimitInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Confirm']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='确认']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確認']"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"确认\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"確認\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Confirm\"])[2]")
    })
    private MobileElement setLimitConfirm;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 50,000.00\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 50,000.00\"])[3]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement currentLimitAmount;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 50,000.00\"])[5]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 50,000.00\"])[5]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限額 HKD 50,000.00\"])[6]/XCUIElementTypeOther/XCUIElementTypeTextField"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"最高限额 HKD 50,000.00\"])[6]/XCUIElementTypeOther/XCUIElementTypeTextField"),
    })
    private MobileElement noCardLimitAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Debit Card Transaction Setting']/following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Debit Card 交易設定\"])[3]/XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"借记卡交易设置\"])[3]/XCUIElementTypeOther[2]"),
    })
    private MobileElement setLimitPageCloseButton;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'你已超出無卡交易限額')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'你已超出交易限額')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'你已超出无卡交易限额')]"),
            //@iOSXCUITBy(xpath = "//XCUIElementTypeOther[contains(@name, '你已經超出你早前所設定嘅每日交易限額。請檢查你嘅交易限額設定，然後再試多次啦')]"),
    })
    private MobileElement notifacationContent;

    @AndroidFindBy(xpath = "//android.widget.ImageView/../android.widget.TextView[2]")
    //@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"主頁, tab, 1 of 4 GoSave, tab, 2 of 4 存款與轉賬, tab, 3 of 4 GoFlexi, tab, 4 of 4\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
    @iOSXCUITFindAll({
//            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"主頁, tab, 1 of 4 GoSave, tab, 2 of 4 存款與轉賬, tab, 3 of 4 GoFlexi, tab, 4 of 4\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"),
//            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"主頁, tab, 1 of 5 GoSave, tab, 2 of 5 存款與轉賬, tab, 3 of 5 GoFlexi, tab, 4 of 5 GoWealth, tab, 5 of 5\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"),
//            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"主頁, tab, 1 of 5 GoSave, tab, 2 of 5 存款與轉賬, tab, 3 of 5 貸款, tab, 4 of 5 GoWealth, tab, 5 of 5\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"),
//            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Home, tab, 1 of 5 GoSave, tab, 2 of 5 Transfer, tab, 3 of 5 Loan, tab, 4 of 5 GoWealth, tab, 5 of 5\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"),
//            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"主页, tab, 1 of 5 GoSave, tab, 2 of 5 存款与转账, tab, 3 of 5 贷款, tab, 4 of 5 GoWealth, tab, 5 of 5\"])[1]/../XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"财务概览 隐藏资料\"]/../XCUIElementTypeOther[1]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"財務概覽 隱藏資料\"]/../XCUIElementTypeOther[1]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"财务概览 卡资料\"]/../XCUIElementTypeOther[1]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"財務概覽 卡資料\"]/../XCUIElementTypeOther[1]"),
    })
    private MobileElement cardInfomationshow;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='WeLab Debit Card']")
    private MobileElement welabDebitCard;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
    private MobileElement securityKey;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Expiry date']"),
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[3]")
    })
    private MobileElement expiryDate;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='CVV']"),
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[4]")
    })
    private MobileElement cvv;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Daily card transaction limits']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='設定限額']")
    })
    private MobileElement transactionLimits;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText")
    private MobileElement transactionLimitText;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.EditText")
    private MobileElement withdrawalLimitText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Confirm']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確認']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確認報失']")
    })
    private MobileElement confirmBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Available balance (HKD)']"),
            @AndroidBy(xpath = "//*[@text='可用結餘 (HKD)']"),
    })    private MobileElement debitCard;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Card details']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='卡資料']")
    })
    private MobileElement cardDetails;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//[@name=\"報失卡\"]"),
            @iOSXCUITBy(xpath = "//[@value=\"报失卡\"]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"報失卡？\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Report lost card?\"])[2]")
    })
    private MobileElement reportCardLostPageTitle;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"確認報失\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"确认报失\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Confirm\"]")
    })
    private MobileElement confirmReportCardLostBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '你張實體卡預計會喺')]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"你已經成功報失卡。 你嘅虛擬卡已經可以用喇。\""),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Your card is expected to arrive by')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@value, '你已經成功報失卡')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@value, '你已成功报失卡')]"),
    })
    private MobileElement cardDeliverMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"啟動實體卡\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"启用实体卡\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Activate physical card\"])[1]")
    })
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Activate physical card']")
    private MobileElement activeCardBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"啟用實體卡前，請細閱保安提示以獲取更多資訊\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@value=\"启用实体卡前，请仔细阅读保安提示以获取更多信息\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Before activating your physical Debit Card, please read these security tips carefully.\"]")
    })
    @AndroidFindBy(
            xpath =
                    "//android.widget.TextView[@text='Before activating your physical Debit Card, please read these security tips carefully.']")
    private MobileElement activeCardPageMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Activate now\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"啟動\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@value=\"启用\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"启用\"]"),
    })
    @AndroidFindBy(
            xpath = "//android.widget.TextView[@text='Activate now']")
    private MobileElement activeBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Use 9-digit number located below the QR code\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"使用QR Code下方啟動編碼\"])[2]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"使用QR Code 下方启動编碼\" AND name == \"text\"")
    })
    @AndroidFindBy(xpath = "//android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView")
    private MobileElement activeCardByQRCodeMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"成功啟用實體Debit Card\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"成功启用实体借记卡\""),
            //@iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"成功啟用實體Debit Card\"]"),
            //@iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"Physical Debit Card activated successfully\"]")
    })
    private MobileElement activeCardSuccessMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Got it!']"),
            @AndroidBy(xpath = "//*[@text='明白啦!']"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"明白啦!\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"明白了！\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Got it!\"])[2]")
    })
    private MobileElement iSeeBtn;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"Set card PIN\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"設定WeLab Debit Card 密碼\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"设置WeLab Debit Card密码\" AND name == \"text\""),
    })
    private MobileElement setCardPasswordPageTitle;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    private MobileElement cardNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Lost card']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='報失卡']")
    })
    private MobileElement lostCard;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[@text=\"Reset card PIN\"]/following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[@text=\"流動保安編碼認證\"]/following-sibling::android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement resetPINCloseBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[@text=\"Set card PIN\"]/following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[@text=\"流動保安編碼認證\"]/following-sibling::android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement setPINCloseBtn;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[7]/android.widget.TextView[1]")
    private MobileElement reportAnErrorMessage;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
    private MobileElement cancelBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Activate physical card']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='啟動實體卡']/following-sibling::android.view.ViewGroup")
    })
    private MobileElement backBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.Button[@text='Allow']"),
            @AndroidBy(xpath = "//android.widget.Button[@text='ALWAYS ALLOW']"),
            @AndroidBy(xpath = "//android.widget.Button[@text='始终允许']"),
            @AndroidBy(xpath = "//android.widget.Button[@text='仅在使用中允许']"),

    })
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
    private MobileElement allowButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Allow all the time\"]")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow all the time'")
    private MobileElement allowAllButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Yes, I want to have physical Debit Card']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='想要實體Debit Card']/preceding-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement yesLostCard;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'You have successfully reported lost card')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'你已經成功報失卡')]"),
    })
    private MobileElement lostCardSuccess;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Done']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='完成']"),
    })
    @iOSXCUITFindBy(xpath = "//*[@name=\"完成\"]")
    private MobileElement doneBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Next']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='下一步']"),
    })
    private MobileElement nextBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Reset card PIN']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='重設卡密碼']")
    })
    private MobileElement resetPINBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Insights']")
    })
    private MobileElement insightsButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Your monthly spending']")
    })
    private MobileElement yourMonthlySpending;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Top spending categories this month']")
    })
    private MobileElement spendingCategories;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Debit Card Transaction Setting']")
    })
    private MobileElement debitCardTransactionSetting;



    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Debit Card transaction limit']/following-sibling::android.view.ViewGroup[1]/android.widget.EditText")
    })
    private MobileElement debitCardTransactionLimit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Debit Card Card-not-present transaction limit']/following-sibling::android.view.ViewGroup[2]/android.widget.EditText")
    })
    private MobileElement debitCardCardNotPresentTransactionLimit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Local ATM withdrawal limit']/following-sibling::android.view.ViewGroup[1]/android.widget.EditText")
    })
    private MobileElement localATMWithdrawalLimit;


    public static final String allow = "Allow";
    public static final String allowCH = "仅在使用中允许";
    public static final String alwaysAllow = "ALWAYS ALLOW";

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate= "label == \"財務概覽 卡資料\""),
            @iOSXCUITBy(iOSNsPredicate= "label == \"财务概览 卡资料\""),
    })
    private MobileElement cardInfoElement;

    public void clickCardInfomation(){
        int y = cardInfoElement.getCenter().getY();
        int height = driver.manage().window().getSize().height;
        double yPercent = y*100/height;
        logger.info("y: " + y);
        logger.info("height: " + height);
        logger.info("yPercent: " + yPercent);
        clickByLocationByPercent(70, yPercent);

        //not have apple wallet button
//        if(isIphoneSE()){
//            clickByLocationByPercent(70,43);  //scroll up
//        }else if(isIphone7Plus()){
//            clickByLocationByPercent(70,52);
//        }else if(isIphoneXR()){
//            clickByLocationByPercent(70,51);
//        }else{
//            clickByLocationByPercent(70,55);
//        }
        //clickByPicture2("src/main/resources/images/channel/card/CardInfo.png",50,50);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Total balance')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用余额')]"),
    })
    public MobileElement totalBalance;

    public void clickCardPicturGoToCardPage(){
        //clickByPicture2("src/main/resources/images/channel/card/CardPageIco.png",50,50);
        //clickByLocationByPercent(50,30);
        int height = driver.manage().window().getSize().getHeight();
        int y = commonPage.totalBalance.getLocation().getY();
        int yPercent = (y * 100) / height;
        clickByLocationByPercent(70, yPercent+7);
    }

    public void clickTopRightCloseBtn(){
        clickByLocationByPercent(95,6);
        //clickByPicture2("src/main/resources/images/channel/card/CloseBtn.png",50,50);
    }

    public void clickSetLimit(){
        int y = cardInfoElement.getLocation().getY();
        int eleHeight = cardInfoElement.getSize().getHeight();
        int height = driver.manage().window().getSize().height;
        double yPercent = (y+eleHeight)*100/height + 5 ; // under the element 5%
        // card active
        if(isIphoneSE()){
            clickByPicture("src/main/resources/images/channel/card/setCardLimit.png",50,60);
        }else if(isIphone7Plus()){
            //clickByLocationByPercent(50, 65); // no apple wallet
            clickByPicture("src/main/resources/images/channel/card/setCardLimit.png",50,60);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,63.3);
        }else if(isIphone13()){
            //clickByLocationByPercent2(50,56.8);
            clickByLocationByPercent2(50, yPercent);
        }else{
            clickByPicture("src/main/resources/images/channel/card/setCardLimit.png",50,60);
        }

    }

    @SneakyThrows
    public void clickTopLeftBtnToExit(){
        Thread.sleep(1000);
        clickByLocationByPercent(6, 6);
        //clickByPicture2("src/main/resources/images/channel/BackBtn.png",50,50);
    }

    @SneakyThrows
    public void clickNotifacationBtn(){
        //clickByPicture2("src/main/resources/images/channel/card/NotificationBtn.png", 50, 50);
        clickByLocationByPercent(80,7);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void getHomePage(String screenShotName,String screenShotName1){
        Thread.sleep(3000);
        waitUntilElementVisible(currentlyTransactions);
        takeScreenshot(screenShotName);
        scrollUp();
        Thread.sleep(1000 * 10);
        waitUntilElementVisible(transactionDetails);
        transactionText = transactionDetails.getText();
        takeScreenshot(screenShotName1);
        scrollDown();
    }

    @SneakyThrows
    public void checkGpsTransaction(String screenShotName,String screenShotName1){
        if(!GlobalVar.IS_CHECK_TRANSACTION) return;
        int count=0;
        while(true) {
            System.out.println("count: "+count);
            if(count > 10) {
                assertThat(false);
                break;
            }
            scrollDown();
            scrollDown();
            Thread.sleep(1000 * 5);
            scrollUp();
            Thread.sleep(1000);
            waitUntilElementVisible(transactionDetails);
            String text = transactionDetails.getText();
            text = text.substring(text.indexOf("(HKD)")+5).trim();
            String lastTransactionId = text.split(" ")[0].trim();
            if (transactionText.contains(lastTransactionId)){
                count += 1;
                scrollUp();
                Thread.sleep(1000 * 5);
            } else{
                scrollUp();
                takeScreenshot(screenShotName);
                break;
            }
        }
        scrollDown();
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void goToDebitCard(String screenShotName,String screenShotName1){
        takeScreenshot(screenShotName);
        clickCardPicturGoToCardPage();
        if(isShow(resetPasswordPage,10)){
            clickTopRightCloseBtn();
        }
        Thread.sleep(1000*10);
        waitUntilElementVisible(WelabDebitCardPageTitle);
        waitUntilElementClickable(cardInfomation);
        scrollUp();
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void clickCardInfoAndinputMSK(String screenShotName,String screenShotName1){
        Thread.sleep(3000);
        scrollDown();
        Thread.sleep(1000);
        clickCardInfomation();
        Thread.sleep(5000);
        waitUntilElementVisible(MSKpage);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        commonPage.inputMSK();
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void getCardInfomation(String screenShotName){
        String platform =  System.getProperty("mobile");
        String user= GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
        String pwd=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        Thread.sleep(1000*10);
        waitUntilElementVisible(WelabDebitCardPageTitle);
        Thread.sleep(1000*10);
        takeScreenshot(screenShotName);
        String cardInfo = cardInfomationshow.getText();
        System.out.println("cardInfomationshow: "+cardInfomationshow.getText());
        String cardLastFourNumber =getCardLastFourNumber(cardInfo);
        GlobalVar.GLOBAL_VARIABLES.put("cardLastFourNumber",cardLastFourNumber);
        System.out.println("cardLastFourNumber: "+GlobalVar.GLOBAL_VARIABLES.get("cardLastFourNumber"));
        String token = getCardToken(user,pwd);
        GlobalVar.GLOBAL_VARIABLES.put("token",token);
        System.out.println("token: "+GlobalVar.GLOBAL_VARIABLES.get("token"));
        //System.out.println("cardInfomationshow: "+cardInfomationshow.getText());
        //LI NGON CHUNK 5479 7401 1984 5313 有效期限 05/26 CVV 098 添加到Apple钱包 Insights 隱藏資料 設定限額 重設卡密碼 報失卡 水平滚动条, 1页 WeLab Debit Card
        //LI NGON CHUNK 5479 7401 6183 8687 Expiry date 04/27 CVV 686 Your card is expected to arrive by 10 Jun 2022  Insights Hide details Daily card transaction limits Lost card 水平滚动条, 1页 Activate physical card WeLab Debit Card
    }
    public String getCardLastFourNumber(String data) {
        if (commonPage.isIOS()) {
            //LI NGON CHUNK **** **** **** 5662
            //LI NGON CHUNK 5479 7401 4423 5662 有效期 02/29 CVV 472
            ///I NGON CHUNK 5479 7401 8670 2413 有效期限 02/29 CVV 138
            if(data.contains("有效期")){
                data = data.substring(0, data.indexOf("有效期")).trim();
            }else {
            }
            /**
            if(data.contains("有效期")){
                data = data.substring(0, data.indexOf("有效期")).trim();
            }else if(data.contains("添加到Apple钱包")) {
                data = data.substring(0, data.indexOf("添加到Apple钱包")).trim();
            }else if(data.contains("****")) {
                data = data.substring(0, data.indexOf("財務概覽")).trim();
            }else{
                //data = data.substring(0, data.indexOf("Expiry date")).trim();
                data = data.substring(0, data.indexOf("添加到Apple钱包")).trim();
            }
             */
        } else {
        }
        String[] datas = data.split(" ");
        return datas[datas.length - 1].trim();
    }

    @SneakyThrows
    public void inputAmoutForSetLimit(String amount, String screenShotName){
//        clickElement(currentLimitAmount);
//        commonPage.inputDeleteByNumber(6);
//        commonPage.clickSoftKeyByNumberString(amount);
        clearAndSendKeys(currentLimitAmount, amount);
        inputEnter(currentLimitAmount);
        Thread.sleep(1000);
//        clickElement(noCardLimitAmount);
//        commonPage.inputDeleteByNumber(6);
//        commonPage.clickSoftKeyByNumberString(amount);
        clearAndSendKeys(noCardLimitAmount, amount);
        Thread.sleep(1000);
        waitUntilElementClickable(setLimitConfirm);
        takeScreenshot(screenShotName);
        clickElement(setLimitConfirm);
        waitUntilElementVisible(MSKpage);
        Thread.sleep(2000);
        commonPage.inputMSK();
        Thread.sleep(10000);
        waitUntilElementVisible(WelabDebitCardPageTitle);
        Thread.sleep(3000);
        clickTopLeftBtnToExit();
    }

    @SneakyThrows
    public void setLimit(String screenShotName){
        String amount="1000";
        clickSetLimit();
        waitUntilElementVisible(debitCardTopLimit);
        if(getLimitAmout().equals("1,000")){
            takeScreenshot(screenShotName);
            clickElement(setLimitPageCloseButton);
            clickTopLeftBtnToExit();
        }else{
            inputAmoutForSetLimit(amount, screenShotName);
        }
        Thread.sleep(2000);
        clickTopLeftBtnToExit();
    }


    public String getLimitAmout(){
        return currentLimitAmount.getText();
    }
    public String getNoCardLimitAmout(){
        return noCardLimitAmount.getText();
    }


    @SneakyThrows
    public void setLimitLarge(){
        String amount="50000";
        clickSetLimit();
        waitUntilElementVisible(debitCardTopLimit);
        if(getLimitAmout().contains("50,000")){
            clickElement(setLimitPageCloseButton);
            Thread.sleep(3000);
            clickTopLeftBtnToExit();
        }else {
            inputAmoutForSetLimit(amount, "Card_Limit_large.png");
        }
        Thread.sleep(2000);
        clickTopLeftBtnToExit();
    }

    @SneakyThrows
    public void checkNotification(String screenShotName){
        Thread.sleep(15000);
        clickNotifacationBtn();
        Thread.sleep(3000);
        if(!isShow(notifacationContent,10)){
            clickNotifacationBtn();
            Thread.sleep(3000);
        }
        waitUntilElementVisible(notifacationContent);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        clickTopLeftBtnToExit();
    }

    @SneakyThrows
    public void iClickDebitCard(String screenShotName){
        Thread.sleep(2000);
        waitUntilElementVisible(debitCard);
        if(isSamsungA3460()){
//            clickCardPicturGoToCardPage();
            clickElement(debitCard);
        }else {
            clickElement(debitCard);
        }
        Thread.sleep(2000);
        if (verifyElementExist(resetPINCloseBtn)){
            clickElement(resetPINCloseBtn);
        }
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickCardDetails(){
        Thread.sleep(3000);
        waitUntilElementVisible(welabDebitCard);
        //String picPath="src/main/java/com/welab/automation/projects/channel/images/cardPic.png";
        //clickMobileElementByPicture(picPath);
        clickElement(cardDetails);
    }


    @SneakyThrows
    public void clickDailyCardTransactionLimits(String screenShotName){
        Thread.sleep(5000);
        if (verifyElementExist(debitCardTransactionSetting)){
            clickElement(debitCardTransactionSetting);
        }else{
            clickElement(transactionLimits);
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    public Double stringToDouble(String v){
        v = v.replaceAll(",","").trim();
        return Double.parseDouble(v);
    }

    @SneakyThrows
    public boolean changeDailyTransactionAndWithdrawalLimit(String TransactionLimit, String WithdrawalLimit, String screenShotName4, String screenShotName5){
        if (debitCardTransactionLimit.getText().equals("1,000")) {
            TransactionLimit = "999";
        }
        if (localATMWithdrawalLimit.getText().equals("1,000")) {
            WithdrawalLimit = "999";
        }
        clearAndSendKeys(debitCardTransactionLimit, TransactionLimit);
        inputEnter(debitCardTransactionLimit);
        scrollUpByPercent (33,5);
        scrollUpByPercent (33,5);

        String debitCardCardNotPresentTransactionLimitValue = debitCardCardNotPresentTransactionLimit.getText();
        if(stringToDouble(debitCardCardNotPresentTransactionLimitValue)>999){
            clearAndSendKeys(debitCardCardNotPresentTransactionLimit, "999");
            inputEnter(debitCardCardNotPresentTransactionLimit);
        }

        clearAndSendKeys(localATMWithdrawalLimit,WithdrawalLimit);
        inputEnter(localATMWithdrawalLimit);

        getTransactionLimitText = debitCardTransactionLimit.getText();
        getWithdrawalLimitText = localATMWithdrawalLimit.getText();

        takeScreenshot(screenShotName4);
        clickElement(confirmBtn);
        Thread.sleep(2000);
        commonPage.waiteSendMsk();
        Thread.sleep(5000);

        waitUntilElementVisible(welabDebitCard);
        Thread.sleep(20 * 1000);
        if (verifyElementExist(debitCardTransactionSetting)){
            clickElement(debitCardTransactionSetting);
        }else{
            clickElement(transactionLimits);
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName5);
        return debitCardTransactionLimit.getText().equals(getTransactionLimitText) && localATMWithdrawalLimit.getText().equals(getWithdrawalLimitText);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"想要實體Debit Card\"])[2]"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"想要实体借记卡\"])[2]")
    })
    private MobileElement wantDebitCardItem;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"下一步\"]")
    private MobileElement lostCardNextButton;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"你已經成功報失卡。 你嘅虛擬卡已經可以用喇。\""),
            @iOSXCUITBy(xpath = "//*[contains(@value ,'你已經成功報失卡')]"),
    })
    private MobileElement lostSuccessPage;


    public void selectWantDebitCard() {
        int yPercent = getYPercentByElement(wantDebitCardItem);
        logger.info("ypercent: "+yPercent);
        clickByLocationByPercent(8, yPercent);
        //clickByLocationByPercent(8, 46);
    }

    @SneakyThrows
    public void reportCardLostForMCV(String screenShotName1, String screenShotName2) {
        scrollUp();
        Thread.sleep(1000);
        clickReportCardLostItemForMCV();
        Thread.sleep(2000);
        waitUntilElementVisible(lostCardNextButton);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickElement(lostCardNextButton);
        waitUntilElementVisible(lostSuccessPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
    }

    @SneakyThrows
    public void reportCardLost(String screenShotName1, String screenShotName2) {
        scrollUp();
        Thread.sleep(1000);
        clickReportCardLostItem();
        Thread.sleep(2000);
        waitUntilElementVisible(wantDebitCardItem);
        Thread.sleep(1000);
        selectWantDebitCard();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        //clickElement(confirmReportCardLostBtn);
        clickElement(lostCardNextButton);
        Thread.sleep(10000);
        //waitUntilElementVisible(cardDeliverMessage);
        waitUntilElementVisible(doneBtn);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickElement(doneBtn);
        Thread.sleep(1000);
    }

    public void clickReportCardLostItem(){
        if(isIphoneSE()){
            //clickByLocationByPercent(20,89);// no apple wallet
            clickByPicture("src/main/resources/images/channel/card/ReportLostCard.png",50,60);
        }else if(isIphone7Plus()){
            logger.info("Click location for iphone 7 plus!!!!!!");
            clickByLocationByPercent(20,91);// no apple wallet
            //clickByPicture2("src/main/resources/images/channel/card/reportLost2.png",50,20);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,88.5);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,82.7);
        }else{
            clickByPicture("src/main/resources/images/channel/card/ReportLostCard.png",50,60);
        }
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Debit Card 交易設定 凍結卡 報失卡 舉報可疑交易\"]"),
    })
    private MobileElement cardSettingForMCV;

    public void clickReportCardLostItemForMCV(){
        int height = cardSettingForMCV.getLocation().getY();
        int eleHeight = cardSettingForMCV.getSize().getHeight();
        int  y  = height + (eleHeight * 64) / 100;
        clickByLocation(200, y);
        //clickByPicture("src/main/resources/images/channel/card/ReportLostCard.png",50,60);
    }

    public void clickToInputQrCode(){
        clickByLocationByPercent(50,91);
    }

    @SneakyThrows
    public void debitCardActivation(String screenShotName1, String screenShotName2,
                           String screenShotName3, String screenShotName4,String screenShotName5,
                           String screenShotName6,String screenShotName7){
        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        Thread.sleep(3000);
        waitUntilElementVisible(activeCardBtn);
        clickElement(activeCardBtn);
        Thread.sleep(3000);
        //waitUntilElementVisible(activeCardPageMessage);
        takeScreenshot(screenShotName1);
        clickElement(activeBtn);
        Thread.sleep(3000);
        if(isShow(activeCardByQRCodeMessage,10)){
            takeScreenshot(screenShotName2);
            clickElement(activeCardByQRCodeMessage);
        }else{
            takeScreenshot(screenShotName2);
            clickToInputQrCode();
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName3);
        String token=GlobalVar.GLOBAL_VARIABLES.get("token");
        commonPage.inputMSKByString(token);
        Thread.sleep(5000);
        waitUntilElementVisible(activeCardSuccessMessage);
        takeScreenshot(screenShotName4);
        clickElement(iSeeBtn);
        Thread.sleep(5000);
        waitUntilElementVisible(setCardPasswordPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(2000);
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(2000);
        waitUntilElementVisible(MSKpage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName6);
        commonPage.inputMSKByString(mskPassword);
        Thread.sleep(3000);
        if(isShow(resetPasswordPage,10)){
            clickTopRightCloseBtn();
        }
        Thread.sleep(5000);
        if(isShow(resetPasswordPage,5)){
            clickTopRightCloseBtn();
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName7);
        //waitUntilElementVisible(activeCardFinishMessage);
        Thread.sleep(3000);
        takeScreenshot(screenShotName7);
        Thread.sleep(1000);
    }

    public String getCardToken(String username,String password) throws Exception{
        String cardLastFourNumber = GlobalVar.GLOBAL_VARIABLES.get("cardLastFourNumber");
        coreUiAndApiTest coreUiAndApiTest = new coreUiAndApiTest();
        List<String> valueExpression = coreUiAndApiTest.gridFinder(username, password,cardLastFourNumber);
        System.out.println("valueExpression.get(0):"+valueExpression.get(0));
        return valueExpression.get(0);
    }

    @SneakyThrows
    public void resetCardPinFlow(String screenShotName1, String screenShotName2) {
        clickResetCardPinItem();
        Thread.sleep(2000);
        waitUntilElementVisible(resetPasswordPage);
        Thread.sleep(1000);
        commonPage.inputMSKByString("123456");
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        commonPage.inputMSKByString("123456");
        Thread.sleep(3000);
        waitUntilElementVisible(MSKpage);
        commonPage.inputMSKByString("123456");
        waitUntilElementVisible(WelabDebitCardPageTitle);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        Thread.sleep(1000);
    }

    public void clickResetCardPinItem(){

        if(isIphoneSE()){
            //clickByLocationByPercent(25, 69); //iphone SE scroll up
            clickByPicture("src/main/resources/images/channel/card/resetCard.png",50,50);
        }else if(isIphone7Plus()){
            logger.info("Click location for iphone 7 plus!!!!!!");
            clickByLocationByPercent(25, 75);//iphone7 plus // no apple wallet
            //clickByPicture2("src/main/resources/images/channel/card/resetCardPassword2.png",50,10);
        }else if(isIphoneXR()){
            clickByLocationByPercent(25,71);
        }else if(isIphone13()){
            clickByLocationByPercent(25,65.4);
        }else{
            clickByPicture("src/main/resources/images/channel/card/resetCard.png",50,50);
        }
    }

    public void getDebitCardNumber() {
        waitUntilElementVisible(cardNumber);
        cardNumbers = cardNumber.getText();
    }

    @SneakyThrows
    public boolean clickLostCard(String screenShotName){
        waitUntilElementVisible(lostCard);
        clickElement(lostCard);
        waitUntilElementClickable(yesLostCard);
        clickElement(yesLostCard);
        Thread.sleep(3000);
        clickElement(nextBtn);
        Thread.sleep(3000);
//        if (verifyElementExist(resetPINCloseBtn)){
//            clickElement(resetPINCloseBtn);
//        }
        takeScreenshot(screenShotName);
        return verifyElementExist(lostCardSuccess);
    }

    public void clickConfirmBtn(){
        waitUntilElementVisible(doneBtn);
        clickElement(doneBtn);
    }

    @SneakyThrows
    public void verifyReportCardLostSuccess(String screenShotName){
        Thread.sleep(5000);
        waitUntilElementVisible(welabDebitCard);
        Thread.sleep(8000);
        takeScreenshot(screenShotName);
//        String valueExpression = channelAPI.channelCard("48176");
//        AssertionsForClassTypes.assertThat(valueExpression.equals("success")).isTrue();
//        System.out.println(valueExpression);
//        cardNumber.getText().equals(cardNumbers);
    }
    @SneakyThrows
    public void setLimitLargeAndroid() {
        Thread.sleep(2000);
        if (verifyElementExist(debitCardTransactionSetting)){
            clickElement(debitCardTransactionSetting);
        }else{
            clickElement(transactionLimits);
        }

        if (!debitCardTransactionLimit.getText().contains("50,000")) {
            clearAndSendKeys(debitCardTransactionLimit, "50000");
            Thread.sleep(1000);
            inputEnter(debitCardTransactionLimit);
            Thread.sleep(2000);
        }

        if (!debitCardCardNotPresentTransactionLimit.getText().contains("50,000")) {
            clearAndSendKeys(debitCardCardNotPresentTransactionLimit, "50000");
            Thread.sleep(1000);
            inputEnter(debitCardCardNotPresentTransactionLimit);
            Thread.sleep(2000);
            waitUntilElementClickable(setLimitConfirm);
            clickElement(setLimitConfirm);
            Thread.sleep(2000);
            waitUntilElementVisible(MSKpage);
            Thread.sleep(2000);
            commonPage.waiteSendMsk();
        }else{
            clickElement(setLimitPageCloseButton);
        }


        Thread.sleep(10000);
        waitUntilElementVisible(WelabDebitCardPageTitle);
        Thread.sleep(5000);

        clickElement(WelabDebitCardPageTitle);

    }

    @SneakyThrows
    public boolean verifyCardDetails(String screenShotName){
        Thread.sleep(8000);
        takeScreenshot(screenShotName);
        Thread.sleep(3000);
        return verifyElementExist(expiryDate) && verifyElementExist(cvv);
    }

    @SneakyThrows
    public void activateDebitCard( String screenShotName,String screenShotName1, String screenShotName2,
                                  String screenShotName3, String screenShotName4,String screenShotName5
                                 ) {
//        String mskPassword =  GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
        Thread.sleep(3000);
        waitUntilElementVisible(activeCardBtn);
        clickElement(activeCardBtn);
        Thread.sleep(1000);
        waitUntilElementVisible(activeCardPageMessage);
        takeScreenshot(screenShotName);
        clickElement(activeBtn);
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);

        if (isElementDisplayed(allowButton, allow, 2)) allowButton.click();
        if (isElementDisplayed(allowButton, allowCH, 2)) allowButton.click();
        if (isElementDisplayed(allowButton, alwaysAllow, 2)) allowButton.click();

        Thread.sleep(3000);
        if (isShow(activeCardByQRCodeMessage, 10)) {
            takeScreenshot(screenShotName3);
            clickElement(activeCardByQRCodeMessage);
        } else {
            takeScreenshot(screenShotName2);
            clickToInputQrCode();
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName3);
        sendActivationCode();
        Thread.sleep(10000);


        if(isShow(iSeeBtn,2)){
            clickElement(iSeeBtn);
        }

        Thread.sleep(2000);
        if (verifyElementExist(resetPINCloseBtn)){
            clickElement(resetPINCloseBtn);
        }
        Thread.sleep(2000);
        if(isShow(setPINCloseBtn)) {
            clickElement(setPINCloseBtn);
        }

        Thread.sleep(2000);
        if (verifyElementExist(backBtn)){
            clickElement(backBtn);
        }

        Thread.sleep(5000);

        takeScreenshot(screenShotName4);
        assertThat(!verifyElementExist(activeCardBtn)).isTrue();
        takeScreenshot(screenShotName5);
    }

    @SneakyThrows
    public boolean clickResetCardPIN(String screenShotName,String screenShotName1) {
        Thread.sleep(5000);
        waitUntilElementClickable(resetPINBtn);
        clickElement(resetPINBtn);
        Thread.sleep(2000);
        sendMsk();
        Thread.sleep(2000);
        sendMsk();
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        commonPage.waiteSendMsk();
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        return verifyElementExist(welabDebitCard);
    }


    @SneakyThrows
    public void  InsightsPageInformationDisplayIOS(String screenShotName,String screenShotName1) {
        Thread.sleep(5000);
        clickInsights();
        waitUntilElementVisible(insightsPage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        Thread.sleep(1000);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'你嘅每月消費')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'你的每月消费')]"),
    })
    private MobileElement insightsPage;

    public void clickInsights(){
        int y = cardInfoElement.getCenter().getY();
        int height = driver.manage().window().getSize().height;
        double yPercent = y*100/height;
        clickByLocationByPercent(30, yPercent);
//        if(isIphoneSE()){
//            clickByPicture("src/main/resources/images/channel/card/Insights.png",50,60);
//        }else if(isIphone7Plus()){
//            clickByLocationByPercent(30, 50);//iphone7 plus // no apple wallet
//        }else if(isIphoneXR()){
//            clickByLocationByPercent(30,50);
//        }else{
//            clickByPicture("src/main/resources/images/channel/card/Insights.png",50,60);
//        }
    }

    @SneakyThrows
    public void verifyInsightsPageDisplay(String screenShotName, String screenShotName1) {
        Thread.sleep(5000);
        clickElement(insightsButton);
        Thread.sleep(10000);
        waitUntilElementClickable(yourMonthlySpending);
        takeScreenshot(screenShotName);
        scrollUp();
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void clickAndVerifyLostCard(String screenShotName, String screenShotName1) {
        waitUntilElementClickable(lostCard);
        Thread.sleep(2000);
        clickElement(lostCard);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(nextBtn);
        waitUntilElementVisible(lostCardSuccess);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public boolean changeCardLimitForMCV(String cardLimit, String screenShotName, String screenShotName1) {
        String debitCardTransactionLimitValue = debitCardCardNotPresentTransactionLimit.getText();
        if (stringToDouble(debitCardTransactionLimitValue)>999) {
            clearAndSendKeys(debitCardTransactionLimit, "999");
        }else{
            clearAndSendKeys(debitCardTransactionLimit, cardLimit);
        }
        inputEnter(debitCardTransactionLimit);
        String debitCardCardNotPresentTransactionLimitValue = debitCardCardNotPresentTransactionLimit.getText();
        if(stringToDouble(debitCardCardNotPresentTransactionLimitValue)>999){
            clearAndSendKeys(debitCardCardNotPresentTransactionLimit, "999");
        }else{
            clearAndSendKeys(debitCardCardNotPresentTransactionLimit, cardLimit);
        }
        inputEnter(debitCardCardNotPresentTransactionLimit);
        getTransactionLimitText = debitCardTransactionLimit.getText();
        takeScreenshot(screenShotName);
        clickElement(confirmBtn);
        Thread.sleep(2000);
        commonPage.waiteSendMsk();
        Thread.sleep(5000);
        waitUntilElementVisible(welabDebitCard);
        Thread.sleep(20 * 1000);
        if (verifyElementExist(debitCardTransactionSetting)){
            clickElement(debitCardTransactionSetting);
        }else{
            clickElement(transactionLimits);
        }
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        return debitCardTransactionLimit.getText().equals(getTransactionLimitText);
    }

}
