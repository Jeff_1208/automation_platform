Feature: Open account of wealth Application CH

  Background: Login_CH
    Given Open WeLab App


  Scenario: Verify the account opening process CH
    And Click account opening button on the home page
      | screenShotName | Wealth_Login_0001_Android_01 |
    Then See account opening requirements and click it
      | screenShotName | Wealth_Login_0001_Android_02 |
    And Enter an incorrectPhone mobile phone number
      | incorrectPhone | 888888888                    |
      | screenShotName | Wealth_Login_0001_Android_03 |
    And Enter an incorrectEmail email address
      | incorrectEmail | xxxxx@xx                     |
      | screenShotName | Wealth_Login_0001_Android_04 |
    Then Checking terms of use of app
      | screenShotName  | Wealth_Login_0001_Android_05 |
      | screenShotName1 | Wealth_Login_0001_Android_06 |
      | screenShotName2 | Wealth_Login_0001_Android_07 |
      | screenShotName3 | Wealth_Login_0001_Android_08 |
      | screenShotName4 | Wealth_Login_0001_Android_09 |
      | screenShotName5 | Wealth_Login_0001_Android_10 |
      | screenShotName6 | Wealth_Login_0001_Android_11 |
      | screenShotName7 | Wealth_Login_0001_Android_12 |
      | screenShotName8 | Wealth_Login_0001_Android_13 |



  Scenario: Account is dormant CH
    And Enable skip Root
    Given Check upgrade page appears
    And Login with user and password
      | user     | swmcust00547  |
      | password | Aa123321      |
    And Verify return dormant account text
      | screenShotName | Wealth_Login_0002_Android_01 |


  Scenario: Correct username - incorrect password or incorrect username - correct password  CH
    And Enable skip Root
    Given Check upgrade page appears
    And Login with user and password
      | user     | swmcust      |
      | password | Aa123321     |
    Then Login failed and give error message
    And Login with user and password
      | user     | swmcust00107 |
      | password | Aa123456     |
    Then Login failed and give error message
    And Login with user and password
      | user     | swmcust00107 |
      | password | Aa123321     |
    And Verify goWealth and Transfer whether to show
      | screenShotName | Wealth_Login_0003_Android_01 |
      | screenShotName1 | Wealth_Login_0003_Android_02 |