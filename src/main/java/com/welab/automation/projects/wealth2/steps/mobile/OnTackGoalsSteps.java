package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.iao.OnTackGoalsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OnTackGoalsSteps {

    OnTackGoalsPage onTackGoalsPage;

    public OnTackGoalsSteps(){
        onTackGoalsPage = new OnTackGoalsPage();
    }

    public void editOnTackGoalSteps(){
        onTackGoalsPage = new OnTackGoalsPage();
    }

    @And("^I click the order of goal with the status of on track$")
    public void clickOnTrackGoal(Map<String, String> data){
        onTackGoalsPage.clickOnTrackGoal(data.get("screenShotName01"), data.get("screenShotName02"));
    }

    @And("I click view updated recommendation")
    public void clickViewUpdateRecommendationBtn(Map<String, String> data) {
        onTackGoalsPage.clickViewUpdateRecommendationBtn(data.get("screenShotName"),data.get("screenShotName"));
    }

    @And("^I click View New Portfolio button$")
    public void clickViewNewPortfolioBtn(Map<String, String> data){
        onTackGoalsPage.clickViewNewPortfolioBtn(data.get("screenShotName05"));
    }

    @And("^I click view updated recommendation and click next button$")
    public void clickViewUpdateRecommendationBtnAndClickNextBtn(Map<String, String> data) {
        onTackGoalsPage.clickViewUpdateRecommendationBtnAndClickNextBtn(data.get("screenShotName"),data.get("screenShotName01"),data.get("screenShotName02"));
    }

    @And("^I click view updated recommendation and verify have two documents in review and confirm order page$")
    public void verifyHaveTwoDocuments(Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 5; i < data.values().size()+5 ; i++) {
            screenShotNameList.add(data.get("screenShotName" + i));
        }
        onTackGoalsPage.verifyHaveTwoDocuments(screenShotNameList);
    }

    @And("^I click view updated recommendation button and slide trading$")
    public void orderSuccessfully(Map<String, String> data) {
        List<String> screenShotNameList = new ArrayList<>();
        for (int i = 5; i < data.values().size()+5 ; i++) {
            screenShotNameList.add(data.get("screenShotName" + i));
        }
        onTackGoalsPage.orderSuccessfully(screenShotNameList);
    }

    @Then("I click View Updated Recommendation")
    public void iClickViewUpdatedRecommendation(Map<String, String> data) {
        onTackGoalsPage.clickViewUpdatedRecommendation(data.get("screenShotName"));
    }
}
