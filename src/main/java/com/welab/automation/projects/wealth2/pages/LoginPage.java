package com.welab.automation.projects.wealth2.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class LoginPage extends AppiumBasePage {

    private static final String[] login_text = {"Login","登入"};
    private static final String cancelText = "Cancel";
    private int customWait = 60;
    private int cancelMessageWait = 5;
    private String pageName = "Login Page";
    CommonPage commonPage;

    public LoginPage() {
        super.pageName = pageName;
        commonPage = new CommonPage();
        if (System.getProperty("mobile").equalsIgnoreCase("cloudios"))
            ((AppiumDriver<MobileElement>) BaseDriver.getMobileDriver()).activateApp("welab.bank.sit");
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"不，谢谢\"]")
    private MobileElement noThanksBtn;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
    })
    protected MobileElement loginTxt;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='我明白啦']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Got it''"),
            @iOSXCUITBy(iOSNsPredicate = "name == '我明白啦'"),
    })
    private MobileElement gotIt;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Username']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='用戶名稱']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
    })
    private MobileElement userNameTxt;

    @AndroidFindBy(accessibility = "username-input")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
    })
    private MobileElement userNameInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Password']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='密碼']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
    })
    private MobileElement passwordTxt;

    @AndroidFindBy(accessibility = "password-input")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
    })
    private MobileElement passwordInput;

    @AndroidFindBy(xpath = "//*[@text='Welcome back']")
    private MobileElement welcomeBack;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
    })
    private MobileElement loginButton;


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
    private MobileElement contactWarningCancel;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
    private MobileElement notification;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
    private MobileElement next;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start Testing'")
    private MobileElement start;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Ask App not to Track'")
    private MobileElement noTracking;

    //  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]")
//  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Total balance')]")
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, '總結餘')]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Total balance')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
    })
    private MobileElement totalBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'WeLab')]"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name, 'WeLab')]"),
    })
    private MobileElement openAppEle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'month GoSave Time Deposit')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Other transaction')]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'4個月GoSave定期存款')]"),
    })
    private MobileElement transaction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> textView;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement returnButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Recent transactions']"),
            @AndroidBy(xpath = "//*[@text='最近交易']"),
    })
    private MobileElement transactions;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//*[@text='查看更多']"),
    })
    private MobileElement seeAll;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private List<MobileElement> getList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"),
    })
    private List<MobileElement> textElement;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[3]"),
    })
    private MobileElement reference;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> referenceNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc='login-screen']/android.view.ViewGroup[3]/android.widget.TextView[2]"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"開戶\"]/../XCUIElementTypeStaticText"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Open account\"]/../XCUIElementTypeStaticText[1]"),
    })
    private MobileElement appVersion;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 1 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 3 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]/../XCUIElementTypeOther[2]"),
    })
    private MobileElement transactionDetails;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"I am rebound\"]"),
    })
    private MobileElement helloAlert;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"OK\"]"),
    })
    private MobileElement helloOKBtn;

    @AndroidFindBy(accessibility = "GoWealth, tab, 5 of 5")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoWealth, tab, 5 of 5'")
    private MobileElement wealthTab;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"username-input\"]")
    private MobileElement reEnterUser;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"password-input\"]")
    private MobileElement reEnterPwd;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"welcome-screen\"]/android.view.ViewGroup[2]/android.widget.TextView"),
    })
    private MobileElement welcomeLogin;


    public void clickContactWarningLogin() throws InterruptedException {
        if (isElementDisplayed(contactWarningCancel, cancelText, cancelMessageWait)) {
            waitUntilElementClickable(contactWarningCancel);
            contactWarningCancel.click();
        }
        if (isElementVisible(wealthTab, customWait)) {
            Thread.sleep(1500);
            if (isElementDisplayed(contactWarningCancel, cancelText, cancelMessageWait)) {
                waitUntilElementClickable(contactWarningCancel);
                contactWarningCancel.click();
            }
        }
    }

    public String getLoggedInPageText() {
        // if (isElementDisplayed(debitCard).isDisplayed()){
        if (isElementDisplayed(totalBalance) != null) {
            return totalBalance.getText();
        }
        ;
        return "Can't find total balance element.";
    }


    public void loginWithCredential(String user, String password) {
        logger.info("Login with credential: " + user + ", " + password);
        clickElement(reEnterUser);
        clearAndSendKeys(reEnterUser, user);
        if (System.getProperty("mobile").contains("android")) hideKeyboard();
        clickElement(reEnterPwd);
        clearAndSendKeys(reEnterPwd, password);
        if (System.getProperty("mobile").contains("android")) hideKeyboard();
        clickElement(loginButton);
    }

    public void loginWithErrorPwd(String user, String password) {
        logger.info("Login with error password: " + user + ", " + password);
        clickElement(reEnterUser);
        clearAndSendKeys(reEnterUser, user);
        if (System.getProperty("mobile").contains("android")) hideKeyboard();
        clickElement(reEnterPwd);
        clearAndSendKeys(reEnterPwd, password);
        if (System.getProperty("mobile").contains("android")) hideKeyboard();
        clickElement(loginButton);
    }

    @SneakyThrows
    public String getLoginIconText() {
        if (System.getProperty("mobile").contains("ios")) {
            Thread.sleep(1000 * 5);
        }
        waitUntilElementVisible(loginTxt);
        return loginTxt.getText();
    }

    public String getLoginButtonText() {
        waitUntilElementVisible(loginButton);
        return totalBalance.getText();
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"以后\"]"),
    })
    private MobileElement laterButton;

    public void checkLaterButton(){
        if(isShow(laterButton,30)){
            clickElement(laterButton);
        }
    }
    public void loginWithUserAndPasswordFromProperties() {
        String platform =  System.getProperty("mobile");
        String user= GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        loginWithUserAndPasswordByParams(user, password);
    }

    public void loginWithUserAndPasswordByParams(String user, String password) {
        if(!isShow(userNameTxt,2)){
            clickElement(loginTxt);
        }
        storeAppVersion();
        clickElement(userNameTxt);
        clearAndSendKeys(userNameInput, user);
        hideAndroidKeyboard();
        clickElement(passwordTxt);
        clearAndSendKeys(passwordInput, password);
        inputEnter(passwordInput);
        hideAndroidKeyboard();
        clickElement(loginButton);
        if(isIOS()){
            checkLaterButton();
        }
    }

    public void hideAndroidKeyboard(){
        try {
            if (System.getProperty("mobile").contains("android")) hideKeyboard();
        }catch (Exception e){
        }
    }
    @SneakyThrows
    public void waitAppOpen() {
        Thread.sleep(1000 * 5);
        if (commonPage.isIOS()) {
            if (isShow(noThanksBtn, 3)) {
                clickElement(noThanksBtn);
            }
            commonPage.checkUpgrade();
        }
        if (commonPage.isAndroid()) {
            commonPage.checkLogin();
            commonPage.checkUpgrade();
        }
    }

    public void storeAppVersion() {
        StringBuffer stringBuffer = new StringBuffer();
        waitUntilElementVisible(appVersion);
        GlobalVar.GLOBAL_VARIABLES.put("appVersion", appVersion.getText());
    }

    public void loginWithIAOAccount(String sheetName) throws IOException {
        String fileName = "src/main/resources/app/testData/IAOAccount.xlsx";
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(new FileInputStream(fileName));
        XSSFSheet sheet = xssfWorkbook.getSheet(sheetName);
        int num = sheet.getLastRowNum();
        String IAOAccount = sheet.getRow(num).getCell(0).toString();
        String  IAOPassword = sheet.getRow(num).getCell(1).toString();
        loginWithUserAndPasswordByParams(IAOAccount,IAOPassword);
        sheet.removeRow(sheet.getRow(num));
        FileOutputStream out = new FileOutputStream(fileName);
        out.flush();
        xssfWorkbook.write(out);
        out.close();
    }
}
