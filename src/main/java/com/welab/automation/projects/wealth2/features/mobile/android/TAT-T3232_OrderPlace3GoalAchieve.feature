Feature: GoWealth Regression

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    When Login with user and password from properties


  @VBWMIS-T483
  Scenario Outline: Reg_GoalBase_001 Build My Investing Habit place order successfully
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_001_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_001_Android_02 |
    And I click DIY My Plan
    When I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
      | screenShotName   | Reg_GoalBase_001_Android_03 |
      | screenShotName01 | Reg_GoalBase_001_Android_04 |
      | screenShotName02 | Reg_GoalBase_001_Android_05 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_001_Android_06 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_001_Android_07 |
      | screenShotName01 | Reg_GoalBase_001_Android_08 |
      | screenShotName02 | Reg_GoalBase_001_Android_09 |
      | screenShotName03 | Reg_GoalBase_001_Android_10 |
      | screenShotName04 | Reg_GoalBase_001_Android_11 |
    And I slide to confirm order
      | screenShotName   | Reg_GoalBase_001_Android_12 |
      | screenShotName01 | Reg_GoalBase_001_Android_13 |
    And I click Done button
      | screenShotName | Reg_GoalBase_001_Android_14 |
    Then I click back button by location
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_001_Android_15 |
      | screenShotName01 | Reg_GoalBase_001_Android_16 |
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 4000        | 3000         | 30    |


  @VBWMIS-T484
  Scenario Outline: Reg_GoalBase_002 Reach My Target place order successfully
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_002_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_002_Android_02 |
    And I click DIY My Plan
    And I click set my target and set <targetValue> and <time>
      | screenShotName | Reg_GoalBase_002_Android_03 |
    Then I click View recommendation and save my goal button
      | screenShotName | Reg_GoalBase_002_Android_04 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_002_Android_05 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_002_Android_06 |
      | screenShotName01 | Reg_GoalBase_002_Android_07 |
      | screenShotName02 | Reg_GoalBase_002_Android_08 |
      | screenShotName03 | Reg_GoalBase_002_Android_09 |
      | screenShotName04 | Reg_GoalBase_002_Android_10 |
    And I slide to confirm order
      | screenShotName   | Reg_GoalBase_002_Android_11 |
      | screenShotName01 | Reg_GoalBase_002_Android_12 |
    And I click Done button
      | screenShotName | Reg_GoalBase_002_Android_13 |
    Then I click back button by location
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_002_Android_14 |
      | screenShotName01 | Reg_GoalBase_002_Android_15 |
    Examples:
      | targetValue | time |
      | 4500        | 45   |

  @VBWMIS-T485
  Scenario Outline: Reg_GoalBase_003 Achieve Financial Freedom place order successfully
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_003_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_003_Android_02 |
    And I click DIY My Plan
    And I click only Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
      | screenShotName   | Reg_GoalBase_003_Android_03 |
      | screenShotName01 | Reg_GoalBase_003_Android_04 |
    Then I click View recommendation and save my goal button
      | screenShotName | Reg_GoalBase_003_Android_05 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_003_Android_06 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_003_Android_07 |
      | screenShotName01 | Reg_GoalBase_003_Android_08 |
      | screenShotName02 | Reg_GoalBase_003_Android_09 |
      | screenShotName03 | Reg_GoalBase_003_Android_10 |
      | screenShotName04 | Reg_GoalBase_003_Android_11 |
    And I slide to confirm order
      | screenShotName   | Reg_GoalBase_003_Android_12 |
      | screenShotName01 | Reg_GoalBase_003_Android_13 |
    And I click Done button
      | screenShotName | Reg_GoalBase_003_Android_14 |
    Then I click back button by location
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_003_Android_15 |
      | screenShotName01 | Reg_GoalBase_003_Android_16 |
    Examples:
      | age | monthlyExpenses | otherExpenses |
      | 40  | 100             | ,Budget,      |

# need OnTrack/Off Track Goal
#  @VBWMIS-T565
#  Scenario:Sell Partial GoalBase
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_004_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_004_Android_02 |
#    And I click view goal with the status of OnTrack or OffTrack
#      | screenShotName | Reg_Wealth_004_Android_03 |
#    And I click SellBtn and Proceed button
#      | screenShotName  | Reg_Wealth_004_Android_04 |
#      | screenShotName1 | Reg_Wealth_004_Android_05 |
#    And I edit Sell Value
#      | sellValue      | 100                       |
#      | screenShotName | Reg_Wealth_004_Android_06 |
#    And I Slide to confirm Sell And Click Done
#    Then I can see Sell button will be disabled and goal is Processing
#      | screenShotName | Reg_Wealth_004_Android_07 |
#    And I only can see Target wealth
#    Then I verify sell goal order status
#      | screenShotName | Reg_Wealth_004_Android_08 |

# need OnTrack/Off Track Goal
#  @VBWMIS-T568
#  Scenario:Sell All GoalBase
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_005_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_005_Android_02 |
#    And I click view goal with the status of OnTrack or OffTrack
#      | screenShotName | Reg_Wealth_005_Android_03 |
#    And I click SellBtn and Proceed button
#      | screenShotName  | Reg_Wealth_005_Android_04 |
#      | screenShotName1 | Reg_Wealth_005_Android_05 |
#    And I click Sell All Button and cancel button
#      | screenShotName | Reg_Wealth_005_Android_06 |
#    And I click Sell All Button
#      | screenShotName | Reg_Wealth_005_Android_07 |
#    And I Slide to confirm Sell And Click Done
#    Then I can see Sell button will be disabled and goal is Processing
#      | screenShotName | Reg_Wealth_005_Android_08 |
#    And I only can see Target wealth
#    Then I verify sell goal order status
#      | screenShotName | Reg_Wealth_005_Android_09 |

# need OnTrack/Off Track Goal
#  @VBWMIS-T569
#  Scenario:Switching Order
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_006_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_006_Android_02 |
#    And I click view goal with the status of OnTrack or OffTrack
#      | screenShotName | Reg_Wealth_006_Android_03 |
#    And I click Edit button
#      | screenShotName | Reg_Wealth_006_Android_04 |
#    Then I enter New monthly Investment
#      | newMonthly     | 340                       |
#      | screenShotName | Reg_Wealth_006_Android_05 |
#    Then I click View Updated Recommendation
#      | screenShotName | Reg_Wealth_006_Android_06 |
#    And I slide to confirm order
#      | screenShotName   | Reg_Wealth_006_Android_07 |
#      | screenShotName01 | Reg_Wealth_006_Android_08 |
#    And I click Done button
#      | screenShotName | Reg_Wealth_006_Android_09 |
#    Then I verify buy goal order status
#      | screenShotName | Reg_Wealth_006_Android_10 |


  @VBWMIS-T583
  Scenario Outline: Reg_GoalBase_007 Create a draft order
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_007_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_007_Android_02 |
    And I click DIY My Plan
    When I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
      | screenShotName   | Reg_GoalBase_007_Android_03 |
      | screenShotName01 | Reg_GoalBase_007_Android_04 |
      | screenShotName02 | Reg_GoalBase_007_Android_05 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_007_Android_06 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_007_Android_07 |
      | screenShotName01 | Reg_GoalBase_007_Android_08 |
      | screenShotName02 | Reg_GoalBase_007_Android_09 |
      | screenShotName03 | Reg_GoalBase_007_Android_10 |
      | screenShotName04 | Reg_GoalBase_007_Android_11 |
    And I quit the ordering process but go back
    Then I click X icon
    Then I click quit and save, then back to wealth page
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_007_Android_12 |
      | screenShotName01 | Reg_GoalBase_007_Android_13 |
    And I verify goal draft status
      | screenShotName | Reg_GoalBase_007_Android_14 |
    Then Delete draft goal
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 430        | 3000         | 32    |


  @VBWMIS-T584
  Scenario Outline: Reg_GoalBase_008 Delete a draft order
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_008_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_008_Android_02 |
    And I click DIY My Plan
    When I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
      | screenShotName   | Reg_GoalBase_008_Android_03 |
      | screenShotName01 | Reg_GoalBase_008_Android_04 |
      | screenShotName02 | Reg_GoalBase_008_Android_05 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_008_Android_06 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_008_Android_07 |
      | screenShotName01 | Reg_GoalBase_008_Android_08 |
      | screenShotName02 | Reg_GoalBase_008_Android_09 |
      | screenShotName03 | Reg_GoalBase_008_Android_10 |
      | screenShotName04 | Reg_GoalBase_008_Android_11 |
    Then I click X icon
    Then I click quit and save, then back to wealth page
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_008_Android_12 |
      | screenShotName01 | Reg_GoalBase_008_Android_13 |
    And I verify goal draft status
      | screenShotName | Reg_GoalBase_008_Android_14 |
    And I click Delete button
      | screenShotName   | Reg_GoalBase_008_Android_15 |
      | screenShotName01 | Reg_GoalBase_008_Android_16 |
    And I verify the goal with targetName have been delete
      | screenShotName | Reg_GoalBase_008_Android_17 |
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 450        | 3000         | 33    |


  @VBWMIS-T589
  Scenario Outline: Reg_GoalBase_009 Edit a draft order and continue place order successfully
    Then I goto wealthPage
      | screenShotName | Reg_GoalBase_009_Android_01 |
    And I click view goal goto view goal page
      | screenShotName | Reg_GoalBase_009_Android_02 |
    And I click DIY My Plan
    When I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
      | screenShotName   | Reg_GoalBase_009_Android_03 |
      | screenShotName01 | Reg_GoalBase_009_Android_04 |
      | screenShotName02 | Reg_GoalBase_009_Android_05 |
    And I click View Portfolio button on Portfolio Recommendation Page
      | screenShotName | Reg_GoalBase_009_Android_06 |
    And I click Next Button four times
      | screenShotName   | Reg_GoalBase_009_Android_07 |
      | screenShotName01 | Reg_GoalBase_009_Android_08 |
      | screenShotName02 | Reg_GoalBase_009_Android_09 |
      | screenShotName03 | Reg_GoalBase_009_Android_10 |
      | screenShotName04 | Reg_GoalBase_009_Android_11 |
    Then I click X icon
    Then I click quit and save, then back to wealth page
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_009_Android_12 |
      | screenShotName01 | Reg_GoalBase_009_Android_13 |
    And I verify goal draft status
      | screenShotName | Reg_GoalBase_009_Android_14 |
    And I click Edit button
      | screenShotName | Reg_GoalBase_009_Android_15 |
    Then I click create goal flow
      | screenShotName   | Reg_GoalBase_009_Android_16 |
      | screenShotName01 | Reg_GoalBase_009_Android_17 |
      | screenShotName02 | Reg_GoalBase_009_Android_18 |
      | screenShotName03 | Reg_GoalBase_009_Android_19 |
      | screenShotName04 | Reg_GoalBase_009_Android_20 |
      | screenShotName05 | Reg_GoalBase_009_Android_21 |
      | screenShotName06 | Reg_GoalBase_009_Android_22 |
      | screenShotName07 | Reg_GoalBase_009_Android_23 |
      | screenShotName08 | Reg_GoalBase_009_Android_24 |
      | screenShotName09 | Reg_GoalBase_009_Android_25 |
    Then I click back button by location
    And Verify goal success and click
      | screenShotName | Reg_GoalBase_009_Android_26 |
      | screenShotName01 | Reg_GoalBase_009_Android_27 |
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 470        | 3000         | 36    |