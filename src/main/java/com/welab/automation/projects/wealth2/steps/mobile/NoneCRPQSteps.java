package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.framework.utils.app.ExcelDataToDataTable;
import com.welab.automation.projects.wealth2.entities.NoneCRPQ;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.iao.NoneCRPQPage;
import io.appium.java_client.MobileElement;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class NoneCRPQSteps {

  private NoneCRPQ questionsEntity;
  NoneCRPQPage noneCRPQPage;
  CommonPage commonPage;

  public NoneCRPQSteps(){
    noneCRPQPage = new NoneCRPQPage();
    commonPage = new CommonPage() ;
  }

  private static final String got_it_text = "Got it!";
  private static final String mutual_fund_text = "Welcome to Wealth Management Services";
  private static final String mutual_fund_textCH = "歡迎使用理財服務";
  private static final String verify_personal_info_text = "Verify personal information";
  private static final String data_file = "data_template.xlsx";
  public static final String expectedImportantNotes =
      "Terms & Conditions, Important Notes, Risk Disclosures and other documents";
  public static final String expectedUpdatedNotes = "Updated and verified personal information";
  public static final String expectedUpdatedNotesCH = "更新及核對個人資料";

  public void NoneCRPQSteps(NoneCRPQ questions) {
    this.questionsEntity = questions;
  }

  @And("I click Got it on Welcome page")
  public void iClickGotItOnWelcomePage() throws InterruptedException {
    Thread.sleep(5000);
    noneCRPQPage.waitUntilElementVisible(noneCRPQPage.getGotItTab());
    noneCRPQPage.clickElement(noneCRPQPage.getGotItTab());
  }

  @And("I click GoalBased on Select GoalBased page")
  public void iClickGoalBasedOnSelectGoalBasedPage() {
    noneCRPQPage.clickElement(noneCRPQPage.getGoalBasePicture());
  }

  @And("I click Skip to open an account")
  public void iClickSkipToOpenAnAccount() {
    noneCRPQPage.clickElement(noneCRPQPage.getSkipLink());
  }

  @And("Click Next on Mutual Fund Services Welcome page")
  public void clickNextOoMutualFundServicesWelcomePage() {
    noneCRPQPage.scrollUp();
    noneCRPQPage.clickElement(noneCRPQPage.getMutualFundNextButton());
  }

  @And("I scroll left on GoalBased Description page")
  public void iScrollLeftOnGoalBasedDescriptionPage() {
    noneCRPQPage.swipeLeftToRight();
  }

  @And("I select nonCRPQ answer")
  public void iSelectNonCRPQAnswer(List<Map<String, String>> data) throws InterruptedException {
    for (Map<String, String> tmp : data) {
      noneCRPQPage.chooseNoneCRQP(tmp.get("question"), tmp.get("answer"));
    }
  }

  @And("I click confirm on nonCRPQ review page")
  public void iClickConfirmOnNonCRPQReviewPage() {
    noneCRPQPage.confirmReviewProfile();
  }

  // A_02_Welcome
  @And("I prepare none-CRPQ through wealth tab")
  public void prepareNoneCRPQThroughWealthTab() throws InterruptedException {
    commonPage.gotoWealth();
    MobileElement gotItTab = noneCRPQPage.getGotItTab();
  }

  @And("I begin none-CRPQ questionnaire process")
  public void beginNoneCRQPQuestionnaireProcess() throws InterruptedException {
    Boolean beforeStepsTag = noneCRPQPage.stepsBeforeNoneCRQP();
    assertThat(beforeStepsTag).isTrue();
    boolean flag = false;
    MobileElement mutualFundElement = noneCRPQPage.getMutualFundTitle();
    if(mutualFundElement.getText().equals(mutual_fund_text) || mutualFundElement.getText().equals(mutual_fund_textCH)){
      flag = true;
    }
    assertThat(flag).isTrue();
  }

  // C_01a_infoReview
  @And("I begin to check personal information")
  public void beginToCheckPersonalInformation() {
    noneCRPQPage.selectNoneCRPQOptions();
//    MobileElement personalInfo = noneCRPQPage.getVerifyInfo();
//    assertThat(personalInfo.getText()).isEqualTo(verify_personal_info_text);
  }

  @And("I confirm the personal information")
  public void confirmThePersonalInformation() {
    noneCRPQPage.clickPersonalInfoConfirm();
  }

  @And("I answer the none-CRPQ questions at {string}")
  public void answerTheNoneCRPQQuestions(String sheetName)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException,
          NoSuchFieldException {
    DataTable dataTable = new ExcelDataToDataTable(data_file, sheetName).getDatatable();
    List<List<String>> data = dataTable.asLists();
    Class questionsClass = Class.forName("com.welab.automation.projects.wealth.entities.NoneCRPQ");
    Object questionObj = questionsClass.newInstance();
    for (List<String> row : data) {
      String question = row.get(0);
      String answer = row.get(1);
      // use java reflection to set NoneCRPQ entity
      Field fieldName =
          questionsClass.getDeclaredField(question.replace(" ", "").replace("–", "").toLowerCase());
      fieldName.setAccessible(true);
      fieldName.set(questionObj, answer);
      String actualTitle = noneCRPQPage.getQuestionTitle(question);
      assertThat(actualTitle).isEqualTo(question);
      Boolean noneCRPQTag = noneCRPQPage.chooseNoneCRQP(question, answer);
      assertThat(noneCRPQTag).isTrue();
      if(answer.equals("Yes")){
        noneCRPQPage.enterName();
      }
//      if(question.equals("Declaration – Employee of Intermediary")){
//        noneCRPQPage.enterName();
//      }
    }


//    questionsEntity = (NoneCRPQ) questionObj;
  }

  @And("I review investment account profile")
  public void reviewInvestmentAccountProfile()
          throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, ClassNotFoundException, InstantiationException {
    Map<String, MobileElement> actualReviewMap = noneCRPQPage.NoneCRPQVerifyResultMap();
    Iterator entries = actualReviewMap.entrySet().iterator();
    while (entries.hasNext()) {
        Map.Entry entry = (Map.Entry) entries.next();
        String question = (String) entry.getKey();
        MobileElement answerEle = (MobileElement) entry.getValue();
        String answer = answerEle.getText();
        String reviewProfileResult = noneCRPQPage.getProfileAnswer(question, questionsEntity);
        assertThat(answer).isEqualTo(reviewProfileResult);
      }
  }

  @And("I confirm the Investment account profile")
  public void confirmTheInvestmentAccountProfile() throws InterruptedException {
    assertThat(noneCRPQPage.confirmReviewProfile()).isTrue();
    String updatedString = noneCRPQPage.confirmRiskDisclosure();
    boolean flag = false;
    if(updatedString.equals(expectedUpdatedNotes) || updatedString.equals(expectedUpdatedNotesCH)){
      flag = true;
    }
    assertThat(flag).isTrue();
    noneCRPQPage.clickNextDone();
  }

  @SneakyThrows
  @And("I click next button")
  public void iClickNextButton() {
    noneCRPQPage.clickNextDone();
  }

  @Then("I'm on mutual fund services welcome page")
  public void iMOnMutualFundServicesWelcomePage() {
    assertThat(noneCRPQPage.isWelcomeTitleDisplayed());
  }

  @Then("I'm on particular page")
  public void iMOnParticularPage(DataTable dataTable) {
    List<String> dataList = dataTable.asList(String.class);
    String title = dataList.get(0);
    assertThat(noneCRPQPage.isOnParticularPage(title)).as("On " + title + " page.").isTrue();
  }

  @And("I click confirm button")
  public void iClickConfirmButton() {
    noneCRPQPage.clickConfirmButton();
  }

  @And("I go to Review Investment Account Profile page")
  public void iGoToRIAPPage(){
    noneCRPQPage.iGoToRIAPPage();
  }

  @And("I click Open an account button")
  public void iClickOpenAnAccountBtn(){
    noneCRPQPage.iClickOpenAnAccountBtn();
  }

  @And("I verify step 1 is still done")
  public void verifyStep1IsStillDone(){
    noneCRPQPage.verifyStep1IsStillDone();
  }

  @And("I click by checking the box")
  public void iClickByCheckingBtn(){
    noneCRPQPage.iClickByCheckingBtn();
  }

  @And("I get default CRPQ options")
  public void iGetDefaultOptions(){
    noneCRPQPage.iGetDefaultDeclaration();
    noneCRPQPage.iGetDefaultQualificationInformation();
    noneCRPQPage.iGetDefaultInvestmentHorizon();
    noneCRPQPage.iGetDefaultKnowledgeAndExperience();
    noneCRPQPage.iGetDefaultNetWorth();
  }

  @And("I click X to close the page")
  public void iClickCloseBtn(){
    noneCRPQPage.iClosePage();
  }

  @SneakyThrows
  @And("I verify options have not change")
  public void iVerifyOptionsNotChange(){
    Thread.sleep(2*1000);
    boolean declarationNotChange = noneCRPQPage.iVerifyDeclarationNotChange();
    assertThat(declarationNotChange).isTrue();
    boolean qualificationInformationNotChange = noneCRPQPage.iVerifyQualificationInformationNotChange();
    assertThat(qualificationInformationNotChange).isTrue();
    boolean investmentHorizonNotChange = noneCRPQPage.iVerifyInvestmentHorizonNotChange();
    assertThat(investmentHorizonNotChange).isTrue();
    boolean knowledgeAndExperienceNotChange = noneCRPQPage.iVerifyKnowledgeAndExperienceNotChange();
    assertThat(knowledgeAndExperienceNotChange).isTrue();
    boolean netWorthNotChange = noneCRPQPage.iVerifyNetWorthNotChange();
    assertThat(netWorthNotChange).isTrue();
  }

  @And("I change options")
  public void iChangeOptions(){
    noneCRPQPage.iChangeOptions();
  }

  @And("I verify submit data is update")
  public void iVerifySubmitIsUpdate(){
    noneCRPQPage.iVerifySubmitIsUpdate();
  }

  @And("I click confirm button in transition page")
  public void iClickConfirmBtn(){
    noneCRPQPage.iClickConfirmBtn();
  }

  @And("verify I'm on particular page")
  public void verifyIMOnParticularPage(Map<String,String> data) {
    noneCRPQPage.verifyIAOPageTitle(data.get("title"));
  }

  @And("I answer the new none-CRPQ questions at {string}")
  public void answerTheNoneCRPQQuestionsCH(String sheetName){
    DataTable dataTable = new ExcelDataToDataTable(data_file, sheetName).getDatatable();
    List<List<String>> data = dataTable.asLists();
    for (List<String> row : data) {
      String question = row.get(0);
      String answer = row.get(1);
      String shotName = row.get(2);
      noneCRPQPage.chooseNoneCRQPWithScreenShot(question,answer,shotName);
      if(answer.startsWith("Yes") || answer.startsWith("是")){
        noneCRPQPage.enterName();
      }
    }
  }

  @And("I select nonCRPQ answerCH")
  public void iSelectNonCRPQAnswerCH(Map<String,String> data) {
    noneCRPQPage.chooseNoneCRQPCH(data.get("question"),data.get("answer"));
  }

}
