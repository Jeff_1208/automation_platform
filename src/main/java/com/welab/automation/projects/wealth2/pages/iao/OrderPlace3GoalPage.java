package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.List;

public class OrderPlace3GoalPage extends AppiumBasePage {
    private final int maxInvestAge = 69;
    protected String pageName = "Wealth Page";
    CommonPage commonPage;

    public OrderPlace3GoalPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "\t//android.view.ViewGroup[@content-desc=\"GOAL_SELECTION_LIST-category-Achieve Financial Freedom\"]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement achieveFinancialFreedom;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"]")
    private MobileElement continueBtn;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Monthly expenses (HKD)']")
    private MobileElement monthlyExpensesInput;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"calcButton-title\"]")
    private MobileElement calculateTargetWealthBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='View recommendation & save my goal']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='查閱建議及儲存目標']"),
    })
    private MobileElement viewRecommendationAndSaveMyGoalBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm that you want to proceed?']")
    private MobileElement confirmProceedText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Yes']")
    private MobileElement yesBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='View Portfolio']")
    private MobileElement viewPortfolioBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Back to Home']")
    private MobileElement backToHomeBtn;

    @AndroidFindBy(xpath = "//*[@text='Add New Goals']")
    private MobileElement addNewGoalsLink;

    @AndroidFindBy(xpath = "//*[@text='Select your goal']")
    private MobileElement selectGoalTitleTxt;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Next']")
    private MobileElement nextBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Review and confirm order']")
    private MobileElement reviewAndConfirmOrderText;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='66']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='67']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='68']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='69']/../../android.view.ViewGroup")
    })
    private List<MobileElement> ageElementsList;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView")
    private MobileElement FFTAgeSlider;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]")
    private MobileElement slideToConfirm;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]")
    private MobileElement radioBtn2;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Submit error']")
    private MobileElement submitErrorText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm']")
    private MobileElement confirmBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Done']"),
            @AndroidBy(xpath = "//*[@text='完成']")
    })
    private MobileElement doneButton;

    @SneakyThrows
    public void chooseAchieveFinancialFreedom(String screenShotName) {
        waitUntilElementVisible(achieveFinancialFreedom);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clickElement(achieveFinancialFreedom);
    }

    @SneakyThrows
    public void clickContinueBtn(String screenShotName) {
        waitUntilElementVisible(continueBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clickElement(continueBtn);
    }

    @SneakyThrows
    public void inputMonthlyExpenses(String monthlyExpenses,String screenShotName, String screenShotName01) {
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clearAndSendKeys(monthlyExpensesInput, monthlyExpenses);
        inputEnter(monthlyExpensesInput);
        Thread.sleep(2 * 1000);
        if (screenShotName01 != null) {
            takeScreenshot(screenShotName01);
        }
    }

    @SneakyThrows
    public void clickCalculateTargetWealthBtn(String screenShotName) {
        scrollUpToFindElement(calculateTargetWealthBtn, 2, 10);
        clickElement(calculateTargetWealthBtn);
        Thread.sleep(5 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void clickSaveMyGoalBtn(String screenShotName) {
        scrollUpToFindElement(viewRecommendationAndSaveMyGoalBtn, 2, 10);
        takeScreenshot(screenShotName);
        clickElement(viewRecommendationAndSaveMyGoalBtn);
    }

    @SneakyThrows
    public void clickViewPortfolioBtn(String screenShotName) {
        if (verifyElementExist(confirmProceedText)) {
            Thread.sleep(2 * 1000);
            clickElement(yesBtn);
        }
        Thread.sleep(5 * 1000);
        waitUntilElementVisible(viewPortfolioBtn);
        takeScreenshot(screenShotName);
        clickElement(viewPortfolioBtn);
    }

    @SneakyThrows
    public boolean verifyHaveNotEnoughMoney(String screenShotName) {
        if (verifyElementExist(backToHomeBtn)) {
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName);
            return true;
        }
        return false;
    }

    public void clickAddNewGoalLink() {
        clickElement(addNewGoalsLink);
        waitUntilElementVisible(selectGoalTitleTxt);
    }

    @SneakyThrows
    public void clickNextBtnOnScenarioAnalysisPage(String screenShotName) {
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(nextBtn);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(nextBtn);
    }

    @SneakyThrows
    public void clickNextBtnOnPortfolioPage(String screenShotName) {
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(nextBtn);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(nextBtn);
    }

    @SneakyThrows
    public void clickNextBtnOnOrderPage(String screenShotName) {
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(nextBtn);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(nextBtn);
    }

    @SneakyThrows
    public void clickNextButtonOnReviewAndConfirmPage(String screenShotName, String screenShotName01) {
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(reviewAndConfirmOrderText);
        Thread.sleep(5 * 1000);
        takeScreenshot(screenShotName);
        scrollUpToFindElement(nextBtn, 20, 3);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName01);
        clickElement(nextBtn);
    }

    @SneakyThrows
    public void slideToAge(String retiredAge, String screenShotName, String screenShotName01) {
        waitUntilElementVisible(continueBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        int offset = Integer.parseInt(retiredAge) - (maxInvestAge - ageElementsList.size()) - 1;
        MobileElement retiredAgeElement = ageElementsList.get(offset);
        setAge(FFTAgeSlider, retiredAgeElement);
        Thread.sleep(2 * 1000);
        if (screenShotName01 != null) {
            takeScreenshot(screenShotName01);
        }
    }

    public void setAge(MobileElement start, MobileElement end) {
        TouchAction action = new TouchAction(driver);
        action
                .longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(start)))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                .moveTo(ElementOption.element(end))
                .release()
                .perform();
    }

    @SneakyThrows
    public void inputMonthlyExpensesText(String screenShotName06, String screenShotName07) {
        Thread.sleep(2 * 1000);
        if (screenShotName06 != null) {
            takeScreenshot(screenShotName06);
        }
        clearAndSendKeys(monthlyExpensesInput, "10000");
        inputEnter(monthlyExpensesInput);
        Thread.sleep(2 * 1000);
        if (screenShotName07 != null) {
            takeScreenshot(screenShotName07);
        }
    }

    @SneakyThrows
    public void slideToConfirmOrder(String screenShotName14, String screenShotName15, String screenShotName16) {
        Thread.sleep(2 * 1000);
        if (screenShotName14 != null) {
            takeScreenshot(screenShotName14);
        }
        scrollUpToFindElement(slideToConfirm, 8, 3);
        Thread.sleep(2 * 1000);
        if (screenShotName15 != null) {
            takeScreenshot(screenShotName15);
        }
        radioBtn2.click();
        Thread.sleep(2 * 1000);
        if (screenShotName16 != null) {
            takeScreenshot(screenShotName16);
        }
        Thread.sleep(1000);
        // top-left
        int x = slideToConfirm.getLocation().x;
        int y = slideToConfirm.getLocation().y;
        // size
        int width = slideToConfirm.getRect().width;
        int x_start = x + 50;
        int x_end = x + width;
        int y_co = y + 2;
        TouchAction action =
                new TouchAction(driver)
                        .longPress(PointOption.point(x_start, y_co))
                        .moveTo(PointOption.point(x_end, y_co))
                        .release();
        action.perform();
    }

    @SneakyThrows
    public boolean verifyTheAccountOnlySell(String screenShotName) {
        if (verifyElementExist(submitErrorText)) {
            Thread.sleep(3 * 1000);
            takeScreenshot(screenShotName);
            clickElement(confirmBtn);
            return true;
        }
        return false;
    }

    public void scrollFindAddGoalLink(String screenShotName) {
        scrollUpToFindElement(addNewGoalsLink, 18, 1);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickDoneBtn(String screenShotName) {
        Thread.sleep(20 * 1000);
        waitUntilElementVisible(doneButton);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(doneButton);
    }
}

