package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.SneakyThrows;

public class IAOIOSPage extends AppiumBasePage {
    private final int maxInvestAge = 69;
    private String pageName = "IAO Page";
    CommonPage commonPage;

    public IAOIOSPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }


    public void clickOpenAccountButton(){
        clickByPicture("src/main/resources/images/wealth/iphoneSE/IAO/openAccountButton.png",50,50);
    }
    public void clickNextButton(){
        clickByPicture("src/main/resources/images/wealth/iphoneSE/IAO/nextButtonn.png",50,50);
    }

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'歡迎使用理財服務')]")
    private MobileElement welcomeWealthPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'確認資料及輸入投資賬戶資料')]")
    private MobileElement confirmInfomationStartPage;


    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'確認資料 個人資料')]")
    private MobileElement infomationDetailPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你嘅教育水平? 大學學士或以上')]")
    private MobileElement educationalPage;


    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你心目中嘅投資年期*?')]")
    private MobileElement selectYearsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'在過去3年，就以下產品我有相關知識或/及經驗:')]")
    private MobileElement experiencePage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'嘅總資產淨值有幾多')]")
    private MobileElement totalAmountPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'持牌人士聲明 如果你目前受僱於《證券及期貨條例》')]")
    private MobileElement employeeOfIntermediaryPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'覆核投資賬戶資料')]")
    private MobileElement doubleConfirmInfomationDetailPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,' 服務條款、風險披露、重要事項及其他文件')]")
    private MobileElement termsAndConditionsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'您已提交申請')]")
    private MobileElement applicationSubmitPage ;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'無需上載文件')]")
    private MobileElement noDocumentUploadRequiredPage ;

//    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'上載文件')]")
    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'只適用於中介人僱員')]")
    private MobileElement withDocumentUploadRequiredPage ;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'寫客戶風險分析問卷以了解自已嘅風險概況')]")
    private MobileElement fillInCustomerRiskProfilingQuestionPage ;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'上載文件 僱主同意書 請提供你嘅僱主同意書')]")
    private MobileElement uploadPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'準備好啲文件再返嚟財服務中心補交啦')]")
    private MobileElement pleaseSelectFilePage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你已成功上載文件')]")
    private MobileElement uploadFileSuccessPage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"知道喇\"]")
    private MobileElement IKnowButton;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"允许访问所有照片\"]")
    private MobileElement allowAccessPictureLibrary;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'主同意書 格式: 圖片/PDF')]")
    private MobileElement addPicturePage;
    @iOSXCUITFindBy(xpath = "//*[@name=\"圖庫\"]")
    private MobileElement pictureLibrary;

    @iOSXCUITFindBy(xpath = "//*[@name=\"最近项目\"]")
    private MobileElement lastPictureLibrary;

    @iOSXCUITFindBy(xpath = "//*[@name=\"全景照片\"]")
    private MobileElement allViewPictureLibrary;


    @SneakyThrows
    public void IselectNoSkipItem(String screenShotName){
        waitUntilElementVisible(employeeOfIntermediaryPage);
        takeScreenshot(screenShotName);
        clickByLocationByPercent2(7.8, 58.3); //select no skip
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void IselectYesConsentLetterFromYourEmployer(String screenShotName){
        waitUntilElementVisible(employeeOfIntermediaryPage);
        clickByLocationByPercent2(7.8, 65); //select Yes Consent
        Thread.sleep(1000);
        clickByLocationByPercent2(50, 75); //click input
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString("Dawen");
        takeScreenshot(screenShotName);
        Thread.sleep(1000);
        clickByLocationByPercent2(50, 60); //click confirm button
    }


    @SneakyThrows
    public void clickOpenWealthAccountIOS() {
        clickOpenAccountButton();
    }

    @SneakyThrows
    public void clickNextConfirmInformationPageIOS(String screenShotName,
                                                   String screenShotName1, String screenShotName2) {

        waitUntilElementVisible(welcomeWealthPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        clickNextButton();

        waitUntilElementVisible(confirmInfomationStartPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);

        clickNextButton();//go to information page
        waitUntilElementVisible(infomationDetailPage);
        Thread.sleep(4000);
        scrollUp();
        scrollUp();
        takeScreenshot(screenShotName2);
        clickNextButton();
    }

    public void selectFirstCircleItem(){
        clickByLocationByPercent2(8.6,27);
    }

    @SneakyThrows
    public void answerQuestions(String screenShotName, String screenShotName1,
                                String screenShotName2, String screenShotName3) {

        waitUntilElementVisible(educationalPage);
        takeScreenshot(screenShotName);
        selectFirstCircleItem();
        Thread.sleep(1000);

        waitUntilElementVisible(selectYearsPage);
        takeScreenshot(screenShotName1);
        selectFirstCircleItem();
        Thread.sleep(1000);

        waitUntilElementVisible(experiencePage);
        clickByLocationByPercent2(8.6,48.5);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);

        clickNextButton();
        waitUntilElementVisible(totalAmountPage);
        takeScreenshot(screenShotName3);
        clickByLocationByPercent2(8.6,40.7);
        Thread.sleep(1000);
    }


    @SneakyThrows
    public void confirmApplicationInformationAndUploadPicture(String screenShotName,
                                                              String screenShotName1,
                                                              String screenShotName2,
                                                              String screenShotName3,
                                                              String screenShotName4){

        confirmApplicationAndSubmit(screenShotName, screenShotName1, screenShotName2);

        waitUntilElementVisible(withDocumentUploadRequiredPage);
        Thread.sleep(1000);
        clickNextButton();
        waitUntilElementVisible(uploadPage);
        Thread.sleep(1000);

        uploadPicture();

        waitUntilElementVisible(uploadPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);

        clickByLocationByPercent(50,92); //click confirm button
        //waitUntilElementVisible(uploadFileSuccessPage);
        Thread.sleep(5000);
        clickNextButton();
        checkFillInCustomerRiskProfilingQuestionPage(screenShotName4);
    }


    @SneakyThrows
    public void uploadPicture(){
        clickByLocationByPercent(50,27);//click consent from your employer

        waitUntilElementVisible(addPicturePage);
        Thread.sleep(1000);
        clickByLocationByPercent(20,80);//click add button
        Thread.sleep(1000);

        waitUntilElementVisible(pictureLibrary);
        clickElement(pictureLibrary);
        Thread.sleep(1000);
        if(isShow(allowAccessPictureLibrary,2)){
            clickElement(allowAccessPictureLibrary);
            Thread.sleep(1000);
        }
        waitUntilElementVisible(lastPictureLibrary);
        clickElement(lastPictureLibrary);
        Thread.sleep(1000);

        clickByLocationByPercent(15,17);//select first picture
        Thread.sleep(1000);
        clickByLocationByPercent(93,6); //click finish
        Thread.sleep(1000);
        waitUntilElementVisible(addPicturePage);
        Thread.sleep(6000);
        clickByLocationByPercent(50,92); //click finish
    }

    @SneakyThrows
    public void confirmApplicationInformationSteps(String screenShotName,
                                                   String screenShotName1,
                                                   String screenShotName2,
                                                   String screenShotName3,
                                                   String screenShotName4){


        confirmApplicationAndSubmit(screenShotName, screenShotName1, screenShotName2);

        waitUntilElementVisible(noDocumentUploadRequiredPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickNextButton();

        checkFillInCustomerRiskProfilingQuestionPage(screenShotName4);
    }

    @SneakyThrows
    public void checkFillInCustomerRiskProfilingQuestionPage(String screenShotName){

        waitUntilElementVisible(fillInCustomerRiskProfilingQuestionPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickNextButton();
    }


    @SneakyThrows
    public void confirmApplicationAndSubmit(String screenShotName,
                                                   String screenShotName1,
                                                   String screenShotName2){
        waitUntilElementVisible(doubleConfirmInfomationDetailPage);
        takeScreenshot(screenShotName);
        scrollUp();
        clickByLocationByPercent(50,92); //click confirm button
        waitUntilElementVisible(termsAndConditionsPage);
        clickByLocationByPercent2(7,87.8); //click circle
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        clickByLocationByPercent(50,92); //click sumbit button go to application submit page

        waitUntilElementVisible(applicationSubmitPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickNextButton();
    }


    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'現時財富總值')]")
    private MobileElement wealthCurrentAmount;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'用1分鐘嚟了解你嘅客戶風險狀況')]")
    private MobileElement fillCustomerRiskStartPage ;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'傾向保護資本的投資以賺取相當於存款利率的回報')]")
    private MobileElement earnDespositRatePage;


    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'只想承受最低風險及享有最低程度的回報')]")
    private MobileElement howDoYouSeeRiskReturn ;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'我可以接受到嘅投資價格波動為')]")
    private MobileElement myInvestmentValueFluctuatesBetween;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'有幾多可以用嚟儲蓄同埋投資')]")
    private MobileElement howMuchTotalPercentPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你喺未來12個月內最多會賣出')]")
    private MobileElement _12MonthsSellPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'如果你嘅投資價格喺6個月內跌咗50%')]")
    private MobileElement droppedOverPast6MonthsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你可以隨時調動嘅應急資產')]")
    private MobileElement howManyMonthsExpenses;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'你嘅客戶風險評級')]")
    private MobileElement riskLevel;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'恭喜晒，所有嘢準備好啦！你個戶口將會喺2個工作天內搞掂')]")
    private MobileElement FinishPage;

    @SneakyThrows
    public void fillCustomerRiskProfilingQuestions(String screenShotName,
                                                   String screenShotName1,
                                                   String screenShotName2,
                                                   String screenShotName3,
                                                   String screenShotName4,
                                                   String screenShotName5,
                                                   String screenShotName6){

        waitUntilElementVisible(fillCustomerRiskStartPage);
        clickByLocationByPercent(50,92);  //click start

        waitUntilElementVisible(earnDespositRatePage);
        Thread.sleep(200);
        takeScreenshot(screenShotName);
        clickByLocationByPercent(50,45);  //select item

        waitUntilElementVisible(howDoYouSeeRiskReturn);
        Thread.sleep(200);
        takeScreenshot(screenShotName1);
        clickByLocationByPercent(50,46);  //select item

        waitUntilElementVisible(myInvestmentValueFluctuatesBetween);
        Thread.sleep(200);
        takeScreenshot(screenShotName2);
        clickByLocationByPercent(50,46);  //select item

        waitUntilElementVisible(howMuchTotalPercentPage);
        Thread.sleep(200);
        takeScreenshot(screenShotName3);
        clickByLocationByPercent(50,46);  //select item

        waitUntilElementVisible(_12MonthsSellPage);
        Thread.sleep(200);
        takeScreenshot(screenShotName4);
        clickByLocationByPercent(50,46);  //select item

        waitUntilElementVisible(droppedOverPast6MonthsPage);
        Thread.sleep(200);
        takeScreenshot(screenShotName5);
        clickByLocationByPercent(50,46);  //select item

        waitUntilElementVisible(howManyMonthsExpenses);
        Thread.sleep(200);
        takeScreenshot(screenShotName6);
        clickByLocationByPercent(50,46);  //select item
    }

    @SneakyThrows
    public void confirmRiskDetail(String screenShotName, String screenShotName1){
        Thread.sleep(5000);
        scrollUp();
        scrollUp();
        clickByLocationByPercent(50,92); //click confirm
        Thread.sleep(3000);
        waitUntilElementVisible(riskLevel);
        takeScreenshot(screenShotName);
        scrollUp();
        clickNextButton();
        waitUntilElementVisible(FinishPage);
        takeScreenshot(screenShotName1);
        clickNextButton();
        Thread.sleep(4000);
    }

    @SneakyThrows
    public void checkApplicationResult(String screenShotName){
        waitUntilElementVisible(wealthCurrentAmount);
        Thread.sleep(1000);
        clickOpenAccountButton();
        Thread.sleep(4000);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
    }


}
