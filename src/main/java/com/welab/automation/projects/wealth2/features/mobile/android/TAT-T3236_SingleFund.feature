Feature: singleFund

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    When Login with user and password from properties

  @VBWMIS-T304
  Scenario:Reg_SingleFund_001 HKD buy USD Fund(one investment+monthly subscription)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    Then I click View full fund list
      | screenShotName | Reg_SingleFund_001_Android_04 |
    Then I search USD fund
      | screenShotName   | Reg_SingleFund_001_Android_05 |
      | screenShotName01 | Reg_SingleFund_001_Android_06 |
    And I click random fund on Full Fund List Page
      | screenShotName | Reg_SingleFund_001_Android_07 |
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment oneTime and monthly
      | oneTime          | 101                           |
      | monthly          | 102                           |
      | screenShotName   | Reg_SingleFund_001_Android_09 |
      | screenShotName01 | Reg_SingleFund_001_Android_10 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |


  @VBWMIS-T604
  Scenario:Reg_SingleFund_002 HKD buy USD Fund(one investment)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_002_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_002_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_002_Android_03 |
    Then I click View full fund list
      | screenShotName | Reg_SingleFund_002_Android_04 |
    Then I search USD fund
      | screenShotName   | Reg_SingleFund_002_Android_05 |
      | screenShotName01 | Reg_SingleFund_002_Android_06 |
    And I click random fund on Full Fund List Page
      | screenShotName | Reg_SingleFund_002_Android_07 |
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_002_Android_08 |
    And I enter buy fund investment oneTime
      | oneTime          | 103                           |
      | screenShotName   | Reg_SingleFund_002_Android_09 |
      | screenShotName01 | Reg_SingleFund_002_Android_10 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_002_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_002_Android_12 |
      | screenShotName01 | Reg_SingleFund_002_Android_13 |


  @VBWMIS-T606
  Scenario:Reg_SingleFund_003 USD buy USD Fund(one investment)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_003_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_003_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_003_Android_03 |
    Then I click View full fund list
      | screenShotName | Reg_SingleFund_003_Android_04 |
    Then I search USD fund
      | screenShotName   | Reg_SingleFund_003_Android_05 |
      | screenShotName01 | Reg_SingleFund_003_Android_06 |
    And I click random fund on Full Fund List Page
      | screenShotName | Reg_SingleFund_003_Android_07 |
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_003_Android_08 |
    Then I click Currency Switch button
    And I enter buy fund investment oneTime
      | oneTime          | 59                            |
      | screenShotName   | Reg_SingleFund_003_Android_09 |
      | screenShotName01 | Reg_SingleFund_003_Android_10 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_003_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_003_Android_12 |
      | screenShotName01 | Reg_SingleFund_003_Android_13 |



  @VBWMIS-T655
  Scenario:Reg_SingleFund_004 HKD buy HKD Fund(one investment+monthly subscription)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_004_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_004_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_004_Android_03 |
    Then I click View full fund list
      | screenShotName | Reg_SingleFund_004_Android_04 |
    Then I search HKD fund
      | screenShotName   | Reg_SingleFund_004_Android_05 |
      | screenShotName01 | Reg_SingleFund_004_Android_06 |
    And I click random fund on Full Fund List Page
      | screenShotName | Reg_SingleFund_004_Android_07 |
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_004_Android_08 |
    And I enter buy fund investment oneTime and monthly
      | oneTime          | 105                           |
      | monthly          | 106                           |
      | screenShotName   | Reg_SingleFund_004_Android_09 |
      | screenShotName01 | Reg_SingleFund_004_Android_10 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_004_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_004_Android_12 |
      | screenShotName01 | Reg_SingleFund_004_Android_13 |

  @VBWMIS-T656
  Scenario:Reg_SingleFund_005 HKD buy HKD Fund(one investment)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_005_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_005_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_005_Android_03 |
    Then I click View full fund list
      | screenShotName | Reg_SingleFund_005_Android_04 |
    Then I search HKD fund
      | screenShotName   | Reg_SingleFund_005_Android_05 |
      | screenShotName01 | Reg_SingleFund_005_Android_06 |
    And I click random fund on Full Fund List Page
      | screenShotName | Reg_SingleFund_005_Android_07 |
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_005_Android_08 |
    And I enter buy fund investment oneTime
      | oneTime          | 107                           |
      | screenShotName   | Reg_SingleFund_005_Android_09 |
      | screenShotName01 | Reg_SingleFund_005_Android_10 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_005_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_005_Android_12 |
      | screenShotName01 | Reg_SingleFund_005_Android_13 |

  @VBWMIS-T607
  Scenario:Reg_SingleFund_006 Top up order(USD Fund)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_006_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_006_Android_02 |
    Then I click USD Fund From Fund List
      | screenShotName | Reg_SingleFund_006_Android_03 |
    Then I click Buy button on Fund detail page
      | screenShotName | Reg_SingleFund_006_Android_04 |
    And I enter buy fund investment oneTime
      | oneTime          | 107                           |
      | screenShotName   | Reg_SingleFund_006_Android_05 |
      | screenShotName01 | Reg_SingleFund_006_Android_06 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_006_Android_07 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_006_Android_08 |
      | screenShotName01 | Reg_SingleFund_006_Android_09 |

  @VBWMIS-T647
  Scenario:Reg_SingleFund_007 Top up order(HKD Fund)
    And I goto wealth page
      | screenShotName | Reg_SingleFund_007_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_007_Android_02 |
    Then I click HKD Fund From Fund List
      | screenShotName | Reg_SingleFund_007_Android_03 |
    Then I click Buy button on Fund detail page
      | screenShotName | Reg_SingleFund_007_Android_04 |
    And I enter buy fund investment oneTime
      | oneTime          | 108                           |
      | screenShotName   | Reg_SingleFund_007_Android_05 |
      | screenShotName01 | Reg_SingleFund_007_Android_06 |
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_007_Android_07 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_007_Android_08 |
      | screenShotName01 | Reg_SingleFund_007_Android_09 |


    #    this case need USDBalance>1
#  @VBWMIS-T608
#  Scenario:Reg_SingleFund_008 Sell Partial HKD Fund
#    And I goto wealth page
#      | screenShotName | Reg_SingleFund_008_Android_01 |
#    Then I click view my funds on GoWealth Page
#      | screenShotName | Reg_SingleFund_008_Android_02 |
#    And find can sell HKD Fund From Fund List
#      | screenShotName | Reg_SingleFund_008_Android_03 |
#    Then I click sell button on Fund detail page
#      | screenShotName | Reg_SingleFund_008_Android_04 |
#    And I enter sell One-off Redemption
#      | amount           | 10                             |
#      | screenShotName   | Reg_SingleFund_008_Android_05 |
#      | screenShotName01 | Reg_SingleFund_008_Android_06 |
#    Then I confirm Review and Slide to confirm Order
#    Then I verify sell fund by success
#      | screenShotName | Reg_SingleFund_008_Android_07 |
#    And I verify sell fund order status
#      | screenShotName   | Reg_SingleFund_008_Android_08 |
#      | screenShotName01 | Reg_SingleFund_008_Android_09 |


#      this case need USDBalance>1
#  @VBWMIS-T610
#  Scenario:Reg_SingleFund_009 Sell All HKD Fund
#    And I goto wealth page
#      | screenShotName | Reg_SingleFund_009_Android_01 |
#    Then I click view my funds on GoWealth Page
#      | screenShotName | Reg_SingleFund_009_Android_02 |
#    And find can sell HKD Fund From Fund List
#      | screenShotName | Reg_SingleFund_009_Android_03 |
#    Then I click sell button on Fund detail page
#      | screenShotName | Reg_SingleFund_009_Android_04 |
#    And I enter sell One-off Redemption
#      | amount           | all                           |
#      | screenShotName   | Reg_SingleFund_009_Android_05 |
#      | screenShotName01 | Reg_SingleFund_009_Android_06 |
#    Then I confirm Review and Slide to confirm Order
#    Then I verify sell fund by success
#      | screenShotName | Reg_SingleFund_009_Android_07 |
#    And I verify sell fund order status
#      | screenShotName   | Reg_SingleFund_009_Android_08 |
#      | screenShotName01 | Reg_SingleFund_009_Android_09 |

#      this case need USDBalance>1
#  @VBWMIS-T658
#  Scenario:Reg_SingleFund_010 Sell Partial USD Fund
#    And I goto wealth page
#      | screenShotName | Reg_SingleFund_010_Android_01 |
#    Then I click view my funds on GoWealth Page
#      | screenShotName | Reg_SingleFund_010_Android_02 |
#    And find can sell USD Fund From Fund List
#      | screenShotName | Reg_SingleFund_010_Android_03 |
#    Then I click sell button on Fund detail page
#      | screenShotName | Reg_SingleFund_010_Android_04 |
#    And I enter sell One-off Redemption
#      | amount           | 2                             |
#      | screenShotName   | Reg_SingleFund_010_Android_05 |
#      | screenShotName01 | Reg_SingleFund_010_Android_06 |
#    Then I confirm Review and Slide to confirm Order
#    Then I verify sell fund by success
#      | screenShotName | Reg_SingleFund_010_Android_07 |
#    And I verify sell fund order status
#      | screenShotName   | Reg_SingleFund_010_Android_08 |
#      | screenShotName01 | Reg_SingleFund_010_Android_09 |


#      this case need USDBalance>1
#  @VBWMIS-T659
#  Scenario:Reg_SingleFund_011 Sell All USD Fund
#    And I goto wealth page
#      | screenShotName | Reg_SingleFund_011_Android_01 |
#    Then I click view my funds on GoWealth Page
#      | screenShotName | Reg_SingleFund_011_Android_02 |
#    And find can sell USD Fund From Fund List
#      | screenShotName | Reg_SingleFund_011_Android_03 |
#    Then I click sell button on Fund detail page
#      | screenShotName | Reg_SingleFund_011_Android_04 |
#    And I enter sell One-off Redemption
#      | amount           | all                           |
#      | screenShotName   | Reg_SingleFund_011_Android_05 |
#      | screenShotName01 | Reg_SingleFund_011_Android_06 |
#    Then I confirm Review and Slide to confirm Order
#    Then I verify sell fund by success
#      | screenShotName | Reg_SingleFund_011_Android_07 |
#    And I verify sell fund order status
#      | screenShotName   | Reg_SingleFund_011_Android_08 |
#      | screenShotName01 | Reg_SingleFund_011_Android_09 |


  @VBWMIS-T611
  Scenario:Reg_SingleFund_012 Edit Monthly standing instruction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_012_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_012_Android_02 |
    And I goto Monthly standing instruction Edit page
      | screenShotName | Reg_SingleFund_012_Android_04 |
    Then I enter Monthly instruction amount
      | screenShotName   | Reg_SingleFund_012_Android_05 |
      | screenShotName01 | Reg_SingleFund_012_Android_06 |
    Then I confirm Review and confirm Order
    And I verify update Monthly instruction success
      | screenShotName | Reg_SingleFund_012_Android_07 |
    Then I verify Monthly instruction amount changed
      | screenShotName | Reg_SingleFund_012_Android_08 |
    And I goto Order status page to verify Monthly instruction
      | screenShotName | Reg_SingleFund_012_Android_09 |



