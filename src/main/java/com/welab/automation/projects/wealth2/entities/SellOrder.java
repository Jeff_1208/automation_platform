package com.welab.automation.projects.wealth2.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SellOrder {
  @Getter @Setter private String portfolioName;
  @Getter @Setter private String estimatedSellAmount;
  @Getter @Setter private String settleIn;
  @Getter @Setter private List<Fund> funds;

  public static class Fund {
    @Getter @Setter String FundName;
    @Getter @Setter String estimatedSellAmount;
    @Getter @Setter String indicativeNAV;
    @Getter @Setter String numberOfUnits;
  }
}
