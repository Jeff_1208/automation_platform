Feature: Update CRPQ for MCV

  Background: Login
    Given Open WeLab App
    And Enable skip Root


  Scenario: Reg_IAO_001 ETB_IAO NST for MCV
    Then Login with IAO Account for MCV
    Then I can see Marketing preferences Page
      | screenShotName | Reg_IAO_001_Android_01 |
    And I goto wealth page
      | screenShotName | Reg_IAO_001_Android_02 |
    Then Verify GPS Checking for MCV
      | screenShotName | Reg_IAO_001_Android_03 |
      | screenShotName1 | Reg_IAO_001_Android_04 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_001_Android_05 |
    And I click next on Welcome to Wealth Management Services Page
      | screenShotName | Reg_IAO_001_Android_06 |
    And I click next on Confirm information Page
      | screenShotName | Reg_IAO_001_Android_07 |
    And I click next on Verify personal information page
      | screenShotName | Reg_IAO_001_Android_08 |
    And I answer the new none-CRPQ questions at "none-CRPQ-NST"
    #RiskPage
    And I click confirm on Confirm page
      | screenShotName | Reg_IAO_001_Android_09 |
    Then I click circle and submit on Update and verify personal info
      | screenShotName | Reg_IAO_001_Android_10 |
    #upload-1
    Then I click next on Update and verify personal info page
      | screenShotName | Reg_IAO_001_Android_11 |
    #upload-2
    Then I click next on Upload document if required page
      | screenShotName | Reg_IAO_001_Android_12 |
    #Customer Risk Profiling
    Then I click next on Complete Customer Risk Profiling Questionnaire page
      | screenShotName | Reg_IAO_001_Android_13 |
    Then I click Start button Complete Customer Risk Profiling
      | screenShotName | Reg_IAO_001_Android_14 |
    And I select CRPQ answer with "CRPQ"
    #upload-3RiskPage
    And I verify Customer Risk Rating
      | risk            | 2                      |
      | screenShotName  | Reg_IAO_001_Android_15 |
      | screenShotName1 | Reg_IAO_001_Android_16 |
    And I click next on Congratulations page
      | screenShotName | Reg_IAO_001_Android_17 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_001_Android_18 |
    Then I verify open account success
      | screenShotName | Reg_IAO_001_Android_19 |



  Scenario: Reg_IAO_002 ETB_IAO ST for MCV
    Then Login with IAO Account for MCV
    Then I can see Marketing preferences Page
      | screenShotName | Reg_IAO_002_Android_01 |
    And I goto wealth page
      | screenShotName | Reg_IAO_002_Android_02 |
    Then Verify GPS Checking for MCV
      | screenShotName | Reg_IAO_002_Android_03 |
      | screenShotName1 | Reg_IAO_002_Android_04 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_002_Android_05 |
    And I click next on Welcome to Wealth Management Services Page
      | screenShotName | Reg_IAO_002_Android_06 |
    And I click next on Confirm information Page
      | screenShotName | Reg_IAO_002_Android_07 |
    And I click next on Verify personal information page
      | screenShotName | Reg_IAO_002_Android_08 |
    And I answer the new none-CRPQ questions at "none-CRPQ-ST"
    And I click confirm on Confirm page
      | screenShotName | Reg_IAO_002_Android_09 |
    Then I click circle and submit on Update and verify personal info
      | screenShotName | Reg_IAO_002_Android_10 |
    Then I click next on Update and verify personal info page
      | screenShotName | Reg_IAO_002_Android_11 |
    Then I click next on Upload document if required page
      | screenShotName | Reg_IAO_002_Android_12 |
    And I take photo to upload with screenShot
      | screenShotName   | Reg_IAO_002_Android_13 |
      | screenShotName01 | Reg_IAO_002_Android_14 |
      | screenShotName02 | Reg_IAO_002_Android_15 |
      | screenShotName03 | Reg_IAO_002_Android_16 |
      | screenShotName04 | Reg_IAO_002_Android_17 |
      | screenShotName05 | Reg_IAO_002_Android_18 |
    Then I click next on Complete Customer Risk Profiling Questionnaire page
      | screenShotName | Reg_IAO_002_Android_19 |
    Then I click Start button Complete Customer Risk Profiling
      | screenShotName | Reg_IAO_002_Android_20 |
    And I select CRPQ answer with "CRPQ"
    #upload-3RiskPage
    And I verify Customer Risk Rating
      | risk            | 2                      |
      | screenShotName  | Reg_IAO_002_Android_21 |
      | screenShotName1 | Reg_IAO_002_Android_22 |
    And I click next on Congratulations page
      | screenShotName | Reg_IAO_002_Android_23 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_002_Android_24 |
    Then I verify open account success
      | screenShotName | Reg_IAO_002_Android_25 |


