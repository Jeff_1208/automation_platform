package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.LoginPage;
import com.welab.automation.projects.wealth2.pages.iao.ForeignExchangePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Map;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.ui.Sleeper;
import org.picocontainer.annotations.Inject;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

public class ForeignExchangeSteps {

//    SoftAssertions softAssert = new SoftAssertions();

    LoginPage loginPage;
    CommonPage commonPage;
    ForeignExchangePage foreignExchangePage;

    public ForeignExchangeSteps() {
        loginPage = new LoginPage();
        commonPage = new CommonPage();
        foreignExchangePage = new ForeignExchangePage();
    }


    @Given("I go to Foreign Exchange Page")
    public void openFXPage() {
        foreignExchangePage.clickHomeForeignExchange();
    }

//    @And("I enter amount {}")
//    public void iEnterAmount(String amount) {
//        foreignExchangePage.enterAmount(amount);
//    }

    @And("I check slide button disabled")
    public void iCheckSlideBtn() {
        Boolean status = foreignExchangePage.slideToConfirmStatus();
        assertThat(status).isFalse();
    }

    @And("I check WealthBalance")
    public void iCheckWealthBalance() throws InterruptedException {
        Boolean status = foreignExchangePage.checkWealthBalance();
        assertThat(status).isTrue();
    }

    @And("I check TotalBalanceArrow")
    public void iCheckTotalBalanceArrow() throws InterruptedException {
        Boolean status = foreignExchangePage.checkTotalBalanceArrow();
        assertThat(status).isTrue();
    }

    @And("I click TotalBalanceArrow")
    public void iClickTotalBalanceArrow() {
        foreignExchangePage.clickTotalBalanceArrow();
    }

    @And("I check Success Done")
    public void iCheckSuccesssDone() throws InterruptedException {
        Boolean status = foreignExchangePage.checkSuccesssDone();
        assertThat(status).isTrue();
    }

    @And("I click Success Done")
    public void iClickSuccesssDone() {
        foreignExchangePage.clickSuccesssDone();
    }

    @And("I click slide button")
    public void iClickSlideBtn() {
        foreignExchangePage.clickImportantNotes();
    }

    @And("I slide confirm button")
    public void iSlideConfirmBtn() {
        foreignExchangePage.setSlideToConfirm();
    }

    @And("I check review button displayed")
    public void iCheckReviewBtn() throws InterruptedException {
        Boolean status = foreignExchangePage.reviewBtnstatus();
        assertThat(status).isTrue();
    }

    @And("I check the Foreign Exchange Show")
    public void iCheckgetForeignExchangeShow() {
        Boolean foreignExchangeStatus = foreignExchangePage.getForeignExchangeStatus();
        assertThat(foreignExchangeStatus).isFalse();
    }

    @And("I check the Foreign Exchange Not Show")
    public void iCheckgetForeignExchangeNotShow() {
        Boolean foreignExchangeStatus = foreignExchangePage.getForeignExchangeStatus();
        assertThat(foreignExchangeStatus).isFalse();
    }

    @And("I check the Add Money Show")
    public void iCheckgetAddMoneyShow() {
        Boolean foreignExchangeStatus = foreignExchangePage.getAddMoneyStatus();
        assertThat(foreignExchangeStatus).isTrue();
    }

    @And("I goto the foreign exchange page")
    public void iEnteredTheForeignExchangePage(Map<String, String> data) {
        foreignExchangePage.clickForeignExchangeBtn(data.get("screenShotName"));
    }

    @And("The entered amount is greater than the available balance ([^\"]\\S*)$")
    public void theEnteredAmountIsGreaterThanTheAvailableBalance(String moreThan) {
        foreignExchangePage.inputYouConvert(moreThan);
    }

    @And("I saw the prompt ([^\"]\\S*)$")
    public void iSawThePrompt(String moreThan) {
        Boolean GreaterThanAvailable = foreignExchangePage.prompt();
        if (moreThan.equals("moreThan")) {
            assertThat(GreaterThanAvailable).isTrue();
        }
        if (moreThan.equals("equal")) {
            assertThat(GreaterThanAvailable).isFalse();
        }
    }

    @And("I check review button displayed for {} mins")
    public void iCheckReviewButtonDisplayedForNumberMins(String time) throws ParseException, InterruptedException {
        assertThat(foreignExchangePage.enRouteInput("", time)).isTrue();
    }

    @And("I clicked the review button")
    public void iClickedTheReviewButton() {
        foreignExchangePage.clickReview();
    }

    @And("I want to enter the ([^\"]\\S*) while waiting for ([^\"]\\S*) minutes")
    public void iWantToEnterTheAmountWhileWaitingForNumberMinutes(String amount, String time)
            throws ParseException, InterruptedException {
        assertThat(foreignExchangePage.enRouteInput(amount, time)).isTrue();
    }

    @And("I choose the ([^\"]\\S*)$")
    public void iChooseTheCurrency(String currency) {
        foreignExchangePage.clickDropDownBox(currency);
    }

    @And("I enter exchange money")
    public void iEnterTheMoney(Map<String,String> data) {
        foreignExchangePage.enterAmount(data.get("money"), data.get("screenShotName"));
    }

    @And("I submitted my foreign exchange order")
    public void iSubmittedMyForeignExchangeOrder() {
        foreignExchangePage.setSlideToConfirm();
    }

    @Then("through adb common input MSK password")
    public void inputMSKPassword(Map<String, String> data) {
        foreignExchangePage.clickPersonalInformation(data.get("screenShotName"));
    }

    @And("I verify the Foreign Exchange submitted order")
    public void verifyFXSubmittedOrder(Map<String, String> data) {
        assertThat(foreignExchangePage.transactionStatus(data.get("screenShotName"))).isTrue();
//    Boolean amountJudgment=foreignExchangePage.amountJudgment();
//    assertThat(amountJudgment).isTrue();
    }

//    @And("I verify HKD transaction on process page")
//    public void iVerifyHKDTransactionOnProcessPage() {
//    assertThat(foreignExchangePage.whether()).isTrue();
//        assertThat(foreignExchangePage.verifyHKDTransaction()).isTrue();
//    }

    @And("I went to the balance details to check the converted amount")
    public void iWentToTheDalanceBetailsToCheckTheConvertedAmount() {
        foreignExchangePage.clickBalanceDetails();
    }

    @And("I clicked USB balance")
    public void iClickeUsdBalance() {
        foreignExchangePage.clickBalance();
    }

    @And("I saw the HKD order information")
    public void iSawTheOrderInformation() {
        assertThat(foreignExchangePage.balanceHKD()).isTrue();
    }

    @And("Determine whether the control is available")
    public void controlAvailable() {
        assertThat(foreignExchangePage.slideButtonAvailable()).isTrue();
    }

    @And("View Currency Message")
    public void currency() {
        assertThat(foreignExchangePage.viewCurrency()).isTrue();
    }

    @And("Check whether the current page jumps")
    public void checkPage() {
        assertThat(foreignExchangePage.checkPage()).isTrue();
    }

    @And("I stayed on the FX page for ([^\"]\\S*) minutes")
    public void minutes(String number) {
        foreignExchangePage.expect(number);
    }

    @And("I verify Total balance and Available balance")
    public void verifyBalance(Map<String, String> data) {
        foreignExchangePage.verifyBalance(data.get("screenShotName"));
    }

    @And("I goto Go save page")
    public void gotoGoSavePage(Map<String, String> data) {
        foreignExchangePage.openGoSave(data.get("screenShotName"));
    }

    @Then("Operate the web page on the app")
    public void operateWebPageOnAppage(Map<String, String> data) {
        foreignExchangePage.checkTheHomePageTotalBalance(data.get("screenShotName"));
    }

    @Then("I switch to h5")
    public void switchH5() {
        foreignExchangePage.switchH5();
    }

    @Then("I clicked join Now")
    public void joinNow() {
        foreignExchangePage.joinNow();
    }

    @Then("I Enter ([^\"]\\S*)$")
    public void enter(String amount) {
        foreignExchangePage.enter(amount);
    }

    @Then("I click Next")
    public void clickNext() {
        foreignExchangePage.clickNext();
    }

    @Then("I clicked OK and agreed to the terms and conditions")
    public void clickedConditions() {
        foreignExchangePage.clickedConditions();
    }

    @Then("I clicked the Finish button")
    public void clickedFinish() {
        foreignExchangePage.clickedConditions();
    }

    @Then("I switch back to the native app")
    public void switchNative() {
        foreignExchangePage.switchNative();
    }

    @Then("I click transfer goto transfer page")
    public void iClickTransfer(Map<String,String>data) {
        foreignExchangePage.iClickTransfer(data.get("screenShotName"));
    }

    @Then("USD page back home page")
    public void usdPageBackHomePage() {
        foreignExchangePage.usdPageBackHomePage();
    }

    @Then("I verify HKD transaction detail")
    public void iVerifyHKDTransactionDetail() {
        assertThat(foreignExchangePage.verifyHKDTransactionDetail()).isTrue();
    }

    @Then("I click change button change to USD transaction")
    public void iClickChangeButtonChangeToUSDTransaction(Map<String ,String>data) {
        foreignExchangePage.changeToUSDTransaction(data.get("screenShotName"));
    }

//    @And("I verify USD transaction on process page")
//    public void iVerifyUSDTransactionOnProcessPage() {
//        assertThat(foreignExchangePage.verifyUSDTransaction()).isTrue();
//    }

    @And("I saw the USD order information")
    public void iSawUSDOrderInformation() {
        assertThat(foreignExchangePage.balanceUSD()).isTrue();
    }

    @Then("I verify USD transaction detail")
    public void iVerifyUSDTransactionDetail() {
        assertThat(foreignExchangePage.verifyUSDTransactionDetail()).isTrue();
    }

    @Then("I saw the minimum transaction amount tips")
    public void iSawTheMinimumTransactionAmountTips() {
        assertThat(foreignExchangePage.minimumTransactionAmountTips()).isTrue();
    }

    @Then("I check wrong amount tips")
    public void iCheckWrongAmountTips(Map<String, String> data) {
        assertThat(foreignExchangePage.checkWrongAmountTips(data.get("screenShotName"), data.get("screenShotName01"))).isTrue();
    }

    @Then("I verify Foreign Exchange Record")
    public void iVerifyForeignExchangeRecord(Map<String, String> data) {
        assertThat(foreignExchangePage.verifyForeignExchangeRecord(data.get("screenShotName"), data.get("screenShotName01"))).isTrue();
    }

    @Then("convert HKD to USD IOS")
    public void convertHKDtoUSDIOS(Map<String, String> data) {
        foreignExchangePage.convertHKDtoUSDIOS(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"),
                data.get("screenShotName4"));
    }

    @Then("convert USD to HKD IOS")
    public void convertUSDtoHKDIOS(Map<String, String> data) {
        foreignExchangePage.convertUSDtoHKDIOS(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("check Transation On Home Page")
    public void checkTransationOnHomePage(Map<String, String> data) {
        foreignExchangePage.checkTransationOnHomePage(data.get("screenShotName"), data.get("screenShotName1"));
    }

}
