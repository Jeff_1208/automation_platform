Feature:Foreign Exchange On WeLab app


  Background: Login with correct user and password
    Given Open WeLab App
    And wealth skip Root IOS
    And Login with user and password by params
      | user     | swmcust00165 |
      | password | Aa123321     |

  @VBWMIS-T702
  Scenario:Reg_Foreign_Exchange_001 FX convert HKD to USD IOS
    Then I click transfer goto transfer page
      | screenShotName | Reg_Foreign_Exchange_001_IOS_01 |
    And I goto the foreign exchange page
      | screenShotName | Reg_Foreign_Exchange_001_IOS_02 |
    And convert HKD to USD IOS
      | screenShotName  | Reg_Foreign_Exchange_001_IOS_03 |
      | screenShotName1 | Reg_Foreign_Exchange_001_IOS_04 |
      | screenShotName2 | Reg_Foreign_Exchange_001_IOS_05 |
      | screenShotName3 | Reg_Foreign_Exchange_001_IOS_06 |
      | screenShotName4 | Reg_Foreign_Exchange_001_IOS_07 |
    And check Transation On Home Page
      | screenShotName  | Reg_Foreign_Exchange_001_IOS_08 |
      | screenShotName1 | Reg_Foreign_Exchange_001_IOS_09 |

  @VBWMIS-T703
  Scenario:Reg_Foreign_Exchange_002 FX convert USD to HKD IOS
    Then I click transfer goto transfer page
      | screenShotName | Reg_Foreign_Exchange_002_IOS_01 |
    And I goto the foreign exchange page
      | screenShotName | Reg_Foreign_Exchange_002_IOS_02 |
    And convert USD to HKD IOS
      | screenShotName  | Reg_Foreign_Exchange_002_IOS_03 |
      | screenShotName1 | Reg_Foreign_Exchange_002_IOS_04 |
      | screenShotName2 | Reg_Foreign_Exchange_002_IOS_05 |
      | screenShotName3 | Reg_Foreign_Exchange_002_IOS_06 |
    And check Transation On Home Page
      | screenShotName  | Reg_Foreign_Exchange_002_IOS_07 |
      | screenShotName1 | Reg_Foreign_Exchange_002_IOS_08 |


