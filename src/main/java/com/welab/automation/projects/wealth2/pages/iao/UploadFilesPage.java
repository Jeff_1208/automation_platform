package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.welab.automation.framework.utils.ADB.adbDeleteFile;
import static com.welab.automation.framework.utils.ADB.adbPushFile;

public class UploadFilesPage extends AppiumBasePage {

  public static final String git_pdf = "sample.pdf";
  public static final String sdCardPictures = "/sdcard/Android/data/welab.bank.sit/files/Pictures/";
  public static final String sdCardCamera = "/sdcard/DCIM/Camera/";
  public static final String sdFilesPDF = "/sdcard/Android/data/welab.bank.sit/files/";
  public final String pageName = "upload files page";
  public static final String pic = "welab_pic.jpg";
  public static final String pdfFile = "sample.pdf";
  public static final String allow = "Allow";
  public static final String allowCH = "仅在使用中允许";
  public static final String alwaysAllow = "ALWAYS ALLOW";
  public static final int customWait = 60;
  public static final int waitAllow = 1;
  public static final String titleTxt = "Document upload"; // todo, traditional chinese

  @Getter private boolean isSkipped = false;

  @Getter
  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"PROCESS_GUIDE-btn\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement nextButton;

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
  private List<MobileElement> stepPromptElements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One more step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/child::*")
  private List<MobileElement> processGuideAfterStep1Group1Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One more step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep1Group2Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One more step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep1Group3Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Complete Customer Risk Profiling Questionnaire Please fill in your Customer Risk Profiling Questionnaire to help you understand your customer risk profile.']/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/child::*")
  private List<MobileElement> processGuideAfterStep12Group1Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Complete Customer Risk Profiling Questionnaire Please fill in your Customer Risk Profiling Questionnaire to help you understand your customer risk profile.']/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep12Group2Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Complete Customer Risk Profiling Questionnaire Please fill in your Customer Risk Profiling Questionnaire to help you understand your customer risk profile.']/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep12Group3Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One last step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/child::*")
  private List<MobileElement> processGuideAfterStep13Group1Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One last step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep13Group2Elements;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@label='Upload document(s) One last step to complete the investment account opening journey']/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/child::XCUIElementTypeOther")
  private List<MobileElement> processGuideAfterStep13Group3Elements;

  @Getter
  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther)[1]/XCUIElementTypeOther[2]")
  private MobileElement stepPromptElement1;

  @Getter
  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther)[2]/XCUIElementTypeOther[2]")
  private MobileElement stepPromptElement2;

  @Getter
  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther)[3]/XCUIElementTypeOther[2]")
  private MobileElement stepPromptElement3;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"PROCESS_GUIDE-title\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement documentUpLoadTitle;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Upload document (if required)']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='Upload document(s)']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='上載文件（如需要）']"),
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Upload document(s)'")
  private MobileElement documentUpLoadSecondTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Upload document (if required)']")
  private MobileElement documentUpLoadSecond;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"DOCUMENT_UPLOAD-btn-title\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Submit application'")
  private MobileElement submitApplication;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@content-desc=\"DOCUMENT_UPLOAD-card-address-desc\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'DOCUMENT_UPLOAD-card-address-desc'")
  private MobileElement addressProofTextButton;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Address proof\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Address proof'")
  private MobileElement addressProofTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Sample\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Sample'")
  private MobileElement sampleButton;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther//XCUIElementTypeOther")
  private List<MobileElement> plusSignButtons;

  @AndroidFindBy(
          xpath =
                  "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement plusBtn;

  @Getter
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.ImageView")
  private MobileElement imageStatus;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"git_tutorial.pdf\"]")
  private MobileElement documentStatus;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Done']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='完成']")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Done'")
  private MobileElement doneButton;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]") // [1]
  private MobileElement samplePageTitle;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text=\"Address proof\"]/following-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Sample']/../XCUIElementTypeOther")
  private MobileElement XiconSamplePage;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text=\"Take photo\"]"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='影相']")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Take photo'")
  private MobileElement takePhoto;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Photo library\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Photo library'")
  private MobileElement photoLibrary;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Document\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Document'")
  private MobileElement addDocument;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.Button[@text='Allow']"),
          @AndroidBy(xpath = "//android.widget.Button[@text='ALWAYS ALLOW']"),
          @AndroidBy(xpath = "//android.widget.Button[@text='始终允许']"),
          @AndroidBy(xpath = "//android.widget.Button[@text='仅在使用中允许']"),

  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
  private MobileElement allowButton;

  @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Allow all the time\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow all the time'")
  private MobileElement allowAllButton;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Next\"]")
  private MobileElement allowNextButton;

  @AndroidFindBy(xpath = "//*[@resource-id=\"Android.widget.Button\"]")
  private MobileElement cameraNextButton;

  @AndroidFindBy(
      xpath = "//android.widget.ImageView[@resource-id=\"com.android.camera2:id/shutter_button\"]")
  private MobileElement takePicture;

  @AndroidFindBy(
      xpath = "//android.widget.ImageButton[@resource-id=\"com.android.camera2:id/done_button\"]")
  private MobileElement takePictureDone;



  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"])[2]"),
          @AndroidBy(xpath = "//*[@text='Done']")
  })
  private MobileElement DoenBtn;


  @AndroidFindBy(
      xpath =
          "//android.widget.ImageView[@resource-id=\"resourcecom.android.documentsui:id/app_icon\"]")
  private MobileElement choosePhotos;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeCell")
  private List<MobileElement> photos;

  @AndroidFindBy(
      xpath = "//android.view.View[@resource-id=\"com.google.android.apps.photos:id/image\"]")
  private MobileElement chooseCameraFolder;

  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Recents'")
  private MobileElement recentPhotoLib;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Document\"]")
  private MobileElement chooseDocument;

  @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Show roots\"]")
  private MobileElement androidHomeHamburger;

  @AndroidFindBy(xpath = "//android.widget.ImageButton[@text=\"Photos\"]")
  private MobileElement androidPhotoIcon;

  @AndroidFindBy(xpath = "//android.widget.ImageButton[@text=\"Pictures\"]")
  private MobileElement androidLibraryPictureIcon;

  @AndroidFindBy(
      xpath = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup[@index=1]")
  private MobileElement chooseAPicture;

  @AndroidFindBy(xpath = "//android.widget.ImageButton[@text=\"Done\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Done'")
  private MobileElement chooseLibraryDone;

  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Please select file to upload'")
  private MobileElement warning;

  @iOSXCUITFindBy(
      iOSNsPredicate = "name == 'Some of photos are uploading, please wait for uploaded'")
  private MobileElement uploading;

  @iOSXCUITFindBy(iOSNsPredicate = "name == 'OK'")
  private MobileElement ok;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"BROWSE FILES IN OTHER APPS\"]")
  private MobileElement recentIcon;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"git_tutorial.pdf\"]")
  private MobileElement pdfFileIcon;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Android SDK built for x86\"]")
  private MobileElement androidSDK;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Android\"]")
  private MobileElement androidFolder;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Data\"]")
  private MobileElement dataFolder;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"welab.bank.sit\"]")
  private MobileElement welabFolder;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"files\"]")
  private MobileElement filesFolder;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"pdf\")]")
  private List<MobileElement> fdfFiles;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc='拍照']"),
          @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc='Capture']"),
          @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"拍照\"]/android.widget.ImageView"),
  })
  private MobileElement photo;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc='Done']"),
          @AndroidBy(xpath = "//android.widget.Button[@content-desc=\"确定\"]/android.view.ViewGroup/android.widget.TextView")
  })
  private MobileElement photoDone;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc='DOCUMENT_UPLOAD-card-income-title']"),
          @AndroidBy(xpath = "//*[@text='Consent from your employer']"),
  })
  private MobileElement licensedStaffConsentLetter;

  public UploadFilesPage() {
    super.pageName = pageName;
  }

  @SneakyThrows
  public boolean getUploadDocumentTitle() {
    Thread.sleep(5000);
    return verifyElementExist(documentUpLoadSecondTitle);

  }

  // onboarding: D_01a_transition
  //    public void clickUploadNext(){
  //        waitUntilElementClickable(pageName,nextButton);
  //        nextButton.click();
  //    }

  // onboarding: D_01a_transition
  // upload document starts from D_01a_transition
  @SneakyThrows
  public boolean startDocumentUpload() {
    // check "Document upload" title firstly in D_01a_transition, then click next button.
    // if click next button firstly, it may click the next button in previous page D_01_transition
    Thread.sleep(4000);
    String titleTxtStr = "Upload document (if required)";
    String titleTxtCH = "上載文件（如需要）"; //
    if (documentUpLoadTitle.getText().equals(titleTxtStr) ||documentUpLoadTitle.getText().equals(titleTxtCH)){
      clickElement(nextButton);
      return true;
    }
    return false;
  }

  // onboarding: D_01a_transition
  // upload document starts from D_01a_transition
  public boolean verifyDocumentUpload() {
    waitUntilElementVisible(nextButton);
    String titleTxt = "Upload document(s)"; // todo, traditional chinese
    if (isElementDisplayed(documentUpLoadTitle, titleTxt)) {
      return true;
    }
    return false;
  }

  // onboarding: E_01_upload/E_01a_upload/E_01b_upload to F_03_transition
  // click submit application without uploading any document will skip it.
  public boolean skipDocumentUpload() {
    waitUntilElementClickable(submitApplication);
    clickElement(submitApplication);
    String titleTxt = "Upload documents(s) later";

    if (isElementDisplayed(documentUpLoadTitle, titleTxt)) {
      clickElement(nextButton);
      isSkipped = true;
      return true;
    }
    return false;
  }

  public void clickAddressProof() {
    waitUntilElementClickable(addressProofTextButton);
    addressProofTextButton.click();
  }

  public String getAddressProofText() {
    return addressProofTitle.getText();
  }

  public void clickSampleButton() {
    sampleButton.click();
  }

  @SneakyThrows
  public String getSamplePageTitle() {
    Thread.sleep(3*1000);
    String sampleTitle = samplePageTitle.getText();
    swipeRightToLeft();
    return sampleTitle;
  }

  @SneakyThrows
  public boolean verifySamplePageTitle(String title) {
    Thread.sleep(1000 * 2);
    MobileElement me =
        (MobileElement) findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + title + "']"));
    boolean isDisplayed = me.isDisplayed();
    swipeRightToLeft();
    return isDisplayed;
  }

  public void clickXicon() {
    XiconSamplePage.click();
  }


  @SneakyThrows
  public void clickPicture(String screenShotName){
    Thread.sleep(2000);
    clickElement(photo);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
    clickElement(photoDone);
    Thread.sleep(10000);
  }
  // onboarding: E_06_camera to E_07_upload
  public void takePhotoDone(String screenShotName) {
    takeScreenshot(screenShotName);
    waitUntilElementClickable(doneButton);
    clickElement(doneButton);
  }

  // onboarding: E_07_upload to F_02_transition
  public void submitApplication() {
    waitUntilElementClickable(submitApplication);
    clickElement(submitApplication);
  }

  // onboarding: F_02_transition to F_01c_transition
  // CRPQ will start from F_01c_transition
  @SneakyThrows
  public boolean finishUploadDocument(String para) {
    String checkTxt = "uploaded successfully";
    if (!isElementDisplayed(documentUpLoadTitle, checkTxt)) {
      return false;
    }
    clickElement(nextButton);
    // if already finished CRPQ, click next button will direct to wealth main page.
    if (para != null && para.equalsIgnoreCase("crpq done")) {
      return true;
    }
    Thread.sleep(1000 * 3);
    checkTxt = "Customer Risk Profiling Questionnaire";
    return isElementDisplayed(documentUpLoadTitle, checkTxt);
  }

  @SneakyThrows
  public boolean finishUploadDocument2() {
    boolean flag = false;
    Thread.sleep(5000);
    String checkTxt ="Customer Risk Profiling Questionnaire";
    String checkTxtCH ="客戶風險分析問卷";
    if(documentUpLoadTitle.getText().contains(checkTxt) || documentUpLoadTitle.getText().equals(checkTxtCH)){
      flag = true;
    }
    return flag;
  }

  public Boolean checkUpLoadStatus(MobileElement element) {
    Boolean uploadStatus = isElementVisible(element, customWait);
    return uploadStatus;
  }

  //    public Boolean checkUpLoadDocument() {
  //        if(isElementVisible(documentStatus, documentWait))
  //            return true;
  //        else return false;
  //    }

  private void clickPlusButton() throws InterruptedException {
//    MobileElement plusSignButton = getPlusIcon();
//    waitUntilElementClickable(plusSignButton);
//    plusSignButton.click();

    clickElement(plusBtn);
    Thread.sleep(1000 * 3);
  }

  @SneakyThrows
  public void clickLicensedStaffConsentLetter(String screenShotName) {
    takeScreenshot(screenShotName);
    clickElement(licensedStaffConsentLetter);
    Thread.sleep(1000 * 3);
  }
  @SneakyThrows
  public void upLoadPhotoLibrary() throws InterruptedException {
    boolean isUploaded = false;
    int i = 0;
    while (!isUploaded) {
      clickPlusButton();
      waitUntilElementClickable(photoLibrary);
      photoLibrary.click();
      if (System.getProperty("mobile").contains("android")) {
        // allow using camera
        //    if (isElementDisplayed(allowButton, allow, waitAllow)) allowButton.click();
        // all welab to use camera
        //    if (isElementDisplayed(allowButton, allow, waitAllow)) allowButton.click();
        androidPhotoIcon.click();
        //        takePicture.click();
        //        chooseCameraFolder.click();
        //        chooseAPicture.click();
        androidHomeHamburger.click();
        androidPhotoIcon.click();
        androidLibraryPictureIcon.click();
        chooseAPicture.click();
      }
      if (System.getProperty("mobile").contains("ios")) {
        recentPhotoLib.click();
        if (photos.size() > 0) {
          photos.get(0).click();
          chooseLibraryDone.click();
          Thread.sleep(1000 * 10);
          doneButton.click();
          try {
            uploading.isDisplayed();
            ok.click();
            Thread.sleep(1000 * 10);
            doneButton.click();
          } catch (NoSuchElementException ex) {

          }
          try {
            warning.isDisplayed();
            ok.click();
            continue;
          } catch (NoSuchElementException ex) {
            isUploaded = true;
          }
          if (i++ == 5) throw new Exception("Can not upload photo after 5 times.");
        } else {
          throw new Exception("No recent photo can be selected.");
        }
      }
    }
  }

  public void chooseADocument() {
    chooseDocument.click();
    androidHomeHamburger.click();
    androidSDK.click();
    androidFolder.click();
    dataFolder.click();
    welabFolder.click();
    filesFolder.click();
    clickPDFfiles();
  }

  public void clickPDFfiles() {
    for (int i = 0; i < fdfFiles.size(); i++) {
      WebElement file = fdfFiles.get(i);
      file.click();
    }
  }

  public void cleanAndroidFiles() {
    String pictures = sdCardPictures + "*";
    String pdfs = sdFilesPDF + "*.pdf";
    String cameras = sdCardCamera + "*";
    adbDeleteFile(pictures);
    adbDeleteFile(cameras);
    adbDeleteFile(pdfs);
  }

  public void pushPictures() {
    //        String picturePath = sdCardCamera + pic;
    String picturePath = sdCardPictures + pic;
    Path picture = Paths.get(GlobalVar.APP_TEST_DATA_PATH, pic);
    adbPushFile(picture.toString(), picturePath);
  }

  public void pushPDF() {
    String pdfPath = sdFilesPDF + pdfFile;
    Path pdf = Paths.get(GlobalVar.APP_TEST_DATA_PATH, pdfFile);
    adbPushFile(pdf.toString(), pdfPath);
  }

  private MobileElement getPlusIcon() throws InterruptedException {
    //        if(waitUntilElementsVisible(pageName,plusSignButtons));
    Thread.sleep(2000);
    int size = plusSignButtons.size();
    return plusSignButtons.get(size - 1);
  }

  //    private MobileElement getUploadingIcon() throws InterruptedException {
  //        if(waitUntilElementsVisible(pageName,plusSignButtons));
  /*        Thread.sleep(2000);
      int size = plusSignButtons.size();
      return plusSignButtons.get(size - 2);
  }*/

  public void uploadDocument() throws InterruptedException {
    clickPlusButton();
    waitUntilElementClickable(photoLibrary);
    addDocument.click();
    // allow using camera
    if (isElementDisplayed(allowButton, allow, waitAllow)) allowButton.click();
    // all welab to use camera
    if (isElementDisplayed(allowButton, allow, waitAllow)) allowButton.click();
    if (recentIcon.isDisplayed()) {
      waitUntilElementVisible(pdfFileIcon, waitAllow);
      pdfFileIcon.click();
    }
    androidHomeHamburger.click();
    waitUntilElementVisible(recentIcon);
    recentIcon.click();
    waitUntilElementVisible(pdfFileIcon);
    pdfFileIcon.click();
  }

  @SneakyThrows
  public void clickPlusBtn(String screenShotName) {
    takeScreenshot(screenShotName);
    clickElement(plusBtn);
    Thread.sleep(1000 * 3);
  }

  @SneakyThrows
  public void takePhotoWithScreenShotName(String screenShotName,String screenShotName01){
    takeScreenshot(screenShotName);
    clickElement(takePhoto);
    if(isShow(allowButton,2)){
      clickElement(allowButton);
    }
    clickPicture(screenShotName01);
    Thread.sleep(10000);
  }

  public void clickSubmitApplication(String screenShotName) {
    waitUntilElementClickable(submitApplication);
    takeScreenshot(screenShotName);
    clickElement(submitApplication);
  }
}
