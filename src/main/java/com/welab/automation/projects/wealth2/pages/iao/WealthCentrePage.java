package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WealthCentrePage extends AppiumBasePage {

    private final Logger logger = LoggerFactory.getLogger(CRPQPage.class);
    public String pageName = "Wealth Centre Page";

    public WealthCentrePage() {
        super.pageName = pageName;
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Total balance (HKD)']/../../../" +
                    "preceding-sibling::android.view.ViewGroup/android.view.ViewGroup[4]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '總結餘 (HKD)']/../../../" +
                    "preceding-sibling::android.view.ViewGroup/android.view.ViewGroup[4]")
    })
    private MobileElement myAccounts;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Wealth Centre']/.."),
    })
    private MobileElement wealthCenterItem;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Transaction']"),
    })
    private MobileElement Transaction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'View record']"),
    })
    private MobileElement viewRecord;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'Can you tell me why')]"),
            @AndroidBy(xpath = "//*[contains(@text,'How do you see risk-return')]"),
            @AndroidBy(xpath = "//*[@text = 'Can you tell me why you're investing with us?']"),
            @AndroidBy(xpath = "//*[@text = 'How do you see risk-return? I'm willing to take']"),
            @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"CRPQ_RESULT_REVIEW-question-title-RAI01\"]"),
    })
    private MobileElement recordDetailPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@content-desc = 'btnClose']"),
    })
    private MobileElement closeBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Order status']"),
    })
    private MobileElement orderStatus;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'All']"),
    })
    private MobileElement orderStatusAllItem;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Orders are sorted from newest to oldest.']/" +
                    "following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
    })
    private MobileElement firstOrderOpenBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "(//android.view.ViewGroup[@content-desc=\"-btn\"])[1]/" +
                    "android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text = 'Orders are sorted from newest to oldest.']/" +
                    "following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]"),
    })
    private MobileElement standingInstructionFirstItemOpenButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Orders are sorted from newest to oldest.']"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'Orders are sorted from newest to oldest.')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'指示由新至舊順序排列')]"),
    })
    private MobileElement orderSortMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Standing Instruction']"),
    })
    private MobileElement standingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Product Key Facts']"),
    })
    private MobileElement productKeyFacts;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@content-desc = 'btnBackContainer']"),
    })
    private MobileElement backBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Review customer risk profile']"),
    })
    private MobileElement reviewCustomerRiskProfile;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Your Customer Risk Rating is:']"),
            @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"UPDATE_CRPQ_RISK_RESULT-title\"]"),
    })
    private MobileElement riskProfileDetail;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Tutorial']"),
    })
    private MobileElement tutorial;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.View[@content-desc=\"WeLab Bank 講呢啲\"]"),
    })
    private MobileElement tutorialPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Review investment account profile']"),
    })
    private MobileElement reviewInvestmentAccountProfile;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Investment account profile']"),
    })
    private MobileElement investmentAccountProfilePage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Promotion']"),
    })
    private MobileElement promotion;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'GoWealth Digital Wealth Advisory']"),
    })
    private MobileElement goWealthDigitalWealthAdvisory;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Prospectus']"),
    })
    private MobileElement orderStatusProspectus;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'GoWealth Digital Wealth Advisory']/../../../../../../../" +
                    "preceding-sibling::android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement promotionBackButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"UPDATE_CRPQ_RISK_RESULT-btn-title\"]"),
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"UPDATE_CRPQ_RISK_RESULT-btn\"]"),
            @AndroidBy(xpath = "//*[@text = 'Update Customer Risk Profile']"),
    })
    private MobileElement UpdateCustomerRiskProfileBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Update My Risk Profile']"),
    })
    private MobileElement UpdateMyRiskProfilePageTitle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text = 'Can you tell me why you're investing with us?']"),
            @AndroidBy(xpath = "//*[contains(@text,'Can you tell me why')]"),
            @AndroidBy(xpath = "//*[@text = '//android.widget.TextView[@content-desc=\"UPDATE_CRPQ-txt-desc\"]']"),
    })
    private MobileElement updateMyRiskProfileMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.Button[@content-desc=\"Review customer risk profile, back\"]"),
            @AndroidBy(xpath = "//android.widget.Button[@resource-id=\"header-back\"]"),
    })
    private MobileElement myRiskProfileBackButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.View[@content-desc=\"WeLab Bank 講呢啲\"]")
    })
    private MobileElement welabBank;

    @SneakyThrows
    public void goToMyAccount() {
        Thread.sleep(5000);
        waitUntilElementVisible(myAccounts);
        clickElement(myAccounts);
        waitUntilElementVisible(wealthCenterItem);
    }

    @SneakyThrows
    public void goToWealthCenter(String screenShotName) {
        clickElement(wealthCenterItem);
        waitUntilElementVisible(Transaction);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openOrderStatus(String screenShotName) {
        clickElement(orderStatus);
        waitUntilElementVisible(orderStatusAllItem);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openFirstOrderDetail(String screenShotName) {
        Thread.sleep(3000);
        waitUntilElementVisible(firstOrderOpenBtn);
        clickElement(firstOrderOpenBtn);
        waitUntilElementVisible(productKeyFacts);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openProspectusDocument(String screenShotName) {
        clickElement(orderStatusProspectus);
        Thread.sleep(25*1000); //wait to open pdf
        waitUntilElementVisible(orderStatusProspectus);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openOrderStatusStandingInstruction(String screenShotName){
        clickElement(standingInstruction);
        waitUntilElementVisible(standingInstructionFirstItemOpenButton);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkOrderStatusStandingInstruction(String screenShotName){
        clickElement(standingInstructionFirstItemOpenButton);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openViewRecord(String screenShotName){
        clickElement(viewRecord);
        waitUntilElementVisible(recordDetailPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkViewRecordDetail(String screenShotName, String screenShotName1){
        swipeRightToLeft();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        swipeRightToLeft();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        swipeLeftToRight();
    }

    @SneakyThrows
    public void toWealthCentrePage(String screenShotName){
        waitUntilElementVisible(Transaction);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickCloseButton(){
        clickElement(closeBtn);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void clickBackButton(){
        clickElement(backBtn);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void clickPromotionBackButton(){
        clickElement(promotionBackButton);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void openReviewCustomerRiskProfile(String screenShotName){
        clickElement(reviewCustomerRiskProfile);
        waitUntilElementVisible(riskProfileDetail);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void toReviewCustomerRiskProfile(String screenShotName){
        waitUntilElementVisible(riskProfileDetail);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openWealthCenterTutorial(String screenShotName, String screenShotName1){
        clickElement(tutorial);
        waitUntilElementVisible(welabBank);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        Thread.sleep(10*1000);
        takeScreenshot(screenShotName1);
        clickByLocation(60,150);
    }

    @SneakyThrows
    public void openReviewInvestmentAccountProfile(String screenShotName){
        clickElement(reviewInvestmentAccountProfile);
        waitUntilElementVisible(investmentAccountProfilePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void goToUpdateCustomerRiskProfilePage(String screenShotName){
        clickElement(UpdateCustomerRiskProfileBtn);
        waitUntilElementVisible(updateMyRiskProfileMessage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickMyRiskProfileBackButton(){
        clickElement(myRiskProfileBackButton);
    }

    @SneakyThrows
    public void openWealthCenterPromotion(String screenShotName) {
        clickElement(promotion);
        waitUntilElementVisible(goWealthDigitalWealthAdvisory);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkWealthCenterPromotionDetail(String screenShotName, String screenShotName1) {
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        scrollDown();
        scrollDown();
        scrollDown();
        scrollDown();
        Thread.sleep(1000);
    }

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Root/Jailbreak detection\"])[2]")
    private MobileElement skipRoot;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'OCR')]/../android.widget.Switch")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过OCR扫描\"])[2]")
    private MobileElement skipORC;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Rebind Device')]/../android.widget.Switch")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Rebind Device\"])[2]")
    private MobileElement skipRebindDevice;

    @iOSXCUITFindBy(xpath = "//*[@name=\"是否跳过Rebind Device Authentication\"]")
    private MobileElement skipRebindDeviceAuthentication;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Total balance')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
    })
    private MobileElement totalBalance;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"以后\"]"),
    })
    private MobileElement laterButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text ,'WeLab Bank 都會為大家實現')]")
    })
    private MobileElement aboutUsPageTips;

    @iOSXCUITFindBy(xpath = "//*[contains(@text ,'WeLab Bank 都會為大家實現')]")
    private MobileElement test1;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'理財服務中心')]")
    })
    private MobileElement myAccountPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'交易指示狀態')]")
    })
    private MobileElement wealthCentrePage;

    @SneakyThrows
    public void skipRootIos() throws InterruptedException {
        clickByPicture2("src/main/resources/images/skip/eye.png",50,50);
        Thread.sleep(2000);
        clickByPicture2("src/main/resources/images/skip/rocket.png",50,50);
        Thread.sleep(5000);
        scrollUp();
        scrollUp();
        scrollUp();
        Thread.sleep(2000);
        scrollByLocation(88,99,getElementCenterY(skipRebindDevice));
        Thread.sleep(1000);
        clickByLocationByPercent(9,6);
    }

    public void clickMyAccountBtn(){
        clickByLocationByPercent(93,7);
    }

    @SneakyThrows
    public void goToMyAccountPageIOS(String screenShotName){
        if(isShow(laterButton,3)){
            clickElement(laterButton);
        }
        waitUntilElementVisible(totalBalance);
        Thread.sleep(1000);
        clickMyAccountBtn();
        waitUntilElementVisible(myAccountPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    public void clickWealthCenterBtn(){
        if(isIphone13()){
            clickByLocationByPercent(20, 54);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/wealthCentre.png", 50, 50);
        }
    }

    @SneakyThrows
    public void goToWealthCenterPageIOS(String screenShotName){
        clickWealthCenterBtn();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    public void clickBackIOS(){
        clickByLocationByPercent(6,6);
    }

    public void clickCloseIOS(){
        clickByLocationByPercent(94,6);
    }
    public void clickOrderStatus(){
        if(isIphone13()){
            clickByLocationByPercent(20, 21);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/orderStatus.png", 50, 50);
        }
    }

    public void clickFirstOrder(){
        clickByLocationByPercent(50, 58);
    }

    public void clickOrderStatusFirstPdf(){
        clickByLocationByPercent(50, 25);
    }

    public void clickOrderStatusStandingInstruction(){
        clickByLocationByPercent(75, 13);
    }

    @SneakyThrows
    public void chekcOrderStatusIOS(String screenShotName, String screenShotName1, String screenShotName2){
        clickOrderStatus();
        waitUntilElementVisible(orderSortMessage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);

        clickFirstOrder();
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);

        clickOrderStatusFirstPdf();
        Thread.sleep(20*1000);
        takeScreenshot(screenShotName2);
        clickBackIOS();
        Thread.sleep(1000);
        clickBackIOS();
        Thread.sleep(1000);
    }
    @SneakyThrows
    public void chekcOrderStatusStandingInstructionIOS(String screenShotName, String screenShotName1){
         clickOrderStatusStandingInstruction();
        Thread.sleep(8000);
        takeScreenshot(screenShotName);
        clickFirstOrder();
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
    }

    public void clickCustomerRiskProfile(){
        if(isIphone13()){
            clickByLocationByPercent(20, 36);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/customRisk.png", 50, 50);
        }
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "//*[contains(@name ,'label == \"你嘅客戶風險評級：\"')]"),
            @iOSXCUITBy(xpath = "//*[contains(@value ,'你嘅客戶風險評級')]")
    })
    private MobileElement reviewCustomerRiskProfilePage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"更新客戶風險狀況\" AND name == \"UPDATE_CRPQ_RISK_RESULT-btn\""),
            @iOSXCUITBy(xpath = "//*[contains(@value ,'更新客戶風險狀況')]")
    })
    private MobileElement updateCustomerRiskButton;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"想請問一下你的投資目標？\" AND name == \"UPDATE_CRPQ-txt-desc\""),
            @iOSXCUITBy(xpath = "//*[contains(@value ,'想請問一下你的投資目標')]")
    })
    private MobileElement myRiskProfilePage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'想請問一下你的投資目標')]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"查閱填寫記錄\"]"),
    })
    private MobileElement viewRecordPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'')]"),
    })
    private MobileElement TutorialPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"投資知識及經驗\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"資歷\""),
    })
    private MobileElement reviewInvestmentAccountPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name=\"GoWealth 智動投資顧問\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"資歷\""),
    })
    private MobileElement promotionPage;

    @SneakyThrows
    public void checkReviewCustomerRiskProfileIOS(String screenShotName, String screenShotName1,
          String screenShotName2, String screenShotName3, String screenShotName4){

        clickCustomerRiskProfile();
        waitUntilElementVisible(reviewCustomerRiskProfilePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        clickElement(updateCustomerRiskButton);
        waitUntilElementVisible(myRiskProfilePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);

        clickCloseIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);

        clickCustomerRiskProfile();
        //waitUntilElementVisible(reviewCustomerRiskProfilePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);

        clickBackIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName4);
    }

    public void clickViewRecord(){
        if(isIphone13()){
            clickByLocationByPercent(20, 44);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/viewRecord.png", 50, 50);
        }
    }

    @SneakyThrows
    public void checkViewRecordIOS(String screenShotName, String screenShotName1,
                                   String screenShotName2, String screenShotName3){

        clickViewRecord();
        waitUntilElementVisible(viewRecordPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        swipeRightToLeft();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        swipeRightToLeft();
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        swipeLeftToRight();

        clickCloseIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
    }

    public void clickTutorial(){
        if(isIphone13()){
            clickByLocationByPercent(20, 59);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/Tutorial.png", 50, 50);
        }
    }

    public void clickAccountProfile(){
        if(isIphone13()){
            clickByLocationByPercent(20, 66);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/accountProfile.png", 50, 50);
        }
    }

    public void clickPromotion(){
        if(isIphone13()){
            clickByLocationByPercent(20, 74);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/promotion.png", 50, 50);
        }
    }

    @SneakyThrows
    public void checkWealthCenterTutorialIOS(String screenShotName, String screenShotName1) {

        clickTutorial();
        waitUntilElementVisible(TutorialPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        Thread.sleep(30*1000);
        takeScreenshot(screenShotName1);
    }

    @SneakyThrows
    public void checkReviewInvestmentAccountProfileIOS(String screenShotName, String screenShotName1,
                                                       String screenShotName2, String screenShotName3) {

        clickAccountProfile();
        waitUntilElementVisible(reviewInvestmentAccountPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        clickCloseIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);

        clickAccountProfile();
        waitUntilElementVisible(reviewInvestmentAccountPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);

        clickBackIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void checkWealthCenterPromotionIOS(String screenShotName, String screenShotName1,
                 String screenShotName2, String screenShotName3) {

        clickPromotion();
        waitUntilElementVisible(promotionPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);

        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);

        scrollDown();
        scrollDown();
        scrollDown();
        scrollDown();
        Thread.sleep(1000);

        clickBackIOS();
        waitUntilElementVisible(wealthCentrePage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);

    }
}
