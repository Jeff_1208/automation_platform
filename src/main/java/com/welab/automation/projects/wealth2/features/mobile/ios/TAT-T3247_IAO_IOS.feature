Feature: Update CRPQ

  Background: Login
    Given Open WeLab App
    And wealth skip Root IOS

  @VBWMIS-T680
  Scenario: Reg_Wealth_IAO_001 ETB_IAO NST IOS
    And Login with user and password by params
      | user     | contester0043 |
      | password | Aa123321      |
    And I goto wealth page ios
      | screenShotName | Reg_IAO_001_IOS_01 |
    Then Open Wealth Account IOS
    Then click next Confirm information Page IOS
      | screenShotName  | Reg_IAO_001_IOS_02 |
      | screenShotName1 | Reg_IAO_001_IOS_03 |
      | screenShotName2 | Reg_IAO_001_IOS_04 |
    Then answer Questions IOS
      | screenShotName  | Reg_IAO_001_IOS_05 |
      | screenShotName1 | Reg_IAO_001_IOS_06 |
      | screenShotName2 | Reg_IAO_001_IOS_07 |
      | screenShotName3 | Reg_IAO_001_IOS_08 |
    Then I select no skip item IOS
      | screenShotName | Reg_IAO_001_IOS_09 |
    Then confirm application Information steps
      | screenShotName  | Reg_IAO_001_IOS_11 |
      | screenShotName1 | Reg_IAO_001_IOS_12 |
      | screenShotName2 | Reg_IAO_001_IOS_13 |
      | screenShotName3 | Reg_IAO_001_IOS_14 |
      | screenShotName4 | Reg_IAO_001_IOS_15 |
    Then fill Customer Risk Profiling Questions IOS
      | screenShotName  | Reg_IAO_001_IOS_16 |
      | screenShotName1 | Reg_IAO_001_IOS_17 |
      | screenShotName2 | Reg_IAO_001_IOS_18 |
      | screenShotName3 | Reg_IAO_001_IOS_19 |
      | screenShotName4 | Reg_IAO_001_IOS_20 |
      | screenShotName5 | Reg_IAO_001_IOS_21 |
      | screenShotName6 | Reg_IAO_001_IOS_22 |
    Then confirm Risk Detail IOS
      | screenShotName  | Reg_IAO_001_IOS_23 |
      | screenShotName1 | Reg_IAO_001_IOS_24 |
    Then check Application Result IOS
      | screenShotName | Reg_IAO_001_IOS_25 |

  @VBWMIS-T679
  Scenario: Reg_Wealth_IAO_002 ETB_IAO ST IOS
    And Login with user and password by params
      | user     | contester0041 |
      | password | Aa123321      |
    And I goto wealth page ios
      | screenShotName | Reg_IAO_002_IOS_01 |
    Then Open Wealth Account IOS
    Then click next Confirm information Page IOS
      | screenShotName  | Reg_IAO_002_IOS_02 |
      | screenShotName1 | Reg_IAO_002_IOS_03 |
      | screenShotName2 | Reg_IAO_002_IOS_04 |
    Then answer Questions IOS
      | screenShotName  | Reg_IAO_002_IOS_05 |
      | screenShotName1 | Reg_IAO_002_IOS_06 |
      | screenShotName2 | Reg_IAO_002_IOS_07 |
      | screenShotName3 | Reg_IAO_002_IOS_08 |
    Then I select Yes Consent Letter From Your Employer
      | screenShotName | Reg_IAO_002_IOS_09 |
    Then confirm application Information and upload picture
      | screenShotName  | Reg_IAO_002_IOS_11 |
      | screenShotName1 | Reg_IAO_002_IOS_12 |
      | screenShotName2 | Reg_IAO_002_IOS_13 |
      | screenShotName3 | Reg_IAO_002_IOS_14 |
      | screenShotName4 | Reg_IAO_002_IOS_15 |
    Then fill Customer Risk Profiling Questions IOS
      | screenShotName  | Reg_IAO_002_IOS_16 |
      | screenShotName1 | Reg_IAO_002_IOS_17 |
      | screenShotName2 | Reg_IAO_002_IOS_18 |
      | screenShotName3 | Reg_IAO_002_IOS_19 |
      | screenShotName4 | Reg_IAO_002_IOS_20 |
      | screenShotName5 | Reg_IAO_002_IOS_21 |
      | screenShotName6 | Reg_IAO_002_IOS_22 |
    Then confirm Risk Detail IOS
      | screenShotName  | Reg_IAO_002_IOS_23 |
      | screenShotName1 | Reg_IAO_002_IOS_24 |
    Then check Application Result IOS
      | screenShotName | Reg_IAO_002_IOS_25 |



