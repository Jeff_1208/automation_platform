package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.iao.UploadFilesPage;
import io.appium.java_client.MobileElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadFileStepsDef {

  UploadFilesPage uploadFilesPage;
 public UploadFileStepsDef(){
   uploadFilesPage = new UploadFilesPage();

 }


  private static final String document_upload_if_required = "Upload document (if required)";
  private static final String document_upload = "Upload document(s)";
  private static final String address_proof = "Address proof";
  private static final String[] sample_title = {
    "Monthly bank statement sample", "Utility bill sample", "Phone bills sample"
  };

  @And("I click next on document transition page")
  public void iClickNextOnDocumentTransitionPage() {
    uploadFilesPage.clickElement(uploadFilesPage.getNextButton());
  }

  @And("I can see document transition page")
  public void iCanSeeDocumentTransitionPage() {
    assertThat(uploadFilesPage.verifyDocumentUpload()).isTrue();
  }

  @And("I can see the upload files page")
  public void iCanSeeTheUploadFilesPage() throws InterruptedException {
    //    commonPage.gotoWealth();
    // check "Document upload" title firstly in D_01a_transition, then click next button.
    // if click next button firstly in D_01a_transition, it may click the next button in previous
    // page D_01a_transition
    boolean result = uploadFilesPage.startDocumentUpload();
    assertThat(result).isTrue();
//    boolean uploadDocument = uploadFilesPage.getUploadDocumentTitle();
//    assertThat(uploadDocument).isTrue();
  }

  @And("I review the sample files")
  public void iReviewTheSampleFiles() {
//    uploadFilesPage.clickAddressProof();
//    String actualAddressProof = uploadFilesPage.getAddressProofText();
//    assertThat(actualAddressProof).isEqualTo(address_proof);
    uploadFilesPage.clickSampleButton();
    for (int i = 0; i < sample_title.length; i++) {
      if (System.getProperty("mobile").contains("android")) {
        String sampleTitle = uploadFilesPage.getSamplePageTitle();
        assertThat(sampleTitle).isEqualTo(sample_title[i]);
      }
      if (System.getProperty("mobile").contains("ios")) {
        assertThat(uploadFilesPage.verifySamplePageTitle(sample_title[i]))
            .as("check sample page title %s", sample_title[i])
            .isTrue();
      }
    }
    uploadFilesPage.clickXicon();
  }

  @And("I go to address proof page")
  public void iGoToAddressProofPage() {
    uploadFilesPage.clickAddressProof();
  }
//
//  @And("I take photo to upload")
//  public void iTakePhotoToUpload() throws InterruptedException {
//    String actualAddressProof = uploadFilesPage.getAddressProofText();
//    assertThat(actualAddressProof).isEqualTo(address_proof);
//    uploadFilesPage.clickTakePhoto();
//
//    MobileElement imageStatus = uploadFilesPage.getImageStatus();
//    Boolean uploadResult = uploadFilesPage.checkUpLoadStatus(imageStatus);
//    assertThat(uploadResult).isTrue();
//
//    uploadFilesPage.takePhotoDone("screenShotName");
//    uploadFilesPage.submitApplication();
//  }

  @And("I add document in my smartphone")
  public void iAddDocumentInMySmartphone() throws InterruptedException {
    String actualAddressProof = uploadFilesPage.getAddressProofText();
    assertThat(actualAddressProof).isEqualTo(address_proof);
    uploadFilesPage.uploadDocument();
    MobileElement documentStatus = uploadFilesPage.getDocumentStatus();
    Boolean uploadResult = uploadFilesPage.checkUpLoadStatus(documentStatus);
    assertThat(uploadResult).isTrue();
  }

  @And("I add photo from photo library")
  public void iAddPhotoFromPhotoLibrary() throws InterruptedException {
    String actualAddressProof = uploadFilesPage.getAddressProofText();
    assertThat(actualAddressProof).isEqualTo(address_proof);
    uploadFilesPage.upLoadPhotoLibrary();
    //    Thread.sleep(3000);
    uploadFilesPage.submitApplication();
  }

  @And("I add photo from photo library without submit")
  public void iAddPhotoFromPhotoLibrary2() throws InterruptedException {
    String actualAddressProof = uploadFilesPage.getAddressProofText();
    assertThat(actualAddressProof).isEqualTo(address_proof);
    uploadFilesPage.upLoadPhotoLibrary();
    //    Thread.sleep(3000);
  }

  @Then("I finish upload document")
  public void iFinishUploadDocument() {
    boolean result = uploadFilesPage.finishUploadDocument2();
    assertThat(result).isTrue();
  }

  // todo, only one iFinishUploadDocument can cover both crpq already done and crpq not done step.
  @Then("^I finish upload document with (.*)")
  public void iFinishUploadDocument(String para) {
    boolean result = uploadFilesPage.finishUploadDocument(para);
    assertThat(result).isTrue();
  }

  @And("I Skip Upload Document")
  public void skipUploadDocument() {
    boolean result = uploadFilesPage.startDocumentUpload();
    assertThat(result).isTrue();
    result = uploadFilesPage.skipDocumentUpload();
    assertThat(result).isTrue();
  }

  @And("I prepare to upload pictures and documents")
  public void iPrepareToUploadPicturesAndDocuments() {
    uploadFilesPage.cleanAndroidFiles();
    uploadFilesPage.pushPictures();
    uploadFilesPage.pushPDF();

        //  adb shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d
        // file:///sdcard/DCIM/Camera/welab_pic.jpg
    }

    @And("I submit application")
    public void iSubmitApplication() {
        uploadFilesPage.submitApplication();
    }


    @And("I take photo to upload with screenShot")
    public void iTakePhotoToUploadWithScreenShot(Map<String, String> data) {
      uploadFilesPage.clickLicensedStaffConsentLetter(data.get("screenShotName"));
      uploadFilesPage.clickPlusBtn(data.get("screenShotName01"));
      uploadFilesPage.takePhotoWithScreenShotName(data.get("screenShotName02"), data.get("screenShotName03"));

      MobileElement imageStatus = uploadFilesPage.getImageStatus();
      Boolean uploadResult = uploadFilesPage.checkUpLoadStatus(imageStatus);
      assertThat(uploadResult).isTrue();

      uploadFilesPage.takePhotoDone(data.get("screenShotName04"));
      uploadFilesPage.clickSubmitApplication(data.get("screenShotName05"));
    }


}
