Feature: Update CRPQ

  Background: Login
    Given Open WeLab App
    And Enable skip Root
#    When Login with user and password from properties

#  @open_acc01
#  Scenario:open an account Security reminder
#    Then I can see Marketing preferences Page
#
#
#  @open_acc02
#  Scenario:open account back working properly
#    And I goto wealth main page
#    And I click gotIt on wealth page
#    And I click Learn More on goWealth page
#    And I click Skip to open an account
#    And I click next on Welcome to Wealth Management Services Page
#    And I click next on Update and verify personal info page
#    And I enter input MSK key
#    And I click next on Verify personal information page
#    And I select nonCRPQ answer
#      | question                                    | answer                    |
#      | Declaration – Employee of Intermediary      | No                        |
#    And verify I'm on particular page
#      |title | Qualification information                   |
#    And I select nonCRPQ answer
#      | question                                    | answer                    |
#      | Qualification information                   | University level or above |
#    And verify I'm on particular page
#      |title | Investment Horizon |
#    And I select nonCRPQ answer
#      | question                                    | answer                    |
#      | Investment Horizon                          | Over 5 years              |
#    And verify I'm on particular page
#      |title                        | Investment Product Knowledge and Experience |
#    And I select nonCRPQ answer
#      | question                                    | answer                    |
#      | Investment Product Knowledge and Experience | Bonds, Bond Funds         |
#    And verify I'm on particular page
#      |title | Net worth                                   |
#    And I select nonCRPQ answer
#      | question                                    | answer                    |
#      | Net worth                                   | Above HKD 5,000,000       |
#    Then I click back on Update and verify personal info page
#    Then I can see App homepage

#  @open_acc03
#  Scenario: IAO full process with no license
#    And I can see the LoggedIn page
#    And I prepare none-CRPQ through wealth tab
#    And I begin none-CRPQ questionnaire process
#    And I begin to check personal information
#    And I enter input MSK key
#    And I confirm the personal information
#    And I answer the new none-CRPQ questions at "none-CRPQ"
#    And I confirm the Investment account profile
#    Then I prepare upload document with "No"
#    #   Do CRPQ
#    And I Do CRPQ with "CRPQ"
#    Then I finish account opening process
#
#
#  @open_acc04
#  Scenario: IAO full process with license
#    And I can see the LoggedIn page
#    And I prepare none-CRPQ through wealth tab
#    And I begin none-CRPQ questionnaire process
#    And I begin to check personal information
#    And I enter input MSK key
#    And I confirm the personal information
#    And I answer the new none-CRPQ questions at "none-CRPQ-withLicense"
#    And I confirm the Investment account profile
#    Then I prepare upload document with "Yes"
#    #   upload document
#    And I can see the upload files page
#    And I take photo to upload
#    Then I finish upload document
#    #    Do CRPQ
#    And I Do CRPQ with "CRPQ"
#    Then I finish account opening process


  @VBWMIS-T621
  Scenario: Reg_IAO_001 ETB_IAO NST
    Then Login with IAO Account
    Then I can see Marketing preferences Page
      | screenShotName | Reg_IAO_001_Android_01 |
    And I goto wealth page
      | screenShotName | Reg_IAO_001_Android_02 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_001_Android_03 |
    And I click next on Welcome to Wealth Management Services Page
      | screenShotName | Reg_IAO_001_Android_04 |
    And I click next on Confirm information Page
      | screenShotName | Reg_IAO_001_Android_05 |
    And I click next on Verify personal information page
      | screenShotName | Reg_IAO_001_Android_06 |
    And I answer the new none-CRPQ questions at "none-CRPQ-NST"
    #RiskPage
    And I click confirm on Confirm page
      | screenShotName | Reg_IAO_001_Android_07 |
    Then I click circle and submit on Update and verify personal info
      | screenShotName | Reg_IAO_001_Android_08 |
    #upload-1
    Then I click next on Update and verify personal info page
      | screenShotName | Reg_IAO_001_Android_09 |
    #upload-2
    Then I click next on Upload document if required page
      | screenShotName | Reg_IAO_001_Android_10 |
    #Customer Risk Profiling
    Then I click next on Complete Customer Risk Profiling Questionnaire page
      | screenShotName | Reg_IAO_001_Android_11 |
    Then I click Start button Complete Customer Risk Profiling
      | screenShotName | Reg_IAO_001_Android_12 |
    And I select CRPQ answer with "CRPQ"
    #upload-3RiskPage
    And I verify Customer Risk Rating
      | risk            | 2                      |
      | screenShotName  | Reg_IAO_001_Android_13 |
      | screenShotName1 | Reg_IAO_001_Android_14 |
    And I click next on Congratulations page
      | screenShotName | Reg_IAO_001_Android_15 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_001_Android_16 |
    Then I verify open account success
      | screenShotName | Reg_IAO_001_Android_17 |


  @VBWMIS-T620
  Scenario: Reg_IAO_002 ETB_IAO ST
    Then Login with IAO Account
    Then I can see Marketing preferences Page
      | screenShotName | Reg_IAO_002_Android_01 |
    And I goto wealth page
      | screenShotName | Reg_IAO_002_Android_02 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_002_Android_03 |
    And I click next on Welcome to Wealth Management Services Page
      | screenShotName | Reg_IAO_002_Android_04 |
    And I click next on Confirm information Page
      | screenShotName | Reg_IAO_002_Android_05 |
    And I click next on Verify personal information page
      | screenShotName | Reg_IAO_002_Android_06 |
    And I answer the new none-CRPQ questions at "none-CRPQ-ST"
    And I click confirm on Confirm page
      | screenShotName | Reg_IAO_002_Android_07 |
    Then I click circle and submit on Update and verify personal info
      | screenShotName | Reg_IAO_002_Android_08 |
    Then I click next on Update and verify personal info page
      | screenShotName | Reg_IAO_002_Android_09 |
    Then I click next on Upload document if required page
      | screenShotName | Reg_IAO_002_Android_10 |
    And I take photo to upload with screenShot
      | screenShotName   | Reg_IAO_002_Android_11 |
      | screenShotName01 | Reg_IAO_002_Android_12 |
      | screenShotName02 | Reg_IAO_002_Android_13 |
      | screenShotName03 | Reg_IAO_002_Android_14 |
      | screenShotName04 | Reg_IAO_002_Android_15 |
      | screenShotName05 | Reg_IAO_002_Android_16 |
    Then I click next on Complete Customer Risk Profiling Questionnaire page
      | screenShotName | Reg_IAO_002_Android_17 |
    Then I click Start button Complete Customer Risk Profiling
      | screenShotName | Reg_IAO_002_Android_18 |
    And I select CRPQ answer with "CRPQ"
    #upload-3RiskPage
    And I verify Customer Risk Rating
      | risk            | 2                      |
      | screenShotName  | Reg_IAO_002_Android_19 |
      | screenShotName1 | Reg_IAO_002_Android_20 |
    And I click next on Congratulations page
      | screenShotName | Reg_IAO_002_Android_21 |
    Then I click Open Account button
      | screenShotName | Reg_IAO_002_Android_22 |
    Then I verify open account success
      | screenShotName | Reg_IAO_002_Android_23 |

