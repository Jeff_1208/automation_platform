package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;


public class IAOPage extends AppiumBasePage {
    private final int maxInvestAge = 69;
    private String pageName = "IAO Page";
    NoneCRPQPage noneCRPQPage;
    CommonPage commonPage;

    public IAOPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        noneCRPQPage = new NoneCRPQPage();
    }


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Marketing preferences']"),
            @AndroidBy(xpath = "//*[@text='推廣資訊設定']")
    })
    private MobileElement marketingPreferences;



    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText")
    })
    private MobileElement nameOfYourEmployerInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Confirm']"),
            @AndroidBy(xpath = "//*[@text='確定']")
    })
    private MobileElement confirm;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Total balance (HKD)']"),
    })
    private MobileElement totalBalance;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Qualification information']/following-sibling::android.view.ViewGroup"),
    })
    private MobileElement qualificationInformationEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='University level or above']/preceding-sibling::android.view.ViewGroup"),
    })
    private MobileElement universityLevelCircle;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text, 'Terms & Conditions, Important Notes')]/following-sibling::android.view.ViewGroup[4]/android.view.ViewGroup")
    })
    private MobileElement circleOnTermsPage;

    @AndroidFindAll({
            @AndroidBy(xpath = " //*[@text='Submit']"),
            @AndroidBy(xpath = " //*[@text='提交']"),
    })
    private MobileElement submit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Your Customer Risk Rating is:']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='你嘅客戶風險評級：']/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement riskRatingLevel;

    @AndroidFindAll({
           @AndroidBy(xpath = "//*[@text='Thanks you for completing the application']")
    })
    private MobileElement thanksYouForCompletingTheApplication;



    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Next']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='下一步']")
    })
    private MobileElement verifyNextButton;
    @Getter
    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Current Wealth (HKD)']")})
    private MobileElement currentWealth;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Add New Goals']"),
            @AndroidBy(xpath = "//*[@text='增設理財目標']")
    })
    private MobileElement addNewGoalsLink;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Select your goal']"),
            @AndroidBy(xpath = "//*[@text='選擇新嘅理財目標']")
    })
    private MobileElement selectGoalTitleTxt;

    @AndroidFindBy(
            xpath =
                    "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    private MobileElement GoalNameEditButton;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup[1]/android.widget.EditText[1]")
    private MobileElement nameInput;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
            @AndroidBy(
                    xpath =
                            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
    })
    private MobileElement targetEditBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
            @AndroidBy(
                    xpath =
                            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
    })
    private MobileElement targetInput;

    @AndroidFindBy(xpath = "//*[@text='Exceed maximum amount']")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Exceed maximum amount'")
    private MobileElement exceedMsg;

    @AndroidFindBy(
            xpath =
                    "(//android.widget.ScrollView//android.widget.ImageView)[2]/../android.widget.TextView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage/../..//XCUIElementTypeStaticText")
    private MobileElement SMTAgeSlider;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='66']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='67']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='68']/../../android.view.ViewGroup"),
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.widget.TextView[@text='69']/../../android.view.ViewGroup")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(
                    xpath =
                            "//XCUIElementTypeOther[@name='When do you want to retire? (age)']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]//XCUIElementTypeOther"),
            @iOSXCUITBy(
                    xpath =
                            "//XCUIElementTypeOther[@name='When do you want to achieve your goal? (age)']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]//XCUIElementTypeOther")
    })
    private List<MobileElement> ageElementsList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Continue']"),
            @AndroidBy(xpath = "//*[@text='繼續']")
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Continue']/..")
    private MobileElement continueBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Goal horizon')]/../android.widget.TextView[2]")
    })
    @iOSXCUITFindBy(
            xpath =
                    "//XCUIElementTypeStaticText[starts-with(@name,'Target Wealth')]/following-sibling::XCUIElementTypeStaticText")
    private MobileElement goalAchieveAmountValue;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='View recommendation & save my goal']"),
            @AndroidBy(xpath = "//*[@text='View recommendation']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name='View recommendation & save my goal']/XCUIElementTypeOther[1]"),
            @iOSXCUITBy(xpath = "//*[@name='View recommendation']/XCUIElementTypeOther[1]")
    })
    private MobileElement saveGoalBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.FrameLayout[2]//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup\n")
    })
    private MobileElement editName;

    @Getter
    @AndroidFindAll({@AndroidBy(xpath = "//android.widget.TextView[starts-with(@text, 'What is your target wealth')]")})
    private MobileElement targetTitleTxt;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Goal horizon')]/../android.view.ViewGroup[2]")
    })
    private MobileElement HorizonEdit;

    @Getter
    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.FrameLayout[2]//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]\n")
    })
    private MobileElement goalName;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.FrameLayout[2]//android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText\\n")
    })
    private MobileElement goalNameEditbox;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnBackContainer\"]")
    })
    private MobileElement btnBackContainer;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Investment')]/../android.view.ViewGroup[3]")
    })
    private MobileElement oneTimeEditbtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Investment')]/../android.view.ViewGroup[5]")
    })
    private MobileElement monthlyEditbtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Investment')]/../android.widget.EditText[1]")
    })
    private MobileElement oneTimeEditbox;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'Investment')]/..//android.widget.EditText[2]")
    })
    private MobileElement monthlyEditbox;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]"),
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text, '產品發行人會')]/following-sibling::android.view.ViewGroup[1]")
    })
    @iOSXCUITFindBy(
            xpath =
                    "//*[@name='Slide to Confirm']/preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther")
    private MobileElement radioBtn;
    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]")
    private MobileElement slidetoconfirm;

    @AndroidFindBy(xpath = "//*[@text='Achieve Financial Freedom']")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Achieve Financial Freedom'")
    private MobileElement financialFreedomLink;

    @Getter
    @AndroidFindBy(
            xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView\n")
    private MobileElement intoRightAllowText;

    @Getter
    @AndroidFindAll({@AndroidBy(xpath = "//android.widget.FrameLayout[2]//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView")})
    private MobileElement rightArrowTitleTxt;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Wealth Management Services Terms")
    })
    private MobileElement wealthManagementServicesTerms;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Important Notes and Risk Disclosures")
    })
    private MobileElement importantNotesAndRiskDisclosures;

    @AndroidFindAll({
            @AndroidBy(accessibility = "Wealth, tab, 5 of 5"),
            @AndroidBy(accessibility = "GoWealth, tab, 5 of 5")
    })
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Wealth, tab, 5 of 5'")
    private MobileElement wealthTab;


    @AndroidFindBy(accessibility = "CRPQ_INTRODUCE-btn")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start'")
    private MobileElement StartBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-title']")
    })
    private MobileElement accountSuccess;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='DIY My Plan']")
    })
    private MobileElement DIYMyPlan;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='仅在使用此应用时允许']")
    })
    private MobileElement gpsAllow;

    @AndroidFindBy(xpath = "//*[@text='Open an account']")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'openAccountBtn-title'")
    private MobileElement openAccountBtn;

    @AndroidFindBy(xpath = "//*[@text='Return']")
    private MobileElement returnBtn;

    @AndroidFindBy(xpath = "//*[@text='You are not in Hong Kong']")
    private MobileElement unableLocation;

    @AndroidFindBy(xpath = "//*[@text='Mock Wealth GPS']/following-sibling::android.view.ViewGroup[1]")
    private MobileElement locationXCoin;

    @SneakyThrows
    public boolean iCanSeeMarketingPreferences(String screenShotName) {
        Thread.sleep(5000);
        if(isShow(marketingPreferences,5)){
            clickElement(confirm);
        }
        waitUntilElementVisible(wealthTab);
        takeScreenshot(screenShotName);
        return verifyElementExist(wealthTab);
    }

    public void clickGotIt() {
        clickElement(noneCRPQPage.getGotItTab());
    }


    @SneakyThrows
    public void clickLearnMore() {
        clickElement(noneCRPQPage.getGoalBasePicture());
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void scroll(){
        Thread.sleep(2000);
        scrollUp();
    }

    @SneakyThrows
    public void clickNext() {
        Thread.sleep(5000);
        clickElement(verifyNextButton);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void clickNextWithScreen(String screenShotName) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        clickElement(verifyNextButton);
        Thread.sleep(5000);
    }


    @SneakyThrows
    public void iEnterNameOfYourEmployer(String name) {
        Thread.sleep(5000);
        clickElement(nameOfYourEmployerInput);
        andSendKeys(nameOfYourEmployerInput,name);
        inputEnter(nameOfYourEmployerInput);

    }

    @SneakyThrows
    public void clickConfirm(String screenShotName) {
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        scrollUp();
        Thread.sleep(2000);
        clickElement(confirm);
    }

    @SneakyThrows
    public void iModifyOneOfUpdateAndVerifyPersonalInfo() {
        Thread.sleep(5000);
        clickElement(qualificationInformationEdit);
        clickElement(universityLevelCircle);
        clickElement(confirm);
    }


    @SneakyThrows
    public boolean iVerifyOnHomePage() {
        Thread.sleep(2000);
        return verifyElementExist(totalBalance);
    }

    @SneakyThrows
    public void clickCircleOnTermsPage() {
        Thread.sleep(2000);
        clickElement(circleOnTermsPage);
    }

    @SneakyThrows
    public void clickSubmit(String screenShotName) {
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(submit);
    }

    public boolean iVerifyCustomerRiskRating(String risk) {
        boolean flag = false;
        int level = Integer.parseInt(getElementText(riskRatingLevel));
        if (level>= Integer.parseInt(risk)){
            flag = true;
        }
        return flag;
    }

    public boolean iVerifyOnThanksYouForCompletingTheApplicationPage() {
        return  verifyElementExist(thanksYouForCompletingTheApplication);
    }

    public void iSkipToOpenAnAccount() {
        clickElement(noneCRPQPage.getSkipLink());
    }

    public void clickXBtnToSkip() {
    }

    public boolean checkWealth() {
        return isElementDisplayed(getCurrentWealth(), "Current Wealth (HKD)", 6);
    }

    public void findAddnewgoals() {
        scrollUpToFindElement(addNewGoalsLink, 30);
    }

    @SneakyThrows
    public void addNewGoal() {
        Thread.sleep(2000);
        scrollUpToFindElement(addNewGoalsLink, 18, 1);
        clickElement(addNewGoalsLink);
    }

    public WebElement EleByText(String goalXpath) {
        WebElement ele = findElement(By.xpath(goalXpath));
        return ele;
    }

    public void clickEleByText(String text) {
        String goalXpath = "//android.widget.TextView[contains(@text,'" + text + "')]";
        if (!verifyElementExist(EleByText(goalXpath))) {
            scrollUpToFindElement(By.xpath(goalXpath), 8, 10);
        }
        clickElement(EleByText(goalXpath));
    }

    public void setAge(MobileElement start, MobileElement end) {
        TouchAction action = new TouchAction(driver);
        action
                .longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(start)))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                .moveTo(ElementOption.element(end))
                .release()
                .perform();
    }

    private void setGoalTime(MobileElement sourceTimeElement, String age) {
        int ageIndex = Integer.parseInt(age) - (maxInvestAge - ageElementsList.size()) - 1;
        MobileElement targetTimeElement = ageElementsList.get(ageIndex);
        setAge(sourceTimeElement, targetTimeElement);
    }

    @SneakyThrows
    public boolean setTargetGoal(String targetValue, String time, String... name) {
        //    selectGoal(targetLink);
        if (name.length > 0) {
            waitUntilElementClickable(GoalNameEditButton);
            clickElement(GoalNameEditButton);
            waitUntilElementClickable(nameInput);
            clearAndSendKeys(nameInput, name[0]);
            inputEnter(nameInput);
        }
        if (null != targetValue) {
            clickElement(targetEditBtn);
            clearAndSendKeys(targetInput, targetValue);
            inputEnter(targetInput);
        }
        try {
            exceedMsg.isDisplayed();
            return false;
        } catch (NoSuchElementException ex) {
            logger.debug("No error msg.");
        }
        clickElement(SMTAgeSlider);
        scrollUp(scrollPosition.RIGHT);
        setGoalTime(SMTAgeSlider, time);
        Thread.sleep(200);
        return true;
    }

    public String getTargetWealth() {
        return getElementText(targetInput);
    }

    public String getSMTAge() {
        return getElementText(SMTAgeSlider);
    }

    public void clickContinue() {
        waitUntilElementVisible(continueBtn);
        clickElement(continueBtn);
    }

    public String getGoalAchieveAmount() {
        waitUntilElementVisible(goalAchieveAmountValue);
        return getElementText(goalAchieveAmountValue);
    }

    public void saveGoal() {
        waitUntilElementVisible(saveGoalBtn);
        clickElement(saveGoalBtn);
    }

    @SneakyThrows
    public void editGoalName(String text) {
        Thread.sleep(3*1000);
        waitUntilElementVisible(editName);
        clickElement(editName);
        Thread.sleep(2*1000);
        waitUntilElementVisible(goalNameEditbox);
        clearAndSendKeys(goalNameEditbox, text);
        waitUntilElementVisible(rightArrowTitleTxt);
        clickElement(rightArrowTitleTxt);
        Thread.sleep(2*1000);
    }

    public void backBtn() {
        waitUntilElementVisible(btnBackContainer);
        clickElement(btnBackContainer);
    }

    public boolean checkHorizonEdit() {
        waitUntilElementVisible(HorizonEdit);
        clickElement(HorizonEdit);
        return verifyElementExist(targetTitleTxt);
    }

    public void setInvestmentPlan(String onetimeValue, String monthlyValue) {
        waitUntilElementVisible(oneTimeEditbtn);
        clickElement(oneTimeEditbtn);
        clearAndSendKeys(oneTimeEditbox, onetimeValue);
        waitUntilElementVisible(monthlyEditbtn);
        clickElement(monthlyEditbtn);
        clearAndSendKeys(monthlyEditbox, monthlyValue);
    }

    @SneakyThrows
    public void slideToConfirmOrder(String screenShotName, String screenShotName01) {
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        scrollUpToFindElement(slidetoconfirm, 8, 3);
        radioBtn.click();
        takeScreenshot(screenShotName01);
        Thread.sleep(1000);
        // top-left
        int x = slidetoconfirm.getLocation().x;
        int y = slidetoconfirm.getLocation().y;
        // size
        int width = slidetoconfirm.getRect().width;
        int x_start = x + 50;
        int x_end = x + width;
        int y_co = y + 2;
        TouchAction action =
                new TouchAction(driver)
                        .longPress(PointOption.point(x_start, y_co))
                        .moveTo(PointOption.point(x_end, y_co))
                        .release();
        action.perform();
        Thread.sleep(1000 * 10);
    }

    public Boolean verifyEleByText(String text, String screenShotName) {
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        return isElementVisible(EleByText(text), 10);
    }

    @SneakyThrows
    public void checkServicesTerms(String screenShotName){
        Thread.sleep(3000);
        clickElement(wealthManagementServicesTerms);
        isElementVisible(EleByText("1 / 40"));
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void checkINAndRD(String screenShotName){
        Thread.sleep(3000);
        clickElement(importantNotesAndRiskDisclosures);
        isElementVisible(EleByText("1 / 11"));
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    public void clickFinancialPage() {
        waitUntilElementVisible(financialFreedomLink);
        clickElement(financialFreedomLink);
    }

    @SneakyThrows
    public void clickRightArrow() {
        waitUntilElementVisible(rightArrowTitleTxt);
        clickElement(rightArrowTitleTxt);
        Thread.sleep(3*1000);
    }

    @SneakyThrows
    public void setupMyTargetWealthAndAge(String targetValue,String age,String screenShotName) {
        Thread.sleep(2000);
        clickElement(targetEditBtn);
        clearAndSendKeys(targetInput, targetValue);
        inputEnter(targetInput);
        Thread.sleep(2000);
        clickElement(SMTAgeSlider);
        scrollUp(scrollPosition.RIGHT);
        setGoalTime(SMTAgeSlider, age);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void setupOneTimeAndMonthlyInvestment(String oneTime, String monthly, String screenShotName) {
        Thread.sleep(3000);
        waitUntilElementVisible(oneTimeEditbtn);
        clickElement(oneTimeEditbtn);
        clearAndSendKeys(oneTimeEditbox, oneTime);
        Thread.sleep(3000);
        waitUntilElementVisible(monthlyEditbtn);
        clickElement(monthlyEditbtn);
        clearAndSendKeys(monthlyEditbox, monthly);
        inputEnter(monthlyEditbox);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public boolean verifyExceedMaximumAmount() {
        Thread.sleep(2000);
        return verifyElementExist(exceedMsg);
    }

    public void clickStartBtn(String screenShotName) {
        waitUntilElementVisible(StartBtn);
        takeScreenshot(screenShotName);
        clickElement(StartBtn);
    }

    @SneakyThrows
    public boolean openAccountSuccess(String screenShotName) {
        waitUntilElementVisible(accountSuccess);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        return verifyElementExist(accountSuccess);
    }

    @SneakyThrows
    public void backBtnLocation() {
        Thread.sleep(3000);
        //click left top to exit
        clickByLocation(60,150);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void clickDIYMyPlan() {
        Thread.sleep(2000);
        scrollUpToFindElement(DIYMyPlan, 18, 1);
        clickElement(DIYMyPlan);
    }

    @SneakyThrows
    public void verifyGPSCheckingForMCV(String screenShotName, String screenShotName1) {
        Thread.sleep(2000);
        waitUntilElementClickable(openAccountBtn);
        clickElement(openAccountBtn);
        Thread.sleep(3000);
        if(isShow(gpsAllow,2)){
            clickElement(gpsAllow);
            clickElement(openAccountBtn);
        }
        takeScreenshot(screenShotName);
        if(isShow(unableLocation)){
            clickElement(returnBtn);
        }
        commonPage.enterGPSLocation(screenShotName1);
        Thread.sleep(2000);
        waitUntilElementClickable(wealthTab);
        clickElement(wealthTab);
    }

}
