package com.welab.automation.projects.wealth2.WelabAPITest;

import com.alibaba.fastjson.JSONObject;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.ProxyUtils;
import com.welab.automation.framework.utils.JsonUtil;
import com.welab.automation.framework.utils.api.BaseRunner;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * used to send request during app UI automation.
 */
public class WealthQuestionnaires extends BaseRunner{
    private final Logger logger= LoggerFactory.getLogger(WealthQuestionnaires.class);
    private IWealthQuestionnairesAPI iWealthQuestionnairesAPI = ProxyUtils.create(IWealthQuestionnairesAPI.class);

    public WealthQuestionnaires(String ...config){
        super.BeforeClass(config); //pass username and password
    }

    public Response postQuestionnaires(String jsonFile)  {
        //return, so that the invoking place can store the response and check it later.
        Response response = null;
        try {
            JSONObject jsonObject = JsonUtil.extractDataJson(GlobalVar.REQUEST_JSON_FILE_PATH + "/Welab/body/" + jsonFile);
            String objBody = jsonObject.toJSONString();
            response = iWealthQuestionnairesAPI.postQuestionnaries(objBody);
        } catch(Exception e) {
           logger.error("failed to post questionnaires with error {}", e.toString());
        }
        return response;
    }

    public Response getQuestionnaires() {
        return iWealthQuestionnairesAPI.getQuestionnariesCRPQ();
    }

    public static void main(String[] args) {
        Response resp = new WealthQuestionnaires("testsit056", "Aa123321").getQuestionnaires();
        System.out.println(resp.getStatusCode());
        System.out.println(resp.getBody());
    }


}
