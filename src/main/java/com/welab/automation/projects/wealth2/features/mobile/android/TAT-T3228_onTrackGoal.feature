#Feature: Wealth Application
#
#  Background: Login
#    Given Open WeLab App
#    And Enable skip Root
#    When Login with user and password from properties
#    #    And Login with user and password
#    #      | user     | swmcust00165 |
#    #      | password | Aa123321 |
#
#   #this case need goal with OnTrack status
#  Scenario Outline: Reg_Wealth_001 edit the order name with the status of on track
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_001_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    When I click edit button and update goalName
#      | goalName       | bruce1                    |
#      | screenShotName | Reg_Wealth_001_Android_03 |
#    And I click back icon to Wealth page
#      | screenShotName04 | Reg_Wealth_001_Android_04 |
#    And I verify goalName change success
#      | screenShotName | Reg_Wealth_001_Android_05 |
#    Examples:
#      | orderStatus |
#      | OnTrack     |
#
#     #this case need goal with OnTrack status
#  Scenario Outline: Reg_Wealth_002 edit the order name with the status of on track and verify exist confirm button
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_002_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_001_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    And I click Edit button
#      | screenShotName | Reg_Wealth_002_Android_03 |
#    And I click view updated recommendation
#      | screenShotName   | Reg_Wealth_002_Android_04 |
#      | screenShotName01 | Reg_Wealth_002_Android_05 |
#    Examples:
#      | orderStatus |
#      | OnTrack     |
#
#     #this case need goal with OnTrack status
#  Scenario Outline:Reg_Wealth_003 edit the order name with the status of on track and verify exist next button
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_003_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_003_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    And I click Edit button
#      | screenShotName | Reg_Wealth_003_Android_03 |
#    And I click view updated recommendation and click next button
#      | screenShotName   | Reg_Wealth_003_Android_04 |
#      | screenShotName01 | Reg_Wealth_003_Android_05 |
#      | screenShotName02 | Reg_Wealth_003_Android_06 |
#    Examples:
#      | orderStatus |
#      | OnTrack     |
#
#     #this case need goal with OnTrack status
#  Scenario Outline:Reg_Wealth_004 edit the order name with the status of on track and verify have two documents in review and confirm order page
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_004_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_004_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    And I click Edit button
#      | screenShotName | Reg_Wealth_004_Android_03 |
#    And I click view updated recommendation and verify have two documents in review and confirm order page
#      | screenShotName   | Reg_Wealth_004_Android_04 |
#      | screenShotName01 | Reg_Wealth_004_Android_05 |
#      | screenShotName02 | Reg_Wealth_004_Android_06 |
#      | screenShotName03 | Reg_Wealth_004_Android_07 |
#      | screenShotName04 | Reg_Wealth_004_Android_08 |
#      | screenShotName05 | Reg_Wealth_004_Android_09 |
#      | screenShotName06 | Reg_Wealth_004_Android_10 |
#    Examples:
#      | orderStatus |
#      | OnTrack     |
#
#     #this case need goal with OnTrack status
#  Scenario Outline:Reg_Wealth_005 edit the order name with the status of on track and order successfully
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_005_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_005_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    And I click view updated recommendation button and slide trading
#      | screenShotName   | Reg_Wealth_005_Android_03 |
#      | screenShotName01 | Reg_Wealth_005_Android_04 |
#      | screenShotName02 | Reg_Wealth_005_Android_05 |
#      | screenShotName03 | Reg_Wealth_005_Android_06 |
#      | screenShotName04 | Reg_Wealth_005_Android_07 |
#      | screenShotName05 | Reg_Wealth_005_Android_08 |
#      | screenShotName06 | Reg_Wealth_005_Android_09 |
#      | screenShotName07 | Reg_Wealth_005_Android_10 |
#      | screenShotName08 | Reg_Wealth_005_Android_11 |
#      | screenShotName09 | Reg_Wealth_005_Android_12 |
#    Examples:
#      | orderStatus |
#      | OnTrack     |
#
