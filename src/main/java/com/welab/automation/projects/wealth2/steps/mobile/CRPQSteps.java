package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.framework.utils.api.Matchers;
import com.welab.automation.framework.utils.app.ExcelDataToDataTable;
import com.welab.automation.projects.wealth2.WelabAPITest.WealthQuestionnaires;
import com.welab.automation.projects.wealth2.pages.iao.UploadFilesPage;
import com.welab.automation.projects.wealth2.pages.iao.CRPQPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;


import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CRPQSteps {


  // it will be used to verify the original risk score during updating CRPQ.
  Response response;

  UploadFilesPage uploadFilesPage;
  CRPQPage crpqPage;
  public CRPQSteps(){
    uploadFilesPage = new UploadFilesPage();
    crpqPage = new CRPQPage();
  }

  @Given("^Finished uploading document$")
  public void uploadDocumentToCRPQ() {
   crpqPage.clickTransitionPage2();

  }

  @When("^Starting CRPQ$")
  public void clickStartCRPQ() {
    crpqPage.startCRPQ();
  }



  @And("^I Select Answer for (.*)")
  public void selectOptionForEachQuestion(String questionDesc, DataTable table) {
    List<List<String>> data = table.asLists();
    for (List<String> row : data) {
      Boolean result = crpqPage.selectOption(questionDesc, row.get(0));
      assertThat(result).isTrue();
    }
  }

  @And("^Confirm CRPQ answer$")
  public void confirmCRPQAnswer() {
    crpqPage.confirmCRPQAnser();
  }

  @And("Finish CRPQ process")
  public void finishCRPQProcess() {
    crpqPage.finishCRPQ(uploadFilesPage.isSkipped());
  }

  @Given("I Submit CRPQ Answers with {} {} {}")
  public void submitCRPQAnswers(String Username, String Password, String jsonFile) {
    response = new WealthQuestionnaires(Username, Password).postQuestionnaires(jsonFile);
    assertThat(response).isNotNull();
    assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
  }

  @And("I view Risk Profile Result")
  public void viewCRPQResult() {
    crpqPage.viewCRPQResult();
  }

  @Then("I Check Original Risk Score")
  public void checkRiskScore() {
    String actualScore = crpqPage.getCRPQScore();
    // create crpq profile firstly, then check the actual score here during updating CRPQ profile.
    Matchers.checkField(
        response.jsonPath(), "data.crrLevel", String.format("equals(%s)", actualScore));
  }

  @Then("My risk score should be {}")
  public void checkRiskScore(String expScore) {
    String actualScore = crpqPage.getCRPQScore();
    boolean flag = false;
    if(Integer.parseInt(actualScore)>=Integer.parseInt(expScore)){
      flag = true;
    }
    assertThat(flag).isTrue();
  }

  @And("I Start to Update CRPQ Profile")
  public void StartUpdateCRPQ() {
    crpqPage.startUpdateCRPQ();
  }

  @And("I Review My Risk Profile")
  public void reviewRiskProfile() {
    crpqPage.startReviewCRPQ();
  }

  @Then("^The Answer of \"(.*)\" should be")
  public void verifyAnswer(String questionDesc, DataTable table) {
    List<List<String>> data = table.asLists();
    for (List<String> row : data) {
      Boolean result = crpqPage.verifyAnswer(questionDesc, row.get(0));
      assertThat(result).isTrue();
    }
  }

  @Given("I Submit CRPQ Answers maximum times with {} {} {}")
  public void submitCRPQAnswersMaxTimes(String Username, String Password, String jsonFile) {
    short maxCRPQTrial = 5; // or 3?
    while (true) {
      Response resp = new WealthQuestionnaires(Username, Password).postQuestionnaires(jsonFile);
      if (resp.getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR || maxCRPQTrial == 0) {
        break;
      }
      maxCRPQTrial--;
    }
  }

  @Then("I find I can not Update Risk Profile again")
  public void verifyCRPQCannotUpdated() {
    boolean rslt = crpqPage.verifyCannotUpdateCRPQ();
    assertThat(rslt).isTrue();
  }

  @When("I click X on CRPQ page")
  public void iClickXOnCRPQPage() {
    crpqPage.clickElement(crpqPage.getXBtnG0CRPQ());
  }
  /**
   * select CRPQ answer as per excel data.
   *
   * @param sheetName
   * @return expected risk score
   */
  private String selectCRPQOptAsPerExcel(String sheetName) {
    String dataFile = "data_template.xlsx";
    DataTable dataTable = new ExcelDataToDataTable(dataFile, sheetName).getDatatable();
    List<List<String>> data = dataTable.asLists();
    String expRiskScore = "";

    for (List<String> row : data) {
      if (row.get(0).equals("expected risk score")) {
        // like 2.0
        if (row.get(1).contains(".")) {
          expRiskScore = row.get(1).split("\\.")[0];
        } else {
          expRiskScore = row.get(1);
        }
        continue;
      }
      Boolean result = crpqPage.selectOption(row.get(0), row.get(1));
      assertThat(result).isTrue();
    }
    return expRiskScore;
  }

  @And("I Do CRPQ with {string}")
  public void doCRPQ(String sheetName) {
    // including all CRPQ steps
    uploadDocumentToCRPQ();
    clickStartCRPQ();
    String expRiskScore = selectCRPQOptAsPerExcel(sheetName);
    System.out.println(expRiskScore+"expRiskScore");
    confirmCRPQAnswer();
    checkRiskScore(expRiskScore);
    finishCRPQProcess();
  }

  @And("I can see CRPQ welcome page")
  public void iCanSeeCRPQWelcomePage() {
    crpqPage.seeCRPQWelcomePage();
  }

  @And("I'm on CRPQ start page")
  public void iMOnCRPQStartPage() {
    assertThat(crpqPage.isCRPQIntroductionDisplayed()).isTrue();
  }

  @Then("I click confirm on update page")
  public void iClickConfirmOnUpdatePage() {
    crpqPage.clickConfirm();
  }

  @Then("I click next on update page with license")
  public void iClickNextOnUpdatePageWithlicense() {
    crpqPage.clickNextWithLicense();
  }

  @Then("I prepare upload document with {string}")
  public void iPrepareUploadDocumentWith(String answer) {
    crpqPage.iPrepareUploadDocument(answer);
  }

  @And("I select CRPQ answer with {string}")
  public void iSelectCRPQAnswerWith(String sheetName) {
    String dataFile = "data_template.xlsx";
    DataTable dataTable = new ExcelDataToDataTable(dataFile, sheetName).getDatatable();
    List<List<String>> data = dataTable.asLists();
    for (List<String> row : data) {
      Boolean result = crpqPage.selectOptionWithScreen(row.get(1),row.get(2));
      assertThat(result).isTrue();
    };
  }

}
