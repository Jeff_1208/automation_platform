package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.projects.ScenarioContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;

import static com.welab.automation.framework.utils.TimeUtils.getCurrentTimeAsString;

public class MobileHooks extends AppiumBasePage {
  private static final Logger logger = LoggerFactory.getLogger(MobileHooks.class);
  BaseDriver baseDriver;
  boolean recordVideo = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("recordVideo"));

  @Before
  public void beforeScenario(Scenario scenario) throws IOException {
    logger.info("* Start running scenario: {}", scenario.getName());
    this.baseDriver = new BaseDriver();
    this.baseDriver.initMobileDriver();
    if (recordVideo) {
      startRecording();
    }
    storeCaseInitResult(scenario);
  }

  @After
  public void afterScenario(Scenario scenario) {
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    String screenshotVideoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      stopRecording(screenshotVideoName);
    }

    if (scenario.isFailed()) {
      // Take screenshot to report
      final byte[] screenshot =
          ((TakesScreenshot) BaseDriver.getMobileDriver()).getScreenshotAs(OutputType.BYTES);
      scenario.attach(screenshot, "image/png", screenshotVideoName);
      // Take screenshots to project folder
      takeScreenshot(screenshotVideoName);
    }
    logger.info("* End running scenario: {}", scenario.getName());
    BaseDriver.closeMobileDriver();
    storeCaseResult(scenario);
    ScenarioContext.getInstance().clearScenarioData();
  }


  public void storeCaseInitResult(Scenario scenario){
    String tag = getCaseTag(scenario);
    if(tag != ""){
      tag = tag.substring(1);
      GlobalVar.CASE_RESULT.put(tag, false);
    }
  }

  public void storeCaseResult(Scenario scenario){
    String tag = getCaseTag(scenario);
    if(tag != ""){
      tag = tag.substring(1);
      GlobalVar.CASE_RESULT.put(tag, !scenario.isFailed());
    }
  }

  public String getCaseTag(Scenario scenario){
    Collection<String> tags = scenario.getSourceTagNames();
    String tag = "";
    for (String t: tags) {
      if(t.startsWith("@VBWMIS")){
        tag = t;
        break;
      }
    }
    return tag;
  }
}
