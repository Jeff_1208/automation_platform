package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.LoginPage;
import com.welab.automation.projects.wealth2.pages.iao.SingleFundIosPage;
import com.welab.automation.projects.wealth2.pages.iao.SingleFundPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class SingleFundIosSteps {
    LoginPage loginPage;
    CommonPage commonPage;
    SingleFundIosPage singleFundIosPage;

    public SingleFundIosSteps(){
        loginPage = new LoginPage();
        commonPage = new CommonPage();
        singleFundIosPage = new SingleFundIosPage();
    }


    @Then("wealth Check Funds Page IOS")
    public void wealthCheckFundsPageIOS(Map<String, String> data) {
        singleFundIosPage.wealthCheckFundsPageIOS(data.get("screenShotName"));
    }

    @Then("get Feature Funds Themes IOS")
    public void getFeatureFundsThemes(Map<String, String> data) {
        singleFundIosPage.getFeatureFundsThemes(data.get("screenShotName"));
    }

    @Then("check Feature Funds List Page IOS")
    public void checkFeatureFundsListPage(Map<String, String> data) {
        singleFundIosPage.checkFeatureFundsListPage(data.get("screenShotName"));
    }

    @Then("search USD Funds IOS")
    public void searchUSDFunds(Map<String, String> data) {
        singleFundIosPage.searchUSDFunds(data.get("screenShotName"));
    }

    @Then("search HKD Funds IOS")
    public void searchHKDFunds(Map<String, String> data) {
        singleFundIosPage.searchHKDFunds(data.get("screenShotName"));
    }

    @Then("search Funds by Name IOS")
    public void searchFundsByName(Map<String, String> data) {
        singleFundIosPage.searchFundsByName(data.get("fundName"));
    }

    @Then("select One Fund To Buy")
    public void selectOneFundToBuy(Map<String, String> data) {
        singleFundIosPage.selectOneFundToBuy(data.get("screenShotName"));
    }

    @Then("buy One Fund In Funds List IOS")
    public void buyOneFundInFundsList(Map<String, String> data) {
        singleFundIosPage.buyOneFundInFundsList(data.get("oneTimeAmount"), data.get("monthlyAmount"),
                data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("check First Order In Wealth Page IOS")
    public void checkFirstOrderInWealthPage(Map<String, String> data) {
        singleFundIosPage.checkFirstOrderInWealthPage(data.get("screenShotName"));
    }

    @Then("switch HKD to USD Currency")
    public void switchHKDtoUSDCurrency(Map<String, String> data) {
        singleFundIosPage.switchHKDtoUSDCurrency(data.get("screenShotName"));
    }


    @Then("open USD Fund On My Fund Page")
    public void openUSDFundOnMyFundPage(Map<String, String> data) {
        singleFundIosPage.openUSDFundOnMyFundPage(data.get("screenShotName"));
    }
    @Then("open HKD Fund On My Fund Page")
    public void openHKDFundOnMyFundPage(Map<String, String> data) {
        singleFundIosPage.openHKDFundOnMyFundPage(data.get("screenShotName"));
    }

    @Then("click Add Buy Button")
    public void clickAddBuyButton() {
        singleFundIosPage.clickAddBuyButton();
    }

    @Then("click Sell Button")
    public void clickSellButton() {
        singleFundIosPage.clickSellButton();
    }

    @Then("click Monthly Edit")
    public void clickMonthlyEdit() {
        singleFundIosPage.clickMonthlyEdit();
    }

    @Then("sell Fund Partial Amount IOS")
    public void sellFundPartialAmount(Map<String, String> data) {
        singleFundIosPage.sellFundPartialAmount(data.get("sellAmount"),
                data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("sell Fund All Amount IOS")
    public void sellFundAllAmount(Map<String, String> data) {
        singleFundIosPage.sellFundAllAmount(
                data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("edit Monthly Standing Instruction IOS")
    public void editMonthlyStandingInstruction(Map<String, String> data) {
        singleFundIosPage.editMonthlyStandingInstruction(data.get("monthlyAmount"),
                data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("check the Fund update")
    public void checkTheFundUpdate(Map<String, String> data) {
        singleFundIosPage.checkTheFundUpdate(data.get("screenShotName"));
    }

}
