Feature: singleFund

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    When Login with user and password from properties

  @singleFund-01
  Scenario Outline: :Buy Technology fund by HKD through Top funds
    And I verify Available balance before transaction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Top funds and hot themes |
    And I click random Top fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I verify buy fund balance
    Then I verify buy fund record
    Examples:
      | oneTime | type | monthly |
      | 100     | HKD  | 100     |

  @singleFund-02
  Scenario Outline: :Buy Technology fund by USD  through Top funds
    And I goto Total balance page
    Then I verify Total Balance before transaction
    And Total balance page back to home page
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Top funds and hot themes |
    And I click random Top fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I goto Total balance page
    And I goto USD Balance page
    Then I verify USD balance After transaction
    Examples:
      | oneTime | type | monthly |
      | 100     | USD  | 00      |

  @singleFund-03
  Scenario Outline: :Buy Healthcare fund by HKD through Equities
    And I verify Available balance before transaction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Equity |
    And I click random Equity fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I verify buy fund balance
    Then I verify buy fund record
    Examples:
      | oneTime | type | monthly |
      | 100     | HKD  | 100     |

  @singleFund-04
  Scenario Outline: :Buy Healthcare fund by USD through Equities
    And I goto Total balance page
    Then I verify Total Balance before transaction
    And Total balance page back to home page
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Equity |
    And I click random Equity fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I goto Total balance page
    And I goto USD Balance page
    Then I verify USD balance After transaction
    Examples:
      | oneTime | type | monthly |
      | 100     | USD  | 00      |

  @singleFund-05
  Scenario Outline: :Buy ESG Bonds fund by HKD through Bond Fund & Money Market Fund
    And I verify Available balance before transaction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Bond Fund & Money Market Fund |
    And I click random Bond fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I verify buy fund balance
    Then I verify buy fund record
    Examples:
      | oneTime | type | monthly |
      | 100     | HKD  | 100     |

  @singleFund-06
  Scenario Outline: :Buy ESG Bonds fund by USD through Bond Fund & Money Market Fund
    And I goto Total balance page
    Then I verify Total Balance before transaction
    And Total balance page back to home page
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click Featured Funds
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Bond Fund & Money Market Fund |
    And I click random Bond fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I goto Total balance page
    And I goto USD Balance page
    Then I verify USD balance After transaction
    Examples:
      | oneTime | type | monthly |
      | 100     | USD  | 00      |

  @singleFund-07
  Scenario Outline: :Buy Top Performing Funds fund by HKD through Mixed Asset
    And I verify Available balance before transaction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And I click text
      | text | Mixed Asset |
    And I click random Mixed Asset fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I verify buy fund balance
    Then I verify buy fund record
    Examples:
      | oneTime | type | monthly |
      | 100     | HKD  | 100     |

  @singleFund-08
  Scenario Outline: :Buy Top Performing Funds fund by USD through Mixed Asset
    And I goto Total balance page
    Then I verify Total Balance before transaction
    And Total balance page back to home page
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
      | screenShotName | Reg_SingleFund_001_Android_03 |
    And I click text
      | text | Mixed Asset |
    And I click random Mixed Asset fund Investment themes
    And I click Show me relevant funds
    And I click random Related Products
    And I click Buy On Featured Fund Page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and Slide to confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I goto Total balance page
    And I goto USD Balance page
    Then I verify USD balance After transaction
    Examples:
      | oneTime | type | monthly |
      | 100     | USD  | 00      |

  @singleFund-09
  Scenario Outline: :Buy fund by HKD
    And I verify Available balance before transaction
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click Buy button on Fund detail page
      | screenShotName | Reg_SingleFund_001_Android_08 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I verify buy fund balance
    Then I verify buy fund record
    Examples:
      | oneTime | type | monthly |
      | 100     | HKD  | 100      |


  @singleFund-10
  Scenario Outline: :Buy fund by USD
    And I goto Total balance page
    Then I verify Total Balance before transaction
    And Total balance page back to home page
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And Choose a fund that is not zero
    And verify I'm on particular page
      | title | Fund detail |
    Then I click Buy button on Fund detail page
      | screenShotName | Reg_SingleFund_006_Android_04 |
    And I enter buy fund investment <oneTime> and <type> and <monthly>
    Then I confirm Review and confirm Order
    Then I verify buy fund success
      | screenShotName | Reg_SingleFund_001_Android_11 |
    And I verify buy fund order status
      | screenShotName   | Reg_SingleFund_001_Android_12 |
      | screenShotName01 | Reg_SingleFund_001_Android_13 |
    Then order status page back to home page
    And I goto Total balance page
    And I goto USD Balance page
    Then I verify USD balance After transaction
    Examples:
      | oneTime | type | monthly |
      | 10      | USD  | 00      |


  @singleFund-11
  Scenario Outline:Sell part of fund
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And Choose a fund that is not zero
    And verify I'm on particular page
      | title | Fund detail |
    Then I click sell button on Fund detail page
      | screenShotName | Reg_SingleFund_006_Android_04 |
    And I enter sell fund <amount> and <type>
    Then I confirm Review and confirm Order
    Then I verify sell fund by success
      | screenShotName | Reg_SingleFund_011_Android_07 |
    And I verify sell fund order status
      | screenShotName   | Reg_SingleFund_011_Android_08 |
      | screenShotName01 | Reg_SingleFund_011_Android_09 |
    Then I verify sell fund balance After transaction
    Examples:
      | amount | type |
      | 100    | part |


  @singleFund-12
  Scenario Outline: Sell All of fund
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And Choose a fund that is not zero
    And verify I'm on particular page
      | title | Fund detail |
    Then I click sell button on Fund detail page
      | screenShotName | Reg_SingleFund_006_Android_04 |
    And I enter sell fund <amount> and <type>
    Then I confirm Review and confirm Order
    Then I verify sell fund by success
      | screenShotName | Reg_SingleFund_011_Android_07 |
    And I verify sell fund order status
      | screenShotName   | Reg_SingleFund_011_Android_08 |
      | screenShotName01 | Reg_SingleFund_011_Android_09 |
    Then I verify sell fund balance After transaction
    Examples:
      | amount | type |
      | 100    | all  |


  @singleFund-13
  Scenario Outline:update fund Monthly standing instruction to Greater and less
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And Choose a fund that is not zero
    And verify I'm on particular page
      | title | Fund detail |
    Then I get Monthly standing instruction amount before update
    And I goto Monthly standing instruction Edit page
      | screenShotName | Reg_SingleFund_012_Android_04 |
    Then I enter Monthly instruction amount choose <type>
    Then I confirm Review and confirm Order
    And I verify update Monthly instruction success
      | screenShotName | Reg_SingleFund_012_Android_07 |
    Then I verify Monthly instruction amount changed
      | screenShotName | Reg_SingleFund_012_Android_08 |
    And I goto Order status page to verify Monthly instruction
      | screenShotName | Reg_SingleFund_012_Android_09 |

    Then standing instruction page back to Fund detail page
    Then I get Monthly standing instruction amount before update
    And I goto Monthly standing instruction Edit page
    Then I enter Monthly instruction amount choose <type2>
    Then I confirm Review and confirm Order
    And I verify update Monthly instruction success
      | screenShotName | Reg_SingleFund_012_Android_07 |
    Then I verify Monthly instruction amount changed
      | screenShotName | Reg_SingleFund_012_Android_08 |
    And I goto Order status page to verify Monthly instruction
      | screenShotName | Reg_SingleFund_012_Android_09 |
    Examples:
      | type    | type2 |
      | greater | less  |


  @singleFund-14
  Scenario Outline:update fund Monthly standing instruction to zero
    And I goto wealth page
      | screenShotName | Reg_SingleFund_001_Android_01 |
    Then I click view my funds on GoWealth Page
      | screenShotName | Reg_SingleFund_001_Android_02 |
    And Choose a fund that is not zero
    And verify I'm on particular page
      | title | Fund detail |
    Then I get Monthly standing instruction amount before update
    And I goto Monthly standing instruction Edit page
      | screenShotName | Reg_SingleFund_012_Android_04 |
    Then I enter Monthly instruction amount choose <type>
    Then I confirm Review and confirm Order
    And I verify update Monthly instruction success
      | screenShotName | Reg_SingleFund_012_Android_07 |
    Then I verify Monthly instruction amount changed
      | screenShotName | Reg_SingleFund_012_Android_08 |
    And I goto Order status page to verify Monthly instruction
      | screenShotName | Reg_SingleFund_012_Android_09 |

    Then standing instruction page back to Fund detail page
    Then I get Monthly standing instruction amount before update
    And I goto Monthly standing instruction Edit page
      | screenShotName | Reg_SingleFund_012_Android_04 |
    Then I enter Monthly instruction amount choose <type2>
    Then I confirm Review and confirm Order
    And I verify update Monthly instruction success
      | screenShotName | Reg_SingleFund_012_Android_07 |
    Then I verify Monthly instruction amount changed
      | screenShotName | Reg_SingleFund_012_Android_08 |
    And I goto Order status page to verify Monthly instruction
      | screenShotName | Reg_SingleFund_012_Android_09 |
    Examples:
      | type | type2  |
      | zero | normal |

