package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import static org.assertj.core.api.Assertions.assertThat;

;

public class LoginSteps {

  LoginPage loginPage;

  public LoginSteps(){
    loginPage= new LoginPage();
  }

//  @When("Login with user and password")
//  public void loginWithUserAndPassword(Map<String, String> data) {
//    loginPage.loginWithCredential(data.get("user"), data.get("password"));
//  }

  @SneakyThrows
  @When("I on Welab app homepage")
  public void iClickLoginButton() {

  }

  @And("I enter valid Welab account {} and {}")
  public void iEnterValidAccount(String userName, String password) {
    loginPage.loginWithCredential(userName,password);
  }

  @SneakyThrows
  @Then("I see the Welab app home page")
  public void iSeeTheAppHomePage() {
    assertThat(true);
  }


}
