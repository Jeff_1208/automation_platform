package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;
import org.openqa.selenium.By;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SingleFundPage extends AppiumBasePage {
    private String pageName = "SingleFund Page";
    IAOPage iaoPage;
    CommonPage commonPage;

    public SingleFundPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        iaoPage = new IAOPage();
    }

    private double availableBalanceBeforeTran=0.00;
    private double availableBalanceAfterTran=0.00;
    private double transactionAmount=0.00;
    private String fundName;
    private String buyFundMoneyType;
    private String language;
    private String amountStr;
    private double USDBalanceBeforeTran=0.00;
    private double USDBalanceAfterTran=0.00;
    private double balanceBeforeTran=0.00;
    private double balanceAfterTran=0.00;
    private String buyAndSellAmount;


    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[contains(@text,'Available balance')]/../android.widget.TextView[2]"),
            @AndroidBy(
                    xpath = "//android.widget.TextView[contains(@text,'可用結餘')]/../android.widget.TextView[2]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Available balance')]")
    })
    private MobileElement availableBalance;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Total balance (HKD)']/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[1]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='總結餘 (HKD)']/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[1]")
    })
    private MobileElement totalBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='View featured funds']"),
            @AndroidBy(xpath = "//*[@text='查看基金']")
    })
    private MobileElement viewMyFundsOnGoWealthPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Current Wealth (HKD)']"),
            @AndroidBy(xpath = "//*[@text='現時財富總值(港元)']")
    })
    private MobileElement currentWealth;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Weight']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='比重']/following-sibling::android.view.ViewGroup[1]"),
    })
    private MobileElement firstFund;

    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Buy ($0 Subscription Fee)']"), @AndroidBy(xpath = "//*[@text='買入']")})
    private MobileElement buySubscriptionFee;

    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Buy']"), @AndroidBy(xpath = "//*[@text='買入']")})
    private MobileElement buyBtnOnFeaturedFunds;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='One time investment (HKD)']"),
            @AndroidBy(xpath = "//*[@text='一次性投資 (港元)']")
    })
    private MobileElement investmentInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='One time investment (USD)']"),
            @AndroidBy(xpath = "//*[@text='一次性投資 (美元)']")
    })
    private MobileElement investmentInputUSD;

    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Next']"), @AndroidBy(xpath = "//*[@text='下一步']")})
    private MobileElement nextButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Congratulations!']"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'恭喜')]")
    })
    private MobileElement congratulations;

    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Done']"), @AndroidBy(xpath = "//*[@text='完成']")})
    private MobileElement doneButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Order Status']"),
            @AndroidBy(xpath = "//*[@text='交易指示狀態']")
    })
    private MobileElement orderStatus;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[@content-desc='btnBackContainer']/android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement backBtnOnOrderStatus;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.Button[@content-desc='Home, tab, 1 of 5']/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView"),
            @AndroidBy(
                    xpath =
                            "//android.widget.Button[@content-desc='主頁, tab, 1 of 5']/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView"),
    })
    private MobileElement homeBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Weight']/following-sibling::android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text='比重']/following-sibling::android.view.ViewGroup")
    })
    private List<MobileElement> fundList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Pay in HKD']/preceding-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='以港元支付']/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement fundNameOnBuyPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[starts-with(@text, 'Last Year')]/preceding-sibling::android.widget.TextView[3]"),
    })
    private MobileElement fundNameOnFeaturedFund;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Investment mode']/preceding-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='投資模式']/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement buyFundNameOnReviewPage;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='交易指示日期']/following-sibling::android.view.ViewGroup[3]/android.widget.TextView[1]")
    })
    private MobileElement buyAmountOnReviewPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Order status']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='交易指示狀態']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement transactionSuccessful;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Orders are sorted from newest to oldest.']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[4]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='指示由新至舊順序排列']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[4]")
    })
    private MobileElement orderStatusDetailAmount;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView//android.view.ViewGroup[2]//android.view.ViewGroup[2]/android.widget.TextView"),
            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//*[@text='今日']/../../android.view.ViewGroup[2]")
    })
    private MobileElement traderNameAtHome;

    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//*[@text='Exchange amount']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='兌換金額']/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement exchangeAmountInDetails;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Details']/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='詳情']/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]")
    })
    private MobileElement foreignCurrencyExchangeAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@class='android.widget.Switch']"),
    })
    private MobileElement HKDUSDSwitch;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='USD Balance']/following-sibling::android.widget.TextView[3]"),
            @AndroidBy(xpath = "//*[@text='美元結餘']/following-sibling::android.widget.TextView[3]")
    })
    private MobileElement USDBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Total balance']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='總結餘']/preceding-sibling::android.view.ViewGroup[1]")
    })
    private MobileElement backBtnOnTotalBalance;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
    })
    private MobileElement firstUSDExchange;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Details']/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[2]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='詳情']/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[2]")
    })
    private MobileElement USDAmountOnTranDetail;

    @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Sell']"), @AndroidBy(xpath = "//*[@text='賣出']")})
    private MobileElement sellBtnOnFundDetailPage;

    @AndroidFindAll({
            @AndroidBy(xpath = " //*[starts-with(@text, 'One-off redemption' )]"),
            @AndroidBy(xpath = " //*[starts-with(@text, '一次性贖回金額' )]")
    })
    private MobileElement sellInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Sell all']"),
            @AndroidBy(xpath = "//*[@text='全部賣出']")
    })
    private MobileElement sellAllBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Redeem All']"),
            @AndroidBy(xpath = "//*[@text='全部贖回']")
    })
    private MobileElement redeemAllBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Sell now']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='立即賣出']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement sellFundNameOnSellPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Sell amount']/preceding-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='賣出金額']/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement sellFundNameReviewPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Sell order completed']"),
            @AndroidBy(xpath = "//*[@text='賣出交易指示已完成']")
    })
    private MobileElement sellOrderCompleted;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[contains(@text,'Reference number')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[contains(@text,'參考編號')]/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement fundTranTips;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Orders are sorted from newest to oldest.']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[3]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='指示由新至舊順序排列']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[3]")
    })
    private MobileElement sellOrBuyOnOrderStatus;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Orders are sorted from newest to oldest.']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[2]"),
            @AndroidBy(
                    xpath =
                            "//*[@text='指示由新至舊順序排列']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[2]")
    })
    private MobileElement sellOrBuyFundOnOrderStatus;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available balance')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'可用結餘')]")
    })
    private MobileElement sellAvailableBalance;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='Monthly standing instruction']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='每月常設指示']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement monthlyStandingInstructionAmount;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'All original monthly')]/preceding-sibling::android.view.ViewGroup[1]/android.widget.EditText"),
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, '所有原定每月投資指示')]/preceding-sibling::android.view.ViewGroup[1]/android.widget.EditText")
    })
    private MobileElement monthlyInstructionInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Confirm']"),
            @AndroidBy(xpath = "//*[@text='確定']")
    })
    private MobileElement confirmBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, 'equivalent to')]/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]"),
            @AndroidBy(
                    xpath =
                            "//android.widget.TextView[starts-with(@text, '相等於')]/preceding-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]")
    })
    private MobileElement equivalentMonthlyInvestment;

    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//android.widget.TextView[starts-with(@text, 'Your monthly instruction for')]")
    })
    private MobileElement monthlyInvestmentEditText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Standing Instruction']"),
            @AndroidBy(xpath = "//*[@text='常設指示']")
    })
    private MobileElement standingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Buy']/preceding-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='買入']/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement fundNameOnStandingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Buy']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='買入']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement monthlyAmountOnStandingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Featured Funds']"),
            @AndroidBy(xpath = "//*[@text='精選主題基金']")
    })
    private MobileElement FeaturedFunds;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Show me relevant funds']"),
            @AndroidBy(xpath = "//*[@text='瀏覽相關基金']")
    })
    private MobileElement showRelevantFunds;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[starts-with(@text, 'Investment involves')]"),
            @AndroidBy(xpath = "//*[starts-with(@text, '投資涉及風險')]")
    })
    private MobileElement InvestmentRiskText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Slide to Confirm']/../."),
            @AndroidBy(xpath = "//*[@text='滑動確認']/../.")
    })
    private MobileElement slideToConfirm;

    @AndroidFindAll({
            @AndroidBy(xpath = "(//android.widget.TextView[@content-desc=\"FUND_DETAIL-subtitle\"])[1]/preceding-sibling::android.widget.TextView[1]")
    })
    private MobileElement fundNameOnFundDetail;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Monthly investment (HKD)']"),
    })
    private MobileElement monthlyInvestmentInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Top funds and hot themes']/../../../android.view.ViewGroup[1]/following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> topFundsList;
    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//*[@text='Equity']/../../../android.view.ViewGroup[1]/following-sibling::android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> equityFundsList;
    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//*[@text='Bond & Money Market']/../../../android.view.ViewGroup[1]/following-sibling::android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> bondFundsList;
    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//*[@text='Mixed Asset']/../../../android.view.ViewGroup[1]/following-sibling::android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> mixedFundsList;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Related products']/../../android.view.ViewGroup[1]/following-sibling::android.view.ViewGroup")
    })
    private List<MobileElement> relatedProductsList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup")
    })
    private MobileElement firstFundOnInvestment;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"-btn\"]/android.view.ViewGroup")
    })
    private MobileElement firstOnStandingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Product name']/following-sibling::android.widget.TextView[3]")
    })
    private MobileElement amountInStandingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Product name']/following-sibling::android.widget.TextView[2]")
    })
    private MobileElement typeInStandingInstruction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='New monthly investment']/following-sibling::android.widget.EditText")
    })
    private MobileElement newMonthlyInvestment;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Fund list']")
    })
    private MobileElement fundListText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='View full fund list']")
    })
    private MobileElement viewFullFundList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='All funds']")
    })
    private MobileElement allFunds;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='HKD funds']")
    })
    private MobileElement HKDFunds;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='USD funds']")
    })
    private MobileElement USDFunds;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[starts-with(@text, 'Showing' )]/following-sibling::android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    })
    private List<MobileElement> fundListOnFullFundPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[starts-with(@text, 'One time investment ' )]"),
            @AndroidBy(xpath = "//*[starts-with(@text, '一次性投資' )]")
    })
    private MobileElement oneTimeInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[starts-with(@text, 'You have invested' )]")
    })
    private MobileElement amountOnProcessPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
    })
    private List<MobileElement> orderFundInFundList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    })
    private List<MobileElement> weightFundList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Sell amount']/following-sibling::android.view.ViewGroup[1]/android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='賣出金額']/following-sibling::::android.view.ViewGroup[1]/android.widget.TextView[1]")
    })
    private MobileElement sellAmountReviewPage;

    @SneakyThrows
    public Double getAvailableBalance() {
        Thread.sleep(5000);
        return strToDouble(availableBalance.getText());
    }

    @SneakyThrows
    public void getAvailableBalanceBefore(){
        Thread.sleep(1000*10);
        availableBalanceBeforeTran=getAvailableBalance();
    }

    public double strToDouble(String data){
        if(data.contains(",")) {
            data = data.replace(",", "");
        }
        return Double.parseDouble(data);
    }

    @SneakyThrows
    public void clickViewMyFundsOnGoWealthPage(String screenShotName) {
        Thread.sleep(5000);
        waitUntilElementClickable(viewMyFundsOnGoWealthPage);
        clickElement(viewMyFundsOnGoWealthPage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
//        language=System.getProperty("language");
    }

    @SneakyThrows
    public boolean chooseAFundIsNoZero() {
        if (fundList.size()==0){
            return false;
        }
        Thread.sleep(2000);
        MobileElement ele;
        for (int i = 1; i < 4; i++) {
            if(language.contains("zh")){
                ele = driver.findElementByXPath("//*[@text='比重']/following-sibling::android.view.ViewGroup["+i+"]/android.widget.TextView[2]");

            }else{
                ele = driver.findElementByXPath("//*[@text='Weight']/following-sibling::android.view.ViewGroup["+i+"]/android.widget.TextView[2]");

            }

            if(!ele.getText().equals("0%")){
                clickElement(ele);
                Thread.sleep(2000);
                return true;
            }
        }
        return  false;
    }

    @SneakyThrows
    public void clickBuyButtonOnFundDetailPage(String screenShotName) {
        fundName=fundNameOnFundDetail.getText();
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(buyBtnOnFeaturedFunds);
    }


    @SneakyThrows
    public boolean enterBuyFundInvestmentAmount(String oneTime, String type, String monthly) {
        buyFundMoneyType=type;
        if(strToDouble(oneTime)<100){
            oneTime="110";
        }
        if(type.equals("USD")){
            clickElement(HKDUSDSwitch);
            Thread.sleep(2000);
            clickElement(investmentInputUSD);
            clearAndSendKeys(investmentInputUSD,oneTime);
        }else{
            clickElement(investmentInput);
            clearAndSendKeys(investmentInput,oneTime);
            if (!monthly.equals("00")) {
                if (strToDouble(monthly) < 100) {
                    monthly = "110";
                }
                clickElement(monthlyInvestmentInput);
                clearAndSendKeys(monthlyInvestmentInput, monthly);
                inputEnter(monthlyInvestmentInput);
            }

        }
        transactionAmount=strToDouble(oneTime);
        Thread.sleep(2000);
        clickElement(nextButton);
        return buyFundNameOnReviewPage.getText().equals(fundName);
    }

    @SneakyThrows
    public void confirmReviewAndConfirmOrder() {
        Thread.sleep(2000);
        confirmReviewAndSlideToConfirmOrder();
    }

    @SneakyThrows
    public boolean verifyBuyFundSuccess(String screenShotName) {
        Thread.sleep(10000);
        takeScreenshot(screenShotName);
        waitUntilElementVisible(congratulations);
        if (verifyElementExist(congratulations) && amountOnProcessPage.getText().contains(buyAndSellAmount)) {
            clickElement(doneButton);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifyBuyFundOrderStatus(String screenShotName, String screenShotName01) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        chooseSameFund();
        Thread.sleep(5000);
        waitUntilElementVisible(orderStatus);
        clickElement(orderStatus);
        waitUntilElementVisible(sellOrBuyFundOnOrderStatus);
        takeScreenshot(screenShotName01);
        boolean flag = sellOrBuyFundOnOrderStatus.getText().startsWith("Buy") || sellOrBuyFundOnOrderStatus.getText().startsWith("買入");
        boolean flagName = sellOrBuyFundOnOrderStatus.getText().contains(fundName);
        return  orderStatusDetailAmount.getText().contains(buyAndSellAmount) && flag && flagName;
    }


    @SneakyThrows
    public void chooseSameFund(){
        Thread.sleep(5000);
//        MobileElement ele = driver.findElementByXPath("//*[@text='"+fundName+"']");
//        scrollUpToFindElement(ele,5,2);
//        clickElement(ele);
        Thread.sleep(5000);
        String namePath = "//android.widget.TextView[contains(@text,'" + fundName + "')]";
        scrollUpToFindElement(By.xpath(namePath), 5, 2);
        clickElement(driver.findElement(By.xpath(namePath)));

    }

    @SneakyThrows
    public void orderStatusPageBackToHomePage() {
        Thread.sleep(2000);
        clickElement(backBtnOnOrderStatus);
        Thread.sleep(2000);
        clickElement(backBtnOnOrderStatus);
        Thread.sleep(5000);
        waitUntilElementClickable(homeBtn);
        clickElement(homeBtn);
        Thread.sleep(5000);
    }

    public  boolean verifyBalance(){
        availableBalanceAfterTran= getAvailableBalance();
        return availableBalanceAfterTran == availableBalanceBeforeTran - transactionAmount;
    }

    @SneakyThrows
    public boolean buyFundOneTimeInvestment() {
        Thread.sleep(5000);
//        if(language.contains("zh")){
//            return strToDouble(oneTimeInvestmentToday.getText().split(" ")[0])==transactionAmount;
//        }else{
//            String moneyType=oneTimeInvestmentToday.getText().split(" ")[0];
//            amountStr=oneTimeInvestmentToday.getText().split(" ")[1];
//            return moneyType.equals(buyFundMoneyType) && strToDouble(amountStr) == transactionAmount;
//        }
        return buyAmountOnReviewPage.getText().contains(String.valueOf(transactionAmount));
    }

    @SneakyThrows
    public boolean verifyBuyFundByRecord() {
        clickElement(traderNameAtHome);
        Thread.sleep(5000);
        amountStr=exchangeAmountInDetails.getText().split("HKD")[1];
        String fxAmount=foreignCurrencyExchangeAmount.getText();
        return strToDouble(amountStr) == transactionAmount && strToDouble(amountStr) + strToDouble(fxAmount) == 0;
    }

    @SneakyThrows
    public boolean gotoTotalBalancePage() {
        Thread.sleep(5000);
        clickElement(totalBalance);
        Thread.sleep(2000);
        return verifyElementExist(USDBalance);
    }

    public void verifyTotalBalanceBeforeTransaction() {
        USDBalanceBeforeTran=getUSDBalance();
    }

    public Double getUSDBalance(){
        return strToDouble( USDBalance.getText().split("USD")[1]);
    }

    @SneakyThrows
    public void totalBalancePageBackToHomePage() {
        Thread.sleep(2000);
        clickElement(backBtnOnTotalBalance);
    }

    @SneakyThrows
    public void gotoUSDBalancePage() {
        Thread.sleep(2000);
        USDBalanceAfterTran=getUSDBalance();
        clickElement(USDBalance);
    }

    public boolean verifyUSDBalanceAfterTransaction() {
        clickElement(firstUSDExchange);
        double USDAmount =strToDouble(USDAmountOnTranDetail.getText());
        return USDAmount + transactionAmount == 0 && USDBalanceBeforeTran + USDAmount == USDBalanceAfterTran;
    }

    @SneakyThrows
    public boolean clickSellButtonOnFundDetailPage(String screenShotName) {
        Thread.sleep(2000);
        fundName = fundNameOnFundDetail.getText();
        takeScreenshot(screenShotName);
        clickElement(sellBtnOnFundDetailPage);
        Thread.sleep(2000);
        return sellBtnOnFundDetailPage.getText().equals("Sell") || sellBtnOnFundDetailPage.getText().equals("賣出");
    }

    public Double getSellAvailableBalance(){
//
//        if(language.contains("en")){
//            return strToDouble(sellAvailableBalance.getText().split("USD")[1]);
//        }else{
//            return  strToDouble(sellAvailableBalance.getText().split("美元")[0].split(":")[1]);
//        }
        String sellAvailableBalanceText = sellAvailableBalance.getText().substring(sellAvailableBalance.getText().indexOf("."));
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(sellAvailableBalanceText);
        sellAvailableBalanceText = m.replaceAll("").trim();

        return strToDouble(sellAvailableBalanceText);
    }

    @SneakyThrows
    public boolean enterSellFundAmountAndType(String amount,String type) {
        USDBalanceBeforeTran=getSellAvailableBalance();
        if(type.equals("part")){
            clickElement(sellInput);
            clearAndSendKeys(sellInput,amount);
            transactionAmount=strToDouble(amount);
        }else{
            Thread.sleep(2000);
            clickElement(sellAllBtn);
            Thread.sleep(2000);
            clickElement(redeemAllBtn);
            transactionAmount=USDBalanceBeforeTran;
        }
        fundName=sellFundNameOnSellPage.getText();
        clickElement(nextButton);
        return sellFundNameReviewPage.getText().equals(fundName);

    }

    @SneakyThrows
    public boolean verifySellFundBySuccess(String screenShotName) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        if (verifyElementExist(sellOrderCompleted) && verifyElementExist(transactionSuccessful)) {
            clickElement(doneButton);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifySellFundOrderStatus(String screenShotName, String screenShotName01) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        chooseSameFund();
        Thread.sleep(2000);
        clickElement(orderStatus);
        waitUntilElementVisible(sellOrBuyOnOrderStatus);
        takeScreenshot(screenShotName01);
        boolean flag = sellOrBuyOnOrderStatus.getText().startsWith("Sell") || sellOrBuyOnOrderStatus.getText().startsWith("賣出");
        boolean flagName = sellOrBuyOnOrderStatus.getText().contains(fundName);
        return orderStatusDetailAmount.getText().contains(buyAndSellAmount) && flag && flagName;
    }


    @SneakyThrows
    public boolean verifySellFundBalanceAfterTransaction() {
        Thread.sleep(2000);
        clickElement(backBtnOnOrderStatus);
        clickElement(sellBtnOnFundDetailPage);
        Thread.sleep(2000);
        USDBalanceAfterTran=getSellAvailableBalance();
//        if((Math.abs(USDBalanceBeforeTran-USDBalanceAfterTran-transactionAmount)<=0.1)){
//            return true;
//        }
        return Math.abs(USDBalanceBeforeTran - USDBalanceAfterTran - transactionAmount) <= 0.1;
    }

    @SneakyThrows
    public double getMonthlyStandingInstructionAmount() {
        Thread.sleep(2000);
        return strToDouble(monthlyStandingInstructionAmount.getText().split("HKD")[1]);

    }

    public void monthlyStandingInstructionAmountBeforeUpdate() {
        balanceBeforeTran=getMonthlyStandingInstructionAmount();
    }

    @SneakyThrows
    public boolean gotoMonthlyStandingInstructionEditPage(String screenShotName) {
        Thread.sleep(2000);
        clickElement(orderFundInFundList.get(randomNumber(orderFundInFundList.size())));
        Thread.sleep(2000);
        fundName=fundNameOnFundDetail.getText();
        monthlyStandingInstructionAmountBeforeUpdate();
        clickElement(monthlyStandingInstructionAmount);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        return verifyElementExist(monthlyInstructionInput);
    }

    @SneakyThrows
    public boolean enterMonthlyInstructionAmount(String type) {
        clickElement(monthlyInstructionInput);
        switch (type) {
            case "greater":
                clearAndSendKeys(monthlyInstructionInput, String.valueOf(balanceBeforeTran + 100));
                transactionAmount = (balanceBeforeTran + 100);

                break;
            case "less":
                clearAndSendKeys(monthlyInstructionInput, String.valueOf(balanceBeforeTran - 200));
                transactionAmount = (balanceBeforeTran - 200);
                break;
            case "zero":
                clearAndSendKeys(monthlyInstructionInput, "0");
                transactionAmount = 0.0;
                break;
            default:
                clearAndSendKeys(monthlyInstructionInput, "1000");
                transactionAmount = 1000;
                break;
        }

        fundName=fundNameOnBuyPage.getText();
        clickElement(nextButton);
        if(type.equals("zero")){
            Thread.sleep(2000);
            clickElement(confirmBtn);
        }
        return buyFundNameOnReviewPage.getText().equals(fundName);
    }


    @SneakyThrows
    public boolean verifyEquivalentMonthlyInvestment() {
        Thread.sleep(3000);
//        if(language.contains("zh")){
//            return strToDouble(equivalentMonthlyInvestment.getText().split("港元")[0])==transactionAmount;
//
//        }else{
//            return strToDouble(equivalentMonthlyInvestment.getText().split("on")[0].split("HKD")[1])==transactionAmount;
//        }
       return equivalentMonthlyInvestment.getText().contains(String.valueOf(transactionAmount));
    }

    @SneakyThrows
    public boolean verifyUpdateMonthlyInstructionSuccess(String screenShotName) {
        Thread.sleep(2000);
        if(verifyElementExist(congratulations)){
            takeScreenshot(screenShotName);
            Thread.sleep(2000);
            clickElement(doneButton);
            return true;
        }
        return false;
    }

    public boolean verifyMonthlyInstructionAmountChanged(String screenShotName) {
        chooseSameFund();
        takeScreenshot(screenShotName);
        return monthlyStandingInstructionAmount.getText().contains(buyAndSellAmount);
    }

    @SneakyThrows
    public void gotoOrderStatusPage() {
        Thread.sleep(2000);
        clickElement(orderStatus);
    }

    @SneakyThrows
    public boolean verifyMonthlyInstruction(String screenShotName) {
        Thread.sleep(2000);
        clickElement(standingInstruction);
        chooseSameFund();
        waitUntilElementVisible(fundNameOnStandingInstruction);
        takeScreenshot(screenShotName);
        return fundName.equals(fundNameOnStandingInstruction.getText()) && monthlyAmountOnStandingInstruction.getText().contains(buyAndSellAmount);


    }

    @SneakyThrows
    public void standingInstructionPageBackToFundDetailPage() {
        Thread.sleep(2000);
        clickElement(backBtnOnOrderStatus);
        if(transactionAmount!=0.0){
            Thread.sleep(5000);
            clickElement(backBtnOnOrderStatus);
        }
    }
    @SneakyThrows
    public void clickFeaturedFunds(String screenShotName) {
        Thread.sleep(2000);
//        waitUntilElementVisible(fundListText);
        scrollUpToFindElement(FeaturedFunds, 8, 3);
        clickElement(FeaturedFunds);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(firstFundOnInvestment);
    }


    @SneakyThrows
    public void clickShowRelevantFunds() {
        Thread.sleep(2000);
        scrollUpToFindElement(showRelevantFunds, 8, 8);
        clickElement(showRelevantFunds);
    }


    @SneakyThrows
    public void clickBuyOnFeaturedFund(String screenShotName) {
        Thread.sleep(5000);
        fundName = fundNameOnFeaturedFund.getText();
        clickElement(buySubscriptionFee);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void confirmReviewAndSlideToConfirmOrder() {
        scrollUpToFindElement(slideToConfirm, 8, 5);
        scrollUpToFindElement(iaoPage.getRadioBtn(), 8, 5);
        iaoPage.getRadioBtn().click();
        Thread.sleep(1000);
        // top-left
        int x = slideToConfirm.getLocation().x;
        int y = slideToConfirm.getLocation().y;
        // size
        int width = slideToConfirm.getRect().width;
        int x_start = x + 50;
        int x_end = x + width;
        int y_co = y + 2;
        TouchAction action =
                new TouchAction(driver)
                        .longPress(PointOption.point(x, y_co))
                        .moveTo(PointOption.point(x_end, y_co))
                        .release();
        action.perform();
        Thread.sleep(1000 * 10);
    }

    @SneakyThrows
    public void clickByText(String text) {
        Thread.sleep(5000);
        MobileElement ele = driver.findElementByXPath("//*[@text='" + text + "']");
        clickElement(ele);
    }

    @SneakyThrows
    public void clickByProducts(String products) {
        Thread.sleep(5000);
        MobileElement ele =
                driver.findElementByXPath(
                        "//android.view.ViewGroup[2]/android.view.ViewGroup[1]/*[starts-with(@text, '"
                                + products
                                + "')]");
        clickElement(ele);
    }

    @SneakyThrows
    public void clickRandomTopFund() {
        Thread.sleep(5000);
        logger.info("topFundsList:{}", topFundsList.size());
        Thread.sleep(5000);
        clickElement(topFundsList.get(randomNumber(topFundsList.size())));
    }

    @SneakyThrows
    public void clickRandomEquityFund() {
        Thread.sleep(5000);
        logger.info("equityFundsList:{}", equityFundsList.size());
        Thread.sleep(5000);
        clickElement(equityFundsList.get(randomNumber(equityFundsList.size())));
    }

    @SneakyThrows
    public void clickRandomBondFund() {
        Thread.sleep(5000);
        logger.info("bondFundsList:{}", bondFundsList.size());
        Thread.sleep(5000);
        clickElement(bondFundsList.get(randomNumber(bondFundsList.size())));
    }

    @SneakyThrows
    public void clickRandomMixedFund() {
        Thread.sleep(5000);
        logger.info("mixedFundsList:{}", mixedFundsList.size());
        Thread.sleep(5000);
        clickElement(mixedFundsList.get(randomNumber(mixedFundsList.size())));
    }

    @SneakyThrows
    public void randomRelatedProducts() {
        Thread.sleep(5000);
        logger.info("relatedProductsList :{}", relatedProductsList.size());
        Thread.sleep(5000);
        clickElement(relatedProductsList.get(randomNumber(relatedProductsList.size())));
    }


    public int randomNumber(int num) {

        return (int) (Math.random() * num);
//        return new Random().nextInt(num);
    }

    @SneakyThrows
    public boolean verifySellGoalOrderStatus(String screenShotName) {
        Thread.sleep(2000);
        clickElement(orderStatus);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        boolean flag = sellOrBuyOnOrderStatus.getText().startsWith("Sell") || sellOrBuyOnOrderStatus.getText().startsWith("賣出");
        return orderStatusDetailAmount.getText().contains(GlobalVar.GLOBAL_VARIABLES.get("globalVarAmount")) && flag;
    }

    @SneakyThrows
    public boolean verifyBuyGoalOrderStatus(String screenShotName) {
        Thread.sleep(2000);
        clickElement(orderStatus);
        Thread.sleep(2000);
        clickElement(standingInstruction);
        Thread.sleep(2000);
        clickElement(firstOnStandingInstruction);
        waitUntilElementVisible(amountInStandingInstruction);
        takeScreenshot(screenShotName);
        return amountInStandingInstruction.getText().contains(amountStr);
    }

    public void enterNewMonthlyInvestment(String newMonthly,String screenShotName) {
        clickElement(newMonthlyInvestment);
        amountStr=newMonthly;
        clearAndSendKeys(newMonthlyInvestment,newMonthly);
        inputEnter(newMonthlyInvestment);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickViewFullFundList(String screenShotName) {
        clickElement(viewFullFundList);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void searchUSDFund(String screenShotName, String screenShotName01) {
        clickElement(allFunds);
        waitUntilElementVisible(USDFunds);
        takeScreenshot(screenShotName);
        clickElement(USDFunds);
        Thread.sleep(3000);
        takeScreenshot(screenShotName01);
    }

    @SneakyThrows
    public void clickRandomFund(String screenShotName) {
        Thread.sleep(5000);
        int size = randomNumber(fundListOnFullFundPage.size());
        if(size==4 && isRedMiNote12()){
            clickElement(fundListOnFullFundPage.get(size-1));
        }else{
            clickElement(fundListOnFullFundPage.get(size));
        }
//        clickElement(fundListOnFullFundPage.get(randomNumber(fundListOnFullFundPage.size())));
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }

    public void enterBuyFundOneTime(String oneTime) {
        clickElement(oneTimeInput);
        clearAndSendKeys(oneTimeInput, oneTime);
        inputEnter(oneTimeInput);
        buyAndSellAmount = oneTime;
    }

    public void enterBuyFundMonthly(String monthly) {
        if (isShow(monthlyInvestmentInput, 2)) {
            clickElement(monthlyInvestmentInput);
            clearAndSendKeys(monthlyInvestmentInput, monthly);
            inputEnter(monthlyInvestmentInput);
        }
    }

    @SneakyThrows
    public void clickNextBtn(String screenShotName, String screenShotName01) {
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        clickElement(nextButton);
        takeScreenshot(screenShotName01);
    }

    @SneakyThrows
    public boolean verifyBuyConfirmReviewAndOrder() {
        Thread.sleep(3000);
        waitUntilElementVisible(buyAmountOnReviewPage);
        boolean flagAmount = buyAmountOnReviewPage.getText().contains(buyAndSellAmount);
        boolean flagName = buyFundNameOnReviewPage.getText().equals(fundName);
        return flagAmount && flagName;
    }

    @SneakyThrows
    public void clickCurrencySwitchButton() {
        clickElement(HKDUSDSwitch);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void searchHKDFund(String screenShotName, String screenShotName01) {
        clickElement(allFunds);
        waitUntilElementVisible(HKDFunds);
        takeScreenshot(screenShotName);
        clickElement(HKDFunds);
        Thread.sleep(2000);
        takeScreenshot(screenShotName01);
    }

    @SneakyThrows
    public boolean clickUSDFundFromFundList(String screenShotName) {
        Thread.sleep(3000);
        for (MobileElement mobileElement : orderFundInFundList) {
            if (mobileElement.getText().contains("USD")) {
                clickElement(mobileElement);
                break;
            }
        }
        Thread.sleep(5000);
        if(!verifyElementExist(fundNameOnFundDetail)){
            scrollUpByPercent(67,  19);
            for (MobileElement mobileElement : orderFundInFundList) {
                if (mobileElement.getText().contains("USD")) {
                    clickElement(mobileElement);
                    break;
                }
            }
        }
        takeScreenshot(screenShotName);
        Thread.sleep(10000);
        return verifyElementExist(fundNameOnFundDetail);
    }

    @SneakyThrows
    public boolean clickHKDFundFromFundList(String screenShotName) {
        for (MobileElement mobileElement : orderFundInFundList) {
            if (!mobileElement.getText().contains("USD")) {
                clickElement(mobileElement);
                break;
            }
        }
        Thread.sleep(5000);
        if(!verifyElementExist(fundNameOnFundDetail)){
            scrollUpByPercent(67,  19);
            for (MobileElement mobileElement : orderFundInFundList) {
                if (!mobileElement.getText().contains("USD")) {
                    clickElement(mobileElement);
                    break;
                }
            }
        }
        takeScreenshot(screenShotName);
        Thread.sleep(5000);
        return verifyElementExist(fundNameOnFundDetail);
    }

    @SneakyThrows
    public boolean findCanSellHKDFund(String screenShotName) {
        for (int i = 0; i < orderFundInFundList.size(); i++) {
            if(orderFundInFundList.get(i).getText().contains("HKD") && !weightFundList.get(i).getText().contains("00%")){
                orderFundInFundList.get(i).click();
                break;
            }
        }
        if(!isShow(sellBtnOnFundDetailPage)){
            scrollUpByPercent(67,  22);
            for (int i = 0; i < weightFundList.size(); i++) {
                if(orderFundInFundList.get(i).getText().contains("HKD") && !weightFundList.get(i).getText().contains("00%")){
                    orderFundInFundList.get(i+1).click();
                    break;
                }
            }
        }
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        return verifyElementExist(sellBtnOnFundDetailPage);
    }

    @SneakyThrows
    public boolean findCanSellUSDFund(String screenShotName) {
        for (int i = 0; i < orderFundInFundList.size(); i++) {
            if(orderFundInFundList.get(i).getText().contains("USD") && !weightFundList.get(i).getText().contains("00%")){
                orderFundInFundList.get(i).click();
                break;
            }
        }
        if(!verifyElementExist(sellBtnOnFundDetailPage)){
            scrollUpByPercent(67,  22);
            for (int i = 0; i < weightFundList.size(); i++) {
                if(orderFundInFundList.get(i).getText().contains("USD") && !weightFundList.get(i).getText().contains("00%")){
                    orderFundInFundList.get(i+1).click();
                    break;
                }
            }
        }
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        return verifyElementExist(sellBtnOnFundDetailPage);
    }

    @SneakyThrows
    public void sellFundOneOffRedemption(String amount) {
        if (amount.equals("all")) {
            Thread.sleep(2000);
            clickElement(sellAllBtn);
            Thread.sleep(2000);
            clickElement(redeemAllBtn);
            buyAndSellAmount = sellAvailableBalance.getText().split("D")[1];;
        } else {
            clickElement(sellInput);
            clearAndSendKeys(sellInput, amount);
            buyAndSellAmount = amount;
        }
    }

    @SneakyThrows
    public boolean verifySellConfirmReviewAndOrder() {
        Thread.sleep(10000);
        waitUntilElementVisible(sellAmountReviewPage);
        boolean flagAmount = sellAmountReviewPage.getText().contains(buyAndSellAmount);
        boolean flagName = sellFundNameReviewPage.getText().equals(fundName);
        return flagAmount && flagName;
    }


    @SneakyThrows
    public boolean enterMonthlyInstruction(String screenShotName,String screenShotName01) {
        clickElement(monthlyInstructionInput);
        clearAndSendKeys(monthlyInstructionInput,"10");
        inputEnter(monthlyInvestmentInput);
        takeScreenshot(screenShotName);
        clearAndSendKeys(monthlyInstructionInput, String.valueOf(balanceBeforeTran + 100));
        inputEnter(monthlyInvestmentInput);
        buyAndSellAmount = String.valueOf(balanceBeforeTran + 100);
        takeScreenshot(screenShotName01);
        clickElement(nextButton);
        Thread.sleep(2000);
        return buyFundNameOnReviewPage.getText().equals(fundName);
    }

}
