package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.OpenAccountPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class OpenAccountSteps {

    OpenAccountPage openAccountPage;


    public OpenAccountSteps() {
        openAccountPage = new OpenAccountPage();
    }

    @And("Click account opening button on the home page")
    public void clickOpenAccountBtn(Map<String, String> data) {
        openAccountPage.openAccountBtn(data.get("screenShotName"));
    }

    @Then("See account opening requirements and click it")
    public void verifyRequirements(Map<String, String> data) {
        openAccountPage.clickEligibilityText();
        openAccountPage.verifyEligibilityText(data.get("screenShotName"));
    }

    @And("Enter an incorrectPhone mobile phone number")
    public void verifyPhoneNum(Map<String, String> data) {
        assertThat(openAccountPage.inputIncorrectPhone(data.get("incorrectPhone"),data.get("screenShotName"))).isTrue();
    }

    @And("Enter an incorrectEmail email address")
    public void verifyEmailAddress(Map<String, String> data) {
       assertThat(openAccountPage.inputIncorrectEmail(data.get("incorrectEmail"),data.get("screenShotName"))).isTrue();

    }

    @Then("Checking terms of use of app")
    public void checkTermsOfUse(Map<String, String> data) {
        openAccountPage.getTermsLocation();
        assertThat(openAccountPage.clickTermsOfUse(data.get("screenShotName"),data.get("screenShotName1"),data.get("screenShotName2"))).isTrue();
        assertThat(openAccountPage.clickAccountTerms(data.get("screenShotName3"),data.get("screenShotName4"),data.get("screenShotName5"))).isTrue();
        assertThat(openAccountPage.clickPrivacyPolicy(data.get("screenShotName6"),data.get("screenShotName7"),data.get("screenShotName8"))).isTrue();
    }

    @And("Verify return dormant account text")
    public void verifyDormantAccount(Map<String, String> data) {
        assertThat(openAccountPage.verifyDormantText(data.get("screenShotName"))).isTrue();
    }

    @And("Verify goWealth and Transfer whether to show")
    public void verifyWealthAndTransfer(Map<String, String> data) {
        assertThat(openAccountPage.verifyWealthAndTranIcon(data.get("screenShotName"))).isTrue();
        assertThat(openAccountPage.checkWealthBalanceDetail(data.get("screenShotName1"))).isTrue();
    }

    @Then("Login failed and give error message")
    public void getErrorMessage(Map<String, String> data) {
        assertThat(openAccountPage.getLoginErrorMessage(data.get("screenShotName"))).isTrue();

    }
}
