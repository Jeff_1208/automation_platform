package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.iao.GoalSettingOrderPage;
import com.welab.automation.projects.wealth2.pages.iao.GoalSettingPage;
import com.welab.automation.projects.wealth2.pages.iao.OrderPlace3GoalPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderPlacementSteps {
    GoalSettingOrderPage goalSettingOrderPage;
    CommonPage commonPage;
    GoalSettingPage goalSettingPage;
    OrderPlace3GoalPage orderPlace3GoalPage;

    public OrderPlacementSteps() {
        goalSettingOrderPage = new GoalSettingOrderPage();
        commonPage = new CommonPage();
        goalSettingPage = new GoalSettingPage();
        orderPlace3GoalPage = new OrderPlace3GoalPage();
    }

    private static int oneTimeInvestmentValue;

    @And("I buy the fund without confirm")
    public void iBuyTheFundWithoutConfirm(DataTable dataTable) {
        List<Map<String, String>> data = dataTable.asMaps();
        String currency = data.get(0).get("currency");
        String portfolioName = goalSettingOrderPage.getPortfolioName();
        // Buy
        String actualPortfolioName = goalSettingOrderPage.getJumpPagePortfolioName();
        assertThat(actualPortfolioName).startsWith(portfolioName);
        goalSettingOrderPage.selectCurrency(currency);
        goalSettingOrderPage.getNextButton().click();
    }

    @And("I buy the fund")
    public void iBuyTheFund(DataTable dataTable) {
        List<Map<String, String>> data = dataTable.asMaps();
        String currency = data.get(0).get("currency");
        String portfolioName = goalSettingOrderPage.getPortfolioName();
        // Buy
        String actualPortfolioName = goalSettingOrderPage.getJumpPagePortfolioName();
        assertThat(actualPortfolioName).startsWith(portfolioName);
        goalSettingOrderPage.selectCurrency(currency);
        goalSettingOrderPage.reviewOrder();
    }

    @And("I quit the ordering process but go back")
    public void iQuitTheOrderingProcessButGoBack() {
        commonPage.clickXIcon();
        goalSettingOrderPage.getNotNow().click();
    }

    @Then("I still stay on review and confirm order page")
    public void iStillStayOnReviewAndConfirmOrderPage() {
        assertThat(goalSettingOrderPage.getReviewAndConfirmOrder().isDisplayed()).isEqualTo(true);
    }

    @And("I slide to confirm the order")
    public void iSlideToConfirmTheOrder() {
        goalSettingOrderPage.slideToConfirmOrder();
    }

    @Then("I can see the error message")
    public void iCanSeeTheErrorMessage() {
        String msg = goalSettingOrderPage.getErrorMsg();
        goalSettingOrderPage.clickElement(goalSettingOrderPage.getBack());
        assertThat(msg).as("Error: " + msg).isNotEmpty();
    }

    @Then("I can see the insufficient amount error message")
    public void iCanSeeTheSpecificErrorMessage() {
        String msg = goalSettingOrderPage.getErrorMsg();
        assertThat(msg).as("Error: " + msg).isEqualTo("There is insufficient amount in your account");
    }

    @Then("I can see the submit error message")
    public void iCanSeeTheSubmitErrorMessage() {
        String msg = goalSettingOrderPage.getErrorMsg();
        assertThat(msg).as("Error: " + msg).startsWith("Submit error");
        goalSettingOrderPage.getBack().click();
    }

    @Then("I can see the congratulation page")
    public void iCanSeeTheCongratulationPage() {
        goalSettingOrderPage.waitUntilElementVisible(goalSettingOrderPage.getCongratulation(), 30);
        goalSettingOrderPage.getReferenceNumber();
        goalSettingOrderPage.clickElement(goalSettingOrderPage.getDone());
        goalSettingOrderPage.waitUntilElementClickable(goalSettingOrderPage.getProcessing());
        goalSettingOrderPage.getHeaderBackBtn().click();
    }

    @And("I quit the ordering process")
    public void iQuitTheOrderingProcess() {
        commonPage.clickXIcon();
        goalSettingOrderPage.getQuit().click();
    }

    @And("I review the order")
    public void iReviewTheOrder() {
        goalSettingOrderPage.clickNext();
    }

    @And("I check Subscription Fee")
    public void iCheckSubscriptionFee() {
        Boolean txtStatus = goalSettingOrderPage.getSubscriptionFeeStatus();
        assertThat(txtStatus).isTrue();
    }

    @And("I check Starting From")
    public void iCheckStartingFrom() {
        Boolean txtStatus = goalSettingOrderPage.getStartingFrom();
        assertThat(txtStatus).isTrue();
    }

    @And("I click back to home button")
    public void iClickBackToHomeButton() {
        goalSettingOrderPage.getBack().click();
    }

    @Then("I click View update recommendation")
    public void iClickUpRecommendation() {
        goalSettingOrderPage.clickUpRecommen();
    }

    @Then("I slide to right and click Keep existing one")
    public void iClickKeepexistingone() {
        goalSettingOrderPage.slideToclickKeepone();
    }

    @And("^I click edit Button and set ([^\"]\\S*) monthly investment plan$")
    public void iClickEditButtonAndSetMonthly(String monthlyValue) {
        goalSettingOrderPage.clickEditButton();
        goalSettingOrderPage.updateMonthlyInvest(monthlyValue);
    }

    @And("^I click the order of goal ([^\"]\\S*) with the status of on track or off track$")
    public void iClickOrder(String targeName) {
        if (targeName.contains("Reach")) {
            goalSettingOrderPage.clickReachGoal(targeName);
            assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
        } else if (targeName.contains("Build")) {
            goalSettingOrderPage.clickBuild(targeName);
            assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
        } else if (targeName.contains("Achieve")) {
            goalSettingOrderPage.clickAchieveGoal(targeName);
            assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
        }
    }

    @And("^I click edit Button and update  monthly investment plan ([^\"]\\S*)$")
    public void iClickEditButtonAndSetTargeValue(String targeValue) {
        goalSettingOrderPage.clickEditButton();
        goalSettingOrderPage.updateMonthlyInvest(targeValue);
    }

    @And(
            "^I click Build My Investment Habit edit Button and update  monthly investment plan ([^\"]\\S*)$")
    public void iclickBuildButtonAndSetTime(String time) throws Exception {
        goalSettingOrderPage.clickEditButton();
        goalSettingOrderPage.setMyInvestingHabitGoal(time);
    }

    @Then("I click View updated recommendation")
    public void iClickUpRecommendationNew() {
        goalSettingOrderPage.clickUpRecommen();
    }

    @And("I click View New Portfolio and buy it")
    public void iClickViewNewPortfolio() {
        goalSettingOrderPage.clickViewNewPortlioAndBuy();
    }

    @And("I click View Portfolio")
    public void iClickViewPortfolio() {
        goalSettingOrderPage.clickViewPortlio();
    }

    @And("I click buy button")
    public void iClickBuyBtn() {
        goalSettingOrderPage.clickBuyBtn();
    }

    @And("I review and confirm order util look next button")
    public void iReviewAndConfirmOrder() {
        goalSettingOrderPage.reviewNewOrder();
    }

    @And("I click keep existing one and confirm it")
    public void iKeepExistingOneAndConfirm() {
        goalSettingOrderPage.clickKeepExistingOneAndConfirm();
    }

    @And("I click confirm button and slide to confirm order")
    public void iClickConfirmAndSlideToConfirm() {
        goalSettingOrderPage.clickConfirmAndSlideToConfirm();
        //goalSettingOrderPage.clickElement(goalSettingOrderPage.getDone());
    }

    @And("I click edit Button")
    public void iclickEditButton() {
        goalSettingOrderPage.clickEditButton();
    }

    @And("^I review my goal detail page ([^\"]\\S*)$")
    public void iReviewGoalDetail(String targeName) {
        goalSettingOrderPage.waitAndReviewGoalName(targeName);
    }

    @And("I click OK button")
    public void iClickOK() {
        goalSettingOrderPage.clickOKButton();
    }

    @And("I click YES button")
    public void iClickYES() {
        goalSettingOrderPage.clickYESButton();
    }

    @And("I click Sell button")
    public void iClickSell() {
        goalSettingPage.clickSellBtn();
    }

    @And("I edit Sell Value")
    public void iEditSellValue(Map<String,String>data) {
        goalSettingOrderPage.clearSellInputEdit();
//        String name = goalSettingOrderPage.getPortfolioNameText().getText();
//        String balance = goalSettingOrderPage.getAvailableBalance().getText();
        goalSettingOrderPage.editSellValue(data.get("sellValue"),data.get("screenShotName"));
//        assertThat(name).isEqualTo(goalSettingOrderPage.getPortfolioNameText().getText());
//        assertThat(balance).isEqualTo(goalSettingOrderPage.getAvailableBalance().getText());
        goalSettingOrderPage.clickSellNextBtn();
    }

    @And("I Slide to confirm Sell And Click Done")
    public void iSlideToConfirmSell() {
        goalSettingOrderPage.SlideToConfirmSellOrder();
        goalSettingOrderPage.clickDoneBtn();
    }

    @And("^I click the order all of goal with the status of ([^\"]\\S*)$")
    public void iClickProcessingOrder(String orderStatus) {
        switch (orderStatus) {
            case "Processing":
                goalSettingOrderPage.clickProcessingOrder();
                assertThat(goalSettingOrderPage.getProcessing().getText()).isEqualTo("Processing");
                break;
            case "Ontrack":
                goalSettingOrderPage.clickOnTrackOrder();
                break;
            case "Offtrack":
                goalSettingOrderPage.clickOffTrackOrder();
                break;
            case "Achieved":
                goalSettingOrderPage.clickAchievedOrder();
                break;
            case "Completed":
                goalSettingOrderPage.clickCompletedOrder();
                break;
        }
    }

    @Then("I click order status icon")
    public void iClickOrderStatus() {
        goalSettingOrderPage.clickOrderStatus();
        assertThat(goalSettingOrderPage.getOrderStatusPage().getText()).isEqualTo("Order status");
    }

    @And("Verify orders by dates from newest to oldest")
    public void verifyTimeNewestToOldest() {
        boolean isTimeOrderTure = goalSettingOrderPage.verifyGoalTime();
        assertThat(isTimeOrderTure).isTrue();
    }

    @And(
            "Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance")
    public void iVerifyMoPoStatusReBa() {
        boolean orderOtherStrTure = goalSettingOrderPage.verifyMoPoStatus();
        assertThat(orderOtherStrTure).isTrue();
    }

    @And("I click any fund to verify fund documents")
    public void iClickFundAndVerifyDocuments() {
        goalSettingOrderPage.clickFundAndVerifyDocuments();
        assertThat(goalSettingOrderPage.getDownloadSuccess().getText()).isEqualTo("Download Completed");
    }

    @And("I click standing instruction tab and verify monthly subscription instructions")
    public void iClickStandInstruction() {
        goalSettingOrderPage.clickStandInstruction();
        assertThat(goalSettingOrderPage.getEachMonthInstruction().getText())
                .isEqualTo("Transaction on every  of each month");
    }

    @Then("I click Model portfolio monthly subscription instructions details")
    public void iClickInstructionsDetails() {
        goalSettingOrderPage.clickInstructionsDetails();
    }

    @And("I back goal edit page")
    public void iClickBackBtn() {
        goalSettingOrderPage.clickBackBtn();
    }

    @And("I click standing instruction tab and verify  instructions be cancelled")
    public void iclickDoneBtnAndVerifyCancel() {
        // goalSettingOrderPage.clickDoneBtn();
        goalSettingOrderPage.clickOrderStatus();
        assertThat(goalSettingOrderPage.clickStandInstruction()).isTrue();
    }

    @And("^I click goal name with ([^\"]\\S*)$")
    public void iClickGoalNameWith(String goalname) {
        goalSettingOrderPage.clickGoalNameWith(goalname);
    }


    @And("I click Sell All Button")
    public void iClickSellAllButton(Map<String, String> data) {
//        String name = goalSettingOrderPage.getPortfolioNameText().getText();
        String balance = goalSettingOrderPage.getAvailableBalance().getText();
        goalSettingOrderPage.globalVarAmount(balance.split("USD")[1]);
        goalSettingOrderPage.clickSellAllConfirm(data.get("screenShotName"));
//        assertThat(name).isEqualTo(goalSettingOrderPage.getPortfolioNameText().getText());
//        assertThat(balance).isEqualTo(goalSettingOrderPage.getAvailableBalance().getText());
        goalSettingOrderPage.clickSellNextBtn();
    }

    @And("I click Sell All Button only")
    public void iclickSellAllButtonOnly(Map<String,String>data) {
        goalSettingOrderPage.clickSellAllConfirm(data.get("screenShotName"));
        goalSettingOrderPage.clickSellNextBtn();
    }

    @And("I click Next Button")
    public void iClickNextButton() {
        goalSettingOrderPage.clickSellNextBtn();
    }

    @When("^I enter balance ([^\"]\\S*)$")
    public void iEnterBalance(String bValue) {
        goalSettingOrderPage.enterBalance(bValue);
    }

    @Then("I click  the button to re-edit the target")
    public void iClickReeditButton() {
        goalSettingOrderPage.clickAgainGoal();
    }

    @And("^I pass interface get current age ([^\"]\\S*) and ([^\"]\\S*)$")
    public void getCurrentAge(String user, String password) throws Exception {
        goalSettingOrderPage.judgeCurrentAge(user, password);
    }

    @When("^I verify Achieve Financial Freedom and set new ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
    public void verifyFinancialFreedomGoal(String age, String monthlyExpenses, String otherExpenses)
            throws Exception {
        goalSettingOrderPage.setFinancialFreedomGoal(age);
        assertThat(goalSettingOrderPage.getFFTAgeSliderText()).isEqualTo(age);
        assertThat(goalSettingOrderPage.judgeCurAgeLessThanReAge(age)).isTrue();
        goalSettingOrderPage.clickContinue();
        goalSettingOrderPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
        String totalMonthly = goalSettingOrderPage.getTotalMonthly();
        goalSettingOrderPage.calculateTargetWealth();
        assertThat(goalSettingOrderPage.getRetiredAgeLink()).isEqualTo("Retire at " + age);
        String editPageTotalMonthly = goalSettingOrderPage.getEditPageTotalMonthly();
        assertThat(totalMonthly).isEqualTo(editPageTotalMonthly);
        goalSettingOrderPage.getCurrentWealth();
        assertThat(oneTimeInvestmentValue).isEqualTo(goalSettingOrderPage.getCurrentWealth());
    }

    @And("I get one time investment value")
    public int getOneTimeInvestmentValue() {
        oneTimeInvestmentValue = goalSettingOrderPage.oneTimeInvestmentValue();
        return oneTimeInvestmentValue;
    }

    @And("I  retieve monthly investment value")
    public void retieveFeasible() {
        goalSettingOrderPage.retieveFeasibleMonthlyValue();
    }

    @And("I click SellBtn and cancel button")
    public void iClickSellBtn(Map<String, String> data) {
        goalSettingOrderPage.clickSellButton(data.get("screenShotName"));
        commonPage.clickCancelBtn();
    }

    @And("I click Sell All Button and cancel button")
    public void clickSellAllBtnAndCancelBtn(Map<String, String> data) {
        goalSettingOrderPage.clickSellAllButton(data.get("screenShotName"));
        commonPage.clickCancelBtn();
    }

    @And("I click SellBtn and Proceed button")
    public void clickSellBtnAndProceedBtn(Map<String,String >data) {
        goalSettingOrderPage.clickSellButton(data.get("screenShotName"));
        goalSettingOrderPage.clickProceedButton(data.get("screenShotName1"));
    }

    @Then("I can see Sell button will be disabled and goal is Processing")
    public void iCanSeeSellButtonWillBeDisabled(Map<String, String> data) {
        assertThat(goalSettingOrderPage.sellBtnUnClickable(data.get("screenShotName"))).isFalse();
        assertThat(goalSettingOrderPage.verifyGoalIsProcessing()).isTrue();
    }

    @And("I only can see Target wealth")
    public void onlySeeTargetWealth() {
        Boolean probability = goalSettingOrderPage.verifyExistProbability();
        assertThat(probability).isFalse();
        Boolean targetWealth = goalSettingOrderPage.verifyExistTargetWealth();
        assertThat(targetWealth).isTrue();
        Boolean goalHorizon = goalSettingOrderPage.verifyExistGoalHorizon();
        assertThat(goalHorizon).isFalse();
    }

    @And("I click Proceed button")
    public void iClickProceed(Map<String, String> data) {
        goalSettingOrderPage.clickProceedButton(data.get("screenShotName"));
    }

    @And("I edit Sell Value to over range")
    public void iEditSellValueToOverRange() {
        goalSettingOrderPage.editSellValueToOverRange();
        goalSettingOrderPage.verifyNextBtnUnClickable();
    }

    @And("I click edit Button and change monthlyInvestment")
    public void iClickEditBtnAndChangeMonthlyValue() {
        goalSettingOrderPage.clickEditButton();
        goalSettingOrderPage.iChangeMonthlyInvestment();
    }

    @And("I click goal with ([^\"]\\S*)$")
    public void iClickGoalWithGoalName(String goalName) {
        goalSettingOrderPage.clickGoalWithGoalName(goalName);
    }

    @Then("I Modify the goal title to the current time")
    public void iModifyTheGoalTitleToTheCurrentTime(Map<String, String> data) {
        goalSettingOrderPage.iModifyTheGoalTitleToTheCurrentTime(data.get("screenShotName"));
    }

    @And("I click Add New Goals link")
    public void clickAddNewGoalLink(Map<String, String> data) {
        orderPlace3GoalPage.scrollFindAddGoalLink(data.get("screenShotName"));
        orderPlace3GoalPage.clickAddNewGoalLink();
    }

    @And("I choose Achieve Financial Freedom")
    public void chooseAchieveFinancialFreedom(Map<String, String> data) {
        orderPlace3GoalPage.chooseAchieveFinancialFreedom(data.get("screenShotName"));
    }


    @And("I click Continue button")
    public void clickContinueBtn(Map<String, String> data) {
        orderPlace3GoalPage.clickContinueBtn(data.get("screenShotName"));
    }

    @And("I input Monthly expenses")
    public void inputMonthlyExpenses(Map<String, String> data) {
        orderPlace3GoalPage.inputMonthlyExpenses(data.get("monthlyExpenses"),data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("I click Calculate Target Wealth button")
    public void clickCalculateTargetWealthBtn(Map<String, String> data) {
        orderPlace3GoalPage.clickCalculateTargetWealthBtn(data.get("screenShotName"));
    }

    @And("I click View recommendation and save my goal button")
    public void clickSaveMyGoalBtn(Map<String, String> data) {
        orderPlace3GoalPage.clickSaveMyGoalBtn(data.get("screenShotName"));
    }

    @And("I click View Portfolio button on Portfolio Recommendation Page")
    public void clickViewPortfolioBtn(Map<String, String> data) {
        orderPlace3GoalPage.clickViewPortfolioBtn(data.get("screenShotName"));
    }

    @And("I verify have not enough money")
    public void verifyHaveNotEnoughMoney(Map<String, String> data) {
        boolean result = orderPlace3GoalPage.verifyHaveNotEnoughMoney(data.get("screenShotName"));
        assertThat(result).isTrue();
    }

    @And("^I click Next button on Scenario Analysis Page$")
    public void clickNextBtnOnScenarioAnalysisPage(Map<String, String> data) {
        orderPlace3GoalPage.clickNextBtnOnScenarioAnalysisPage(data.get("screenShotName"));
    }

    @And("^I click Next button on Portfolio Page$")
    public void clickNextBtnOnPortfolioPage(Map<String, String> data) {
        orderPlace3GoalPage.clickNextBtnOnPortfolioPage(data.get("screenShotName"));
    }

    @And("^I click Next button on Order Page$")
    public void clickNextBtnOnOrderPage(Map<String, String> data) {
        orderPlace3GoalPage.clickNextBtnOnOrderPage(data.get("screenShotName"));
    }

    @And("I click Next button on Review and confirm Page")
    public void iClickNextButtonOnReviewAndConfirmPage(Map<String, String> data) {
        orderPlace3GoalPage.clickNextButtonOnReviewAndConfirmPage(data.get("screenShotName"), data.get("screenShotName01"));
    }

//    @And("^I click Achieve Financial Freedom Button")
//    public void clickAchieveFinancialFreedomButton() {
//        goalSettingOrderPage.clickAchieveFinancialFreedomButton();
//    }

    @And("Modify the goalName to verify whether the display is consistent")
    public void modifyName(Map<String, String> data) {
        assertThat(goalSettingOrderPage.modifyName(data.get("goalName"), data.get("screenShotName"))).isTrue();
    }

    @And("Arrival order confirmation page")
    public void arrivalOrderConfirmationPage() {
        goalSettingOrderPage.arrivalOrderConfirmationPage();
    }

    @And("^Enter an amount ([^\"]\\S*) than or less than")
    public void setOneTimeInvestmentValuenvestment(String Greater) {
        goalSettingOrderPage.setOneTimeInvestmentValuenvestment(Greater);
    }

    @And("^I slide to ([^\"]\\S*)$")
    public void slideToAge(String age, Map<String, String> data) {
        orderPlace3GoalPage.slideToAge(age, data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("^I input 10000 in Monthly expenses text$")
    public void inputMonthlyExpensesText(Map<String, String> data) {
        orderPlace3GoalPage.inputMonthlyExpensesText(data.get("screenShotName06"), data.get("screenShotName07"));
    }

    @And("^I slide to confirm order in order confirm Page$")
    public void slideToConfirmOrder(Map<String, String> data) {
        orderPlace3GoalPage.slideToConfirmOrder(data.get("screenShotName16"), data.get("screenShotName17"), data.get("screenShotName18"));
    }

    @And("I verify the account only sell")
    public void verifyTheAccountOnlySell(Map<String, String> data) {
        boolean result = orderPlace3GoalPage.verifyTheAccountOnlySell(data.get("screenShotName19"));
        assertThat(result).isTrue();
    }

    @And("I setup Build One time investment today")
    public void iSetBuildOneTimeInvestmentToday(Map<String, String> data) {
        goalSettingOrderPage.setBuildOneTimeInvestmentToday(data.get("oneTime"),data.get("screenShotName"));
    }


    @And("I click Done button")
    public void clickDoneBtn(Map<String, String> data) {
        orderPlace3GoalPage.clickDoneBtn(data.get("screenShotName"));
    }

    @And("I click view goal with the status of OnTrack or OffTrack")
    public void iClickGoalWithStatusOnOrOffTrack(Map<String, String> data) {
        goalSettingOrderPage.clickGoalWithStatusOnOrOffTrack(data.get("screenShotName"));
    }
}
