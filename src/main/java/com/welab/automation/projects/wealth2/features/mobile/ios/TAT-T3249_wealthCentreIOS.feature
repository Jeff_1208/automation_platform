Feature: Wealth Application

  Background: Login
    Given Open WeLab App
    And wealth skip Root IOS
    And Login with user and password by params
      | user     | qatest101 |
      | password | Aa123456  |

  @VBWMIS-T704
  Scenario: Reg_Wealth_001 Wealth Center_Order status IOS
    Then  go to my Account Page IOS
      | screenShotName | Reg_Wealth_Centre_001_IOS_01 |
    Given go to wealth centre IOS
      | screenShotName | Reg_Wealth_Centre_001_IOS_02 |
    Then check Order Status IOS
      | screenShotName  | Reg_Wealth_Centre_001_IOS_03 |
      | screenShotName1 | Reg_Wealth_Centre_001_IOS_04 |
      | screenShotName2 | Reg_Wealth_Centre_001_IOS_05 |
    Then check Order Status Standing Instruction IOS
      | screenShotName  | Reg_Wealth_Centre_001_IOS_06 |
      | screenShotName1 | Reg_Wealth_Centre_001_IOS_07 |

  @VBWMIS-T705
  Scenario: Reg_Wealth_002 Wealth Center Review customer risk profile
    Then  go to my Account Page IOS
      | screenShotName | Reg_Wealth_Centre_002_IOS_01 |
    Given go to wealth centre IOS
      | screenShotName | Reg_Wealth_Centre_002_IOS_02 |
    Then check Review Customer Risk Profile IOS
      | screenShotName  | Reg_Wealth_Centre_002_IOS_03 |
      | screenShotName1 | Reg_Wealth_Centre_002_IOS_04 |
      | screenShotName2 | Reg_Wealth_Centre_002_IOS_05 |
      | screenShotName3 | Reg_Wealth_Centre_002_IOS_06 |
      | screenShotName4 | Reg_Wealth_Centre_002_IOS_07 |

  @VBWMIS-T706
  Scenario: Reg_Wealth_003 Wealth Center View record IOS
    Then  go to my Account Page IOS
      | screenShotName | Reg_Wealth_Centre_003_IOS_01 |
    Given go to wealth centre IOS
      | screenShotName | Reg_Wealth_Centre_003_IOS_02 |
    Then check View Record IOS
      | screenShotName  | Reg_Wealth_Centre_003_IOS_03 |
      | screenShotName1 | Reg_Wealth_Centre_003_IOS_04 |
      | screenShotName2 | Reg_Wealth_Centre_003_IOS_05 |
      | screenShotName3 | Reg_Wealth_Centre_003_IOS_06 |

#  @VBWMIS-T707
#  Scenario: Reg_Wealth_004 Wealth Center Tutorial IOS
#    Then  go to my Account Page IOS
#      | screenShotName | Reg_Wealth_Centre_004_IOS_01 |
#    Given go to wealth centre IOS
#      | screenShotName | Reg_Wealth_Centre_004_IOS_02 |
#    Then check Wealth Center Tutorial IOS
#      | screenShotName  | Reg_Wealth_Centre_004_IOS_03 |
#      | screenShotName1 | Reg_Wealth_Centre_004_IOS_04 |

  @VBWMIS-T708
  Scenario: Reg_Wealth_005 Wealth Center Review investment account profile IOS
    Then  go to my Account Page IOS
      | screenShotName | Reg_Wealth_Centre_005_IOS_01 |
    Given go to wealth centre IOS
      | screenShotName | Reg_Wealth_Centre_005_IOS_02 |
    Then check Review Investment Account Profile IOS
      | screenShotName  | Reg_Wealth_Centre_005_IOS_03 |
      | screenShotName1 | Reg_Wealth_Centre_005_IOS_04 |
      | screenShotName2 | Reg_Wealth_Centre_005_IOS_05 |
      | screenShotName3 | Reg_Wealth_Centre_005_IOS_06 |

  @VBWMIS-T709
  Scenario: Reg_Wealth_006 Wealth Center Promotion IOS
    Then  go to my Account Page IOS
      | screenShotName | Reg_Wealth_Centre_006_IOS_01 |
    Given go to wealth centre IOS
      | screenShotName | Reg_Wealth_Centre_006_IOS_02 |
    Then check Wealth Center Promotion IOS
      | screenShotName  | Reg_Wealth_Centre_006_IOS_03 |
      | screenShotName1 | Reg_Wealth_Centre_006_IOS_04 |
      | screenShotName2 | Reg_Wealth_Centre_006_IOS_05 |
      | screenShotName3 | Reg_Wealth_Centre_006_IOS_06 |



