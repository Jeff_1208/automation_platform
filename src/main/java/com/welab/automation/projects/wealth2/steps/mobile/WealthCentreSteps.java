package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.iao.WealthCentrePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class WealthCentreSteps {

  WealthCentrePage wealthCentrePage;

  public WealthCentreSteps(){
    wealthCentrePage = new WealthCentrePage();
  }

  @Given("Go To My Account")
  public void goToMyAccount(){
    wealthCentrePage.goToMyAccount();
  }

  @Then("Go To Wealth Center")
  public void goToWealthCenter(Map<String, String> data){
    wealthCentrePage.goToWealthCenter(data.get("screenShotName"));
  }

  @Then("to Wealth Center Page")
  public void toWealthCentrePage(Map<String, String> data){
    wealthCentrePage.toWealthCentrePage(data.get("screenShotName"));
  }

  @Then("Click Close Button")
  public void clickCloseButton(){
    wealthCentrePage.clickCloseButton();
  }

  @Then("Click Back Button")
  public void clickBackButton(){
    wealthCentrePage.clickBackButton();
  }

  @Then("Click Promotion Back Button")
  public void clickPromotionBackButton(){
    wealthCentrePage.clickPromotionBackButton();
  }

  @Then("open Order Status")
  public void openOrderStatus(Map<String, String> data) {
    wealthCentrePage.openOrderStatus(data.get("screenShotName"));
  }

  @Then("open First Order Detail")
  public void openFirstOrderDetail(Map<String, String> data) {
    wealthCentrePage.openFirstOrderDetail(data.get("screenShotName"));
  }

  @Then("open Prospectus Document")
  public void openProspectusDocument(Map<String, String> data) {
    wealthCentrePage.openProspectusDocument(data.get("screenShotName"));
  }

  @Then("open Order Status Standing Instruction")
  public void openOrderStatusStandingInstruction(Map<String, String> data) {
    wealthCentrePage.openOrderStatusStandingInstruction(data.get("screenShotName"));
  }

  @Then("check Order Status Standing Instruction")
  public void checkOrderStatusStandingInstruction(Map<String, String> data) {
    wealthCentrePage.checkOrderStatusStandingInstruction(data.get("screenShotName"));
  }

  @Then("open Review Customer Risk Profile")
  public void openReviewCustomerRiskProfile(Map<String, String> data) {
    wealthCentrePage.openReviewCustomerRiskProfile(data.get("screenShotName"));
  }

  @Then("go To Update Customer Risk Profile Page")
  public void goToUpdateCustomerRiskProfilePage(Map<String, String> data) {
    wealthCentrePage.goToUpdateCustomerRiskProfilePage(data.get("screenShotName"));
  }

  @Then("click My Risk Profile Back Button")
  public void clickMyRiskProfileBackButton() {
    wealthCentrePage.clickMyRiskProfileBackButton();
  }

  @Then("to Review Customer Risk Profile")
  public void toReviewCustomerRiskProfile(Map<String, String> data){
    wealthCentrePage.toReviewCustomerRiskProfile(data.get("screenShotName"));
  }

  @Then("open View Record")
  public void openViewRecord(Map<String, String> data) {
    wealthCentrePage.openViewRecord(data.get("screenShotName"));
  }

  @Then("check View Record Detail")
  public void checkViewRecordDetail(Map<String, String> data) {
    wealthCentrePage.checkViewRecordDetail(data.get("screenShotName"), data.get("screenShotName1"));
  }

  @Then("open Wealth Center Tutorial")
  public void openWealthCenterTutorial(Map<String, String> data) {
    wealthCentrePage.openWealthCenterTutorial(data.get("screenShotName"), data.get("screenShotName1"));
  }

  @Then("open Review Investment Account Profile")
  public void openReviewInvestmentAccountProfile(Map<String, String> data) {
    wealthCentrePage.openReviewInvestmentAccountProfile(data.get("screenShotName"));
  }

  @Then("open Wealth Center Promotion")
  public void openWealthCenterPromotion(Map<String, String> data) {
    wealthCentrePage.openWealthCenterPromotion(data.get("screenShotName"));
  }

  @Then("check Wealth Center Promotion Detail")
  public void checkWealthCenterPromotionDetail(Map<String, String> data) {
    wealthCentrePage.checkWealthCenterPromotionDetail(data.get("screenShotName"), data.get("screenShotName1"));
  }


  @And("wealth skip Root IOS")
  public void enableSkipRootIos() throws InterruptedException {
    wealthCentrePage.skipRootIos();
  }

  @Then("go to my Account Page IOS")
  public void goToMyAccountPageIOS(Map<String, String> data) {
    wealthCentrePage.goToMyAccountPageIOS(data.get("screenShotName" ));
  }

  @Then("click Close IOS")
  public void clickCloseIOS() {
    wealthCentrePage.clickCloseIOS();
  }

  @Then("click Back IOS")
  public void clickBackIOS() {
    wealthCentrePage.clickBackIOS();
  }

  @Then("go to wealth centre IOS")
  public void goToWealthCenterPageIOS(Map<String, String> data) {
    wealthCentrePage.goToWealthCenterPageIOS(data.get("screenShotName" ));
  }

  @Then("check Order Status IOS")
  public void chekcOrderStatusIOS(Map<String, String> data) {
    wealthCentrePage.chekcOrderStatusIOS(data.get("screenShotName"), data.get("screenShotName1"), data.get("screenShotName2"));
  }

  @Then("check Order Status Standing Instruction IOS")
  public void chekcOrderStatusStandingInstructionIOS(Map<String, String> data) {
    wealthCentrePage.chekcOrderStatusStandingInstructionIOS(data.get("screenShotName"), data.get("screenShotName1"));
  }

  @Then("check Review Customer Risk Profile IOS")
  public void checkReviewCustomerRiskProfileIOS(Map<String, String> data) {
    wealthCentrePage.checkReviewCustomerRiskProfileIOS(data.get("screenShotName"), data.get("screenShotName1"),
            data.get("screenShotName2"), data.get("screenShotName2"), data.get("screenShotName3"));
  }

  @Then("check View Record IOS")
  public void checkViewRecordIOS(Map<String, String> data) {
    wealthCentrePage.checkViewRecordIOS(data.get("screenShotName"), data.get("screenShotName1"),
            data.get("screenShotName2"), data.get("screenShotName3"));
  }

  @Then("check Wealth Center Tutorial IOS")
  public void checkWealthCenterTutorialIOS(Map<String, String> data) {
    wealthCentrePage.checkWealthCenterTutorialIOS(data.get("screenShotName"), data.get("screenShotName1"));
  }

  @Then("check Review Investment Account Profile IOS")
  public void checkReviewInvestmentAccountProfileIOS(Map<String, String> data) {
    wealthCentrePage.checkReviewInvestmentAccountProfileIOS(data.get("screenShotName"), data.get("screenShotName1"),
            data.get("screenShotName2"), data.get("screenShotName3"));
  }

  @Then("check Wealth Center Promotion IOS")
  public void checkWealthCenterPromotionIOS(Map<String, String> data) {
    wealthCentrePage.checkWealthCenterPromotionIOS(data.get("screenShotName"), data.get("screenShotName1"),
            data.get("screenShotName2"), data.get("screenShotName3"));
  }


}
