Feature:Foreign Exchange On WeLab app


  Background: Login with correct user and password
    Given Open WeLab App
#    And Check upgrade page appears
    And Enable skip Root
    When Login with user and password from properties
        #    And Login with user and password
#      | user     | swmcust00171/165 |
#      | password | Aa123321 |


#  @2 @3 @4
#  Scenario: Click on Forex trading Click Currency
#    Then I click transfer goto transfer page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then I check the Add Money Show
#    And I check the Foreign Exchange Show
#    And I goto the foreign exchange page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    And View Currency Message

#  @8 @9 @11
#  Scenario: Verify HKD transaction is successful
#    Then I click transfer goto transfer page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    And I goto the foreign exchange page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then I enter exchange money
#      | money          | 10                        |
#      | screenShotName | Reg_Wealth_010_Android_05 |
#    And I submitted my foreign exchange order
#    Then  through adb common input MSK password
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    And I verify the Foreign Exchange submitted order
#      | screenShotName | Reg_Wealth_010_Android_07 |
#    And I saw the submitted order
#    And I verify HKD transaction on process page
#    And I goto Total balance page
#    And I goto USD Balance page
#    And I saw the HKD order information
#    Then USD page back home page


#
#  @num9
#  Scenario: Verify USD transaction is successful
#    Then I click transfer goto transfer page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    And I goto the foreign exchange page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then I click change button change to USD transaction
#      | screenShotName | Reg_Wealth_010_Android_03 |
#    Then I enter exchange money
#      | money          | 10                        |
#      | screenShotName | Reg_Wealth_010_Android_04 |
#    And I submitted my foreign exchange order
#    Then  through adb common input MSK password
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    And I verify USD transaction on process page
#    And I goto Total balance page
#    And I goto USD Balance page
#    And I saw the USD order information
#    Then USD page back home page
#    Then I verify USD transaction detail



#  @7 @10
#  Scenario Outline: Verify that my input exceeds the available balance
#    Then I click transfer goto transfer page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then I check the Add Money Show
#    And I goto the foreign exchange page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then The entered amount is greater than the available balance <moreThan>
#    And I saw the prompt <moreThan>
#    Examples:
#      | moreThan |
#      | moreThan |
#      | equal    |
#      | lessThan |
#
#  Scenario: Verify minimum transaction amount
#    Then I click transfer goto transfer page
#      | screenShotName | Reg_Wealth_010_Android_01 |
#    And I goto the foreign exchange page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    Then I enter exchange money
#      | money          | 0                         |
#      | screenShotName | Reg_Wealth_010_Android_05 |
#    Then I saw the minimum transaction amount tips


#    @5
#  Scenario Outline: Verify exchange rate is expired
#    And I go to Foreign Exchange Page
#    Then I check the Add Money Show
#    And I entered the foreign exchange page
#    And I enter amount <amount>
#    And I check review button displayed for <number> mins
#    Then I clicked the review button
#    And I want to enter the <amount> while waiting for <number> minutes
#    Examples:
#      | amount    |number|
#      | 20      | 10     |
#
#    @6
#  Scenario Outline: Verify transaction is successful
#    And I go to Foreign Exchange Page
#    And I entered the foreign exchange page
#    Then I enter the <amount>
#    And I submitted my foreign exchange order
#    When Check whether the current page jumps
#    Examples:
#      | amount |
#      | 0 |




  @VBWMIS-T600
  Scenario:Reg_Foreign_Exchange_001 FX convert HKD to USD
    Then I click transfer goto transfer page
      | screenShotName | Reg_Foreign_Exchange_001_Android_01 |
    And I goto the foreign exchange page
      | screenShotName | Reg_Foreign_Exchange_001_Android_02 |
    Then I check wrong amount tips
      | screenShotName   | Reg_Foreign_Exchange_001_Android_03 |
      | screenShotName01 | Reg_Foreign_Exchange_001_Android_04 |
    Then I enter exchange money
      | money          | 900                                   |
      | screenShotName | Reg_Foreign_Exchange_001_Android_05   |
    And I submitted my foreign exchange order
    And I verify the Foreign Exchange submitted order
      | screenShotName | Reg_Foreign_Exchange_001_Android_06 |
    Then I verify Foreign Exchange Record
      | screenShotName | Reg_Foreign_Exchange_001_Android_06 |
      | screenShotName01 | Reg_Foreign_Exchange_001_Android_07 |


  @VBWMIS-T601
  Scenario:Reg_Foreign_Exchange_002 FX convert USD to HKD
    Then I click transfer goto transfer page
      | screenShotName | Reg_Foreign_Exchange_002_Android_01 |
    And I move eyeBtn to top right corner
    And I goto the foreign exchange page
      | screenShotName | Reg_Foreign_Exchange_002_Android_02 |
    Then I click change button change to USD transaction
      | screenShotName | Reg_Foreign_Exchange_002_Android_03 |
    Then I enter exchange money
      | money          | 10                                  |
      | screenShotName | Reg_Foreign_Exchange_002_Android_04 |
    And I submitted my foreign exchange order
    And I verify the Foreign Exchange submitted order
      | screenShotName | Reg_Foreign_Exchange_002_Android_05 |
    Then I verify Foreign Exchange Record
      | screenShotName | Reg_Foreign_Exchange_002_Android_06 |
      | screenShotName01 | Reg_Foreign_Exchange_002_Android_07 |




