package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.iao.DraftGoalPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class DraftGoalSteps {
    DraftGoalPage draftGoalPage;

    public DraftGoalSteps() {
        draftGoalPage = new DraftGoalPage();
    }

    @Then("I goto wealthPage")
    public void goToWealthPage(Map<String, String> data) {
        assertThat(draftGoalPage.goToWealthPage(data.get("screenShotName"))).isTrue();
    }

    @And("I click draft")
    public void clickDraft(Map<String, String> data) {
        draftGoalPage.clickDraft(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @When("I click edit button and update goalName")
    public void clickNameEdit(Map<String, String> data) {
        draftGoalPage.clickNameEdit();
        draftGoalPage.editGoalName(data.get("goalName"), data.get("screenShotName"));
    }

    @And("I click back icon to Wealth page")
    public void clickBackToWealthPage(Map<String, String> data) {
        draftGoalPage.backToWealthPage(data.get("screenShotName"));
    }

    @And("I verify goalName change success")
    public void goalNameChangeSuccess(Map<String, String> data) {
        boolean result = draftGoalPage.selectTargetGoalName(data.get("screenShotName"));
        assertThat(result).isTrue();
    }

    @And("I click Edit button")
    public void clickEditBtn(Map<String, String> data) {
        draftGoalPage.clickEditBtn(data.get("screenShotName"));
    }

    @And("^I click View recommendation & save my goal button$")
    public void clickViewRecommendationAndSaveMyGoalBtn(Map<String, String> data) {
        draftGoalPage.clickViewRecommendationAndSaveMyGoalBtn(data.get("screenShotName04"));
    }

    @And("I verify goal success and click goal")
    public void selectChangeGoalName( Map<String, String> data) {
        draftGoalPage.clickTargetGoal(data.get("screenShotName"));
    }

    @And("I click View Portfolio button")
    public void clickViewPortfolioBtn(Map<String, String> data) {
        draftGoalPage.clickViewPortfolioBtn(data.get("screenShotName"));
    }

    @And("I click Next button on Scenario Analysis page")
    public void clickNextBtnOnScenarioAnalysisPage(Map<String, String> data) {
        draftGoalPage.clickNextBtnOnScenarioAnalysisPage(data.get("screenShotName"));
    }

    @And("I click Next button on Portfolio page")
    public void clickNextBtnOnPortfolioPage(Map<String, String> data) {
        draftGoalPage.clickNextBtnOnPortfolioPage(data.get("screenShotName"));
    }

    @And("I click Next button on Order page")
    public void clickNextBtnOnOrderPage(Map<String, String> data) {
        draftGoalPage.clickNextBtnOnOrderPage(data.get("screenShotName"));
    }

    @And("I Review and confirm order")
    public void reviewAndConfirmOrder(Map<String, String> data) {
        draftGoalPage.reviewAndConfirmOrder(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("I slide to confirm order in order confirm page")
    public void slideToConfirmOrder(Map<String, String> data) {
        draftGoalPage.slideToConfirmOrder(data.get("screenShotName"));
    }

    @And("I click Delete button")
    public void clickDeleteBtn(Map<String, String> data) {
        draftGoalPage.clickDeleteBtn(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("I verify the goal with targetName have been delete")
    public void verifyGoalDeleted(Map<String, String> data) {
        boolean result = draftGoalPage.verifyGoalDeleted(data.get("screenShotName"));
        assertThat(result).isTrue();
    }

    @And("^I verify Portfolio Recommendation page normal$")
    public void verifyPortfolioRecommendationPageNormal() {
        boolean result = draftGoalPage.verifyPortfolioRecommendationPageNormal();
        assertThat(result).isTrue();
    }

    @And("^I click draft name with ([^\"]\\S*)$")
    public void iClickGoalNameWith(String targetName, Map<String, String> data) {
        draftGoalPage.clickGoalNameWith(targetName, data.get("screenShotName01"));
    }

    @And("I click view goal goto view goal page")
    public void clickViewGoal(Map<String, String> data) {
        assertThat(draftGoalPage.clickViewGoal(data.get("screenShotName"))).isTrue();
    }

    @And("I edit Reach Goal Name")
    public void editReachGoalName(Map<String, String> data) {
        draftGoalPage.editReachGoalName(data.get("goalName"), data.get("screenShotName"));
    }

    @Then("portfolio recommendation page back to wealth page")
    public void portfolioRecommendationPageBackToWealthPage(Map<String, String> data) {
        draftGoalPage.backToWealthPage(data.get("screenShotName"));
        draftGoalPage.backToWealthPage(data.get("screenShotName01"));
        draftGoalPage.backToWealthPage(data.get("screenShotName02"));
        draftGoalPage.backToWealthPage(data.get("screenShotName03"));
    }


    @And("I verify goal draft status")
    public void iVerifyGoalDraftStatus(Map<String, String> data) {
        assertThat(draftGoalPage.verifyGoalDraftStatus(data.get("screenShotName"))).isTrue();
    }

    @And("Verify goal success and click")
    public void verifyGoalSuccessAndClick(Map<String, String> data) {
        draftGoalPage.verifyGoalSuccessAndClick(data.get("screenShotName"),data.get("screenShotName01"));
    }

    @Then("Delete draft goal")
    public void deleteDraftGoal() {
        draftGoalPage.deleteGoalAndConfirm();
    }

}

