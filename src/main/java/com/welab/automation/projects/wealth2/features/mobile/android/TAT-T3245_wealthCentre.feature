Feature: Wealth Application

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    When Login with user and password from properties

  @VBWMIS-T612
  Scenario: Reg_Wealth_001 Wealth Center_Order status
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName | Reg_Wealth_Centre_001_Android_01 |
    Then open Order Status
      | screenShotName | Reg_Wealth_Centre_001_Android_02 |
    Then open First Order Detail
      | screenShotName | Reg_Wealth_Centre_001_Android_03 |
    Then open Prospectus Document
      | screenShotName | Reg_Wealth_Centre_001_Android_04 |
    Then Click Back Button
    Then Click Back Button
    Then open Order Status Standing Instruction
      | screenShotName | Reg_Wealth_Centre_001_Android_05 |
    Then check Order Status Standing Instruction
      | screenShotName | Reg_Wealth_Centre_001_Android_06 |

  @VBWMIS-T613
  Scenario: Reg_Wealth_002 Wealth Center Review customer risk profile
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName  | Reg_Wealth_Centre_002_Android_01 |
    Then open Review Customer Risk Profile
      | screenShotName  | Reg_Wealth_Centre_002_Android_02 |
    Then go To Update Customer Risk Profile Page
      | screenShotName  | Reg_Wealth_Centre_002_Android_03 |
    Then Click Close Button
    Then to Wealth Center Page
      | screenShotName  | Reg_Wealth_Centre_002_Android_04 |
    Then open Review Customer Risk Profile
      | screenShotName  | Reg_Wealth_Centre_002_Android_05 |
    Then Click Back Button
    Then to Wealth Center Page
      | screenShotName  | Reg_Wealth_Centre_002_Android_06 |

  @VBWMIS-T614
  Scenario: Reg_Wealth_003 Wealth Center View record
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName  | Reg_Wealth_Centre_003_Android_01 |
    Then open View Record
      | screenShotName  | Reg_Wealth_Centre_003_Android_02 |
    Then check View Record Detail
      | screenShotName  | Reg_Wealth_Centre_003_Android_03 |
      | screenShotName1 | Reg_Wealth_Centre_003_Android_04 |
    Then Click Close Button
    Then to Wealth Center Page
      | screenShotName  | Reg_Wealth_Centre_003_Android_05 |

  @VBWMIS-T615
  Scenario: Reg_Wealth_004 Wealth Center Tutorial
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName  | Reg_Wealth_Centre_004_Android_01 |
    Then open Wealth Center Tutorial
      | screenShotName  | Reg_Wealth_Centre_004_Android_02 |
      | screenShotName1 | Reg_Wealth_Centre_004_Android_03 |
#    Then Click Back Button
    Then to Wealth Center Page
      | screenShotName  | Reg_Wealth_Centre_004_Android_04 |

  @VBWMIS-T616
  Scenario: Reg_Wealth_005 Wealth Center Review investment account profile
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName | Reg_Wealth_Centre_005_Android_01 |
    Then open Review Investment Account Profile
      | screenShotName | Reg_Wealth_Centre_005_Android_02 |
    Then Click Close Button
    Then to Wealth Center Page
      | screenShotName | Reg_Wealth_Centre_005_Android_03 |
    Then open Review Investment Account Profile
      | screenShotName | Reg_Wealth_Centre_005_Android_04 |
    Then Click Back Button
    Then to Wealth Center Page
      | screenShotName | Reg_Wealth_Centre_005_Android_05 |

  @VBWMIS-T617
  Scenario: Reg_Wealth_006 Wealth Center Promotion
    Given Go To My Account
    Then Go To Wealth Center
      | screenShotName  | Reg_Wealth_Centre_006_Android_01 |
    Then open Wealth Center Promotion
      | screenShotName  | Reg_Wealth_Centre_006_Android_02 |
    Then check Wealth Center Promotion Detail
      | screenShotName  | Reg_Wealth_Centre_006_Android_03 |
      | screenShotName1 | Reg_Wealth_Centre_006_Android_04 |
    Then Click Promotion Back Button
    Then to Wealth Center Page
      | screenShotName  | Reg_Wealth_Centre_006_Android_05 |
