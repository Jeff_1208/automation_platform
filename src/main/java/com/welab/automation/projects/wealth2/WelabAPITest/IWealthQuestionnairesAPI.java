package com.welab.automation.projects.wealth2.WelabAPITest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.*;
import io.restassured.response.Response;

@Server(GlobalVar.WEALTH_PATH)
public interface IWealthQuestionnairesAPI {
    @Get(path = "/questionnaires/crpq", description = "Fetch CRPQ Questions")
    @Headers(key={"Accept-Language"}, value={"en-US"})
    Response getQuestionnariesCRPQ();

    @Post(path = "/questionnaires/answers", description = "submit questionnaires answers")
    Response postQuestionnaries(@Body String body);
}
