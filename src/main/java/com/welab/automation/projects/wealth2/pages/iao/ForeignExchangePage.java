package com.welab.automation.projects.wealth2.pages.iao;

import static com.welab.automation.framework.utils.Utils.logFail;
import static com.welab.automation.framework.utils.Utils.logPass;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.utils.TimeUtils;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForeignExchangePage extends AppiumBasePage {

    private final String pageName = "Foreign Exchange Page";
    CommonPage commonPage;

    public ForeignExchangePage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }

    private String youConvertAmount;
    private String youGetAmount;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transfer']"),
            @AndroidBy(xpath = "//android.widget.Button[@content-desc=\"存款與轉賬, tab, 3 of 5\"]/android.widget.TextView")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '存款與轉賬, tab, 3 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemTransferStack, tab, 3 of 5'")
    })
    private MobileElement homeForeignExchangeBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Add money']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='存款']")
    })
    @iOSXCUITFindBy(accessibility = "Add money")
    private MobileElement  addMoneyBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Foreign exchange']"),
            @AndroidBy(xpath = "//*[@text='外匯交易']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "Foreign exchange"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"外匯交易\"]")
    })
    private MobileElement foreignExchangeBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"convertSelector\"]/preceding-sibling::android.view.ViewGroup[1]/android.widget.EditText"),
    })
    @iOSXCUITFindBy(iOSNsPredicate = "value == \"You convert\" AND type == \"XCUIElementTypeTextField\"")
    private MobileElement youConvert;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='You get']/following-sibling::android.view.ViewGroup[2]/android.widget.EditText[1]")
    })
    @iOSXCUITFindBy(iOSNsPredicate = "value == \"You get\" AND type == \"XCUIElementTypeTextField\"")
    private MobileElement youGet;

    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Review']"))
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review'")
    private MobileElement reviewBtn;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"Important notes\"")
    private MobileElement mImportantNotes;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"getSelector\"]")
    private MobileElement youGetSelector;

    @iOSXCUITFindBy(
            xpath = "//*[starts-with(@name, 'Available amount')]")
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available balance: HKD')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'可用結餘:')]")
    })
    private MobileElement availableAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Amount exceeds available balance')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'輸入金額超過可用結餘')]")
    })
    @iOSXCUITFindBy(
            iOSNsPredicate =
                    "label == \"Amount exceeds available balance\" AND name == \"Amount exceeds available balance\" AND value == \"Amount exceeds available balance\"")
    private MobileElement errorMessage;

    @iOSXCUITFindBy(
            xpath =
                    "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[3]")
    private MobileElement slideToConfirmTxt;

    @iOSXCUITFindBy(
            xpath = "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
    private MobileElement slideToConfirmArrow;

    @iOSXCUITFindBy(
            xpath =
                    "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
    private MobileElement slideToConfirmBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "(//android.widget.TextView[@content-desc=\"-title\"])[5]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Done']")
    })
    @iOSXCUITFindBy(
            xpath =
                    "(//XCUIElementTypeOther[@name=\"Done\"])[2]")
    private MobileElement successDone;

    @iOSXCUITFindBy(
            xpath = "(//XCUIElementTypeStaticText[@name=\"text\"])[4]/../../XCUIElementTypeOther[3]")
    private MobileElement totalBalanceArrow;

    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Please review and confirm.']"))
    private MobileElement exchangeRatePrompt;

    @iOSXCUITFindBy(
            xpath = "//*[starts-with(@name, 'Wealth Balance')]")
    private MobileElement mWealthBalance;

    @iOSXCUITFindBy(iOSNsPredicate = "name == 'HKD'")
    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='HKD']"))
    private MobileElement HKD;

    @iOSXCUITFindBy(iOSNsPredicate = "name == 'USD'")
    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='USD']"))
    private MobileElement USD;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"convertSelector\"]")
    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[@content-desc=\"convertSelector\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"))
    private MobileElement topBropDownBox;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[@content-desc=\"getSelector\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"))
    private MobileElement downUS;


    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup"))
    private MobileElement slideButton;

    @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='Transaction in progress']"))
    private MobileElement transactionInProgress;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Your transaction is successful.']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='交易成功']")
    })
    private MobileElement transactionTips;

    @AndroidFindAll(@AndroidBy(xpath = "(//android.widget.TextView[@content-desc=\"-title\"])[5]"))
    private MobileElement Done;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='USD Balance']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='美元結餘']")
    })
    private MobileElement accountBalance;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"USD Balance\"]")
    private MobileElement balance;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"alance(USD)\"]")
    private MobileElement balanceUsd;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Amount exceeds available balance\"]")
    private MobileElement availableBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Exchange amount']/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//*[@text='兌換金額']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement transactionAmount;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "(//android.widget.TextView[@content-desc=\"-content\"])[2]"))
    private MobileElement afterExchangeHK;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "(//android.widget.TextView[@content-desc=\"-content\"])[3]"))
    private MobileElement afterExchangeUS;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.widget.EditText"))
    private MobileElement enterAmountUS;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"))
    private List<MobileElement> currencyList;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"))
    private MobileElement selectCurrencyClose ;

    @AndroidFindAll(
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView"))
    private MobileElement totalBalance ;


    @AndroidFindAll({
            @AndroidBy(  xpath = "//android.widget.TextView[@text = 'Mobile security key']" ),
            @AndroidBy(  xpath = "//android.widget.TextView[@text = '流動保安編碼認證']")
    })
    private MobileElement MSKTitle;

    @AndroidFindAll({@AndroidBy(xpath = "(//android.widget.TextView[@content-desc=\"-content\"])[2]")})
    private MobileElement HKDAmountOnSuccessPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'ve added')]/following-sibling::android.widget.TextView[1]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'你已買入')]/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement USDAmountOnSuccessPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[3]"),
            @AndroidBy(xpath = "//*[@text='今日']/../../android.view.ViewGroup[3]")
    })
    private MobileElement firstRecord;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Transaction detail']/../android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text='交易詳情']/../android.view.ViewGroup")})
    private MobileElement backBtnInRecordPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Total balance']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='總結餘']/preceding-sibling::android.view.ViewGroup[1]")})
    private MobileElement backBtnOnTotalBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='USD Balance']/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='美元結餘']/preceding-sibling::android.view.ViewGroup[1]")})
    private MobileElement backBtnOnUSDBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='查看更多']"),
            @AndroidBy(xpath = "//*[@text='See all']")
    })
    private MobileElement seeAll;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup"),
            @AndroidBy(xpath = "//*[@text='Today']/../../android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//*[@text='今日']/../../android.view.ViewGroup[2]")
    })
    private MobileElement traderNameAtHome;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text = 'Select your currency']/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text = '選擇貨幣']/following-sibling::android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]")
    })
    private MobileElement USDExchange;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'The minimum transaction')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'最低交易金額')]"),
    })
    private MobileElement minimumTransaction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='You convert']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement youConvertOnProcess;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='You get']/following-sibling::android.widget.TextView[1]")
    })
    private MobileElement youGetOnProcess;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Exchange amount']/following-sibling::android.widget.TextView[1]"),
    })
    private MobileElement FXAmountOnDetail;


    public void clickHomeForeignExchange() {



        Set<String> contexts = driver.getContextHandles();

        for(String cotext:contexts) {

            System.out.println(cotext);

//会打印出  NATIVE_APP（原生app handle）和WEBVIEW_com.example.testapp(webview的 handle)

        }
        driver.context((String) contexts.toArray()[1]);
//android.widget.TextView[@text='交易成功']
        driver.getContextHandles();

        waitUntilElementVisible(homeForeignExchangeBtn,60);
        clickElement(homeForeignExchangeBtn);
    }

    public Boolean checkWealthBalance()  {
        return verifyElementExist(mWealthBalance);
    }

    public Boolean checkTotalBalanceArrow()  {
        return verifyElementExist(totalBalanceArrow);
    }

    public void clickTotalBalanceArrow() {
        clickElement(totalBalanceArrow);
    }

    public void clickImportantNotes() {
        clickElement(mImportantNotes);
    }

    public Boolean checkSuccesssDone() throws InterruptedException {
        return isElementVisible(successDone,120000);
    }

    @SneakyThrows
    public void clickSuccesssDone() {
        Thread.sleep(5000);
        Thread.sleep(5000);
        clickElement(successDone);
    }

    public void clickSlideToConfirm() {
        clickElement(slideToConfirmBtn);
    }

    @SneakyThrows
    public void enterAmount(String amount,String screenShotName) {
        Thread.sleep(5000);
        clearAndSendKeys(youConvert,amount);
        inputEnter(youConvert);
        Thread.sleep(2000);
        youConvertAmount = amount;
        youGetAmount = youGet.getText();
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
//        commonPage.waiteSendMsk();
    }

    public void setSlideToConfirm() {
        if (System.getProperty("mobile").contains("android")) {
            List<String> yNow = getAttribute(slideButton);
            swipeDown(Integer.parseInt(yNow.get(0)), Integer.parseInt(yNow.get(1)),
                    Integer.parseInt(yNow.get(2)), Integer.parseInt(yNow.get(3))-80);
        }
        else if (System.getProperty("mobile").contains("ios")){
            int widthArrow= getIOSAttributePoints(slideToConfirmArrow,"width")/2;
            int witdthTxt= getIOSAttributePoints(slideToConfirmTxt,"width");
            int btnX=getIOSAttributePoints(slideToConfirmBtn,"x");
            int btnY=getIOSAttributePoints(slideToConfirmBtn,"y");
            swipeDown(btnX,btnY,btnX+widthArrow+witdthTxt,btnY);
        }
    }

    public Boolean getForeignExchangeStatus() {
        return isElementNotVisible(foreignExchangeBtn,3);
    }
    public Boolean getAddMoneyStatus() {
        return isElementVisible(addMoneyBtn,1);
    }

    @SneakyThrows
    public void clickForeignExchangeBtn(String screenShotName) {
        Thread.sleep(3000);
        clickElement(foreignExchangeBtn);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void inputYouConvert(String moreThan) {
        String moneyText;
        double moneyAmount = 0;
        Thread.sleep(5000);
        if (getElementText(availableAmount).contains("HKD")) {
            moneyText = getElementText(availableAmount).replace("Available balance: HKD", "");

        } else {
            moneyText = getElementText(availableAmount).replace("可用結餘: ", "").split("港元")[0];
        }
        moneyAmount = Double.parseDouble(moneyText.replace(",", ""));
        logger.info("Available balance: HKD "+moneyAmount);
        if (moreThan.equals("moreThan")){
            moneyAmount = moneyAmount + 2;
        }
        if (moreThan.equals("lessThan")){
            moneyAmount = moneyAmount - 2;
        }
        logger.info("Enter Balance "+moneyAmount);
        andSendKeys(youConvert, String.valueOf(moneyAmount));
    }
    public Boolean prompt() {
        return verifyElementExist(errorMessage);
    }

    public Boolean slideToConfirmStatus() {
        return isElementClickable(By.xpath("(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeO￼ther[2]"));
    }

    public Boolean reviewBtnstatus() {
        return isElementVisible(reviewBtn,600);
    }

    public void swipeDown(int xstart, int ystart, int xend, int yend) {
        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        PointOption pointOption1 = PointOption.point(xstart, ystart);
        PointOption pointOption2 = PointOption.point(xend, yend);
        Duration duration = Duration.ofMillis(1000);
        WaitOptions waitOptions = WaitOptions.waitOptions(duration);
        touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
                .perform();
    }

    public int getIOSAttributePoints(WebElement element,String aName){
        WebElement seekBar = element;
        String rect=seekBar.getAttribute("rect");
        if(aName=="y"){
            String strx= rect.split(",")[0].toString().split(":")[1];
            return (Integer.parseInt(strx));
        }
        else if(aName=="x"){
            String stry= rect.split(",")[1].toString().split(":")[1];
            return (Integer.parseInt(stry));
        }
        else if (aName.toLowerCase()=="width"){
            String strx= rect.split(",")[2].toString().split(":")[1];
            return (Integer.parseInt(strx));
        }
        return 0;
    }
    public void clickReview(){
        clickElement(reviewBtn);
    }

    public Boolean enRouteInput(String amount,String time) throws ParseException, InterruptedException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, Integer.parseInt(time));
        nowTime.add(Calendar.MINUTE, Integer.parseInt(String.valueOf(Integer.parseInt(time)/2)));
        Date halfwayTime= sdf.parse(sdf.format(nowTime.getTime()));
        Date endTime= sdf.parse(sdf.format(nowTime.getTime()));
        for (int i=0;i<1;){
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now1 = new Date();
            Date startTime= sdf1.parse(sdf.format(now1));
            if (compareDate(startTime,halfwayTime)==1 && amount!=""){
                andSendKeys(youConvert,amount);
            }
            i=compareDate(startTime,endTime);
            if (isExistElement(exchangeRatePrompt)){
                return verifyElementExist(reviewBtn);
            }
            Thread.sleep(60000);
        }
        return false;
    }

    public boolean isExistElement( MobileElement element) {
        try {
            MobileElement element1 = element;
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static int compareDate(Date now, Date validity) {
        if(validity != null){
            try {
                if (now.getTime() > validity.getTime())
                    return 1;
                else if (now.getTime() < validity.getTime())
                    return 0;
                else
                    return 0;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return 0;
    }
    public void clickDropDownBox(String currency) {
        clickElement(topBropDownBox);
        String actualValue=getElementText(availableAmount);
        if (actualValue!=currency){
            if(currency.equalsIgnoreCase("USD")){
                clickElement(USD);
            }else if(currency.equalsIgnoreCase("HKD")){
                clickElement(HKD);
            }
        }
    }
    public List<String> getAttribute(WebElement element){
        WebElement seekBar = element;
        String Now=seekBar.getAttribute("bounds");
        return test(Now);
    }

    public static List<String> test(String Now) {
        String result = Now.replaceAll("[-+.^:,]","");
        List<String> lst = new ArrayList<String>();
        String quStr=Now.substring(Now.indexOf("[")+1,Now.indexOf("]"));
        int index = Now.indexOf("]");
        String after1 = Now.substring(index + 1);
        String[] yNow =quStr.split(",");
        lst.add(yNow[0]);
        lst.add(yNow[1]);
        String after2=after1.substring(after1.indexOf("[")+1,after1.indexOf("]"));
        String[] xStart =after2.split(",");
        lst.add(xStart[0]);
        lst.add(xStart[1]);
        return lst;
    }

    @SneakyThrows
    public boolean transactionStatus(String screenShotName) {
        Thread.sleep(5*1000);
        Thread.sleep(5*1000);
        Thread.sleep(5*1000);
        waitUntilElementVisible(youConvertOnProcess,20);
        takeScreenshot(screenShotName);
        if (youConvertOnProcess.getText().contains(youConvertAmount) && youGetOnProcess.getText().contains(youGetAmount)) {
            Thread.sleep(5000);
            clickElement(successDone);
            return true;
        }
        return false;
    }
//
//    @SneakyThrows
//    public Boolean amountJudgment() {
//        boolean flg=false;
//        String HKDollars= GlobalVar.GLOBAL_VARIABLES.get("HKDollars");
//        String USollars= GlobalVar.GLOBAL_VARIABLES.get("USDollars");
//
//        if (getElementText(afterExchangeHK).indexOf(HKDollars)>=0 && getElementText(afterExchangeUS).indexOf(USollars)>=0){
//            return flg=true;
//        }
//        return flg;
//    }

//    @SneakyThrows
//    public boolean whether(){
//        Thread.sleep(5000);
//        Thread.sleep(5000);
//        return isElementVisible(Done,1);
//    }

    @SneakyThrows
    public void  clickBalanceDetails(){
        Thread.sleep(5000);
        clickElement(totalBalance);
        Thread.sleep(5000);
        waitUntilElementVisible(accountBalance,1);
        clickElement(accountBalance);
    }

    @SneakyThrows
    public void clickBalance(){
        Thread.sleep(3000);
        waitUntilElementVisible(balance,1);
        if (isElementVisible(balance)){
            clickElement(accountBalance);
        }

    }
    @SneakyThrows
    public Boolean balanceHKD(){
        Thread.sleep(5000);
        clickElement(firstRecord);
        if (transactionAmount.getText().split("USD")[1].contains(GlobalVar.GLOBAL_VARIABLES.get("USDollars"))) {
            clickElement(backBtnInRecordPage);
            return true;
        }
        return false;
    }


    public Boolean slideButtonAvailable(){
        boolean flg=false;
        if (slideButton.isDisplayed()){
            flg=true;
        }
        return flg;
    }

    @SneakyThrows
    public boolean viewCurrency() {
        boolean flg=false;
        boolean falseHKD=false;
        boolean US=false;
        clickElement(topBropDownBox);
        clickElement(topBropDownBox);
        Thread.sleep(2000);
        int size=currencyList.size();
        if (size==2){
            falseHKD=true;
        }
        clickElement(selectCurrencyClose);
        Thread.sleep(5000);
        clickElement(downUS);
        Thread.sleep(2000);
        int sizeUS=currencyList.size();
        if (sizeUS==1){
            US=true;
        }
        if (falseHKD && US){
            flg=true;
        }
        return flg;
    }

    public Boolean checkPage(){
        return isExistElement(availableAmount);
    }
    public void  expect(String number){
        Scanner scanner =new Scanner(System.in);
        int time = 0;
        int cd = 0;
        int saveTime = 0;
        saveTime = Integer.parseInt(number)*60;
        cd = saveTime;
        logger.info("Prepare countdown"+ saveTime/60 + "minute");
        while (time < saveTime) {
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            time++;
            cd--;
        }
        scanner.close();
    }
    @SneakyThrows
    public void clickPersonalInformation(String screenShotName) {
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(MSKTitle);
//    takeScreenshot(screenShotName);
        Thread.sleep(5*1000);
        sendMsk();
        Thread.sleep(2 * 1000);
    }


    public void verifyBalance(String screenShotName) {
//    TotalBalance = getAccountTotalBalance();
//    AvailableBalance = getAccountAvailableBalance();
        takeScreenshot(screenShotName);
    }

    @Getter
    @AndroidFindAll({
            @AndroidBy(accessibility = "GoSave, tab, 2 of 4"),
            @AndroidBy(accessibility = "GoSave, tab, 2 of 5")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'GoSave, tab, 2 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemGroupSavingStack, tab, 2 of 5'")
    })
    private MobileElement goSaveTab;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='My total GoSave balance (HKD)']/../android.widget.TextView[2]"),
            @AndroidBy(xpath = "//*[@text='我的GoSave總額 (HKD)']/../android.widget.TextView[2]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '我的GoSave總額 (HKD)')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'My total GoSave balance (HKD)')]")
    })
    private MobileElement goSaveBalance;


    @Getter
    @AndroidFindAll({
            @AndroidBy(accessibility = "Home, tab, 1 of 4"),
            @AndroidBy(accessibility = "Home, tab, 1 of 5"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='主頁']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'Home, tab, 1 of 5'")
    })
    private MobileElement homeTab;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//button[@aria-disabled='false'] ")
    })

    private MobileElement joinNow;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//input[@aria-required='false']")
    })
    private MobileElement depositAmount;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@id=\"mat-dialog-0\"]/ngx-input-amount/div[2]/button")
    })

    private MobileElement next;

    @SneakyThrows
    public void openGoSave(String screenShotName) {
        String amount="100";
        Thread.sleep(3000);
        waitUntilElementVisible(goSaveTab);
        goSaveTab.click();
        Thread.sleep(3000);
        takeScreenshot(screenShotName);


    }

    public WebElement waitUntilElementClickable2(WebElement element, int... timeOut) {
        int actualTimeout = TimeUtils.setTimeOut(timeOut);
        WebElement el = null;
        try {
            WebDriverWait wait = new WebDriverWait(driver, actualTimeout);
            el = wait.until(ExpectedConditions.elementToBeClickable(element));
            String logDetails = String.format("Element is clickable in %s seconds", actualTimeout);
            logPass(logDetails, pageName, element);
        } catch (Exception e) {
            String errorMsg =
                    String.format("Failed to wait element clickable in %s seconds", actualTimeout);
            logFail(errorMsg, pageName, element);
            throw e;
        }
        return el;
    }

    @SneakyThrows
    public void checkTheHomePageTotalBalance(String screenShotName) {
        Thread.sleep(2000);
        waitUntilElementClickable(homeTab);
        clickElement(homeTab);
//    waitUntilElementVisible(theAccountTotalBalance);
//    TotalBalance = getAccountTotalBalance();
//    AvailableBalance = getAccountAvailableBalance();
//    System.out.println("TotalBalance: " + TotalBalance + ",AvailableBalance:" + AvailableBalance);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
    }
    public void switchH5() {
        contextWebview();
    }
    @SneakyThrows
    public void joinNow() {
        Thread.sleep(3000);

        clickElement(driver.findElement(By.xpath("//button[@aria-disabled='false']")));
        Thread.sleep(3000);
    }
    @SneakyThrows
    public void enter(String amount) {
        Thread.sleep(3000);
        driver.switchTo();
        clickElement(driver.findElement(By.xpath("//input[@aria-required='false']")));
//    andSendKeys(depositAmount,amount);
        Thread.sleep(3000);
        andSendKeys(driver.findElement(By.xpath("//input[@aria-required='false']")),amount);
        Thread.sleep(3000);
    }
    @SneakyThrows
    public void clickNext() {
        Thread.sleep(3000);
        clickElement(driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/ngx-input-amount/div[2]/button")));
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void clickedConditions() {
        Thread.sleep(3000);
        clickElement(driver.findElement(By.xpath("//button[@aria-disabled='false']")));
        Thread.sleep(3000);
    }
    @SneakyThrows
    public void clickComplete() {
        Thread.sleep(3000);
        clickElement(driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/ngx-input-amount/div[2]/button")));

        Thread.sleep(3000);
    }
    @SneakyThrows
    public void switchNative() {
        Thread.sleep(3000);
        returnToNayive();
        Thread.sleep(3000);
    }

    @SneakyThrows
    public void iClickTransfer(String screenShotName) {
        Thread.sleep(3000);
        waitUntilElementClickable(homeForeignExchangeBtn);
        clickElement(homeForeignExchangeBtn);
        waitUntilElementVisible(foreignExchangeBtn);
        takeScreenshot(screenShotName);
    }

//    @SneakyThrows
//    public boolean verifyHKDTransaction() {
//        if (USDAmountOnSuccessPage.getText().split("\\+")[1].equals(GlobalVar.GLOBAL_VARIABLES.get("USDollars")) && HKDAmountOnSuccessPage.getText().contains(GlobalVar.GLOBAL_VARIABLES.get("HKDollars"))) {
//            Thread.sleep(5000);
//            Thread.sleep(5000);
//            clickElement(successDone);
//            return true;
//        }
//        return false;
//    }

    @SneakyThrows
    public void usdPageBackHomePage() {
        Thread.sleep(3000);
        waitUntilElementClickable(backBtnOnUSDBalance);
        clickElement(backBtnOnUSDBalance);
        Thread.sleep(3000);
        waitUntilElementClickable(backBtnOnTotalBalance);
        clickElement(backBtnOnTotalBalance);
    }

    @SneakyThrows
    public boolean verifyHKDTransactionDetail() {
        scrollUpToFindElement(seeAll, 3, 3);
        clickElement(seeAll);
        Thread.sleep(5000);
        clickElement(traderNameAtHome);
        Thread.sleep(5000);
        if (transactionAmount.getText().split("HKD")[1].contains(GlobalVar.GLOBAL_VARIABLES.get("HKDollars"))) {
            return true;
        }
        return false;
    }


    public void changeToUSDTransaction(String screenShotName) {
        clickElement(topBropDownBox);
        clickElement(topBropDownBox);
        waitUntilElementVisible(USDExchange);
        takeScreenshot(screenShotName);
        clickElement(USDExchange);
    }
//
//    @SneakyThrows
//    public boolean verifyUSDTransaction() {
//        if (USDAmountOnSuccessPage.getText().split("\\+")[1].equals(GlobalVar.GLOBAL_VARIABLES.get("USDollars")) && HKDAmountOnSuccessPage.getText().contains(GlobalVar.GLOBAL_VARIABLES.get("HKDollars"))) {
//            Thread.sleep(5000);
//            clickElement(successDone);
//            return true;
//        }
//        return false;
//    }


    @SneakyThrows
    public boolean balanceUSD() {
        Thread.sleep(5000);
        clickElement(firstRecord);
        if (transactionAmount.getText().split("USD")[1].contains(GlobalVar.GLOBAL_VARIABLES.get("HKDollars"))) {
            clickElement(backBtnInRecordPage);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifyUSDTransactionDetail() {
        scrollUpToFindElement(seeAll, 3, 3);
        clickElement(seeAll);
        Thread.sleep(5000);
        waitUntilElementClickable(traderNameAtHome);
        clickElement(traderNameAtHome);
        Thread.sleep(5000);
        if (transactionAmount.getText().split("HKD")[1].contains(GlobalVar.GLOBAL_VARIABLES.get("USDollars"))) {
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean minimumTransactionAmountTips() {
        Thread.sleep(3000);
        return verifyElementExist(minimumTransaction);
    }

    @SneakyThrows
    public boolean checkWrongAmountTips(String screenShotName, String screenShotName01) {
        andSendKeys(youConvert, "99999999999999");
        inputEnter(youConvert);
        waitUntilElementVisible(errorMessage);
        takeScreenshot(screenShotName);
        boolean flag = verifyElementExist(errorMessage);
        Thread.sleep(3000);
        clickElement(youConvert);
        clearAndSendKeys(youConvert, "9");
        inputEnter(youConvert);
        waitUntilElementVisible(minimumTransaction);
        takeScreenshot(screenShotName01);
        Thread.sleep(3000);
        return flag && verifyElementExist(minimumTransaction);
    }

    @SneakyThrows
    public void clickHomePageFirstRecord(){
        Thread.sleep(3000);
        clickElement(traderNameAtHome);
        Thread.sleep(3000);
        clickElement(backBtnInRecordPage);
        Thread.sleep(3000);
    }

    @SneakyThrows
    public boolean verifyForeignExchangeRecord(String screenShotName, String screenShotName01) {
        Thread.sleep(3000);
        waitUntilElementVisible(traderNameAtHome);
        takeScreenshot(screenShotName);
        clickHomePageFirstRecord();
        Thread.sleep(3000);
        clickElement(traderNameAtHome);
        waitUntilElementVisible(FXAmountOnDetail);
        takeScreenshot(screenShotName01);
        return FXAmountOnDetail.getText().contains(youConvertAmount) || FXAmountOnDetail.getText().contains(youGetAmount);
    }

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"賣出\"])[1]")
    private MobileElement soldInput;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"輸入金額超過可用結餘。\"]")
    private MobileElement outAmountErrorMessage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"* 最低交易金額為港元10。\"]")
    private MobileElement limitAmountErrorMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"滑動確認\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"滑動確認\" AND name == \"滑動確認\" AND value == \"滑動確認\""),
    })
    private MobileElement slideButtonIOS;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"你已買入\"]")
    private MobileElement youAreBuying;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"完成\" AND name == \"-title\"")
    private MobileElement FinishButton;

    @iOSXCUITFindBy(iOSNsPredicate = "value == \"9999999999\"")
    private MobileElement soldInputLargeValue;
    @iOSXCUITFindBy(iOSNsPredicate = "value == \"9\"")
    private MobileElement soldInputLimitValue;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"港元\"]")
    private MobileElement HKDItem;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"美元\"])[4]")
    private MobileElement USDItem;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"你已買入\"]")
    private MobileElement yourTransactionIsSucccessful;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"港元\"])[2]/XCUIElementTypeOther[2]")
    private MobileElement HKDtoUSDSwitch;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"選擇貨幣\"]")
    private MobileElement selectYourCurrency;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 1 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 3 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 4 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 5 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 6 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]/../XCUIElementTypeOther[2]"),
    })
    public MobileElement transactionDetails;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 1 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 3 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 4 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 5 交易記錄\"]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 6 交易記錄\"]"),
    })
    public MobileElement currentlyTransactions;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"交易詳情\"]")
    private MobileElement transactionDetailPage;

    @SneakyThrows
    public void checkTransationOnHomePage(String screenShotName, String screenShotName1){
        scrollDown();
        Thread.sleep(1000 * 5);
        scrollDown();
        waitUntilElementVisible(transactionDetails);
        scrollUp();
        Thread.sleep(1000 * 2);
        takeScreenshot(screenShotName);
        clickTodayLastTransaction();
        waitUntilElementVisible(transactionDetailPage);
        Thread.sleep(15000);
        takeScreenshot(screenShotName1);
    }

    public void clickTodayLastTransaction(){
        //the last transaction  under below  currentlyTransactions  13% height
        int eleHeight = getIOSAttributePoints(currentlyTransactions,"y");
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int x = width * 25 / 100;
        int y = height * 13 / 100 +eleHeight;
        clickByLocation(x,y);
    }

    public void slipeConfirmBtn() {
        // y = 64 %
        // x =  18 ~ 95 %
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int yPercent =  getYPercentByElement(slideButtonIOS)+1;
        int y = height * yPercent / 100;
        int start_x = width * 18 / 100;
        int end_x = width * 95 / 100;

        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        PointOption pointOption1 = PointOption.point(start_x, y);
        PointOption pointOption2 = PointOption.point(end_x, y);
        Duration duration = Duration.ofMillis(1000);
        WaitOptions waitOptions = WaitOptions.waitOptions(duration);
        touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
                .perform();
    }

    @SneakyThrows
    public void convertHKDtoUSDIOS(String screenShotName, String screenShotName1,
                                   String screenShotName2, String screenShotName3,
                                   String screenShotName4){

        waitUntilElementVisible(HKDItem);

        clearAndSendKeys(soldInput,"9999999999");
        waitUntilElementVisible(outAmountErrorMessage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        clearInput(soldInputLargeValue);
        Thread.sleep(1000);
        clearAndSendKeys(soldInput,"9");
        waitUntilElementVisible(limitAmountErrorMessage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);

        clearInput(soldInputLimitValue);
        Thread.sleep(1000);
        clearAndSendKeys(soldInput,"20");
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        slipeConfirmBtn();

        waitUntilElementVisible(youAreBuying);
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);

        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName4);
        clickElement(FinishButton);
    }

    public void moveEyeBtnToMiddleRight() {
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        TouchAction action = new TouchAction(driver);
        action.press(PointOption.point(width*88/100, height*26/100))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(width, height/2))
                .release()
                .perform();
        action.press(PointOption.point(width*92/100, height*22/100))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(width, height/2))
                .release()
                .perform();
    }

    @SneakyThrows
    public void convertUSDtoHKDIOS(String screenShotName, String screenShotName1,
                                   String screenShotName2, String screenShotName3){

        waitUntilElementVisible(HKDItem);
        moveEyeBtnToMiddleRight();
        clickElement(HKDtoUSDSwitch); //first click to hide keyboard
        clickElement(HKDtoUSDSwitch);
        waitUntilElementVisible(selectYourCurrency);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickElement(USDItem);

        Thread.sleep(6000);

        clearAndSendKeys(soldInput,"10");
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        slipeConfirmBtn();

        waitUntilElementVisible(yourTransactionIsSucccessful);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickElement(FinishButton);
    }
}
