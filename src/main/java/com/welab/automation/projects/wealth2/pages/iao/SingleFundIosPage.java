package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.cucumber.java.en.Then;
import lombok.SneakyThrows;
import lombok.Synchronized;

import java.time.Duration;

public class SingleFundIosPage extends AppiumBasePage {
    private String pageName = "SingleFund Page";
    private int yPercentTemp = 0;
    private int xPercentTemp = 0;
    CommonPage commonPage;
    private boolean isUSDbuyUSD = false;

    public SingleFundIosPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'現時財富總值')]")
    private MobileElement wealthCurrentAmount;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'基金清單')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'基金總值')]"),
    })
    private MobileElement fundListPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'領先基金及熱門主題')]")
    private MobileElement LeadingFundsAndPopularTopicsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'基金列表 ISIN號碼、基金名稱、基金公司')]")
    private MobileElement featureFundsListPage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"美元基金\"]")
    private MobileElement USDItem;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"港元基金\"]")
    private MobileElement HKDItem;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Search\"]")
    private MobileElement softkeySearch;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'月投資係由下個月嘅8號開始')]")
    private MobileElement fundInputAmountPageForNewFund;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'重要事項及風險披露')]")
    private MobileElement fundInputAmountPageImportantAndRiskMessage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'重要事項及風險披露')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'月投資係由下個月嘅8號開始')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'一次性投資 (港元)')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'一次性投資 (美元)')]"),
//            @iOSXCUITBy(xpath = "//*[contains(@name ,'一次性贖回金額')]"),
    })
    private MobileElement fundInputAmountPageForAllScenario;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'恭喜')]")
    private MobileElement BuysuccessfulCongratulationsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'交易指示狀態')]")
    private MobileElement fundDetailPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'FeaturedFund.monthlyInstructionTip')]")
    private MobileElement monthlyInstructionPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'一次性贖回金額')]")
    private MobileElement sellInputAmountPage;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"全部贖回\" AND name == \"text\"")
    private MobileElement sellAllConfrimbutton;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'贖回單位不符合本基金最少贖回單位要求')]")
    private MobileElement sellAllAmountErrorMessage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'輸入金額超過可用結餘')]")
    private MobileElement inputAmountErrorMessage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'賣出交易指示已完成')]")
    private MobileElement sellSuccessfulPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'每月常設指示 HKD 0.00')]")
    private MobileElement monthlyStandingIsZeroMessage;

    public void clickCheckFunds(){
        if(isIphone13()){
            clickByLocationByPercent(42, 36);
        }else{
            clickByLocationByPercent(42, 50);
        }
    }

    @SneakyThrows
    public void wealthCheckFundsPageIOS(String screenShotName){
        Thread.sleep(2000);
        clickCheckFunds();
        waitUntilElementVisible(fundListPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void getFeatureFundsThemes(String screenShotName){
        scrollUpByTimes(8);
        Thread.sleep(1000);
        clickFeatureFundsThemes();
        waitUntilElementVisible(LeadingFundsAndPopularTopicsPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    public void clickFeatureFundsThemes(){
        if(isIphone13()){
            clickByLocationByPercent(50, 68);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/featureFundsThemes.png",50,50);
        }
    }

    public void clickCheckFeatureFundsListButton(){
        clickByLocationByPercent(50, 92);
        //clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/checkFundListButton.png",50,50);
    }

    public void clickFundsTypeSelect(){
        clickByLocationByPercent(20, 18);
    }

    public void clickFisrtFunds(){
        clickByLocationByPercent(50, 37);
    }

    public void clickFundInput(){
        clickByLocationByPercent(50, 12);
    }

    public void clickBuy(){
        clickByLocationByPercent(50, 92);
    }

    public void clickFinish(){
        clickByLocationByPercent(50, 92);
    }

    public void clickOneTimeInput(){
        if(isIphone13()){
            clickByLocationByPercent(50, 37);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/oneInvestment.png",50,60);
        }
    }
    public void clickMonthInput(){
        //clickByLocationByPercent(50, 52);
        clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/monthlyInvestment.png",50,60);
    }
    public void clickNextButton(){
        clickByLocationByPercent(50, 60);
    }

    @SneakyThrows
    public void checkFeatureFundsListPage(String screenShotName){
        clickCheckFeatureFundsListButton();
        waitUntilElementVisible(featureFundsListPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void searchUSDFunds(String screenShotName){
        clickFundsTypeSelect();
        Thread.sleep(1000);
        waitUntilElementVisible(USDItem);
        clickElement(USDItem);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void searchHKDFunds(String screenShotName){
        clickFundsTypeSelect();
        Thread.sleep(1000);
        waitUntilElementVisible(HKDItem);
        clickElement(HKDItem);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void searchFundsByName(String fundName){
        clickFundInput();
        commonPage.clickSoftKeyByNumberString(fundName);
        Thread.sleep(1000);
        clickElement(softkeySearch);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void selectOneFundToBuy(String screenShotName){
        clickFisrtFunds();
        Thread.sleep(12*1000);
        takeScreenshot(screenShotName);
        clickBuy();
        Thread.sleep(8*1000);
        waitUntilElementVisible(fundInputAmountPageForAllScenario);
    }

    @SneakyThrows
    public void buyOneFundInFundsList(String oneTimeAmount, String monthlyAmount,
            String screenShotName, String screenShotName1,
                                      String screenShotName2, String screenShotName3){

        if(isShow(fundInputAmountPageForNewFund, 3)){
            buyNewFundInFundsList(oneTimeAmount, monthlyAmount,
                    screenShotName, screenShotName1, screenShotName2, screenShotName3);
        }else{
            buyOldFundInFundsList(oneTimeAmount,
                    screenShotName, screenShotName1, screenShotName2, screenShotName3);
            isUSDbuyUSD = false;
        }
    }

    @SneakyThrows
    public void buyOldFundInFundsList(String oneTimeAmount,
                                      String screenShotName, String screenShotName1,
                                      String screenShotName2,String screenShotName3){
//        if(!isUSDbuyUSD){
//            clickByLocationByPercent(50, 45);
//        }else{
//            clickByLocationByPercent(50, 42);//for USD BY USD
//        }
        clickOneTimeInput();
        logger.info("oneTimeAmount: "+oneTimeAmount);
        commonPage.clickSoftKeyByNumberString(oneTimeAmount);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        confirmBuy( screenShotName1, screenShotName2, screenShotName3);
    }

    @SneakyThrows
    public void buyNewFundInFundsList(String oneTimeAmount, String monthlyAmount,
                                      String screenShotName, String screenShotName1,
                                      String screenShotName2, String screenShotName3){

        clickOneTimeInput();
        commonPage.clickSoftKeyByNumberString(oneTimeAmount);
        Thread.sleep(1000);
        clickByLocationByPercent(20,25); //for hide keyBoard
        Thread.sleep(1000);
        clickMonthInput();
        commonPage.clickSoftKeyByNumberString(monthlyAmount);
        takeScreenshot(screenShotName);
        confirmBuy( screenShotName1, screenShotName2, screenShotName3);
    }

    @SneakyThrows
    public void confirmBuy(String screenShotName, String screenShotName1, String screenShotName2){
        clickNextButtonAndSlideConfirm(screenShotName);
        waitUntilElementClickable(BuysuccessfulCongratulationsPage, 50);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickFinish();
        waitUntilElementVisible(wealthCurrentAmount);
        Thread.sleep(6000);
        takeScreenshot(screenShotName2);
    }

    @SneakyThrows
    public void clickNextButtonAndSlideConfirm(String screenShotName){
        clickNextButton();
        Thread.sleep(10 * 1000);
        scrollUpByTimes(6);
        Thread.sleep(1000);;
        clickCircle();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
    }

    public void clickCircle(){
        if(isIphone13()){
            clickByLocationByPercent2(8,81);
        }else{
            clickByLocationByPercent2(7,74.5);
        }
    }

    public void slipeConfirmBtn() {
        // y = 64 %
        // x =  18 ~ 95 %
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;

        int yPercent = 0;
        // yPercent=getYlocation("src/main/resources/images/channel/payment/Slide.png",50,100);  //iphone7 plus
        //iphone se
        if(isIphone13()){
            yPercent=getYlocation("src/main/resources/images/channel/payment/Slide.png",50,60);
        }else{
            yPercent=getYlocation("src/main/resources/images/channel/payment/slide1.png",50,50);
        }
        int y = height * yPercent / 100;
        int start_x = width * 18 / 100;
        int end_x = width * 95 / 100;

        TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
        PointOption pointOption1 = PointOption.point(start_x, y);
        PointOption pointOption2 = PointOption.point(end_x, y);
        Duration duration = Duration.ofMillis(1000);
        WaitOptions waitOptions = WaitOptions.waitOptions(duration);
        touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
                .perform();
    }

    @SneakyThrows
    public void checkFirstOrderInWealthPage(String screenShotName){
        Thread.sleep(2000);
        clickByLocationByPercent(50,82);
        Thread.sleep(2000);
        waitUntilElementVisible(fundDetailPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void switchHKDtoUSDCurrency(String screenShotName){
       //clickByLocationByPercent(78,27);
        slipeByLocationPercent(76, 27, 80,27);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        isUSDbuyUSD=true;
    }

    @SneakyThrows
    public void openUSDFundOnMyFundPage(String screenShotName){
        Thread.sleep(3000);
        scrollUp();
        Thread.sleep(1000);
        clickUSDfund();
        waitUntilElementVisible(fundDetailPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void openHKDFundOnMyFundPage(String screenShotName){
        Thread.sleep(3000);
        scrollUp();
        Thread.sleep(1000);
        clickHKDfund();
        waitUntilElementVisible(fundDetailPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    public void clickUSDfund(){
        String picturePath="";
        if(isIphone13()){
             picturePath="src/main/resources/images/wealth/iphoneSE/singleFund/USD2.png";
        }else{
            picturePath="src/main/resources/images/wealth/iphoneSE/singleFund/USD.png";
        }
        int[] location = clickByPicture(picturePath,50,60);
        xPercentTemp = location[0];
        yPercentTemp = location[1];
    }
    public void clickHKDfund(){
        String picturePath="";
        if(isIphone13()){
            picturePath="src/main/resources/images/wealth/iphoneSE/singleFund/HKD2.png";
        }else{
            picturePath="src/main/resources/images/wealth/iphoneSE/singleFund/HKD.png";
        }
        int[] location = clickByPicture(picturePath,50,60);
        xPercentTemp = location[0];
        yPercentTemp = location[1];
    }

    public void clickAddBuyButton(){
        //clickByLocationByPercent(24,43);
        if(isIphone13()){
            clickByPicture2("src/main/resources/images/wealth/iphoneSE/singleFund/buyButton2.png",50,60);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/buyButton.png",50,60);
        }
        waitUntilElementVisible(fundInputAmountPageForAllScenario);
    }

    public void clickSellButton(){
        //clickByLocationByPercent(50,43);
        if(isIphone13()){
            clickByPicture2("src/main/resources/images/wealth/iphoneSE/singleFund/sellButton2.png",50,60);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/sellButton.png",50,60);
        }
        waitUntilElementVisible(sellInputAmountPage);
    }

    public void clickSellPartial(){
        clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/SellPartial.png",50,60);
    }

    public void clickSellAll(){
        clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/SellAll.png",50,60);
    }



    public void clickBack() {
        clickByLocationByPercent(6,7);
    }

    @SneakyThrows
    public void sellFundAllAmount(String screenShotName, String screenShotName1,
                                      String screenShotName2, String screenShotName3){
        clickSellAll();
        if(isShow(sellAllConfrimbutton,3)){
            clickElement(sellAllConfrimbutton);
        }

        if(isShow(sellAllAmountErrorMessage,3)){
            clickBack();
            Thread.sleep(2000);
            clickBack();
            Thread.sleep(2000);
            waitUntilElementVisible(wealthCurrentAmount);
            Thread.sleep(2000);
            scrollUp();
            openHKDFundOnMyFundPage(screenShotName);
            clickSellButton();
            clickSellAll();
            if(isShow(sellAllConfrimbutton,3)){
                clickElement(sellAllConfrimbutton);
            }
        }

        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickByLocationByPercent(50,92); //click Next Button
        Thread.sleep(10 * 1000);
        scrollUpByTimes(2);
        Thread.sleep(1000);;
        clickSellConfirmPageCircle();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        slipeConfirmBtn();
        waitUntilElementClickable(sellSuccessfulPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickFinish();
        waitUntilElementVisible(wealthCurrentAmount);
        Thread.sleep(6000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void sellFundPartialAmount(String sellAmount,
                                      String screenShotName, String screenShotName1,
                                      String screenShotName2, String screenShotName3){
        clickSellPartial();
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString(sellAmount);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        confirmSell(screenShotName1, screenShotName2, screenShotName3);
    }

    public void clickSellConfirmPageCircle(){
        if(isIphone13()){
            clickByLocationByPercent2(8,81);
        }else{
            clickByLocationByPercent2(7,77);
        }
    }

    @SneakyThrows
    public void confirmSell(String screenShotName, String screenShotName1, String screenShotName2){
        clickNextButton();
        Thread.sleep(10 * 1000);
        scrollUpByTimes(2);
        Thread.sleep(1000);;
        clickSellConfirmPageCircle();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        slipeConfirmBtn();
        waitUntilElementClickable(sellSuccessfulPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickFinish();
        waitUntilElementVisible(wealthCurrentAmount);
        Thread.sleep(6000);
        takeScreenshot(screenShotName2);
    }

    private boolean monthlyStandingIsZero =false;
    public void checkMonthlyStandingIsZero(){
        if(isShow(monthlyStandingIsZeroMessage, 2)){
            monthlyStandingIsZero = true;
        }
    }

    public void clickMonthlyEdit(){
        checkMonthlyStandingIsZero();
        //clickByLocationByPercent(50,91);
        clickMonthlyStandingInstruction();
        waitUntilElementVisible(fundInputAmountPageForAllScenario);
    }
    public void clickMonthlyStandingInstruction(){
        //clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/MonthlyStandingInstruction.png",50,60);
        if(isIphone13()){
            clickByLocationByPercent(50,75);
        }else{
            clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/MonthlyStandingInstructionZh.png",50,60);
        }
    }
    public void clickMonthlyStandingInstructionInput(){
        //clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/MonthlyStandingInstructionInput.png",50,60);
        clickByPicture("src/main/resources/images/wealth/iphoneSE/singleFund/MonthlyStandingInstructionInput.png",50,60);
    }

    public void clickMonthlyInputWithValue(){
        int yPercent = 0;
        if(isIphone13()){
            yPercent = 26;
        }else{
            yPercent = getYlocation("src/main/resources/images/wealth/iphoneSE/singleFund/monthlyInvestmentBlue.png",50,60);
        }
        // input is under the picture, need add 6 Percent
        clickByLocationByPercent(50, yPercent + 6);
    }

    @SneakyThrows
    public void editMonthlyStandingInstruction(String monthlyAmount,
                                      String screenShotName, String screenShotName1,
                                      String screenShotName2, String screenShotName3){

        int amount = 100 + (int)(Math.random()*20);

        //clickMonthlyStandingInstructionInput();
        if(monthlyStandingIsZero){
            clickMonthInput();
        }else{
            clickMonthlyInputWithValue();
            Thread.sleep(1000);
            commonPage.inputDeleteByNumber(4);
            Thread.sleep(1000);
        }

        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString(amount+"");
        Thread.sleep(1000);
        takeScreenshot(screenShotName);

        clickNextButton();
        Thread.sleep(15 * 1000);
        scrollUpByTimes(5);
        Thread.sleep(1000);;
        clickCircle();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        slipeConfirmBtn();
        waitUntilElementClickable(BuysuccessfulCongratulationsPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName2);
        clickFinish();
        waitUntilElementVisible(wealthCurrentAmount);
        Thread.sleep(6000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void checkTheFundUpdate(String screenShotName){
        Thread.sleep(2000);
        scrollUp();
        Thread.sleep(2000);
        clickByLocationByPercent(xPercentTemp, yPercentTemp);
        waitUntilElementVisible(fundDetailPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

}
