Feature: singleFund

  Background: Login
    Given Open WeLab App
    And wealth skip Root IOS


  @VBWMIS-T681
  Scenario:Reg_SingleFund_001 HKD buy USD Fund(one investment+monthly subscription) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_001_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_001_IOS_02 |
    Then get Feature Funds Themes IOS
      | screenShotName | Reg_SingleFund_001_IOS_03 |
    Then check Feature Funds List Page IOS
      | screenShotName | Reg_SingleFund_001_IOS_04 |
    Then search USD Funds IOS
      | screenShotName | Reg_SingleFund_001_IOS_05 |
    Then search Funds by Name IOS
      | fundName | Allian |
    Then select One Fund To Buy
      | screenShotName | Reg_SingleFund_001_IOS_06 |
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 101                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_001_IOS_07 |
      | screenShotName1 | Reg_SingleFund_001_IOS_08 |
      | screenShotName2 | Reg_SingleFund_001_IOS_09 |
      | screenShotName3 | Reg_SingleFund_001_IOS_10 |
    Then check First Order In Wealth Page IOS
      | screenShotName | Reg_SingleFund_001_IOS_11 |

  @VBWMIS-T682
  Scenario:Reg_SingleFund_002 HKD buy USD Fund(one investment) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_002_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_002_IOS_02 |
    Then get Feature Funds Themes IOS
      | screenShotName | Reg_SingleFund_002_IOS_03 |
    Then check Feature Funds List Page IOS
      | screenShotName | Reg_SingleFund_002_IOS_04 |
    Then search USD Funds IOS
      | screenShotName | Reg_SingleFund_002_IOS_05 |
    Then search Funds by Name IOS
      | fundName | Allian |
    Then select One Fund To Buy
      | screenShotName | Reg_SingleFund_002_IOS_06 |
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 103                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_002_IOS_07 |
      | screenShotName1 | Reg_SingleFund_002_IOS_08 |
      | screenShotName2 | Reg_SingleFund_002_IOS_09 |
      | screenShotName3 | Reg_SingleFund_002_IOS_10 |
    Then check First Order In Wealth Page IOS
      | screenShotName | Reg_SingleFund_002_IOS_11 |

  @VBWMIS-T683
  Scenario:Reg_SingleFund_003 USD buy USD Fund(one investment) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_003_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_003_IOS_02 |
    Then get Feature Funds Themes IOS
      | screenShotName | Reg_SingleFund_003_IOS_03 |
    Then check Feature Funds List Page IOS
      | screenShotName | Reg_SingleFund_003_IOS_04 |
    Then search USD Funds IOS
      | screenShotName | Reg_SingleFund_003_IOS_05 |
    Then select One Fund To Buy
      | screenShotName | Reg_SingleFund_002_IOS_06 |
    Then switch HKD to USD Currency
      | screenShotName | Reg_SingleFund_003_IOS_07 |
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 103                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_003_IOS_08 |
      | screenShotName1 | Reg_SingleFund_003_IOS_09 |
      | screenShotName2 | Reg_SingleFund_003_IOS_10 |
      | screenShotName3 | Reg_SingleFund_003_IOS_11 |
    Then check First Order In Wealth Page IOS
      | screenShotName | Reg_SingleFund_003_IOS_12 |

  @VBWMIS-T686
  Scenario:Reg_SingleFund_004 HKD buy HKD Fund(one investment+monthly subscription) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_004_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_004_IOS_02 |
    Then get Feature Funds Themes IOS
      | screenShotName | Reg_SingleFund_004_IOS_03 |
    Then check Feature Funds List Page IOS
      | screenShotName | Reg_SingleFund_004_IOS_04 |
    Then search HKD Funds IOS
      | screenShotName | Reg_SingleFund_004_IOS_05 |
    Then search Funds by Name IOS
      | fundName | Allian |
    Then select One Fund To Buy
      | screenShotName | Reg_SingleFund_004_IOS_06 |
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 104                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_004_IOS_07 |
      | screenShotName1 | Reg_SingleFund_004_IOS_08 |
      | screenShotName2 | Reg_SingleFund_004_IOS_09 |
      | screenShotName3 | Reg_SingleFund_004_IOS_10 |
    Then check First Order In Wealth Page IOS
      | screenShotName | Reg_SingleFund_004_IOS_11 |

  @VBWMIS-T687
  Scenario:Reg_SingleFund_005 HKD buy HKD Fund(one investment) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_005_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_005_IOS_02 |
    Then get Feature Funds Themes IOS
      | screenShotName | Reg_SingleFund_005_IOS_03 |
    Then check Feature Funds List Page IOS
      | screenShotName | Reg_SingleFund_005_IOS_04 |
    Then search HKD Funds IOS
      | screenShotName | Reg_SingleFund_005_IOS_05 |
    Then select One Fund To Buy
      | screenShotName | Reg_SingleFund_005_IOS_06 |
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 105                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_005_IOS_07 |
      | screenShotName1 | Reg_SingleFund_005_IOS_08 |
      | screenShotName2 | Reg_SingleFund_005_IOS_09 |
      | screenShotName3 | Reg_SingleFund_005_IOS_10 |
    Then check First Order In Wealth Page IOS
      | screenShotName | Reg_SingleFund_005_IOS_11 |

  @VBWMIS-T684
  Scenario:Reg_SingleFund_006 Top up order(USD Fund) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_006_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_006_IOS_02 |
    Then open USD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_006_IOS_03 |
    Then click Add Buy Button
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 106                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_006_IOS_04 |
      | screenShotName1 | Reg_SingleFund_006_IOS_05 |
      | screenShotName2 | Reg_SingleFund_006_IOS_06 |
      | screenShotName3 | Reg_SingleFund_006_IOS_07 |
    Then check the Fund update
      | screenShotName | Reg_SingleFund_006_IOS_08 |

  @VBWMIS-T685
  Scenario:Reg_SingleFund_007 Top up order(HKD Fund) IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_007_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_007_IOS_02 |
    Then open HKD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_007_IOS_03 |
    Then click Add Buy Button
    Then buy One Fund In Funds List IOS
      | oneTimeAmount   | 107                       |
      | monthlyAmount   | 102                       |
      | screenShotName  | Reg_SingleFund_007_IOS_04 |
      | screenShotName1 | Reg_SingleFund_007_IOS_05 |
      | screenShotName2 | Reg_SingleFund_007_IOS_06 |
      | screenShotName3 | Reg_SingleFund_007_IOS_07 |
    Then check the Fund update
      | screenShotName | Reg_SingleFund_007_IOS_08 |


  @VBWMIS-T688
  Scenario:Reg_SingleFund_008 Sell Partial HKD Fund IOS
    And Login with user and password by params
      | user     | sta00797 |
      | password | Aa123321 |
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_008_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_008_IOS_02 |
    Then open HKD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_008_IOS_03 |
    Then click Sell Button
    Then sell Fund Partial Amount IOS
      | sellAmount      | 1                         |
      | screenShotName  | Reg_SingleFund_008_IOS_04 |
      | screenShotName1 | Reg_SingleFund_008_IOS_05 |
      | screenShotName2 | Reg_SingleFund_008_IOS_06 |
      | screenShotName3 | Reg_SingleFund_008_IOS_07 |
    Then check the Fund update
      | screenShotName | Reg_SingleFund_008_IOS_08 |


  @VBWMIS-T690
  Scenario:Reg_SingleFund_009 Sell Partial USD Fund IOS
    And Login with user and password by params
      | user     | sta00797 |
      | password | Aa123321 |
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_009_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_009_IOS_02 |
    Then open USD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_009_IOS_03 |
    Then click Sell Button
    Then sell Fund Partial Amount IOS
      | sellAmount      | 2                         |
      | screenShotName  | Reg_SingleFund_009_IOS_04 |
      | screenShotName1 | Reg_SingleFund_009_IOS_05 |
      | screenShotName2 | Reg_SingleFund_009_IOS_06 |
      | screenShotName3 | Reg_SingleFund_009_IOS_07 |
    Then check the Fund update
      | screenShotName | Reg_SingleFund_009_IOS_08 |

  @VBWMIS-T689
  Scenario:Reg_SingleFund_0010 Sell All HKD Fund IOS
    And Login with user and password by params
      | user     | sta00797 |
      | password | Aa123321 |
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_0011_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_0011_IOS_02 |
    Then open HKD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_0011_IOS_03 |
    Then click Sell Button
    Then sell Fund All Amount IOS
      | screenShotName  | Reg_SingleFund_0011_IOS_04 |
      | screenShotName1 | Reg_SingleFund_0011_IOS_05 |
      | screenShotName2 | Reg_SingleFund_0011_IOS_06 |
      | screenShotName3 | Reg_SingleFund_0011_IOS_07 |

  @VBWMIS-T691
  Scenario:Reg_SingleFund_0011 Sell All USD Fund IOS
    And Login with user and password by params
      | user     | sta00797 |
      | password | Aa123321 |
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_0010_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_0010_IOS_02 |
    Then open USD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_0010_IOS_03 |
    Then click Sell Button
    Then sell Fund All Amount IOS
      | screenShotName  | Reg_SingleFund_0010_IOS_04 |
      | screenShotName1 | Reg_SingleFund_0010_IOS_05 |
      | screenShotName2 | Reg_SingleFund_0010_IOS_06 |
      | screenShotName3 | Reg_SingleFund_0010_IOS_07 |

  @VBWMIS-T692
  Scenario:Reg_SingleFund_0012 Edit Monthly standing instruction IOS
    When Login with user and password from properties
    And I goto wealth page ios
      | screenShotName | Reg_SingleFund_0012_IOS_01 |
    Then wealth Check Funds Page IOS
      | screenShotName | Reg_SingleFund_0012_IOS_02 |
    Then open USD Fund On My Fund Page
      | screenShotName | Reg_SingleFund_0012_IOS_03 |
    Then click Monthly Edit
    Then edit Monthly Standing Instruction IOS
      | monthlyAmount   | 112                        |
      | screenShotName  | Reg_SingleFund_0012_IOS_04 |
      | screenShotName1 | Reg_SingleFund_0012_IOS_05 |
      | screenShotName2 | Reg_SingleFund_0012_IOS_06 |
      | screenShotName3 | Reg_SingleFund_0012_IOS_07 |
    Then check the Fund update
      | screenShotName | Reg_SingleFund_0012_IOS_08 |