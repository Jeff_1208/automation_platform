package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.LoginPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonStepsDef {

 LoginPage loginPage;
 CommonPage commonPage;

  public CommonStepsDef(){
    loginPage = new LoginPage();
    commonPage = new CommonPage();
  }

  private static final String login_icon_text = "Login";
  private static final String total_balance_text = "Total balance";
    private static final String total_balance_text_CH = "總結餘 (HKD)";

  private static final String expect_pending_desc =
      "We're working hard to process your application and will inform you via in-app notification within 2 business days.";
  //      "We're working hard to process this and will inform you via in-app notification within 2
  // business day.";

  @When("Login with user and password")
  public void loginWithUserAndPassword(Map<String, String> data) {
    loginPage.loginWithCredential(data.get("user"), data.get("password"));
  }

  @And("^login welab app with ([^\"]\\S*) and password")
  public void loginWithUserAndPass(String username, Map<String, String> data) {
    loginPage.loginWithCredential(username, data.get("password"));
  }

  @Given("Open WeLab App")
  public void openWeLabApp() throws Exception {
      commonPage.checkUpgrade();
//    String login_icon = loginPage.getLoginIconText();
    //assertThat(login_icon).isEqualTo(login_icon_text);
  }

  @And("Enable skip Root")
  public void enableSkipRoot() throws InterruptedException {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      commonPage.enableSkipRoot();
//      String actual_login_icon = loginPage.getLoginIconText();
      //assertThat(actual_login_icon).isEqualTo(login_icon_text);
    }
  }

  @And("goto loan main page")
  public void gotoLoanPage() throws InterruptedException {
    commonPage.gotoLoan();
  }

  @And("I goto wealth main page")
  public void gotoWealthPage() throws InterruptedException {
    commonPage.gotoWealth();

  }

  @Then("I'm on wealth home page")
  public void iMOnWealthHomePage() {
    boolean isDisplay = commonPage.checkNewGoal();
    assertThat(isDisplay).isTrue();
  }

  @And("goto My Account page")
  public void gotoMyAccountPage() {
    commonPage.gotoMyAccount();
  }

  @And("I goto Wealth Center")
  public void gotoWealthCenterPage() {
    commonPage.gotoWealthCenter();
  }

  @Then("I can see the LoggedIn page")
  public void verifyOnLoggedInPage() throws InterruptedException {
    loginPage.clickContactWarningLogin();
    // for demo sleep 2 seconds
    // Thread.sleep(2000);
    String checkedTxt = loginPage.getLoggedInPageText();
      boolean flag = false;
      if(checkedTxt.contains(total_balance_text)||checkedTxt.contains(total_balance_text_CH)){
          flag = true;
      }
      assertThat(flag).isTrue();
  }

  @Given("Check upgrade page appears")
  public void checkUpgradePageAppears() {
    commonPage.checkUpgrade();
  }

  @Then("I can see pending approval screen")
  public void iCanSeePendingApproval() {
    String approvalTxt = commonPage.getPendingApprovalDesc();
    assertThat(approvalTxt).isEqualTo(expect_pending_desc);
  }

  @Then("I finish account opening process")
  @Then("I can see Wealth Landing Page")
  public void finishIAO() {
    boolean result = commonPage.checkNewGoal();
    assertThat(result).isTrue();
  }

  @And("I skip guidance")
  public void skipGuidance() {
    commonPage.skipGuidance();
  }

  @And("I go to Verify personal info")
  public void goToVerifyPersonalInfo() {
    commonPage.skipToOpenAccount();
  }

  @When("I click X icon")
  public void iClickXIcon() {
    commonPage.clickXIcon();
  }

  @Then("I Cannot View Wealth Centre")
  public void verifyWealthCentreDisabled() {
    boolean result = commonPage.checkWealthCentreDisabled();
    assertThat(result).isTrue();
  }

  @Then("I can see pending page")
  public void verifyPendingPage(Map<String, String> data) {
    String titleText = commonPage.getElementText(commonPage.getThanksTitle()).trim();
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
  }

  @When("I click back icon")
  public void iClickBackIcon() {
    commonPage.clickBackIcon();
  }

  @And("I move eyeBtn to top right corner")
  public void iMoveEyeBtnToTopRight() {
    // if the mobile platform is iOS, then no eyeBtn.
    if (System.getProperty("mobile").equals("android")) {
      commonPage.moveEyeBtnToTopRight();
    }
  }

  @Given("I launch app with account")
  public void iLaunchAppWithAccount(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String account = data.get(0).get("account");
    String password = data.get(0).get("password");
    try {
      commonPage.changeAccount(account, password);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @And("^I update wealth SDK account with ([^\"]\\S*) and password")
  public void updateWealthSDKWithUserAndPass(String username, Map<String, String> data) {
    try {
      commonPage.changeAccount(username, data.get("password"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @And("I can see App homepage")
  public void verifyOnLoginPage(){
    String checkedTxt = loginPage.getLoggedInPageText();
      boolean flag = false;
      if(checkedTxt.contains(total_balance_text)||checkedTxt.contains(total_balance_text_CH)){
          flag = true;
      }
      assertThat(flag).isTrue();
  }

  @And("I click Wealth Center")
  public void iClickWealthCentre(){
    commonPage.iClickWealthCentre();
  }


  @SneakyThrows
  @And("I click Review investment account profile")
  public void iClickReviewInvestmentAccountProfile(){
    Thread.sleep(2*1000);
    commonPage.iClickReviewInvestmentAccountProfile();
  }

  @And("I get default options")
  public void iGetDefaultOptions(){
    commonPage.iGetDefaultInvestmentHorizon();
    commonPage.iGetDefaultKnowledgeAndExperience();
  }

  @And("I click Investment Horizon edit Button")
  public void iClickSingleChoiceEditButton(){
    commonPage.iClickSingleChoiceEditButton();
  }

  @And("I choose Up to 3 years")
  public void iChooseUpTo3Years(){
    commonPage.iChooseUpTo3Years();
  }

  @And("I click multiple choice edit button and choose options")
  public void iClickMutipleChoiceEditButton(){
    commonPage.iClickMutipleChoiceEditButton();
  }

  @And("I click OK Button")
  public void iClickOKButton(){
    commonPage.iClickOKButton();
  }

  @And("I can see multiple choice display in Review investment account profile")
  public void iCanSeeMultipleChoice(){
    commonPage.iCanSeeMutipleChoice();
  }

  @And("I verify Investment Horizon update in Review investment account profile page")
  public void iVerifyInvestmentHorizonUpdate(){
    boolean InvestmentHorizon = commonPage.iVerifyInvestmentHorizonUpdate();
    assertThat(InvestmentHorizon).isTrue();
  }

  @SneakyThrows
  @And("I verify options have not changed")
  public void iVerifyOptionsHaveNotChange(){
    Thread.sleep(2*1000);
    boolean investmentHorizonNotChange = commonPage.iVerifyInvestmentHorizonNotChange();
    assertThat(investmentHorizonNotChange).isTrue();
    boolean knowledgeAndExperienceNotChange = commonPage.iVerifyKnowledgeAndExperienceNotChange();
    assertThat(knowledgeAndExperienceNotChange).isTrue();
  }

  @Then("I can see three sessions divided")
  public void iCanSeeThreeSessionsDivided(){
   boolean threeSessionDivided = commonPage.iCanSeeThreesessionDivided();
   assertThat(threeSessionDivided).isTrue();
  }

  @And("I review CRPQ questions")
  public void iReviewCRPQQuestion(){
    boolean reviewCRPQQuestions = commonPage.iReviewCRPQQuestion();
    assertThat(reviewCRPQQuestions).isTrue();
  }

    @When("Login with user and password from properties")
    public void loginWithUserAndPasswordFromProperties() {
        loginPage.loginWithUserAndPasswordFromProperties();
    }

    @When("Login with user and password by params")
    public void loginWithUserAndPasswordByParams(Map<String, String> data) {
        loginPage.loginWithUserAndPasswordByParams(data.get("user"), data.get("password"));
    }

    @And("I goto wealth page")
    public void iGotoWealthPage(Map<String, String> data) {
        commonPage.openWealthPage(data.get("screenShotName"));
    }

    @Then("I click Open Account button")
    public void clickOpenAccountBtn(Map<String, String> data) {
        commonPage.openAccount(data.get("screenShotName"));
    }

    @And("I goto wealth page ios")
    public void iGotoWealthPageIOS(Map<String, String> data) {
        commonPage.openWealthPageIOS(data.get("screenShotName"));
    }


}
