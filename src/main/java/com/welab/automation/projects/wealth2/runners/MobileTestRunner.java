package com.welab.automation.projects.wealth2.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = {
                "src/main/java/com/welab/automation/projects/wealth2/features/mobile/android",
        },
        glue = {
                "com/welab/automation/projects/wealth2/steps/mobile",
        },
//        tags="@wealth",
        monochrome = true)
public class MobileTestRunner extends TestRunner {}