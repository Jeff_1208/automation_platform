package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;

import java.util.List;

public class OnTackGoalsPage extends AppiumBasePage{

    protected String pageName = "OnTackGoals Page";
    CommonPage commonPage;
    SingleFundPage singleFundPage;

    public OnTackGoalsPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        singleFundPage = new SingleFundPage();
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='GoWealth']"),
    })
    private MobileElement wealthPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Goal Selection']"),
    })
    private MobileElement goalSelection;

    @AndroidFindAll({
            @AndroidBy( xpath = "//android.widget.TextView[@text='On track']")
    })
    private MobileElement onTrackBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='View recommendation & save my goal']")
    private MobileElement viewUpdateRecommendationBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='OK']")
    private MobileElement okBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Yes']")
    private MobileElement yesBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Taking into consideration your latest inputs')]")
    private MobileElement keepUpWithTheGoodWorkText;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'The monthly investment you have set')]")
    private MobileElement confirmThatYouWantToProceedText;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'The latest inputs fall outside of our recommendation range')]")
    private MobileElement updatedRecommendationIsNotAvailableText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='View New Portfolio']")
    private MobileElement viewNewPortfolioBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Next']")
    private MobileElement nextBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm']")
    private MobileElement confirmBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='View updated recommendation']")
    private MobileElement viewUpdatedRecommendationBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Wealth Management Services Terms']")
    private MobileElement wealthManagementServicesTermsText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Important Notes and Risk Disclosures']")
    private MobileElement importantNotesAndRiskDisclosuresText;

    @AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"btnBackContainer\"])[1]/android.view.ViewGroup[1]/android.view.ViewGroup")
    private MobileElement backBtn;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]")
    private MobileElement slideToConfirm;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]")
    private MobileElement radioBtn2;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Asset')]")
    private MobileElement assetClassText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Review and confirm order']")
    private MobileElement reviewAndConfirmOrderText;



    @SneakyThrows
    public void clickOnTrackGoal(String screenShotName01, String screenShotName02){
        scrollUpToFindElement(onTrackBtn,10,10);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName01);
        clickElement(onTrackBtn);
        Thread.sleep(1000);
        takeScreenshot(screenShotName02);

    }

    @SneakyThrows
    public void clickViewUpdateRecommendationBtn(String screenShotName,String screenShotName01){
        clickElement(viewUpdatedRecommendationBtn);
        Thread.sleep(2*1000);
        if (verifyElementExist(keepUpWithTheGoodWorkText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(okBtn);
        }else if (verifyElementExist(confirmThatYouWantToProceedText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(yesBtn);
            takeScreenshot(screenShotName01);
            clickElement(okBtn);
        }else if (verifyElementExist(viewNewPortfolioBtn)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
        }else if (verifyElementExist(updatedRecommendationIsNotAvailableText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
        }else {
            logger.info("请检查是否存在OnTrack状态的goal！");
        }
    }

    @SneakyThrows
    public void clickViewNewPortfolioBtn(String screenShotName05){
        Thread.sleep(5*1000);
        takeScreenshot(screenShotName05);
        clickElement(viewNewPortfolioBtn);
    }

    @SneakyThrows
    public void clickViewUpdateRecommendationBtnAndClickNextBtn(String screenShotName,String screenShotName01, String screenShotName02){
        clickElement(viewUpdatedRecommendationBtn);
        Thread.sleep(2*1000);
        if (verifyElementExist(keepUpWithTheGoodWorkText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(okBtn);
        }else if (verifyElementExist(confirmThatYouWantToProceedText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(yesBtn);
            takeScreenshot(screenShotName01);
            clickElement(okBtn);
        }else if (verifyElementExist(viewNewPortfolioBtn)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(viewNewPortfolioBtn);
            if (verifyElementExist(nextBtn)){
                Thread.sleep(2*1000);
                takeScreenshot(screenShotName01);
            }
        }else if (verifyElementExist(updatedRecommendationIsNotAvailableText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName);
            clickElement(confirmBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName01);
            scrollUpToFindElement(nextBtn,3,10);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName02);
        }else {
            logger.info("请检查是否存在OnTrack状态的goal！");
        }
    }

    @SneakyThrows
    public void verifyHaveTwoDocuments(List<String> screenShotName){
        clickElement(viewUpdatedRecommendationBtn);
        Thread.sleep(2*1000);
        if (verifyElementExist(keepUpWithTheGoodWorkText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(okBtn);
        }else if (verifyElementExist(confirmThatYouWantToProceedText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(yesBtn);
            takeScreenshot(screenShotName.get(1));
            clickElement(okBtn);
        }else if (verifyElementExist(viewNewPortfolioBtn)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(viewNewPortfolioBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(1));
            Thread.sleep(2*1000);
            waitUntilElementVisible(assetClassText);
            clickElement(nextBtn);
            Thread.sleep(3*1000);
            takeScreenshot(screenShotName.get(2));
            waitUntilElementVisible(reviewAndConfirmOrderText);
            scrollUpToFindElement(nextBtn,10,10);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(3));
            clickElement(nextBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(4));

            checkServicesAndRisk(screenShotName.get(5), screenShotName.get(6));
        }else if (verifyElementExist(updatedRecommendationIsNotAvailableText)) {
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(confirmBtn);
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName.get(1));
            scrollUpToFindElement(nextBtn, 3, 10);
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName.get(2));

            checkServicesAndRisk(screenShotName.get(3), screenShotName.get(4));
        } else {
            logger.info("请检查是否存在OnTrack状态的goal！");
        }
    }

    @SneakyThrows
    public void orderSuccessfully(List<String> screenShotName){
        clickElement(viewUpdatedRecommendationBtn);
        Thread.sleep(2*1000);
        if (verifyElementExist(keepUpWithTheGoodWorkText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(okBtn);
        }else if (verifyElementExist(confirmThatYouWantToProceedText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(yesBtn);
            takeScreenshot(screenShotName.get(1));
            clickElement(okBtn);
        }else if (verifyElementExist(viewNewPortfolioBtn)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(viewNewPortfolioBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(1));
            clickElement(nextBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(2));

            scrollUpToFindElement(nextBtn,10,10);
            Thread.sleep(2*1000);

            takeScreenshot(screenShotName.get(3));
            clickElement(nextBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(4));

            checkServicesAndRisk(screenShotName.get(5), screenShotName.get(6));
            takeScreenshot(screenShotName.get(7));

            singleFundPage.confirmReviewAndSlideToConfirmOrder();
        }else if (verifyElementExist(updatedRecommendationIsNotAvailableText)){
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(0));
            clickElement(confirmBtn);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(1));

            scrollUpToFindElement(nextBtn,3,10);
            Thread.sleep(2*1000);
            takeScreenshot(screenShotName.get(2));

            checkServicesAndRisk(screenShotName.get(3), screenShotName.get(4));
            takeScreenshot(screenShotName.get(5));

            singleFundPage.confirmReviewAndSlideToConfirmOrder();
        }else {
            logger.info("请检查是否存在OnTrack状态的goal！");
        }
    }

    @SneakyThrows
    public void checkServicesAndRisk(String screenShotName, String screenShotName01) {
        scrollUpToFindElement(wealthManagementServicesTermsText, 3, 10);
        clickElement(wealthManagementServicesTermsText);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(backBtn);
        Thread.sleep(2 * 1000);
        clickElement(importantNotesAndRiskDisclosuresText);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName01);
        clickElement(backBtn);
    }

    @SneakyThrows
    public void clickViewUpdatedRecommendation(String screenShotName) {
        Thread.sleep(2000);
        clickElement(viewUpdatedRecommendationBtn);
        if(isShow(confirmBtn,5)){
            clickElement(confirmBtn);
        }
//        takeScreenshot(screenShotName);
//        clickElement(viewNewPortfolioBtn);
//        clickElement(nextBtn);
        scrollUpToFindElement(nextBtn,10,3);
        clickElement(nextBtn);
    }
}
