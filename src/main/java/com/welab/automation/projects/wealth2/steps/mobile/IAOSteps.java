package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.LoginPage;
import com.welab.automation.projects.wealth2.pages.iao.IAOIOSPage;
import com.welab.automation.projects.wealth2.pages.iao.IAOPage;
import com.welab.automation.projects.wealth2.pages.iao.OrderPlace3GoalPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
public class IAOSteps {
    LoginPage loginPage;
    CommonPage commonPage;
    IAOPage iaoPage;
    IAOIOSPage iaoIosPage;
    OrderPlace3GoalPage orderPlace3GoalPage;

    public IAOSteps(){
        loginPage = new LoginPage();
        commonPage = new CommonPage();
        iaoPage = new IAOPage();
        iaoIosPage = new IAOIOSPage();
        orderPlace3GoalPage = new OrderPlace3GoalPage();
    }

    @Then("I can see Marketing preferences Page")
    public void iCanSeeMarketingPreferences(Map<String,String>data) {
        assertThat(iaoPage.iCanSeeMarketingPreferences(data.get("screenShotName"))).isTrue();
    }

    @And("I click gotIt on wealth page")
    public void iClickGotItOnWealthPage() {
        iaoPage.clickGotIt();
    }

    @And("I click XBtn to skip")
    public void iClickXBtnToSkip() {
        iaoPage.clickXBtnToSkip();
    }

    @And("I click Learn More on goWealth page")
    public void iClickLearnMoreOnGoWealthPage() {
        iaoPage.clickLearnMore();
    }

    @And("I click next on Welcome to Wealth Management Services Page")
    public void iClickNextOnWelcomeToWealthManagementServicesPage(Map<String,String>data) {
        iaoPage.scroll();
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @And("I click next on Update and verify personal info page")
    public void iClickNextOnUpdateAndVerifyPersonalInfoPage(Map<String,String>data) {
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @And("I click next on Verify personal information page")
    public void iClickNextOnVerifyPersonalInformationPage(Map<String,String>data) {
        iaoPage.scroll();
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @And("I click next on Investment Product Knowledge and Experience page")
    public void iClickNextOnInvestmentProductKnowledgeAndExperiencePage() {
        iaoPage.clickNext();
    }
//
//    @And("I enter Name of your employer and confirm")
//    public void iEnterNameOfYourEmployer(Map<String, String> data) {
//        System.out.println(data.get("name")+"1111111111");
//        iaoPage.iEnterNameOfYourEmployer(data.get("name"));
//        iaoPage.clickConfirm();
//    }

    @And("I Modify one of Review investment account profile")
    public void iModifyOneOfUpdateAndVerifyPersonalInfo() {
        iaoPage.iModifyOneOfUpdateAndVerifyPersonalInfo();
    }

    @And("I enter input MSK key")
    public void iEnterInputMSKKey() {
        commonPage.waiteSendMsk();
    }

    @Then("I click back on Update and verify personal info page")
    public void iClickBackOnNetWorthPage() {
        commonPage.clickBackIcon();
    }

    @And("I verify on home page")
    public void iVerifyOnHomePage() {
        assertThat(iaoPage.iVerifyOnHomePage()).isTrue();
    }

    @And("I click circle and submit on Update and verify personal info")
    public void iClickCircleAndSubmitOnUpdateAndVerifyPersonalInfo(Map<String,String>data) {
        iaoPage.clickCircleOnTermsPage();
        iaoPage.clickSubmit(data.get("screenShotName"));
    }

    @And("I click next on Your application is being processed page")
    public void iClickNextOnYourApplicationIsBeingProcessedPage() {
        iaoPage.clickNext();
    }

    @And("I click next on Upload document if required page")
    public void iClickNextOnUploadDocumentIfRequiredPage(Map<String,String>data) {
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }
//
//    @And("I click confirm in Upload document if required page")
//    public void iClickConfirmInUploadDocumentIfRequiredPage() {
//        iaoPage.clickConfirm();
//    }

    @And("I click next on Complete Customer Risk Profiling Questionnaire page")
    public void iClickNextOnCompleteCustomerRiskProfilingQuestionnairePage(Map<String,String>data) {
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @And("I click confirm on Confirm page")
    public void iClickConfirmOnConfirmPage(Map<String,String>data) {
        iaoPage.clickConfirm(data.get("screenShotName"));
    }

    @And("I verify Customer Risk Rating")
    public void iVerifyCustomerRiskRating(Map<String, String> data) {
        iaoPage.clickConfirm(data.get("screenShotName"));
        assertThat(iaoPage.iVerifyCustomerRiskRating(data.get("risk"))).isTrue();
        iaoPage.clickNextWithScreen(data.get("screenShotName01"));
    }

    @And("I click next on Your Customer Risk Rating page")
    public void iClickNextOnYourCustomerRiskRatingPage() {
        iaoPage.clickNext();
    }

    @And("I click next on Congratulations page")
    public void iClickNextOnCongratulationsPage(Map<String, String> data) {
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @Then("I verify On Thanks you for completing the application page")
    public void iVerifyOnThanksYouForCompletingTheApplicationPage() {
        iaoPage.iVerifyOnThanksYouForCompletingTheApplicationPage();
    }


    @And("I skip to open an account")
    public void iSkipToOpenAnAccount() {
        iaoPage.iSkipToOpenAnAccount();
    }
    @Then("I'm on wealth home page in 6s")
    public void iMOnWealthHomePageIn6s() {
        boolean isDisplay = iaoPage.checkWealth();
        assertThat(isDisplay).isTrue();
    }
    @And("I scroll Up to find the button")
    public void findAddNewGoals() {
        iaoPage.findAddnewgoals();
    }


    @When("I click Add New Goals Button")
    public void addNewGoalsBtn() {
        iaoPage.addNewGoal();
    }

    @And("I click Reach My Target")
    public void clickReachMyTarget() {
        iaoPage.clickEleByText("Reach My Target");
    }

    @And("^I edit goal name ([^\"]\\S*)$")
    public void editGoalName(String txt) {
        iaoPage.editGoalName(txt);
        assertThat(iaoPage.getGoalName().getText()).isEqualTo(txt);
    }

    @And("I click back button")
    public void backBtn() {
        iaoPage.backBtn();
    }

    @And("I check Horizon edit")
    public void checkHorizonEdit() {
        assertThat(iaoPage.checkHorizonEdit()).isTrue();
    }

    @When("^I Set Investment Plan ([^\"]\\S*) and ([^\"]\\S*)$")
    public void setInvestmentPlan(
            String targetValue, String time, String onetimeValue, String monthlyValue) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
        iaoPage.setTargetGoal(targetValue, time);
        if (null == targetValue) targetValue = "1000000";
        assertThat(iaoPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
        assertThat(iaoPage.getSMTAge()).isEqualTo(time);
        iaoPage.clickContinue();
        assertThat(iaoPage.getGoalAchieveAmount()).isEqualTo(nf.format(Long.valueOf(targetValue)));
        iaoPage.setInvestmentPlan(onetimeValue, monthlyValue);
        iaoPage.saveGoal();
    }

    @And("I setup my target and Investment Plan")
    public void iSetupMyTargetAndInvestmentPlan(DataTable dataTable) {
        List<Map<String, String>> data = dataTable.asMaps();
        String targetValue = data.get(0).get("targetValue");
        String time = data.get(0).get("time");
        String onetimeValue = data.get(0).get("onetime");
        String monthlyValue = data.get(0).get("monthly");
        setInvestmentPlan(targetValue, time, onetimeValue, monthlyValue);
    }

    @And("I click View Portfolio Button")
    public void clickViewPortfolio() {
        iaoPage.clickEleByText("View Portfolio");
    }


    @And("I click Next Button four times")
    public void iClickNextButton(Map<String, String> data) {
        orderPlace3GoalPage.clickNextBtnOnScenarioAnalysisPage(data.get("screenShotName"));
        orderPlace3GoalPage.clickNextBtnOnPortfolioPage(data.get("screenShotName01"));
        orderPlace3GoalPage.clickNextBtnOnOrderPage(data.get("screenShotName02"));
        orderPlace3GoalPage.clickNextButtonOnReviewAndConfirmPage(data.get("screenShotName03"),data.get("screenShotName04"));
    }


    @And("I slide to confirm order")
    public void slideToConFirmOrder(Map<String, String> data) {
        iaoPage.slideToConfirmOrder(data.get("screenShotName"),data.get("screenShotName01"));
    }
    @And("I setup a large my target")
    public void iSetupLargeMyTarget(String targetValue, String time) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
        iaoPage.setTargetGoal(targetValue, time);
        if (null == targetValue) targetValue = "1000000";
        assertThat(iaoPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
        assertThat(iaoPage.getSMTAge()).isEqualTo(time);
    }

    @Then("I can see Exceed maximum amount")
    public void ICanSeeExceedMaximumAmount() {
        assertThat(iaoPage.verifyExceedMaximumAmount()).isTrue();
    }
    @And("I check Wealth Management Services Terms")
    public void checkWealthManagementServicesTerms(Map<String, String> data) {
        iaoPage.checkServicesTerms(data.get("screenShotName"));
    }
    @And("I check Important Notes and Risk Disclosures")
    public void checkImportantNotesAndRiskDisclosures(Map<String, String> data) {
        iaoPage.checkINAndRD(data.get("screenShotName"));
    }

    @SneakyThrows
    @And("I click Next Button three times")
    public void iClickNextButtonThreeTimes() {
        Thread.sleep(4000);
        iaoPage.clickEleByText("Next");
        Thread.sleep(2000);
        iaoPage.clickEleByText("Next");
        Thread.sleep(2000);
        iaoPage.clickEleByText("Next");
    }

    @Then("Click Achieve Financial Freedom")
    public void clickAchieveFinancialFreedom() {
        iaoPage.clickFinancialPage();
    }

    @And("click and verify the right arrow text prompt")
    public void clickAndVerifyRightArrow() {
        iaoPage.clickRightArrow();
        String rightArrowText = iaoPage.getIntoRightAllowText().getText();
        assertThat(rightArrowText.contains("financial freedom")).isTrue();
    }

    @Then("I setup my target wealth and age")
    public void iSetupMyTargetWealthAndAge(Map<String, String> data) {
        iaoPage.setupMyTargetWealthAndAge(data.get("targetValue"),data.get("age"),data.get("screenShotName"));
    }

    @Then("I setup oneTime and monthly investment")
    public void iSetupOneTimeAndMonthlyInvestment(Map<String, String> data) {
        iaoPage.setupOneTimeAndMonthlyInvestment(data.get("oneTime"),data.get("monthly"),data.get("screenShotName"));
    }

    @Then("I click create goal flow")
    public void iClickCreateGoalFlow(Map<String, String> data) {
        orderPlace3GoalPage.clickSaveMyGoalBtn(data.get("screenShotName"));
        orderPlace3GoalPage.clickViewPortfolioBtn(data.get("screenShotName01"));
        orderPlace3GoalPage.clickNextBtnOnScenarioAnalysisPage(data.get("screenShotName02"));
        orderPlace3GoalPage.clickNextBtnOnPortfolioPage(data.get("screenShotName03"));
        orderPlace3GoalPage.clickNextBtnOnOrderPage(data.get("screenShotName04"));
        orderPlace3GoalPage.clickNextButtonOnReviewAndConfirmPage(data.get("screenShotName05"),data.get("screenShotName06"));
        iaoPage.slideToConfirmOrder(data.get("screenShotName07"),data.get("screenShotName08"));
        orderPlace3GoalPage.clickDoneBtn(data.get("screenShotName09"));
    }

    @And("I click next on Confirm information Page")
    public void iClickNextOnConfirmInformationPage(Map<String, String> data) {
        iaoPage.clickNextWithScreen(data.get("screenShotName"));
    }

    @Then("I click Start button Complete Customer Risk Profiling")
    public void iClickStartButtonCompleteCustomerRiskProfiling(Map<String, String> data) {
        iaoPage.clickStartBtn(data.get("screenShotName"));
    }

    @Then("I verify open account success")
    public void iVerifyOpenAccountSuccess(Map<String, String> data) {
        assertThat(iaoPage.openAccountSuccess(data.get("screenShotName"))).isTrue();
    }

    @Then("Open Wealth Account IOS")
    public void clickOpenWealthAccountIOS() {
        iaoIosPage.clickOpenWealthAccountIOS();
    }

    @Then("click next Confirm information Page IOS")
    public void clickNextConfirmInformationPageIOS(Map<String, String> data) {
        iaoIosPage.clickNextConfirmInformationPageIOS(data.get("screenShotName"),
                data.get("screenShotName1"), data.get("screenShotName2"));
    }

    @Then("answer Questions IOS")
    public void answerQuestions(Map<String, String> data) {
        iaoIosPage.answerQuestions(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("I select no skip item IOS")
    public void IselectNoSkipItem(Map<String, String> data) {
        iaoIosPage.IselectNoSkipItem(data.get("screenShotName"));
    }

    @Then("I select Yes Consent Letter From Your Employer")
    public void IselectYesConsentLetterFromYourEmployer(Map<String, String> data) {
        iaoIosPage.IselectYesConsentLetterFromYourEmployer(data.get("screenShotName"));
    }

    @Then("confirm application Information and upload picture")
    public void confirmApplicationInformationAndUploadPicture(Map<String, String> data) {
        iaoIosPage.confirmApplicationInformationAndUploadPicture(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"),data.get("screenShotName4"));
    }

    @Then("confirm application Information steps")
    public void confirmApplicationInformationSteps(Map<String, String> data) {
        iaoIosPage.confirmApplicationInformationSteps(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"),data.get("screenShotName4"));
    }

    @Then("fill Customer Risk Profiling Questions IOS")
    public void fillCustomerRiskProfilingQuestions(Map<String, String> data) {
        iaoIosPage.fillCustomerRiskProfilingQuestions(data.get("screenShotName"), data.get("screenShotName1"),
                data.get("screenShotName2"), data.get("screenShotName3"), data.get("screenShotName4"),
                data.get("screenShotName5"), data.get("screenShotName6"));
    }
    @Then("confirm Risk Detail IOS")
    public void confirmRiskDetail(Map<String, String> data) {
        iaoIosPage.confirmRiskDetail(data.get("screenShotName"), data.get("screenShotName"));
    }

    @Then("check Application Result IOS")
    public void checkApplicationResult(Map<String, String> data) {
        iaoIosPage.checkApplicationResult(data.get("screenShotName"));
    }

    @And("I click back button by location")
    public void backBtnByLocation() {
        iaoPage.backBtnLocation();
    }

    @Then("Login with IAO Account")
    public void loginWithIAOAccount() throws IOException {
        loginPage.loginWithIAOAccount("IAO");
    }

    @And("I click DIY My Plan")
    public void iClickDIYMyPlan() {
        iaoPage.clickDIYMyPlan();
    }

    @Then("Login with IAO Account for MCV")
    public void loginWithIAOAccountForMCV() throws IOException {
        loginPage.loginWithIAOAccount("IAO_MCV");
    }

    @Then("Verify GPS Checking for MCV")
    public void verifyGPSCheckingForMCV(Map<String, String> data) {
        iaoPage.verifyGPSCheckingForMCV(data.get("screenShotName"),data.get("screenShotName1"));
    }
}
