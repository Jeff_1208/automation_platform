package com.welab.automation.projects.wealth2.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
public class OpenAccountPage extends AppiumBasePage {

    private final String pageName = "OpenAccount Page";
    private int startX = 0;
    private int startY = 0;
    private int width =  0;
    private int height = 0;

    CommonPage commonPage;
    public OpenAccountPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Open account']"),
            @AndroidBy(xpath = "//*[@text='開戶']"),
    })
    private MobileElement openAccountBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Eligibility']"),
            @AndroidBy(xpath = "//*[@text='開戶要求']"),
    })
    private MobileElement eligibilityText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Get started']"),
            @AndroidBy(xpath = "//*[@text='開始申請']"),
    })
    private MobileElement getStartedText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'get started')]/../../android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//*[contains(@text,'開立賬戶')]/../../android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> eligibilityTip;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'get started')]/../../android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//*[contains(@text,'開立賬戶')]/../../android.view.ViewGroup[2]"),
    })
    private MobileElement eligibilityXBtn;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText")
    private MobileElement phoneInput;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.EditText")
    private MobileElement emailInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'Invalid mobile number')]"),
            @AndroidBy(xpath = "//*[contains(@text,'正確嘅手機號碼')]"),
    })
    private MobileElement invalidPhoneHint;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'Invalid email address')]"),
            @AndroidBy(xpath = "//*[contains(@text,'正確嘅電郵地址')]"),
    })
    private MobileElement invalidEmailHint;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'Account Term')]"),
            @AndroidBy(xpath = "//*[contains(@text,'賬戶條款')]"),
    })
    private MobileElement acceptText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='text']/../android.view.ViewGroup")
    private MobileElement backMainPageBtn;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[1]")
    private MobileElement termsOfUseOfAppMainText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='text']/../android.view.ViewGroup/following-sibling::android.widget.TextView")
    private MobileElement termsTitleText;

    @AndroidFindBy(xpath = "//android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[2]/android.widget.TextView[1]")
    private MobileElement termsContentText;

    @AndroidFindBy(xpath = "//android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[3]")
    private MobileElement accountTermsContentText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='你嘅核心賬戶已處於「不動賬戶」狀態。']"),
            @AndroidBy(xpath = "//*[@text='Your Core Account has become dormant.']")
    })
    private MobileElement dormantTitleText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'需要幫助或想重新激活核心賬戶')]"),
            @AndroidBy(xpath = "//*[contains(@text,'want to reactivate your Core Account')]")
    })
    private MobileElement dormantContentText;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup/android.widget.TextView")
    private MobileElement dormantOkText;

    @AndroidFindBy(xpath = "//*[@text='GoSave']")
    private MobileElement goSaveBtn;

    @AndroidFindBy(xpath = "//android.widget.Button[@content-desc=\"GoWealth, tab, 5 of 5\"]/android.widget.TextView[2]")
    private MobileElement wealthTab;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.Button[@content-desc=\"Transfer, tab, 3 of 5\"]/android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.Button[@content-desc=\"存款與轉賬, tab, 3 of 5\"]/android.widget.TextView"),
    })
    private MobileElement transferBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Oops! Incorrect username/password, please try again']"),
            @AndroidBy(xpath = "//*[@text='請輸入正確嘅用戶名稱/密碼']")
    })
    private MobileElement loginFailedText;

    @Getter
    @AndroidFindBy(xpath = "//android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup")
    private MobileElement transactionsRecord;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup")
    private MobileElement totalBalanceDetailBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"Total Balance (HKD)\"]/following-sibling::android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"總結餘 (HKD)\"]/following-sibling::android.widget.TextView"),
    })
    private MobileElement totalBalanceHKD;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView[3]")
    private MobileElement wealthBalanceHKD;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView[4]")
    private MobileElement wealthBalanceHKDCH;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView[2]")
    private MobileElement wealthBalancePercent;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView[3]")
    private MobileElement wealthBalancePercentCH;


    @SneakyThrows
    public void openAccountBtn(String screenShotName) {
        Thread.sleep(3*1000);
        waitUntilElementClickable(openAccountBtn);
        clickElement(openAccountBtn);
        Thread.sleep(2*1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickEligibilityText() {
        waitUntilElementClickable(eligibilityText);
        Thread.sleep(2*1000);
        clickElement(getStartedText);
        clickElement(eligibilityText);
    }

    @SneakyThrows
    public void verifyEligibilityText(String screenShotName) {
        waitUntilElementVisible(eligibilityTip.get(0),5);
        if (System.getProperty("language").contains("ch")) {
            assertThat(eligibilityTip.get(0).getText().contains("只需")).isTrue();
            assertThat(eligibilityTip.get(1).getText()).isEqualTo("年滿18歲或以上及持有香港身份證");
            assertThat(eligibilityTip.get(2).getText()).isEqualTo("有香港居住地址");
        }else {
            assertThat(eligibilityTip.get(0).getText().contains("need to get started")).isTrue();
            assertThat(eligibilityTip.get(1).getText().contains("At least 18 years old and have an HKID")).isTrue();
            assertThat(eligibilityTip.get(2).getText().contains("Have a Hong Kong residential address")).isTrue();
        }
        takeScreenshot(screenShotName);
        Thread.sleep(2*1000);
        clickElement(eligibilityXBtn);
    }

    @SneakyThrows
    public boolean inputIncorrectPhone(String incorrectPhone,String screenShotName) {
        waitUntilElementVisible(eligibilityText);
        clearAndSendKeys(phoneInput,incorrectPhone);
        clickElement(getStartedText);
        takeScreenshot(screenShotName);
        Thread.sleep(2*1000);
        return verifyElementExist(invalidPhoneHint);
    }

    @SneakyThrows
    public boolean inputIncorrectEmail(String incorrectEmail,String screenShotName) {
        clearAndSendKeys(emailInput,incorrectEmail);
        clickElement(getStartedText);
        takeScreenshot(screenShotName);
        Thread.sleep(2*1000);
        return verifyElementExist(invalidEmailHint);
    }

    public void getTermsLocation() {
        Point acceptTextLocation = acceptText.getLocation();
        startX = acceptTextLocation.getX();
        startY = acceptTextLocation.getY();
        Dimension acceptSize = acceptText.getSize();
        width = acceptSize.width;
        height = acceptSize.height;
    }


    @SneakyThrows
    public boolean clickTermsOfUse(String screenShotName, String screenShotName1, String screenShotName2) {
        //click first one
        clickByLocation(startX + width - 100, startY);
        Thread.sleep(5 * 1000);
        if (termsTitleText.getText().equals("Terms of Use of App") || termsTitleText.getText().equals("應用程式使用條款")) {
            takeScreenshot(screenShotName);
            for (int i = 0; i < 20; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName1);
            for (int i = 0; i < 12; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName2);
            clickElement(backMainPageBtn);
            Thread.sleep(2 * 1000);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean clickAccountTerms(String screenShotName3, String screenShotName4, String screenShotName5) {
        //click second one
        clickByLocation(startX + 150, startY + height - 10);
        Thread.sleep(5 * 1000);
        if (termsTitleText.getText().equals("賬戶條款") || termsTitleText.getText().equals("Account Terms")) {
            takeScreenshot(screenShotName3);
            for (int i = 0; i < 45; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName4);
            for (int i = 0; i < 65; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName5);
            clickElement(backMainPageBtn);
            Thread.sleep(2 * 1000);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean clickPrivacyPolicy(String screenShotName6, String screenShotName7, String screenShotName8) {
        //click last one
        clickByLocation(startX + 300, startY + height - 10);
        if (termsTitleText.getText().equals("私隱政策") || termsTitleText.getText().equals("Privacy Policy")) {
            takeScreenshot(screenShotName6);
            for (int i = 0; i < 25; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName7);
            for (int i = 0; i < 7; i++) {
                scrollUpEntireScreen();
            }
            takeScreenshot(screenShotName8);
            clickElement(backMainPageBtn);
            Thread.sleep(2 * 1000);
            return true;
        }
        return false;
    }

    @SneakyThrows
    public boolean verifyDormantText(String screenShotName) {
        Thread.sleep(6*1000);
        if(verifyElementExist(dormantTitleText) && verifyElementExist(dormantContentText)){
            Thread.sleep(6*1000);
            takeScreenshot(screenShotName);
            clickElement(dormantOkText);
            clickElement(goSaveBtn);
        }
        Thread.sleep(6*1000);
        return verifyElementExist(dormantTitleText) && verifyElementExist(dormantContentText);
    }

    @SneakyThrows
    public boolean verifyWealthAndTranIcon(String screenShotName) {
        Thread.sleep(6*1000);
        takeScreenshot(screenShotName);
        return verifyElementExist(wealthTab)&&verifyElementExist(transferBtn);
    }


    @SneakyThrows
    public boolean checkWealthBalanceDetail(String screenShotName1) {
        clickElement(totalBalanceDetailBtn);
        Thread.sleep(6*1000);
        takeScreenshot(screenShotName1);
        String totalBalanceNum = totalBalanceHKD.getText().replaceAll(",", "");
        BigDecimal totalBalance = new BigDecimal(totalBalanceNum);
        String wealthBalanceNum =wealthBalanceHKD.getText().replace("HKD ", "").replace(",", "");
        BigDecimal wealthBalance = new BigDecimal(wealthBalanceNum);
        BigDecimal wealthPercent = wealthBalance.divide(totalBalance,3, RoundingMode.DOWN);
        DecimalFormat df = new DecimalFormat("0.0%");
        String wealthFormatPercent = df.format(wealthPercent);
        return wealthFormatPercent.equals(wealthBalancePercent.getText().replace(" ",""));
    }

    @SneakyThrows
    public boolean getLoginErrorMessage(String screenShotName) {
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        return verifyElementExist(loginFailedText);
    }

}
