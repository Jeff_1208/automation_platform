package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth2.pages.CommonPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;
import org.openqa.selenium.By;


public class DraftGoalPage extends AppiumBasePage {

    protected String pageName = "Wealth Page";
    CommonPage commonPage;
    SingleFundPage singleFundPage;

    public DraftGoalPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        singleFundPage = new SingleFundPage();
    }

    private String getGoalName;
    private String goalName;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='GoWealth']"),
    })
    private MobileElement wealthPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Goal Selection']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='選擇目標']")
    })
    private MobileElement goalSelection;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Add New Goals']"),
            @AndroidBy(xpath = "//*[@text='增設理財目標']")
    })
    private MobileElement addNewGoalsLink;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Select your goal']"),
            @AndroidBy(xpath = "//*[@text='選擇新嘅理財目標']")
    })
    private MobileElement selectGoalTitleTxt;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text=\"Reach My Target\"]")
    })
    private MobileElement reachMyTarget;

    @AndroidFindBy(
            xpath = "//android.widget.ScrollView/android.view.ViewGroup[1]/android.widget.EditText[1]")
    private MobileElement nameInput;

    @AndroidFindBy(
            xpath =
                    "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    private MobileElement GoalNameEditButton;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
            @AndroidBy(
                    xpath =
                            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
    })
    private MobileElement targetEditBtn;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
            @AndroidBy(
                    xpath =
                            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
    })
    private MobileElement targetInput;

    @AndroidFindBy(xpath = "//*[@text='Exceed maximum amount']")
    private MobileElement exceedMsg;

    @AndroidFindBy(
            xpath =
                    "(//android.widget.ScrollView//android.widget.ImageView)[2]/../android.widget.TextView")
    private MobileElement SMTAgeSlider;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Draft']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='草稿']")
    })
    private MobileElement draftBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
    })
    private MobileElement draftNameEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.EditText[@content-desc=\"MY_GOAL_DETAIL-editName\"]")
    })
    private MobileElement editName;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnBackContainer\"]")
    private MobileElement backToWealthBtn;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'bruce1')]/../android.widget.TextView[1]")
    private MobileElement setTargetName;

    @AndroidFindBy(
            xpath = "//android.widget.TextView[contains(@text,'bruce2')]/../android.widget.TextView[1]")
    private MobileElement deleteTargetName;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Edit']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='編輯']")
    })
    private MobileElement editBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='View recommendation & save my goal']")
    private MobileElement viewRecommendationAndSaveMyGoalBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"]")
    private MobileElement viewPortfolioBtn;

    @AndroidFindBy(xpath = "//*[contains(@text,'Conservative Multi-Asset')]")
    private MobileElement conservativeMultiAssetText;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Next']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='下一步']"),
    })
    private MobileElement nextBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Review and confirm order']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='檢視及確定交易指示']"),
    })    private MobileElement reviewAndConfirmOrderText;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]")
    private MobileElement slideToConfirm;

    @AndroidFindBy(xpath = "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]")
    private MobileElement radioBtn2;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Delete']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='刪除']")
    })
    private MobileElement deleteBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Confirm']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='確定']")
    })
    private MobileElement confirmBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Taking into consideration your latest inputs')]")
    private MobileElement keepUpWithTheGoodWorkText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm that you want to proceed?']")
    private MobileElement confirmProceedText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Yes']")
    private MobileElement yesBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Back to Home']")
    private MobileElement backToHomeBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='查看目標']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='View goals']")
    })
    private MobileElement viewGoal;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Current Wealth (HKD)']"),
            @AndroidBy(xpath = "//*[@text='現時財富總值(港元)']")
    })
    private MobileElement currentWealth;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Reach My Target']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='達成我的目標']/following-sibling::android.view.ViewGroup[1]"),
    })
    private MobileElement reachMyTargetEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Want to buy ')]/../../android.view.ViewGroup[1]/preceding-sibling::android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'話我哋知你嘅 ')]/../../android.view.ViewGroup[1]/preceding-sibling::android.widget.EditText")
    })
    private MobileElement reachMyTargetInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Continue']"),
            @AndroidBy(xpath = "//*[@text='繼續']")
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Continue']/..")
    private MobileElement continueBtn;

    @SneakyThrows
    public boolean goToWealthPage(String screenShotName) {
        Thread.sleep(10 * 1000);
        waitUntilElementClickable(wealthPage);
        clickElement(wealthPage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        return verifyElementExist(viewGoal);
    }


    @SneakyThrows
    public void clickDraft(String screenShotName, String screenShotName01) {
        scrollUpToFindElement(draftBtn, 10, 10);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clickElement(draftBtn);
//        getGoalName = getElementText(editName);
        Thread.sleep(1 * 1000);
        if (screenShotName01 != null) {
            takeScreenshot(screenShotName01);
        }
    }

    public void clickNameEdit() {
        clickElement(draftNameEdit);
    }

    @SneakyThrows
    public void editGoalName(String GoalName, String screenShotName) {
        goalName = GoalName;
        waitUntilElementClickable(editName);
        clearAndSendKeys(editName, GoalName);
        inputEnter(editName);
        Thread.sleep(1 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void backToWealthPage(String screenShotName) {
        clickElement(backToWealthBtn);
        Thread.sleep(1 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public boolean selectTargetGoalName(String screenShotName) {
        String editGoalNameXpath;
        if (IS_IOS) {
            editGoalNameXpath = "//XCUIElementTypeStaticText[@name='" + goalName + "']";
            logger.info("xpath: {}", editGoalNameXpath);
        } else {
            if (goalName == null) {
                goalName = GlobalVar.GLOBAL_VARIABLES.get("goalName");
            }
            editGoalNameXpath =
                    "//android.widget.TextView[contains(@text,'" + goalName + "')]/../android.widget.TextView[1]";
            logger.info("xpath: {}", editGoalNameXpath);
        }
        scrollUpToFindElement(By.xpath(editGoalNameXpath), 10, 10);
        if (verifyElementExist(By.xpath(editGoalNameXpath))) {
            Thread.sleep(2 * 1000);
            takeScreenshot(screenShotName);
            return true;
        } else {
            Thread.sleep(1000);
            takeScreenshot(screenShotName);
            return false;
        }
    }

    @SneakyThrows
    public void clickEditBtn(String screenShotName) {
        Thread.sleep(1 * 1000);
        clickElement(editBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void clickViewRecommendationAndSaveMyGoalBtn(String screenShotName) {
        scrollUpToFindElement(viewRecommendationAndSaveMyGoalBtn, 3, 5);
        clickElement(viewRecommendationAndSaveMyGoalBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
    }

    @SneakyThrows
    public void clickTargetGoal( String screenShotName) {
        String targetGoalXpath;
        if (IS_IOS) {
            targetGoalXpath = "//XCUIElementTypeStaticText[@name='" + goalName + "']";
            logger.info("xpath: {}", targetGoalXpath);
        } else {
            if (goalName == null) {
                goalName = GlobalVar.GLOBAL_VARIABLES.get("goalName");
            }
            targetGoalXpath =
                    "//android.widget.TextView[contains(@text,'" + goalName + "')]/../android.widget.TextView[1]";
            logger.info("xpath: {}", targetGoalXpath);
        }
        scrollUpToFindElement(By.xpath(targetGoalXpath), 20, 3).click();
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        Thread.sleep(2 * 1000);
    }

    @SneakyThrows
    public void clickViewPortfolioBtn(String screenShotName) {
        if (verifyElementExist(confirmProceedText)) {
            clickElement(yesBtn);
        }
        waitUntilElementVisible(viewPortfolioBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clickElement(viewPortfolioBtn);
    }

    @SneakyThrows
    public void clickNextBtnOnScenarioAnalysisPage(String screenShotName) {
        clickNextBtn(screenShotName);
    }

    @SneakyThrows
    public void clickNextBtnOnPortfolioPage(String screenShotName) {
        clickNextBtn(screenShotName);
    }

    @SneakyThrows
    public void clickNextBtnOnOrderPage(String screenShotName) {
        clickNextBtn(screenShotName);
    }


    @SneakyThrows
    public void clickNextBtn(String screenShotName) {
        waitUntilElementVisible(nextBtn);
        Thread.sleep(2 * 1000);
        if (screenShotName != null) {
            takeScreenshot(screenShotName);
        }
        clickElement(nextBtn);
    }


    @SneakyThrows
    public void reviewAndConfirmOrder(String screenShotName07, String screenShotName08) {
        waitUntilElementVisible(reviewAndConfirmOrderText);
        Thread.sleep(2 * 1000);
        if (screenShotName07 != null) {
            takeScreenshot(screenShotName07);
        }
//        scrollUpFast();
        scrollUpToFindElement(nextBtn, 20, 30);
        Thread.sleep(2 * 1000);
        if (screenShotName08 != null) {
            takeScreenshot(screenShotName08);
        }
        clickElement(nextBtn);
    }

    @SneakyThrows
    public void slideToConfirmOrder(String screenShotName) {
        if (verifyElementExist(backToHomeBtn)) {
            clickElement(backToHomeBtn);
        } else {
            Thread.sleep(2 * 1000);
            if (screenShotName != null) {
                takeScreenshot(screenShotName);
            }
            singleFundPage.confirmReviewAndSlideToConfirmOrder();
//            scrollUpToFindElement(slideToConfirm, 8, 3);
//            Thread.sleep(2*1000);
//            if (screenShotName12!=null){
//                takeScreenshot(screenShotName12);
//            }
//            radioBtn2.click();
//            Thread.sleep(2*1000);
//            if (screenShotName13!=null){
//                takeScreenshot(screenShotName13);
//            }
//            Thread.sleep(1000);
//            // top-left
//            int x = slideToConfirm.getLocation().x;
//            int y = slideToConfirm.getLocation().y;
//            // size
//            int width = slideToConfirm.getRect().width;
//            int x_start = x + 50;
//            int x_end = x + width;
//            int y_co = y + 2;
//            TouchAction action =
//                    new TouchAction(driver)
//                            .longPress(PointOption.point(x_start, y_co))
//                            .moveTo(PointOption.point(x_end, y_co))
//                            .release();
//            action.perform();
//            Thread.sleep(1000 * 10);
        }
    }

    @SneakyThrows
    public void clickDeleteBtn(String screenShotName, String screenShotName01) {
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        clickElement(deleteBtn);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName01);
        clickElement(confirmBtn);
    }

    @SneakyThrows
    public boolean verifyGoalDeleted(String screenShotName) {
        String deleteGoalXpath;
        if (IS_IOS) {
            deleteGoalXpath = "//XCUIElementTypeStaticText[@name='" + getGoalName + "']";
            logger.info("xpath: {}", deleteGoalXpath);
        } else {
                goalName = GlobalVar.GLOBAL_VARIABLES.get("goalName");
            deleteGoalXpath =
                    "//*[@text='" + goalName + "']";
            logger.info("xpath: {}", deleteGoalXpath);
        }
        scrollUpToFindElement(By.xpath(deleteGoalXpath), 5, 3);
        Thread.sleep(2 * 1000);
        takeScreenshot(screenShotName);
        return verifyElementNotExist(By.xpath(deleteGoalXpath));
    }

    public boolean verifyPortfolioRecommendationPageNormal() {
        if (verifyElementExist(confirmProceedText)) {
            clickElement(yesBtn);
        }
        if (verifyElementExist(viewPortfolioBtn)) {
            return true;
        }
        return false;
    }

    @SneakyThrows
    public void clickGoalNameWith(String name, String screenShotName01) {
        String goalXpath;
        if (IS_IOS) {
            goalXpath = "//XCUIElementTypeStaticText[@name='" + name + "']";
            logger.info("xpath: {}", goalXpath);
        } else {
            goalXpath =
                    "//android.widget.TextView[contains(@text,'" + name + "')]/../android.widget.TextView[1]";
            logger.info("xpath: {}", goalXpath);
        }
        scrollUpToFindElement(By.xpath(goalXpath), 8, 10);
        clickElement(isElementByClickable(By.xpath(goalXpath)));
        Thread.sleep(2 * 1000);
        if (screenShotName01 != null) {
            takeScreenshot(screenShotName01);
        }
    }

    @SneakyThrows
    public boolean clickViewGoal(String screenShotName) {
        Thread.sleep(3000);
        clickElement(viewGoal);
        Thread.sleep(3000);
        waitUntilElementVisible(currentWealth);
        takeScreenshot(screenShotName);
        return currentWealth.getText().contains("Current Wealth") || currentWealth.getText().contains("現時財富總值");
    }

    @SneakyThrows
    public void editReachGoalName(String name, String screenShotName) {
        getGoalName = name;
        Thread.sleep(3 * 1000);
        waitUntilElementVisible(reachMyTargetEdit);
        clickElement(reachMyTargetEdit);
        Thread.sleep(2 * 1000);
        waitUntilElementVisible(reachMyTargetInput);
        clearAndSendKeys(reachMyTargetInput, name);
        inputEnter(reachMyTargetInput);
        waitUntilElementVisible(continueBtn);
        takeScreenshot(screenShotName);
//        clickElement(continueBtn);
        Thread.sleep(2 * 1000);
    }

    @SneakyThrows
    public boolean verifyGoalDraftStatus(String screenShotName) {
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        return verifyElementExist(draftBtn);
    }

    @SneakyThrows
    public void verifyGoalSuccessAndClick(String screenShotName,String screenShotName1){
        Thread.sleep(10000);
        goalName = GlobalVar.GLOBAL_VARIABLES.get("goalName");
        String editGoalNameXpath = "//*[@text='" + goalName + "']";
        Thread.sleep(3000);
        scrollDown();
        if(verifyElementExist(By.xpath(editGoalNameXpath))){
            takeScreenshot(screenShotName);
        }else {
            scrollUpToFindElement(By.xpath(editGoalNameXpath), 10, 2);
            takeScreenshot(screenShotName);
        }
        driver.findElement(By.xpath(editGoalNameXpath)).click();
        Thread.sleep(3000);
        takeScreenshot(screenShotName1);
    }

    public void deleteGoalAndConfirm() {
        waitUntilElementVisible(deleteBtn);
        clickElement(deleteBtn);
        waitUntilElementVisible(confirmBtn);
        clickElement(confirmBtn);
    }
}


