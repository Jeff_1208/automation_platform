package com.welab.automation.projects.wealth2.steps.mobile;

import com.welab.automation.projects.wealth2.pages.CommonPage;
import com.welab.automation.projects.wealth2.pages.LoginPage;
import com.welab.automation.projects.wealth2.pages.iao.SingleFundPage;
import static org.assertj.core.api.Assertions.assertThat;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

public class SingleFundSteps {
    LoginPage loginPage;
    CommonPage commonPage;
    SingleFundPage singleFundPage;

    public SingleFundSteps(){
        loginPage = new LoginPage();
        commonPage = new CommonPage();
        singleFundPage = new SingleFundPage();
    }


    @And("I verify Available balance before transaction")
    public void iVerifyAvailableBalanceBeforeTransaction() {
        singleFundPage.getAvailableBalanceBefore();

    }

    @Then("I click view my funds on GoWealth Page")
    public void iClickViewMyFundsOnGoWealthPage(Map<String, String> data) {
        singleFundPage.clickViewMyFundsOnGoWealthPage(data.get("screenShotName"));
    }

    @And("Choose a fund that is not zero")
    public void chooseAFundIsNoZero() {
        assertThat(singleFundPage.chooseAFundIsNoZero()).isTrue();
    }

    @Then("I click Buy button on Fund detail page")
    public void iClickBuyButtonOnFundDetailPage(Map<String, String> data) {
        singleFundPage.clickBuyButtonOnFundDetailPage(data.get("screenShotName"));
    }

    @And("I enter buy fund investment ([^\"]\\S*) and ([^\"]\\S*) and ([^\"]\\S*)$")
    public void iEnterBuyFundInvestmentAmount(String oneTime, String type, String monthly) {
        assertThat(singleFundPage.enterBuyFundInvestmentAmount(oneTime, type, monthly)).isTrue();
        assertThat(singleFundPage.buyFundOneTimeInvestment()).isTrue();
    }

    @Then("I confirm Review and confirm Order")
    public void iConfirmReviewAndConfirmOrder() {
        singleFundPage.confirmReviewAndConfirmOrder();

    }

    @Then("I verify buy fund success")
    public void iVerifyBuyFundSuccess(Map<String, String> data) {
        assertThat(singleFundPage.verifyBuyFundSuccess(data.get("screenShotName"))).isTrue();

    }

    @And("I verify buy fund order status")
    public void iVerifyBuyFundOrderStatus(Map<String, String> data) {
        assertThat(singleFundPage.verifyBuyFundOrderStatus(data.get("screenShotName"), data.get("screenShotName01"))).isTrue();
    }

    @Then("order status page back to home page")
    public void orderStatusPageBackToHomePage() {
        singleFundPage.orderStatusPageBackToHomePage();
    }

    @And("I verify buy fund balance")
    public void iVerifyBuyFundBalance() {
        assertThat(singleFundPage.verifyBalance()).isTrue();
    }

    @Then("I verify buy fund record")
    public void iVeifyBuyFundRecord() {
        assertThat(singleFundPage.verifyBuyFundByRecord()).isTrue();
    }


    @And("I goto Total balance page")
    public void iGotoTotalBalancePage() {
        assertThat(singleFundPage.gotoTotalBalancePage()).isTrue();

    }

    @Then("I verify Total Balance before transaction")
    public void iVerifyTotalBalanceBeforeTransaction() {
        singleFundPage.verifyTotalBalanceBeforeTransaction();
    }

    @And("Total balance page back to home page")
    public void totalBalancePageBackToHomePage() {
        singleFundPage.totalBalancePageBackToHomePage();
    }

    @And("I goto USD Balance page")
    public void iGotoUSDBalancePage() {
        singleFundPage.gotoUSDBalancePage();
    }

    @Then("I verify USD balance After transaction")
    public void iVerifyUSDBalanceAfterTransaction() {
        assertThat(singleFundPage.verifyUSDBalanceAfterTransaction()).isTrue();
    }


    @Then("I click sell button on Fund detail page")
    public void iClickSellButtonOnFundDetailPage(Map<String,String>data) {
        assertThat(singleFundPage.clickSellButtonOnFundDetailPage(data.get("screenShotName"))).isTrue();
    }

    @And("I enter sell fund ([^\"]\\S*) and ([^\"]\\S*)$")
    public void iEnterSellFundAmountAndType(String amount,String type) {
        assertThat(singleFundPage.enterSellFundAmountAndType(amount,type)).isTrue();

    }

    @Then("I verify sell fund by success")
    public void iVerifySellFundBySuccess(Map<String, String> data) {
        assertThat(singleFundPage.verifySellFundBySuccess(data.get("screenShotName"))).isTrue();
    }

    @And("I verify sell fund order status")
    public void iVerifySellFundOrderStatus(Map<String, String> data) {
        assertThat(singleFundPage.verifySellFundOrderStatus(data.get("screenShotName"), data.get("screenShotName01"))).isTrue();
    }

    @Then("I verify sell fund balance After transaction")
    public void iVerifySellFundBalanceAfterTransaction() {
        assertThat(singleFundPage.verifySellFundBalanceAfterTransaction()).isTrue();
    }

    @Then("I get Monthly standing instruction amount before update")
    public void iGetMonthlyStandingInstructionAmountBeforeUpdate() {
        singleFundPage.monthlyStandingInstructionAmountBeforeUpdate();
    }

    @And("I goto Monthly standing instruction Edit page")
    public void iGotoMonthlyStandingInstructionEditPage(Map<String,String>data) {
        assertThat(singleFundPage.gotoMonthlyStandingInstructionEditPage(data.get("screenShotName"))).isTrue();
    }

    @Then("I enter Monthly instruction amount choose ([^\"]\\S*)$")
    public void iEnterMonthlyInstructionAmount(String type) {
        assertThat(singleFundPage.enterMonthlyInstructionAmount(type)).isTrue();
        assertThat(singleFundPage.verifyEquivalentMonthlyInvestment()).isTrue();
    }

    @And("I verify update Monthly instruction success")
    public void iVerifyUpdateMonthlyInstructionSuccess(Map<String,String>data) {
        assertThat(singleFundPage.verifyUpdateMonthlyInstructionSuccess(data.get("screenShotName"))).isTrue();
    }

    @Then("I verify Monthly instruction amount changed")
    public void iVerifyMonthlyInstructionAmountChanged(Map<String,String>data) {
        assertThat(singleFundPage.verifyMonthlyInstructionAmountChanged(data.get("screenShotName"))).isTrue();
    }

    @And("I goto Order status page to verify Monthly instruction")
    public void iGotoOrderStatusPageToVerifyMonthlyInstruction(Map<String,String>data) {
        singleFundPage.gotoOrderStatusPage();
        assertThat(singleFundPage.verifyMonthlyInstruction(data.get("screenShotName"))).isTrue();
    }

    @Then("standing instruction page back to Fund detail page")
    public void standingInstructionPageBackToFundDetailPage() {
        singleFundPage.standingInstructionPageBackToFundDetailPage();
    }
    @And("I click Featured Funds")
    public void iChooseAFund(Map<String, String> data) {
        singleFundPage.clickFeaturedFunds(data.get("screenShotName"));
    }

    @And("I click Show me relevant funds")
    public void clickShowRelevantFunds() {
        singleFundPage.clickShowRelevantFunds();
    }


    @And("I click Buy On Featured Fund Page")
    public void clickBuyOnFeaturedFundPage(Map<String, String> data) {
        singleFundPage.clickBuyOnFeaturedFund(data.get("screenShotName"));
    }

    @Then("I confirm Review and Slide to confirm Order")
    public void iConfirmReviewAndSlideToConfirmOrder() {
        singleFundPage.confirmReviewAndSlideToConfirmOrder();
    }

    @And("I click text")
    public void clickByText(Map<String, String> data) {
        singleFundPage.clickByText(data.get("text"));
    }

    @And("I click products")
    public void clickByProducts(Map<String, String> data) {
        singleFundPage.clickByProducts(data.get("products"));
    }

    @And("I click random Top fund Investment themes")
    public void iClickRandomTopFund() {
        singleFundPage.clickRandomTopFund();
    }

    @And("I click random Equity fund Investment themes")
    public void iClickRandomEquityFund() {
        singleFundPage.clickRandomEquityFund();
    }

    @And("I click random Bond fund Investment themes")
    public void iClickRandomBondFund() {
        singleFundPage.clickRandomBondFund();
    }

    @And("I click random Mixed Asset fund Investment themes")
    public void iClickRandomMixedFund() {
        singleFundPage.clickRandomMixedFund();
    }

    @And("I click random Related Products")
    public void iClickRandomRelatedProducts() {
        singleFundPage.randomRelatedProducts();
    }

    @Then("I verify sell goal order status")
    public void iVerifySellGoalOrderStatus(Map<String, String> data) {
        assertThat(singleFundPage.verifySellGoalOrderStatus(data.get("screenShotName"))).isTrue();
    }

    @Then("I enter New monthly Investment")
    public void iEnterNewMonthlyInvestment(Map<String,String>data) {
        singleFundPage.enterNewMonthlyInvestment(data.get("newMonthly"),data.get("screenShotName"));
    }

    @Then("I verify buy goal order status")
    public void iVerifyBuyGoalOrderStatus(Map<String, String> data) {
        assertThat(singleFundPage.verifyBuyGoalOrderStatus(data.get("screenShotName"))).isTrue();
    }

    @Then("I click View full fund list")
    public void iClickViewFullFundList(Map<String, String> data) {
        singleFundPage.clickViewFullFundList(data.get("screenShotName"));
    }

    @Then("I search USD fund")
    public void iSearchUSDFund(Map<String, String> data) {
        singleFundPage.searchUSDFund(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("I click random fund on Full Fund List Page")
    public void iClickRandomFundOnFullFundListPage(Map<String, String> data) {
        singleFundPage.clickRandomFund(data.get("screenShotName"));
    }

    @And("I enter buy fund investment oneTime and monthly")
    public void iEnterBuyFundInvestmentOneTimeAndMonthly(Map<String, String> data) {
        singleFundPage.enterBuyFundOneTime(data.get("oneTime"));
        singleFundPage.enterBuyFundMonthly(data.get("monthly"));
        singleFundPage.clickNextBtn(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @And("I enter buy fund investment oneTime")
    public void iEnterBuyFundInvestmentOneTime(Map<String, String> data) {
        singleFundPage.enterBuyFundOneTime(data.get("oneTime"));
        singleFundPage.clickNextBtn(data.get("screenShotName"), data.get("screenShotName01"));
        assertThat(singleFundPage.verifyBuyConfirmReviewAndOrder()).isTrue();
    }

    @Then("I click Currency Switch button")
    public void iClickCurrencySwitchButton() {
        singleFundPage.clickCurrencySwitchButton();
    }

    @Then("I search HKD fund")
    public void iSearchHKDFund(Map<String, String> data) {
        singleFundPage.searchHKDFund(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @Then("I click USD Fund From Fund List")
    public void iClickUSDFundFromFundList(Map<String, String> data) {
        assertThat(singleFundPage.clickUSDFundFromFundList(data.get("screenShotName"))).isTrue();
    }

    @Then("I click HKD Fund From Fund List")
    public void iClickHKDFundFromFundList(Map<String, String> data) {
        assertThat(singleFundPage.clickHKDFundFromFundList(data.get("screenShotName"))).isTrue();
    }

    @And("find can sell HKD Fund From Fund List")
    public void findCanSellHKDFundFromFundList(Map<String, String> data) {
        singleFundPage.findCanSellHKDFund(data.get("screenShotName"));
    }

    @And("find can sell USD Fund From Fund List")
    public void findCanSellUSDFundFromFundList(Map<String, String> data) {
        singleFundPage.findCanSellUSDFund(data.get("screenShotName"));
    }

    @And("I enter sell One-off Redemption")
    public void iEnterSellOneOffRedemption(Map<String, String> data) {
        singleFundPage.sellFundOneOffRedemption(data.get("amount"));
        singleFundPage.clickNextBtn(data.get("screenShotName"), data.get("screenShotName01"));
        assertThat(singleFundPage.verifySellConfirmReviewAndOrder()).isTrue();
    }

    @Then("I enter Monthly instruction amount")
    public void iEnterMonthlyInstructionAmount(Map<String,String>data) {
        assertThat(singleFundPage.enterMonthlyInstruction(data.get("screenShotName"),data.get("screenShotName01"))).isTrue();
    }
}
