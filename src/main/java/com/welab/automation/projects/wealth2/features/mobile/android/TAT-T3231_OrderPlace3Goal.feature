#Feature: OrderPlace3Goal
#
#  Background: Login
#    Given Open WeLab App
##    And Check upgrade page appears
#    And Enable skip Root
#    When Login with user and password from properties
##    And Login with user and password
##      | user     | swmcust00166 |
##      | password | Aa123456     |
##    And I can see the LoggedIn page
#
#  @OrderPlace_01
#  Scenario:Existing account total balance screen check
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    Then  I'm on wealth home page in 6s
#
#  @OrderPlace_02
#  Scenario:Target OrderPlace page rename One1
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_002_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_002_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachBBF                  |
#      | screenShotName | Reg_Wealth_002_Android_03 |
#    And I click back button
#
#
##
##  @OrderPlace_03
##  Scenario Outline:Target OrderPlace page Survey suggests One1
##    Then I goto wealthPage
##      | screenShotName | Reg_Wealth_002_Android_01 |
##    Then  I'm on wealth home page in 6s
##    And I scroll Up to find the button
##    And I click Add New Goals Button
##    And I click Reach My Target
##    And I edit goal name <GoalName>
##    And I click back button
##    Examples:
##      | GoalName |
##      | ReachccF |
#
#  @OrderPlace_03
#  Scenario:Target OrderPlace page One2 Horizon edit
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_003_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_003_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachBBF                  |
#      | screenShotName | Reg_Wealth_003_Android_03 |
#    Then I click Continue button
#      | screenShotName | Reg_Wealth_003_Android_04 |
#    And I check Horizon edit
#
#  @OrderPlace_04
#  Scenario:Target OrderPlace page One2 ”ageAndMoney“edit
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_004_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_004_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachBBF                  |
#      | screenShotName | Reg_Wealth_004_Android_03 |
#    Then I setup my target wealth and age
#      | targetValue    | 160000                    |
#      | age            | 65                        |
#      | screenShotName | Reg_Wealth_004_Android_04 |
#    Then I click Continue button
#      | screenShotName | Reg_Wealth_004_Android_05 |
#
#  @OrderPlace_05
#  Scenario:Target OrderPlace page One ”The initial investment is 0, and the next month's investment is not 0“success
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_005_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_005_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachBBF                  |
#      | screenShotName | Reg_Wealth_005_Android_03 |
#    Then I setup my target wealth and age
#      | targetValue    | 160000                    |
#      | age            | 65                        |
#      | screenShotName | Reg_Wealth_005_Android_04 |
#    Then I click Continue button
#      | screenShotName | Reg_Wealth_005_Android_05 |
#    Then I setup oneTime and monthly investment
#      | oneTime        | 0                         |
#      | monthly        | 4800                      |
#      | screenShotName | Reg_Wealth_005_Android_06 |
#    Then I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_005_Android_07 |
#    And I verify Portfolio Recommendation page normal
#    And I click Next Button four times
#      | screenShotName   | Reg_Wealth_005_Android_08 |
#      | screenShotName01 | Reg_Wealth_005_Android_09 |
#      | screenShotName02 | Reg_Wealth_005_Android_10 |
#      | screenShotName03 | Reg_Wealth_005_Android_11 |
#      | screenShotName04 | Reg_Wealth_005_Android_12 |
#    And I slide to confirm order
#      | screenShotName01 | Reg_Wealth_005_Android_13 |
#      | screenShotName02 | Reg_Wealth_005_Android_14 |
#      | screenShotName03 | Reg_Wealth_005_Android_15 |
#
#  @OrderPlace_06
#  Scenario:Target OrderPlace page One3 ”not sufficient funds Order Failure
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_006_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_006_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    Then I setup my target wealth and age
#      | targetValue    | 1600000000                |
#      | age            | 65                        |
#      | screenShotName | Reg_Wealth_006_Android_03 |
#    Then I can see Exceed maximum amount
#
#
#  @OrderPlace_07
#  Scenario:Target OrderPlace page One4 Financial Terms of Service and Important Notes and Risk Disclosures check
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_007_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_007_Android_02 |
#    And I scroll Up to find the button
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    Then I setup my target wealth and age
#      | targetValue    | 160000                    |
#      | age            | 65                        |
#      | screenShotName | Reg_Wealth_007_Android_03 |
#    Then I click Continue button
#      | screenShotName | Reg_Wealth_007_Android_04 |
#    Then I setup oneTime and monthly investment
#      | oneTime        | 41000                     |
#      | monthly        | 2700                      |
#      | screenShotName | Reg_Wealth_007_Android_05 |
#    Then I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_007_Android_06 |
#    And I click View Portfolio button on Portfolio Recommendation Page
#    And I click Next Button four times
#      | screenShotName   | Reg_Wealth_007_Android_07 |
#      | screenShotName01 | Reg_Wealth_007_Android_08 |
#      | screenShotName02 | Reg_Wealth_007_Android_09 |
#      | screenShotName03 | Reg_Wealth_007_Android_10 |
#      | screenShotName04 | Reg_Wealth_007_Android_11 |
#    And I check Wealth Management Services Terms
#      | screenShotName | Reg_Wealth_007_Android_12 |
#    And I click back button
#    And I check Important Notes and Risk Disclosures
#      | screenShotName | Reg_Wealth_007_Android_13 |
#
##  @OrderPlace_09
##  Scenario:Target OrderPlace page One2 ”Disclosure of important matters and risks“
##    Then I goto wealthPage
##      | screenShotName | Reg_Wealth_008_Android_01 |
##    Then  I'm on wealth home page in 6s
##    And I scroll Up to find the button
##    And I click Add New Goals Button
##    And I click Reach My Target
##    And I setup my target and Investment Plan
##      | targetValue | time | onetime | monthly |
##      | 160000      | 60   | 41000   | 2700    |
##    And I click View Portfolio Button
##    And I click Next Button four times
##    And I check Important Notes and Risk Disclosures
##      | screenShotName | Reg_Wealth_008_Android_02 |
#
#  @OrderPlace_08
#  Scenario:Target OrderPlace page One5 Successful transaction
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_008_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_008_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachBBF                  |
#      | screenShotName | Reg_Wealth_008_Android_03 |
#    Then I setup my target wealth and age
#      | targetValue    | 160000                    |
#      | age            | 65                        |
#      | screenShotName | Reg_Wealth_008_Android_04 |
#    Then I click Continue button
#      | screenShotName | Reg_Wealth_008_Android_05 |
#    Then I setup oneTime and monthly investment
#      | oneTime        | 41000                     |
#      | monthly        | 2700                      |
#      | screenShotName | Reg_Wealth_008_Android_06 |
#    Then I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_008_Android_07 |
#    And I click View Portfolio button on Portfolio Recommendation Page
#    And I click Next Button four times
#      | screenShotName   | Reg_Wealth_008_Android_08 |
#      | screenShotName01 | Reg_Wealth_008_Android_09 |
#      | screenShotName02 | Reg_Wealth_008_Android_10 |
#      | screenShotName03 | Reg_Wealth_008_Android_11 |
#      | screenShotName04 | Reg_Wealth_008_Android_12 |
#    And I slide to confirm order
#      | screenShotName01 | Reg_Wealth_008_Android_13 |
#      | screenShotName02 | Reg_Wealth_008_Android_14 |
#      | screenShotName03 | Reg_Wealth_008_Android_15 |
#
#  @OrderPlace_09
#  Scenario Outline:create Achieve Financial Freedom with transaction exception(sta00232:Aa123321)
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_009_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_009_Android_02 |
#    And I click Add New Goals Button
#    And I choose Achieve Financial Freedom
#      | screenShotName | Reg_Wealth_009_Android_03 |
#    And I slide to <age>
#      | screenShotName   | Reg_Wealth_009_Android_04 |
#      | screenShotName01 | Reg_Wealth_009_Android_05 |
#    And I click Continue button
#      | screenShotName | Reg_Wealth_009_Android_06 |
#    And I input Monthly expenses
#      | monthlyExpenses  | 99999                     |
#      | screenShotName   | Reg_Wealth_009_Android_07 |
#      | screenShotName01 | Reg_Wealth_009_Android_08 |
#    And I click Calculate Target Wealth button
#      | screenShotName | Reg_Wealth_009_Android_09 |
#    And I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_009_Android_10 |
#    And I click View Portfolio button on Portfolio Recommendation Page
#      | screenShotName | Reg_Wealth_009_Android_11 |
#    And I click Next Button four times
#      | screenShotName   | Reg_Wealth_009_Android_12 |
#      | screenShotName01 | Reg_Wealth_009_Android_13 |
#      | screenShotName02 | Reg_Wealth_009_Android_14 |
#      | screenShotName03 | Reg_Wealth_009_Android_15 |
#      | screenShotName04 | Reg_Wealth_009_Android_16 |
#    And I verify have not enough money
#      | screenShotName | Reg_Wealth_009_Android_17 |
#    Examples:
#      | age |
#      | 54  |
#
#  @OrderPlace_10
#  Scenario:Achieve Financial Freedom goal edit name
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_010_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_010_Android_02 |
#    And I click Add New Goals Button
#    And I click Achieve Financial Freedom
#      | screenShotName | Reg_Wealth_010_Android_03 |
#    And Modify the goalName to verify whether the display is consistent
#      | goalName       | achieveGoal               |
#      | screenShotName | Reg_Wealth_010_Android_04 |
#
#  @OrderPlace11
#  Scenario:  Build My Investing Habit edit name
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_011_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_011_Android_02 |
#    And I click Add New Goals Button
#    And I click Build My Investing Habit
#      | screenShotName | Reg_Wealth_011_Android_03 |
#    And Modify the goalName to verify whether the display is consistent
#      | goalName       | buildInvesting            |
#      | screenShotName | Reg_Wealth_011_Android_04 |
#
#  @OrderPlace_12
#  Scenario: Habit  OrderPlace page Two2 ”Insufficient account balance“
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_026_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_008_Android_02 |
#    And I click Add New Goals Button
#    And I click Build My Investing Habit
#      | screenShotName | Reg_Wealth_007_Android_01 |
#    And I setup Build One time investment today
#      | oneTime        | 3000000001                |
#      | screenShotName | Reg_Wealth_008_Android_02 |
#    Then I can see Exceed maximum amount
#
##wrong flow finally not exit "Not now"
##  @OrderPlace_13
##  Scenario Outline: Set My Target goal  through picture based on investing habit, and finally exit with "Not now"
##    And I goto wealth main page
##    Then  I'm on wealth home page in 6s
##    And I scroll Up to find the button
##    When I click Add New Goals
##    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
##    And Arrival order confirmation page
##    Examples:
##      | oneTimeValue | monthlyValue | month |
##      | 30000        | 4000         | 288   |
#
##  worong password need special account
##  @OrderPlace_14
##  Scenario Outline:Reg_Wealth_030 create Achieve Financial Freedom with the account only sell (need special account sta00237:Aa123321 )
##    Then I goto wealthPage
##      |screenShotName      |  Reg_Wealth_030_Android_01  |
##    And I click Add New Goals link
##      |screenShotName01    |  Reg_Wealth_030_Android_02  |
##    And I choose Achieve Financial Freedom
##      |screenShotName02    |  Reg_Wealth_030_Android_03  |
##    And I slide to <age>
##      |screenShotName03    |  Reg_Wealth_030_Android_04  |
##      |screenShotName04    |  Reg_Wealth_030_Android_05  |
##    And I click Continue button
##      |screenShotName05    |  Reg_Wealth_030_Android_06  |
##    And I input 10000 in Monthly expenses text
##      |screenShotName06    |  Reg_Wealth_030_Android_07  |
##      |screenShotName07    |  Reg_Wealth_030_Android_08  |
##    And I click Calculate Target Wealth button
##      |screenShotName08    |  Reg_Wealth_030_Android_09  |
##    And I click View recommendation and save my goal button
##      |screenShotName09    |  Reg_Wealth_030_Android_10  |
##    And I click View Portfolio button on Portfolio Recommendation Page
##      |screenShotName10    |  Reg_Wealth_030_Android_11  |
##    And I click Next button on Scenario Analysis Page
##      |screenShotName11    |  Reg_Wealth_030_Android_12  |
##    And I click Next button on Portfolio Page
##      |screenShotName12    |  Reg_Wealth_030_Android_13  |
##    And I click Next button on Order Page
##      |screenShotName13    |  Reg_Wealth_030_Android_14  |
##    And I Review and confirm Order
##      |screenShotName14    |  Reg_Wealth_030_Android_15  |
##      |screenShotName15    |  Reg_Wealth_030_Android_16  |
##    And I slide to confirm order in order confirm Page
##      |screenShotName14    |  Reg_Wealth_030_Android_15  |
##      |screenShotName15    |  Reg_Wealth_030_Android_16  |
##      |screenShotName16    |  Reg_Wealth_030_Android_17  |
##    And I verify the account only sell
##      |screenShotName16    |  Reg_Wealth_030_Android_17  |
##    Examples:
##      | age |
##      | 58  |
#
#  @OrderPlace_15
#  Scenario Outline:Achieve Financial Freedom Successful order process
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_015_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_015_Android_02 |
#    And I click Add New Goals Button
#    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
#    And I click View Portfolio button on Portfolio Recommendation Page
#      | screenShotName | Reg_Wealth_015_Android_03 |
#    And I click Next Button four times
#      | screenShotName   | Reg_Wealth_015_Android_04 |
#      | screenShotName01 | Reg_Wealth_015_Android_05 |
#      | screenShotName02 | Reg_Wealth_015_Android_06 |
#      | screenShotName03 | Reg_Wealth_015_Android_07 |
#      | screenShotName04 | Reg_Wealth_015_Android_08 |
#    And I slide to confirm order
#      | screenShotName01 | Reg_Wealth_015_Android_09 |
#      | screenShotName02 | Reg_Wealth_015_Android_10 |
#    Examples:
#      | age | monthlyExpenses | otherExpenses        |
#      | 61  | 20000           |    ,,                |