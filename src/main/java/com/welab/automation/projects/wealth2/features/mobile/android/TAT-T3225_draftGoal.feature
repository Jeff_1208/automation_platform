#Feature: Wealth Application
#
#  Background: Login
#    Given Open WeLab App
#    And Enable skip Root
#    When Login with user and password from properties
##    And Login with user and password
##      | user     | swmcust00171 |
##      | password | Aa123321 |
#
#
#  Scenario: Reg_Wealth_001 draft Goal edit name
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_001_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | BulidA                   |
#      | screenShotName | Reg_Wealth_001_Android_03 |
#    And I click Continue button
#      | screenShotName | Reg_Wealth_001_Android_04 |
#    Then I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_001_Android_05 |
#    Then portfolio recommendation page back to wealth page
#      | screenShotName   | Reg_Wealth_001_Android_06 |
#      | screenShotName01 | Reg_Wealth_001_Android_07 |
#      | screenShotName02 | Reg_Wealth_001_Android_08 |
#      | screenShotName03 | Reg_Wealth_001_Android_09 |
#    And I click draft
#      | screenShotName   | Reg_Wealth_001_Android_10 |
#      | screenShotName01 | Reg_Wealth_001_Android_11 |
#    When I click edit button and update goalName
#      | goalName       | bruce1                    |
#      | screenShotName | Reg_Wealth_001_Android_12 |
#    And I click back icon to Wealth page
#      | screenShotName | Reg_Wealth_001_Android_13 |
#    And I verify goalName change success
#      | screenShotName | Reg_Wealth_001_Android_14 |
#
#
#  Scenario: Reg_Wealth_002 draft Goal Confirm to modify box information
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_002_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_002_Android_02 |
#    And I click draft
#      | screenShotName   | Reg_Wealth_002_Android_03 |
#      | screenShotName01 | Reg_Wealth_002_Android_04 |
#    And I click Edit button
#      | screenShotName | Reg_Wealth_002_Android_05 |
#    Then I click View recommendation and save my goal button
##    And I click View recommendation & save my goal button
#      | screenShotName | Reg_Wealth_002_Android_06 |
#    And I verify Portfolio Recommendation page normal
#
#
#  Scenario: Reg_Wealth_003 draft Goal edit name
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_003_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_003_Android_02 |
#    And I click draft
#      | screenShotName   | Reg_Wealth_003_Android_03 |
#      | screenShotName01 | Reg_Wealth_003_Android_04 |
#    When I click edit button and update goalName
#      | goalName       | bruce1                    |
#      | screenShotName | Reg_Wealth_003_Android_05 |
#    And I click Edit button
#      | screenShotName | Reg_Wealth_003_Android_06 |
#    And I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_003_Android_07 |
#    And I click View Portfolio button
#      | screenShotName | Reg_Wealth_003_Android_08 |
#    And I click Next button on Scenario Analysis page
#      | screenShotName | Reg_Wealth_003_Android_09 |
#    And I click Next button on Portfolio page
#      | screenShotName | Reg_Wealth_003_Android_10 |
#    And I click Next button on Order page
#      | screenShotName | Reg_Wealth_003_Android_11 |
#    And I Review and confirm order
#      | screenShotName   | Reg_Wealth_003_Android_12 |
#      | screenShotName01 | Reg_Wealth_003_Android_13 |
#    And I slide to confirm order in order confirm page
#      | screenShotName | Reg_Wealth_003_Android_14 |
#
#
#  Scenario: Reg_Wealth_005 draft delete
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_005_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_005_Android_02 |
#    And I click Add New Goals Button
#    And I click Reach My Target button
#    And I edit Reach Goal Name
#      | goalName       | ReachAB                   |
#      | screenShotName | Reg_Wealth_005_Android_03 |
#    And I click Continue button
#      | screenShotName | Reg_Wealth_005_Android_04 |
#    Then I click View recommendation and save my goal button
#      | screenShotName | Reg_Wealth_005_Android_05 |
#    Then portfolio recommendation page back to wealth page
#      | screenShotName   | Reg_Wealth_005_Android_06 |
#      | screenShotName01 | Reg_Wealth_005_Android_07 |
#      | screenShotName02 | Reg_Wealth_005_Android_08 |
#      | screenShotName03 | Reg_Wealth_005_Android_09 |
#    And I click draft
#      | screenShotName   | Reg_Wealth_005_Android_10 |
#      | screenShotName01 | Reg_Wealth_005_Android_11 |
#    And I click Delete button
#      | screenShotName   | Reg_Wealth_005_Android_12 |
#      | screenShotName01 | Reg_Wealth_005_Android_13 |
#    And I verify the goal with targetName have been delete
#      | screenShotName03 | Reg_Wealth_005_Android_14 |
#
#
#
