Feature: Update CRPQ

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    When Login with user and password from properties


  @open_acc01
  Scenario:open an account Security reminder
    Then I can see Marketing preferences Page

  @open_acc02
  Scenario:open account back working properly
    And I goto wealth main page
    And I click gotIt on wealth page
    And I click Learn More on goWealth page
    And I click Skip to open an account
    And I click next on Welcome to Wealth Management Services Page
    And I click next on Update and verify personal info page
    And I enter input MSK key
    And I click next on Verify personal information page
    And I select nonCRPQ answerCH
      | question       | 持牌人士聲明                |
      | answer         | 否                        |
    And verify I'm on particular page
      | title          | 資歷                       |
    And I select nonCRPQ answerCH
      | question       | 資歷                       |
      | answer         | 大學學士或以上               |
    And verify I'm on particular page
      | title          | 投資年期                    |
    And I select nonCRPQ answerCH
      | question       | 投資年期                    |
      | answer         | 5年以上                    |
    And verify I'm on particular page
      | title          | 投資知識及經驗               |
    And I select nonCRPQ answerCH
      | question       | 投資知識及經驗               |
      | answer         | 股票，交易所買賣基金，股票基金  |
    And verify I'm on particular page
      | title          | 資產淨值                    |
    And I select nonCRPQ answerCH
      | question       | 資產淨值                    |
      | answer         | 多於 5,000,000港元          |
    Then I click back on Update and verify personal info page
    Then I can see App homepage



  @open_acc03
  Scenario: IAO full process with no license
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I enter input MSK key
    And I confirm the personal information
    And I select nonCRPQ answerCH
      | question       | 持牌人士聲明                |
      | answer         | 否                        |
    And verify I'm on particular page
      | title          | 資歷                       |
    And I select nonCRPQ answerCH
      | question       | 資歷                       |
      | answer         | 大學學士或以上               |
    And verify I'm on particular page
      | title          | 投資年期                    |
    And I select nonCRPQ answerCH
      | question       | 投資年期                    |
      | answer         | 5年以上                    |
    And verify I'm on particular page
      | title          | 投資知識及經驗               |
    And I select nonCRPQ answerCH
      | question       | 投資知識及經驗               |
      | answer         | 股票，交易所買賣基金，股票基金  |
    And verify I'm on particular page
      | title          | 資產淨值                    |
    And I select nonCRPQ answerCH
      | question       | 資產淨值                    |
      | answer         | 多於 5,000,000港元          |
    And I confirm the Investment account profile
    And I click next button
    Then I click confirm on update page
    And I click next button
    And I click next button
    And Starting CRPQ
    And I Select Answer for 想請問一下你的投資目標
      | 傾向保護資本的投資以賺取相當於存款利率的回報 |
    And I Select Answer for 你點睇自己追求回報及承受風險的程度
      | 承受較高風險以令回報最大化 |
    And I Select Answer for 我可以接受到嘅投資價格波動為
      | 超40%的向上或下波幅 |
    And I Select Answer for 你嘅總流動資產入面
      | 多於50% |
    And I Select Answer for 就你嘅財政需要，你喺未來12個月內最多會賣出
      | 最多75% |
    And I Select Answer for 如果你嘅投資價格喺6個月內跌咗50%
      | 趁個價低咗，大幅增加投資 |
    And I Select Answer for 你可以隨時調動嘅應急資產
      | 12個月或以上 |
    And I verify Customer Risk Rating
      | risk | 2 |
    And I click next button
    Then I finish account opening process



  @open_acc04
  Scenario: IAO full process with license
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I enter input MSK key
    And I confirm the personal information
    And I select nonCRPQ answerCH
      | question       | 持牌人士聲明                |
      | answer         | 是                        |
    And verify I'm on particular page
      | title          | 資歷                       |
    And I select nonCRPQ answerCH
      | question       | 資歷                       |
      | answer         | 大學學士或以上               |
    And verify I'm on particular page
      | title          | 投資年期                    |
    And I select nonCRPQ answerCH
      | question       | 投資年期                    |
      | answer         | 5年以上                    |
    And verify I'm on particular page
      | title          | 投資知識及經驗               |
    And I select nonCRPQ answerCH
      | question       | 投資知識及經驗               |
      | answer         | 股票，交易所買賣基金，股票基金  |
    And verify I'm on particular page
      | title          | 資產淨值                    |
    And I select nonCRPQ answerCH
      | question       | 資產淨值                    |
      | answer         | 多於 5,000,000港元          |
    And I confirm the Investment account profile
    And I can see the upload files page
    And I take photo to upload
    Then I finish upload document
    Then I click next on update page with license
    And Starting CRPQ
    And I Select Answer for 想請問一下你的投資目標
      | 傾向保護資本的投資以賺取相當於存款利率的回報 |
    And I Select Answer for 你點睇自己追求回報及承受風險的程度
      | 承受較高風險以令回報最大化 |
    And I Select Answer for 我可以接受到嘅投資價格波動為
      | 超40%的向上或下波幅 |
    And I Select Answer for 你嘅總流動資產入面
      | 多於50% |
    And I Select Answer for 就你嘅財政需要，你喺未來12個月內最多會賣出
      | 最多75% |
    And I Select Answer for 如果你嘅投資價格喺6個月內跌咗50%
      | 趁個價低咗，大幅增加投資 |
    And I Select Answer for 你可以隨時調動嘅應急資產
      | 12個月或以上 |
    And I verify Customer Risk Rating
      | risk | 2 |
    And I click next button
    Then I finish account opening process


