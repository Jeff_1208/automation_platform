package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.projects.wealth2.WelabAPITest.VerifyUiAndApiGoalSetting;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class GoalSettingOrderPage extends GoalSettingPage {
  private String pageName = "Goal Setting Portfolio Page";
  private int currentAge;
  private int currentWealthTextInt;
  private Integer getMinMonlyValueLess;
  private Integer getMinMonlyValue;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name,'One-time')]")
  private MobileElement enterBalance;

  @AndroidFindBy(accessibility = "GOAL_PORTFOLIO_DETAIL-confirmModal-switchCurrency")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GOAL_PORTFOLIO_DETAIL-confirmModal-switchCurrency'")
  private MobileElement switchCurrency;

  @AndroidFindBy(xpath = "//*[@text='Pay in HKD']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Pay in HKD'")
  private MobileElement payInHKD;

  @AndroidFindBy(xpath = "//*[@text='Pay in USD']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Pay in USD'")
  private MobileElement payInUSD;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Review and confirm order']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review and confirm order'")
  private MobileElement reviewAndConfirmOrder;

  @AndroidFindBy(xpath = "//*[@text='Investment mode']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Investment mode'")
  private MobileElement investMode;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Not now']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Not now'")
  private MobileElement notNow;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Quit and save']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Quit and save'")
  private MobileElement quit;

  @AndroidFindBy(xpath = "//*[@text='Prospectus']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Prospectus'")
  private MobileElement prospectusTxt;

  @AndroidFindBy(xpath = "(//*[@text='Prospectus'])[2]")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Prospectus'])[2]")
  private MobileElement prospectusTxt2;

  @AndroidFindBy(xpath = "//*[@text='Factsheet']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Factsheet'")
  private MobileElement factSheetTxt;

  @AndroidFindBy(xpath = "//*[@text='Product Key Facts']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Product Key Facts'")
  private MobileElement productKeyFactsTxt;

  @AndroidFindBy(xpath = "//*[@text='Annual Report']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Annual Report'")
  private MobileElement annualReportTxt;

  @AndroidFindBy(xpath = "//*[@text='Semi Annual Report']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Semi Annual Report'")
  private MobileElement semiAnnualReportTxt;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.widget.TextView")
  private MobileElement pdfPageNo;

  @AndroidFindBy(
      xpath = "(//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup)[1]")
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        iOSNsPredicate = "label == \"FEATUREDFUNDS_HOME_STACK, back\" AND name == \"header-back\""),
    @iOSXCUITBy(accessibility = "btnBack")
  })
  private MobileElement backBtn;

  @AndroidFindBy(xpath = "//*[@text='Slide to Confirm']/..")
  @iOSXCUITFindBy(xpath = "//*[@name='Slide to Confirm']")
  private MobileElement slideToConfirm;

  @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]")
  private MobileElement slidetoconfirm;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/preceding-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(
      xpath =
          "//*[@name='Slide to Confirm']/preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther")
  private MobileElement radioBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]")
  @iOSXCUITFindBy(
      xpath =
          "//*[@name='Slide to Confirm']/preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther")
  private MobileElement radioBtn2;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Back']")
  @iOSXCUITFindBy(iOSNsPredicate = "name IN {'Back', 'Back to Home'}")
  private MobileElement back;

  @AndroidFindBy(xpath = "//*[starts-with(@text,'Back')]/../../android.widget.TextView[1]")
  @iOSXCUITFindBy(
      xpath =
          "//*[starts-with(@name,'Back')]/../../../../preceding-sibling::XCUIElementTypeScrollView//XCUIElementTypeStaticText[1]")
  private MobileElement errorMsg;

  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Subscription fee\"])[2]/../..")
  private MobileElement subscriptionFee;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Starting from\"]/../..")
  private MobileElement startingFrom;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Congratulations!']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Congratulations!'")
  private MobileElement congratulation;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Done']")
  @iOSXCUITFindBy(xpath = "//*[@name='-title']/..")
  private MobileElement done;

  @AndroidFindBy(
      xpath = "//*[@text='Congratulations!']/following-sibling::android.widget.TextView[1]")
  @iOSXCUITFindBy(
      xpath = "//*[@name='Congratulations!']/following-sibling::XCUIElementTypeStaticText[1]")
  private MobileElement referenceNumber;

  @AndroidFindBy(xpath = "//*[@text='Fund order has been processed']")
  @iOSXCUITFindBy(xpath = "//*[@name='Fund order has been processed']")
  private MobileElement processedTxt;

  @iOSXCUITFindBy(iOSNsPredicate = "label == 'Fund name'")
  @AndroidFindBy(xpath = "//*[@text = 'Fund name']")
  private MobileElement fundNameTitle;

  @iOSXCUITFindBy(
      iOSNsPredicate =
          "label == \"View updated recommendation\" AND name == \"GOAL_SETTING_EDIT-btn-done\"")
  @AndroidFindBy(accessibility = "GOAL_SETTING_EDIT-btn-done")
  private MobileElement viewUpRecommen;

  @iOSXCUITFindBy(xpath = "//*[@name='Keep existing one']")
  @AndroidFindBy(xpath = "(//*[@content-desc=\"-title\"])[2]")
  private MobileElement Keepexistingone;

  @AndroidFindBy(accessibility = "MY_GOAL_DETAIL-btn-edit")
  @iOSXCUITFindBy(iOSNsPredicate = "value == \"Edit\"")
  private MobileElement editButton;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeOther[@name=\"HKD\"]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
  private MobileElement upMonthly;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'ReachA')]")
  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Reach')]/../android.widget.TextView[1]")
  private MobileElement reachMyTargetlink;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Achieve')]/../android.widget.TextView[1]")
  private MobileElement achievementlink;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Build')]/../android.widget.TextView[1]")
  private MobileElement buildlink;

  @iOSXCUITFindBy(xpath = "//*[@name='View New Portfolio']")
  @AndroidFindBy(xpath = "//*[@text='View New Portfolio']")
  private MobileElement viewNewPortlio;

  @iOSXCUITFindBy(xpath = "//*[@name='Portfolio']")
  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Portfolio']")
  private MobileElement portfolioTab;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'Review and confirm order')]")
  @AndroidFindBy(xpath = "//android.view.View[@text=\"Review and confirm order\"]")
  private MobileElement confirmViewTitle;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'Goal Selection')]")
  @AndroidFindBy(xpath = "//*[@text=\"Goal Selection\"]")
  private MobileElement goalSelectionLink;

  @Getter
  @iOSXCUITFindBy(xpath = "//*[@name='New Portfolio']")
  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[2]")
  private MobileElement newPortfolioLink;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'Reach')]/../android.view.ViewGroup[2]/android.widget.TextView")
  private MobileElement getReachStatus;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'Build')]/../android.view.ViewGroup[2]/android.widget.TextView")
  private MobileElement getBuildStatus;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'Achieve')]/../android.view.ViewGroup[2]/android.widget.TextView")
  private MobileElement getAchieveStatus;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[1]")
  private MobileElement goalNameLink;

  @AndroidFindBy(
      xpath =
          "(//android.widget.ScrollView//android.widget.ImageView)[3]/../android.widget.TextView")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage/../..//XCUIElementTypeStaticText")
  protected MobileElement investTimeSlider;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.TextView")
  private MobileElement buildInvestTimeSlider;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='MY_GOAL_DETAIL-goalName']")
  private MobileElement editGoalNameText;

  @iOSXCUITFindBy(xpath = "//*[@name='Keep existing one']")
  @AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"-title\"])[2]")
  private MobileElement keepButton;

  @iOSXCUITFindBy(xpath = "//*[@name='Confirm']")
  @AndroidFindBy(xpath = "//android.widget.TextView[@text =\"Confirm\"]")
  private MobileElement confirmAlertButton;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup")
  private MobileElement OKButton;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup[2]")
  //  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Confirm\"")
  private MobileElement YesButton;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Sell')]")
  @AndroidFindAll({
         @AndroidBy(xpath = "//*[@text='Sell']"),
  })
  private MobileElement Sellbutton;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Proceed')]")
  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Proceed']"),
          @AndroidBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup[2]/../android.view.ViewGroup[1]")
  })
  private MobileElement ProceedBtn;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Sell all')]")
  @AndroidFindBy(accessibility = "SELL_GOAL-btn-sell-all")
  private MobileElement SellAllBtn;

  @iOSXCUITFindBy(
      xpath = "//*[starts-with(@name, 'Cancel')]/../..//*[starts-with(@name, 'Sell all')]")
  @AndroidFindBy(xpath = "//*[starts-with(@text, 'Cancel')]/../../android.view.ViewGroup[1]/android.widget.TextView[1]")
  private MobileElement SellAllConfirmBtn;

  @AndroidFindBy(xpath = "//android.widget.EditText")
  private MobileElement SellInputEdit;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Next')]")
  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text=''Next]"),
          @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"SELL_GOAL-btn-next\"]")
  })
  private MobileElement SellNextBtn;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Next\" AND name == \"-title\"")
  private MobileElement confirmNextBtn;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@content-desc=\"SELL_GOAL_CONFIRM-btn-checkbox\"]/android.view.ViewGroup")
  private MobileElement SellradioBtn;

  @AndroidFindBy(xpath = "//*[@text='Processing']")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Processing\"])[3]")
  private MobileElement processingStatus;

  @AndroidFindBy(xpath = "//*[@text='On track']")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"On track\"])[3]")
  private MobileElement onTrackStatus;

  @AndroidFindBy(xpath = "//*[@text='Off track']")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Off track\"])[3]")
  private MobileElement offTrackStatus;

  @AndroidFindBy(xpath = "//*[@text='Achieved']")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Achieved\"])[3]")
  private MobileElement achievedStatus;

  @AndroidFindBy(xpath = "//*[@text='Completed']")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Completed\"])[3]")
  private MobileElement completedStatus;

  @AndroidFindBy(xpath = "//*[@text=\"Order Status\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "value == \"Order Status\"")
  private MobileElement orderStatusLink;

  @Getter
  @AndroidFindBy(xpath = "//android.view.View[@text= \"Order status\"]")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Order status\"])[6]")
  private MobileElement orderStatusPage;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[2]")
  private MobileElement timeListOne;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]")
  private MobileElement timeListTwo;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[9]")
  private MobileElement isHaveLine;

  @AndroidFindBy(xpath = "//*[@text=\"Orders are sorted from newest to oldest.\"]")
  private MobileElement newestToOldest;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView")
  private List<MobileElement> otherFieldList;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]")
  private MobileElement anyFundLink;

  @AndroidFindBy(xpath = "//*[@text='Prospectus']")
  private MobileElement prospectusLink;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup")
  private MobileElement pdfDownloadLink;

  @Getter
  @AndroidFindBy(xpath = "//*[@text= \"Download Completed\"]")
  private MobileElement downloadSuccess;

  @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.Button[1]")
  private MobileElement allowButton;

  @AndroidFindBy(xpath = "//*[@text=\"Standing Instruction\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Standing Instruction\"")
  private MobileElement standInstructionTab;

  @Getter
  @AndroidFindBy(accessibility = "-desc")
  @iOSXCUITFindBy(accessibility = "-desc")
  private MobileElement eachMonthInstruction;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@content-desc=\"-btn\"]/android.view.ViewGroup/android.widget.TextView[1]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name=\"-desc\"]/../XCUIElementTypeOther/XCUIElementTypeStaticText")
  private MobileElement goalNameDetailsLink;

  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.TextView[1]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name=\"Product name\"]/../../../XCUIElementTypeStaticText")
  private MobileElement goalNameDetailsPageLink;

  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.TextView[3]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name=\"Product name\"]/following-sibling::XCUIElementTypeStaticText")
  private MobileElement productNameLink;

  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.TextView[11]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name=\"Reference number\"]/following-sibling::XCUIElementTypeStaticText")
  private MobileElement referenceNumLink;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement reeditLink;

  @AndroidFindBy(accessibility = "MY_GOAL_DETAIL-WealthHKD")
  private MobileElement myCurrentWealth;

  @AndroidFindBy(accessibility = "totalMonthly")
  private MobileElement totalMonthly;

  @AndroidFindBy(
      xpath = "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement retiredAge;

  @AndroidFindBy(accessibility = "GOAL_SETTING_EDIT-estIncomeStreamPerMonth")
  private MobileElement editPageTotalMonthly;

  @AndroidFindBy(
      xpath = "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[2]")
  private MobileElement currentWealth;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[4]")
  private MobileElement minMonlyValueLink;

  @AndroidFindBy(accessibility = "GOAL_SETTING_EDIT-message-monthlySavingLowerThanMin")
  private MobileElement monthlySavingLowerThanMin;

  @AndroidFindBy(accessibility = "GOAL_SETTING_EDIT-message-monthlySavingHigherThanMax")
  private MobileElement monthlySavingHigherThanMax;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[2]")
  private MobileElement slideToMax;

  @AndroidFindBy(xpath = "//*[@text='Target wealth']")
  private MobileElement targetWealth;

  @AndroidFindBy(xpath = "//*[@text='Probability of reaching goal']")
  private MobileElement probability;

  @AndroidFindBy(xpath = "//*[@text='Goal horizon']")
  private MobileElement goalHorizon;

  @AndroidFindBy(xpath = "//*[@text='Updating...']")
  private MobileElement updatingStatus;

  @Getter
  @AndroidFindBy(xpath = "//android.view.ViewGroup[3]/android.widget.TextView")
  private MobileElement availableBalance;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[3]")
  private MobileElement PortfolioNameText;

  @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[4]")
  private MobileElement minMonthlyInvestment;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Achieve Financial Freedom']"),
  })
  private MobileElement financialFreedomLink;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup"),
  })
  private MobileElement modifyButton;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText"),
  })
  private MobileElement inputButton;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"]"),
  })
  private MobileElement viewPortfolio;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"GOAL_PORTFOLIO_DETAIL-btn-buy-title\"]"),
  })
  private MobileElement analysisNext;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"GOAL_PORTFOLIO_DETAIL-confirmModal-btn-next-title\"]"),
  })
  private MobileElement oedrtNext;


  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"]"),
  })
  private MobileElement confirmOrderNext;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]"),
  })
  private MobileElement slide;
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup"),
  })
  private MobileElement dot;
  @AndroidFindAll({
          @AndroidBy(
                  xpath =
                          "//*[@text='One-time investment today']/following-sibling::android.widget.EditText[1]"),
          @AndroidBy(
                  xpath =
                          "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(
                  xpath =
                          "//*[@name='One-time investment today']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField"),
          @iOSXCUITBy(
                  xpath =
                          "//*[@name='One time investment today']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  })
  private MobileElement oneTimeInvestmentInput;

  @AndroidFindAll({
          @AndroidBy(
                  xpath =
                          "//*[@text='One-time investment today']/following-sibling::android.view.ViewGroup[1]"),
          @AndroidBy(
                  xpath =
                          "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup[1]//android.widget.ImageView")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(
                  xpath =
                          "//*[@name='One-time investment today']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]"),
          @iOSXCUITBy(
                  xpath =
                          "//*[@name='One time investment today']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
  })
  private MobileElement oneTimeInvestmentEditBtn;


  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='View recommendation & save my goal']"),
          @AndroidBy(xpath = "//*[@text='View recommendation']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//*[@name='View recommendation & save my goal']/XCUIElementTypeOther[1]"),
          @iOSXCUITBy(xpath = "//*[@name='View recommendation']/XCUIElementTypeOther[1]")
  })
  private MobileElement saveGoalBtn;
  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Build My Investing Habit']"),
          @AndroidBy(xpath = "//*[@text='建立我的投資習慣']")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Build My Investing Habit'")
  private MobileElement investingHabitLink;

  private String fundNameTxt = "PIMCO Asia Strategic Interest Bond Fund (USD)(Accumulation)";

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]")
  })
  private MobileElement goalNameText;


  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='ReachM']")
  })
  private MobileElement reachM;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]")
  })
  private MobileElement editBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.EditText[@content-desc='MY_GOAL_DETAIL-editName']")
  })
  private MobileElement goalNameInput;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'track')]")
  })
  private MobileElement trackGoal;

  public GoalSettingOrderPage() {
    super.pageName = pageName;
  }

  public Boolean getSubscriptionFeeStatus() {
    scrollUpToFindElement(subscriptionFee, 8, 3);
    Boolean displayFlag = false;
    if (isElementVisible(subscriptionFee, 3)) {
      displayFlag = subscriptionFee.getText().contains("equivalent to");
    }
    return displayFlag;
  }

  public Boolean getStartingFrom() {
    Boolean displayFlag = false;
    if (isElementVisible(startingFrom, 3)) {
      displayFlag = startingFrom.getText().contains("th of each month till");
    }
    return displayFlag;
  }

  public void orderPlacement() {
    waitUntilElementVisible(buyButton);
    clickElement(buyButton);
    clickElement(nextButton);
    waitUntilElementVisible(reviewAndConfirmOrder);
    waitUntilElementVisible(investMode);
    scrollUpToFindElement(nextButton, 8, 3);
    clickElement(nextButton);
  }

  public String getFundDoc(String docName) throws InterruptedException {
    if (docName.equalsIgnoreCase("prospectus")) {
      scrollUpToFindElement(prospectusTxt, 3, 3);
      clickElement(prospectusTxt);
    } else if (docName.equalsIgnoreCase("Factsheet")) {
      scrollUpToFindElement(factSheetTxt, 3, 3);
      clickElement(factSheetTxt);
    } else if (docName.equalsIgnoreCase("Product Key Facts")) {
      scrollUpToFindElement(productKeyFactsTxt, 3, 3);
      clickElement(productKeyFactsTxt);
    } else if (docName.equalsIgnoreCase("Annual Report")) {
      scrollUpToFindElement(annualReportTxt, 3, 3);
      clickElement(annualReportTxt);
    } else if (docName.equalsIgnoreCase("Semi Annual Report")) {
      scrollUpToFindElement(semiAnnualReportTxt, 3, 3);
      clickElement(semiAnnualReportTxt);
    }
    Thread.sleep(10000);
    waitUntilElementVisible(pdfPageNo);
    String pageNo = pdfPageNo.getText();
    logger.info(docName + " Fund PDF page number: " + pageNo);
    clickElement(backBtn);
    return pageNo;
  }

  public String getFundDoc2(String docName) throws InterruptedException {
    if (docName.equalsIgnoreCase("prospectus")) {
      scrollUpToFindElement(prospectusTxt, 3, 3);
      clickElement(prospectusTxt);
      // if reviewAndConfirmOrder still exist, then the Doc is not loaded.
      if (verifyElementExist(reviewAndConfirmOrder)) return "Doc is not loaded";
    } else if (docName.equalsIgnoreCase("Factsheet")) {
      scrollUpToFindElement(factSheetTxt, 3, 3);
      clickElement(factSheetTxt);
    } else if (docName.equalsIgnoreCase("Product Key Facts")) {
      scrollUpToFindElement(productKeyFactsTxt, 3, 3);
      clickElement(productKeyFactsTxt);
    } else if (docName.equalsIgnoreCase("Annual Report")) {
      scrollUpToFindElement(annualReportTxt, 3, 3);
      clickElement(annualReportTxt);
    } else if (docName.equalsIgnoreCase("Semi Annual Report")) {
      scrollUpToFindElement(semiAnnualReportTxt, 3, 3);
      clickElement(semiAnnualReportTxt);
    }
    Thread.sleep(10000);
    takeScreenshot(docName);
    clickElement(backBtn);
    return "";
  }

  public void selectCurrency(String currency) {
    String currentCurrency = "HKD";
    try {
      payInUSD.isDisplayed();
      currentCurrency = "USD";
    } catch (NoSuchElementException ex) {
      logger.info("");
    } finally {
      if (!currency.equals(currentCurrency)) switchCurrency.click();
    }
  }

  @SneakyThrows
  public void slideToConfirmOrder() {
    scrollUpToFindElement(slidetoconfirm, 8, 3);
    radioBtn2.click();
    Thread.sleep(1000);
    // top-left
    int x = slidetoconfirm.getLocation().x;
    int y = slidetoconfirm.getLocation().y;
    // size
    int width = slidetoconfirm.getRect().width;
    int x_start = x + 50;
    int x_end = x + width;
    int y_co = y + 2;
    TouchAction action =
        new TouchAction(driver)
            .longPress(PointOption.point(x_start, y_co))
            .moveTo(PointOption.point(x_end, y_co))
            .release();
    action.perform();
    Thread.sleep(1000 * 10);
  }

  public String getErrorMsg() {
    String msg = errorMsg.getText();
    return msg;
  }

  @SneakyThrows
  public void clickNext() {
    scrollUpToFindElement(nextButton, 8, 3);
    // sometimes click nextButton no effect, wait 500 ms.
    Thread.sleep(500);
    clickElement(nextButton);
    waitUntilElementVisible(fundNameTitle);
  }

  public String getReferenceNumber() {
    String number = referenceNumber.getText().split(":")[1].trim();
    logger.info("Reference number: " + number);
    return number;
  }

  public void clickUpRecommen() {
    waitUntilElementClickable(viewUpRecommen);
    clickElement(viewUpRecommen);
  }

  public void slideToclickKeepone() {
    swipeRightToLeft();
    waitUntilElementClickable(Keepexistingone);
    clickElement(Keepexistingone);
  }

  public void clickEditButton() {
    waitUntilElementClickable(editButton);
    clickElement(editButton);
  }

  public void updateMonthlyInvest(String MonthlyValue) {
    clearAndSendKeys(upMonthly, MonthlyValue);
    inputEnter(upMonthly);
  }

  public void clickReachGoal(String Name) {
//    waitUntilElementVisible(goalSelectionLink, 30);
    if (verifyElementExist(reachMyTargetlink)) {
      if (getReachStatus.getText().equals("On track")
          || getReachStatus.getText().equals("Off track")) {
        getReachStatus.click();
      }
    } else {
      scrollUpToFindElement(reachMyTargetlink, 30, 5);
      if (reachMyTargetlink.getText().equals(Name)) {
        if (getReachStatus.getText().equals("On track")
            || getReachStatus.getText().equals("Off track")) {
          getReachStatus.click();
        }
      }
    }
  }

  public void clickAchieveGoal(String Name) {
//    waitUntilElementVisible(goalSelectionLink, 30);
    if (verifyElementExist(achievementlink)) {
      if (getAchieveStatus.getText().equals("On track")
          || getAchieveStatus.getText().equals("Off track")) {
        getAchieveStatus.click();
      }
    } else {
      scrollUpToFindElement(achievementlink, 30, 5);
      if (achievementlink.getText().equals(Name)) {
        if (getAchieveStatus.getText().equals("On track")
            || getAchieveStatus.getText().equals("Off track")) {
          getAchieveStatus.click();
        }
      }
    }
  }

  public void clickBuild(String Name) {
//    waitUntilElementVisible(goalSelectionLink, 30);
    if (verifyElementExist(buildlink)) {
      if (getBuildStatus.getText().equals("On track")
          || getBuildStatus.getText().equals("Off track")) {
        getBuildStatus.click();
      }
    } else {
      scrollUpToFindElement(buildlink, 30, 5);
      if (buildlink.getText().equals(Name)) {
        if (getBuildStatus.getText().equals("On track")
            || getBuildStatus.getText().equals("Off track")) {
          getBuildStatus.click();
        }
      }
    }
  }

  public void setMyInvestingHabitGoal(String time) throws Exception {
    int index = convertInvestTime2Index(time);
    MobileElement targetTimeElement = getBIMTimeList().get(index);
    setAge(buildInvestTimeSlider, targetTimeElement);
  }

  @SneakyThrows
  public void viewNewPortfolio() {
    waitUntilElementClickable(viewNewPortlio);
    assertThat(getNewPortfolioLink().getText()).isEqualTo("New Portfolio");
    Thread.sleep(2 * 1000);
    clickElement(viewNewPortlio);
  }

  public void clickViewNewPortlioAndBuy() {
    viewNewPortfolio();
    waitUntilElementClickable(portfolioTab);
    clickElement(buyButton);
  }

  public void clickViewPortlio() {
    viewPortfolioOverview();
  }

  public void clickBuyBtn() {
    clickElement(buyButton);
  }

  public void reviewNewOrder() {
    if (isElementVisible(confirmViewTitle)) {
      scrollUpToFindElement(nextButton, 8, 3);
    }
  }

  public void clickKeepExistingOneAndConfirm() {
    waitUntilElementClickable(viewNewPortlio);
    swipeRightToLeft();
    assertThat(keepButton.getText()).isEqualTo("Keep existing one");
    clickElement(keepButton);
    clickElement(confirmAlertButton);
  }

  @SneakyThrows
  public void clickConfirmAndSlideToConfirm() {
    //waitUntilElementClickable(confirmAlertButton);
    // clickElementCenter(confirmAlertButton);
    scrollUpToFindElement(nextButton, 8, 3);
    clickElement(nextButton);
    scrollUpToFindElement(slideToConfirm, 8, 3);
    radioBtn2.click();
    Thread.sleep(1000);
    // top-left
    int x = slideToConfirm.getLocation().x;
    int y = slideToConfirm.getLocation().y;
    // size
    int width = slideToConfirm.getRect().width;
    int x_start = x + 50;
    int x_end = x + width;
    int y_co = y + 2;
    TouchAction action =
        new TouchAction(driver)
            .longPress(PointOption.point(x_start, y_co))
            .moveTo(PointOption.point(x_end, y_co))
            .release();
    action.perform();
    Thread.sleep(1000 * 10);
  }

  public void waitAndReviewGoalName(String targeName) {
    waitUntilElementVisible(editGoalNameText);
    assertThat(editGoalNameText.getText()).isEqualTo(targeName);
  }

  public void clickOKButton() {
    waitUntilElementClickable(OKButton);
    clickElement(OKButton);
  }

  @SneakyThrows
  public void clickYESButton() {
    // sometimes YesButton will pop, sometime will not.
    Thread.sleep(2 * 1000);
    if (verifyElementExist(YesButton)) {
      clickElementCenter(YesButton);
    }
  }

  @SneakyThrows
  public void clickSellButton(String screenShotName) {
    Thread.sleep(2000);
    waitUntilElementVisible(Sellbutton);
    takeScreenshot(screenShotName);
    clickElement(Sellbutton);
    Thread.sleep(2000);
  }

  @SneakyThrows
  public void clickProceedButton(String screenShotName) {
    Thread.sleep(2000);
    waitUntilElementVisible(ProceedBtn);
    takeScreenshot(screenShotName);
    clickElement(ProceedBtn);
  }

  public void editSellValue(String SellValue,String screenShotName) {
    waitUntilElementClickable(SellInputEdit);
    clickElement(SellInputEdit);
    clearAndSendKeys(SellInputEdit, SellValue);
    globalVarAmount(SellValue);
    inputEnter(SellInputEdit);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void SlideToConfirmSellOrder() {
    scrollUpToFindElement(slideToConfirm, 8, 3);
    SellradioBtn.click();
    Thread.sleep(1000);
    // top-left
    int x = slideToConfirm.getLocation().x;
    int y = slideToConfirm.getLocation().y;
    // size
    int width = slideToConfirm.getRect().width;
    int x_start = x + 50;
    int x_end = x + width;
    int y_co = y + 2;
    TouchAction action =
        new TouchAction(driver)
            .longPress(PointOption.point(x_start, y_co))
            .moveTo(PointOption.point(x_end, y_co))
            .release();
    action.perform();
    Thread.sleep(1000 * 10);
  }

  public void clickProcessingOrder() {
//    waitUntilElementVisible(goalSelectionLink);
    scrollUpToFindElement(processingStatus, 10, 2);
    clickElement(processingStatus);
  }

  public void clickOnTrackOrder() {
//    waitUntilElementVisible(goalSelectionLink);
    scrollUpToFindElement(onTrackStatus, 6, 2);
    clickElement(onTrackStatus);
  }

  public void clickOffTrackOrder() {
//    waitUntilElementVisible(goalSelectionLink);
    scrollUpToFindElement(offTrackStatus, 6, 2);
    clickElement(offTrackStatus);
  }

  public void clickAchievedOrder() {
//    waitUntilElementVisible(goalSelectionLink);
    scrollUpToFindElement(achievedStatus, 6, 2);
    clickElement(achievedStatus);
  }

  public void clickCompletedOrder() {
//    waitUntilElementVisible(goalSelectionLink);
    scrollUpToFindElement(completedStatus, 6, 2);
    clickElement(completedStatus);
  }

  public void clickOrderStatus() {
    waitUntilElementVisible(orderStatusLink);
    clickElement(orderStatusLink);
  }

  @SneakyThrows
  public boolean verifyGoalTime() {
    Thread.sleep(5 * 1000);
    List<String> timeList = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      if (i == 0) {
        MobileElement timeList1 = timeListOne;
        timeList.add(timeList1.getText());
        if (!verifyElementExist(isHaveLine)) {
          break;
        }
      } else {
        MobileElement timeList1 = timeListTwo;
        timeList.add(timeList1.getText());
      }
      scrollUp();
      scrollUp();
    }
    LinkedHashSet<String> hashSet = new LinkedHashSet<>(timeList);
    List<String> timeListNoDistinct = new ArrayList<>(hashSet);
    for (int i = 0; i < timeListNoDistinct.size() - 1; i++) {
      for (int j = 1 + i; j < timeListNoDistinct.size(); j++) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
        Date date2 = sdf.parse(timeListNoDistinct.get(i));
        Date date3 = sdf.parse(timeListNoDistinct.get(j));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(simpleDateFormat.format(date2), dft);
        LocalDate localDate2 = LocalDate.parse(simpleDateFormat.format(date3), dft);
        if (!localDate.isAfter(localDate2)) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean verifyMoPoStatus() {
    scrollDownToFindElement(newestToOldest, 6, 2);
    scrollDown();
    List<MobileElement> orderOtherList = otherFieldList;
    for (int i = 0; i < 3; i++) {
      if (i == 0) {
        for (int j = 4; j < orderOtherList.size(); j++) {
          if (j == 4) {
            String modelPortlioName =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            if (modelPortlioName.contains("MP")) continue;
            else return false;
          }
          if (j == 5) {
            String orderStatus =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            if (orderStatus.equals("Processing")
                || orderStatus.equals("Settled")
                || orderStatus.equals("Cancelled")) continue;
            else return false;
          }
          if (j == 6) {
            String banlances =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            String compareStr = ".";
            if (strCount(compareStr, banlances)) continue;
            else return false;
          }
          if (j == 7) {
            String reNumber =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            String compareStr = "SE";
            if (strCount(compareStr, reNumber)) break;
            else return false;
          }
        }
        if (!verifyElementExist(isHaveLine)) {
          break;
        }
      } else {
        for (int j = 3; j < orderOtherList.size(); j++) {
          if (j == 3) {
            String modelPortlioName =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            if (modelPortlioName.contains("MP")) {
              continue;
            } else return false;
          }
          if (j == 4) {
            String orderStatus =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            if (orderStatus.equals("Processing")
                || orderStatus.equals("Settled")
                || orderStatus.equals("Cancelled")) continue;
            else return false;
          }
          if (j == 5) {
            String banlances =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            String compareStr = ".";
            if (strCount(compareStr, banlances)) continue;
            else return false;
          }
          if (j == 6) {
            String reNumber =
                driver
                    .findElement(
                        By.xpath(
                            "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView["
                                + j
                                + "]"))
                    .getText();
            String compareStr = "SE";
            if (strCount(compareStr, reNumber)) break;
            else return false;
          }
        }
      }
      scrollUp();
      scrollUp();
    }
    return true;
  }

  public boolean strCount(String compareStr, String number) {
    int indexStart = 0;
    int compareStrLength = compareStr.length();
    int count = 0;
    while (true) {
      int tm = number.indexOf(compareStr, indexStart);
      if (tm >= 0) {
        count++;
        indexStart = tm + compareStrLength;
      } else break;
    }
    return count == 1;
  }

  @SneakyThrows
  public void clickFundAndVerifyDocuments() {
    scrollDownToFindElement(newestToOldest, 6, 2);
    scrollDown();
    clickElement(anyFundLink);
    waitUntilElementVisible(prospectusLink);
    clickElement(prospectusLink);
    assertThat(prospectusLink.getText()).isEqualTo("Prospectus");
    waitUntilElementVisible(pdfDownloadLink);
    Thread.sleep(5 * 1000);
    clickElement(pdfDownloadLink);
    Thread.sleep(5 * 1000);
    if (verifyElementExist(allowButton)) {
      clickElement(allowButton);
    } else {
      waitUntilElementVisible(downloadSuccess);
    }
  }

  @SneakyThrows
  public boolean clickStandInstruction() {
    Thread.sleep(3 * 1000);
    waitUntilElementVisible(standInstructionTab);
    clickElement(standInstructionTab);
    if (verifyElementExist(eachMonthInstruction)) {
      return false;
    }
    return true;
  }

  @SneakyThrows
  public void clickInstructionsDetails() {
    String goalNameDetails = goalNameDetailsLink.getText();
    clickElement(goalNameDetailsLink);
    Thread.sleep(3 * 1000);
    waitUntilElementVisible(goalNameDetailsPageLink);
    String productName = productNameLink.getText();
    assertThat(goalNameDetailsPageLink.getText()).isEqualTo(goalNameDetails);
    assertThat(productName.contains("MP")).isTrue();
    String referenceNum = referenceNumLink.getText();
    assertThat(referenceEquals(referenceNum)).isTrue();
  }

  public boolean referenceEquals(String str) {
    String pattern = "[A-Z]{2}";
    String pattern2 = "[0-9]{10}";
    Pattern r = Pattern.compile(pattern);
    Pattern r1 = Pattern.compile(pattern2);
    Matcher m = r.matcher(str);
    Matcher m1 = r1.matcher(str);
    if (m.find() && m1.find()) {
      if (m.group(0).contains("SI") && m1.group(0).length() == 10) return true;
    }
    return false;
  }

  public void clickDoneBtn() {
    waitUntilElementVisible(done);
    clickElement(done);
  }

  @SneakyThrows
  public void clickBackBtn() {
    Thread.sleep(2 * 1000);
    clickElement(backBtn);
    waitUntilElementVisible(backBtn, 2);
    Thread.sleep(2 * 1000);
    clickElement(backBtn);
  }

  public boolean sellBtnUnClickable(String screenShotName) {
    if (isElementClickable(By.partialLinkText("Sell"), 10)) {
      takeScreenshot(screenShotName);
      return true;
    }
    return false;
  }

  public void clickGoalNameWith(String name) {
    String goalXpath;
    if (IS_IOS) {
      goalXpath = "//XCUIElementTypeStaticText[@name='" + name + "']";
      logger.info("xpath: {}", goalXpath);
    } else {
      goalXpath =
          "//android.widget.TextView[contains(@text,'" + name + "')]/../android.widget.TextView[1]";
      logger.info("xpath: {}", goalXpath);
    }
    scrollUpToFindElement(By.xpath(goalXpath), 8, 10);
    clickElement(isElementByClickable(By.xpath(goalXpath)));
  }

  public void clickSellNextBtn() {
    clickElement(waitUntilElementClickable(SellNextBtn));
  }

  public void enterBalance(String bValue) {
    andSendKeys(enterBalance, bValue);
  }

  @SneakyThrows
  public void clickSellAllButton(String screenShotName) {
    waitUntilElementClickable(SellAllBtn);
    clickElement(SellAllBtn);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void clickSellAllConfirm(String screenShotName) {
    waitUntilElementClickable(SellAllBtn);
    clickElement(SellAllBtn);
    waitUntilElementClickable(SellAllConfirmBtn);
    clickElement(SellAllConfirmBtn);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  public void clickAgainGoal() {
    waitUntilElementVisible(viewUpRecommen);
    clickElement(reeditLink);
  }

  public int oneTimeInvestmentValue() {
    waitUntilElementVisible(myCurrentWealth);
    BigDecimal myCurrentWealthTextInt =
        new BigDecimal(myCurrentWealth.getText().replaceAll(",", ""));
    BigDecimal currentWealthTextInt = myCurrentWealthTextInt.setScale(0, BigDecimal.ROUND_HALF_UP);
    return currentWealthTextInt.intValue();
  }

  public String getTotalMonthly() {
    scrollUp();
    waitUntilElementVisible(totalMonthly);
    String totalMonthlyValue = totalMonthly.getText();
    Integer totalMonthlyValueInt = Integer.valueOf(totalMonthlyValue);
    if (totalMonthlyValueInt < 1000) {
      return totalMonthlyValueInt.toString();
    } else {
      return totalMonthlyValueInt / 1000 + "K";
    }
  }

  public String getRetiredAgeLink() {
    return getElementText(retiredAge);
  }

  public String getEditPageTotalMonthly() {
    return getElementText(editPageTotalMonthly);
  }

  public int judgeCurrentAge(String name, String password) throws Exception {
    Map<String, String> valueExpression = VerifyUiAndApiGoalSetting.getYearAge(name, password);
    currentAge = Integer.parseInt(valueExpression.get("age"));
    return currentAge;
  }

  public boolean judgeCurAgeLessThanReAge(String age) throws Exception {
    if (currentAge <= Integer.parseInt(age)) {
      return true;
    }
    return false;
  }

  public int getCurrentWealth() {
    return Integer.parseInt(currentWealth.getText().replaceAll(",", ""));
  }

  @SneakyThrows
  public void retieveFeasibleMonthlyValue() {
    getMinMonlyValueLess =
        Integer.parseInt(
                Pattern.compile("[^0-9]").matcher(minMonlyValueLink.getText()).replaceAll(""))
            - 100;
    getMinMonlyValue =
        Integer.valueOf(
            Pattern.compile("[^0-9]").matcher(minMonlyValueLink.getText()).replaceAll(""));
    clearAndSendKeys(upMonthly, getMinMonlyValueLess.toString());
    inputEnter(upMonthly);
    assertThat(monthlySavingLowerThanMin.getText()).contains("less");
    clearAndSendKeys(upMonthly, getMinMonlyValue.toString());
    inputEnter(upMonthly);
    // top-left
    int x = slideToMax.getLocation().x;
    int y = slideToMax.getLocation().y;
    // size
    int x_start = x;
    int y_co = y + 2;
    TouchAction action = new TouchAction(driver);
    for (int i = 0; i < 100; i++) {
      action
          .longPress(PointOption.point(x_start, y_co))
          .moveTo(PointOption.point(x_start + 10, y_co))
          .release()
          .perform();
      x_start += 10;
      if (verifyElementExist(monthlySavingHigherThanMax)) {
        break;
      }
    }

    Integer getMaxMonlyValue = new Integer(upMonthly.getText().replaceAll(",", ""));
    clearAndSendKeys(upMonthly, Integer.toString(getMaxMonlyValue - 6000));
    inputEnter(upMonthly);
    for (Integer i = getMaxMonlyValue + 1000; i < 30000; i = i + 1000) {
      if (verifyElementExist(monthlySavingHigherThanMax)) {
        for (Integer j = i - 800; j < i; j = j + 200) {
          clearAndSendKeys(upMonthly, Integer.toString(j));
          inputEnter(upMonthly);
          if (verifyElementExist(monthlySavingHigherThanMax)) {
            clearAndSendKeys(upMonthly, Integer.toString(j - 100));
            inputEnter(upMonthly);
            break;
          }
        }
        break;
      } else {
        clearAndSendKeys(upMonthly, i.toString());
        inputEnter(upMonthly);
      }
    }
  }

  @SneakyThrows
  public boolean verifyGoalIsProcessing() {
    Thread.sleep(3000);
    return processingStatus.getText().contentEquals("Processing");
  }

  public boolean verifyExistTargetWealth() {
    return verifyElementExist(targetWealth) && verifyElementExist(updatingStatus);
  }

  public boolean verifyExistProbability() {
    return verifyElementExist(probability);
  }

  public boolean verifyExistGoalHorizon() {
    scrollUp();
    return verifyElementExist(goalHorizon);
  }

  public void editSellValueToOverRange() {
    waitUntilElementClickable(SellInputEdit);
    clickElement(SellInputEdit);
    clearAndSendKeys(SellInputEdit, "999999999");
    inputEnter(SellInputEdit);
  }

  public boolean verifyNextBtnUnClickable() {
    if (isElementClickable(By.partialLinkText("next"), 10)) {
      return true;
    }
    return false;
  }

  public void clearSellInputEdit() {
    clearInput(SellInputEdit);
  }

  @SneakyThrows
  public void iChangeMonthlyInvestment(){
    Thread.sleep(3000);
    String getMinMonthlyInvestmentText = getElementText(minMonthlyInvestment);
    String getStringMinMonthlyInvestment = getMinMonthlyInvestmentText.replaceAll("\\D","");
    int minMonthlyInvestment = Integer.parseInt(getStringMinMonthlyInvestment);
    int changeMonthlyInvestment = minMonthlyInvestment + 100;
    clearAndSendKeys(upMonthly, String.valueOf(changeMonthlyInvestment));
    inputEnter(upMonthly);
  }

  @SneakyThrows
  public void clickGoalWithGoalName(String goalName) {
    Thread.sleep(2000);
    clickElement(reachM);
//    String goalXpath;
//
//    goalXpath ="//android.widget.TextView[@text='" + goalname + "')]";
//    driver.findElementByXPath(goalXpath).click();
//    MobileElement ele = driver.findElementByXPath("//android.widget.TextView[@text='" + goalname + "')]");
//    if (verifyElementExist(ele)){
//      clickElement(ele);
//    }else{
//      scrollUpToFindElement(ele, 8, 10);
//      clickElement(ele);
//    }

//    MobileElement name = driver.findElement(By.xpath(goalXpath));
//    if(verifyElementExist(name)) {
//      clickElement(isElementByClickable(By.xpath(goalXpath)));
//    }else {
//      scrollUpToFindElement(By.xpath(goalXpath), 8, 10);
//      clickElement(isElementByClickable(By.xpath(goalXpath)));
//    }

  }

  SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  Date date = new Date(System.currentTimeMillis());
  @SneakyThrows
  public void iModifyTheGoalTitleToTheCurrentTime(String screenShotName) {
    Thread.sleep(2000);
    clickElement(editBtn);
    String goalNameText = formatter.format(date);
    GlobalVar.GLOBAL_VARIABLES.put("goalName", goalNameText);
    clearAndSendKeys(inputButton,formatter.format(date));
    inputEnter(inputButton);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
  }
//  public void clickAchieveFinancialFreedomButton() {
//    clickElement(financialFreedomLink);
//  }


  @SneakyThrows
  public boolean modifyName(String name,String screenShotName) {
    Thread.sleep(5000);
    clickElement(modifyButton);
    Thread.sleep(2000);
    clearInput(inputButton);
    Thread.sleep(2000);
    andSendKeys(inputButton,name);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    return goalNameText.getText().equals(name);
  }

  @SneakyThrows
  public void arrivalOrderConfirmationPage() {
    Thread.sleep(5000);
    clickElement(viewPortfolio);
    Thread.sleep(2000);
    clickElement(analysisNext);
    Thread.sleep(2000);
    clickElement(analysisNext);
    Thread.sleep(2000);
    clickElement(oedrtNext);
    Thread.sleep(2000);
    scrollUpToFindElement(confirmOrderNext, 18, 6);
    Thread.sleep(2000);
    clickElement(confirmOrderNext);
    Thread.sleep(5000);
    scrollUpToFindElement(slide, 18, 6);
    Thread.sleep(2000);
    clickElement(dot);
    List<String> yNow = getAttribute(slide);
    swipeDown(Integer.parseInt(yNow.get(0)), Integer.parseInt(yNow.get(1)),
            Integer.parseInt(yNow.get(2)), Integer.parseInt(yNow.get(3)));
    Thread.sleep(2000);
  }
  @SneakyThrows
  public void setOneTimeInvestmentValuenvestment(String Greater) {
    Thread.sleep(5000);
    selectGoal(investingHabitLink);
    String oneTimeInvestmentMoney = GlobalVar.GLOBAL_VARIABLES.get("availableBalance");
    if (Greater.equals("upwards")) {
      String str = oneTimeInvestmentMoney.replace(".", "");
      clearInput(oneTimeInvestmentInput);
      andSendKeys(oneTimeInvestmentInput, str + "99");
      System.out.println(isVisible(saveGoalBtn));
      System.out.println("----------------------------------------");
      System.out.println(saveGoalBtn.isEnabled());
    }
    if (Greater.equals("under")) {
      String str = oneTimeInvestmentMoney.replace(".", "");
      clearInput(oneTimeInvestmentInput);
      andSendKeys(oneTimeInvestmentInput,  "3000");
    }
  }
  public List<String> getAttribute(WebElement element){
    WebElement seekBar = element;
    String Now=seekBar.getAttribute("bounds");
    return test(Now);
  }
  public void swipeDown(int xstart, int ystart, int xend, int yend) {
    TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
    PointOption pointOption1 = PointOption.point(xstart, ystart);
    PointOption pointOption2 = PointOption.point(xend, yend);
    Duration duration = Duration.ofMillis(1000);
    WaitOptions waitOptions = WaitOptions.waitOptions(duration);
    touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
            .perform();
  }
  public static List<String> test(String Now) {
    String result = Now.replaceAll("[-+.^:,]","");
    List<String> lst = new ArrayList<String>();
    String quStr=Now.substring(Now.indexOf("[")+1,Now.indexOf("]"));
    int index = Now.indexOf("]");
    String after1 = Now.substring(index + 1);
    String[] yNow =quStr.split(",");
    lst.add(yNow[0]);
    lst.add(yNow[1]);
    String after2=after1.substring(after1.indexOf("[")+1,after1.indexOf("]"));
    String[] xStart =after2.split(",");
    lst.add(xStart[0]);
    lst.add(xStart[1]);
    return lst;
  }

  @SneakyThrows
  public void setBuildOneTimeInvestmentToday(String oneTime, String screenShotName) {
    clearInput(oneTimeInvestmentInput);
    andSendKeys(oneTimeInvestmentInput, oneTime);
    inputEnter(oneTimeInvestmentInput);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void clickGoalWithStatusOnOrOffTrack(String screenShotName) {
    scrollUpToFindElement(trackGoal, 20, 3);
    Thread.sleep(2000);
    takeScreenshot(screenShotName);
    clickElement(trackGoal);
  }


  public void globalVarAmount(String amount) {
    GlobalVar.GLOBAL_VARIABLES.put("globalVarAmount", amount);
  }
}
