package com.welab.automation.projects.wealth2.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.ScenarioContext;
import com.welab.automation.projects.wealth2.entities.SellOrder;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.springframework.util.StringUtils;

import java.text.NumberFormat;
import java.time.Duration;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.welab.automation.framework.utils.Utils.logFail;
import static org.assertj.core.api.Assertions.assertThat;

public class GoalSettingPage extends AppiumBasePage {
  protected String pageName = "Goal Setting Page";
  private final int maxInvestAge = 69;
  private final String investTimeErrorMessage = "time does not conform to time list options.";
  private String targetWealthValue;
  private String monthlyValue;
  private String oneTimeValue;
  private static final int customWait = 15;
  private String monthlyInvestmentMoney;
  private String oneTimeInvestmentMoney;
  private String mainPageCurrentWealth;

  @Getter
  @iOSXCUITFindBy(
      iOSNsPredicate =
          "label == \"View updated recommendation\" AND name == \"GOAL_SETTING_EDIT-btn-done\"")
  private MobileElement viewUpdatedRec;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'too much to reach')]")
  private MobileElement seemsLikeLabel;

  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeOther[@name=\"By checking the confirmation box, I confirm that I have read, understood and accepted the order details, fund documents, Wealth Management Services Terms, Important Notes and Risk Disclosures, the disclosure of monetary benefits received from the product issuer(s) and the relevant risks of the product(s).\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
  private MobileElement byChecking;

  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeOther[@name=\"Investment Plan New monthly investment HKD Min monthly investment: HKD 18,000\"]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]")
  private MobileElement investmentBtn;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Your goal has been edited!\"]")
  private MobileElement beenEdited;

  @iOSXCUITFindAll({
    @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"Done\"])[2]"),
    @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Done')]")
  })
  private MobileElement doneBtn;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[contains(@name, 'Yes')]")
  private MobileElement confirmYes;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Confirm')]")
  private MobileElement confirmConfirm;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'Back')]")
  private MobileElement confirmBack;

  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Next\"])[2]")
  private MobileElement confirmNext;

  @iOSXCUITFindAll({@iOSXCUITBy(xpath = "//*[contains(@name, 'ReachA')]")})
  private MobileElement onTrackReachA;

 @iOSXCUITFindAll({
          @iOSXCUITBy(xpath =  "//XCUIElementTypeOther[starts-with(@name, 'BuildB On track Current Wealth ')]"),
          @iOSXCUITBy(xpath =  "//XCUIElementTypeOther[starts-with(@name, 'Build My')]")
  })
  private MobileElement onTrackBuildB;

  @iOSXCUITFindBy(
      xpath = "//XCUIElementTypeOther[starts-with(@name, 'AchieveC On track Current Wealth ')]")
  private MobileElement onTrackAchieveC;

  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeOther[@name=\"Edit\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
  private MobileElement onTrackEdit;

  @Getter
  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Add New Goals']"),
    @AndroidBy(xpath = "//*[@text='增設理財目標']")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Add New Goals']")
  private MobileElement addNewGoalsLink;

  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Select your goal']"),
    @AndroidBy(xpath = "//*[@text='選擇新嘅理財目標']")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Select your goal']")
  private MobileElement selectGoalTitleTxt;

  @iOSXCUITFindBy(
      iOSNsPredicate = "name == 'GOAL_SETTING_SET_TARGET-message-investmentHigherThanRequirement'")
  private MobileElement warningMsg;

  @AndroidFindBy(xpath = "//*[@text='Exceed maximum amount']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Exceed maximum amount'")
  private MobileElement exceedMsg;

  @iOSXCUITFindBy(
      iOSNsPredicate = "label == \"1Y\" AND name == \"GOAL_SETTING_SET_TARGET-horizon\"")
  private MobileElement goalHorizon1Y;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"50% to 65%\"")
  private MobileElement label50_65;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'MP')]")
  private MobileElement MPID;

  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Continue']"),
    @AndroidBy(xpath = "//*[@text='繼續']")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Continue']/..")
  private MobileElement continueBtn;

  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='View recommendation & save my goal']"),
    @AndroidBy(xpath = "//*[@text='View recommendation']")
  })
  @iOSXCUITFindAll({
    @iOSXCUITBy(xpath = "//*[@name='View recommendation & save my goal']/XCUIElementTypeOther[1]"),
    @iOSXCUITBy(xpath = "//*[@name='View recommendation']/XCUIElementTypeOther[1]")
  })
  private MobileElement saveGoalBtn;

  @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Unlock the details']")})
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GOAL_PORTFOLIO_RECOMM_LOCK-btn-openAccount'")
  private MobileElement unlockDetailsBtn;

  // Set My Target Elements
  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Reach My Target']"),
    @AndroidBy(xpath = "//*[@text='設立我的目標']")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Reach My Target']")
  private MobileElement targetLink;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
    @AndroidBy(
        xpath =
            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name='What is your target wealth?']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  private MobileElement targetInput;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
    @AndroidBy(
        xpath =
            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name='What is your target wealth?']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
  private MobileElement targetEditBtn;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='What is your target wealth?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText"),
    @AndroidBy(
        xpath =
            "//*[@text='達到目標要幾多錢?']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name='What is your target wealth?']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  private MobileElement getOneTimeInvestmentInput;

  // SMT: set my target
  @AndroidFindAll({
          @AndroidBy(xpath = "(//android.widget.ScrollView//android.widget.ImageView)[2]/../android.widget.TextView"),
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.widget.TextView")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage/../..//XCUIElementTypeStaticText")
  private MobileElement SMTAgeSlider;

  // On Android, The index of the parent of these elements might be different on different device,
  // So must use the age
  // to locate the sub-element first, then find this elements list.
  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//android.widget.ScrollView//android.widget.TextView[@text='66']/../../android.view.ViewGroup"),
    @AndroidBy(
        xpath =
            "//android.widget.ScrollView//android.widget.TextView[@text='67']/../../android.view.ViewGroup"),
    @AndroidBy(
        xpath =
            "//android.widget.ScrollView//android.widget.TextView[@text='68']/../../android.view.ViewGroup"),
    @AndroidBy(
        xpath =
            "//android.widget.ScrollView//android.widget.TextView[@text='69']/../../android.view.ViewGroup")
  })
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        xpath =
            "//XCUIElementTypeOther[@name='When do you want to retire? (age)']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]//XCUIElementTypeOther"),
    @iOSXCUITBy(
        xpath =
            "//XCUIElementTypeOther[@name='When do you want to achieve your goal? (age)']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]//XCUIElementTypeOther")
  })
  private List<MobileElement> ageElementsList;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='One time investment today']/following-sibling::android.widget.TextView[2]")
  })
  private MobileElement oneTimeInvestmentCalculatedTxt;

  @AndroidFindAll({
    @AndroidBy(
        xpath = "//*[@text='Monthly investment']/following-sibling::android.widget.TextView[3]")
  })
  private MobileElement monthlyInvestmentCalculatedTxt;

  @AndroidFindBy(xpath = "//*[@text='Goal horizon']/../android.view.ViewGroup[2]")
  private MobileElement goalhorizonEdit;

  // Build My Investing Habit Elements
  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Build My Investing Habit']"),
    @AndroidBy(xpath = "//*[@text='建立我的投資習慣']")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Build My Investing Habit'")
  private MobileElement investingHabitLink;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='One-time investment today']/following-sibling::android.widget.EditText[1]"),
    @AndroidBy(
        xpath =
            "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        xpath =
            "//*[@name='One-time investment today']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField"),
    @iOSXCUITBy(
        xpath =
            "//*[@name='One time investment today']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  })
  private MobileElement oneTimeInvestmentInput;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='One-time investment today']/following-sibling::android.view.ViewGroup[1]"),
    @AndroidBy(
        xpath =
            "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup[1]//android.widget.ImageView")
  })
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        xpath =
            "//*[@name='One-time investment today']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]"),
    @iOSXCUITBy(
        xpath =
            "//*[@name='One time investment today']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
  })
  private MobileElement oneTimeInvestmentEditBtn;

  @AndroidFindAll({
    @AndroidBy(
        xpath = "//*[@text='Monthly investment']/following-sibling::android.widget.EditText[1]"),
    @AndroidBy(
        xpath =
            "//*[@text='Monthly investment']/following-sibling::android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.EditText")
  })
  @iOSXCUITFindBy(
      xpath =
          "//*[@name='Monthly investment']/following-sibling::XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  private MobileElement monthlyInvestmentInput;

  @AndroidFindAll({
    @AndroidBy(
        xpath = "//*[@text='Monthly investment']/following-sibling::android.view.ViewGroup[1]"),
    @AndroidBy(
        xpath =
            "//*[@text='Monthly investment']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  })
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        xpath =
            "//*[@name='Monthly investment']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]"),
    @iOSXCUITBy(
        xpath =
            "//*[@name='Monthly investment']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]")
  })
  private MobileElement monthlyInvestmentEditBtn;

  @AndroidFindBy(
      xpath =
          "//*[contains(@text,'Build My Investing Habit ')]/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='On track']")
  private MobileElement BMIHOntrackBtn;

  @AndroidFindBy(
      xpath =
          "//*[contains(@text,'Reach My Target')]/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='On track']")
  private MobileElement SMTOntrackBtn;

  @AndroidFindBy(
      xpath =
          "//*[contains(@text,'Achieve Financial Freedom')]/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='On track']")
  private MobileElement AFFOntrackBtn;

  @AndroidFindBy(
      xpath =
          "//*[@text='Reach My TargetAA']/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='Draft']")
  private MobileElement SMTDraftBtn;

  @AndroidFindBy(
      xpath =
          "//*[@text='Build My Investing HabitBB']/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='Draft']")
  private MobileElement BMIHDraftBtn;

  @AndroidFindBy(
      xpath =
          "//*[@text='Achieve Financial FreedomCC']/following-sibling::android.view.ViewGroup[2]/android.widget.TextView[@text='Draft']")
  private MobileElement AFFDraftBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView//android.widget.ImageView/following-sibling::android.widget.TextView")
  private List<MobileElement> YearsTexts;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText")
  private MobileElement editTimeInput;

  // Achieve Financial Freedom Elements
  @AndroidFindBy(xpath = "//*[@text='Achieve Financial Freedom']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Achieve Financial Freedom'")
  private MobileElement financialFreedomLink;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"Risk Rating\")]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[contains(@name, 'Fund weight Risk Rating')]")
  private MobileElement riskRate;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
  private List<MobileElement> retireAgeElements;

  @AndroidFindAll({
    @AndroidBy(xpath = "//android.widget.EditText[@text='Monthly expenses (HKD)']"),
    @AndroidBy(
        xpath =
            "//android.widget.TextView[@text='Monthly expenses (HKD)']/following-sibling::android.view.ViewGroup/android.widget.EditText")
  })
  @iOSXCUITFindBy(xpath = "//*[@name='Monthly expenses (HKD)']//XCUIElementTypeTextField")
  private MobileElement monthlyExpensesInput;

  @AndroidFindBy(xpath = "//*[starts-with(@text,'Budget')]")
  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Budget')]")
  private List<MobileElement> budgets;

  @AndroidFindBy(xpath = "//*[starts-with(@text,'Modest')]")
  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Modest')]")
  private List<MobileElement> modests;

  @AndroidFindBy(xpath = "//*[starts-with(@text,'Deluxe')]")
  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Deluxe')]")
  private List<MobileElement> deluxes;

  @AndroidFindBy(xpath = "//*[@text='Calculate Target Wealth']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Calculate Target Wealth'")
  private MobileElement calculateTargetWealthBtn;

  // Recommendation Elements
  @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Portfolio Recommendation']")})
  private MobileElement recommendationTitle;

  @AndroidFindAll({
    @AndroidBy(
        xpath = "//*[@text='One time investment']/following-sibling::android.widget.TextView[1]")
  })
  private MobileElement oneTimeInvestmentTxt;

  @AndroidFindAll({
    @AndroidBy(
        xpath = "//*[@text='Monthly investment']/following-sibling::android.widget.TextView[1]")
  })
  private MobileElement monthlyInvestmentTxt;

  // Tooltip Elements
  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//*[@text='How long do you want to be investing for?']/following-sibling::android.view.ViewGroup[1]")
  })
  private MobileElement timeTooltipIcon;

  @AndroidFindAll({@AndroidBy(xpath = "//*[@text='Tips']")})
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Tips'")
  private MobileElement toolTipTitle;

  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Tips']/following-sibling::android.widget.TextView")
  })
  private MobileElement toolTipTxt;

  @AndroidFindAll({
    @AndroidBy(xpath = "//*[@text='Tips']/following-sibling::android.view.ViewGroup")
  })
  private MobileElement toolTipCloseIcon;

  @AndroidFindBy(xpath = "(//android.widget.TextView[@text = 'View Portfolio'])")
  @iOSXCUITFindBy(iOSNsPredicate = "label == 'View Portfolio'")
  private MobileElement viewPortfolioBtn;

 @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"-title\"]")
 @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@label='View Portfolio'])[2]")
 private MobileElement viewPortfolio_discover;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "//android.widget.TextView[starts-with(@text, 'Target Wealth')]/../android.widget.TextView[2]")
  })
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[starts-with(@name,'Target Wealth')]/following-sibling::XCUIElementTypeStaticText")
  private MobileElement goalAchieveAmountValue;

  @AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc = '-title'])[2]")
  private MobileElement discoverBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Overview']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Overview'")
  private MobileElement overviewTab;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Portfolio']")
  @iOSXCUITFindBy(xpath = "//*[@name='Overview Portfolio']/XCUIElementTypeOther[@name='Portfolio']")
  private MobileElement portfolioTab;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Fund list']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Fund list'")
  private MobileElement fundListTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Fund list']/../android.view.ViewGroup")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Fund list']/following-sibling::*")
  private List<MobileElement> funds;

  @AndroidFindBy(accessibility = "FEATURED_FUND-btn-tipForRiskRating-1")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'FEATURED_FUND-btn-tipForRiskRating-1'")
  private MobileElement riskRateTip;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@contect-desc = 'FEATURED_FUND-btn-tipForMorningStarRating']")
  private MobileElement morningStarRateTitle;

  @AndroidFindBy(accessibility = "FEATURED_FUND-btn-tipForMorningStarRating")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'FEATURED_FUND-btn-tipForMorningStarRating'")
  private MobileElement morningStarRateTip;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Semi Annual Report']")
  private MobileElement fundLastDocument;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Tips']")
  private MobileElement toolTips;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Tips']/../android.view.ViewGroup")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name='Tips']/following-sibling::XCUIElementTypeOther[1]")
  private MobileElement closeToolTipsBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Tips']/../android.widget.TextView[2]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Tips']/following-sibling::*")
  private MobileElement tipsContent;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"Retire at\")]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, 'Retire at')]")
  private MobileElement retiredAge;

  @Getter
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Tips']/following-sibling::*")
  private List<MobileElement> tipsContentList;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Draft']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Draft'")
  private List<MobileElement> draftBtns;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Draft']")
  private MobileElement draftBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'My Goals']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'My Goals'")
  private MobileElement myGoalsTitle;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Delete']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'MY_GOAL_DETAIL-btn-delete'")
  protected MobileElement deleteBtn;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Edit']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'MY_GOAL_DETAIL-btn-edit'")
  protected MobileElement editBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Confirm']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Confirm'")
  private MobileElement confirmBtn;

  @AndroidFindBy(xpath = "//*[@text='View Portfolio']")
  private MobileElement ViewPortfolioBtn;

  @AndroidFindBy(
      xpath = "//*[@text='Target Wealth (HKD)']/following-sibling::android.widget.TextView[1]")
  private MobileElement targetWealthFFTxt;

  @AndroidFindBy(
      xpath = "//*[@text='Total Monthly Expenses']/following-sibling::android.widget.TextView[1]")
  private MobileElement totalMonthlyExpensesFFTxt;

  @AndroidFindBy(
      xpath =
          "//*[@text='Target wealth {target wealth amt}']/following-sibling::android.widget.TextView[1]")
  private MobileElement targetWealthTxt;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Discover\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Discover'")
  private MobileElement discoveryElement;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Portfolio Recommendation\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Portfolio Recommendation'")
  private MobileElement portfolioRecommendationElement;

  @AndroidFindBy(
      xpath =
          "//android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[@text=\"View Portfolio\"]")
  private MobileElement viewPortfolioElement;

  @AndroidFindBy(xpath = "//android.widget.Button/../../android.widget.TextView[1]")
  @iOSXCUITFindBy(
      xpath = "//*[@name='GOAL_PORTFOLIO_DETAIL-btn-buy']/../../XCUIElementTypeOther[1]")
  private MobileElement portfolioNameElement;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Buy\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GOAL_PORTFOLIO_DETAIL-btn-buy'")
  protected MobileElement buyButton;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Order\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GOAL_PORTFOLIO_DETAIL-confirmModal-btn-next'")
  private MobileElement orderTitle;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text=\"Order\"]/following-sibling::android.widget.TextView[2]")
  @iOSXCUITFindBy(xpath = "//*[@name='Order']/following-sibling::XCUIElementTypeStaticText[2]")
  private MobileElement jumpPagePortfolioName;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Next\"]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@label='Next']/..")
  protected MobileElement nextButton;

  @AndroidFindBy(xpath = "//android.view.View[@text=\"Review and confirm order\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review and confirm order'")
  private MobileElement confirmViewTitle;

  @AndroidFindBy(
      xpath =
          "//android.widget.Button[@content-desc=\"Portfolio, back\"]/../android.view.ViewGroup[2]/android.view.ViewGroup")
  private MobileElement xAtReviewConfirmPage;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Learn more\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Learn more'")
  private MobileElement learMoreLink;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"GoEasy Features\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoEasy Features'")
  private MobileElement goEasyFeatures;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Create a goal\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Create a goal'")
  private MobileElement createAGoal;

  @AndroidFindBy(
      xpath =
          "//android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup[1]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeScrollView")
  private MobileElement horizontalScrollView;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Are you sure you want to quit?\"]")
  private MobileElement quitWarning;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Not now\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Not now'")
  private MobileElement notNow;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Quit and save\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Quit and save'")
  private MobileElement quitAndSave;

  @AndroidFindBy(xpath = "//android.view.View[@text=\"Wealth\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Wealth'")
  private MobileElement wealthTitle;

  @AndroidFindBy(
      xpath = "//android.widget.HorizontalScrollView/../../../android.widget.TextView[2]")
  private MobileElement wealthCurrentWealth;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Draft\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Draft'")
  private MobileElement draftStatus;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Reach My Target']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='達成我的目標']")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Reach My Target'")
  private MobileElement reachMyTarget;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"Build My Investing Habit Make monthly investment an effortless habit.\"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeImage")
  private MobileElement buildMyHabit;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"Achieve Financial Freedom Want to enjoy your retirement life? We help you achieve your dream.\"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeImage")
  private MobileElement achieveFreedom;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text=\"There is insufficient amount in your account\"]")
  private MobileElement insufficientAmount;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text=\"Please go back to add money and make sure you have sufficient fund in your account.\"]")
  private MobileElement addMoneyMessage;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Back to Home\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Back to Home'")
  private MobileElement backToHomeBtn;

  @Getter
  @AndroidFindAll({
    @AndroidBy(accessibility = "Select your goal, back"),
    @AndroidBy(accessibility = "Go back"),
    @AndroidBy(accessibility = "Achieve Financial Freedom, back"),
    @AndroidBy(accessibility = "Portfolio Recommendation, back"),
    @AndroidBy(accessibility = "GOAL_STACK, back"),
    @AndroidBy(accessibility = "WEALTH_HOME_STACK, back"),
    @AndroidBy(accessibility = "Reach My Target, back"),
    @AndroidBy(accessibility = "Set My Target, back"),
    @AndroidBy(accessibility = "Build My Investing Habit, back"),
    @AndroidBy(accessibility = "GOAL_DETAIL_FINANCIAL_FREEDOM, back"),
    @AndroidBy(accessibility = "btnBack"),
    @AndroidBy(accessibility = "GOAL_INVESTING_HABIT, back"),
    @AndroidBy(accessibility = "MY_GOAL_DETAIL, back"),
    @AndroidBy(accessibility = "HOME_STACK, back")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name IN {'header-back', 'btnBackContainer'}")
  private MobileElement headerBackBtn;

  // BIM: Build My Investing Habit
  @AndroidFindAll({
//          @AndroidBy(xpath = "(//android.widget.ScrollView//android.widget.ImageView)[3]/../android.widget.TextView"),
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup[1]/android.widget.TextView")
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage/../..//XCUIElementTypeStaticText")
  private MobileElement investTimeSlider;

  @AndroidFindAll({
         @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup[1]/android.widget.TextView")
  })
  private MobileElement investTimeSliderSM_A560;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup")
  @iOSXCUITFindBy(
      xpath =
          "//*[@name='How long do you want to invest?']/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther[2]//XCUIElementTypeOther")
  protected List<MobileElement> BIMTimeList_candidate1;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup")
  protected List<MobileElement> BIMTimeList_candidate2;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup")
  protected List<MobileElement> BIMTimeList_candidate3;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup")
  protected List<MobileElement> BIMTimeList_candidate4;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Processing'")
  @AndroidFindBy(xpath = "//*[@text='Processing']")
  private MobileElement processing;

  @iOSXCUITFindBy(iOSNsPredicate = "name BEGINSWITH 'Loading'")
  @AndroidFindBy(xpath = "//*[starts-with(@text, 'Loading')]")
  private MobileElement loading;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Reach')]/../android.widget.TextView[1]")
  private MobileElement SetMyTargetName;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Achieve')]/../android.widget.TextView[1]")
  private MobileElement AFFName;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Build')]/../android.widget.TextView[1]")
  private MobileElement BMIHName;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]")
  private MobileElement GoalNameEditButton;

  @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"MY_GOAL_DETAIL-editName\"]")
  @iOSXCUITFindAll({
    @iOSXCUITBy(
        xpath =
            "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]//XCUIElementTypeTextField"),
    @iOSXCUITBy(
        xpath =
            "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]")
  })
  private MobileElement editName;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup[1]/android.widget.EditText[1]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]//XCUIElementTypeTextField")
  private MobileElement nameInput;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'DraftMyTarget')]")
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'DraftMyTarget')]/../android.widget.TextView[1]")
  private MobileElement draftSetMyTargetName;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'DraftFreedom')]/../android.widget.TextView[1]")
  private MobileElement draftAFFName;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'DraftMyHabit')]/../android.widget.TextView[1]")
  private MobileElement draftBMIHName;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@content-desc=\"MY_GOAL_DETAIL-btn-edit\"]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement editBtnAtGoalPage;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement editBtnAtFreedomPage;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement editBtnAtSetMyTargetPage;

  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeOther[@name=\"HKD\"]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText[2]")
  private MobileElement editMonthlyInvestment;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"85K\" AND name == \"85K\" AND value == \"85K\"")
  private MobileElement label_85k;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"32.5M\"")
  private MobileElement label_32m;

 @Getter
 @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"Total monthly\")]/following-sibling::android.widget.TextView")
 private MobileElement totalMonthlyRetirement;

  public GoalSettingPage() {
    super.pageName = this.pageName;
  }

  public List<MobileElement> getBIMTimeList() {
    if (BIMTimeList_candidate1.size() > 60) {
      logger.info("BIMTimeList_candidate1 size:{}", BIMTimeList_candidate1.size());
      return BIMTimeList_candidate1;
    } else if (BIMTimeList_candidate2.size() > 60) {
      logger.info("BIMTimeList_candidate2 size:{}", BIMTimeList_candidate2.size());
      return BIMTimeList_candidate2;
    } else if (BIMTimeList_candidate3.size() > 60) {
      logger.info("BIMTimeList_candidate3 size:{}", BIMTimeList_candidate3.size());
      return BIMTimeList_candidate3;
    } else if (BIMTimeList_candidate4.size() > 60) {
      logger.info("BIMTimeList_candidate4 size:{}", BIMTimeList_candidate4.size());
      return BIMTimeList_candidate4;
    }
    logger.error("No matched candidate.");
    return null;
  }

 @AndroidFindAll({
         @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView"),
         @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.widget.TextView")
 })
 @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage/../..//XCUIElementTypeStaticText")
 private MobileElement FFTAgeSlider;

  @AndroidFindBy(
      xpath =
          "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement TargetMonthly;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"HKD\"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'Investment Plan')]/following-sibling::android.widget.EditText[1]")
  private MobileElement investmentPlan;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"HKD\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'Investment Plan')]/following-sibling::android.widget.EditText[2]")
  private MobileElement monthlyInvestment;

  @AndroidFindBy(xpath = "//*[@text='Investment Plan']")
  private MobileElement investmentButton;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'HKD')]")
  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'HKD')]")
  private List<MobileElement> containsHKD;

  @iOSXCUITFindBy(xpath = "//*[contains(@name, 'HKD')]")
  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'HKD')]")
  private MobileElement containsHKDs;

  @AndroidFindBy(
      xpath =
          "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.EditText[2]")
  private MobileElement oneTimeText;

  @AndroidFindBy(xpath = "//*[@text='Goal horizon']/following-sibling::android.widget.TextView")
  private MobileElement goaHorizon;

  @AndroidFindBy(xpath = "//*[@text='Slide to Confirm']/..")
  @iOSXCUITFindBy(xpath = "//*[@name='Slide to Confirm']")
  private MobileElement slideToConfirm;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'By checking the confirmation box')]/../android.view.ViewGroup[4]")
  private MobileElement radioBtn2;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Done']")
  @iOSXCUITFindBy(xpath = "//*[@name='-title']/..")
  private MobileElement done;

  @AndroidFindBy(xpath = "//*[@text='Yes']")
  private MobileElement yesBtn;

  @AndroidFindBy(
      xpath =
          "//*[@class='android.widget.HorizontalScrollView']/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[4]")
  private MobileElement probability;

  @AndroidFindBy(
      xpath =
          "//*[@class='android.widget.HorizontalScrollView']/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]")
  private MobileElement title;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'My Target')]/following-sibling::android.view.ViewGroup/android.widget.TextView[@text='On track']")
  private MobileElement myTargetOnTrack;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'Achieve Financial Freedom ')]/following-sibling::android.view.ViewGroup/android.widget.TextView[@text='On track']")
  private MobileElement achieveFinancialFreedomOnTrack;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,'Build My Investing Habit')]/following-sibling::android.view.ViewGroup/android.widget.TextView[@text='On track']")
  private MobileElement buildMyInvestingHabitOnTrack;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Sell order']/following-sibling::android.view.ViewGroup[1]/android.widget.EditText")
  @iOSXCUITFindBy(
      iOSNsPredicate =
          "value == \"One-time redemption (USD)\" AND type == \"XCUIElementTypeTextField\"")
  private MobileElement inputBtn;

  @AndroidFindBy(xpath = "//*[@text='Proceed']")
  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Proceed\"")
  private MobileElement ProceedBtn;

  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[@name=\"Sell now\"]/following-sibling::XCUIElementTypeStaticText")
  private MobileElement sellingPortfolioName;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@content-desc=\"MY_GOAL_DETAIL-btn-sell\"]/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Sell\" AND name == \"Sell\" AND value == \"Sell\"")
  private MobileElement SellBtn;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[7]/android.view.ViewGroup[1]")
  private MobileElement SellAllConfirmBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"SELL_GOAL-btn-next-title\"]")
  private MobileElement nextBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
  private List<MobileElement> orderInformation;

  @AndroidFindBy(
      xpath =
          "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
  private MobileElement orderInformationd;

  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Estimated sell amount\"])[1]")
  private MobileElement estimatedSellAmountTitle;

  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText)[1]")
  private MobileElement portfolioName;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Estimated sell amount\"]/../..")
  private List<MobileElement> estimatedSellAmountValues;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Settle in\"]/../..")
  private List<MobileElement> settleInValue;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Fund Name\"]/../..")
  private List<MobileElement> fundNameValues;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Indicative NAV\"]/../..")
  private List<MobileElement> indicativeNAVValues;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Number of units\"]/../..")
  private List<MobileElement> numberOfUnitsValues;

  @iOSXCUITFindBy(
      xpath = "//XCUIElementTypeStaticText[@name=\"Fund list\"]/../../XCUIElementTypeOther")
  private List<MobileElement> portfolioFundList;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "label == 'On track'")
  private MobileElement onTrackBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[4]")
  private MobileElement SuccessRate;

  @AndroidFindBy(
      xpath =
          "//android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[6]")
  private MobileElement orderDisposable;

  @AndroidFindBy(
      xpath =
          "//android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[8]")
  private MobileElement orderMonthlyPayment;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[starts-with(@text, 'Target Wealth (HKD)')]/../android.widget.TextView[2]")
  private MobileElement targetWealth;

  @AndroidFindBy(xpath = "//*[@text='View New Portfolio']")
  private MobileElement viewNewPortfolioBtn;

 @AndroidFindBy(xpath = "//*[@text='Fund list']")
 private MobileElement fundListName;

 @AndroidFindBy(
         xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
 private List<MobileElement> fundListsNameOne;

 @AndroidFindBy(
         xpath = "//android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
 private List<MobileElement> fundListsNameTwo;

 @AndroidFindBy(
         xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]")
 private List<MobileElement> fundListsNumOne;

 @AndroidFindBy(
         xpath = "//android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]")
 private List<MobileElement> fundListsNumTwo;

  @Getter
  @iOSXCUITFindBy(
      xpath = "//XCUIElementTypeStaticText[@name=\"My Goals\"]/../../XCUIElementTypeOther")
  private List<MobileElement> targets;

 @AndroidFindBy(
         xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[7]/android.widget.TextView[2]")
 private MobileElement moreThanMonthlyIncome;

 @AndroidFindBy(
         xpath =
                 "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView")
 protected List<MobileElement> portfolioInvestmentList;

  public void addNewGoal() {
    scrollUpToFindElement(addNewGoalsLink, 18, 1);
    clickElement(addNewGoalsLink);
    waitUntilElementVisible(selectGoalTitleTxt);
  }

  public void selectGoal(MobileElement goalsElement) {
    clickElement(goalsElement);
  }

  public boolean checkTooltip(MobileElement toolTipIcon, String expectedToolTip) {
    clickElement(toolTipIcon);
    waitUntilElementVisible(toolTipTitle);
    String actualToolTip = getElementText(toolTipTxt);
    clickElement(toolTipCloseIcon);
    return actualToolTip.equals(expectedToolTip);
  }

  private void setGoalTime(MobileElement sourceTimeElement, String age) {
    int ageIndex = Integer.parseInt(age) - (maxInvestAge - ageElementsList.size()) - 1;
    MobileElement targetTimeElement = ageElementsList.get(ageIndex);
    setAge(sourceTimeElement, targetTimeElement);
  }

  // There will be no animation with longPress()
  public void setAge(MobileElement start, MobileElement end) {
    TouchAction action = new TouchAction(driver);
    action
        .longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(start)))
        .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
        .moveTo(ElementOption.element(end))
        .release()
        .perform();
  }

 public void setAges(MobileElement start, MobileElement end) {
  TouchAction action = new TouchAction(driver);
  action
          .longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(start)))
          .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
          .moveTo(ElementOption.element(end))
          .release()
          .perform();
 }
  // press() by elements will mismatch the destination, so move by points.
  private void setAgeWithAnimation(MobileElement start, MobileElement end) {
    TouchAction action = new TouchAction(driver);
    action
        .press(PointOption.point(start.getCenter().getX(), start.getCenter().getY()))
        .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
        .moveTo(PointOption.point(end.getCenter().getX(), start.getCenter().getY()))
        .release()
        .perform();
  }

  private String formatValue(String value) {
    String[] values = value.split(",");
    StringBuffer sf = new StringBuffer();
    for (String v : values) {
      sf.append(v);
    }
    return sf.toString();
  }

  public void startSetMyTarget(String age) {
    selectGoal(targetLink);
    setGoalTime(SMTAgeSlider, age);
    waitUntilElementVisible(continueBtn);
  }

  @SneakyThrows
  public boolean setTargetGoal(String targetValue, String time, String... name) {
        selectGoal(targetLink);
    if (name.length > 0) {
      clickGoalNameEditButton();
      editGoalSettingName(name[0]);
    }
    if (null != targetValue) {
      clickElement(targetEditBtn);
      clearAndSendKeys(targetInput, targetValue);
      inputEnter(targetInput);
    }
    try {
      exceedMsg.isDisplayed();
      return false;
    } catch (NoSuchElementException ex) {
      logger.debug("No error msg.");
    }
    clickElement(SMTAgeSlider);
    scrollUp(scrollPosition.RIGHT);
    setGoalTime(SMTAgeSlider, time);
    Thread.sleep(200);
    return true;
  }

 @SneakyThrows
 public void clickReachTarget() {
   Thread.sleep(3000);
   clickElement(reachMyTarget);
 }

 @SneakyThrows
  public void clickBuildHabit(String screenShotName) {
   Thread.sleep(2000);
   takeScreenshot(screenShotName);
    clickElement(investingHabitLink);
  }

  public void clickAchieveFreedom() {
    waitUntilElementVisible(achieveFreedom);
    clickElement(achieveFreedom);
  }

  public void clickContinue() {
    waitUntilElementVisible(continueBtn);
    clickElement(continueBtn);
  }

  public String getTargetWealth() {
    return getElementText(targetInput);
  }

  // As the element index will be changed when setting the age. This method is to get the updated
  // age.
  public String getSMTAge() {
    return getElementText(SMTAgeSlider);
  }

  public String getCalculatedOneTimeInvestment() {
    return getElementText(oneTimeInvestmentCalculatedTxt);
  }

  public String getCalculatedMonthlyInvestment() {
    return getElementText(monthlyInvestmentCalculatedTxt);
  }

  public String getGoalAchieveAmount() {
    waitUntilElementVisible(goalAchieveAmountValue);
    return getElementText(goalAchieveAmountValue);
  }

  @SneakyThrows
  public void saveGoal() {
    // on iOS, sometimes saveGoalBtn is obscured by a warning dialog, wait some seconds to cancel it
    // manually.
    waitUntilElementVisible(saveGoalBtn);
    clickElement(saveGoalBtn);
  }

  public void viewPortfolio() {
    viewPortfolioOverview();
    viewPortfolioTab();
  }

  // GD_01_Overview
  @SneakyThrows
  public void viewPortfolioOverview() {
    waitUntilElementClickable(viewPortfolioBtn);
    // if no sleep, it will not click viewPortfolioBtn
    Thread.sleep(2 * 1000);
    clickElement(viewPortfolioBtn);
  }

  // GD_03_Portfolio
  public void viewPortfolioTab() {
    waitUntilElementClickable(portfolioTab);
    clickElement(portfolioTab);
    waitUntilElementVisible(fundListTitle);
  }

  public Boolean checkCrrValue(int crrvalue) {
    String riskRateint = getElementText(riskRate);
    String riskRateint2 = riskRateint.split("Rating")[1].split("Asset class")[0];
    int riskRatetxt =
        Integer.parseInt(riskRateint.split("Rating")[1].split("Asset class")[0].trim());
    String MPIDint = getElementText(MPID);
    int MPIDtxt = Integer.parseInt(MPIDint.split("MP")[1].substring(0, 1));
    if (crrvalue == 1 && riskRatetxt == 1 && MPIDtxt >= 1 && MPIDtxt <= 2) return true;
    else if (crrvalue > 1 && riskRatetxt >= 1 && riskRatetxt <= 5 && MPIDtxt >= 1 && MPIDtxt <= 12)
      return true;
    return false;
  }

  @SneakyThrows
  public void viewFeatureFunds() {
    for (MobileElement fund : funds) {
      clickElement(fund);
      Thread.sleep(1000 * 5);
      waitUntilElementVisible(riskRateTip);
      clickElement(headerBackBtn);
      Thread.sleep(1000 * 3);
      waitUntilElementVisible(fundListTitle);
    }
    clickElement(funds.get(0));
    waitUntilElementVisible(riskRateTip);
  }

  public void clickYespopup() {
    waitUntilElementVisible(confirmYes);
    clickElement(confirmYes);
  }

  public void clickNext() {
    scrollUpToFindElement(confirmNext, 2, 2);
    clickElement(confirmNext);
  }

  public void clickConfirmpopup() {
    clickElement(confirmConfirm);
  }

  public void clickDone() {
    clickElement(doneBtn);
  }

  public void clickBackpopup() {
    clickElement(confirmBack);
  }

  public boolean checkViewUpdatedBtn() {
    return isElementVisible(viewUpdatedRec);
  }

  public void clickByChecing() {
    scrollUpToFindElement(byChecking, 2, 2);
    // clickElement(byChecking);
  }

  public void clickViewUpdatedRec() {
    waitUntilElementVisible(viewUpdatedRec);
    clickElement(viewUpdatedRec);
  }

  public void clickOnTrackReachA() {
    waitUntilElementVisible(onTrackReachA);
    clickElement(onTrackReachA);
  }

  public void clickOnTrackBuildB() {
   scrollUpToFindElement(onTrackBuildB,2,2);
    clickElement(onTrackBuildB);
  }

  public void clickOnTrackAchieveC() {
    scrollUpToFindElement(onTrackAchieveC, 2, 2);
    clickElement(onTrackAchieveC);
  }

  public void clickOnTrackEdit() {
    clickElement(onTrackEdit);
  }

  public boolean verifyBeenEdited() {
    return isElementVisible(beenEdited);
  }

  public boolean verifySeemLikeLabel() {
    waitUntilElementVisible(seemsLikeLabel);
    return isElementVisible(seemsLikeLabel);
  }

  @SneakyThrows
  public void viewFirstFeatureFund() {
    clickElement(funds.get(0));
    waitUntilElementVisible(riskRateTip);
  }

  @SneakyThrows
  public String getRiskRateTips() {
    // sometimes it will not click the element, so wait
    Thread.sleep(1000);
    clickElement(riskRateTip);
    waitUntilElementVisible(toolTipTitle);
    return getElementText(tipsContent);
  }

  public void closeToolTips() {
    clickElement(closeToolTipsBtn);
    waitUntilElementClickable(morningStarRateTip);
  }

  public String getMorningStarRateTips() {
    clickElement(morningStarRateTip);
    waitUntilElementClickable(toolTipTitle);
    return getElementText(tipsContent);
  }

  public void backToWealthMainPageFromBIHFund() {
    clickElement(headerBackBtn);
    waitUntilElementVisible(portfolioTab);
    clickElement(headerBackBtn);
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void backToWealthMainPageFromFFTFund() {
    clickElement(headerBackBtn);
    waitUntilElementVisible(portfolioTab);
    clickElement(headerBackBtn);
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(calculateTargetWealthBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(continueBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void backToWealthMainPageFromSMTFund() {
    clickElement(headerBackBtn);
    waitUntilElementVisible(portfolioTab);
    clickElement(headerBackBtn);
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(continueBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void backToWealthMainPageFromPortfolioRecommendAsSMT() {
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(continueBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void backToWealthMainPageFromPortfolioRecommendAsAFF() {
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(calculateTargetWealthBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(continueBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void backToWealthMainPageFromPortfolioRecommendAsBIH() {
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(selectGoalTitleTxt);
    clickElement(headerBackBtn);
  }

  public void delDraft() {
    waitUntilElementVisible(myGoalsTitle, 60);
    Optional.ofNullable(draftBtns)
        .ifPresent(
            buttons -> {
              try {
                buttons.stream()
                    .forEach(
                        draftBtn -> {
                          scrollUpToFindElement(draftBtn, 2, 2);
                          clickElement(draftBtn);
                          waitUntilElementClickable(deleteBtn);
                          clickElement(deleteBtn);
                          waitUntilElementClickable(confirmBtn);
                          clickElementCenter(confirmBtn);
                          scrollDownToFindElement(myGoalsTitle, 3, 2);
                        });
              } catch (StaleElementReferenceException e) {
                logger.debug("");
              }
            });
  }

  @SneakyThrows
  private void selectExpensesOption(String[] values) {
    String travel = values[0];
    String medical = values[1];
    String rental = values[2];
    if (!travel.isEmpty()) {
      if (travel.trim().equalsIgnoreCase("budget")) clickElement(budgets.get(0));
      else if (travel.trim().equalsIgnoreCase("modest")) clickElement(modests.get(0));
      else if (travel.trim().equalsIgnoreCase("deluxe")) clickElement(deluxes.get(0));
      Thread.sleep(1000);
    }
    if (!medical.isEmpty()) {
      if (medical.trim().equalsIgnoreCase("budget")) clickElement(budgets.get(1));
      else if (medical.trim().equalsIgnoreCase("modest")) clickElement(modests.get(1));
      else if (medical.trim().equalsIgnoreCase("deluxe")) clickElement(deluxes.get(1));
      Thread.sleep(1000);
    }
    if (!rental.isEmpty()) {
      if (rental.trim().equalsIgnoreCase("budget")) clickElement(budgets.get(2));
      else if (rental.trim().equalsIgnoreCase("modest")) clickElement(modests.get(2));
      else if (rental.trim().equalsIgnoreCase("deluxe")) clickElement(deluxes.get(2));
    }
  }

  @SneakyThrows
  public boolean setInvestingHabitGoal(
      String oneTimeValue, String monthlyValue, String time, String... name) {
    monthlyInvestmentMoney = oneTimeValue;
    oneTimeInvestmentMoney = monthlyValue;
    selectGoal(investingHabitLink);
    Thread.sleep(1000 * 2);
    if (name.length > 0) {
      clickGoalNameEditButton();
      editGoalSettingName(name[0]);
    }
    if (!setInvestmentPlan(oneTimeValue, monthlyValue)) return false;
    if (null != time) {
      try {
        setInvestingTime(time);
      } catch (Exception e) {
        logger.error("time does not conform to time list options.");
      }
    }
    return true;
  }

  public boolean setInvestmentPlan(String oneTimeValue, String monthlyValue) {
    if (null != oneTimeValue) {
      clickElement(oneTimeInvestmentEditBtn);
      clearAndSendKeys(oneTimeInvestmentInput, oneTimeValue);
      if (!IS_IOS) hideKeyboard();
      try {
        exceedMsg.isDisplayed();
        return false;
      } catch (NoSuchElementException ex) {
        logger.debug("No error msg.");
      }
      inputEnter(oneTimeInvestmentInput);
    }
    if (null != monthlyValue) {
      clickElement(monthlyInvestmentEditBtn);
      clearAndSendKeys(monthlyInvestmentInput, monthlyValue);
      if (!IS_IOS) hideKeyboard();
      try {
        exceedMsg.isDisplayed();
        return false;
      } catch (NoSuchElementException ex) {
        logger.debug("No error msg.");
      }
      inputEnter(monthlyInvestmentInput);
    }
    return true;
  }

  public String formatInvestTime(String time) {
    int t = Integer.parseInt(time);
    if (t > 60) {
      if ((t < 120) && (t % 12 == 6)) {
        return String.valueOf((float) t / 12);
      }
      return String.valueOf(t / 12);
    }
    return time;
  }

  public int convertInvestTime2Index(String time) throws Exception {
    int index = Integer.parseInt(time);

    if ((index > 60) && (index < 120)) {
      if ((index) % 6 != 0) {
        throw new Exception(investTimeErrorMessage);
      }
      // 60 + (index - 60)/6
      index = 50 + index / 6;
    } else if ((index >= 120) && (index <= 408)) {
      if ((index) % 12 != 0) {
        throw new Exception(investTimeErrorMessage);
      }
      // 60 + (120 - 60)/6 + (index - 120)/12
      index = 60 + index / 12;
    } else if (index > 408) {
      throw new Exception(investTimeErrorMessage);
    }
    logger.info("index:{}", index);
    return index - 1;
  }

  public void setInvestingTime(String time) throws Exception {
    int index = convertInvestTime2Index(time);
    MobileElement targetTimeElement = getBIMTimeList().get(index-11);
    if (isSamsungA5460()) {
     setAge(investTimeSliderSM_A560, targetTimeElement);
    } else {
     setAge(investTimeSlider, targetTimeElement);
    }
  }

  public String getBIMAgeSliderText() {
    return getElementText(investTimeSlider);
  }

  public void setFinancialFreedomGoal(String retiredAge) {
    selectGoal(financialFreedomLink);
    waitUntilElementVisible(continueBtn);
    int offset = Integer.parseInt(retiredAge) - (maxInvestAge - ageElementsList.size()) - 1;
    MobileElement retiredAgeElement = ageElementsList.get(offset);
    setAge(FFTAgeSlider, retiredAgeElement);
  }

  public String getFFTAgeSliderText() {
    return getElementText(FFTAgeSlider);
  }

  public String getRetiredAgeText() {
    return getElementText(retiredAge);
  }

  public void setMonthlyExpenses(String monthlyExpenses, String otherExpenses) {
    waitUntilElementVisible(monthlyExpensesInput);
    if (null != monthlyExpenses) {
      clearAndSendKeys(monthlyExpensesInput, monthlyExpenses);
      inputEnter(monthlyExpensesInput);
    }
    String[] values = otherExpenses.split(",", 3);
    selectExpensesOption(values);
  }

 public void enterMonthlyExpenses(String monthlyExpenses, String otherExpenses,String screenShotName) {
  waitUntilElementVisible(monthlyExpensesInput);
  if (null != monthlyExpenses) {
   clearAndSendKeys(monthlyExpensesInput, monthlyExpenses);
   inputEnter(monthlyExpensesInput);
  }
  String[] values = otherExpenses.split(",", 3);
  selectExpensesOption(values);
  takeScreenshot(screenShotName);
 }

  public void setDefaultMonthlyExpenses() {
    clearAndSendKeys(monthlyExpensesInput, "1000");
    inputEnter(monthlyExpensesInput);
  }

  public void calculateTargetWealth() {
    waitUntilElementClickable(calculateTargetWealthBtn);
    clickElement(calculateTargetWealthBtn);
  }

  public Map<String, String> getFinancialFreedomTarget() {
    Map<String, String> fft = new HashMap<>();
    fft.put("target wealth", getElementText(targetWealthFFTxt));
    fft.put("total monthly expenses", getElementText(totalMonthlyExpensesFFTxt));
    fft.put("one time investment", getElementText(oneTimeInvestmentCalculatedTxt));
    fft.put("monthly investment", getElementText(monthlyInvestmentCalculatedTxt));

    return fft;
  }

  public boolean verifyGoal(String oneTimeValue, String monthlyValue) {
    waitUntilElementVisible(recommendationTitle);
    String actualOneTimeValue = getElementText(oneTimeInvestmentTxt);
    String actualMonthlyValue = getElementText(monthlyInvestmentTxt);
    if (actualOneTimeValue.equals("HKD " + oneTimeValue)
        && actualMonthlyValue.equals("HKD " + monthlyValue)) {
      return true;
    } else {
      return false;
    }
  }

  public boolean verifyAFMData() {
    if (!isElementVisible(label_32m)) return false;
    if (!isElementVisible(label_85k)) return false;
    return true;
  }

  public boolean viewRecommendation() {
    waitUntilElementVisible(recommendationTitle);
    String actualOneTimeValue = getElementText(oneTimeInvestmentTxt);
    String actualMonthlyValue = getElementText(monthlyInvestmentTxt);
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    if (!oneTimeValue.contains(",")) {
      oneTimeValue = nf.format(Long.valueOf(oneTimeValue));
    }
    if (!monthlyValue.contains(",")) {
      monthlyValue = nf.format(Long.valueOf(monthlyValue));
    }
    if (actualOneTimeValue.equals("HKD " + oneTimeValue)
        && actualMonthlyValue.equals("HKD " + monthlyValue)) {
      return true;
    } else {
      return false;
    }
  }

  public void setDefaultTarget() {
    clickElement(targetLink);
    waitUntilElementClickable(continueBtn);
    clickElement(continueBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(saveGoalBtn);
  }

  public void unlockDetails() {
    waitUntilElementVisible(unlockDetailsBtn);
    clickElement(unlockDetailsBtn);
  }

  @SneakyThrows
  public void clickDiscoverAndViewPortfolio() {
   waitUntilElementDisappeared(loading);
   Thread.sleep(5*1000);
   String oneTimeInvestment = portfolioInvestmentList.get(3).getText();
   String monthlyInvestment = portfolioInvestmentList.get(5).getText();
   String oneTimeInvestmentNum = oneTimeInvestment.replace("HKD ", "");
   String monthlyInvestmentNum  = monthlyInvestment.replace("HKD ","");
   assertThat(oneTimeInvestmentNum).isEqualTo(oneTimeInvestmentMoney);
   assertThat(monthlyInvestmentNum).isEqualTo(monthlyInvestmentMoney);
   String investmentHorizon = portfolioInvestmentList.get(7).getText();
   String totalInvestmentAmount = portfolioInvestmentList.get(9).getText().replace("HKD ","");
   if (investmentHorizon.contains("Y")) {
    String[] investmentHorizonNum = investmentHorizon.split(" ");
    String investmentYear = investmentHorizonNum[0].replace("Y","");
    String investmentMonth = investmentHorizonNum[1].replace("M","");
    int sumMonth = (Integer.parseInt(investmentYear)*12) + Integer.parseInt(investmentMonth);
    int portfolioSumAmount =
            Integer.parseInt(oneTimeInvestmentNum.replace(",","")) + Integer.parseInt(monthlyInvestmentNum.replace(",","")) * sumMonth;
    assertThat(portfolioSumAmount).isEqualTo(Integer.parseInt(totalInvestmentAmount.replace(",","")));
   }else {
    String investmentMonth = investmentHorizon.replace("M","");
    int portfolioSumAmount =
            Integer.parseInt(oneTimeInvestmentNum.replace(",","")) + Integer.parseInt(monthlyInvestmentNum.replace(",","")) * Integer.parseInt(investmentMonth);
    assertThat(portfolioSumAmount).isEqualTo(Integer.parseInt(totalInvestmentAmount.replace(",","")));
   }

   swipeRightToLeft();
// }
// }
   waitUntilElementVisible(discoveryElement);
   clickElement(discoveryElement);
   scrollUp();
   waitUntilElementVisible(viewPortfolio_discover);
   clickElement(scrollUpToFindElement(viewPortfolio_discover, 3, 3));
  }

  public String getPortfolioName() {
    String portfolioName = "";
    if (isElementVisible(portfolioTab)) clickElement(portfolioTab);
    if (isElementVisible(portfolioNameElement)) portfolioName = portfolioNameElement.getText();
    return portfolioName;
  }

  public boolean checkFundWeight() throws InterruptedException {
   double fundNumberSum = 0.0;
   for (Map.Entry<String,String> fundListNumber : getFundList().entrySet()) {
    fundNumberSum += Double.parseDouble(fundListNumber.getValue().replaceAll("[%]", ""));
   }
   logger.info("fundNumberSum : {}",fundNumberSum);
   return fundNumberSum == 100;
  }

  public Map<String, String> getFundList() throws InterruptedException {

   Map<String,String> getFundLists = new HashMap<>();
    List<MobileElement> fundList = new ArrayList<>();
    List<MobileElement> fundListNum = new ArrayList<>();
    for (int i = 0; i < 2; i++) {
      if (i == 0) {
        fundList.addAll(fundListsNameOne);
        fundListNum.addAll(fundListsNumOne);
    }else{
     Thread.sleep(3000);
     for (int j = 3; j < fundListsNameTwo.size() - 1; j++) {
      fundList.add(fundListsNameTwo.get(j));
     }
       for (int j = 0; j < fundListsNumTwo.size(); j++) {
       fundListNum.add(fundListsNumTwo.get(j));
     }
    }
    scrollUp();
    if (verifyElementExist(fundListName)) {
     break;
    }
   }
   for (int i = 0; i < fundListNum.size() ; i++) {
    getFundLists.put(fundList.get(i).getText(),fundListNum.get(i).getText());
   }
   return getFundLists;
  }

  public String getRiskRate() {
    String getRiskRateTxt = getElementText(riskRate);
    // getMPID = getElementText(moduleID);
    String[] getRiskRateString = getRiskRateTxt.split("Rating ");

    return getRiskRateTxt;
  }

  public int getPortNumber() {
   String getPort = getPortfolioName();
   return Integer.parseInt(getPort.substring(getPort.indexOf('P') + 1, getPort.indexOf('P') + 2 ));
  }

  public String getJumpPagePortfolioName() {
    clickElement(buyButton);
    waitUntilElementVisible(orderTitle);
    waitUntilElementVisible(jumpPagePortfolioName);
    return jumpPagePortfolioName.getText();
  }

  public void reviewOrder() {
    clickElement(nextButton);
    // if (isElementVisible(confirmViewTitle)) {
    //  scrollUpToFindElement(nextButton, 8, 3);
    // nextButton.click();
    // }
  }

  public String clickXGetQuitWarning() {
    Point xBack = xAtReviewConfirmPage.getCenter();
    clickCoordinates(xBack.x, xBack.y);
    waitUntilElementVisible(quitWarning);
    return quitWarning.getText();
  }

  public Boolean clickXBackReachMyTarget() {
    clickElement(xAtReviewConfirmPage);
    return isElementVisible(reachMyTarget);
  }

  public void clickLearnMoreLinkToCreateGoal() {
    scrollUpToFindElement(learMoreLink, 5, 3);
    clickElement(learMoreLink);
    waitUntilElementVisible(goEasyFeatures);
    clickElement(createAGoal);
  }

  public String clickNoNow() {
    clickElement(notNow);
    waitUntilElementVisible(confirmViewTitle);
    return confirmViewTitle.getText();
  }

  public void clickQuitAndSave() {
    clickElement(quitAndSave);
//    waitUntilElementVisible(wealthTitle);
//    return wealthTitle.getText();
  }

  public Boolean getGoalStatus() {
    return isElementVisible(draftStatus);
  }

  public void startBuildInvestHabit() {
    selectGoal(investingHabitLink);
    waitUntilElementVisible(saveGoalBtn);
  }

  public boolean checkInvestHabitYearsInterval(String indexInHorizonSetting, String expectedYears) {

    int i = Integer.parseInt(indexInHorizonSetting);

    List<MobileElement> bimAgeList = getBIMTimeList();
    MobileElement targetTimeElement;

    System.out.println("bim Age List size is " + bimAgeList.size());
    if (i < bimAgeList.size()) {
      targetTimeElement = bimAgeList.get(i);
    } else {
      logFail("failed to get index " + i + "element in invest habit horizon setting", pageName);
      return false;
    }
    setAge(investTimeSlider, targetTimeElement);
    if (System.getProperty("mobile").contains("android")) {
      //    press investingTimeElement and get text
      TouchAction action = new TouchAction(driver);
      action
          .longPress(
              LongPressOptions.longPressOptions()
                  .withElement(ElementOption.element(targetTimeElement)))
          .perform();
      if (YearsTexts.size() < 2) {
        logFail("cannot get years text in invest habit horizon setting", pageName);
        return false;
      }
      // sometimes textviews are [months, 6], sometimes are [6, months]
      String item0 = getElementText(YearsTexts.get(0));
      String item1 = getElementText(YearsTexts.get(1));
      String actualYears;
      if (NumberUtils.isCreatable(item0)) {
        actualYears = item0 + " " + item1;
      } else {
        actualYears = item1 + " " + item0;
      }
      if (!actualYears.equals(expectedYears)) {
        logFail(
            String.format(
                "index: %s, expected years %s , actual years %s ",
                indexInHorizonSetting, expectedYears, actualYears),
            pageName);
        return false;
      }
    }
    if (System.getProperty("mobile").contains("ios")) {
      if (!isElementVisible(
          driver.findElement(By.xpath("//*[@name='" + expectedYears + "']")), 5)) {
        return false;
      }
      setAge(investTimeSlider, bimAgeList.get(0));
    }
    return true;
  }

  public boolean scrollToLastDocumentInFeaturedFund() {
    MobileElement mobileElement = scrollUpToFindElement(fundLastDocument, 7);
    if (mobileElement == null) {
      return false;
    }
    return true;
  }

  public void InsufficientAmountWhenHkdBuyUsdProduct() {
    viewPortfolioOverview();
    waitUntilElementClickable(portfolioTab);
    clickElement(buyButton);
    waitUntilElementClickable(orderTitle);
    clickElement(nextButton);
    waitUntilElementClickable(confirmViewTitle);
    scrollUpToFindElement(nextButton, 20, 1);
    clickElement(nextButton);
  }

  public void InsufficientAmountInYourAccount() {
    waitUntilElementClickable(backToHomeBtn);
    waitUntilElementClickable(addMoneyMessage);
    waitUntilElementClickable(insufficientAmount);
    clickElement(backToHomeBtn);
    waitUntilElementClickable(myGoalsTitle);
  }

  public void clickMyTargetGoal() {
    scrollUpToFindElement(SetMyTargetName, 6, 3);
    clickElement(SetMyTargetName);
  }

  public void clickAFFGoal() {
    scrollUpToFindElement(AFFName, 6, 3);
    clickElement(AFFName);
  }

  public void clickBMIHGoal() {
    scrollUpToFindElement(BMIHName, 6, 3);
    clickElement(BMIHName);
  }

  public void clickGoalNameEditButton() {
    waitUntilElementClickable(GoalNameEditButton);
    clickElement(GoalNameEditButton);
  }

  public void editGoalName(String GoalName) {
    waitUntilElementClickable(editName);
    clearAndSendKeys(editName, GoalName);
    inputEnter(editName);
  }

  public void editGoalSettingName(String GoalName) {
    waitUntilElementClickable(nameInput);
    clearAndSendKeys(nameInput, GoalName);
    inputEnter(nameInput);
  }

  @SneakyThrows
  public void backToWealthMainPageFromGoalPage() {
    Thread.sleep(4000);
    clickElement(headerBackBtn);
  }

  public boolean scrollToFindSetMyTargetName() {
    MobileElement mobileElement = scrollUpToFindElement(SetMyTargetName, 7, 3);
    if (mobileElement == null) {
      return false;
    }
    return true;
  }

  public void GoalHorizon() {
    clickElement(goalhorizonEdit);
  }

  public void clickMyTargetGoalDraft() {
    scrollUpToFindElement(draftSetMyTargetName, 6, 3);
    clickElement(draftSetMyTargetName);
  }

  public void clickAFFGoalDraft() {
    scrollUpToFindElement(draftAFFName, 6, 3);
    clickElement(draftAFFName);
  }

  public void clickBMIHGoalDraft() {
    scrollUpToFindElement(draftBMIHName, 6, 3);
    clickElement(draftBMIHName);
  }

  @SneakyThrows
  public void editSetMyTargetGoal(String targetValue, String time) {
    clickElement(editBtnAtGoalPage);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(editBtnAtSetMyTargetPage);
    waitUntilElementVisible(continueBtn);
    clearAndSendKeys(targetInput, targetValue);
    inputEnter(targetInput);
    scrollUp(scrollPosition.RIGHT);
    setGoalTime(SMTAgeSlider, time);
    Thread.sleep(200);
  }

  public void editFinancialFreedomGoal(String retiredAge) {
    clickElement(editBtnAtGoalPage);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(editBtnAtFreedomPage);
    waitUntilElementVisible(continueBtn);
    int offset = Integer.parseInt(retiredAge) - (maxInvestAge - ageElementsList.size()) - 1;
    MobileElement retiredAgeElement = ageElementsList.get(offset);
    setAge(FFTAgeSlider, retiredAgeElement);
  }

  @SneakyThrows
  public void editMonthlyInvestment(String monthlyValue) {
    waitUntilElementVisible(editMonthlyInvestment);
    clearAndSendKeys(editMonthlyInvestment, monthlyValue);
    inputEnter(editMonthlyInvestment);
    Thread.sleep(200);
  }

  public void backToWealthMainPageFromeditPage() {
    waitUntilElementVisible(viewPortfolioBtn);
    clickElement(headerBackBtn);
    waitUntilElementVisible(saveGoalBtn);
    clickElement(headerBackBtn);
    waitUntilElementDisappeared(saveGoalBtn);
    clickElement(headerBackBtn);
  }

  public void editInvestingHabitGoal(String oneTimeValue, String monthlyValue, String time) {
    clickElement(editBtnAtGoalPage);
    clearAndSendKeys(oneTimeInvestmentInput, oneTimeValue);
    inputEnter(oneTimeInvestmentInput);
    clearAndSendKeys(monthlyInvestmentInput, monthlyValue);
    inputEnter(monthlyInvestmentInput);
    try {
      setInvestingTime(time);
    } catch (Exception e) {
      logger.error("");
    }
  }

  public Boolean checkReviewConfirmOrder() {
    return isElementVisible(confirmViewTitle);
  }

  public Boolean checkDetailEditBtn() {
    return isElementVisible(onTrackEdit);
  }

  public Boolean checkGoalHorizon() {
    return isElementVisible(goalHorizon1Y);
  }

  public Boolean checkLabel50_65() {
    return isElementVisible(label50_65);
  }

  public Boolean checkMP5() {
    return isElementVisible(MPID);
  }

  public void clickBtnAtGoalPage() {
    waitUntilElementClickable(editBtnAtGoalPage);
    clickElement(editBtnAtGoalPage);
  }

  public void scrollToFindName(String name) {
    MobileElement mobileElement = null;
    if (System.getProperty("mobile").contains("ios"))
      mobileElement =
          scrollUpToFindElement(By.xpath("//*[starts-with(@name, '" + name + "')]"), 7, 3);
    if (System.getProperty("mobile").contains("android"))
      mobileElement =
          scrollUpToFindElement(By.xpath("//*[starts-with(@text, '" + name + "')]"), 7, 3);
    clickElement(mobileElement);
    clickElement(editBtn);
  }

  public String getWarningMsg() {
    return warningMsg.getText();
  }

  public boolean isExceedMaxAmountErrorMsgExist() {
    return exceedMsg.isDisplayed();
  }

  public void addTargetGoalWithInvestmentPlan(
      String targetValue, String time, String oneTimeValue, String monthlyValue, String name) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    setTargetGoal(targetValue, time, name);
    if (null != targetValue)
      assertThat(getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(getSMTAge()).isEqualTo(time);
    clickContinue();
    setInvestmentPlan(oneTimeValue, monthlyValue);
    saveGoal();
  }

  public void iClickBMIHOntrack() {
    waitUntilElementClickable(myGoalsTitle);
    scrollUpToFindElement(BMIHOntrackBtn, 20, 2);
    clickElement(BMIHOntrackBtn);
  }

  public void iClickSMTOntrack() {
    waitUntilElementClickable(myGoalsTitle);
    scrollUpToFindElement(SMTOntrackBtn, 20, 2);
    clickElement(SMTOntrackBtn);
  }

  public void iClickAFFOntrack() {
    waitUntilElementClickable(myGoalsTitle);
    scrollUpToFindElement(AFFOntrackBtn, 20, 2);
    clickElement(AFFOntrackBtn);
  }

  public void iClickDeleteAndVerifySMTDraft() {
    waitUntilElementVisible(myGoalsTitle);
    scrollUpToFindElement(SMTDraftBtn, 8, 2);
    clickElement(SMTDraftBtn);
    waitUntilElementVisible(deleteBtn);
    clickElement(deleteBtn);
    waitUntilElementVisible(confirmBtn);
    clickElement(confirmBtn);
  }

  public void iClickDeleteAndVerifyBMIHDraft() {
    scrollDownToFindElement(myGoalsTitle, 8, 3);
    scrollDownToFindElement(myGoalsTitle, 8, 3);
    scrollUpToFindElement(BMIHDraftBtn, 8, 2);
    clickElement(BMIHDraftBtn);
    waitUntilElementVisible(deleteBtn);
    clickElement(deleteBtn);
    waitUntilElementVisible(confirmBtn);
    clickElement(confirmBtn);
  }

  public void iClickDeleteAndVerifyAFFDraft() {
    scrollDownToFindElement(myGoalsTitle, 8, 3);
    scrollUpToFindElement(AFFDraftBtn, 8, 2);
    clickElement(AFFDraftBtn);
    waitUntilElementVisible(deleteBtn);
    clickElement(deleteBtn);
    waitUntilElementVisible(confirmBtn);
    clickElement(confirmBtn);
  }

  public Boolean iVerifySMTRemoved() {
    if (verifyElementExist(SMTDraftBtn)) {
      return false;
    }
    return true;
  }

  public Boolean iVerifyBMIHRemoved() {
    if (verifyElementExist(BMIHDraftBtn)) {
      return false;
    }
    return true;
  }

  public Boolean iVerifyAFFemoved() {
    if (verifyElementExist(AFFDraftBtn)) {
      return false;
    }
    return true;
  }

  public String getOneTimeText() {
    return getElementText(oneTimeText);
  }

  public String getInvestmentPlan() {
    oneTimeInvestmentMoney = getElementText(investmentPlan);
    return oneTimeInvestmentMoney;
  }

  public String getMonthlyInvestment() {
    monthlyInvestmentMoney = getElementText(monthlyInvestment);
    return monthlyInvestmentMoney;
  }

  @SneakyThrows
  public void clickTargetGoal(String targetValue, String time) {
    waitUntilElementVisible(targetInput, 60);
    clearAndSendKeys(targetInput, targetValue);
    inputEnter(targetInput);
    clickElement(SMTAgeSlider);
    scrollUp(scrollPosition.RIGHT);
    setGoalTime(SMTAgeSlider, time);
    Thread.sleep(200);
  }

  public void getActualValue() {
    monthlyInvestmentMoney = getMonthlyInvestment();
    oneTimeInvestmentMoney = getOneTimeText();
  }

  public String getGoalHorizon() {
    return getElementText(goaHorizon);
  }

  public Boolean getText() {
    waitUntilElementVisible(viewPortfolioBtn, 20);
    List<String> list = new ArrayList<String>();
    Boolean result = false;
    for (int i = 0; i < containsHKD.size(); i++) {
      String money = containsHKD.get(i).getText();
      String regEx = "[^0-9]";
      Pattern p = Pattern.compile(regEx);
      Matcher m = p.matcher(money);
      money = m.replaceAll("").trim();
      list.add(money);
    }
    if (list.contains(monthlyInvestmentMoney.replace(",", ""))
        && list.contains(oneTimeInvestmentMoney.replace(",", ""))) {
      return result = true;
    }
    return result;
  }

  public void clickDraftSetMyTargetName() {
    scrollUpToFindElement(draftSetMyTargetName, 10, 3);
    clickElement(draftSetMyTargetName);
  }

  @SneakyThrows
  public boolean determineTheAmountOf() {
    Thread.sleep(2000);
    List<String> list = new ArrayList<String>();
    Boolean result = false;
    scrollUp();
    for (int a = 1; a != 0; a++) {
      for (int i = 0; i < containsHKD.size(); i++) {
        String money = containsHKD.get(i).getText();
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(money);
        money = m.replaceAll("").trim();
        list.add(money);
      }
      if (list.contains(monthlyInvestmentMoney.replace(",", ""))
          && list.contains(oneTimeInvestmentMoney.replace(",", ""))) {
        return result = true;
      } else if (ElementExist(By.xpath("//android.widget.TextView[@text='Next']")) == true) {
        return result = false;
      }
      scrollUp();
    }
    return result;
  }

  public boolean ElementExist(By locator) {
    try {
      driver.findElement(locator);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public void setFinancialFreedom(String retiredAge) {
    waitUntilElementVisible(continueBtn);
    int offset = Integer.parseInt(retiredAge) - (maxInvestAge - ageElementsList.size()) - 1;
    MobileElement retiredAgeElement = ageElementsList.get(offset);
    setAge(FFTAgeSlider, retiredAgeElement);
  }

  public void pullDownToVerifyTheOriginalData() {
    viewPortfolioOverview();
    waitUntilElementClickable(portfolioTab);
    clickElement(buyButton);
    waitUntilElementClickable(orderTitle);
    clickElement(nextButton);
    waitUntilElementClickable(confirmViewTitle);
  }

  @SneakyThrows
  public void setInvesting(String oneTimeValue, String monthlyValue, String time,String screenShotName) {
//    selectGoal(investingHabitLink);
    monthlyInvestmentMoney = oneTimeValue;
    oneTimeInvestmentMoney = monthlyValue;
    clearAndSendKeys(oneTimeInvestmentInput, oneTimeValue);
    inputEnter(oneTimeInvestmentInput);
    clearAndSendKeys(monthlyInvestmentInput, monthlyValue);
    inputEnter(monthlyInvestmentInput);
    Thread.sleep(5000);
    scrollUp();
    try {
      setInvestingTime(time);
    } catch (Exception e) {
      logger.error("");
    }
   takeScreenshot(screenShotName);
  }

  public void iGetWealthCurrentWealth() {
    waitUntilElementVisible(wealthCurrentWealth, 30);
   mainPageCurrentWealth = getElementText(wealthCurrentWealth);
  }

  @SneakyThrows
  public void iClickConfirmAndSlideToConfirm() {
   if(verifyElementExist(nextButton)){
    clickElement(nextButton);
   }
   waitUntilElementClickable(buyButton);
   clickElement(buyButton);
   if (verifyElementExist(nextButton)) {
    waitUntilElementVisible(nextButton);
    clickElement(nextButton);
   }
    waitUntilElementVisible(confirmViewTitle);
    scrollUpToFindElement(nextButton, 8, 3);
    clickElement(nextButton);
    scrollUpToFindElement(slideToConfirm, 8, 3);
    radioBtn2.click();
    Thread.sleep(1000);
    // top-left
    int x = slideToConfirm.getLocation().x;
    int y = slideToConfirm.getLocation().y;
    // size
    int width = slideToConfirm.getRect().width;
    int x_start = x + 50;
    int x_end = x + width;
    int y_co = y + 2;
    TouchAction action =
        new TouchAction(driver)
            .longPress(PointOption.point(x_start, y_co))
            .moveTo(PointOption.point(x_end, y_co))
            .release();
    action.perform();
    Thread.sleep(2000 * 10);
  }

  @SneakyThrows
 public boolean iVerifyCurrentWealthUpdate(){
   Thread.sleep(3000);
  if (getElementText(wealthCurrentWealth).equals(mainPageCurrentWealth)){
   return false;
  }
  return true;
 }

  public void enterAmount(String quantity) {
    waitUntilElementVisible(inputBtn);
    andSendKeys(inputBtn, quantity);
  }

  public void setSellingPortfolioName2Context(ScenarioContext context) {
    context.setScenarioData("portfolioName", getElementText(sellingPortfolioName));
  }

  public void clickProceedBtn() {
    waitUntilElementVisible(ProceedBtn);
    clickElementCenter(ProceedBtn);
  }

  @SneakyThrows
  public void clickSellBtn() {
    Thread.sleep(5000);
    clickElement(SellBtn);
  }

  public void clickNextBtn() {
    waitUntilElementVisible(nextBtn);
    clickElement(nextBtn);
  }



//  public void clickSellAllConfirm() {
//    waitUntilElementVisible(SellAllConfirmBtn);
//    clickElement(SellAllConfirmBtn);
//  }

  public void clickMyTargetOnTrack() {
    scrollUpToFindElement(myTargetOnTrack, 20, 1);
    clickElement(myTargetOnTrack);
  }

  public void clickBuildMyInvestingHabitOnTrack() {
    scrollUpToFindElement(buildMyInvestingHabitOnTrack, 20, 1);
    clickElement(buildMyInvestingHabitOnTrack);
  }

  public void clickAchieveFinancialFreedomOnTrack() {
    scrollUpToFindElement(achieveFinancialFreedomOnTrack, 20, 1);
    clickElement(achieveFinancialFreedomOnTrack);
  }

 @SneakyThrows
 public void clickYesBtn() {
  Thread.sleep(3*1000);
  if (verifyElementExist(yesBtn)) {
   waitUntilElementVisible(yesBtn, 10);
   Thread.sleep(3*1000);
   assertThat(moreThanMonthlyIncome.getText().contains("is more than your monthly income")).isTrue();
   clickElement(yesBtn);
  }
 }

  public String getSuccessRate() {
    waitUntilElementVisible(SuccessRate, 60);
    return getElementText(SuccessRate);
  }

  public String geTargetWealth() {
    return getElementText(targetWealth);
  }

  public ArrayList<String> getPortfolioFundList() {
    ArrayList<String> FundList = new ArrayList<>();
    for (MobileElement ele : portfolioFundList) {
      if (getElementText(ele).contains("Fund list")) {
        continue;
      }
      // only keep letters and brace.
      FundList.add(getElementText(ele).replaceAll("[^a-zA-Z ()-]", "").trim());
    }
    return FundList;
  }

  public SellOrder getSellOrderInfo() {
    // IOS can load all elements of a scroll layout, so can retrieve all text on a for loop
    // Android can only load the displayed elements, so cannot use this method to get info.
    waitUntilElementVisible(estimatedSellAmountTitle, 30);
    SellOrder sellOrder = new SellOrder();

    sellOrder.setPortfolioName(getElementText(portfolioName));

    String[] amountTexts = getElementText(estimatedSellAmountValues.get(0)).split(" ");
    sellOrder.setEstimatedSellAmount(amountTexts[amountTexts.length - 1]);

    String[] settleInTexts = getElementText(settleInValue.get(0)).split(" ");
    sellOrder.setSettleIn(settleInTexts[settleInTexts.length - 1]);

    List<SellOrder.Fund> funds = new ArrayList<>();
    for (int i = 0; i < fundNameValues.size(); i++) {
      SellOrder.Fund fund = new SellOrder.Fund();

      fund.setFundName(getElementText(fundNameValues.get(i)).replace("Fund Name ", ""));

      // the first estimatedSellAmount is for sellOrder , not fund, so need to plus 1 here.
      String[] fundAmountTexts = getElementText(estimatedSellAmountValues.get(i + 1)).split(" ");
      fund.setEstimatedSellAmount(fundAmountTexts[fundAmountTexts.length - 1]);

      fund.setIndicativeNAV(
          getElementText(indicativeNAVValues.get(i)).replace("Indicative NAV ", ""));

      fund.setNumberOfUnits(
          getElementText(numberOfUnitsValues.get(i)).replace("Number of units ", ""));

      funds.add(fund);
    }
    sellOrder.setFunds(funds);

    //    logger.info(sellOrder.getPortfolioName());
    //    logger.info(sellOrder.getEstimatedSellAmount());
    //    logger.info(sellOrder.getSettleIn());
    //    for (SellOrder.Fund fund : sellOrder.getFunds()) {
    //      logger.info(fund.getFundName());
    //      logger.info(fund.getEstimatedSellAmount());
    //      logger.info(fund.getIndicativeNAV());
    //      logger.info(fund.getNumberOfUnits());
    //    }

    return sellOrder;
  }

  public Boolean verifyingOrderData() {
    waitUntilElementVisible(orderInformationd, 30);
    Boolean result = false;
    Boolean str = false;
    List<String> list = new ArrayList<String>();
    for (int a = 1; a != 0; a++) {
      for (int i = 0; i < orderInformation.size(); i++) {
        list.add(orderInformation.get(i).getText());
      }
      if (ElementExist(
          By.xpath("//android.widget.TextView[contains(@text,'Terms and Conditions')]"))) {
        break;
      }
      scrollUp();
    }
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }

    for (int i = 0; i < list.size() - 1; i++) {
      int size = i;
      if (list.get(i).equals("Estimated sell amount")) {
        List<String> decimalPoint =
            Arrays.asList(StringUtils.delimitedListToStringArray(list.get(size + 1), "."));
        if (decimalPoint.size() == 2) {
          if (decimalPoint.get(1).length() == 2) {
            str = true;
          } else if (decimalPoint.get(1).length() != 2) {
            str = false;
            break;
          }
        }
      }
    }
    return result;
  }

  public Boolean Comfortable(String successRate, String successTex) {
    Boolean success = false;
    Double i = Double.valueOf(successRate);
    if (i <= 50) {
      if (successTex.contains("Off-track")) {
        return success = true;
      }
    } else if (i > 50 && i <= 65) {
      if (successTex.contains("Comfortable")) {
        return success = true;
      }
    } else if (i > 65 && i <= 80) {
      if (successTex.contains("Very comfortable")) {
        return success = true;
      }
    } else if (i > 80 && i <= 100) {
      if (successTex.contains("Very very comfortable")) {
        return success = true;
      }
    }
    return success;
  }

  public void iClickViewNewPortfoloi() {
    waitUntilElementVisible(viewNewPortfolioBtn);
    clickElement(viewNewPortfolioBtn);
  }

  @SneakyThrows
 public void clickGoalWithGoalName(String goalname) {
    Thread.sleep(2000);
    String goalXpath;
    goalXpath ="//android.widget.TextView[@text='" + goalname + "')]";
    MobileElement ele = driver.findElement(By.xpath("//*[@text='" + goalname + "')]"));
    if (verifyElementExist(ele)){
      clickElement(ele);
    }else{
      scrollUpToFindElement(ele, 8, 10);
      clickElement(ele);
    }
 }

}
