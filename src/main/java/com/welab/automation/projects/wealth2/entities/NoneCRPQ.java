package com.welab.automation.projects.wealth2.entities;

import lombok.Getter;
import lombok.Setter;

public class NoneCRPQ {

  @Getter @Setter private String declarationemployeeofintermediary;

  @Getter @Setter private String qualificationinformation;

  @Getter @Setter private String investmenthorizon;

  @Getter @Setter private String investmentproductknowledgeandexperience;

  @Getter @Setter private String networth;

  public NoneCRPQ(String declarationemployeeofintermediary, String qualificationinformation, String investmenthorizon, String investmentproductknowledgeandexperience, String networth) {
    this.declarationemployeeofintermediary = declarationemployeeofintermediary;
    this.qualificationinformation = qualificationinformation;
    this.investmenthorizon = investmenthorizon;
    this.investmentproductknowledgeandexperience = investmentproductknowledgeandexperience;
    this.networth = networth;
  }
}
