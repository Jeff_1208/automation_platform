package com.welab.automation.projects.wealth2.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import static com.welab.automation.framework.utils.Utils.logFail;

public class CommonPage extends AppiumBasePage {

  private String pageName = "Common Page";
  private String upgradeText = "Got it";
  private String cancelText = "Cancel";
  private int customWait = 50;
  private int toastWait = 5;
  private int waitMillSeconds = 3000;
  private String defaultInvestmentHorizon;
  private String defaultKnowledgeAndExperience;

  GoalSettingPage goalSettingPage;
  public CommonPage(){
    super.pageName = this.pageName;
    goalSettingPage = new GoalSettingPage();
  }
  public static final String skipRootString =
          "//android.widget.TextView[contains(@text,'Jailbreak')]/../android.widget.Switch";

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']/../android.widget.TextView[1]")
  private MobileElement devPanelBackBtn;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
  })
  private MobileElement loginTxt;

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']")
  private MobileElement devPanelTitle;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='我明白啦']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Got it''"),
          @iOSXCUITBy(iOSNsPredicate = "name == '我明白啦'"),
  })
  private MobileElement upgradeOKBtn;

  @iOSXCUITFindBy(accessibility = "-btn-title")
  @AndroidFindBy(accessibility = "-btn")
  private MobileElement gotItBtn;

  @iOSXCUITFindBy(accessibility = "Select one you want to explore now!")
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Select one you want to explore now!']")
  private MobileElement exploreText;

  @iOSXCUITFindBy(accessibility = "btnGoalCard")
  @AndroidFindBy(accessibility = "btnGoalCard")
  private MobileElement goalCardBtn;

  @Getter
  @AndroidFindBy(
          xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
  private MobileElement eyeBtn;

  @AndroidFindBy(
          xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]")
  private MobileElement rocketButton;

  @AndroidFindBy(
          xpath = "//android.widget.TextView[contains(@text,'Root')]/../android.widget.Switch")
  private MobileElement skipRoot;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @AndroidFindBy(accessibility = "GoFlexi, tab, 4 of 5")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoFlexi, tab, 4 of 5'")
  private MobileElement loanTab;

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "Wealth, tab, 5 of 5"),
          @AndroidBy(accessibility = "GoWealth, tab, 5 of 5")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Wealth, tab, 5 of 5'"),
          @iOSXCUITBy(iOSNsPredicate = "label == \"GoWealth, tab, 5 of 5\""),
  })
  private MobileElement wealthTab;

  @iOSXCUITFindBy(xpath = "//*[contains(@name ,'現時財富總值')]")
  private MobileElement wealthCurrentAmount;

  @AndroidFindBy(
          xpath = "//android.widget.TextView[contains(@text, 'Hi,')]/../../android.view.ViewGroup[3]")
  @iOSXCUITFindBy(
          xpath =
                  "//XCUIElementTypeStaticText[starts-with(@label, 'Hi,')]/../../../../XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
  private MobileElement MyAccountBtn;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Learn more']")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Learn more']")
  private MobileElement learnMoreBtn;

  @AndroidFindBy(xpath = "//*[@text='Skip to open an account']")
  @iOSXCUITFindBy(accessibility = "skipTipText")
  private MobileElement skipToOpenAccountBtn;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  private MobileElement welcomeMutFundTitle;

  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  private MobileElement welcomeMutFundBtn;

  @AndroidFindBy(xpath = "//*[@text='Update and verify personal info']")
  @iOSXCUITFindBy(accessibility = "PROCESS_GUIDE-title")
  private MobileElement updateVerifyPersonalInfoTip;

  @AndroidFindBy(xpath = "//*[@text='Next']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement nextBtn;

  // used for exception showing up.
  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement exceptionCancelBtn;

  @AndroidFindBy(xpath = "//*[@text='Open an account']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'openAccountBtn-title'")
  private MobileElement openAccountBtn;

  @Getter
  @AndroidFindAll({@AndroidBy(xpath = "//*[@content-desc='btnClose']/android.view.ViewGroup[1]")})
  @iOSXCUITFindBy(xpath = "//*[@name='btnClose']/XCUIElementTypeOther[1]")
  private MobileElement xIcon;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnBackContainer\"]"),
          @AndroidBy(accessibility = "btnBack"),
          @AndroidBy(accessibility = "btnBackContainer")
  })
  @iOSXCUITFindBy(xpath = "//*[@name='btnBack']/XCUIElementTypeOther[1]")
  private MobileElement backIcon;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Cancel')]")
  @AndroidFindBy(xpath = "//*[starts-with(@text, 'Cancel')]")
  private MobileElement cancelBtn;

  @AndroidFindBy(
          xpath =
                  "//android.widget.TextView[@text='Check your order status, client risk profiling, promotion, tutorial, and more']/..")
  @iOSXCUITFindBy(
          iOSNsPredicate =
                  "name == 'Wealth Centre Check your order status, client risk profiling, promotion, tutorial, and more'")
  private MobileElement wealthCenterMenu;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-title']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement thanksTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-desc']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-desc'")
  private MobileElement pendingDesc;

  @AndroidFindBy(xpath = "//*[@text='Wealth Centre']")
  private MobileElement wealthCentre;

  @AndroidFindBy(xpath = "//*[@text='Review investment account profile']")
  private MobileElement reviewInvestmentAccountProfile;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnBackContainer\"]")
  private MobileElement backBtn;

  @AndroidFindBy(
          xpath =
                  "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-btn-toggle\"]/../android.view.ViewGroup")
  private MobileElement investmentHorizonEditBtn;

  @AndroidFindBy(
          xpath =
                  "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-radio-btn-1\"]/android.view.ViewGroup")
  private MobileElement upTo3Years;

  @AndroidFindBy(
          xpath =
                  "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-btn-toggle\"]/../android.view.ViewGroup")
  private MobileElement multipleChoiceEdit;

  @AndroidFindBy(
          xpath =
                  "//android.view.ViewGroup[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-0\"]/android.view.ViewGroup")
  private MobileElement multipleChoiceFirst;

  @AndroidFindBy(
          xpath =
                  "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-1\"]/android.view.ViewGroup")
  private MobileElement multipleChoiceSecond;

  @AndroidFindBy(
          xpath =
                  "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-6\"]/android.view.ViewGroup")
  private MobileElement multipleChoicesSeventh;

  @AndroidFindBy(xpath = "//*[contains(@text,'We will automatically')]")
  private MobileElement weWillAutomatically;

  @AndroidFindBy(
          xpath = "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-btn-confirm\"]")
  private MobileElement OKBtn;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-desc-0")
  private MobileElement defaultHorizon;

  @AndroidFindBy(xpath = "//*[@text='Net worth']")
  private MobileElement netWorth;

  @AndroidFindBy(xpath = "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-desc-0\"]")
  private MobileElement knowledgeAndExperience;

  @AndroidFindBy(xpath = "//*[@text='Transaction']")
  private MobileElement transaction;

  @AndroidFindBy(xpath = "//*[@text='Customer Risk Profiling Questionnaire']")
  private MobileElement clientRiskProfiling;

  @AndroidFindBy(xpath = "//*[@text='Community']")
  private MobileElement community;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-0-subTitle")
  private MobileElement question1;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-subTitle")
  private MobileElement question2;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-subTitle")
  private MobileElement question3;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-3-subTitle")
  private MobileElement question4;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'OCR')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过OCR扫描\"])[2]")
  private MobileElement skipORC;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Rebind Device')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Rebind Device\"])[2]")
  private MobileElement skipRebindDevice;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Add New Goals']"),
          @AndroidBy(xpath = "//*[@text='設立新目標']"),
  })
  private MobileElement addNewGoal;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Confirm']"),
          @AndroidBy(xpath = "//*[@text='確定']")
  })
  private MobileElement confirmBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc=\"welcome-screen\"]/android.view.ViewGroup[2]/android.widget.TextView"),
  })
  private MobileElement welcomeLogin;

  @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[6]")
  private MobileElement gpsElement;

  @AndroidFindBy(xpath = "//*[@text='Please enter latitude']")
  private MobileElement latitude;

  @AndroidFindBy(xpath = "//*[@text='Please enter longitude']")
  private MobileElement longitude;

  @AndroidFindBy(xpath = "//*[@text='Mock Wealth GPS']")
  private MobileElement mockWealthGPS;

  @AndroidFindBy(xpath = "//*[@text='save']")
  private MobileElement saveBtn;

  @AndroidFindBy(xpath = "//*[@text='Mock Wealth GPS']/following-sibling::android.view.ViewGroup[1]")
  private MobileElement locationXCoin;

  public void enableSkipRoot() throws InterruptedException {
    By skipRootBy = By.xpath(skipRootString);
    Thread.sleep(3000);
    waitUntilElementClickable(eyeBtn);
    clickElement(eyeBtn);
    //waitUntilElementVisible(rocketButton);
    //clickElement(rocketButton);
    Thread.sleep(2000);
    clickByPicture2("src/main/resources/images/skip/rocket.png",50,50);
    waitUntilElementVisible(devPanelTitle);
    scrollUpToFindElement(skipRootBy, 6, 3);
    Thread.sleep(2000);
//    setSwithToChecked(skipORC);
    setSwithToChecked(skipRebindDevice);
//    setSwithToChecked(skipRoot);
//    setSwithToChecked(skipRoot);
    clickElement(devPanelBackBtn);
  }

  public void moveEyeBtnToTopRight() {
    int width = driver.manage().window().getSize().width;
    TouchAction action = new TouchAction(driver);
    action
            .press(PointOption.point(eyeBtn.getCenter().getX(), eyeBtn.getCenter().getY()))
            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
            .moveTo(PointOption.point(width-80, 0))
            .release()
            .perform();
  }

  @SneakyThrows
  public void checkUpgrade() {
    Thread.sleep(5000);
//    clickElement(loginTxt);
    if (isShow(upgradeOKBtn, 5)) {
      clickElement(upgradeOKBtn);
    }
//    if (isShow(welcomeLogin, 5)) {
//      Thread.sleep(3000);
//      clickElement(welcomeLogin);
//    }
  }

  public void clickContactWarningLogin() throws InterruptedException {
    Thread.sleep(3000);
    if (isElementDisplayed(wealthTab, customWait).isDisplayed()) {
      Thread.sleep(3000);
      if (isElementDisplayed(contactWarningCancel, cancelText)) {
        waitUntilElementClickable(contactWarningCancel);
        contactWarningCancel.click();
      }
    }
  }

  public void clickContactWarningCommon() {
    if (isElementDisplayed(contactWarningCancel, cancelText, 3)) {
      waitUntilElementClickable(contactWarningCancel);
      clickElement(contactWarningCancel);
    }
  }

  public void gotoLoan() throws InterruptedException {
    waitUntilElementVisible(loanTab);
    loanTab.click();
    Thread.sleep(waitMillSeconds);
  }

  public void gotoWealth() throws InterruptedException {
    if (verifyElementExist(confirmBtn)){
      clickElement(confirmBtn);
    }
    waitUntilElementVisible(wealthTab);
    wealthTab.click();
    Thread.sleep(waitMillSeconds);
  }

  // GoalsNSettings: A_01_Landing to CA_01_SideMenu
  public void gotoMyAccount() {
    waitUntilElementVisible(MyAccountBtn);
    clickElement(MyAccountBtn);
    clickContactWarningCommon();
  }

  // GoalsNSettings: CA_01_SideMenu to CB_01_WealthCentre
  public void gotoWealthCenter() {
    waitUntilElementClickable(wealthCenterMenu);
    clickElement(wealthCenterMenu);
  }

  @SneakyThrows
  public void skipGuidance() {
    if (verifyElementExist(gotItBtn)) {
      waitUntilElementClickable(gotItBtn);
      Thread.sleep(200);
      clickElement(gotItBtn);
      waitUntilElementVisible(goalCardBtn);
      clickXIcon();
    }
  }

  // This is a workaround to go to versify personal information page
  // should not use this method if account generation script is ready.
  public void skipToOpenAccount() {
    waitUntilElementVisible(learnMoreBtn);
    clickElement(learnMoreBtn);
    waitUntilElementVisible(skipToOpenAccountBtn);
    clickElement(skipToOpenAccountBtn);
    waitUntilElementVisible(welcomeMutFundTitle);
    scrollUp();
    clickElement(welcomeMutFundBtn);
    waitUntilElementVisible(updateVerifyPersonalInfoTip);
    // if no delay, the "welcome mutual fund" might be generated again after click "Next"
    // sleep 200 ms to avoid this.
    try {
      Thread.sleep(500);
    } catch (Exception e) {
      logFail(e.getMessage(), pageName);
    }
    clickElement(nextBtn);
    // currently, exception might pop up during the step: Verify personal information.
    // check and cancel the exception page to continue
    checkException();
  }

  public void checkException() {
    if (verifyElementExist(contactWarningCancel)) {
      waitUntilElementVisible(contactWarningCancel);
      clickElement(contactWarningCancel);
    }
  }

  @SneakyThrows
  public void openAccount(String screenShotName) {
    waitUntilElementClickable(openAccountBtn);
    clickElement(openAccountBtn);
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void clickXIcon() {
    Thread.sleep(2000);
    waitUntilElementClickable(xIcon);
    clickElement(xIcon);
  }

  @SneakyThrows
  public void clickBackIcon() {
      Thread.sleep(1000 * 3);
    clickElement(backIcon);
    Thread.sleep(1000 * 3);
  }

  // check the upload page is loaded
  public boolean checkUploadElement() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  /**
   * A_06_Pending: Waiting for uploaded documents approval, the account not opened
   *
   * @return
   */
  public String getPendingApprovalDesc() {
    try {
      Thread.sleep(1000 * 2);
      waitUntilElementVisible(thanksTitle);
      WebElement pendingTxt = waitUntilElementVisible(pendingDesc);
      return pendingTxt.getText();
    } catch (Exception e) {
      return "";
    }
  }

  // onboarding: H_02_Landing
  @SneakyThrows
  public boolean checkWealthLanding() {
    Thread.sleep(1000 * 3);
    if (isElementDisplayed(openAccountBtn) != null) {
      return true;
    }
    return false;
  }

  @SneakyThrows
  public boolean checkNewGoal() {
    Thread.sleep(2000);
    return verifyElementExist(addNewGoal);
  }

  @SneakyThrows
  public boolean checkWealthCentreDisabled() {
    waitUntilElementVisible(wealthCenterMenu);
    clickElement(wealthCenterMenu);
    Thread.sleep(3000);
    // wealthCenterMenu is still displayed after click this menu.
    return isElementVisible(wealthCenterMenu);
  }

  public boolean checkGuidanceElementShowing() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  public void changeAccount(String account, String password) throws IOException {
    List<String> list;
    try {
      logger.info("switched account");
      File file = new File(GlobalVar.RN_ACCOUNT_CONFIG_FILE);
      list = FileUtils.readLines(file, "UTF-8");
      for (int i = 0; i < list.size(); i++) {
        String line = list.get(i);
        if (line.trim().startsWith("TEST_ACCOUNT")) {
          String[] arr = line.split("'");
          if (!account.equals(arr[1])) {

            String str = line.replace(arr[1], account);
            list.remove(i);
            list.add(i, str);
          }
        }
        if (line.trim().startsWith("TEST_PASSWORD")) {
          String[] arr = line.split("'");
          if (!arr[1].equals(password)) {
            String str = line.replace(arr[1], password);
            list.remove(i);
            list.add(i, str);
          }
        }
      }
      FileUtils.writeLines(file, "UTF-8", list, false);
      logger.info("switched account to {}", account);
    } catch (IOException e) {
      logger.error("write file failed");
    }
  }

  public void clickCancelBtn() {
    waitUntilElementClickable(cancelBtn);
    clickElement(cancelBtn);
  }

  public void iClickWealthCentre() {
    waitUntilElementVisible(wealthCentre);
    clickElement(wealthCentre);
  }

  public void iClickReviewInvestmentAccountProfile() {
    clickElement(reviewInvestmentAccountProfile);
  }

  public void iClickSingleChoiceEditButton() {
    waitUntilElementVisible(investmentHorizonEditBtn);
    clickElement(investmentHorizonEditBtn);
  }

  public void iGetDefaultInvestmentHorizon() {
    defaultInvestmentHorizon = getElementText(defaultHorizon);
  }

  public void iGetDefaultKnowledgeAndExperience() {
    defaultKnowledgeAndExperience = getElementText(knowledgeAndExperience);
  }

  public boolean iVerifyInvestmentHorizonUpdate() {
    if (getElementText(defaultHorizon).equals("Up to 3 years")) {
      return true;
    }
    return false;
  }

  public boolean iVerifyInvestmentHorizonNotChange() {
    if (getElementText(defaultHorizon).equals(defaultInvestmentHorizon)) {
      return true;
    }
    return false;
  }

  public boolean iVerifyKnowledgeAndExperienceNotChange() {
    if (getElementText(knowledgeAndExperience).equals(defaultKnowledgeAndExperience)) {
      return true;
    }
    return false;
  }

  public void iChooseUpTo3Years() {
    waitUntilElementVisible(upTo3Years);
    clickElement(upTo3Years);
  }

  public void iClickMutipleChoiceEditButton() {
    clickElement(multipleChoiceEdit);
    scrollUpToFindElement(weWillAutomatically, 8, 3);
    clickElement(multipleChoiceFirst);
    clickElement(multipleChoiceSecond);
    clickElement(multipleChoicesSeventh);
  }

  public void iClickOKButton() {
    scrollDownToFindElement(OKBtn, 8, 2);
    clickElement(OKBtn);
  }

  @SneakyThrows
  public boolean iCanSeeMutipleChoice() {
    scrollUpToFindElement(netWorth, 10, 3);
    Thread.sleep(2 * 1000);
    if ((verifyElementExist(multipleChoiceFirst) && verifyElementExist(multipleChoiceSecond))
            || (verifyElementExist(multipleChoiceFirst) && verifyElementExist(multipleChoicesSeventh))
            || (verifyElementExist(multipleChoiceSecond)
            && verifyElementExist(multipleChoicesSeventh))) {
      return true;
    }
    return false;
  }

  public boolean iCanSeeThreesessionDivided() {
    if (verifyElementExist(transaction)
            && verifyElementExist(clientRiskProfiling)
            && verifyElementExist(community)) {
      return true;
    }
    return false;
  }

  @SneakyThrows
  public boolean iReviewCRPQQuestion() {
    Thread.sleep(2 * 1000);
    if ((getElementText(question1).equals("What is your education level?"))
            && (getElementText(question2).equals("What is your intended investment horizon*?"))
            && (getElementText(question3)
            .equals("I have knowledge and/or experience in the last 3 years in this product:"))
            && (getElementText(question4).equals("What is your total net worth*?"))) {
      return true;
    }
    return false;
  }

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text = 'Enter your mobile security key']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text = '請輸入6位數字嘅流動保安編碼。']"),
  })
  private MobileElement enterKey;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text = 'Forgot mobile security key?']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text = '忘記流動保安編碼？']"),
  })
  private MobileElement forgotKey;

  public void waiteSendMsk() {
    if (isShow(enterKey, 5) && isShow(forgotKey, 5)) {
      sendMsk();
    }
  }

  public boolean isAndroid() {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      return true;
    }
    return false;
  }

  public boolean isIOS() {
    if (System.getProperty("mobile").equalsIgnoreCase("ios")) {
      return true;
    }
    return false;
  }
  public void checkLogin() {
    if (isShow(loginTxt, 5)) {
      clickElement(loginTxt);
    }
  }

  @SneakyThrows
  public void openWealthPage(String screenShotName) {
    waitUntilElementVisible(wealthTab);
    clickElement(wealthTab);
    Thread.sleep(3000);
    takeScreenshot(screenShotName);
  }

  @SneakyThrows
  public void openWealthPageIOS(String screenShotName) {
    waitUntilElementVisible(wealthTab);
    clickElement(wealthTab);
    waitUntilElementVisible(wealthCurrentAmount);
    Thread.sleep(5000);
    takeScreenshot(screenShotName);
  }

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"0\"]")
  private MobileElement softKeyNumber0;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"1\"]")
  private MobileElement softKeyNumber1;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"2\"]")
  private MobileElement softKeyNumber2;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"3\"]")
  private MobileElement softKeyNumber3;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"4\"]")
  private MobileElement softKeyNumber4;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"5\"]")
  private MobileElement softKeyNumber5;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"6\"]")
  private MobileElement softKeyNumber6;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"7\"]")
  private MobileElement softKeyNumber7;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"8\"]")
  private MobileElement softKeyNumber8;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"9\"]")
  private MobileElement softKeyNumber9;

  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"刪除\""),
          @iOSXCUITBy(xpath = "//XCUIElementTypeKey[@name=\"删除\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeKey[@name=\"Delete\"]")
  })
  private MobileElement softKeyDelete;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"more\"]")
  private MobileElement softKeyMore;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Return\"]")
  private MobileElement softKeyReturn;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"shift\"]")
  private MobileElement softKeyShift;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"A\"]")
  private MobileElement softKeyA;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"B\"]")
  private MobileElement softKeyB;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"D\"]")
  private MobileElement softKeyD;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"S\"]")
  private MobileElement softKeyS;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"T\"]")
  private MobileElement softKeyT;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"a\"]")
  private MobileElement softKeya;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"b\"]")
  private MobileElement softKeyb;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"e\"]")
  private MobileElement softKeye;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"g\"]")
  private MobileElement softKeyg;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"i\"]")
  private MobileElement softKeyi;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"l\"]")
  private MobileElement softKeyl;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"m\"]")
  private MobileElement softKeym;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"n\"]")
  private MobileElement softKeyn;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"r\"]")
  private MobileElement softKeyr;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"s\"]")
  private MobileElement softKeys;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"t\"]")
  private MobileElement softKeyt;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"u\"]")
  private MobileElement softKeyu;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"w\"]")
  private MobileElement softKeyw;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"y\"]")
  private MobileElement softKeyy;

  public static int setSleepTime(int... sleepTime) {
    int actualSleepTime = 100;
    if (sleepTime.length != 0) {
      actualSleepTime = sleepTime[0];
    }

    return actualSleepTime;
  }

  @SneakyThrows
  public void clickSoftKeyByNumberString(String data, int... sleepTime) {
    int actualSleepTime = setSleepTime(sleepTime);
    if(isShow(softKeyA,3)){
      clickElement(softKeyShift);
    }
    for (int i = 0; i < data.length(); i++) {
      String v = data.charAt(i) + "";
      Thread.sleep(actualSleepTime);
      switch (v) {
        case "0":
          clickElement(softKeyNumber0);
          break;
        case "1":
          clickElement(softKeyNumber1);
          break;
        case "2":
          clickElement(softKeyNumber2);
          break;
        case "3":
          clickElement(softKeyNumber3);
          break;
        case "4":
          clickElement(softKeyNumber4);
          break;
        case "5":
          clickElement(softKeyNumber5);
          break;
        case "6":
          clickElement(softKeyNumber6);
          break;
        case "7":
          clickElement(softKeyNumber7);
          break;
        case "8":
          clickElement(softKeyNumber8);
          break;
        case "9":
          clickElement(softKeyNumber9);
          break;
        case "a":
          clickElement(softKeya);
          break;
        case "i":
          clickElement(softKeyi);
          break;
        case "l":
          clickElement(softKeyl);
          break;
        case "n":
          clickElement(softKeyn);
          break;
        case "u":
          clickElement(softKeyu);
          break;
        case "m":
          clickElement(softKeym);
          break;
        case "b":
          clickElement(softKeyb);
          break;
        case "e":
          clickElement(softKeye);
          break;
        case "r":
          clickElement(softKeyr);
          break;
        case "t":
          clickElement(softKeyt);
          break;
        case "s":
          clickElement(softKeys);
          break;
        case "g":
          clickElement(softKeyg);
          break;
        case "w":
          clickElement(softKeyw);
          break;
        case "y":
          clickElement(softKeyy);
          break;
        case "A":
          clickElement(softKeyShift);
          clickElement(softKeyA);
          break;
        case "B":
          clickElement(softKeyShift);
          clickElement(softKeyB);
          break;
        case "D":
          clickElement(softKeyShift);
          clickElement(softKeyD);
          break;
        case "S":
          clickElement(softKeyShift);
          clickElement(softKeyS);
          break;
        case "T":
          clickElement(softKeyShift);
          clickElement(softKeyT);
          break;
        default:
          break;
      }

    }
  }

  public void inputDeleteByNumber(int number) {
    for (int i = 0; i < number; i++) {
      clickElement(softKeyDelete);
    }
  }

  @SneakyThrows
  public void enterGPSLocation(String screenShotName){
    Thread.sleep(3000);
    waitUntilElementClickable(eyeBtn);
    clickElement(eyeBtn);
    Thread.sleep(2000);
    clickElement(gpsElement);
    waitUntilElementVisible(mockWealthGPS);
    clearAndSendKeys(latitude,"22.291380");
    inputEnter(latitude);
    clearAndSendKeys(longitude,"114.207413");
    inputEnter(longitude);
    takeScreenshot(screenShotName);
    clickElement(saveBtn);
    Thread.sleep(2000);
    clickElement(locationXCoin);
  }

}
