#Feature: Update CRPQ
#
#  Background: Login
#    Given Open WeLab App
#    And Enable skip Root
#    When Login with user and password from properties
##    And Check upgrade page appears
##    And Login with user and password
##      | user     | swmcust00166 |
##      | password | Aa123321 |
#
#  #this case need goal with OffTrack status
#  @SellOnTrack_01
#  Scenario Outline:Sell part of available balance_Build My investment Habit
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_001_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_001_Android_02 |
#    And I click the order all of goal with the status of <orderStatus>
#    And I click SellBtn and Proceed button
#      | screenShotName  | Reg_Wealth_001_Android_03 |
#      | screenShotName1 | Reg_Wealth_001_Android_04 |
#    And I edit Sell Value
#      | sellValue      | 100                       |
#      | screenShotName | Reg_Wealth_001_Android_05 |
#    And I Slide to confirm Sell And Click Done
#    Then I can see Sell button will be disabled and goal is Processing
#      | screenShotName | Reg_Wealth_001_Android_06 |
#    And I only can see Target wealth
#    Examples:
#      | orderStatus |
#      | Offtrack    |
#
#  #this case need goal with OffTrack status
#  @SellOnTrack_02 @WELATCOE-683
#  Scenario Outline:Sell all the available balance_Set My Target
#    Then I goto wealthPage
#      | screenShotName | Reg_Wealth_002_Android_01 |
#    And I click view goal goto view goal page
#      | screenShotName | Reg_Wealth_002_Android_02 |
##    And I click goal name with <GoalName>
#    And I click the order all of goal with the status of <orderStatus>
#    And I click SellBtn and Proceed button
#      | screenShotName  | Reg_Wealth_002_Android_03 |
#      | screenShotName1 | Reg_Wealth_002_Android_04 |
#    And I click Sell All Button and cancel button
#      | screenShotName | Reg_Wealth_002_Android_05 |
#    And I click Sell All Button
#      | screenShotName | Reg_Wealth_002_Android_06 |
#    And I Slide to confirm Sell And Click Done
#    Then I can see Sell button will be disabled and goal is Processing
#      | screenShotName | Reg_Wealth_002_Android_07 |
#    And I only can see Target wealth
#    Examples:
#      | orderStatus |
#      | Offtrack    |
#
#
#
#
#
#
#
#
