package com.welab.automation.projects;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.entity.app.DeviceAppEntity;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import lombok.SneakyThrows;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static io.appium.java_client.service.local.flags.GeneralServerFlag.LOG_LEVEL;
import static io.appium.java_client.service.local.flags.GeneralServerFlag.SESSION_OVERRIDE;


public class AndroidThread extends Thread{

    AndroidDriver driver;
    String packageName ="com.android.settings";
    String activityName ="com.android.settings.MainSettings";

    @SneakyThrows
    @Override
    public void run() {
        try{
            startTest();
        }catch (Exception e){
        }
    }

    public DesiredCapabilities setAndroidDesiredCapabilities(){
        DesiredCapabilities devices  = new DesiredCapabilities();
        devices.setCapability("platformName", GlobalVar.GLOBAL_VARIABLES.get("platformName"));
        devices.setCapability("deviceName",GlobalVar.GLOBAL_VARIABLES.get("deviceName"));
        devices.setCapability("platformVersion", GlobalVar.GLOBAL_VARIABLES.get("platformVersion"));
        devices.setCapability("appPackage", packageName);
        devices.setCapability("appActivity", activityName);
        //devices.setCapability("appPackage", GlobalVar.GLOBAL_VARIABLES.get("android.appPackage"));
        //devices.setCapability("appActivity", GlobalVar.GLOBAL_VARIABLES.get("android.appActivity"));
        devices.setCapability("automation","uiautomator2");
        devices.setCapability("unicodeKeyboard", true);
        devices.setCapability("resetKeyboard",true);
        return devices;
    }

    @SneakyThrows
    public void startTest()  throws URISyntaxException, IOException {
        AppiumDriverLocalService service ;
        service =startAppiumService(DeviceAppEntity.DRIVER_PATH,DeviceAppEntity.APPIUM_PATH);
        driver = new AndroidDriver(service.getUrl(), setAndroidDesiredCapabilities());

        openAndriodApp(packageName,activityName);

        for(;;){
            Thread.sleep(5*1000);
            driver.runAppInBackground(Duration.ofSeconds(5));
            Thread.sleep(5*1000);
            driver.activateApp(packageName);
        }
    }

    public void openAndriodApp(String packageName, String activityName) {
        AndroidDriver androidDriver = (AndroidDriver) driver;
        Activity activity = new Activity(packageName, activityName);
        androidDriver.startActivity(activity);
    }


    private static AppiumDriverLocalService startAppiumService(String driverPath, String appiumPath) {
        AtomicInteger port = new AtomicInteger();
        AppiumDriverLocalService service = null;
        service = new AppiumServiceBuilder()
                .usingAnyFreePort()
                .withIPAddress("0.0.0.0")
                .withArgument(SESSION_OVERRIDE)
                .withArgument(LOG_LEVEL, "error")
                .usingDriverExecutable(new File(driverPath))
                .withAppiumJS(new File(appiumPath))
                .build();
        Optional.ofNullable(service).ifPresent(s -> {
            s.start();
            port.set(s.getUrl().getPort());
        });
        AppiumDriverLocalService appiumDriverLocalService = service;
        return service;
    }
}
