
Feature: D-GroupSavingJoinPotController
  Scenario Outline: <caseName>
    Given test description: D-GroupSavingJoinPotController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |D-GroupSavingJoinPotController|0|Get GroupSavings|/v1/gs|Get||status=PENDING||200||||data.id[0]|||||
    |D-GroupSavingJoinPotController|1|Get GroupSaving Status Pending|/v1/gs/mygs|Get||status=PENDING||200||{'data[0].id':'equals(${data.id[0]})'}|||||||
    |D-GroupSavingJoinPotController|2|Get Validate Pending|/v1/gs/${data.id[0]}/validate-pending|Post||||200||{'message':'success','data':'isNull'}|||||||
    |D-GroupSavingJoinPotController|3|GroupSaving Info|/v1/gs/${data.id[0]}|Get||||200||{'data.maxNetIndividualCap':'equals(200000.0)'}|||||||
    |D-GroupSavingJoinPotController|4|Get Available Balance|/v1/gs/${data.id[0]}/balance|Get||||200||{'data.minRemainAmountForPending':'isNotNull'}|||||||
    |D-GroupSavingJoinPotController|5|Check User Invited|/v1/gs/invited/check|Post||||200||{'data.invited':'equals(false)'}|||||||
    |D-GroupSavingJoinPotController|6|Validate Pending|/v1/gs/${data.id[0]}/validate-pending|Post||||200||{'message':'success','data':'isNull'}|||||||
    |D-GroupSavingJoinPotController|7|Check Deposit Request|/v1/gs/${data.id[0]}/check|Put|||Welab/body/Check_deposit_request.json|200||{'data.depositAmount':'equals(100)','data.status':'equals(PENDING)'}||data.depositAmount|||||
    |D-GroupSavingJoinPotController|8|Deposit|/v1/gs/${data.id[0]}/deposit|Post|||Welab/body/Deposit.json|200||{'data.depositAmount':'equals(100.0)'}|||||||
    |D-GroupSavingJoinPotController|9|Get GroupSaving Status Pending|/v1/gs/mygs|Get||status=PENDING||200||{'data.status[0]':'PENDING'}||data.id[0]|||||
    |D-GroupSavingJoinPotController|10|Get MyGroupSaving Balance|/v1/gs/mygs/balance|Get||||200||{'data.groupSavingBalance':'isNotNull','data.currency':'HKD'}|||||||
