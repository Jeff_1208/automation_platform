
Feature: E-BankAccountController
  Scenario Outline: <caseName>
    Given test description: E-BankAccountController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |E-BankAccountController|1|Get MySpending Account|/v1/bank-accounts/core/spending-account|Get||type=UNMASK||200||{'data.firstName':'isNotNull'}|||||||
    |E-BankAccountController|2|Get Accounts Balance|/v1/bank-accounts|Get||||200||{'data.currency[0]':'equals(HKD)'}|||||||
    |E-BankAccountController|3|Get Payment Limits|/v1/payments/limits|Get||||200||{'data.transactionTypeId[0]':'equals(DTTL)'}|||||||
    |E-BankAccountController|4|Get Dicts|/v1/accounts/dicts|Get||types=AREA_CODE||200||{'data.dicts.AREA_CODE.code[0]':'equals(244)'}|||||||
    |E-BankAccountController|5|Refresh-token|/v1/oauth/refresh-token|Post|{Authorization=without}||Welab/body/Refresh_token.json|200||{'data.refreshToken':'isNotNull'}|||||||
