
Feature: ApplyLoans
  Scenario Outline: <caseName>
    Given test description: ApplyLoans "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |ApplyLoans|1|getCards|/v1/bank-accounts/core/cards|Get||||200||||data.id[0]|||||
    |ApplyLoans|2|getCardDetail|/v1/bank-accounts/core/cards/${data.id[0]}|Get||||200|||||||||
    |ApplyLoans|5|getCardHolderDetail|/v1/bank-accounts/core/cards/${data.id[0]}/holders/me|get||||200|||||||||
    |ApplyLoans|11|doRequestPin|/v1/bank-accounts/core/cards/${data.id[0]}/verifications|get||||200|||||||||
    |ApplyLoans|12|getCardDetail|/v2/bank-accounts/core/cards/${data.id[0]}|get||||200|||||||||
    |ApplyLoans|13|getAccountsBalance|/v1/bank-accounts|get||||200|||||||||
    |ApplyLoans|14|accountStatus|/v1/bank-accounts/core/account-status|get||||200|||||||||
    |ApplyLoans|15|getAccountBalance|/v1/bank-accounts/core/balances|get||||200|||||||||
    |ApplyLoans|16|getMonthlyBalance|/v1/bank-accounts/core/closing-balances|get||||200|||||||||
    |ApplyLoans|18|getMySpendingAccount|/v1/bank-accounts/core/spending-account|get||||200|||||||||
    |ApplyLoans|21|totalBalance|/v2/bank-accounts/core/balances|get||||200|||||||||
    |ApplyLoans|1|getCards|/v1/bank-accounts/core/cards|Get|{Authorization=xxxx}|||401||||data.id[0]|||||
    |ApplyLoans|2|getCardDetail|/v1/bank-accounts/core/cards/${data.id[0]}|Get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|5|getCardHolderDetail|/v1/bank-accounts/core/cards/${data.id[0]}/holders/me|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|11|doRequestPin|/v1/bank-accounts/core/cards/${data.id[0]}/verifications|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|12|getCardDetail|/v2/bank-accounts/core/cards/${data.id[0]}|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|13|getAccountsBalance|/v1/bank-accounts|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|14|accountStatus|/v1/bank-accounts/core/account-status|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|15|getAccountBalance|/v1/bank-accounts/core/balances|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|16|getMonthlyBalance|/v1/bank-accounts/core/closing-balances|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|18|getMySpendingAccount|/v1/bank-accounts/core/spending-account|get|{Authorization=xxxx}|||401|||||||||
    |ApplyLoans|21|totalBalance|/v2/bank-accounts/core/balances|get|{Authorization=xxxx}|||401|||||||||
