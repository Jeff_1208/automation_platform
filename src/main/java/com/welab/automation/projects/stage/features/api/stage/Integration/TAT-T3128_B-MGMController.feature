
Feature: B-MGM Controller
  Scenario Outline: <caseName>
    Given test description: B-MGM Controller "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |B-MGM Controller|1|Details|/v1/accounts/mgm/details|Get||||200||{'data.referralCode':'isNotNull'}|||||||
    |B-MGM Controller|2|Direct Referrals|/v1/accounts/mgm/directReferrals|Get||||200||{'data.referrals':'isAnArray()'}|||||||
