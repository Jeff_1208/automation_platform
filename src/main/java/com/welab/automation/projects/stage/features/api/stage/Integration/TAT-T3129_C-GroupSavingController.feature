
Feature: C-GroupSavingController
  Scenario Outline: <caseName>
    Given test description: C-GroupSavingController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |C-GroupSavingController|1|My GroupSaving Status|/v1/gs/mygs/status|Get||||200||{'data.hasGroupSavingHistory':'isNotNull'}|||||||
    |C-GroupSavingController|2|My GroupSaving Status-401|/v1/gs/mygs/status|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|3|Get MyGroupSaving Balance|/v1/gs/mygs/balance|Get||||200||{'data.groupSavingBalance':'isNotNull','data.currency':'HKD'}|||||||
    |C-GroupSavingController|4|Get MyGroupSaving Balance-401|/v1/gs/mygs/balance|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|5|Get MyGroupSaving Accumulated Interest|/v1/gs/mygs/accumulated-interest|Get||||200||{'data.accumulatedInterest':'isNotNull','data.currency':'HKD'}|||||||
    |C-GroupSavingController|6|Get MyGroupSaving Accumulated Interest-401|/v1/gs/mygs/accumulated-interest|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|7|Get My GroupSaving|/v1/gs/mygs|Get||||200||{'data.groupName[1]':'contains(GoSave Time Deposit)'}||data.id[1]|||||
    |C-GroupSavingController|8|Get My GroupSaving-401|/v1/gs/mygs|Get|{Authorization=xxxx}|||401||||data.id[1]|||||
    |C-GroupSavingController|9|verifyInvitationCode|/v1/gs/invitation/verify|Post|||Welab/body/|500|||||||||
    |C-GroupSavingController|10|verifyInvitationCode|/v1/gs/invitation/verify|Post|{Authorization=xxxx}||Welab/body/|401|||||||||
    |C-GroupSavingController|11|checkUserInvited|/v1/gs/invited/check|Post|||Welab/body/|200|||||||||
    |C-GroupSavingController|12|checkUserInvited|/v1/gs/invited/check|Post|{Authorization=xxxx}||Welab/body/|401|||||||||
    |C-GroupSavingController|13|GetgsInfo|/v1/gs/mygs/${data.id[1]}|Get||||200|||||||||
    |C-GroupSavingController|14|GetgsInfo|/v1/gs/mygs/${data.id[1]}|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|15|GetgsInfo|/v1/gs/mygs/500|Get||||500|||||||||
    |C-GroupSavingController|16|Get getAvailableBalance|/v1/gs/${data.id[1]}|Get||||200|||||||||
    |C-GroupSavingController|17|Get getAvailableBalance|/v1/gs/500|Get||||200|||||||||
    |C-GroupSavingController|18|Get getAvailableBalance|/v1/gs/${data.id[1]}|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|19|getHistory|/v1/gs/${data.id[1]}/histories|Get||||200|||||||||
    |C-GroupSavingController|20|getHistory|/v1/gs/${data.id[1]}/histories|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|21|getHistory|/v1/gs/500/histories|Get||||200|||||||||
    |C-GroupSavingController|22|healthCheck|/health|Get||||200|||||||||
    |C-GroupSavingController|23|healthCheck|/health|Get|{Authorization=xxxx}|||200|||||||||
    |C-GroupSavingController|24|Get My getGroupSavings|/v1/gs|Get||status=PENDING||200||||data.id[0]|||||
    |C-GroupSavingController|25|getMyGroupSavingDetail|/v1/gs/mygs/${data.id[0]}|Get||||200|||||||||
    |C-GroupSavingController|26|gsInfo|/v1/gs/${data.id[0]}|Get||||200|||||||||
    |C-GroupSavingController|27|getAvailableBalance|/v1/gs/${data.id[0]}/balance|Get||||200|||||||||
    |C-GroupSavingController|28|checkDepositRequest|/v1/gs/${data.id[0]}/check|PUT|||Welab/body/checkDepositRequest.json|200|||||||||
    |C-GroupSavingController|29|deposit|/v1/gs/${data.id[0]}/deposit|POST|||Welab/body/depositRequest.json|200|||||||||
    |C-GroupSavingController|30|getHistory|/v1/gs/${data.id[0]}/histories|Get||||200|||||||||
    |C-GroupSavingController|31|validatePending|/v1/gs/${data.id[0]}/validate-pending|POST||||200|||||||||
    |C-GroupSavingController|32|validateWithdraw|/v1/gs/${data.id[0]}/validate-withdraw|Get||||200|||||||||
    |C-GroupSavingController|33|withdraw|/v1/gs/${data.id[0]}/withdraw-times|Get||||200|||||||||
    |C-GroupSavingController|34|saveWithdrawReason|/v1/gs/withdraw/reason|PUT|||Welab/body/withdrawReason.json|200|||||||||
    |C-GroupSavingController|35|getMyGroupSavingDetail|/v1/gs/mygs/${data.id[0]}|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|36|gsInfo|/v1/gs/${data.id[0]}|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|37|getAvailableBalance|/v1/gs/${data.id[0]}/balance|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|38|checkDepositRequest|/v1/gs/${data.id[0]}/check|PUT|{Authorization=xxxx}||Welab/body/checkDepositRequest.json|401|||||||||
    |C-GroupSavingController|39|deposit|/v1/gs/${data.id[0]}/deposit|POST|{Authorization=xxxx}||Welab/body/depositRequest.json|401|||||||||
    |C-GroupSavingController|40|getHistory|/v1/gs/${data.id[0]}/histories|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|41|validatePending|/v1/gs/${data.id[0]}/validate-pending|POST|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|42|validateWithdraw|/v1/gs/${data.id[0]}/validate-withdraw|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|43|withdraw|/v1/gs/${data.id[0]}/withdraw-times|Get|{Authorization=xxxx}|||401|||||||||
    |C-GroupSavingController|44|saveWithdrawReason|/v1/gs/withdraw/reason|PUT|{Authorization=xxxx}||Welab/body/withdrawReason.json|401|||||||||
    |C-GroupSavingController|45|getMyGroupSavingDetail|/v1/gs/mygs/00|Get||||400|||||||||
    |C-GroupSavingController|46|gsInfo|/v1/gs/00|Get||||400|||||||||
    |C-GroupSavingController|47|getAvailableBalance|/v1/gs/00/balance|Get||||400|||||||||
    |C-GroupSavingController|48|checkDepositRequest|/v1/gs/00/check|PUT|||Welab/body/checkDepositRequest.json|400|||||||||
    |C-GroupSavingController|49|deposit|/v1/gs/00/deposit|POST|||Welab/body/depositRequest.json|500|||||||||
    |C-GroupSavingController|50|getHistory|/v1/gs/00/histories|Get||||400|||||||||
    |C-GroupSavingController|51|validatePending|/v1/gs/00/validate-pending|POST||||400|||||||||
    |C-GroupSavingController|52|validateWithdraw|/v1/gs/00/validate-withdraw|Get||||400|||||||||
    |C-GroupSavingController|53|withdraw|/v1/gs/00/withdraw-times|Get||||400|||||||||
