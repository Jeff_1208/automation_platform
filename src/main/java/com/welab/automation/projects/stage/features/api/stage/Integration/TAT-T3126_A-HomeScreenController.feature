
Feature: A-HomeScreenController
  Scenario Outline: <caseName>
    Given test description: A-HomeScreenController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |A-HomeScreenController|1|Get Onboard Info|/v1/onboarding-requests|Get||||200||{'data.createStatus':'isNotNull'}|||||||
    |A-HomeScreenController|2|Register Notification|/v1/onboarding-requests/notification/registration|Post|||Welab/body/Register_Notification.json|200||{'message':'equals(success)','data':'isNull'}|||||||
    |A-HomeScreenController|3|Update Preferred Settings|/v1/accounts/me/preferred-settings|Put|||Welab/body/Upsetting.json|200||{'data':'isNull'}|||||||
    |A-HomeScreenController|4|Get Post Welcome|/v1/onboarding-requests/post-welcomes|Get||||200||{'data.fpsAutoRegister':'equals(true)','data.fpsDefaultBank':'equals(true)'}|||||||
    |A-HomeScreenController|5|Get Loan Applications|/v1/onboarding-requests/ntb/loan-applications|Get||||200||{'data.id':'isNotNull'}|||||||
    |A-HomeScreenController|6|Get Account By Categories|/v1/accounts/me|Get||categories=contact||200||{'message':'success','data.contact.username':'isNotNull'}||data.contact.email,data.contact.mobile|||||
    |A-HomeScreenController|7|Check Whitelist|/v1/accounts/mgm/whitelist|Get||||200||{'data.whitelisted':'true'}|||||||
    |A-HomeScreenController|8|Retrieve Notifications|/v1/message-center/notifications|Get||returnSize=60||200||{'data.id[0]':'isNotNull'}||data.updatedAt[0]|||||
    |A-HomeScreenController|9|Put Retrieve Notifications|/v1/message-center/notificationInfo|Put|||Welab/body/Update_Notification_Info.json|200|||||||||
    |A-HomeScreenController|10|Get Account Status|/v1/bank-accounts/core/account-status|Get||||200||{'data.productName':'Core Account','data.currencyId':'HKD'}|||||||
    |A-HomeScreenController|11|Get Transactions|/v1/bank-accounts/core/transactions|Get||page=2||200||{'data.page':'equals(2)','data.size':'equals(15)'}|||||||
    |A-HomeScreenController|12|Get Locked Transactions|/v1/bank-accounts/core/locked-transactions|Get||page=1||200||{'data.page':'equals(1)','data.size':'equals(15)'}|||||||
    |A-HomeScreenController|13|Get Account Balance|/v1/bank-accounts/core/balances|Get||||200||{'data.currency':'equals(HKD)'}|||||||
    |A-HomeScreenController|14|Mission information|/v1/accounts/me/missions|Get||||200||{'data.hasRegisterFps':'isNotNull'}|||||||
    |A-HomeScreenController|15|Get Marketing Promotion|/v1/accounts/me/marketing-promotions|GET||||200||{'data.sms':'isNotNull','data.isPushSet':'isNotNull'}|||||||
    |A-HomeScreenController|16|Update Marketing Promotion|/v1/accounts/me/marketing-promotions|Put|||Welab/body/Marketing_Promotions.json|200||{'data':'isNull'}|||||||
