

Feature: F-FpsTransferController
  Scenario Outline: <caseName>
    Given test description: F-FpsTransferController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |F-FpsTransferController|1|Enquire FpsRegistration|/v1/fps-registrations/enquiry|Post|||Welab/body/Fps_Regi_Enquiry.json|200||{'data.fpsRegistrationId':'799045406','data.registrationType':'MOBILE'}|||||||
    |F-FpsTransferController|2|Check Transfer Account|/v1/payments/check-account|Post|||Welab/body/Check_transfer_account.json|200||{'message':'success','data':'isNull'}|||||||
    |F-FpsTransferController|3|Get Banks|/v1/payments/banks|Get||||200||{'data[0].code':'390','data[0].name':'equals(Welab Bank Limited)'}|||||||
    |F-FpsTransferController|4|Get Accounts Balance|/v1/bank-accounts|Get||||200||{'data.currency[0]':'equals(HKD)'}|||||||
    |F-FpsTransferController|5|Get Payment Limits|/v1/payments/limits|Get||||200||{'data.transactionTypeId[0]':'equals(DTTL)'}|||||||
    |F-FpsTransferController|6|Enquiry Existed|/v1/payee/enquiry|Post|||Welab/body/Enquiry_Existed.json|200|||||||||
