package com.welab.automation.projects;

import com.welab.automation.projects.loans.pages.mobile.LoansPage;
import com.welab.automation.projects.wealth.pages.CommonPage;
import com.welab.automation.projects.wealth.pages.LoginPage;
import com.welab.automation.projects.wealth.pages.adminPortal.*;
import com.welab.automation.projects.wealth.pages.iao.*;
import com.welab.automation.projects.wealth2.pages.iao.IAOPage;
import io.cucumber.core.backend.ObjectFactory;
import io.cucumber.picocontainer.PicoFactory;

public class PicoDependencyInjector implements ObjectFactory {

  private PicoFactory delegate = new PicoFactory();

  public PicoDependencyInjector() {
    addClass(CoreBankSettingsPage.class);
    addClass(CRPQPage.class);
    addClass(GoalSettingPage.class);
    addClass(GoalSettingToolTipsPage.class);
    addClass(NoneCRPQPage.class);
    addClass(CommonPage.class);
    addClass(LoginPage.class);
    addClass(UploadFilesPage.class);
    addClass(GoalSettingOrderPage.class);
    addClass(ForeignExchangePage.class);
    addClass(AdminPortalLoginPage.class);
    addClass(AdminPortalMainPage.class);
    addClass(ApprovalPage.class);
    addClass(SummaryPage.class);
    addClass(ValidationDataMakerPage.class);
    addClass(InvestmentAccountManagementPage.class);
    addClass(LoansPage.class);
    addClass(IAOPage.class);
  }

  @Override
  public void start() {
    delegate.start();
  }

  @Override
  public void stop() {
    delegate.stop();
  }

  @Override
  public boolean addClass(Class<?> aClass) {
    return delegate.addClass(aClass);
  }

  @Override
  public <T> T getInstance(Class<T> aClass) {
    return delegate.getInstance(aClass);
  }
}
