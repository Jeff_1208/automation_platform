package com.welab.automation.projects.payment.pages.GPSpage;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.payment.paymentApiTest.coreUiAndApiTest;
import lombok.SneakyThrows;

import java.util.List;

public class GPSpage extends AppiumBasePage {

  private final String pageName = "GPS Page";

  //  public CommonPage() {
  //    super.pageName = this.pageName;
  //  }

  @SneakyThrows
  public void getCardInfomation() {
    String user = "tester050";
    String pwd = "Aa123456";
    String cardLastFourNumber = "8866";
    GlobalVar.GLOBAL_VARIABLES.put("cardLastFourNumber", cardLastFourNumber);
    System.out.println(
        "cardLastFourNumber: " + GlobalVar.GLOBAL_VARIABLES.get("cardLastFourNumber"));
    String token = getCardToken(user, pwd);
    GlobalVar.GLOBAL_VARIABLES.put("token", token);
    System.out.println("token: " + GlobalVar.GLOBAL_VARIABLES.get("token"));
    // System.out.println("cardInfomationshow: "+cardInfomationshow.getText());
    // LI NGON CHUNK 5479 7401 1984 5313 有效期限 05/26 CVV 098 添加到Apple钱包 Insights 隱藏資料 設定限額 重設卡密碼 報失卡
    // 水平滚动条, 1页 WeLab Debit Card
    // LI NGON CHUNK 5479 7401 6183 8687 Expiry date 04/27 CVV 686 Your card is expected to arrive
    // by 10 Jun 2022  Insights Hide details Daily card transaction limits Lost card 水平滚动条, 1页
    // Activate physical card WeLab Debit Card
  }

  public String getCardToken(String username, String password) throws Exception {
    String cardLastFourNumber = GlobalVar.GLOBAL_VARIABLES.get("cardLastFourNumber");
    coreUiAndApiTest coreUiAndApiTest = new coreUiAndApiTest();
    List<String> valueExpression =
        coreUiAndApiTest.gridFinder(username, password, cardLastFourNumber);
    System.out.println("valueExpression.get(0):" + valueExpression.get(0));
    return valueExpression.get(0);
  }
}
