package com.welab.automation.projects.payment.steps.gps;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class readCase {
    public static void main(String[] args) throws Exception {
        String path ="src/main/java/com/welab/automation/projects/channel/features/ios";
        readFeatureFiles(path);
    }
    public static void readFeatureFiles(String path) throws Exception {
        List<String> caseNames =new ArrayList<String>();
        File f = new File(path);
        File[] files = f.listFiles();

        for (int i = 0; i < files.length; i++) {
            System.out.println(files[i].getName());
            String absPath = files[i].getAbsolutePath();
            List<String> lines = Files.readAllLines(Paths.get(absPath));

            for (int j = 0; j < lines.size(); j++) {
                String line = lines.get(j);
                if(line.contains("Scenario")) {
                    if(line.trim().startsWith("#")) continue;
                    System.out.println(line);
                    String caseName = line.substring(line.indexOf(":")+1).trim();
                    caseName = caseName.split(" ")[0].trim();
                    caseNames.add(caseName);
                }
            }
        }
        for (int i = 0; i < caseNames.size(); i++) {
            //System.out.println(caseNames.get(i));
        }
    }

}
