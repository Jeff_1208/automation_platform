package com.welab.automation.projects.payment.steps.gps;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;

public class TestSOAPAPI {
  public String postMethod(String url, String requestPath,String filePath) throws Exception {
    // put the request body in test.xml file.
    FileInputStream fileInputStream = new FileInputStream(filePath);
    RestAssured.baseURI = url;
    Response response =
        RestAssured.given()
            .header("content-Type", "text/xml")
            .and()
            .body(IOUtils.toString(fileInputStream, "UTF-8"))
            .when()
            .post(requestPath)
            .then()
            .statusCode(200)
            .and()
            .log()
            .all()
            .extract()
            .response();

    XmlPath jsXpath = new XmlPath(response.asString()); // Converting string into xml path to assert
    System.out.println(response.asString());
    return  response.asString();
  }
}
