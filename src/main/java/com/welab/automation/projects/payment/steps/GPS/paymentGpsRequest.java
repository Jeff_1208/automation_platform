package com.welab.automation.projects.payment.steps.GPS;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.projects.channel.steps.gps.TestSOAPAPI;
import io.restassured.response.Response;
import lombok.SneakyThrows;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class paymentGpsRequest {

  private static String baseURI = "https://gpsehi.sta-wlab.com";
  private static String urlPath = "/v1/ehi/service/getTransaction.asmx";
  private static long currenttime;
  private static String Trans_link;
  private static String Txn_id_lock;
  private static String traceid_lifecycle;
  private static String Txn_id_confirm;
  private static String now;
  private static String details;
  private static String soapPath1 = "/src/main/resources/app/testData/payment/SoapData1.xml";
  private static String descSoapPath = "/src/main/resources/app/testData/payment/Soap.xml";
  private static String gpsXmlPath = "/src/main/resources/app/testData/payment/Gps.xml";
  private static String POS_Data_DE22;
  private static String GPS_POS_Capability = "A";
  private static Response response;
  private static GlobalVar globalVarGps = null;
  private static Map<String,String>paramMap = new HashMap<String,String>();

  public static void main(String[] args) {
    // Bill_Amt	-10	交易金额
    // Txn_CCy	344	交易币种 HKD：344 USD：840
    // GPS_POS_Capability	6	交易类型 ATM:A | Online:6, M| X-pay:9|Unknown:0|POS:1,2,3,4,5,7,R,
    // Token	101276565	用户卡Token
    // Txn_Amt 和Txn_Ccy表示交易金额和货币，这是商品原始的交易金额，比如外币消费 USD 10
    // Bill_Amt 和Bill_Ccy表示账单金额和货币，这是经过费率转换后的港币金额，比如HKD 8， 并且Txn_Amt是正数金额
    //  POS_Data_DE22= "810"   online  POS_Data_DE22= "832"  pos
//    String Bill_Amt = "-1001";
//    String Txn_Ccy = "344";
//    String GPS_POS_Capability = "A";
    // String Token = "101276565";
//    String Token = "129350791";
//    String Txn_Amt = "1001";
//    String Bill_Ccy = "344";
//    String POS_Data_DE22 = "832";
//    Map<String, String> data = new HashMap<>();
//    paymentGpsRequest gpsHttpRequest = new paymentGpsRequest(data);
//    boolean result = gpsHttpRequest.runGpsApi(data);
//    System.out.println("result: " + result);
  }
//
//  public paymentGpsRequest(Map<String, String> data) {
//    currenttime = System.currentTimeMillis();
//    Trans_link = currenttime / 1000 + "";
//    Txn_id_lock = currenttime / 1000 + "";
//    traceid_lifecycle = currenttime / 1000 + "";
//    Txn_id_confirm = currenttime + "";
//    now = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
//  }


  public boolean runGpsApi(Map<String, String> data) {
    details = now + GPS_POS_Capability;
    return runGpsApi(data);
  }

  @SneakyThrows
  public static boolean soapRequest(
          String soapPath, String descSoapPath, Map<String, String> data) {
    String soap = getSoapBody(soapPath, data);
    String project_path = System.getProperty("user.dir");
    String filepPath = project_path + descSoapPath;
    saveSoapToXml(soap, filepPath);
    TestSOAPAPI testSOAPAPI = new TestSOAPAPI();
    String response = testSOAPAPI.postMethod(baseURI, urlPath, filepPath);
    // System.out.println(isPass(response));
    return isPass(response);
  }

  public static boolean isPass(String response) {
    int index1 = response.indexOf("<Responsestatus>") + "<Responsestatus>".length();
    int index2 = response.indexOf("</Responsestatus>");
    String result = response.substring(index1, index2);
    System.out.println("result: " + result);
    if (result.equals("00")) {
      return true;
    } else {
      return false;
    }
  }

  public static String getSoapBody(String path, Map<String, String> data) {
    String soap = read_soap(path);
    soap = updateParams(soap, data);
    return soap;
  }

  public static String saveSoapToXml(String string, String filePath) {
    try {
      File file = new File(filePath);
      if (!file.exists()) {
        file.createNewFile();
      }
      FileOutputStream outStream = new FileOutputStream(file);
      outStream.write(string.trim().getBytes());
      outStream.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return filePath;
  }

  public static String read_soap(String path) {
    StringBuffer stringBuffer = new StringBuffer();
    try {
      String project_path = System.getProperty("user.dir");
      File file = new File(project_path + path);
      InputStream instream = new FileInputStream(file);
      if (instream != null) {
        InputStreamReader inputreader = new InputStreamReader(instream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line = "";
        while ((line = buffreader.readLine()) != null) {
          stringBuffer.append(line.trim() + "\n");
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return stringBuffer.toString();
  }

  public static String updateParams(String soap, Map<String, String> data) {
    now = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
    currenttime = System.currentTimeMillis();
    Trans_link = currenttime / 1000 + "";
    traceid_lifecycle = currenttime / 1000 + "";
    Txn_id_confirm = currenttime + "";
    Txn_id_lock = currenttime / 1000 + "";
    details = now + "A";
    for (Map.Entry<String, String> entry : data.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      if (value.contains("null")) {
        soap = soap.replace("{" + key + "}", "");
      } else {
        soap = soap.replace("{" + key + "}", value);
      }
      paramMap.put(key, data.get(key));
    }

    if(paramMap.containsKey("new")){
      for (Map.Entry<String, String> entry : paramMap.entrySet()) {
        String key = entry.getKey();
        String value = entry.getValue();
        if (value.contains("null")) {
          soap = soap.replace("{" + key + "}", "");
        } else {
          soap = soap.replace("{" + key + "}", value);
        }
      }
    }
    if (globalVarGps.GLOBAL_VARIABLES.get("traceid_lifecycle")!=null){
      traceid_lifecycle=globalVarGps.GLOBAL_VARIABLES.get("traceid_lifecycle");
      if (!paramMap.containsValue("1000346404")){
        details=globalVarGps.GLOBAL_VARIABLES.get("details");
      }
      Trans_link=globalVarGps.GLOBAL_VARIABLES.get("Trans_link");
    }
    soap = soap.replace("{traceid_lifecycle}", traceid_lifecycle);
    soap = soap.replace("{details}", details);
    soap = soap.replace("{Trans_link}", Trans_link);
    soap = soap.replace("{Txn_id_lock}", Txn_id_lock);
    globalVarGps.GLOBAL_VARIABLES.put("traceid_lifecycle",traceid_lifecycle);
    globalVarGps.GLOBAL_VARIABLES.put("details",details);
    globalVarGps.GLOBAL_VARIABLES.put("Trans_link",Trans_link);
    return soap;
  }


  public boolean sendRunGpsApi(Map<String, String> data) {
    return sendSoapRequest(data);
  }

  @SneakyThrows
  public static boolean sendSoapRequest(Map<String, String> data) {
    String soapPath =gpsXmlPath;
    String soap = getSoapBody(soapPath, data);
    TestSOAPAPI testSOAPAPI = new TestSOAPAPI();
    response = testSOAPAPI.sendPostMethod(baseURI,urlPath,soap);
    return isPass(response.asString());
  }


  public static String verifyGpsResponseCode() {
    return response.getStatusLine();
  }
}
