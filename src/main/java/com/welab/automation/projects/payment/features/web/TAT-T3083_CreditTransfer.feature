Feature: Outward Credit

  Background:  Outward Credit Boa Console

  @payment011
  Scenario Outline: :  Submit a credit transfer request (WeLab Bank account intra-bank transaction)
    Given Open the URL get to https://ecfps.sta-wlab.net/boa/showLogin.action
    And Login to EC_FPS BOA Console
      | username | ayden.li |
      | password | Ayden123 |
    Then Click credit transfer within payment service
    And fill in debtor's information <debtorAccount> <amountHKD> <debtorName>
    And fill in creditor's information
      |  creditorAgent      |  Welab Bank Limited (390) |
      |  creditorAccount    |  1000452077               |
      |  creditorName       |  ROGERS Steve             |
      |  creAcctIdTypeList  |  BBAN                     |
    Then Log out BOA Console Maker
    And Login to EC_FPS BOA Console
      | username | root     |
      | password | changeit |
    Then Click request approval within approval
    And  Approve query data and Confirm Approval
    And  Click Payment Enquiry within payment service
    And  Assert each field after a successful transaction in intra-bank transaction
    Examples:
      | debtorAccount | amountHKD | debtorName   |
      | 1000452085    | 50        | PRINCE Diana |

  @payment012
  Scenario Outline: :  Submit a credit transfer request (External bank transaction)
    Given Open the URL get to https://ecfps.sta-wlab.net/boa/showLogin.action
    And Login to EC_FPS BOA Console
      | username | ayden.li |
      | password | Ayden123 |
    Then Click credit transfer within payment service
    And fill in debtor's information <debtorAccount> <amountHKD> <debtorName>
    And fill in creditor's information
      |  creditorAgent      |  BANK 672 (672)           |
      |  creditorAccount    |  1000452077               |
      |  creditorName       |  ROGERS Steve             |
      |  creAcctIdTypeList  |  BBAN                     |
    Then Log out BOA Console Maker
    And Login to EC_FPS BOA Console
      | username | root     |
      | password | changeit |
    Then Click request approval within approval
    And  Approve query data and Confirm Approval
    And  Click Payment Enquiry within payment service
    And  Assert each field after a successful transaction in External bank transaction
    Examples:
      | debtorAccount | amountHKD | debtorName   |
      | 1000452085    | 50        | PRINCE Diana |

  @payment013
  Scenario Outline: Submit a credit transfer request (Non-bank account transfer methods)
    Given Open the URL get to https://ecfps.sta-wlab.net/boa/showLogin.action
    And Login to EC_FPS BOA Console
      | username | ayden.li |
      | password | Ayden123 |
    Then Click credit transfer within payment service
    And fill in debtor's information <debtorAccount> <amountHKD> <debtorName>
    And fill in creditor's information
      |  creditorAgent      |  Welab Bank Limited (390) |
      |  creditorAccount    |  +852-88880194            |
      |  creditorName       |  GOLDSON Gold             |
      |  creAcctIdTypeList  |  Non-bank                 |
    Then Log out BOA Console Maker
    And Login to EC_FPS BOA Console
      | username | root     |
      | password | changeit |
    Then Click request approval within approval
    And  Approve query data and Confirm Approval
    And  Click Payment Enquiry within payment service
    And  Assert each field after a successful transaction in intra-bank transaction
    Examples:
      | debtorAccount | amountHKD | debtorName   |
      | 1000452085    | 50        | PRINCE Diana |

  @payment014
  Scenario Outline: Submit an intra-bank credit transfer request (via HKID)
    Given Open the URL get to https://ecfps.sta-wlab.net/boa/showLogin.action
    And Login to EC_FPS BOA Console
      | username | ayden.li |
      | password | Ayden123 |
    Then Click credit transfer within payment service
    And fill in debtor's information <debtorAccount> <amountHKD> <debtorName>
    And fill in creditor's information
      |  creditorAgent      |  Welab Bank Limited (390) |
      |  creditorAccount    |  N1234569                 |
      |  customerId         |  8000027981               |
      |  creditorName       |  SMITH Jane               |
      |  creAcctIdTypeList  |  HKID                     |
    Then Log out BOA Console Maker
    And Login to EC_FPS BOA Console
      | username | root     |
      | password | changeit |
    Then Click request approval within approval
    And  Approve query data and Confirm Approval
    And  Click Payment Enquiry within payment service
    And  Assert each field after a successful transaction in intra-bank transaction
    Examples:
      | debtorAccount | amountHKD | debtorName   |
      | 1000452085    | 50        | PRINCE Diana |

