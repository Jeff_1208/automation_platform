package com.welab.automation.projects.payment.steps.mobile.GPS;

import com.welab.automation.projects.payment.pages.GPSpage.GPSpage;
import com.welab.automation.projects.payment.steps.GPS.paymentGpsRequest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GPSstep {

  paymentGpsRequest paymentGpsRequest;

  public GPSstep() {
    paymentGpsRequest = new paymentGpsRequest();
  }

  @And("run payment GPS request")
  public void runGPS(Map<String, String> data) {
//    paymentGpsRequest gpsHttpRequest = new paymentGpsRequest(data);
    boolean result = paymentGpsRequest.sendRunGpsApi(data);
    assertThat(result).isTrue();
    System.out.println("result: " + result);
  }

//  @Given("get card information now")
//  public void getCardInfomation() {
//    GPSpage.getCardInfomation();
//  }

    @Then("^the response http code equals to <httpCode>")
    public void theResponseHttpCodeEqualsToHttpCode(String httpCode) {
      String responseCode =paymentGpsRequest.verifyGpsResponseCode();
      assertThat(responseCode.contains(httpCode));
    }

}
