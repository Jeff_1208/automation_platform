package com.welab.automation.projects.payment.runners.GPS;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {"src/main/java/com/welab/automation/projects/payment/features/GPS/runGPS.feature"},
    glue = {"com/welab/automation/projects/payment/steps/mobile/GPS"},
    monochrome = true)
public class GPSTestRunner extends TestRunner {}
