package com.welab.automation.projects.payment.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.payment.pages.boaPage.BoaConsoleCommonPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.Map;


public class BoaCommonStep extends WebBasePage {

    BoaConsoleCommonPage boaConsoleCommonPage;

    public BoaCommonStep() {
        boaConsoleCommonPage = new BoaConsoleCommonPage();
    }

    @Given("^Open the URL get to ([^\"]\\S*)$")
    public void openUrl(String url) {
        try {
            navigateTo(url);
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    @And("^Login to EC_FPS BOA Console$")
    public void LoginAsMaker(Map<String,String> loginData) {
        boaConsoleCommonPage.LoginBoaConsole(loginData.get("username"),loginData.get("password"));
    }

    @Then("^Log out BOA Console Maker$")
    public void LogoutAsMaker() {
        boaConsoleCommonPage.LogoutBoaConsole();
    }

}
