package com.welab.automation.projects.payment.paymentApiTest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.api.SendRequest;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class paymentAPI {
  Logger logger = LoggerFactory.getLogger("VerifyReportDebitCardLost");
  private static Response response;

  public static Map<String, String> channelLogin(String name, String password, String maskedPan)
      throws Exception {
    String path = "/v1/bank-accounts/core/cards";
    Map<String, String> prepareData = new HashMap<String, String>();
    prepareData.put("TEST_ACCOUNT_SPECIFIED", name);
    String params = "";
    prepareData.put("TEST_PASSWORD_SPECIFIED", password);
    String globalVariable = "";
    Map<String, String> result = channelToken(path, params, prepareData, maskedPan);
    return result;
  }

  private static Map<String, String> channelToken(
      String path, String params, Map<String, String> prepareData, String maskedPan)
      throws Exception {
    if (!prepareData.isEmpty()) {
      for (String key : prepareData.keySet()) {
        GlobalVar.GLOBAL_VARIABLES.put(key, prepareData.get(key));
      }
    }
    File file = new File("./src/main/resources/api_channel.properties");
    if (!file.exists()) file.createNewFile();
    Properties proterties = new Properties();
    proterties.load(new FileInputStream(file));
    GlobalVar.GLOBAL_VARIABLES.put("host", (String) proterties.get("host"));
    GlobalVar.HEADERS.put("Content-Type", (String) proterties.get("Content-Type"));
    GlobalVar.HEADERS.put("Client-Id", (String) proterties.get("CLIENT_ID"));
    GlobalVar.HEADERS.put("Client-Secret", (String) proterties.get("CLIENT_SECRET"));
    GlobalVar.HEADERS.put("User-Agent", (String) proterties.get("User-Agent"));
    GlobalVar.HEADERS.put("Accept-Language", (String) proterties.get("Accept-Language"));
    SendRequest SendRequest = new SendRequest();
    Map<String, String> Map = new HashMap<String, String>();
    Response response = SendRequest.sendRequest(path, "GET", "", params, "");
    List<Map<String, Object>> data = response.jsonPath().getList("data");
    for (int i = 0; i < data.size(); i++) {
      String str = String.valueOf(data.get(i).get("publicToken"));
      if (str != "") {
        String masked = String.valueOf(data.get(i).get("maskedPan"));
        if (masked.contains(maskedPan)) {
          Map.put("public token", (data.get(i).get("publicToken").toString()));
          Map.put("id", (data.get(i).get("id").toString()));
          Map.put("maskedPan", (data.get(i).get("maskedPan").toString()));
        }
      }
    }
    return Map;
  }

  public static String channelCard(String cardId) throws Exception {
    Map<String, String> valueExpression = paymentAPI.channelLogin("tester054", "Aa123456", "3907");

    String id = valueExpression.get("id");
    String path = "/v1/bank-accounts/core/cards/" + id + "/lost";
    SendRequest SendRequest = new SendRequest();
    Response response =
        SendRequest.sendRequest(path, "PUT", "", "", "Welab/body/Channel_cardLost.json");
    String messageStatus = response.jsonPath().get("message");
    return messageStatus;
  }

  public static void main(String[] args) throws Exception {
    String valueExpression = paymentAPI.channelCard("48176");
    assertThat(valueExpression.equals("success")).isTrue();
    System.out.println(valueExpression);
  }
}
