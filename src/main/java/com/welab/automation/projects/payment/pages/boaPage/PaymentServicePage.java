package com.welab.automation.projects.payment.pages.boaPage;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

import static com.welab.automation.framework.utils.FileUtil.readPaymentExCsvFile;

public class PaymentServicePage extends WebBasePage{

    private final String pageName = "payment service page";
    String locator = "//*[@id=\"data-table\"]/tbody";
    long beforeTriggerTime;
    String creationTime;

    @FindBy(how = How.CSS, using = "i[class='glyphicon glyphicon-usd']")
    private WebElement paymentServiceListTitle;

    @FindBy(how = How.XPATH, using = "//*[@id='side-menu']/li[4]/ul/li[1]/a")
    private WebElement creditTransTitle;

    @FindBy(how = How.XPATH, using = "//*[@id='businessService']")
    private WebElement serviceTypeSelect;

    @FindBy(how = How.XPATH, using = "//*[@id='creditTransferForm']/div[2]//input")
    private WebElement debtorACInput;

    @FindBy(how = How.XPATH, using = "//*[@id='inquireDebtorAcctBtn']")
    private WebElement inquireBtn;

    @FindBy(how = How.XPATH, using = "//*[@placeholder='Enter Amount']")
    private WebElement amountInput;

    @FindBy(how = How.XPATH, using = "//*[@id='creditTransferForm']/div[6]//select")
    private WebElement paymentCateSelect;

    @FindBy(how = How.XPATH, using = "//*[@id='creditorAgentList']")
    private WebElement creditorAgentSelect;

    @FindBy(how = How.XPATH, using = "//*[@placeholder='Enter Creditor Account Number']")
    private WebElement creditorACInput;

    @FindBy(how = How.XPATH, using = "//*[@placeholder='Enter Creditor Name']")
    private WebElement creditorNameInput;

    @FindBy(how = How.XPATH, using = "//*[@id='creditTransferForm']/div[10]/div/button[1]")
    private WebElement paymentServiceConfirm;

    @FindBy(how = How.CSS, using = "i[class='glyphicon glyphicon-check']")
    private WebElement approvalListTitle;

    @FindBy(how = How.XPATH, using = "//a[text()=' Request Approval ']")
    private WebElement requestApproval;

    @FindBy(how = How.XPATH, using = "//div/div[2]/div/span")
    private WebElement referNumTip;

    @FindBy(how = How.XPATH, using = "//*[@placeholder='Enter Reference Number']")
    private WebElement queryReferNum;

    @FindBy(how = How.XPATH, using = "//*[@id='searchBtn']")
    private WebElement searchBtn;

    @FindBy(how = How.XPATH, using = "//input[@data-index=\"0\"]")
    private WebElement searchResultLine;

    @FindBy(how = How.XPATH, using = "//input[@data-index=\"1\"]")
    private WebElement searchResultLine2;

    @FindBy(how = How.XPATH, using = "//*[@id='approveRequestsBtn']")
    private WebElement approveRequestsBtn;

    @FindBy(how = How.XPATH, using = "//*[text()='Confirm Approval']")
    private WebElement confirmApprovalBtn;

    @FindBy(how = How.XPATH, using = "//a[text()=' Payment Enquiry ']")
    private WebElement paymentEnquiryTitle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"searchList\"]/button/span[1]")
    private WebElement enquirySearchListBtn;

    @FindBy(how = How.XPATH, using = "//*[@data-value='criteria.transactionId']")
    private WebElement  searchTranId;

    @FindBy(how = How.XPATH, using = "//*[@id='dropdownTextBox']")
    private WebElement  searchTranIdInput;

    @FindBy(how = How.XPATH, using = "//*[@id='viewDetailBtn']")
    private WebElement  viewDetailBtn;

    @FindBy(how = How.NAME, using = "transactionId")
    private WebElement  transactionIdInput;

    @FindBy(how = How.NAME, using = "fpsReferenceNo")
    private WebElement  fpsReferenceNoInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewPaymentDetailModal\"]/div[2]/div/div[1]/button")
    private WebElement clickCloseBtn;

    @FindBy(how = How.XPATH, using = "//a[text() = ' Payment Exception ']")
    private WebElement  paymentExceptionTitle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"dlRecordBtn\"]")
    private WebElement  paymentExceptionDownBtn;

    @FindBy(how = How.XPATH, using = "//h2[text()='Payment Result']")
    private WebElement  paymentResultTitle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"transactionStatusDetailBtn\"]/../../input")
    private WebElement  transactionStatusText;

    @FindBy(how = How.NAME, using = "balanceStatus")
    private WebElement  balanceStatus;

    @FindBy(how = How.XPATH, using = "//*[@id=\"updateExceptionStatusBtn\"]/../../input")
    private WebElement  exHandlingStatus;

    @FindBy(how = How.XPATH, using = "//*[@id=\"balanceTxDetailBtn\"]")
    private WebElement  balanceTxDetailBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ledgerTxDetailTable\"]/tbody/tr/td")
    private WebElement  ledgerTxDetailText;

    @FindBy(how = How.XPATH, using = " //*[@id=\"reprocessLedgerTxBtn\"]")
    private WebElement  reprocessLedgerTxBtn;

    @FindBy(how = How.XPATH, using = " //*[@name='instructionId']/../../../../../div[2]//input[@name='settlementAmt']")
    private WebElement  instructionInfoAmt;

    @FindBy(how = How.NAME, using = "balanceReference")
    private WebElement  balanceReference;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ledgerTxDetailTable\"]/tbody/tr/td")
    private List<WebElement> ledgerTranTable;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ledgerTxDetailModal\"]/div[2]/div/div/button")
    private WebElement  detailCancelBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewPaymentDetailModal\"]//button[7]")
    private WebElement  closeBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewHistoryBtn\"]")
    private WebElement viewJournalBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"eventTable\"]/tbody/tr")
    private List<WebElement> journalTable;

    @FindBy(how = How.XPATH, using = "//div[text() = 'Exception Handling Status']/../div[2]/button")
    private WebElement handlingStatus;

    @FindBy(how = How.XPATH, using = "//div[text() = 'Exception Handling Status']/../div[2]/ul/li/a/label")
    private WebElement selectAll;

    @FindBy(how = How.XPATH, using = "//*[@name=\"debtorName\"]")
    private WebElement debtorNameText;

    @FindBy(how = How.XPATH, using = "//*[@id=\"creditorAcctIdTypeList\"]/button/span[1]")
    private WebElement creditorAcctIdTypeList;

    @FindBy(how = How.XPATH, using = "//*[@data-value=\"MOBN\"]")
    private WebElement mobileNumber;

    @FindBy(how = How.XPATH, using = "//*[@data-value=\"HKID\"]")
    private WebElement hKID;


    @FindBy(how = How.XPATH, using = "//*[@id=\"inquireCreditorAcctBtn\"]")
    private WebElement inquireCreditorAcctBtn;

    public void clickPaymentService() {
        waitUntilElementClickable(paymentServiceListTitle);
        clickElement(paymentServiceListTitle);
    }

    public void clickCreditTransWithinPayService() {
        waitUntilElementClickable(creditTransTitle);
        clickElement(creditTransTitle);
    }

    public void clickPaymentExceptionWithinPayService() {
        waitUntilElementClickable(paymentExceptionTitle);
        clickElement(paymentExceptionTitle);
    }

   @SneakyThrows
    public void fillDebtorInfo(String accountNum,String amount,String debtorName) {
        waitUntilElementClickable(serviceTypeSelect);
        selectByIndex(serviceTypeSelect,1);
        Thread.sleep(1000);
        clearAndSendKeys(debtorACInput,accountNum);
        Thread.sleep(1000);
        clickElement(inquireBtn);
        Thread.sleep(1000);
        assertThat(getElementText(debtorNameText)).isEqualTo(debtorName);
        clearAndSendKeys(amountInput,amount);
        Thread.sleep(1000);
        selectByIndex(paymentCateSelect,2);
    }

    @SneakyThrows
    public void fillCreditorInfo(String creditorAgent,String creditorAccount,String creditorName) {
        Thread.sleep(1000);
        selectByVisibleText(creditorAgentSelect,creditorAgent);
        clearAndSendKeys(creditorACInput,creditorAccount);
        clearAndSendKeys(creditorNameInput,creditorName);
        clickElement(paymentServiceConfirm);
    }

    @SneakyThrows
    public void fillCreditorInfoNonBank(String creditorAgent,String creditorAccount,String creditorName) {
        Thread.sleep(1000);
        selectByVisibleText(creditorAgentSelect,creditorAgent);
        Thread.sleep(1000);
        clickElement(creditorAcctIdTypeList);
        waitUntilElementClickable(mobileNumber);
        clickElement(mobileNumber);
        clearAndSendKeys(creditorACInput,creditorAccount);
        Thread.sleep(1000);
        clickElement(inquireCreditorAcctBtn);
        Thread.sleep(2000);
        assertThat(creditorNameInput.getAttribute("value")).isEqualTo(creditorName);
        Thread.sleep(1000);
        clickElement(paymentServiceConfirm);
    }

    @SneakyThrows
    public void fillCreditorInfoHKID(String creditorAgent,String creditorAccount,String creditorName,String customerID) {
        Thread.sleep(1000);
        selectByVisibleText(creditorAgentSelect,creditorAgent);
        Thread.sleep(1000);
        clickElement(creditorAcctIdTypeList);
        waitUntilElementClickable(hKID);
        clickElement(hKID);
        clearAndSendKeys(creditorACInput,creditorAccount);
        Thread.sleep(1000);
        clickElement(inquireCreditorAcctBtn);
        Thread.sleep(4000);
        assertThat(getElementText(creditorAcctIdTypeList)).isEqualTo("Customer ID (CUST)");
        Thread.sleep(1000);
        assertThat(creditorACInput.getAttribute("value")).isEqualTo(customerID);
        Thread.sleep(1000);
        clearAndSendKeys(creditorNameInput,creditorName);
        Thread.sleep(1000);
        clickElement(paymentServiceConfirm);
    }


    @SneakyThrows
    public String getTranReferNumber() {
        Thread.sleep(2*1000);
        String transReferNum = getElementText(referNumTip);
        Thread.sleep(2*1000);
        clickElement(referNumTip);
        return transReferNum.substring(transReferNum.indexOf("[")+1, transReferNum.indexOf("]"));
    }

    @SneakyThrows
    public void clickApproval() {
        Thread.sleep(2000);
        waitUntilElementClickable(approvalListTitle);
        clickElement(approvalListTitle);
    }

    public void queryAndApproval(String tranReferNum) {
        waitUntilElementClickable(requestApproval);
        clickElement(requestApproval);
        clearAndSendKeys(queryReferNum,tranReferNum);
        clickElement(searchBtn);
        clickElement(searchResultLine);
    }

    public void clickApproveRequestsBtn() {
        waitUntilElementClickable(approveRequestsBtn);
        clickElement(approveRequestsBtn);
    }

    public void clickConfirmApprovalBtn() {
        waitUntilElementClickable(confirmApprovalBtn);
        clickElement(confirmApprovalBtn);
    }

    public void clickPaymentServEnquiry() {
        waitUntilElementClickable(paymentEnquiryTitle);
        clickElement(paymentEnquiryTitle);
    }

    @SneakyThrows
    public void passTranIdSearch(String tranReferNumber) {
        waitUntilElementClickable(enquirySearchListBtn);
        clickElement(enquirySearchListBtn);
        waitUntilElementClickable(searchTranId);
        clickElement(searchTranId);
        clearAndSendKeys(searchTranIdInput,tranReferNumber);
        Thread.sleep(12*1000);
        clickElement(searchBtn);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public boolean refNoDiffTranID() {
        waitUntilElementClickable(searchResultLine);
        clickElement(searchResultLine);
        Thread.sleep(2000);
        waitUntilElementClickable(viewDetailBtn);
        clickElement(viewDetailBtn);
        Thread.sleep(2000);
        return !(transactionIdInput.getAttribute("value").equalsIgnoreCase((fpsReferenceNoInput.getAttribute("value"))));
    }

    public WebElement getCellText(int row,int col) {
        String xpath = locator + "//tr[" + row + "]/td[" + col + "]";
         WebElement cell = driver.findElement(By.xpath(xpath));
         return cell;
    }

    @SneakyThrows
    public boolean boaOutFpsIn() {
        boolean flagBoa = false;
        boolean flagFps = false;
        String jsScript = "arguments[0].removeAttribute(arguments[1])";
        removeAttributeByJS(jsScript,clickCloseBtn,"aria-hidden");
        Thread.sleep(2000);
        clickElement(clickCloseBtn);
        if (getElementText(getCellText(1,14)).equals("ECFPS")) {
            if (getElementText(getCellText(1,5)).equals("Inward")) {
                flagFps = true;
            }
        }
        if (getElementText(getCellText(2,14)).equals("boa")) {
            if (getElementText(getCellText(2,5)).equals("Outward")) {
                flagBoa = true;
            }
        }
        return flagBoa && flagFps;
    }

    @SneakyThrows
    public boolean boaOutFpsExternal() {
        boolean flagBoa = false;
        String jsScript = "arguments[0].removeAttribute(arguments[1])";
        removeAttributeByJS(jsScript,clickCloseBtn,"aria-hidden");
        Thread.sleep(2000);
        clickElement(clickCloseBtn);
        if (getElementText(getCellText(1,14)).equals("boa")) {
            if (getElementText(getCellText(1,5)).equals("Outward")) {
                flagBoa = true;
            }
        }
        return flagBoa;
    }

    public boolean checkDisplayedAC(String debtorAccount,String creditorAccount) {
        boolean flagDebtor = false;
        boolean flagCreditor = false;
        if (getElementText(getCellText(1,7)).contains(debtorAccount)
                && getElementText(getCellText(2,7)).contains(debtorAccount)) {
            flagDebtor = true;
        }

        if (getElementText(getCellText(1,8)).contains(creditorAccount)
                && getElementText(getCellText(2,8)).contains(creditorAccount)) {
            flagCreditor = true;
        }
        return flagDebtor && flagCreditor;
    }

    public boolean checkDisplayedACExternal(String debtorAccount,String creditorAccount) {
        return getElementText(getCellText(1, 7)).contains(debtorAccount)
                && getElementText(getCellText(1, 8)).contains(creditorAccount);
    }

    public boolean getAmtOfBoaAndFps(String amount) {
         int amountInt = Integer.parseInt(amount);
         DecimalFormat df = new DecimalFormat("0.00");
        return getElementText(getCellText(1, 10)).equals("+" + df.format((float)amountInt))
                && getElementText(getCellText(2, 10)).equals("-" + df.format((float)amountInt));
    }

    public boolean getAmtOfBoaAndFpsExternal(String amount) {
        int amountInt = Integer.parseInt(amount);
        DecimalFormat df = new DecimalFormat("0.00");
        return getElementText(getCellText(1, 10)).equals("-" + df.format((float)amountInt));
    }

    public boolean getPaymentStatus() {
        return getElementText(getCellText(1,12)).equals("Accepted & Settled")
                && getElementText(getCellText(2,12)).equals("Accepted & Settled");
    }

    public boolean getPaymentStatusExternal() {
        return getElementText(getCellText(1,12)).equals("Accepted & Settled");
    }

    public boolean getBalanceStatus() {
        return getElementText(getCellText(1,13)).equals("POSTED")
                && getElementText(getCellText(2,13)).equals("POSTED");
    }

    public boolean getBalanceStatusExternal() {
        return getElementText(getCellText(1,13)).equals("POSTED");
    }

    @SneakyThrows
    public void downPaymentExCsv() {
        waitUntilElementClickable(paymentExceptionDownBtn);
        clickElement(paymentExceptionDownBtn);
        Thread.sleep(8*1000);
    }

    public String getPaymentExCsvFilePath(String tranStatus,String balanceStatus,String serviceType) {
        return readPaymentExCsvFile(tranStatus, balanceStatus,serviceType);
    }

    @SneakyThrows
    public void  passRefIdSearchOrder(String refNoId) {
        waitUntilElementClickable(searchTranIdInput);
        clearAndSendKeys(searchTranIdInput,refNoId);
        Thread.sleep(2000);
        clickElement(searchBtn);
    }

    public void clickDetail() {
        waitUntilElementClickable(searchResultLine);
        clickElement(searchResultLine);
        waitUntilElementClickable(viewDetailBtn);
        clickElement(viewDetailBtn);
        scrollToBottom(pageName);
    }

    @SneakyThrows
    public boolean asserThreeFiled(String tranStatus,String sourceBalanceStatus) {
        Thread.sleep(2000);
        waitUntilElementClickable(paymentResultTitle);
        String transactionStatus = transactionStatusText.getAttribute("value");
        String balanceStatusValue = balanceStatus.getAttribute("value");
        String exHandlingStatusValue = exHandlingStatus.getAttribute("value");
        return transactionStatus.equals(tranStatus)
                && balanceStatusValue.equals(sourceBalanceStatus)
                && exHandlingStatusValue.equals("PENDING");
    }

    @SneakyThrows
    public String selectDetailOfLedgeTxStatus() {
        waitUntilElementClickable(balanceTxDetailBtn);
        clickElement(balanceTxDetailBtn);
        Thread.sleep(2000);
        waitUntilElementClickable(ledgerTxDetailText);
        return getElementText(ledgerTxDetailText);
    }

    @SneakyThrows
    public String triggerReprocessLedgerTran() {
        waitUntilElementClickable(reprocessLedgerTxBtn);
        clickElement(reprocessLedgerTxBtn);
        beforeTriggerTime = System.currentTimeMillis();
        logger.info(String.valueOf(beforeTriggerTime));
        Thread.sleep(4000);
        String ledgerStatus = balanceStatus.getAttribute("value");
        return ledgerStatus;
    }

    @SneakyThrows
    public void checkReLedgerTranInfo() {
        String amount = instructionInfoAmt.getAttribute("value");
        logger.info(amount);
        String ledger = balanceReference.getAttribute("value");
        logger.info(ledger);
        waitUntilElementClickable(balanceTxDetailBtn);
        clickElement(balanceTxDetailBtn);
        Thread.sleep(2000);
        String ledgerDetailXpath = "//*[@id=\"ledgerTxDetailTable\"]/tbody/tr/td";
        String xpath;
        for (int i = 3; i <= ledgerTranTable.size() ; i++) {
            xpath = ledgerDetailXpath + "[" + i + "]";
            String detailValue = getElementText(findElement(By.xpath(xpath)));
            switch (i) {
                case 3:
                    creationTime = getElementText(findElement(By.xpath(xpath)));
                    DateTimeFormatter ctFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                    LocalDateTime parse = LocalDateTime.parse(creationTime, ctFormatter);
                    long detailTime = parse.toEpochSecond(ZoneOffset.ofHours(8));
                    assertThat(detailTime-beforeTriggerTime < 30).isTrue();
                    continue;
                case 4:
                    assertThat(detailValue).isEqualTo("CREDIT");
                    continue;
                case 5:
                    assertThat(detailValue).isEqualTo(amount);
                    continue;
                case 6:
                    assertThat(detailValue).isEqualTo(ledger);
                    continue;
                case 7:
                    xpath = ledgerDetailXpath + "[" + i + "]/div";
                    assertThat(getElementText(findElement(By.xpath(xpath)))).isEqualTo("SUCCESS");
                default:
                    logger.error("输入的td超出范围");
                    break;
            }
        }
    }

    @SneakyThrows
    public boolean verifyField(String refNOId) {
        Thread.sleep(2000);
        waitUntilElementClickable(detailCancelBtn);
        String jsScript = "arguments[0].removeAttribute(arguments[1])";
        removeAttributeByJS(jsScript,detailCancelBtn,"aria-hidden");
        clickElement(detailCancelBtn);
        waitUntilElementClickable(closeBtn);
        clickElement(closeBtn);
        waitUntilElementClickable(handlingStatus);
        clickElement(handlingStatus);
        waitUntilElementClickable(selectAll);
        clickElement(selectAll);
        passRefIdSearchOrder(refNOId);
        waitUntilElementClickable(searchResultLine);
        clickElement(searchResultLine);
        waitUntilElementClickable(viewDetailBtn);
        clickElement(viewDetailBtn);
        //需要修改的地方
        Thread.sleep(2000);
        String newTranStatus  = transactionStatusText.getAttribute("value");
        String newBalanceStatus = balanceStatus.getAttribute("value");
        String newExHandlingStatus = exHandlingStatus.getAttribute("value");
        return newTranStatus.equals("Accepted & Settled")
                 && newBalanceStatus.equals("POSTED")
                 && newExHandlingStatus.equals("COMPLETED");
    }

    @SneakyThrows
    public void viewJournal() {
        waitUntilElementClickable(closeBtn);
        clickElement(closeBtn);
        waitUntilElementClickable(viewJournalBtn);
        clickElement(viewJournalBtn);
        Thread.sleep(3000);
        final int size = journalTable.size();
        String colSize = "//*[@id=\"eventTable\"]/tbody/tr[" + size + "]/td";
        List<WebElement> tdSize = driver.findElements(By.xpath(colSize));
        Thread.sleep(2000);
        for (int i = 1; i <= tdSize.size(); i++) {
            colSize = "//*[@id=\"eventTable\"]/tbody/tr[" + size + "]/td[" + i + "]";
            String tableValue = getElementText(findElement(By.xpath(colSize)));
            switch (i) {
                case 1:
                    assertThat(tableValue).isEqualTo(creationTime);
                    continue;
                case 2:
                    assertThat(tableValue).isEqualTo("Exception Resolution");
                    continue;
                case 3:
                    assertThat(tableValue).isEqualTo("SUCCESS");
                    continue;
                case 4:
                    assertThat(tableValue.contains("ledger transactions")).isTrue();
                    continue;
                default:
                    logger.error("获取到的表格参数不在范围内：{}",tableValue);
            }
        }

    }

}
