package com.welab.automation.projects.payment.runners.web;


import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(

   features = {
                "src/main/java/com/welab/automation/projects/payment/features/web/OutwardCredit.feature"
   },
   glue = { "com/welab/automation/projects/payment/steps/web"},
   monochrome = true
)
public class BoaConsoleTestRunner extends TestRunner {}
