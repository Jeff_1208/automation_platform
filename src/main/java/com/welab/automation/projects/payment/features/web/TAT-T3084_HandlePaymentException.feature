Feature: Handle Payment Exception

  Background:  Handle payment Exception of Boa Console

    Scenario: Handle Payment Exception on ECFPS BoA - Timeout Posting to Core Bank (Direct Debit)
      Given Open the URL get to https://ecfps.sta-wlab.net/boa/showLogin.action
      And Login to EC_FPS BOA Console
        | username | ayden.li |
        | password | Ayden123 |
      Then Click payment exception  within payment service
      And  Download exception order csv and get ref.no ID
        | tranStatus      | Accepted & Settled |
        | balanceStatus   | ERROR              |
        | serviceType     | PAYD01             |
      And  View detail and Determine whether the field meets the requirements
      And  Select Detail of Ledge Tx. Status and Trigger Reprocess Ledger Transaction
      And  Search for the captured transaction record and verify field

