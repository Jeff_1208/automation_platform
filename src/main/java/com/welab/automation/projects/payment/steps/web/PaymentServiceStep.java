package com.welab.automation.projects.payment.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.payment.pages.boaPage.PaymentServicePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


public class PaymentServiceStep extends WebBasePage {

    PaymentServicePage paymentServicePage;
    static String tranReferNumber;
    static String debtorAccount;
    static String creditorAccount;
    static String amt;
    static String tranStatus;
    static String balanceStatus;
    static String serviceType;
    static String refNOId;
    static String customerId;


    public PaymentServiceStep() {
        paymentServicePage = new PaymentServicePage();
    }

    @Then("^Click credit transfer within payment service$")
    public void enterPaymentServTransfer() {
        paymentServicePage.clickPaymentService();
        paymentServicePage.clickCreditTransWithinPayService();
    }

    @And("^fill in debtor's information ([^\"]\\S*) ([^\"]\\S*) ([^\"][\\S\\s]*)$")
    public void fillDebtorInfo(String debtorAccount,String amountHKD,String debtorName) {
        PaymentServiceStep.debtorAccount = debtorAccount;
        PaymentServiceStep.amt = amountHKD;
        paymentServicePage.fillDebtorInfo(debtorAccount,amountHKD,debtorName);
    }

    @And("^fill in creditor's information$")
    public void fillCreditorInfo(Map<String,String> creditorData) {
        creditorAccount = creditorData.get("creditorAccount");
        customerId      = creditorData.get("customerId");
        if (creditorData.get("creAcctIdTypeList").equals("BBAN")) {
            paymentServicePage.fillCreditorInfo(creditorData.get("creditorAgent"),creditorData.get("creditorAccount"),creditorData.get("creditorName"));
        }else if (creditorData.get("creAcctIdTypeList").equals("Non-bank")) {
            paymentServicePage.fillCreditorInfoNonBank(creditorData.get("creditorAgent"),creditorData.get("creditorAccount"),creditorData.get("creditorName"));
        }else if (creditorData.get("creAcctIdTypeList").equals("HKID")) {
            paymentServicePage.fillCreditorInfoHKID(creditorData.get("creditorAgent"),creditorData.get("creditorAccount"),creditorData.get("creditorName"),customerId);
        }
        tranReferNumber = paymentServicePage.getTranReferNumber();
    }


    @Then("^Click request approval within approval$")
    public void enterApprovalReqApproval() {
        paymentServicePage.clickApproval();
        paymentServicePage.queryAndApproval(tranReferNumber);
    }

    @And("^Approve query data and Confirm Approval$")
    public void selectQueryDataAndConfirm() {
        paymentServicePage.clickApproveRequestsBtn();
        paymentServicePage.clickConfirmApprovalBtn();

    }

    @And("^Click Payment Enquiry within payment service$")
    public void enterPaymentServEnquiry() {
        paymentServicePage.clickPaymentService();
        paymentServicePage.clickPaymentServEnquiry();
    }

    @And("^Assert each field after a successful transaction in intra-bank transaction$")
    public void assertField() {
        paymentServicePage.passTranIdSearch(tranReferNumber);
        assertThat(paymentServicePage.refNoDiffTranID()).isTrue();
        assertThat(paymentServicePage.boaOutFpsIn()).isTrue();
        if ( null == customerId) {
            assertThat(paymentServicePage.checkDisplayedAC(debtorAccount, creditorAccount)).isTrue();
        }else {
            assertThat(paymentServicePage.checkDisplayedAC(debtorAccount, customerId)).isTrue();
        }
        assertThat(paymentServicePage.getAmtOfBoaAndFps(amt)).isTrue();
        assertThat(paymentServicePage.getPaymentStatus()).isTrue();
        assertThat(paymentServicePage.getBalanceStatus()).isTrue();
    }


    @And("^Assert each field after a successful transaction in External bank transaction$")
    public void assertFieldExternalBank() {
        paymentServicePage.passTranIdSearch(tranReferNumber);
        assertThat(paymentServicePage.refNoDiffTranID()).isTrue();
        assertThat(paymentServicePage.boaOutFpsExternal()).isTrue();
        assertThat(paymentServicePage.checkDisplayedACExternal(debtorAccount, creditorAccount)).isTrue();
        assertThat(paymentServicePage.getAmtOfBoaAndFpsExternal(amt)).isTrue();
        assertThat(paymentServicePage.getPaymentStatusExternal()).isTrue();
        assertThat(paymentServicePage.getBalanceStatusExternal()).isTrue();
    }

    @Then("^Click payment exception  within payment service$")
    public void enterPaymentServException() {
        paymentServicePage.clickPaymentService();
        paymentServicePage.clickPaymentExceptionWithinPayService();
    }

    @And("^Download exception order csv and get ref.no ID$")
    public void downCsvAndGetRefNoID(Map<String,String> screenData) {
        paymentServicePage.downPaymentExCsv();
        tranStatus = screenData.get("tranStatus");
        balanceStatus = screenData.get("balanceStatus");
        serviceType   = screenData.get("serviceType");
        refNOId = paymentServicePage.getPaymentExCsvFilePath(tranStatus, balanceStatus,serviceType);
        paymentServicePage.passRefIdSearchOrder(refNOId);
    }

    @And("^View detail and Determine whether the field meets the requirements$")
    public void determineField() {
        paymentServicePage.clickDetail();
        assertThat(paymentServicePage.asserThreeFiled(tranStatus,balanceStatus)).isTrue();
        assertThat(paymentServicePage.selectDetailOfLedgeTxStatus()).isEqualTo("No matching records found");
    }

    @And("^Select Detail of Ledge Tx. Status and Trigger Reprocess Ledger Transaction$")
    public void triggerLedgerTran() {
        String ledgerTxStatus = paymentServicePage.triggerReprocessLedgerTran();
        assertThat(ledgerTxStatus).isEqualTo("POSTED");
        paymentServicePage.checkReLedgerTranInfo();
    }

    @And("^Search for the captured transaction record and verify field$")
    public void searchTranVerifyFieldAndViewJournal() {
        assertThat(paymentServicePage.verifyField(refNOId)).isTrue();
        paymentServicePage.viewJournal();
    }

}
