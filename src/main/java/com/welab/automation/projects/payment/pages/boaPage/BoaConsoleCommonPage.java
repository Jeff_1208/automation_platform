package com.welab.automation.projects.payment.pages.boaPage;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BoaConsoleCommonPage extends WebBasePage {


    @FindBy(how = How.XPATH, using = "//*[@id='login-form']/div[1]/input")
    private WebElement usernameEditBox;

    @FindBy(how = How.XPATH, using = "//*[@id='login-form']/div[2]/input")
    private WebElement passwordEditBox;

    @FindBy(how = How.XPATH, using = "//*[@id='login-form']/div[3]/button")
    private WebElement loginBtn;

    @FindBy(how = How.CSS, using = "i[class='fa fa-user fa-fw']")
    private WebElement accountBtn;

    @FindBy(how = How.CSS, using = "a[id='btnLogout']")
    private WebElement logoutBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='OK']")
    private WebElement sureLogoutOKBtn;



    public void LoginBoaConsole(String username,String password) {
        clearAndSendKeys(usernameEditBox,username);
        clearAndSendKeys(passwordEditBox,password);
        clickElement(loginBtn);
    }

    @SneakyThrows
    public void LogoutBoaConsole() {
        waitUntilElementClickable(accountBtn);
        clickElement(accountBtn);
        waitUntilElementClickable(logoutBtn);
        clickElement(logoutBtn);
        Thread.sleep(1000);
        waitUntilElementClickable(sureLogoutOKBtn);
        clickElement(sureLogoutOKBtn);
    }


}
