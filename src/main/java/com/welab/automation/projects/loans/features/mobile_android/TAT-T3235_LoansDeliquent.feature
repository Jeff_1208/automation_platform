Feature: Loans Application Deliquent

  Background: Login
    Given Open loans WeLab App
    And Enable skip Root Loans

  @VBL-T2631
  Scenario: Reg_Loan_Deliquent_001 Loan 逾期 -deliquent -本金和逾期费没还
    When Login with user and password
      | user     | stress24028  |
      | password | abcD$123456  |
    Then I goto loansPage
      | screenShotName               | Reg_Loan_Deliquent_001_Android_01 |
    Then open my loans
    Then check my loan second repayment amount
      | screenShotName               | Reg_Loan_Deliquent_001_Android_02 |
      | screenShotName1              | Reg_Loan_Deliquent_001_Android_03 |

  @VBL-T2632
  Scenario: Reg_Loan_Deliquent_002 Loan 逾期  -deliquent  -本金还了，逾期费没还
    When Login with user and password
      | user     | stress24028  |
      | password | abcD$123456  |
    Then I goto loansPage
      | screenShotName               | Reg_Loan_Deliquent_002_Android_01 |
    Then open my loans
    Then check my loan with deliquent
      | screenShotName               | Reg_Loan_Deliquent_002_Android_02 |
      | screenShotName1              | Reg_Loan_Deliquent_002_Android_03 |

  @VBL-T2633
  Scenario: Reg_Loan_Deliquent_003 Loan 逾期  -deliquent  -本金没还，逾期费还了
    When Login with user and password
      | user     | stress27170  |
      | password | abcD$123456  |
    Then I goto loansPage
      | screenShotName               | Reg_Loan_Deliquent_003_Android_01 |
    Then open my loans
    Then check my loan with deliquent
      | screenShotName               | Reg_Loan_Deliquent_003_Android_02 |
      | screenShotName1              | Reg_Loan_Deliquent_003_Android_03 |

  @VBL-T2634
  Scenario: Reg_Loan_Deliquent_004 Loan 逾期  - nab  -本金还部分
    When Login with user and password
      | user     | stress24046  |
      | password | abcD$123456  |
    Then I goto loansPage
      | screenShotName               | Reg_Loan_Deliquent_004_Android_01 |
    Then open my loans
    Then check my loan with deliquent
      | screenShotName               | Reg_Loan_Deliquent_004_Android_02 |
      | screenShotName1              | Reg_Loan_Deliquent_004_Android_03 |
