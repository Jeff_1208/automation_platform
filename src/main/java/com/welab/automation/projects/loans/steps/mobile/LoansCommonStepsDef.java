package com.welab.automation.projects.loans.steps.mobile;


import com.welab.automation.projects.loans.pages.mobile.CommonPage;
import com.welab.automation.projects.loans.pages.mobile.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LoansCommonStepsDef {

  LoginPage loginPage;
  CommonPage commonPage;
  public LoansCommonStepsDef(){
    loginPage = new LoginPage();
    commonPage = new CommonPage();
  }

  private static final String[] login_text = {"Login","登入"};
  private static final String[] total_balance_text = {"Total balance","總結餘"};
  private static final String expect_pending_desc =
          "We're working hard to process your application and will inform you via in-app notification within 2 business days.";

  @When("Login with user and password")
  public void loginWithUserAndPassword(Map<String, String> data) {
    loginPage.loginWithCredential(data.get("user"), data.get("password"));
  }

  @When("Login with user and password from properties")
  public void loginWithUserAndPasswordFromProperties() {
    loginPage.loginWithUserAndPasswordFromProperties();
  }

  @When("Login with user and password from txt")
  public void loginWithUserAndPasswordFromTxt() {
    loginPage.loginWithUserAndPasswordFromTxt();
  }

  @Then("check Unexpected Element")
  public void checkUnexpectedElement(){
    loginPage.checkUnexpectedElement();
  }

  @Given("Open loans WeLab App")
  public void openWeLabApp() throws Exception {
    loginPage.waitAppOpen();
  }

  @And("Enable skip Root Loans")
  public void enableSkipRoot() throws InterruptedException {
    commonPage.enableSkipRoot();
  }

  @And("Enable Skip Root Rebind Device Authentication")
  public void enableSkipRootRebindDeviceAuthentication() throws InterruptedException {
    commonPage.enableSkipRootRebindDeviceAuthentication();
  }

  @And("Enable Skip Root Rebind Device Authentication IOS")
  public void enableSkipRootRebindDeviceAuthenticationIOS() throws InterruptedException {
    commonPage.enableSkipRootRebindDeviceAuthenticationIOS();
  }

  @And("bind Device Authentication IOS")
  public void bindDeviceAuthenticationIOS() throws InterruptedException {
    commonPage.bindDeviceAuthenticationIOS();
  }

  @And("bind Device Authentication")
  public void bindDeviceAuthentication() throws InterruptedException {
    commonPage.bindDeviceAuthentication();
  }

  @And("skip Root ios")
  public void enableSkipRootIos() throws InterruptedException {
    commonPage.skipRootIos();
  }

  @And("skip Live Update ios")
  public void skipLiveUpdateIos() throws InterruptedException {
    commonPage.skipLiveUpdateIos();
  }

  @And("goto My Account page")
  public void gotoMyAccountPage() {
    commonPage.gotoMyAccount();
  }

  @Then("I can see the LoggedIn page")
  public void verifyOnLoggedInPage() throws InterruptedException {
    String checkedTxt = loginPage.getLoggedInPageText();
    assertThat(commonPage.oneOfArrayItemInStr(checkedTxt,total_balance_text));
  }

  @Then("I can see the home page")
  public void IcanSeeHomePage() {
    loginPage.IcanSeeHomePage();
  }

  @Given("Check upgrade page appears Stage")
  public void checkUpgradePageAppears() {
    commonPage.checkLogin();
    commonPage.checkUpgrade();
  }

  @Given("Click got it and Login")
  public void clickGotItAndLogin() {
//    commonPage.checkUpgrade();
//    commonPage.checkLogin();
    commonPage.checkUpgrade();
  }

  @Then("I can see pending approval screen")
  public void iCanSeePendingApproval() {
    String approvalTxt = commonPage.getPendingApprovalDesc();
    assertThat(approvalTxt).isEqualTo(expect_pending_desc);
  }

  @And("I skip guidance")
  public void skipGuidance() {
    commonPage.skipGuidance();
  }

  @And("I go to Verify personal info")
  public void goToVerifyPersonalInfo() {
    commonPage.skipToOpenAccount();
  }

  @When("I click X icon")
  public void iClickXIcon() {
    commonPage.clickXIcon();
  }

  @Then("I can see pending page")
  public void verifyPendingPage(Map<String, String> data) {
    String titleText = commonPage.getElementText(commonPage.getThanksTitle()).trim();
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
  }

  @When("I click back icon")
  public void iClickBackIcon() {
    commonPage.clickBackIcon();
  }

  @And("I move eye to top right corner")
  public void iMoveEyeBtnToTopRight() {
    // if the mobile platform is iOS, then no eyeBtn.
    commonPage.moveEyeBtnToTopRight();
    System.out.println("111111111111111111111111111111111111111111111111");

  }

  @And("Enable skip Root Stage and change language to CH")
  public void enableSkipRootchange() throws InterruptedException {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      boolean flag =commonPage.changeLanguageToCH();
      assertThat(flag).isTrue();
      commonPage.enableSkipRoot();
    }
  }
}
