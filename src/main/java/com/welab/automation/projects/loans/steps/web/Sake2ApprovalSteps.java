package com.welab.automation.projects.loans.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.loans.pages.sake2.Sake2ApprovalPage;
import com.welab.automation.projects.loans.pages.sake2.Sake2LoginPage;
import com.welab.automation.projects.loans.pages.sake2.Sake2MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import java.util.Map;


public class Sake2ApprovalSteps extends WebBasePage {

  Sake2LoginPage sake2LoginPage;
  Sake2MainPage sake2MainPage;
  Sake2ApprovalPage sake2ApprovalPage;
  public Sake2ApprovalSteps(){
    sake2LoginPage = new Sake2LoginPage();
    sake2MainPage = new Sake2MainPage();
  }

  @Given("^Open the URL ([^\"]\\S*)$")
  public void openUrl(String url) {
    try {
      navigateTo(url);
    } catch (Exception e) {
      logger.error(e.toString());
    }
  }

  @And("login sake2")
  public void loginAsMaker(Map<String, String> data) {
    sake2LoginPage.loginSake2(data.get("username"), data.get("password"));
  }

  @And("search Loan Application Number ([^\"]\\S*)$")
  public void searchLoansApplicationNumber(String data) {
    sake2MainPage.clickAppliation();
    sake2MainPage.searchLoansID(data);
  }

  @And("get application level")
  public void getApplicationLevel() {
    sake2MainPage.getloansApplicationLevel();
  }

  @And("set assigner")
  public void setAssigner(Map<String, String> data) {
    sake2MainPage.setAssigner(data.get("name"));
  }

  @And("logout sake2")
  public void loginSake2() {
    sake2MainPage.logout();
  }

  @And("confirm Loans Infomation")
  public void confirmLoansInfomation() {
    sake2ApprovalPage.confirmLoansInfomation();
  }

  @And("check Income and Address File")
  public void checkIncomeAndAddressFile() {
    sake2ApprovalPage.checkIncomeAddressProofFirst();
  }

  @And("Edit Loans Info")
  public void EditLoansInfo() {
    sake2ApprovalPage.EditLoansInfo();
  }

  @And("close web driver")
  public void closeWebDriver() {
    sake2LoginPage.closeWebDriver();
  }

}
