Feature:  Loan Applications Checker

  Background: Approved by checker

  Scenario: checker login
    Given Open the URL https://ploan-portal.sta-wlab.net/
    And Enter the user name and password and log in
      | username | checker1 |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName               | Reg_Checker_001_Android_01 |
    And  Checker update assign successfully
      | upAssigneeName | checker1 checker1 (Checker) |
      | statusSelect   | Pending Approval |
      |  riskName      | levelb1 levelb1 (Credit Checker B) |
      | screenShotName               | Reg_Checker_001_Android_02 |
      | screenShotName1              | Reg_Checker_001_Android_03 |
      | screenShotName2              | Reg_Checker_001_Android_04 |
      | screenShotName3              | Reg_Checker_001_Android_05 |
      | screenShotName4              | Reg_Checker_001_Android_06 |
      | screenShotName5              | Reg_Checker_001_Android_07 |
      | screenShotName6              | Reg_Checker_001_Android_08 |
    And Click the exit login button and click login
    And Enter the user name and password and log in
      | username | levelb1  |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName               | Reg_Checker_001_Android_09 |
    And  Click the tab 'credit info' and  the button 'Approve'
      | loanStatus | Approve |
      | screenShotName               | Reg_Checker_001_Android_10 |
      | screenShotName1              | Reg_Checker_001_Android_11 |