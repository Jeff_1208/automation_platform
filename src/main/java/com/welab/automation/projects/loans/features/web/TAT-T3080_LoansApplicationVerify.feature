Feature: verify APP UI when user personal loans complete application on sake2

  Background: IAO application submit
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'agree' & checker = 'agree'
    Given Open the URL https://sit-ploan-portal.dev-bks.welabts.net/
    And login sake2
      | username | checker1@welab.co |
      | password | Aa123321 |
    And search Loan Application Number <LoansApplicationNumber>
    And get application level
    And set assigner
      | name | John Wick (CheckerMaker) |
    And logout sake2
    And login sake2
      | username | maker1@welab.co |
      | password | Aa123456 |
    And search Loan Application Number <LoansApplicationNumber>
    And confirm Loans Infomation
    And check Income and Address File
    And Edit Loans Info
    And logout sake2
    And login sake2
      | username | checker1@welab.co |
      | password | Aa123321 |
    And set assigner
      | name | Danny Yeung (Checker) |
    Examples:
      | LoansApplicationNumber |
      | P202103049053216    |





