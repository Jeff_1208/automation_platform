package com.welab.automation.projects.loans.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import lombok.SneakyThrows;


public class LoansIosPage extends AppiumBasePage {
    protected String pageName = "Loans Page";
    protected CommonPage commonPage;
    protected LoginPage loginPage;
    public LoansIosPage() {
        super.pageName = this.pageName;
        commonPage = new CommonPage();
        loginPage = new LoginPage();
    }

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"貸款, tab, 4 of 5\"")
    private MobileElement loanBtn;

    @iOSXCUITFindAll({
            //@iOSXCUITBy(xpath = "//*[contains(@name ,'WeLab Bank 私人貸款')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'我的貸款')]"),
    })
    private MobileElement loansPage;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"以后\"]"),
    })
    private MobileElement laterButton;

    public void checkLaterButton(){
        if(isShow(laterButton,10)){
            clickElement(laterButton);
        }
    }

    public void clickMyLoans(){
        clickByLocationByPercent(86, 6 );
    }

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"我的貸款 結欠總額\")]")
    private MobileElement myLoansPage;

    @SneakyThrows
    public void IgoToMyloans(String screenShotName){
        clickMyLoans();
        waitUntilElementVisible(myLoansPage);
        Thread.sleep(6000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void getLoansPage(String screenShotName){
        waitUntilElementVisible(loanBtn);
        clickElement(loanBtn);
        waitUntilElementVisible(loansPage);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
    }

    public void clickPersonalLoans(){
        //clickByLocationByPercent(30,44);//iphone se iphone7 plus
        if(isIphone13()){
            clickByLocationByPercent(20,39);
        }else{
            clickByPicture("src/main/resources/images/loans/PIL_Loans.png",50,60);
        }
    }
    public void clickDCLoans(){
        if(isIphoneXR()){
            clickByLocationByPercent(20,53);
        }else if(isIphone13()){
            clickByLocationByPercent(20,55);
        }else{
            clickByPicture("src/main/resources/images/loans/DC_Loans.png",50,60);
        }
    }

    public void clickRFLoans(){
        if(isIphone13()){
            clickByLocationByPercent(50, 55);
        }else{
            clickByPicture("src/main/resources/images/loans/RF_Loans.png",50,60);
        }
    }

    public void clickApplyNow(){
        if(isIphoneSE()){
            clickByLocationByPercent(50, 62);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,65);
        }else{
            clickByPicture("src/main/resources/images/loans/applyNow.png",50,60);
        }
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'私人分期貸款')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'私人貸款')]"),
    })
    private MobileElement personalLoansPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'貸款計算機')]")
    private MobileElement loansAmountPage;
    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'產品資料概要')]")
    private MobileElement loansPdfPage;

    public void clickReadPdfAgreeButton(){
        if(isIphone7Plus()||isIphoneSE()){
            clickByLocationByPercent(50,88);//iphone se iphone7 plus
        }else{
            clickByPicture("src/main/resources/images/loans/IHaveReadAndAgreeButton.png",50,60);
        }
    }

    @SneakyThrows
    public void checkKFSpage(String screenShotName, String screenShotName1){
        takeScreenshot(screenShotName);
        checkPDF(screenShotName1);
    }

    @SneakyThrows
    public void checkKFSpageAndSetLoansAmount(String Amount,
                                              String screenShotName, String screenShotName1){
        editLoansAmount(Amount);
        takeScreenshot(screenShotName);
        checkPDF(screenShotName1);
    }

    @SneakyThrows
    public void RFsetLoansAmountAndClickApplyNowIOS(String Amount,String tenor,
                                              String screenShotName, String screenShotName1){
        //editLoansAmount(Amount);
        if(tenor==null || tenor.length()<1){
        }else{
            editLoansTenor(tenor);
        }
        setLoansPurposeForRfFlow();
        takeScreenshot(screenShotName);
        Thread.sleep(200);
        scrollUp();
        scrollUp();
        clickByPicture("src/main/resources/images/loans/applyNow.png",50,60);
    }
    @SneakyThrows
    public void setLoansPurposeForRfFlow(){
        clickLoansPurposeOnSetRfAmountPage();
        Thread.sleep(200);
        clickElement(loansPurposeBuyCard);
    }

    public void clickLoansPurposeOnSetRfAmountPage(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,77.6);
        }else{
            clickByLocationByPercent(50,70);
        }
    }

    public void editLoansTenor(String tenor){
        double temp = Double.parseDouble(tenor);
        if(temp > 60 ) temp = 60;
        if(temp < 6 ) temp = 6;
        double endPerX = temp * 100/66;
        double y = 0;
        if(isIphoneSE()){
            y = 56.3;
        }else if(isIphoneSE()){
            y = 56.3;
        }else{

        }
        scrollByLocationPercentFromOnePointToOtherPoint(6.6, y, endPerX, y);
    }

    @SneakyThrows
    public void goToLoansPILPage(){
        waitUntilElementVisible(personalLoansPage);
        clickPersonalLoans();
        Thread.sleep(3000);
        waitUntilElementVisible(loansAmountPage);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void goToLoansRFPage(){
        waitUntilElementVisible(personalLoansPage);
        clickRFLoans();
        Thread.sleep(3000);
        waitUntilElementVisible(loansAmountPage);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void goToLoansDCPage(){
        waitUntilElementVisible(personalLoansPage);
        clickDCLoans();
        Thread.sleep(3000);
        waitUntilElementVisible(loansAmountPage);
        Thread.sleep(1000);
    }


    @SneakyThrows
    public void checkPDF(String screenShotName1){
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        clickApplyNow();
        confirmKfsPdfPage(screenShotName1);
    }

    @SneakyThrows
    public void confirmKfsPdfPage(String screenShotName1){
        waitUntilElementVisible(loansPdfPage);
        Thread.sleep(3000);
        clickByLocationByPercent(50, 50);
        for (int i = 0; i < 6; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickReadPdfAgreeButton();
    }

    @SneakyThrows
    public void editLoansAmount(String Amount){
        clickLoansAmountEditButton();
        commonPage.inputDeleteByNumber(6);
        Thread.sleep(1000);
        commonPage.inputMSKByString(Amount);
        scrollUpByStartEndHeightPercent(20,80); //hide keyboard
        Thread.sleep(1000);
    }

    public void clickLoansAmountEditButton(){
        if(isIphoneSE()){
            clickByLocationByPercent2(86.9,53.4);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else{
            clickByLocationByPercent2(86.9,53.4);
        }
    }


    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'有關你嘅工作')]")
    private MobileElement aboutYourJobPage;

    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, '美容及健身')]")
    private MobileElement positionItem;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"美容及健身\"]")
    private MobileElement positionTypeItem;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"一般員工\"]")
    private MobileElement selectPositionItem;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"完成\"])[2]")
    private MobileElement selectItemFinish;


    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"Aberdeen\"])[2]")
    private MobileElement areaItem;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"受僱 - 固定收入\"])[2]"),
            //@iOSXCUITBy(xpath = "(//XCUIElementTypeOther[@name=\"受僱 - 浮動收入\"])[2]"),
    })
    private MobileElement incomeItem;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"受僱 - 浮動收入\"])[2]")
    private MobileElement incomeItem2;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"待業 - 其他\"])[2]")
    private MobileElement incomeUnemployed;

    public void clickSelectItemFinish(){
        clickElement(selectItemFinish);
    }

    public void scrollUpForWorkYears(){
        scrollUpByStartEndPointPercent( 25, 95, 25, 70);
    }

    public void clickCompanyInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,25);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(50,22);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,20.8);
        }else{
            clickByPicture2("src/main/resources/images/loans/companyInput.png",50,20);
        }
    }
    @SneakyThrows
    public void fillCompanyName(){
        clickCompanyInput();
        Thread.sleep(1000);
        String company="Welab";
        commonPage.clickSoftKeyByNumberString(company);
        commonPage.clickSoftKeyReturn();
    }

    public void clickPositionItemSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,33);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,31.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,28.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,29.4);
        }else{
            clickByPicture2("src/main/resources/images/loans/PositionItemSelector.png",50,10);
        }
    }

    @SneakyThrows
    public void selectPositionItem(String screenShotName){
        Thread.sleep(1000);
        clickPositionItemSelector();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickElement(positionItem);
    }
    public void clickPositionTypeSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,45);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,41);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,36.7);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,38.1);
        }else{
            clickByPicture2("src/main/resources/images/loans/PositionTypeSelector.png",50,50);
        }
    }

    @SneakyThrows
    public void selectPositionType(String screenShotName1){
        Thread.sleep(1000);
        clickPositionTypeSelector();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickElement(positionTypeItem);
    }

    public void clickPositionDetailSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,56);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,50.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,44.2);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,47);
        }else{
            clickByLocationByPercent(50,56);
        }
    }

    @SneakyThrows
    public void selectPositionDetail(){
        Thread.sleep(1000);
        clickPositionDetailSelector();
        Thread.sleep(1000);
        clickElement(selectPositionItem);
    }

    public void clickPositionNameInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,67);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,59.5);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,51.8);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,55);
        }else{
            clickByLocationByPercent(50,67);
        }
    }

    @SneakyThrows
    public void inputPositionName(){
        Thread.sleep(1000);
        clickPositionNameInput();
        Thread.sleep(1000);
        String position="qa";
        commonPage.clickSoftKeyByNumberString(position);
        commonPage.clickSoftKeyReturn();
    }

    public void clickCurrentCompanyYears(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,78);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,69.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,59.7);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,64);
        }else{
            clickByLocationByPercent(50,78);
        }
    }

    @SneakyThrows
    public void selectCurrentCompanyYears(String screenShotName2){
        Thread.sleep(1000);
        clickCurrentCompanyYears();
        Thread.sleep(2000);
        takeScreenshot(screenShotName2);
        clickSelectItemFinish();
        Thread.sleep(1000);
    }

    public void clickTotalYears(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,41);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,31.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,68.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,72);
        }else{
            clickByLocationByPercent(50,41);
        }
    }
    @SneakyThrows
    public void selectTotalYears(String screenShotName3,String screenShotName4){
        Thread.sleep(1000);
        //update
        //31.4
        //41.3
        //51.2
        //63.6
        clickTotalYears();
        Thread.sleep(1000);
        scrollUpForWorkYears();
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickSelectItemFinish();
        Thread.sleep(1000);
        takeScreenshot(screenShotName4);
    }

    public void clickIncomeTypeSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,51);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,41.3);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,76.2);
        }else if(isIphone13()){
//            clickByLocationByPercent2(50,17);
            clickByLocationByPercent2(50,80);
        }else{
            clickByLocationByPercent(50,51);
        }
    }
    @SneakyThrows
    public void selectIncomeType(String incomeType, String screenShotName5){
        Thread.sleep(1000);
        clickIncomeTypeSelector();
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
        if(incomeType.contains("unemployed")){
            clickElement(incomeUnemployed);
        }else{
            if(isShow(incomeItem,2)){
                clickElement(incomeItem);
            }else{
                clickElement(incomeItem2);
            }
        }
    }

    public void clickIncomeAmountInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,62);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,51.2);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,84.9);
        }else if(isIphone13()){
//            clickByLocationByPercent2(50,26);
            clickByLocationByPercent2(50,16);
        }else{
            clickByLocationByPercent(50,62);
        }
    }

    @SneakyThrows
    public void inputMonthlyIncome(String monthlyIncomeAmount,String screenShotName6){
        Thread.sleep(1000);
        clickIncomeAmountInput();
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString(monthlyIncomeAmount);
        Thread.sleep(1000);
        takeScreenshot(screenShotName6);
        if(isIphone13()){
//            clickByLocationByPercent(50,31);// To hide keyboard
            clickByLocationByPercent(50,21);// To hide keyboard
        }else{
            clickByLocationByPercent(50,54);// To hide keyboard
        }
    }

    public void clickSalaryType(){
        if(isIphoneSE()){
            clickByLocationByPercent(15,76);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(15,63.6);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(15,30.9);
        }else if(isIphone13()){
//            clickByLocationByPercent2(15,37);
            clickByLocationByPercent2(15,27);
        }else{
            clickByLocationByPercent(15,76);
        }
    }

    @SneakyThrows
    public void selectSalaryType(String screenShotName7){
        Thread.sleep(1000);
        clickSalaryType();
        Thread.sleep(1000);
        takeScreenshot(screenShotName7);
    }

    @SneakyThrows
    public void FillAboutYourJob(String monthlyIncomeAmount,String incomeType,String Building,
                                 String screenShotName,String screenShotName1,String screenShotName2,
                                 String screenShotName3,String screenShotName4,String screenShotName5,
                                 String screenShotName6,String screenShotName7,String screenShotName8,
                                 String screenShotName9,String screenShotName10,String screenShotName11
                                 ){

        waitUntilElementVisible(aboutYourJobPage);
        Thread.sleep(1000);
        fillCompanyName();
        selectPositionItem(screenShotName);
        selectPositionType(screenShotName1);
        selectPositionDetail();
        inputPositionName();
        selectCurrentCompanyYears(screenShotName2);

        if(!isIphoneXR()){
            if(isIphone13()){
            }else {
                scrollUpByStartEndHeightPercent(80, 30);
            }
        }
        selectTotalYears(screenShotName3, screenShotName4);
        selectIncomeType(incomeType, screenShotName5);
        if(isIphone13()){
            scrollUp();
            scrollUp();
            Thread.sleep(1000);
        }
        inputMonthlyIncome(monthlyIncomeAmount, screenShotName6);
        if(!isIphoneXR()){
            selectSalaryType(screenShotName7);
        }

        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        if(isIphoneXR()){
            selectSalaryType(screenShotName7);
        }
        selectArea(screenShotName8);
        inputStreetName(screenShotName9);
        inputBuildingName(Building, screenShotName10);
        inputFloorName();
        inputCellName();
        inputPhoneNumber(screenShotName11);
        clickNextButtonAfterFillPhoneNumber();
    }

    public void clickNextButtonAfterFillPhoneNumber(){
        clickByLocationByPercent2(50,62.6);//iphone se iphone7 plus
    }

    public void clickAreaSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,29);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,36);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,45.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,43);
        }else{
            clickByLocationByPercent(50,29);
        }
    }

    @SneakyThrows
    public void selectArea(String screenShotName8){
        Thread.sleep(1000);
        clickAreaSelector();
        Thread.sleep(1000);
        takeScreenshot(screenShotName8);
        clickElement(areaItem);
    }

    public void clickStreetNameInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,40);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,45);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,52.5);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,51);
        }else{
            clickByLocationByPercent(50,40);
        }
    }

    @SneakyThrows
    public void inputStreetName(String screenShotName9){
        Thread.sleep(1000);
        //clickByLocationByPercent(50,40);
        clickStreetNameInput();
        Thread.sleep(1000);
        String street="mystreet";
        commonPage.clickSoftKeyByNumberString(street);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
        takeScreenshot(screenShotName9);
    }
    public void clickBuildingNameInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,49);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,54);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,61.1);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,59);
        }else{
            clickByLocationByPercent(50,49);
        }
    }

    @SneakyThrows
    public void inputBuildingName(String Building, String screenShotName10){
        scrollUp();
        Thread.sleep(200);
        clickBuildingNameInput();
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString(Building);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
        takeScreenshot(screenShotName10);
    }
    public void clickFloorNameInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(20,60);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(20,63.6);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,68.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,67);
        }else{
            clickByLocationByPercent(20,60);
        }
    }
    @SneakyThrows
    public void inputFloorName(){
        clickFloorNameInput();
        Thread.sleep(1000);
        String floor="AA";
        commonPage.clickSoftKeyByNumberString(floor);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
    }

    public void clickCellNameInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(70,60);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(70,63.6);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(70,68.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(70,67);
        }else{
            clickByLocationByPercent(70,60);
        }
    }

    @SneakyThrows
    public void inputCellName(){
        clickCellNameInput();
        Thread.sleep(1000);
        String unit="BB";
        commonPage.clickSoftKeyByNumberString(unit);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
    }

    public void clickPhoneNumberInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,70);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,72.7);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,76.9);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,75);
        }else{
            clickByLocationByPercent(50,70);
        }
    }

    @SneakyThrows
    public void inputPhoneNumber(String screenShotName11){
        //phone number
        clickByLocationByPercent(50,71);
        clickPhoneNumberInput();
        Thread.sleep(1000);
        String phoneNumber="22999999";
        commonPage.clickSoftKeyByNumberString(phoneNumber);
        Thread.sleep(1000);
        takeScreenshot(screenShotName11);
    }




    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"償還信用卡/私人貸款\"])[2]")
    private MobileElement loansPurposeCreditCardOrLoanRepayment;
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"買車\"])[2]")
    private MobileElement loansPurposeBuyCard;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"碩士或以上\"])[2]")
    private MobileElement educationItem;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"自置（沒有按揭貸款）\"])[2]")
    private MobileElement houseItem;



    public void clickDiscountCodeInput(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,0);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,65);
        }else{
            clickByLocationByPercent(50,0);
        }
    }

    public void inputDiscountCode() {
        clickDiscountCodeInput();
        commonPage.inputMSKByString("123456");
        clickByLocationByPercent(70,20); //hide keyboard
    }

    public void clickEducationSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,24);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,21.5);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,20.1);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,22);
        }else{
            clickByLocationByPercent(50,24);
        }
    }
    @SneakyThrows
    public void selectEducation(String screenShotName) {
        waitUntilElementVisible(aboutYourMoreInfomationPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        Thread.sleep(1000);
        clickEducationSelector();
        Thread.sleep(1000);
        clickElement(educationItem);
    }

    public void clickHouseSelector(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,35);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,31.5);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,28.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,30);
        }else{
            clickByLocationByPercent(50,35);
        }
    }
    @SneakyThrows
    public void selectHouse() {
        Thread.sleep(1000);
        clickHouseSelector();
        Thread.sleep(1000);
        clickElement(houseItem);
    }

    public void clickPhoneNumberInputForMoreInformation(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,45);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,40.5);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,35.9);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,38);
        }else{
            clickByLocationByPercent(50,45);
        }
    }

    @SneakyThrows
    public void inputPhoneNumberForMoreInformation(String screenShotName) {
        Thread.sleep(1000);
        clickPhoneNumberInputForMoreInformation();
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString("22888888");
        Thread.sleep(1000);
        clickByLocationByPercent(50,14);  //hide keyboard
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    public void clickLoansPurposeItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,56);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,50.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,43.8);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,47);
        }else{
            clickByLocationByPercent(50,56);
        }
    }

    @SneakyThrows
    public void selectLoansPurposeItem(String Purpose, String screenShotName) {
        clickLoansPurposeItem();
        Thread.sleep(1000);
        if(Purpose.contains("CreditCardOrLoanRepayment")){
            clickElement(loansPurposeCreditCardOrLoanRepayment);
        }else{
            clickElement(loansPurposeBuyCard);
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void clickNextButtonForAboutYourMoreInformation(){
        Thread.sleep(1000);
        clickByLocationByPercent2(50,92.5);//iphone se iphone7 plus
        waitUntilElementVisible(confirmInfomationPage);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void waitConfirmInformationPage(){
        waitUntilElementVisible(confirmInfomationPage);
        Thread.sleep(1000);
    }

    public void clickFirstCircleForConfirmLoansInformation(){
        if(isIphoneSE()){
            clickByLocationByPercent(7,66);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(8,69);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(7.7,74.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(8,73);
        }else{
            clickByLocationByPercent(7,66);
        }
    }

    public void clickSecondCircleForConfirmLoansInformation(){
        if(isIphoneSE()){
            clickByLocationByPercent(7,74);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(8,76.4);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(7.7,80.57);
        }else if(isIphone13()){
            clickByLocationByPercent2(8,79.4);
        }else{
            clickByLocationByPercent(7,74);
        }
    }

    public void clickFirstCircleForRfFlowConfirmLoansInformation(){
        if(isIphoneSE()){
            clickByLocationByPercent(7.8,63.1);
        }else{
            clickByLocationByPercent(7.8,66);
        }
    }

    public void clickSecondCircleForRfFlowConfirmLoansInformation(){
        if(isIphoneSE()){
            clickByLocationByPercent(7.8,71.2);
        }else{
            clickByLocationByPercent(7.8,66);
        }
    }

    @SneakyThrows
    public void clickCircleAndConfirmButton(String screenShotName9){
        clickFirstCircleForConfirmLoansInformation();
        clickSecondCircleForConfirmLoansInformation();
        Thread.sleep(1000);
        takeScreenshot(screenShotName9);
        Thread.sleep(1000);
        clickByLocationByPercent(50,93);//confirm
    }

    @SneakyThrows
    public void knowMoreAboutYouForDC(String Purpose, String Building,
                                 String screenShotName,String screenShotName1,String screenShotName2,
                                 String screenShotName3,String screenShotName4,String screenShotName5,
                                 String screenShotName6){
        selectEducation(screenShotName);
        selectHouse();
        inputPhoneNumberForMoreInformation(screenShotName1);
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        clickNextButtonForAboutYourMoreInformation();
        editInformationInReviewPage(Building, screenShotName4);
        for (int i = 0; i < 3; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
        clickCircleAndConfirmButton(screenShotName6);
    }

    @SneakyThrows
    public void knowMoreAboutYou(String Purpose, String Building,
                                 String screenShotName,String screenShotName1,String screenShotName2,
                                 String screenShotName3,String screenShotName4,String screenShotName5,
                                 String screenShotName6){

        selectEducation(screenShotName);
        selectHouse();
        inputPhoneNumberForMoreInformation(screenShotName1);
        selectLoansPurposeItem(Purpose, screenShotName2);

        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName3);
        //inputDiscountCode();

        clickNextButtonForAboutYourMoreInformation();
        editInformationInReviewPage(Building, screenShotName4);
        for (int i = 0; i < 3; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName5);
        clickCircleAndConfirmButton(screenShotName6);

    }

    @SneakyThrows
    public void openWorkInfoPage(){
        scrollUp();
        Thread.sleep(1000);
        int yPercent = getYlocation2("src/main/resources/images/channel/maintenance/workInfoItem.png",50,10);
        clickByLocationByPercent(87, yPercent);
    }

    @SneakyThrows
    public void updateCompanyName(){
        scrollUp();
        Thread.sleep(200);
        openWorkInfoPage();
        Thread.sleep(1000);
        clickByLocationByPercent(50,22);
        commonPage.inputDeleteByNumber(3);
        commonPage.clickSoftKeyByNumberString(generateRandomString());
        commonPage.checkAndclickSoftKeyReturn();
        Thread.sleep(200);
        clickBottomConfirmButton();
        Thread.sleep(1000);
    }
    public String generateRandomString() {
        String result = "";
        for (int i = 0; i <3; i++) {
            int intVal = (int) (Math.random()*26+97);
            result += (char) intVal;
        }
        return result;
    }

    @SneakyThrows
    public void ConfirmInformationForRfFlowIOS(String screenShotName,String screenShotName1){
        waitConfirmInformationPage();
        updateCompanyName();
        for (int i = 0; i < 3; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickFirstCircleForRfFlowConfirmLoansInformation();
        clickSecondCircleForRfFlowConfirmLoansInformation();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        Thread.sleep(1000);
        clickByLocationByPercent(50,93);//confirm
    }

    public void clickResidentialIInformationEditButton(){
        if(isIphoneSE()){
            clickByLocationByPercent2(87.5, 88.83);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(88,68.7);
        }else if(isIphone13()){
            clickByLocationByPercent2(88,73);
        }else{
            clickByLocationByPercent2(87.5, 88.83);
        }
    }

    public void clickBuildingInput(){
        if(isIphoneSE()){
            clickByLocationByPercent2(50, 41.7);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,33.8);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,35.5);
        }else{
            clickByLocationByPercent2(50, 41.7);
        }
    }

    @SneakyThrows
    public void editInformationInReviewPage(String Building,String screenShotName){
        clickResidentialIInformationEditButton();
        Thread.sleep(3000);
        clickBuildingInput();
        commonPage.inputDeleteByNumber(14);
        commonPage.clickSoftKeyByNumberString(Building);
        commonPage.checkAndclickSoftKeyReturn();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickBottomConfirmButton();
    }

    @SneakyThrows
    public void checkAfterSubmitApplicationConfirmCongratulationsPage(String screenShotName){
        // when building  value is sampleRisk08, then can see the page
        if(isShow(afterSubmitApplicationConfirmCongratulationsPage,30)){
            scrollUp();
            scrollUp();
            Thread.sleep(1000);
            takeScreenshot(screenShotName);
            clickBottomConfirmButton();
        }
    }
    @SneakyThrows
    public void checkThePageAfterSubmitApplication(String screenShotName, String screenShotName1,
                                                   String screenShotName2, String screenShotName3){

        if(isShow(loansprotocolReadyingAfterFillInformation, 20)){
            Thread.sleep(1000);
            takeScreenshot(screenShotName);
        }
        checkAfterSubmitApplicationConfirmCongratulationsPage(screenShotName1);
        if(isShow(uploadFilePage,5)){
            Thread.sleep(1000);
            takeScreenshot(screenShotName2);
            //leaveFromUploadPage();
        }
        Thread.sleep(3000);
        takeScreenshot(screenShotName3);
    }

    @SneakyThrows
    public void checkThePageAfterSubmitApplicationRfFlow(String screenShotName){
        if(isShow(loansprotocolReadyingAfterFillInformation)){
            Thread.sleep(1000);
            takeScreenshot(screenShotName);
        }
        takeScreenshot(screenShotName);
    }

    public void leaveFromUploadPage(){
        clickByLocationByPercent(94,6);//close
        if(isShow(leaveButton)){
            clickElement(leaveButton);
        }
    }


    public void clickBottomConfirmButton(){
        clickByLocationByPercent2(50,93.5);
    }

    @SneakyThrows
    public void checkPageWhenSkipUploadPictureAfterSubmitApplication(String screenShotName,String screenShotName1){
        checkAfterSubmitApplicationConfirmCongratulationsPage(screenShotName);
        waitUntilElementVisible(thePageWhenSkipUploadPicture);
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
    }

    //@iOSXCUITFindBy(xpath = "//*[contains(@name ,\"恭喜你！ 你的貸款申請已獲初步審批\")]")
    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"恭喜你\")]")
    private MobileElement afterSubmitApplicationConfirmCongratulationsPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"貸款協議準備中\")]")
    private MobileElement loansprotocolReadyingAfterFillInformation;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"WeLab Bank 私人貸款 我的貸款 貸款協議準備中 我哋準備緊你嘅貸款協議，希望喺一至兩個工作日內完成並透過電郵和短訊通知你\")]")
    private MobileElement thePageWhenSkipUploadPicture;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"WeLab Bank 私人貸款 我的貸款 貸款協議準備中\")]")
    private MobileElement uploadPictureFinishPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"覆核及確認\")]")
    private MobileElement confirmInfomationPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"更多有關你嘅資料\")]")
    private MobileElement aboutYourMoreInfomationPage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"離開\"]")
    private MobileElement leaveButton;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'上載文件')]")
    private MobileElement uploadFilePage;
    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'收入證明')]")
    private MobileElement uploadContentPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'收入證明 (固定收入人仕適用)')]")
    private MobileElement incomeUploadContentPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,'水電煤費單或電話賬單')]")
    private MobileElement addressUploadContentPage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"允许访问所有照片\"]")
    private MobileElement allowAccessPictureLibrary;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"將呢份文件用作住址證明？\" AND name == \"text\"")
    private MobileElement isUseThisFileForAddressProve;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"唔需要\" AND name == \"text\""),
            @iOSXCUITBy(iOSNsPredicate = "label == \"唔需要\""),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"唔需要\"]"),
    })
    private MobileElement donNotNeedButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"知道喇\"]")
    private MobileElement IKnowButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"完成\"]")
    private MobileElement selectPictureFinishButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"PhotoCapture\"]")
    private MobileElement PhotoCapture;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"使用照片\"]")
    private MobileElement usePhoto;

    @iOSXCUITFindBy(xpath = "//*[ends-with(@name, '圖庫')]")
    private MobileElement pictureLibrary;


    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@name=\"最近项目\"]"),
            @iOSXCUITBy(xpath = "//*[@name=\"最近項目\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"最近項目\""),
    })
    private MobileElement lastPictureLibrary;

    public void clickUploadButton(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,55);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,43.1);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,46);
        }else{
            clickByLocationByPercent(50,55);
        }
    }

    @SneakyThrows
    public void goToUploadPage(){
        waitUntilElementVisible(uploadFilePage);
        clickUploadButton();
        waitUntilElementVisible(uploadContentPage);
        Thread.sleep(1000);
    }

    public void clickIncomeProveItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,34);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,28);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,29);
        }else{
            clickByLocationByPercent(50,34);
        }
    }

    @SneakyThrows
    public void selectIncomeProveItem(String screenShotName){
        takeScreenshot(screenShotName);
        clickIncomeProveItem();
        waitUntilElementVisible(incomeUploadContentPage);
        Thread.sleep(3000);
    }

    public void clickAddButtonForIncomeProve(){
        if(isIphoneSE()){
            clickByLocationByPercent(20,65);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,47.4);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,49);
        }else{
            clickByLocationByPercent(20,65);
        }
    }

    @SneakyThrows
    public void selectPictureForIncomeProve(String screenShotName){
        takeScreenshot(screenShotName);
        clickAddButtonForIncomeProve();
        Thread.sleep(3000);
        getPictureByLibrary1();
        //getPictureByPhoto();
    }

    @SneakyThrows
    public void clickUploadButtonForEachItem(){
        if(isIphoneSE()){
            clickByLocationByPercent2(50,93.5);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else{
            clickByLocationByPercent2(50,93.5);
        }
    }

    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, \"度身訂造計劃\")]")
    private MobileElement showUploadItemPage;

    public void checkShowUploadItemPage(){
        if(!isShow(showUploadItemPage,3)){
            goToUploadPage();
        }
    }



    @SneakyThrows
    public void uploadFiles(String screenShotName,String screenShotName1,String screenShotName2,
                            String screenShotName3,String screenShotName4,String screenShotName5,
                            String screenShotName6,String screenShotName7,String screenShotName8,
                            String screenShotName9,String screenShotName10){
        checkShowUploadItemPage();
        uploadProofForIncome(screenShotName,screenShotName1,screenShotName2,screenShotName3);
        isUseThisFileForAddressProve();
        uploadProofForAddress(screenShotName4,screenShotName5,screenShotName7,screenShotName8);
        UploadButtonForAll(screenShotName9, screenShotName10);
    }
    @SneakyThrows
    public void uploadFilesWithEkyc(String screenShotName,String screenShotName1,String screenShotName2,
                                                           String screenShotName3,String screenShotName4,String screenShotName5,
                                                           String screenShotName6,String screenShotName7,String screenShotName8,
                                                           String screenShotName9,String screenShotName10){
        checkShowUploadItemPage();
        uploadProofForIncome(screenShotName,screenShotName1,screenShotName2,screenShotName3);
        isUseThisFileForAddressProve();
        uploadProofForAddress(screenShotName4,screenShotName5,screenShotName7,screenShotName8);

        checkEkycPage(screenShotName9);
        clickUploadButtonForEachItem();
        Thread.sleep(15000);
        takeScreenshot(screenShotName10);
    }


    @SneakyThrows
    public void uploadFilesWithCreditCardPersonalLoanProof(String screenShotName,String screenShotName1,String screenShotName2,
                            String screenShotName3,String screenShotName4,String screenShotName5,
                            String screenShotName6,String screenShotName7,String screenShotName8,
                            String screenShotName9,String screenShotName10){
        checkShowUploadItemPage();
        uploadProofForIncome(screenShotName,screenShotName1,screenShotName2,screenShotName3);
        isUseThisFileForAddressProve();
        uploadProofForAddress(screenShotName4,screenShotName5,screenShotName7,screenShotName8);
        uploadCreditCardPersonalLoanProof();
        UploadButtonForAll(screenShotName9, screenShotName10);
    }

    public void clickCreditCardPersonalLoanProveItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,60);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,46.7);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,50);
        }else{
            clickByLocationByPercent(50,60);
        }
    }

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"請提供你想清還信用卡/私人貸款的文件\")]")
    private MobileElement uploadCreditCardPersonalLoanProofPage;


    public void clickAddButtonForCreditCardPersonalLoanProof(){
        if(isIphoneSE()){
            clickByLocationByPercent2(20,61.6);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,45.3);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,52);
        }else{
            clickByLocationByPercent2(20,61.6);
        }
    }

    @SneakyThrows
    public void uploadCreditCardPersonalLoanProof(){
        clickCreditCardPersonalLoanProveItem();
        waitUntilElementVisible(uploadCreditCardPersonalLoanProofPage);
        Thread.sleep(3000);
        selectPictureForCreditCardPersonalLoanProof();
        clickUploadButtonForEachItem();
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void selectPictureForCreditCardPersonalLoanProof(){
        clickAddButtonForCreditCardPersonalLoanProof();
        Thread.sleep(3000);
        getPictureByLibrary2();
        //getPictureByPhoto();
    }

    @SneakyThrows
    public void uploadProofForIncome(String screenShotName,String screenShotName1,String screenShotName2,
                                     String screenShotName3){
        selectIncomeProveItem(screenShotName1);
        selectPictureForIncomeProve(screenShotName2);
        takeScreenshot(screenShotName3);
        clickUploadButtonForEachItem();
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void isUseThisFileForAddressProve(){
        waitUntilElementVisible(isUseThisFileForAddressProve);
        Thread.sleep(1000);
        clickElement(donNotNeedButton);
    }

    @SneakyThrows
    public void uploadProofForAddress(String screenShotName4,String screenShotName5,String screenShotName7,
                                     String screenShotName8){
        selectAddressProveItem(screenShotName4);
        selectPictureForAddressProve(screenShotName5);
        takeScreenshot(screenShotName7);
        clickUploadButtonForEachItem();
        Thread.sleep(8000);
        takeScreenshot(screenShotName8);
    }


    @SneakyThrows
    public void UploadButtonForAll(String screenShotName9, String screenShotName10){
        takeScreenshot(screenShotName9);
        clickUploadButtonForEachItem();
        waitUntilElementVisible(uploadPictureFinishPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName10);
    }

    @SneakyThrows
    public void clickAddressProveItem(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,47);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,37);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,40);
        }else{
            clickByLocationByPercent(50,47);
        }
    }

    @SneakyThrows
    public void selectAddressProveItem(String screenShotName){
        takeScreenshot(screenShotName);
        clickAddressProveItem();
        waitUntilElementVisible(addressUploadContentPage);
        Thread.sleep(3000);
    }

    public void clickAddButtonForAddressProve(){
        if(isIphoneSE()){
            clickByLocationByPercent(20,53);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(20,41.7);
        }else if(isIphone13()){
            clickByLocationByPercent2(20,43);
        }else{
            clickByLocationByPercent(20,53);
        }
    }

    @SneakyThrows
    public void selectPictureForAddressProve(String screenShotName){
        takeScreenshot(screenShotName);
        clickAddButtonForAddressProve();
        Thread.sleep(3000);
        getPictureByLibrary2();
        //getPictureByPhoto();
    }

    public void clickPhotoItemForUploadPicture(){
        if(isIphoneSE()){
            clickByLocationByPercent(50,73);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,79);
        }else{
            clickByLocationByPercent(50,73);
        }
    }

    @SneakyThrows
    public void getPictureByPhoto(){
        clickPhotoItemForUploadPicture();
        Thread.sleep(2000);
        clickIKnowButton();
        Thread.sleep(3000);
        clickElement(PhotoCapture);
        Thread.sleep(2000);
        clickElement(usePhoto);
        Thread.sleep(15000);
    }

    @SneakyThrows
    public void getPictureByLibrary1(){
        clickPictureLibrary();
        clickIKnowButton();
        if(isShow(allowAccessPictureLibrary,1)){
            clickElement(allowAccessPictureLibrary);
            Thread.sleep(1000);
        }
        clickLastPictureLibrary();
        //select first picture
        clickByLocationByPercent(15,17);
        Thread.sleep(1000);
        clickFinishForSelectPicture();
    }

    @SneakyThrows
    public void getPictureByLibrary2(){
        clickPictureLibrary();
        Thread.sleep(2000);
        clickIKnowButton();
        if(isShow(allowAccessPictureLibrary,1)){
            clickElement(allowAccessPictureLibrary);
            Thread.sleep(1000);
        }
        clickLastPictureLibrary();
        Thread.sleep(1000);
        //select second picture
        clickByLocationByPercent(37,17);
        Thread.sleep(1000);
        clickFinishForSelectPicture();
    }

    @SneakyThrows
    public void clickIKnowButton(){
        if(isShow(IKnowButton,2)){
            clickElement(IKnowButton);
        }
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void clickFinishForSelectPicture(){
        clickElement(selectPictureFinishButton);
        //clickByLocationByPercent(93,6);
        Thread.sleep(15000);
    }

    @SneakyThrows
    public void clickPictureLibrary(){
        //waitUntilElementVisible(pictureLibrary);
        //clickElement(pictureLibrary);
        if(isIphoneSE()){
            clickByLocationByPercent(50,81);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent2(50,86);
        }else if(isIphone13()){
            clickByLocationByPercent2(50,85);
        }else{
            clickByLocationByPercent(50,81);
        }
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void clickLastPictureLibrary(){
        waitUntilElementVisible(lastPictureLibrary);
        clickElement(lastPictureLibrary);
        Thread.sleep(1000);
    }

    public void clickImmediatelyCheckButton(){
        if(isIphoneSE()){
            clickByLocationByPercent(50, 59);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else{
            clickByLocationByPercent(50, 59);
        }
    }

    public void clickAgreementCircle(){
        if(isIphoneSE()){
            clickByLocationByPercent2(7, 75.7);
        }else if(isIphone7Plus()){
            clickByLocationByPercent(0,0);
        }else if(isIphoneXR()){
            clickByLocationByPercent(7.8,81.6);
        }else{
            clickByLocationByPercent2(7, 75.7);
        }
    }

    public void clickAgreeButton(){
        clickByLocationByPercent(50, 94);
    }

    public void clickIknowButton(){
        clickByLocationByPercent(50, 93);
    }

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"貸款協議已經準備好\")]")
    private MobileElement confrimProtocolPage;

    @iOSXCUITFindBy(xpath = "//*[contains(@name ,\"我哋依家正處理緊你嘅貸款\")]")
    private MobileElement dealLoansPage;

    @SneakyThrows
    public void PILNotificationLoanDisbursedIOS(String screenShotName,String screenShotName1,String screenShotName2,
                                     String screenShotName3,String screenShotName4,String screenShotName5,
                                     String screenShotName6) {

        waitUntilElementVisible(confrimProtocolPage);
        takeScreenshot(screenShotName);
        clickImmediatelyCheckButton();
        Thread.sleep(3000);

        scrollUp();
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName1);
        clickAgreementCircle();
        Thread.sleep(1000);

        takeScreenshot(screenShotName2);
        clickAgreeButton();
        Thread.sleep(4000);
        commonPage.inputMSKByString("123456");
        Thread.sleep(15000);

        waitUntilElementVisible(dealLoansPage);
        if(isShow(dealLoansPage)){
            takeScreenshot(screenShotName3);
            clickIknowButton();
            Thread.sleep(6000);
        }

        //Then go to loans page ,did not have next 2 steps
        //click Finish
//        takeScreenshot(screenShotName4);
//        clickByLocationByPercent(50, 93);
//        Thread.sleep(5000);

        //click contribution
//        takeScreenshot(screenShotName5);
//        clickByLocationByPercent(50, 28);
//        Thread.sleep(3000);
//        takeScreenshot(screenShotName6);
//        Thread.sleep(3000);

    }

    @SneakyThrows
    public void goToLoanEditPageByPicture(){
        clickByPicture2("src/main/resources/images/loans/01_ApplyLoansItem.png",50,50);
        Thread.sleep(3000);

        takeScreenshot("loans0002");
        scrollUp();
        Thread.sleep(2000);
        takeScreenshot("loans0003");
        clickByPicture("src/main/resources/images/loans/02_ApplyBtn.png",50,60);
        Thread.sleep(6000);

        for (int i = 0; i < 20; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot("loans0004");
        clickByPicture2("src/main/resources/images/loans/03_AgreeBtn.png",50,10);
        clickByLocationByPercent(50,88);
        Thread.sleep(5000);
        takeScreenshot("loans0005");
        Thread.sleep(2000);

        setPersonalInfo();
        setMoreInfomation();
    }

    @SneakyThrows
    public void setPersonalInfo(){
        //company name
        clickByPicture("src/main/resources/images/loans/04_CompanyItem.png",50,50);
        Thread.sleep(1000);

        String company="Welab";
        commonPage.inputDeleteByNumber(company.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(company);
        commonPage.clickSoftKeyReturn();

        //postion
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/05_BusinessItem.png",50,40);
        Thread.sleep(1000);
        takeScreenshot("loans0006");
        clickByPicture2("src/main/resources/images/loans/06_BusinessSelectedItem.png",50,30);

        //position type
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/07_ServieTypeItem.png",50,30);
        Thread.sleep(1000);
        takeScreenshot("loans0007");
        clickByPicture2("src/main/resources/images/loans/08_ServieTypeSelectedItem.png",50,40);

        //intput position
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/09_PositionItem.png",50,30);
        Thread.sleep(1000);
        String position="qa";
        commonPage.inputDeleteByNumber(position.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(position);
        commonPage.clickSoftKeyReturn();

        //current company years
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/10_CurrentCompanyYears.png",50,30);
        Thread.sleep(2000);
        takeScreenshot("loans0008");
        clickByPicture2("src/main/resources/images/loans/11_FinishBtn.png",50,30);
        // total years
        Thread.sleep(1000);
        takeScreenshot("loans0008_2");
        clickByPicture2("src/main/resources/images/loans/10_TotalYears.png",50,30);
        Thread.sleep(2000);
        takeScreenshot("loans0009");
        clickByPicture2("src/main/resources/images/loans/11_FinishBtn.png",50,30);

        Thread.sleep(1000);
        scrollUp();
        takeScreenshot("loans0010");

        //income
        Thread.sleep(3000);
        clickByPicture2("src/main/resources/images/loans/12_IncomeItem.png",80,10);
        Thread.sleep(1000);
        takeScreenshot("loans0011");
        clickByPicture2("src/main/resources/images/loans/13_IncomeSelectedItem.png",50,30);

        //amount input
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/15_MonthIncomeItem.png",50,30);
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString("50000");
        Thread.sleep(1000);
        takeScreenshot("loans0012");
        clickByPicture2("src/main/resources/images/loans/16_TypeNameItem.png",50,50);

        //income type
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/17_IncomeTypeItem.png",50,25);
        Thread.sleep(1000);
        takeScreenshot("loans0013");
        clickByPicture2("src/main/resources/images/loans/16_TypeNameItem.png",50,50);

        //area
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/20_AreaItem.png",50,60);
        Thread.sleep(1000);
        takeScreenshot("loans0014");
        clickByPicture2("src/main/resources/images/loans/21_AreaSelectedItem.png",50,30);

        Thread.sleep(1000);
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot("loans0015");


        //street name
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/22_StreetItem.png",50,50);
        Thread.sleep(1000);
        String street="mystreet";
        commonPage.inputDeleteByNumber(street.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(street);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
        takeScreenshot("loans0016");

        //build name
        clickByPicture2("src/main/resources/images/loans/23_BuildingItem.png",50,30);
        Thread.sleep(1000);
        String build="A sampleRisk&01& A";
        commonPage.inputDeleteByNumber(build.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(build);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
        takeScreenshot("loans0017");

        //floor
        clickByPicture2("src/main/resources/images/loans/24_FloorItem.png",50,50);
        Thread.sleep(1000);
        String floor="AA";
        commonPage.inputDeleteByNumber(floor.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(floor);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);
        //unit
        clickByPicture2("src/main/resources/images/loans/25_UnitItem.png",50,50);
        Thread.sleep(1000);
        String unit="BB";
        commonPage.inputDeleteByNumber(unit.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(unit);
        commonPage.clickSoftKeyReturn();
        Thread.sleep(1000);

        //phone number
        clickByPicture2("src/main/resources/images/loans/26_CompanyPhoneItem.png",50,10);
        Thread.sleep(1000);
        String phoneNumber="23456789";
        commonPage.inputDeleteByNumber(phoneNumber.length());
        commonPage.changeKeyboardToLower();
        commonPage.clickSoftKeyByNumberString(phoneNumber);
        Thread.sleep(1000);
        takeScreenshot("loans0018");

        //next button
        clickByPicture2("src/main/resources/images/loans/27_NextBtn.png",50,20);
        Thread.sleep(8000);
        takeScreenshot("loans0019");
    }

    @SneakyThrows
    public void setMoreInfomation(){
        //xueli
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/28_EducationItem.png",50,30);
        Thread.sleep(1000);
        takeScreenshot("loans0020");
        clickByPicture2("src/main/resources/images/loans/29_EducationSelectedItem.png",50,20);

        //zhuzhai
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/30_HouseItem.png",50,20);
        Thread.sleep(1000);
        takeScreenshot("loans0021");
        clickByPicture2("src/main/resources/images/loans/31_HouseSelectedItem.png",50,50);

        //phonenumber
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/32_HousePhoneItem.png",50,20);
        Thread.sleep(1000);
        commonPage.clickSoftKeyByNumberString("23456789");
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/33_TopHideKeyBoard.png",50,30);
        Thread.sleep(1000);
        takeScreenshot("loans0021_2");

        //yongtu
        clickByPicture2("src/main/resources/images/loans/34_UseItem.png",50,20);
        Thread.sleep(1000);
        takeScreenshot("loans0022");
        clickByPicture2("src/main/resources/images/loans/35_UseSelectedItem.png",50,20);
        Thread.sleep(1000);
        takeScreenshot("loans0023");
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot("loans0024");

        //click next btn
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/27_NextBtn.png",50,20);
        Thread.sleep(8000);
        takeScreenshot("loans0025");

        for (int i = 0; i < 10; i++) {
            scrollUpFast();
        }
        Thread.sleep(1000);
        takeScreenshot("loans0026");

        //click circle and confirm
        clickByPicture("src/main/resources/images/loans/36_CircleSelectItem.png",8,25);
        Thread.sleep(1000);
        takeScreenshot("loans0027");
        Thread.sleep(1000);
        clickByPicture2("src/main/resources/images/loans/37_ApplicationButton.png",50,15);
        Thread.sleep(8000);
        takeScreenshot("loans0028");
    }


    @SneakyThrows
    public void uploadFilesByPicture(){
        //click upload button
        takeScreenshot("loans0031");
        clickByPicture2("src/main/resources/images/loans/45_UploadButton.png",50,10);
        Thread.sleep(4000);

        //upload files
        takeScreenshot("loans0032");
        clickByPicture2("src/main/resources/images/loans/46_IncomeProof.png",50,30);
        Thread.sleep(3000);

        //click add button
        takeScreenshot("loans0033");
        clickByPicture2("src/main/resources/images/loans/47_AddButton.png",50,70);
        Thread.sleep(3000);

        //click picture library
        takeScreenshot("loans0034");
        clickByPicture2("src/main/resources/images/loans/48_PictureLibaryItem.png",50,40);
        Thread.sleep(3000);

        //click I knows
        takeScreenshot("loans0035");
        clickByPicture2("src/main/resources/images/loans/49_IKnowsButton.png",50,10);
        Thread.sleep(3000);

        //click keep current content
        takeScreenshot("loans0036");
        clickByPicture2("src/main/resources/images/loans/50_KeepCurrent_Content.png",50,30);
        Thread.sleep(3000);

        //click latest picture library
        takeScreenshot("loans0037");
        clickByPicture2("src/main/resources/images/loans/51_LatestPictures.png",50,30);
        Thread.sleep(1000);

        //select first picture
        takeScreenshot("loans0038");
        clickByLocationByPercent(15,17);
        Thread.sleep(1000);

        //click finish
        takeScreenshot("loans0039");
        clickByPicture2("src/main/resources/images/loans/52_FinishItem.png",50,30);
        Thread.sleep(15000);

        //click upload
        takeScreenshot("loans0040");
        clickByPicture2("src/main/resources/images/loans/45_UploadButton.png",50,10);
        Thread.sleep(2000);

        //click ok
        takeScreenshot("loans0041");
        clickByPicture2("src/main/resources/images/loans/53_OKButton.png",50,30);
        Thread.sleep(2000);

        //click finish
        takeScreenshot("loans0042");
        clickByPicture2("src/main/resources/images/loans/54_FinishButton.png",50,10);
        Thread.sleep(8000);
        takeScreenshot("loans0043");
    }

    @SneakyThrows
    public void confirmLoansProtocolByPicture() {

        //click upload button
        takeScreenshot("loans0044");
        clickByPicture2("src/main/resources/images/loans/56_ImmediatelyCheckButton.png",50,15);
        Thread.sleep(3000);

        //click agree
        scrollUp();
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot("loans0045");
        clickByPicture("src/main/resources/images/loans/57_SelectProtocolItem.png",3,20);
        Thread.sleep(1000);

        //click agree
        takeScreenshot("loans0046");
        clickByPicture2("src/main/resources/images/loans/58_AgreeExtractLoans.png",50,10);
        Thread.sleep(4000);

        //input msk
        takeScreenshot("loans0047");
        commonPage.inputMSKByString("123456");
        Thread.sleep(15000);

        //click I Knows
        takeScreenshot("loans0048");
        //clickByLocationByPercent(50, 93);
        Thread.sleep(3000);

        //click Finish
        takeScreenshot("loans0049");
        clickByPicture2("src/main/resources/images/loans/54_FinishButton.png",50,10);
        Thread.sleep(5000);

        //click contribution
        takeScreenshot("loans0050");
        clickByPicture2("src/main/resources/images/loans/59_ContributingLoans.png",80,30);
        Thread.sleep(3000);
        takeScreenshot("loans0051");
        Thread.sleep(3000);

    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name ,'我的貸款 結欠總額 (HKD)')]"),
            @iOSXCUITBy(xpath = "//*[contains(@name ,'結欠總額 (HKD)')]"),
    })
    private MobileElement myloansDetailPage;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"詳情\" AND name == \"text\"")
    private MobileElement repaymentDetail;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"供款金額\"]/following-sibling::XCUIElementTypeStaticText[1]")
    private MobileElement monthlyRepayment;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"逾期費用\"]/following-sibling::XCUIElementTypeStaticText[1]")
    private MobileElement lateCharge;

    @SneakyThrows
    public void openMyLoanIos(String screenShotName){
        Thread.sleep(2000);
        clickMyLoansItem();
        waitUntilElementVisible(myloansDetailPage);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
    }

    public void clickMyLoansItem(){
        clickByLocationByPercent(88, 6);
    }

    public void clickFirstRepaymentAmountOpenButton(){
        clickByLocationByPercent2(42, 62.9);
    }

    public void clickSecondRepaymentAmountOpenButton(){
        clickByLocationByPercent2(42, 76.5);
    }

    @SneakyThrows
    public void checkMyLoansWithDeliquentIOS(String screenShotName){
        clickFirstRepaymentAmountOpenButton();
        getRepaymentDetail(screenShotName);
    }

    @SneakyThrows
    public void checkMyLoansSecondRepaymentWithDeliquentIOS(String screenShotName){
        clickSecondRepaymentAmountOpenButton();
        getRepaymentDetail(screenShotName);
    }

    @SneakyThrows
    public void getRepaymentDetail(String screenShotName){
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        waitUntilElementVisible(repaymentDetail, 5);
        String monthlyRepaymentAmount = monthlyRepayment.getText();
        String lateChargetAmount = lateCharge.getText();
        logger.info("monthlyRepaymentAmount: "+monthlyRepaymentAmount);
        logger.info("lateChargetAmount: "+lateChargetAmount);
    }

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"你的貸款申請已經被拒，我們未能進一步處理你嘅申請。你可以查看電郵了解詳情及聯絡我哋嘅客戶服務團隊查詢。如稍後情況有變，你亦可再次申請。\"")
    private MobileElement rejectApplyLoans;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"知道喇\" AND name == \"text\"")
    private MobileElement confirmRejectApplyLoans;

    public void checkRejectApplyLoans(){
        if(isShow(rejectApplyLoans,3)){
            clickElement(confirmRejectApplyLoans);
        }
    }

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"繁體中文\"])[1]/XCUIElementTypeOther[1]")
    private MobileElement backButtonOnLoginPage;

    public void goToWelabFisrtPage(){
        if(isShow(backButtonOnLoginPage,3)){
            clickElement(backButtonOnLoginPage);
        }
    }

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"申請私人分期貸款\" AND name == \"text\"")
    private MobileElement personalApplyLoansButtonOnFirstPage;

    public void clickPersonalApplyLoans(){
        clickByPicture("src/main/resources/images/loans/01_ApplyLoansItem.png",50,60);
    }

    public void clickAddPictureButton(){
        clickByPicture("src/main/resources/images/loans/addPictureButton.png",50,60);
    }

    @SneakyThrows
    public void applyPersonalLoansWithoutLoginToCheckKFS(String screenShotName){
        goToWelabFisrtPage();
        Thread.sleep(1000);
        clickElement(personalApplyLoansButtonOnFirstPage);
        Thread.sleep(1000);
        clickPersonalApplyLoans();
        Thread.sleep(2000);
        checkPDF(screenShotName);
    }

    @iOSXCUITFindBy(xpath = "//*[starts-with(@name, \"根據閣下環聯個人信貸報告上資料所顯\")]")
    private MobileElement ekycPage;

    public void checkEkycPage(String screenShotName){
        waitUntilElementVisible(ekycPage);
        clickEkycInput();
        String ekycValue="1234";//phone number need get from database
        commonPage.clickSoftKeyByNumberString(ekycValue);
        clickByLocationByPercent2(50, 50);//hide key
        takeScreenshot(screenShotName);
        clickBottomConfirmButton();
        //clickSubmitForEkyc();
    }

    public void clickEkycInput(){
        if(isIphoneSE()){
            clickByLocationByPercent2(0, 0);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(10.3,25.6);
        }else{
            clickByLocationByPercent2(0, 0);
        }
    }

    public void clickSubmitForEkyc(){
        if(isIphoneSE()){
            clickByLocationByPercent2(0, 0);
        }else if(isIphone7Plus()){
            clickByLocationByPercent2(50,66);
        }else{
            clickByLocationByPercent2(0, 0);
        }
    }

}
