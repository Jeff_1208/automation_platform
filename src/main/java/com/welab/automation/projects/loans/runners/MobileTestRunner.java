package com.welab.automation.projects.loans.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = {
                "src/main/java/com/welab/automation/projects/loans/features/mobile_android",
                "src/main/java/com/welab/automation/projects/loans/features/web/TAT-T3079_loanApplicationsSake2.feature",
                "src/main/java/com/welab/automation/projects/loans/features/zmobile_android/TAT-T3251_loansConfirmAndroid.feature",
        },
        glue = {
                "com/welab/automation/projects/loans/steps/mobile",
                //"com/welab/automation/projects/loans/steps/web",
        },
        tags="@RF2",
        monochrome = true)
public class MobileTestRunner extends TestRunner {}