Feature:  Loan Applications Maker

  Background: There are submitted document records

#  Scenario: checker login
#    Given Open the URL https://ploan-portal.sta-wlab.net/
#    And Enter the user name and password and log in
#      | username | maker1 |
#      | password | Aa123456 |
#    And Click the exit login button and click login
#    And Enter the user name and password and log in
#      | username | checker1 |
#      | password | Aa123456 |
#
#  Scenario: checker sign loan to maker
#    Given Open the URL https://ploan-portal.sta-wlab.net/
#    And Enter the user name and password and log in
#      | username | checker1 |
#      | password | Aa123456 |
#    And input the application id and click the button search all loan
#    And click update assign and select one,click Update
#      | selectValue | Ezekiel Wong (CheckerMaker) |

  Scenario: Handle the loan in sake2 with maker and checker
    Given Open the URL https://ploan-portal.sta-wlab.net/
    And Enter the user name and password and log in
      | username | maker1 |
      | password | Aa123456 |
    And Select the corresponding data or query by ID
      | screenShotName               | Reg_maker_001_Android_01 |
    And select pass in theNew mobile number in TU: in the area of Personal info and click Save
    And select all  Maker review valid  in the area of documents
      | status | Pending Review |
      | screenShotName               | Reg_maker_001_Android_02 |
      | screenShotName1               | Reg_maker_001_Android_03 |
    When select Pass in the Address proof Income proof and Employment verification
    And status change to pending checker reason add approved
      | screenShotName               | Reg_maker_001_Android_04 |
      | screenShotName1               | Reg_maker_001_Android_05 |
    And Click the exit login button and click login
    And Enter the user name and password and log in
      | username | checker1 |
      | password | Aa123456 |
    And input the application id and click the button search all loan
    And click update assign and select one,click Update
      | selectValue | Ezekiel Wong (CheckerMaker) |