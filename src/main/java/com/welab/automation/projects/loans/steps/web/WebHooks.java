package com.welab.automation.projects.loans.steps.web;

import atu.testrecorder.ATUTestRecorder;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;

import static com.welab.automation.framework.utils.TimeUtils.getCurrentTimeAsString;

public class WebHooks extends WebBasePage {

  private static final Logger logger = LoggerFactory.getLogger(WebHooks.class);
  BaseDriver baseDriver;
  ATUTestRecorder recorder;
  boolean recordVideo = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("recordVideo"));
  String videoName;
  @Before
  public void beforeScenario(Scenario scenario) throws IOException {
    logger.info("* Start running scenario: {}", scenario.getName());
    this.baseDriver = new BaseDriver();
    this.baseDriver.initDriver();
    System.out.println(BaseDriver.getWebDriver());
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    videoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      logger.info("* Start Web to recording!!!!!!!!!!!!!!");
      recorder = startRecording(recorder, videoName);
    }
    storeCaseInitResult(scenario);
  }

  @After
  public void afterScenario(Scenario scenario) {
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    String screenshotVideoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      logger.info("* Stop Web to recording!!!!!!!!!!!!!!");
      stopRecording(recorder, videoName);
    }

    if (scenario.isFailed()) {
      // Take screenshot to report
      final byte[] screenshot =
          ((TakesScreenshot) BaseDriver.getMobileDriver()).getScreenshotAs(OutputType.BYTES);
      scenario.attach(screenshot, "image/png", screenshotVideoName);
      // Take screenshots to project folder
      takeScreenshot(screenshotVideoName);
    }
    storeCaseResult(scenario);
    logger.info("* End running scenario: {}", scenario.getName());
    BaseDriver.closeDriver();
  }

    public void storeCaseInitResult(Scenario scenario){
        String tag = getCaseTag(scenario);
        if(tag != ""){
            tag = tag.substring(1);
            GlobalVar.CASE_RESULT.put(tag, false);
        }
    }

    public void storeCaseResult(Scenario scenario){
        String tag = getCaseTag(scenario);
        if(tag != ""){
            tag = tag.substring(1);
            GlobalVar.CASE_RESULT.put(tag, !scenario.isFailed());
        }
    }

    public String getCaseTag(Scenario scenario){
        Collection<String> tags = scenario.getSourceTagNames();
        String tag = "";
        for (String t: tags) {
            if(t.startsWith("@VBL")){
                tag = t;
                break;
            }
        }
        return tag;
    }
}
