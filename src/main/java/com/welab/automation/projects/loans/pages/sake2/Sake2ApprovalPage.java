package com.welab.automation.projects.loans.pages.sake2;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Sake2ApprovalPage extends WebBasePage {
  private final String pageName = "Sake2 approval page";
  Sake2MainPage sake2MainPage;
  public Sake2ApprovalPage(){
    sake2MainPage = new Sake2MainPage();
    super.pageName = this.pageName;
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[4]/div[1]/fieldset/div[1]/label/input")
  private WebElement ResidentialInfoPass;
  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[5]/div[1]/fieldset/div[1]/label/input")
  private WebElement WorkInfoPass;
  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[6]/div[1]/fieldset/div[1]/label/input")
  private WebElement PublicDataPass;

  @SneakyThrows
  public void confirmLoansInfomation(){
    sake2MainPage.clickDetail();
    sake2MainPage.sake2SwitchTab();
    clickElement(ResidentialInfoPass);
    scrollByLocation(pageName,0,300);
    Thread.sleep(1000);
    clickElement(WorkInfoPass);
    scrollByLocation(pageName,300,600);
    Thread.sleep(1000);
    scrollByLocation(pageName,600 ,900);
    clickElement(PublicDataPass);
  }


  public void checkByCatchException(int startY,int endY, int scrollHeight, String xpathStart, String xpathEnd){
    try{
      for (int i = 1; i < 100; i++) {
        String income_pass_xpath =xpathStart + i + xpathEnd;
        driver.findElement(By.xpath(income_pass_xpath)).click();
        int Y1 = startY + (i-1)*scrollHeight;
        int Y2 = endY + (i-1)*370;
        scrollByLocation(pageName,Y1 ,Y2);
      }
    }catch(Exception ex) {
      logger.error("Proof Check End!");
    }
  }
  public void checkIncomeAddressProofFirst(){
    checkByCatchException(1200,1400,370,"//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[7]/div[2]/div/div[1]/div[","]/ul/div/li[1]/div[1]/div[1]/label/input");
    scrollByJS(pageName,"var q=document.documentElement.scrollTop=1200");
    checkByCatchException(1200,1400,370,"//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[7]/div[2]/div/div[2]/div[","]/ul/div/li[1]/div[1]/div[1]/label/input");
    scrollToTop(pageName);
  }

  public void checkIncomeAddressProofSecond(){
    checkByCatchException(1400,1700,350,"//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[7]/div[2]/div/div[1]/div[","]/ul/div/li[2]/div[1]/div[1]/label/input");
    scrollByJS(pageName,"var q=document.documentElement.scrollTop=1400");
    checkByCatchException(1400,1700,350,"//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[7]/div[2]/div/div[2]/div[","]/ul/div/li[2]/div[1]/div[1]/label/input");
    scrollToTop(pageName);
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/a/div[2]")
  private WebElement quickEdit;

  @SneakyThrows
  public void clickQuickEdit(){
    clickElement(quickEdit);
    Thread.sleep(2000);
  }

  public void closeWindow(){
    driver.close();
  }

  public void switchToMainPage(){
    closeWindow();
    sake2MainPage.sake2SwitchByUrl(sake2MainPage.loansMainUrl);
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/form/div/div[3]/div/div/div/div[1]/div[1]")
  private WebElement loansReanson;

  @FindBy(how = How.ID, using = "react-select-2-option-0")
  private WebElement reasonItem;

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/form/div/div[8]/div/input")
  private WebElement repayment;

  public void EditLoansInfo(){
    clickQuickEdit();
    clickElement(loansReanson);
    clickElement(reasonItem);
    selectStatus("makerCompleted");
    clearAndSendKeys(repayment,"5");
    clickUpdate();
    switchToMainPage();
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/form/div/div[10]/div[2]/button0")
  private WebElement updateBtn;

  @SneakyThrows
  public void clickUpdate(){
    clickElement(updateBtn);
    Thread.sleep(2000);
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/form/div/div[2]/div/select")
  private WebElement statusSelector;
  public void selectStatus(String status){
    selectByVisibleText(statusSelector,"makerCompleted");
  }

  public void goToDetailPage(){
    sake2MainPage.clickAppliation();
    sake2MainPage.searchLoansID();
    sake2MainPage.clickDetail();
    sake2MainPage.sake2SwitchTab();
  }

  public void secondApproved(){
    checkIncomeAddressProofSecond();
    scrollToTop(pageName);
    clickQuickEdit();
    selectStatus("checkerCompleted");
    clickUpdate();
    switchToMainPage();
  }

}
