package com.welab.automation.projects.loans.runners;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
        features = {"src/main/java/com/welab/automation/projects/loans/features/api/loans/functions"},
        glue = {"com/welab/automation/projects/loans/steps/api"})
public class LoansAPITestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForLoans();

    }
}