package com.welab.automation.projects.loans.pages.sake2;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;
import java.util.Set;

public class Sake2MainPage extends WebBasePage {
  private final String pageName = "Sake2 main page";
  public static String loansApplicationLevel ="";
  public static String loansApplicationID ="";
  public static String loansMainUrl="";
  public Sake2MainPage() {
    super.pageName = this.pageName;
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[8]")
  private WebElement loansLevelEle;

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[11]/select")
  private WebElement assignNameSelector;

  @FindBy(how = How.XPATH, using = "//*[@id=\"navbarDropdown\"]")
  private WebElement accountBtn;

  @FindBy(how = How.XPATH, using = "//*[text()='Log out']")
  private WebElement logoutBtn;

  @FindBy(how = How.XPATH, using = "//*[@id=\"sidebar-container\"]/ul/li[1]/a")
  private WebElement appliationBtn;
  public void clickAppliation() {
    clickElement(appliationBtn);
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[11]/select")
  private WebElement AssignerSelector;
  public void selectAssignerName(String name) {
    selectByVisibleText(AssignerSelector,name);
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"search\"]")
  private WebElement searchInput;
  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[1]/div/form/button")
  private WebElement searchBtn;

  public void searchLoansID(String ID) {
    loansApplicationID = ID;
    loansMainUrl = driver.getCurrentUrl();
    clearAndSendKeys(searchInput,ID);
    clickElement(searchBtn);
  }

  public void searchLoansID(){
    clearAndSendKeys(searchInput,loansApplicationID);
    clickElement(searchBtn);
  }

  @FindBy(how = How.CLASS_NAME, using = "detail")
  private List<WebElement> detailBtn;
  public void clickDetail() {
    clickElement(detailBtn.get(0));
  }

  @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[9]")
  private WebElement authorityLevel;
  public void getloansApplicationLevel() {
    loansApplicationLevel = getElementText(authorityLevel);
  }

  @FindBy(how = How.CSS, using = "button[class='btn btn-light btn-sm mr-1 btn-outline btn btn-secondary']")
  private WebElement assignerBtn;

  @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[1]/div/div/div[2]/div/div[2]/select")
  private WebElement assignerSelector;

  @FindBy(how = How.XPATH, using = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[1]")
  private WebElement assignUpdateBtn;

  public void setAssigner(String assignerName) {
    clickElement(assignerBtn);
    selectByVisibleText(assignerSelector,assignerName);
    clickElement(assignUpdateBtn);
  }

  @SneakyThrows
  public void logout() {
    waitUntilElementClickable(accountBtn);
    clickElement(accountBtn);
    Thread.sleep(1000);
    waitUntilElementClickable(logoutBtn);
    Thread.sleep(1000);
    clickElement(logoutBtn);
  }

  public void sake2SwitchTab() {
    Set<String> handles = driver.getWindowHandles();
    String current_rul=driver.getCurrentUrl();
    for (String s : handles) {
      if(!driver.getCurrentUrl().contains(current_rul)){
        driver.switchTo().window(s);
        break;
      }
    }
  }

  public void sake2SwitchByUrl(String url) {
    Set<String> handles = driver.getWindowHandles();
    for (String s : handles) {
      if(driver.getCurrentUrl().contains(url)){
        driver.switchTo().window(s);
        break;
      }
    }
  }

}
