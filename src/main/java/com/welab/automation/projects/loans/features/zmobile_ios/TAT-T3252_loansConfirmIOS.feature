Feature: Loans Confirm

  Background: Login
    Given Open loans WeLab App
#    And close web driver

  @VBL-T3114
  Scenario: Reg_Loan_004 PIL Notification LOAN DISBURSED IOS
    And Enable Skip Root Rebind Device Authentication IOS
    And get app version loans
    When Login with user and password from properties
    And bind Device Authentication IOS
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_004_IOS_01 |
    Then PIL Notification Loan Disbursed IOS
      | screenShotName   | Reg_Loans_004_IOS_02 |
      | screenShotName1  | Reg_Loans_004_IOS_03 |
      | screenShotName2  | Reg_Loans_004_IOS_04 |
      | screenShotName3  | Reg_Loans_004_IOS_05 |
      | screenShotName4  | Reg_Loans_004_IOS_06 |
      | screenShotName5  | Reg_Loans_004_IOS_07 |
      | screenShotName6  | Reg_Loans_004_IOS_08 |
    Then I go to My loans
      | screenShotName | Reg_Loans_004_IOS_09 |
