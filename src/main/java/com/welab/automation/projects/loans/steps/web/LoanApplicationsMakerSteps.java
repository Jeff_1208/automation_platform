package com.welab.automation.projects.loans.steps.web;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.ZephyrScaleUtils;
import com.welab.automation.projects.loans.pages.sake2.LoanApplicationsCheckerPage;
import com.welab.automation.projects.loans.pages.sake2.LoanApplicationsMakerPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LoanApplicationsMakerSteps {
    LoanApplicationsMakerPage loanApplicationsMakerPage;
    LoanApplicationsCheckerPage loanApplicationsCheckerPage;

    public LoanApplicationsMakerSteps() {
        loanApplicationsMakerPage = new LoanApplicationsMakerPage();
        loanApplicationsCheckerPage = new LoanApplicationsCheckerPage();
    }
    @And("^Click the exit login button and click login$")
    public void outLong() {
        loanApplicationsMakerPage.clickLogin();
    }

    @And("^Enter the user name and password and log in$")
    public void login(Map<String,String> loginData) {
        loanApplicationsMakerPage.login(loginData.get("username"),loginData.get("password"));
    }
    @And("^input the application id and click the button search all loan$")
    public void clicksearch() {
        loanApplicationsMakerPage.searchButton();
    }

    @And("^click update assign and select one,click Update$")
    public void clickupdate(Map<String,String> loginData) {
        assertThat(loanApplicationsMakerPage.clickUpdateAssign(loginData.get("selectValue"))).isTrue();
    }
    @And("^Select the corresponding data or query by ID$")
    public void HKID(Map<String,String> data) {
        ZephyrScaleUtils zephyrScaleUtils = new ZephyrScaleUtils();
        zephyrScaleUtils.getLoansHkid();
        String id= GlobalVar.GLOBAL_VARIABLES.get("hkid");
        if (null!=id){
            loanApplicationsMakerPage.HKid(id,data.get("screenShotName"));
        }
        if (null==id){
            loanApplicationsMakerPage.teraversalQuery();
        }
    }

    @And("get Order Id")
    public void getOrderId(Map<String,String> data) {
        String id= GlobalVar.GLOBAL_VARIABLES.get("hkid");
        String orderId= GlobalVar.GLOBAL_VARIABLES.get("orderId");
        if (null!=orderId){
            loanApplicationsMakerPage.HKid(id,data.get("screenShotName"));
        }
    }

    @Then("back To All Order Page")
    public void backToAllOrderPage() {
        loanApplicationsCheckerPage.backToAllOrderPage();
    }

    @Then("ciclk Order Detail")
    public void ciclkOrderDetail() {
        loanApplicationsMakerPage.clickOrderDetail();
    }

    @And("^select all  Maker review valid  in the area of documents$")
    public void review(Map<String,String> data) {
        loanApplicationsMakerPage.approved(data);
    }
    @And("^select Pass in the Address proof Income proof and Employment verification$")
    public void Pass() {
        loanApplicationsMakerPage.makerOverview();
    }
    @And("^status change to pending checker reason add approved$")
    public void pending(Map<String,String> data) {
        loanApplicationsMakerPage.checker(data);
    }
    @And("^select pass in theNew mobile number in TU: in the area of Personal info and click Save$")
    public void selectTU() {
        loanApplicationsMakerPage.TU();
    }

    @And("maker Approved")
    public void makerApproved(Map<String,String> data) {
        loanApplicationsMakerPage.makerApproved(data);
    }
}
