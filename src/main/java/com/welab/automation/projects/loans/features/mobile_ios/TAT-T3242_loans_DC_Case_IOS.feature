Feature: Loans DC Case

  Background: Login
    Given Open loans WeLab App
    And skip Live Update ios

  Scenario: Reg_Loan_DC_001 Loan DC Flow Landing page and apply loan IOS
    And skip Root ios
    And get app version loans
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_DC_001_IOS_01 |
    Then go To Loans DC Page
    Then check KFS page
      | screenShotName  | Reg_Loans_DC_001_IOS_02 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sampleRisk&82        |
      | screenShotName      | Reg_Loans_DC_001_IOS_04 |
      | screenShotName1     | Reg_Loans_DC_001_IOS_05 |
      | screenShotName2     | Reg_Loans_DC_001_IOS_06 |
      | screenShotName3     | Reg_Loans_DC_001_IOS_07 |
      | screenShotName4     | Reg_Loans_DC_001_IOS_08 |
      | screenShotName5     | Reg_Loans_DC_001_IOS_09 |
      | screenShotName6     | Reg_Loans_DC_001_IOS_10 |
      | screenShotName7     | Reg_Loans_DC_001_IOS_11 |
      | screenShotName8     | Reg_Loans_DC_001_IOS_12 |
      | screenShotName9     | Reg_Loans_DC_001_IOS_13 |
      | screenShotName10    | Reg_Loans_DC_001_IOS_14 |
      | screenShotName11    | Reg_Loans_DC_001_IOS_15 |
    Then know more about you for DC IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&08        |
      | screenShotName  | Reg_Loans_DC_001_IOS_16 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_17 |
      | screenShotName2 | Reg_Loans_DC_001_IOS_18 |
      | screenShotName3 | Reg_Loans_DC_001_IOS_18 |
      | screenShotName4 | Reg_Loans_DC_001_IOS_19 |
      | screenShotName5 | Reg_Loans_DC_001_IOS_20 |
      | screenShotName6 | Reg_Loans_DC_001_IOS_21 |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_DC_001_IOS_22 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_23 |
      | screenShotName2 | Reg_Loans_DC_001_IOS_24 |
      | screenShotName3 | Reg_Loans_DC_001_IOS_25 |
    Then I share hkid


  Scenario: Reg_Loan_DC_002 Loan DC Flow DC Document upload page Credit card Personal loan proof IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_DC_002_IOS_01 |
    Then go To Upload Page
    Then upload files with Credit card Personal loan proof
      | screenShotName   | Reg_Loans_DC_002_IOS_02 |
      | screenShotName1  | Reg_Loans_DC_002_IOS_03 |
      | screenShotName2  | Reg_Loans_DC_002_IOS_04 |
      | screenShotName3  | Reg_Loans_DC_002_IOS_05 |
      | screenShotName4  | Reg_Loans_DC_002_IOS_06 |
      | screenShotName5  | Reg_Loans_DC_002_IOS_07 |
      | screenShotName6  | Reg_Loans_DC_002_IOS_08 |
      | screenShotName7  | Reg_Loans_DC_002_IOS_09 |
      | screenShotName8  | Reg_Loans_DC_002_IOS_10 |
      | screenShotName9  | Reg_Loans_DC_002_IOS_11 |
      | screenShotName10 | Reg_Loans_DC_002_IOS_12 |


