
package com.welab.automation.projects.loans.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {"src/main/java/com/welab/automation/projects/loans/features/web/TAT-T3079_loanApplicationsSake2.feature"},
    glue = {"com/welab/automation/projects/loans/steps/web"},
    monochrome = true)
public class Sake2TestRunner extends TestRunner {}
