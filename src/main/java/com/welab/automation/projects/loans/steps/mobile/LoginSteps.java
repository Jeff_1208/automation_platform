package com.welab.automation.projects.loans.steps.mobile;

import com.welab.automation.projects.loans.pages.mobile.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginSteps {
    LoginPage loginPage;
    public LoginSteps(){
        loginPage = new LoginPage();
    }

    @And("View transaction details")
    public void viewTransactionDetails(Map<String, String> data) {
        loginPage.clickTransactionDetails(data.get("screenShotName"),data.get("screenShotName1"));
        loginPage.viewTransactionDetails();
        loginPage.clickReturnButton(data.get("screenShotName2"));
        loginPage.Pullup();
    }
    @And("I'm checking the information in the purchase record")
    public void checkingInformation(Map<String, String> data) {
        assertThat(loginPage.getListInformation(data.get("screenShotName"))).isTrue();
    }

    @Then("get app version loans")
    public void getAppVersion() {
        loginPage.getAppVersion();
    }

    @Then("get screenShot")
    public void getScreenShot(Map<String, String> data) {
        loginPage.getScreenShot(data.get("screenShotName"));
    }

    @Then("check transaction detail")
    public void checkTransactionDetail(Map<String, String> data) {
        loginPage.checkTransactionDetail(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("close app driver")
    public void closeAppDriver() {
        loginPage.closeAppDriver();
    }
}
