package com.welab.automation.projects.loans.steps.mobile;


import com.welab.automation.projects.loans.pages.mobile.LoansIosPage;
import com.welab.automation.projects.loans.pages.mobile.LoansPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import java.util.Map;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LoansSteps {

    LoansPage loansPage;
    LoansIosPage loansIosPage;
    public LoansSteps(){
        loansPage = new LoansPage();
        loansIosPage = new LoansIosPage();
    }

    @Then("I goto loansPage")
    public void iGotoLoansPage(Map<String,String>data) {
        assertThat(loansPage.goToLoansPage(data.get("screenShotName"))).isTrue();
    }

    @Then("I goto loansPage2")
    public void iGotoLoansPage2(Map<String,String>data) {
        loansPage.goToLoansPage2(data.get("screenShotName"));
    }

    @Then("I go loansPage")
    public void iGoLoansPage(Map<String,String>data) {
        loansPage.goToLoansPage(data.get("screenShotName"));
    }

    @And("I goto Personal Instalmen Page")
    public void iGotoPersonalInstalmenPage(Map<String,String> data) {
        assertThat(loansPage.goToPersonalInstalmenPage(data.get("screenShotName"))).isTrue();
    }

    @And("I goto DC Page")
    public void iGotoDcPage(Map<String,String> data) {
        loansPage.iGotoDcPage(data.get("screenShotName"));
    }

    @And("I goto RF Page")
    public void iGotoRfPage(Map<String,String> data) {
        loansPage.iGotoRfPage(data.get("screenShotName"));
    }

    @And("set Rf Tenor And Purpose")
    public void setRfTenorAndPurpose(Map<String,String> data) {
        loansPage.setRfTenorAndPurpose(data.get("amount"),data.get("tenor"),data.get("screenShotName"));
    }

    @Then("I set Loan Amount and Tenor")
    public void iSetLoanAmountAndTenor(Map<String,String>data) {
        loansPage.setLoanAmountAndTenor(data.get("amount"),data.get("tenor"),data.get("screenShotName"));
    }

    @Then("I can see Key Facts Statements Page")
    public void iCanSeeKeyFactsStatementsPage() {
        assertThat(loansPage.canSeeKeyFactsStatementsPage()).isTrue();
    }

    @And("I read Key Facts Statements Page and agree")
    public void iReadKeyFactsStatementsPageAndAgree(Map<String,String> data) {
        loansPage.readKeyFactsStatementsPageAndAgree(data.get("screenShotName"));
    }

    @Then("I can see About your job Page")
    public void iCanSeeAboutYourJobPage(Map<String,String> data) {
        assertThat(loansPage.canSeeAboutYourJobPage(data.get("screenShotName"))).isTrue();
    }

    @And("I fill in the info in the About your job Page")
    public void iFillInTheInfoInTheAboutYourJobPage(Map<String,String>data) {
        loansPage.fillInTheInfoInTheAboutYourJobPage(data);
    }
//
//    @Then("I go to loansPage")
//    public void iGotoLoansPage(Map<String, String> data) {
//        loansPage.goToLoanPage(data.get("screenShotName"));
//    }

    @And("I click check it now button")
    public void clickCheckItNowBtn(Map<String, String> data){
        loansPage.clickCheckItNowBtn(data.get("screenShotName1"));
    }

    @And("check Congratulation Page Before Upload Files")
    public void checkCongratulationPageBeforeUploadFiles(){
        loansPage.checkCongratulationPageBeforeUploadFiles();
    }

    @And("I click confirm button in the plan page")
    public void clickConfirmBtn(Map<String, String> data){
        loansPage.clickConfirmBtn(data.get("screenShotName2"), data.get("screenShotName3"));
    }

    @Then("I goto Konw more about you Page")
    public void iGotoKonwMoreAboutYouPage(Map<String,String> data) {
        assertThat(loansPage.gotoKonwMoreAboutYouPage(data.get("screenShotName"))).isTrue();
    }

    @Then("I fill in the info in the page Know more about your")
    public void iFillInTheInfoInThePageKnowMoreAboutYour(Map<String,String>data) {
        loansPage.fillInTheInfoInThePageKnowMoreAboutYour(data);
    }

    @Then("fill in the info in the page Know more about your for DC flow")
    public void fillInTheInfoInThePageKnowMoreAboutYourForDcFlow(Map<String,String>data) {
        loansPage.fillInTheInfoInThePageKnowMoreAboutYourForDcFlow(data);
    }


    @And("I goto Review and confirm Page")
    public void iGotoReviewAndConfirmPage(Map<String,String>data) {
        assertThat(loansPage.gotoReviewAndConfirmPage(data.get("screenShotName"))).isTrue();
    }

    @Then("change the info in the page Review and confirm")
    public void changeTheInfoInThePageReviewAndConfirm(Map<String,String>data) {
        loansPage.changeTheInfoInThePageReviewAndConfirm(data);
    }

    @And("I goto The Plan page")
    public void iGotoThePlanPage(Map<String,String>data) {
        loansPage.gotoThePlanPage(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("I get loans page")
    public void getoLoansPage(Map<String,String> data) {
        loansIosPage.getLoansPage(data.get("screenShotName"));
    }

    @Then("I go to My loans")
    public void IgoToMyloans(Map<String, String> data) {
        loansIosPage.IgoToMyloans(data.get("screenShotName"));
    }

    @Then("go To Loans DC Page")
    public void goToLoansDCPage() {
        loansIosPage.goToLoansDCPage();
    }
    @Then("go To Loans PIL Page")
    public void goToLoansPILPage() {
        loansIosPage.goToLoansPILPage();
    }

    @Then("go To Loans RF Page")
    public void goToLoansRFPage() {
        loansIosPage.goToLoansRFPage();
    }

    @Then("confirm KFS PDF Page IOS")
    public void confirmKfsPdfPage(Map<String,String> data) {
        loansIosPage.confirmKfsPdfPage(data.get("screenShotName"));
    }

    @Then("Confirm Information For RF Flow IOS")
    public void ConfirmInformationForRfFlowIOS(Map<String,String> data) {
        loansIosPage.ConfirmInformationForRfFlowIOS(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("check KFS page")
    public void checkKFSpage(Map<String,String> data) {
        loansIosPage.checkKFSpage(data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("check KFS page And Set Loans Amount")
    public void checkKFSpageAndSetLoansAmount(Map<String,String> data) {
        loansIosPage.checkKFSpageAndSetLoansAmount(data.get("Amount"),data.get("screenShotName"), data.get("screenShotName1"));
    }

    @Then("RF set Loans Amount And Click Apply Now IOS")
    public void RFsetLoansAmountAndClickApplyNowIOS(Map<String,String> data) {
        loansIosPage.RFsetLoansAmountAndClickApplyNowIOS(data.get("Amount"),data.get("tenor"),
                data.get("screenShotName"), data.get("screenShotName1"));
    }


    @Then("go To Upload Page")
    public void goToUploadPage() {
        loansIosPage.goToUploadPage();
    }

    @Then("upload files")
    public void uploadFiles(Map<String,String> data) {
        loansIosPage.uploadFiles(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"),data.get("screenShotName7"),
                data.get("screenShotName8"),data.get("screenShotName9"),data.get("screenShotName10"));
    }

    @Then("upload files with Credit card Personal loan proof")
    public void uploadFilesWithCreditCardPersonalLoanProof(Map<String,String> data) {
        loansIosPage.uploadFilesWithCreditCardPersonalLoanProof(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"),data.get("screenShotName7"),
                data.get("screenShotName8"),data.get("screenShotName9"),data.get("screenShotName10"));
    }
    @Then("upload files with Ekyc")
    public void uploadFilesWithEkyc(Map<String,String> data) {
        loansIosPage.uploadFilesWithEkyc(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"),data.get("screenShotName7"),
                data.get("screenShotName8"),data.get("screenShotName9"),data.get("screenShotName10"));
    }

    @Then("PIL Notification Loan Disbursed IOS")
    public void PILNotificationLoanDisbursedIOS(Map<String,String> data) {
        loansIosPage.PILNotificationLoanDisbursedIOS(data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"));
    }

    @Then("I go to loans Page after submit application")
    public void iGotoLoansPageAfterSubmitApplication(Map<String, String> data) {
        loansPage.goToLoanPageAfterSubmitApplication(data.get("screenShotName"));
    }

    @And("I click approval letter details")
    public void clickApprovalLetterDetail(Map<String, String> data){
        loansPage.clickApprovalLetterDetail(data.get("screenShotName2"),data.get("screenShotName3"));
    }

    @And("I return to the approval letter page")
    public void returnToTheApprovalLetterPage(){
        loansPage.returnBtn();
    }

    @And("I click repayment schedule")
    public void clickRepaymentSchedule(Map<String, String> data){
        loansPage.clickRepaymentSchedule(data.get("screenShotName4"));
    }

    @And("I click terms and conditions")
    public void clickTermsAndConditions(Map<String, String> data){
        loansPage.clickTermsAndConditions(data.get("screenShotName5"));
    }

    @And("I click key facts statements")
    public void clickKeyFactsStatements(Map<String, String> data){
        loansPage.clickKeyFactsStatements(data.get("screenShotName6"));
    }

    @And("I get HKId")
    public void getHKId(){
        loansPage.getHKId();
    }

    @And("I click Dev Panel")
    public void clickDevPanel(){
        loansPage.clickDevPanel();
    }

    @And("I click upload button")
    public void clickUploadBtn(Map<String, String> data){
        loansPage.clickUploadBtn(data.get("screenShotName01"));
    }

    @And("I select income proof")
    public void clickIncomeProof(Map<String, String> data){
        loansPage.clickIncomeProof(data.get("screenShotName02"));
    }

    @And("I click add photo")
    public void clickAddPhoto(Map<String, String> data){
        loansPage.clickAddPhoto(data.get("screenShotName03"));
    }
    @And("I click add photo2")
    public void clickAddPhoto2(Map<String, String> data){
        loansPage.clickAddPhoto2(data.get("screenShotName10"));
    }
    @And("I click add photo3")
    public void clickAddPhoto3(Map<String, String> data){
        loansPage.clickAddPhoto3(data.get("screenShotName"));
    }

    @And("I select photo library1")
    public void choosePhotoLibrary1(Map<String, String> data){
        loansPage.choosePhotoLibrary1(data.get("screenShotName04"), data.get("screenShotName05"), data.get("screenShotName06"));
    }

    @And("I select photo library2")
    public void choosePhotoLibrary2(Map<String, String> data){
        loansPage.choosePhotoLibrary2(data.get("screenShotName04"), data.get("screenShotName05"), data.get("screenShotName06"));
    }

    @And("I select take photo")
    public void selectTakePhoto(Map<String, String> data){
        loansPage.selectTakePhoto(data.get("screenShotName4"), data.get("screenShotName5"));
    }

    @And("I select no thanks")
    public void selectNoThanksBtn(Map<String, String> data){
        loansPage.selectNoThanksBtn(data.get("screenShotName08"));
    }

    @And("I select residential address proof")
    public void clickResidentialAddressProof(Map<String, String> data){
        loansPage.clickResidentialAddressProof(data.get("screenShotName09"));
    }

    @And("I select Credit card Personal loan proof")
    public void clickCreditCardPersonalLoanProof(Map<String, String> data){
        loansPage.clickCreditCardPersonalLoanProof(data.get("screenShotName"));
    }

    @And("I click done button")
    public void clickDoneBtn(Map<String, String> data){
        loansPage.clickDoneBtn(data.get("screenShotName"), data.get("screenShotName01"));
    }

    @Then("I share hkid")
    public void iShareHkid() {
        loansPage.iShareHkid();
    }

    @Then("I clicked the Apply button")
    public void Applybutton(Map<String,String>data)  {
        loansPage.clicked(data);
    }

    @Then("Enter amount and select purpose")
    public void modify(Map<String,String>data) throws Exception {
        loansPage.modifyButton(data);
    }
    @Then("Click the Apply button")
    public void clickApplyNow(Map<String,String>data) throws Exception {
        loansPage.clickApplyNow(data);
    }

    @Then("Click the confirm button")
    public void clickFonfirm(Map<String,String>data) throws Exception {
        loansPage.clickconfirm(data);
    }

    @And("I click by pressing button")
    public void clickByPressingBtn(Map<String, String> data){
        loansPage.clickByPressingBtn(data.get("screenShotName7"));
    }

    @And("I click agree and drawdown button")
    public void clickAgreeAndDrawdownBtn(Map<String, String> data){
        loansPage.clickAgreeAndDrawdownBtn(data.get("screenShotName8"));
    }

    @And("open my loans")
    public void openMyLoan(){
        loansPage.openMyLoan();
    }

    @And("check my loan with deliquent")
    public void checkMyLoanWithDeliquent(Map<String, String> data){
        loansPage.checkMyLoanWithDeliquent(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @And("check my loan second repayment amount")
    public void checkSecondRepaymentAmount(Map<String, String> data){
        loansPage.checkSecondRepaymentAmount(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("input promotion code")
    public void inputPromotionCode(Map<String,String>data) {
        loansPage.inputPromotionCode(data);
    }

    @Then("open my loans ios")
    public void openMyLoanIOs(Map<String,String> data){
        loansIosPage.openMyLoanIos(data.get("screenShotName"));
    }

    @Then("check My Loans With Deliquent IOS")
    public void checkMyLoansWithDeliquentIOS(Map<String,String> data){
        loansIosPage.checkMyLoansWithDeliquentIOS(data.get("screenShotName"));
    }

    @Then("check My Loans Second Repayment With Deliquent IOS")
    public void checkMyLoansSecondRepaymentWithDeliquentIOS(Map<String,String> data){
        loansIosPage.checkMyLoansSecondRepaymentWithDeliquentIOS(data.get("screenShotName"));
    }

    @Then("fill about your job IOS")
    public void FillAboutYourJob(Map<String,String> data){
        loansIosPage.FillAboutYourJob(
                data.get("monthlyIncomeAmount"),data.get("incomeType"),data.get("Building"),
                data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"),data.get("screenShotName7"),
                data.get("screenShotName8"),data.get("screenShotName9"),data.get("screenShotName10"),
                data.get("screenShotName11"));
    }

    @Then("know more about you IOS")
    public void knowMoreAboutYou(Map<String,String> data){
        loansIosPage.knowMoreAboutYou(
                data.get("Purpose"),data.get("Building"),
                data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"));
    }

    @Then("know more about you for DC IOS")
    public void knowMoreAboutYouForDC(Map<String,String> data){
        loansIosPage.knowMoreAboutYouForDC(
                data.get("Purpose"),data.get("Building"),
                data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"),data.get("screenShotName4"),
                data.get("screenShotName5"),data.get("screenShotName6"));
    }

    @Then("check The Page After Submit Application IOS")
    public void checkThePageAfterSubmitApplication(Map<String,String> data){
        loansIosPage.checkThePageAfterSubmitApplication(
                data.get("screenShotName"),data.get("screenShotName1"),
                data.get("screenShotName2"),data.get("screenShotName3"));
    }

    @Then("check The Page After Submit Application Rf Flow IOS")
    public void checkThePageAfterSubmitApplicationRfFlow(Map<String,String> data){
        loansIosPage.checkThePageAfterSubmitApplicationRfFlow(data.get("screenShotName"));
    }

    @Then("check The Page After When Skip Upload Picture Submit Application IOS")
    public void checkPageWhenSkipUploadPictureAfterSubmitApplication(Map<String,String> data){
        loansIosPage.checkPageWhenSkipUploadPictureAfterSubmitApplication(data.get("screenShotName"),data.get("screenShotName1"));
    }

    @Then("go to Welab Fisrt Page IOS")
    public void goToWelabFisrtPage(){
        loansIosPage.goToWelabFisrtPage();
    }


}

