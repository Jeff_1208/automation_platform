Feature: Loans

  Background: Login
    Given Open loans WeLab App

  @VBL-T3111
  Scenario: Reg_Loan_001 Loan- Landing page and apply loan
    And skip Root ios
#    When Login with user and password from txt
    And get app version loans
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_001_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page
      | screenShotName  | Reg_Loans_001_IOS_02 |
      | screenShotName1 | Reg_Loans_001_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sampleRisk&82        |
      | screenShotName      | Reg_Loans_001_IOS_04 |
      | screenShotName1     | Reg_Loans_001_IOS_05 |
      | screenShotName2     | Reg_Loans_001_IOS_06 |
      | screenShotName3     | Reg_Loans_001_IOS_07 |
      | screenShotName4     | Reg_Loans_001_IOS_08 |
      | screenShotName5     | Reg_Loans_001_IOS_09 |
      | screenShotName6     | Reg_Loans_001_IOS_10 |
      | screenShotName7     | Reg_Loans_001_IOS_11 |
      | screenShotName8     | Reg_Loans_001_IOS_12 |
      | screenShotName9     | Reg_Loans_001_IOS_13 |
      | screenShotName10    | Reg_Loans_001_IOS_14 |
      | screenShotName11    | Reg_Loans_001_IOS_15 |
    Then know more about you IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&08        |
      | screenShotName  | Reg_Loans_001_IOS_16 |
      | screenShotName1 | Reg_Loans_001_IOS_17 |
      | screenShotName2 | Reg_Loans_001_IOS_18 |
      | screenShotName3 | Reg_Loans_001_IOS_18 |
      | screenShotName4 | Reg_Loans_001_IOS_19 |
      | screenShotName5 | Reg_Loans_001_IOS_20 |
      | screenShotName6 | Reg_Loans_001_IOS_21 |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_001_IOS_22 |
      | screenShotName1 | Reg_Loans_001_IOS_23 |
      | screenShotName2 | Reg_Loans_001_IOS_24 |
      | screenShotName3 | Reg_Loans_001_IOS_25 |
    Then I share hkid

  @VBL-T3112
  Scenario: Reg_Loan_002 Loan- upload proof files
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_002_IOS_01 |
    Then go To Upload Page
    Then upload files
      | screenShotName   | Reg_Loans_002_IOS_02 |
      | screenShotName1  | Reg_Loans_002_IOS_03 |
      | screenShotName2  | Reg_Loans_002_IOS_04 |
      | screenShotName3  | Reg_Loans_002_IOS_05 |
      | screenShotName4  | Reg_Loans_002_IOS_06 |
      | screenShotName5  | Reg_Loans_002_IOS_07 |
      | screenShotName6  | Reg_Loans_002_IOS_08 |
      | screenShotName7  | Reg_Loans_002_IOS_09 |
      | screenShotName8  | Reg_Loans_002_IOS_10 |
      | screenShotName9  | Reg_Loans_002_IOS_11 |
      | screenShotName10 | Reg_Loans_002_IOS_12 |


  @VBL-T3089
  Scenario: Reg_Loan_DC_001 Loan DC Flow Landing page and apply loan IOS
    And skip Root ios
    And get app version loans
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_DC_001_IOS_01 |
    Then go To Loans DC Page
    Then check KFS page
      | screenShotName  | Reg_Loans_DC_001_IOS_02 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sampleRisk&82        |
      | screenShotName      | Reg_Loans_DC_001_IOS_04 |
      | screenShotName1     | Reg_Loans_DC_001_IOS_05 |
      | screenShotName2     | Reg_Loans_DC_001_IOS_06 |
      | screenShotName3     | Reg_Loans_DC_001_IOS_07 |
      | screenShotName4     | Reg_Loans_DC_001_IOS_08 |
      | screenShotName5     | Reg_Loans_DC_001_IOS_09 |
      | screenShotName6     | Reg_Loans_DC_001_IOS_10 |
      | screenShotName7     | Reg_Loans_DC_001_IOS_11 |
      | screenShotName8     | Reg_Loans_DC_001_IOS_12 |
      | screenShotName9     | Reg_Loans_DC_001_IOS_13 |
      | screenShotName10    | Reg_Loans_DC_001_IOS_14 |
      | screenShotName11    | Reg_Loans_DC_001_IOS_15 |
    Then know more about you for DC IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&08        |
      | screenShotName  | Reg_Loans_DC_001_IOS_16 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_17 |
      | screenShotName2 | Reg_Loans_DC_001_IOS_18 |
      | screenShotName3 | Reg_Loans_DC_001_IOS_18 |
      | screenShotName4 | Reg_Loans_DC_001_IOS_19 |
      | screenShotName5 | Reg_Loans_DC_001_IOS_20 |
      | screenShotName6 | Reg_Loans_DC_001_IOS_21 |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_DC_001_IOS_22 |
      | screenShotName1 | Reg_Loans_DC_001_IOS_23 |
      | screenShotName2 | Reg_Loans_DC_001_IOS_24 |
      | screenShotName3 | Reg_Loans_DC_001_IOS_25 |
    Then I share hkid


  @VBL-T3095
  Scenario: Reg_Loan_DC_002 Loan DC Flow DC Document upload page Credit card Personal loan proof IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_DC_002_IOS_01 |
    Then go To Upload Page
    Then upload files with Credit card Personal loan proof
      | screenShotName   | Reg_Loans_DC_002_IOS_02 |
      | screenShotName1  | Reg_Loans_DC_002_IOS_03 |
      | screenShotName2  | Reg_Loans_DC_002_IOS_04 |
      | screenShotName3  | Reg_Loans_DC_002_IOS_05 |
      | screenShotName4  | Reg_Loans_DC_002_IOS_06 |
      | screenShotName5  | Reg_Loans_DC_002_IOS_07 |
      | screenShotName6  | Reg_Loans_DC_002_IOS_08 |
      | screenShotName7  | Reg_Loans_DC_002_IOS_09 |
      | screenShotName8  | Reg_Loans_DC_002_IOS_10 |
      | screenShotName9  | Reg_Loans_DC_002_IOS_11 |
      | screenShotName10 | Reg_Loans_DC_002_IOS_12 |


  @VBL-T2982
  Scenario: Reg_Loan_RF_001 RF Happy Flow IOS
    And skip Root ios
    And get app version loans
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_RF_001_IOS_01 |
    Then go To Loans RF Page
    Then RF set Loans Amount And Click Apply Now IOS
      | Amount         | 5000                    |
      | tenor          | 20                       |
      | screenShotName | Reg_Loans_RF_001_IOS_02 |
    Then confirm KFS PDF Page IOS
      | screenShotName | Reg_Loans_RF_001_IOS_03 |
    Then Confirm Information For RF Flow IOS
      | screenShotName  | Reg_Loans_RF_001_IOS_04 |
      | screenShotName1 | Reg_Loans_RF_001_IOS_05 |
    Then check The Page After Submit Application Rf Flow IOS
      | screenShotName | Reg_Loans_RF_001_IOS_06 |
    Then I share hkid


  @VBL-T2987
  Scenario: Reg_Loan_RF_002 Loan RF Flow upload proof files IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_RF_002_IOS_01 |
    Then go To Upload Page
    Then upload files
      | screenShotName   | Reg_Loans_RF_002_IOS_02 |
      | screenShotName1  | Reg_Loans_RF_002_IOS_03 |
      | screenShotName2  | Reg_Loans_RF_002_IOS_04 |
      | screenShotName3  | Reg_Loans_RF_002_IOS_05 |
      | screenShotName4  | Reg_Loans_RF_002_IOS_06 |
      | screenShotName5  | Reg_Loans_RF_002_IOS_07 |
      | screenShotName6  | Reg_Loans_RF_002_IOS_08 |
      | screenShotName7  | Reg_Loans_RF_002_IOS_09 |
      | screenShotName8  | Reg_Loans_RF_002_IOS_10 |
      | screenShotName9  | Reg_Loans_RF_002_IOS_11 |
      | screenShotName10 | Reg_Loans_RF_002_IOS_12 |