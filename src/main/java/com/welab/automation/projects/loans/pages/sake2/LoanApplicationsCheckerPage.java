package com.welab.automation.projects.loans.pages.sake2;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class LoanApplicationsCheckerPage extends WebBasePage {

    private String pageName = "LoanApplicationsCheckerPage";

    @FindBy(how = How.ID, using = "search")
    private WebElement searchInput;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Search all Loans']")
    private WebElement searchAllLoansBtn;

    @FindBy(how = How.XPATH, using = "//*[@data-testid='copyToClipboard']/../span")
    private WebElement searchApplicationNum;

    @FindBy(how = How.XPATH, using = "//a[text() = 'Detail']")
    private WebElement detailBtn;

    @FindBy(how = How.XPATH, using = "//div[@data-testid='loanInfo']//div[text() = 'Quick Edit']")
    private WebElement loanInfoEditBtn;

    @FindBy(how = How.XPATH, using = "//div[text()='Loan Info']")
    private WebElement loanInfoTab;

    @FindBy(how = How.XPATH, using = "//*[@data-testid=\"status\"]")
    private WebElement statusOptions;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Update']")
    private WebElement updateBtn;

    @FindBy(how = How.XPATH, using = "//tr/td[12]/button")
    private WebElement updateAssigneeBtn;

    @FindBy(how = How.XPATH, using = "//h5[text() = 'Update Assignee']/../../div[2]//select")
    private WebElement updateAssigneeSelect;

    @FindBy(how = How.CSS, using = "button[class=\"btn btn-success\"]")
    private WebElement updateAssigneeChecker;

    @FindBy(how = How.XPATH, using = "//tr/td[12]/*[@id=\"edit-user-11540\"]/../span")
    private WebElement assigneeTableName;

    @FindBy(how = How.XPATH, using = "//p[text() = 'Documents']")
    private WebElement documentsDiv;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div//div[1]/div[7]/div[2]/div/div")
    private List<WebElement> documentsProofList;

    @FindBy(how = How.XPATH, using = "//label[text() = 'Valid']")
    private List<WebElement> validList;

    @FindBy(how = How.XPATH, using = "//div[text() = 'Fraud Detector']")
    private WebElement fraudDetector;

    //@FindBy(how = How.CSS, using = " svg[class=\"css-19bqh2r\"]")
    @FindAll({
            @FindBy(how = How.CSS, using = " svg[class=\"css-19bqh2r\"]"),
            @FindBy(how = How.CLASS_NAME, using = "css-19bqh2r")
    })
    private WebElement loanInfoReason;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Reason']")
    private WebElement loanInfoReasonLabel;

    @FindBy(how = How.XPATH, using = "//*[text() = 'A1. Approved']")
    private WebElement reasonA1;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Credit Info']")
    private WebElement creditInfo;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Quick Edit']")
    private WebElement creditInfoQuickEdit;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Approve']")
    private WebElement approveBtn;

    @FindBy(how = How.XPATH, using = "//*[@class=\" css-tlfecz-indicatorContainer\"][1]")
    private WebElement reasonClear;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div[2]//table/tbody/tr/td[10]/span")
    private WebElement status;

    @FindBy(how = How.CSS, using = "div[class=\"errors col-md-12 text-danger\"]")
    private WebElement planNotSelected;

    @FindBy(how = How.NAME, using = "recommendedAmount")
    private WebElement recommendedAmount;

    @FindBy(how = How.NAME, using = "recommendedAnnualInterest")
    private WebElement recommendedAnnualInterest;

    @FindBy(how = How.XPATH, using = "//a[text()='Overview']")
    private WebElement overviewTitle;

    @FindBy(how = How.XPATH, using = "//a[text()='Applications']")
    private WebElement Applications;

    @SneakyThrows
    public void backToAllOrderPage() {
        clickElement(Applications);
        Thread.sleep(1000);
        waitUntilElementVisible(searchAllLoansBtn);
    }

    @SneakyThrows
    public void searchApplication(String applicationNum,String screenShot) {
        Thread.sleep(3000);
        logger.info("Loans applicationNum: "+applicationNum);
        waitUntilElementClickable(searchInput);
        clearAndSendKeys(searchInput,applicationNum);
        clickElement(searchAllLoansBtn);
        Thread.sleep(2000);
        waitUntilElementVisible(searchApplicationNum,5);
        assertThat(searchApplicationNum.getText()).isEqualTo(applicationNum);
        takeScreenshot(screenShot);
    }

    @FindBy(how = How.XPATH, using = "//tr/td[10]/span")
    private WebElement lastStatusOnSearchPage;

    public void refreshWebPage(){
        driver.navigate().refresh();
    }

    public void verifyLastStatus(){
        driver.navigate().refresh();
        String status = lastStatusOnSearchPage.getText().trim();
        logger.info("status: "+status);
        if (status!=null){
            if(status.contains("Approved")){

            }else{
                assertThat(true).isFalse();
            }
        }else{
            assertThat(true).isFalse();
        }


    }

    @SneakyThrows
    public void updateAssigneeAsChecker(String upAssigneeName,String screenShot) {
        Thread.sleep(2000);
        waitUntilElementClickable(updateAssigneeBtn);
        clickElement(updateAssigneeBtn);
        Thread.sleep(3000);
        selectByVisibleText(updateAssigneeSelect,upAssigneeName);
        Thread.sleep(1000);
        clickElement(updateAssigneeChecker);
        Thread.sleep(1000);
        takeScreenshot(screenShot);
    }

    @SneakyThrows
    public void updateAssign(String longScreenShot,String longScreenShotCredit,String documentShot) {
       String currentWindows = driver.getWindowHandle();
       waitUntilElementClickable(detailBtn);
       clickElement(detailBtn);
       Thread.sleep(2000);
       changeWindows(currentWindows);
       Thread.sleep(2000);
       longScreenShot(longScreenShot);
       Thread.sleep(4000);
       scrollToTop(pageName);
       Thread.sleep(2000);
       clickElement(creditInfo);
       Thread.sleep(2000);
       longScreenShot(longScreenShotCredit);
       scrollToTop(pageName);
       clickElement(overviewTitle);
       scrollToElement(documentsDiv);
       for (int i = 1; i < validList.size(); i = i + 2) {
            if (verifyElementExist(validList.get(i))) {
                clickElement(validList.get(i));
            }
        }
       Thread.sleep(2000);
       takeScreenshot(documentShot);
       scrollToElement(loanInfoTab);
    }

    @SneakyThrows
    public void updateLoanInfoStatus(String statusSelect) {
        waitUntilElementClickable(loanInfoEditBtn);
        clickElement(loanInfoEditBtn);
        Thread.sleep(1000);
        selectByVisibleText(statusOptions,statusSelect);
        Thread.sleep(1000);
        clickElement(reasonClear);
        Thread.sleep(1000);

        if(!isShowWebElement(loanInfoReasonLabel,2)){
            scrollToElement(loanInfoReasonLabel);
        }
        Point p=loanInfoReason.getLocation();
        logger.info("getX:"+p.getX());
        logger.info("getX:"+p.getY());
        scrollByLocation(pageName, p.getY(),p.getY()-150);

        clickElement(loanInfoReason);
        Thread.sleep(1000);
        clickElement(reasonA1);
        Thread.sleep(1000);
        clickElement(updateBtn);
        Thread.sleep(4000);
    }

    @SneakyThrows
    public void loanInfoEdit(String statusSelect,String updatedShot,String refreshShot) {
        updateLoanInfoStatus(statusSelect);
        if (verifyElementExist(planNotSelected)) {
            if (planNotSelected.getText().equals("Recommended plan not selected")) {
                scrollToTop(pageName);
                clickElement(creditInfo);
                waitUntilElementClickable(creditInfoQuickEdit);
                clickElement(creditInfoQuickEdit);
                clearAndSendKeys(recommendedAmount,"10000");
                clearAndSendKeys(recommendedAnnualInterest,"0");
                clickElement(updateBtn);
                Thread.sleep(2*1000);
                clickElement(overviewTitle);
                Thread.sleep(8*1000);
                scrollToElement(loanInfoTab);
                updateLoanInfoStatus(statusSelect);
            }
        }
        scrollToTop(pageName);
        longScreenShot(updatedShot);
        String currentWindows = driver.getWindowHandle();
        changeWindows(currentWindows);
        Thread.sleep(1000);
        pageRefresh(pageName);
        Thread.sleep(2000);

        takeScreenshot(refreshShot);
    }

    @SneakyThrows
    public void clickCreditInfo(String loanStatus,String approveShot,String approvedShot) {
        String currentWindows = driver.getWindowHandle();
        waitUntilElementVisible(status);
        if (!getElementText(status).equals("Approved")) {
            waitUntilElementClickable(detailBtn);
            clickElement(detailBtn);
            Thread.sleep(2000);
            changeWindows(currentWindows);
            clickElement(creditInfo);
            Thread.sleep(2000);
            clickElement(approveBtn);
            longScreenShot(approveShot);
        }
        currentWindows = driver.getWindowHandle();
        changeWindows(currentWindows);
        takeScreenshot(approvedShot);
        Thread.sleep(1000);
    }
}
