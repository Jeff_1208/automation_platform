package com.welab.automation.projects.loans.steps.web;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.projects.loans.pages.sake2.LoanApplicationsCheckerPage;
import io.cucumber.java.en.*;

import java.util.Map;


public class LoanApplicationsCheckerSteps {
    LoanApplicationsCheckerPage loanApplicationsCheckerPage;

    public LoanApplicationsCheckerSteps() {
        loanApplicationsCheckerPage = new LoanApplicationsCheckerPage();
    }

    @Then("Input the application id and click the button \"search all loan\"")
    public void searchApplication(Map<String,String> data) {
        String applicationNum = GlobalVar.GLOBAL_VARIABLES.get("orderId");
        loanApplicationsCheckerPage.searchApplication(applicationNum,data.get("screenShotName"));
    }

    @Then("verify Last Status")
    public void verifyLastStatus() {
        loanApplicationsCheckerPage.verifyLastStatus();
    }

    @Then("refresh Web Page")
    public void refreshWebPage() {
        loanApplicationsCheckerPage.refreshWebPage();
    }

    @And("Checker update assign successfully")
    public void updateAssignAndClickUpdate(Map<String,String> data) {
        loanApplicationsCheckerPage.updateAssigneeAsChecker(data.get("upAssigneeName"),data.get("screenShotName"));
        loanApplicationsCheckerPage.updateAssign(data.get("screenShotName1"),data.get("screenShotName2"),data.get("screenShotName3"));
        loanApplicationsCheckerPage.loanInfoEdit(data.get("statusSelect"),data.get("screenShotName4"),data.get("screenShotName5"));
        loanApplicationsCheckerPage.updateAssigneeAsChecker(data.get("riskName"),data.get("screenShotName6"));
    }

    @And("Click the tab 'credit info' and  the button 'Approve'")
    public void creditInfoAndApprove(Map<String,String> loanStatus) {
        loanApplicationsCheckerPage.clickCreditInfo(loanStatus.get("loanStatus"),loanStatus.get("screenShotName"),loanStatus.get("screenShotName1"));
    }

    @And("update Assign To Account")
    public void updateAssignToAccount(Map<String,String> data) {
        loanApplicationsCheckerPage.updateAssigneeAsChecker(data.get("riskName"),data.get("screenShotName"));
    }
}
