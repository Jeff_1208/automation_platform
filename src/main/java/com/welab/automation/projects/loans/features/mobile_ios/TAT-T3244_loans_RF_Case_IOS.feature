Feature: Loans

  Background: Login
    Given Open loans WeLab App

  Scenario: Reg_Loan_RF_001 RF Happy Flow IOS
    And skip Root ios
    And get app version loans
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_RF_001_IOS_01 |
    Then go To Loans RF Page
    Then RF set Loans Amount And Click Apply Now IOS
      | Amount         | 5000                    |
      | tenor          | 20                       |
      | screenShotName | Reg_Loans_RF_001_IOS_02 |
    Then confirm KFS PDF Page IOS
      | screenShotName | Reg_Loans_RF_001_IOS_03 |
    Then Confirm Information For RF Flow IOS
      | screenShotName  | Reg_Loans_RF_001_IOS_04 |
      | screenShotName1 | Reg_Loans_RF_001_IOS_05 |
    Then check The Page After Submit Application Rf Flow IOS
      | screenShotName | Reg_Loans_RF_001_IOS_06 |
    Then I share hkid


  Scenario: Reg_Loan_RF_002 Loan RF Flow upload proof files IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_RF_002_IOS_01 |
    Then go To Upload Page
    Then upload files
      | screenShotName   | Reg_Loans_RF_002_IOS_02 |
      | screenShotName1  | Reg_Loans_RF_002_IOS_03 |
      | screenShotName2  | Reg_Loans_RF_002_IOS_04 |
      | screenShotName3  | Reg_Loans_RF_002_IOS_05 |
      | screenShotName4  | Reg_Loans_RF_002_IOS_06 |
      | screenShotName5  | Reg_Loans_RF_002_IOS_07 |
      | screenShotName6  | Reg_Loans_RF_002_IOS_08 |
      | screenShotName7  | Reg_Loans_RF_002_IOS_09 |
      | screenShotName8  | Reg_Loans_RF_002_IOS_10 |
      | screenShotName9  | Reg_Loans_RF_002_IOS_11 |
      | screenShotName10 | Reg_Loans_RF_002_IOS_12 |


