package com.welab.automation.projects.loans.pages.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.framework.utils.ZephyrScaleUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.cucumber.java.en.And;
import lombok.Getter;
import lombok.SneakyThrows;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class LoginPage extends AppiumBasePage {

    private String pageName = "Login Page";
    private static final String[] login_text = {"Login","登入"};
    private static final String cancelText = "Cancel";
    private int customWait = 60;
    private int cancelMessageWait = 5;


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"不，谢谢\"]")
    private MobileElement noThanksBtn;

    @Getter
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
    })
    protected MobileElement loginTxt;

    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "//XCUIElementTypeButton[@name=\"取消\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"取消\""),
    })
    private MobileElement cancel;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='我明白啦']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Got it''"),
            @iOSXCUITBy(iOSNsPredicate = "name == '我明白啦'"),
    })
    private MobileElement gotIt;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Username']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='用戶名稱']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
    })
    private MobileElement userNameTxt;

    @AndroidFindBy(accessibility = "username-input")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
    })
    private MobileElement userNameInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Password']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='密碼']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
    })
    private MobileElement passwordTxt;

    @AndroidFindBy(accessibility = "password-input")
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Password'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '密碼'"),
    })
    private MobileElement passwordInput;

    @AndroidFindBy(xpath = "//*[@text='Welcome back']")
    private MobileElement welcomeBack;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
    })
    private MobileElement loginButton;


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
    private MobileElement contactWarningCancel;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
    private MobileElement notification;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
    private MobileElement next;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start Testing'")
    private MobileElement start;

    @Getter
    @iOSXCUITFindBy(iOSNsPredicate = "name == 'Ask App not to Track'")
    private MobileElement noTracking;

    //  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]")
//  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Total balance')]")
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, '總結餘')]")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[starts-with(@name, 'Total balance')]"),
            @iOSXCUITBy(xpath = "//*[starts-with(@name, '可用結餘')]"),
    })
    private MobileElement totalBalance;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text, 'WeLab')]"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[contains(@name, 'WeLab')]"),
    })
    private MobileElement openAppEle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'month GoSave Time Deposit')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Other transaction')]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'4個月GoSave定期存款')]"),
    })
    private MobileElement transaction;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> textView;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement returnButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Recent transactions']"),
            @AndroidBy(xpath = "//*[@text='最近交易']"),
    })
    private MobileElement transactions;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//*[@text='查看更多']"),
    })
    private MobileElement seeAll;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private List<MobileElement> getList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"),
    })
    private List<MobileElement> textElement;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[3]"),
    })
    private MobileElement reference;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView"),
    })
    private List<MobileElement> referenceNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@content-desc='login-screen']/android.view.ViewGroup[3]/android.widget.TextView[2]"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"開戶\"]/../XCUIElementTypeStaticText"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Open account\"]/../XCUIElementTypeStaticText[1]"),
    })
    private MobileElement appVersion;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"最近交易 處理中的交易 2 交易記錄\"]/../XCUIElementTypeOther[2]"),
            @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Recent transactions\"]/../XCUIElementTypeOther[2]"),
    })
    private MobileElement transactionDetails;

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeStaticText[@name=\"I am rebound\"]"),
    })
    private MobileElement helloAlert;
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"OK\"]"),
    })
    private MobileElement helloOKBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Your Core Account has become dormant.']"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "label == \"你嘅核心賬戶已處於「不動賬戶」狀態。\" AND name == \"text\""),
            @iOSXCUITBy(xpath = "//*[contains(@value ,'你嘅核心賬戶已處於「不動賬戶」狀態"),
    })
    private MobileElement YourCoreAccountHasBecomeDormant;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='OK']"),
    })
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"知道\"]")
    private MobileElement dormantOkButton;


    @AndroidFindAll({
            @AndroidBy(accessibility = "Home, tab, 1 of 4"),
            @AndroidBy(accessibility = "Home, tab, 1 of 5"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='主頁']")
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 4'"),
            @iOSXCUITBy(iOSNsPredicate = "name == '主頁, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'TabBarItemHomeStack, tab, 1 of 5'"),
            @iOSXCUITBy(iOSNsPredicate = "name == 'Home, tab, 1 of 5'")
    })
    private MobileElement homeTab;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"系統遇到問題\" AND name == \"text\"")
    private MobileElement systemIssueAlert;
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"取消\" AND name == \"text\"")
    private MobileElement systemIssueAlertCancelButton;

    public void checkSystemIssue(){
        if(isShow(systemIssueAlert,2)){
            clickElement(systemIssueAlertCancelButton);
        }
    }


    CommonPage commonPage;
    public LoginPage() {
        super.pageName = pageName;
        commonPage = new CommonPage();
        if (System.getProperty("mobile").equalsIgnoreCase("cloudios"))
            ((AppiumDriver<MobileElement>) BaseDriver.getMobileDriver()).activateApp("welab.bank.sit");
    }

    public void checkHelloAlert(){
        if(isShow(helloAlert,5)){
            clickElement(helloOKBtn);
        }
    }

    @SneakyThrows
    public void IcanSeeHomePage() {
        checkHelloAlert();
        waitUntilElementVisible(homeTab);
        Thread.sleep(1000);
    }

    public String getLoggedInPageText() {
        checkHelloAlert();
        waitUntilElementVisible(totalBalance,60);
        if (isElementDisplayed(totalBalance) != null) {
            return totalBalance.getText();
        }
        return "Can't find total balance element.";
    }

    public void loginWithUserAndPasswordFromTxt() {
        ZephyrScaleUtils zephyrScaleUtils = new ZephyrScaleUtils();
        String data = zephyrScaleUtils.deleteFirstLineForTxt();
        String user = data.split(",")[0].trim();
        String password = data.split(",")[1].trim();
        logger.info("user: " + user);
        logger.info("password: " + password);
        String platform =  System.getProperty("mobile");
        GlobalVar.GLOBAL_VARIABLES.put(platform+".userName", user);
        GlobalVar.GLOBAL_VARIABLES.put(platform+".password", password);
        inputUserNamePassword(user, password);
    }

    @SneakyThrows
    public void inputUserNamePassword(String user, String password){
        if(!isShow(userNameTxt, 2)){
            clickElement(loginButton);
        }
        clickElement(userNameTxt);
        clearAndSendKeys(userNameInput, user);
        hideAndroidKeyboard();
        clickElement(passwordTxt);
        clearAndSendKeys(passwordInput, password);
//        inputEnter(passwordInput);
        hideAndroidKeyboard();
        Thread.sleep(1000);
        clickElement(loginButton);
        Thread.sleep(10000);
    }

    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"以后\"]"),
            @iOSXCUITBy(iOSNsPredicate = "label == \"稍後再說\""),
    })
    private MobileElement laterButton;

    @iOSXCUITFindBy(iOSNsPredicate = "label == \"推廣資訊設定\"")
    private MobileElement marketingPage;
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"確定\" AND name == \"text\"")
    private MobileElement marketingConfirmButton;

    public void checkMarketingPage(){
        if(commonPage.isIOS()) {
            if (isShow(marketingPage, 1)) {
                clickElement(marketingConfirmButton);
            }
        }
    }

    public void checkLaterButton(){
        if(commonPage.isIOS()) {
            if (isShow(laterButton, 20)) {
                clickElement(laterButton);
            }
        }
    }

    public void loginWithUserAndPasswordFromProperties() {
        String platform =  System.getProperty("mobile");
        String user= GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".password");
        inputUserNamePassword(user, password);
    }

    public void checkUnexpectedElement(){
        checkLaterButton();
        checkMarketingPage();
        checkBrazeTestingCloseMessage();
        checkDormantMessage();
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text=\"Close Message\"]"),
    })
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//XCUIElementTypeButton[@name=\"Close Message\"]"),
    })
    private MobileElement brazeTestingCloseMessage;


    @SneakyThrows
    public void checkBrazeTestingCloseMessage(){
        if(isShow(brazeTestingCloseMessage,1)){
            Thread.sleep(100);
            clickElement(brazeTestingCloseMessage);
        }
    }
    @SneakyThrows
    public void checkDormantMessage(){
        if(isShow(YourCoreAccountHasBecomeDormant,1)){
            Thread.sleep(100);
            clickElement(dormantOkButton);
        }
    }

    @SneakyThrows
    public void loginWithCredential(String user, String password) {
        logger.info("Login with credential: " + user + ", " + password);
        clickElement(userNameTxt);
        clearAndSendKeys(userNameInput, user);
        hideAndroidKeyboard();
        clickElement(passwordTxt);
        clearAndSendKeys(passwordInput, password);
        inputEnter(passwordInput);
        hideAndroidKeyboard();
        clickElement(loginButton);
        Thread.sleep(10000);
        checkDormantMessage();
        Thread.sleep(2000);
    }
    public String getLoginPageText(Map<String, String> data) {
        waitUntilElementVisible(totalBalance,60);
        takeScreenshot(data.get("screenShotName"));
        if (isElementDisplayed(totalBalance) != null) {
            return totalBalance.getText();
        }
        return "Can't find total balance element.";
    }

    @SneakyThrows
    public void LoginAgain( Map<String, String> data) {
        Thread.sleep(1000 * 5);
        String platform =  System.getProperty("mobile");
        String user=GlobalVar.GLOBAL_VARIABLES.get(platform+".userName");
        String password=GlobalVar.GLOBAL_VARIABLES.get(platform+".newPassword");
        logger.info("Login with credential: " + user + ", " + password);
        takeScreenshot(data.get("screenShotName"));
        clickElement(userNameTxt);
        clearAndSendKeys(userNameInput, user);
        hideAndroidKeyboard();
        clickElement(passwordTxt);
        clearAndSendKeys(passwordInput, password);
        inputEnter(passwordInput);
        hideAndroidKeyboard();
        takeScreenshot(data.get("screenShotName1"));
        clickElement(loginButton);
    }
    public void hideAndroidKeyboard(){
        try {
            if (System.getProperty("mobile").contains("android")) hideKeyboard();
        }catch (Exception e){
        }
    }

    @SneakyThrows
    public String getLoginIconText() {
        if (System.getProperty("mobile").contains("ios")) {
            Thread.sleep(1000 * 5);
        }
        waitUntilElementVisible(loginTxt);
        return loginTxt.getText();
    }

    @SneakyThrows
    public void waitAppOpen() {
        Thread.sleep(1000 * 5);
        if(commonPage.isIOS()) {
            if(isShow(cancel, 1)){clickElement(cancel);}
            if (isShow(noThanksBtn,3)){
                clickElement(noThanksBtn);
            }
            if (isShow(userNameTxt,2)){
            }else{
                clickElement(loginTxt);
            }
            commonPage.checkUpgrade();
        }
        if(commonPage.isAndroid()) {
            commonPage.checkLogin();
            commonPage.checkUpgrade();
        }
    }

    public String getLoginButtonText() {
        waitUntilElementVisible(loginButton);
        return totalBalance.getText();
    }

    @SneakyThrows
    public void clickTransactionDetails(String screenShotName,String screenShotName1) {
        takeScreenshot(screenShotName);
        scrollUpToFindElement(transaction, 8, 3);
        clickElement(transaction);
        Thread.sleep(9000);
        takeScreenshot(screenShotName1);
    }
    public boolean viewTransactionDetails() {
        List<String> list=new ArrayList<>();
        waitUntilElementVisible(reference,6000);
        boolean detailText=true;
        for (MobileElement text : textView) {
            list.add(text.getText());
        }
        for (MobileElement text : referenceNumber) {
            list.add(text.getText());
        }
        for (String str : list) {
            if (str==""){
                return detailText=false;
            }
        }
        return detailText;
    }
    @SneakyThrows
    public void clickReturnButton(String screenShotName) {
        clickElement(returnButton);
        Thread.sleep(9000);
        takeScreenshot(screenShotName);
    }
    public void Pullup() {
        waitUntilElementVisible(transaction,600000000);
        for (int i=0;i<=100;i++){
            if (!verifyElementExist(transactions)){
                scrollUp();
            }
        }
    }
    @SneakyThrows
    public Boolean getListInformation(String screenShotName){
        List<String> list=new ArrayList<>();
        for (int i=1;i<=getList.size();i++){
            String str=null;
            int a = i + 3;
            if (a >= 10) {
                str = screenShotName + a;
                takeScreenshot(str);
            }
            if (a < 10) {
                str = screenShotName + "0" + a;
                takeScreenshot(str);
            }

            for (MobileElement text : textElement) {
                if (!list.contains(text.getText())) {
                    list.add(text.getText());
                }
                if (isValidDate(text.getText())) {
                    if (!isValidDate(text.getText())) {
                        return false;
                    }
                }
                if (isValidSimpleDateFormat(text.getText())) {
                    if (!isValidSimpleDateFormat(text.getText())) {
                        return false;
                    }
                }
            }
            if (verifyElementExist(seeAll)){
                if (seeAll.getText().equals("See all") ||seeAll.getText().equals("查看更多")) {
                    break;
                }
            }
            scrollUp();
        }
        return listAfter(list);
    }
    public boolean listAfter(List<String> listMaker) throws ParseException, InterruptedException {
        Thread.sleep(3000);
        boolean than=true;
        SimpleDateFormat dateFormat =null;
        if (listMaker.size()!=0){
            if (isValidSimpleDateFormat(listMaker.get(0))){
                dateFormat = new SimpleDateFormat("yyyy年MM月dd日", Locale.ENGLISH);
            }
            if (isValidDate(listMaker.get(0))){
                dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
            }
        }
        for (int i = 0; i < listMaker.size(); i++) {
            int a=i;
            if (i!=0){
                Date date = dateFormat.parse(listMaker.get(i));
                Date datestr = dateFormat.parse(listMaker.get(a-1));
                if (date.after(datestr)){
                    return than=false;
                }
            }
        }
        return than;
    }
    public static boolean isValidDate(String str) {
        boolean convertSuccess=true;
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        try {
            dateFormat.parse(str);
        } catch (ParseException e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }
    public static boolean isValidSimpleDateFormat(String str) {
        boolean convertSuccess=true;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日", Locale.ENGLISH);
        try {
            dateFormat.parse(str);
        } catch (ParseException e) {
            convertSuccess=false;
        }
        return convertSuccess;
    }

    public void getAppVersion() {
        waitUntilElementVisible(appVersion);
        System.out.println("appVersion.getText(): "+appVersion.getText());
        GlobalVar.GLOBAL_VARIABLES.put("appVersion", appVersion.getText());
    }

    public void getScreenShot(String screenShotName) {
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void checkTransactionDetail(String screenShotName,String screenShotName1) {
        scrollUp();
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        clickElement(transactionDetails);
        Thread.sleep(8000);
        takeScreenshot(screenShotName1);
    }

    public void closeAppDriver(){
        if(driver !=null) {
            driver.close();
        }
    }

}
