

Feature: ApplyLoans
  Scenario Outline: <caseName>
    Given test description: ApplyLoans "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|
    |ApplyLoans|1|Get Token|/account-mob/v1/oauth/jwt-token|Get||customerId=882770610630172672||200||||data.jwtToken||||
    |ApplyLoans|2|Get Loans ID|/personal-loan-mob/v1/loan-applications|Post|{Authorization=Bearer ${data.jwtToken}}||body/GetLoansID.json|200||||data.id,data.applicationId||||
    |ApplyLoans|3|Apply Loans|/personal-loan-mob/v1/loan-applications/${data.id}|Get|{Authorization=Bearer ${data.jwtToken}}|||200||||||||
    |ApplyLoans|4|Put personal info|/personal-loan-mob/v1/loan-applications/${data.id}|Put|{Authorization=Bearer ${data.jwtToken}}||body/ApplyLoans.json|200||||||||
    |ApplyLoans|5|Submit|/personal-loan-mob/v1/loan-applications/${data.id}/submit|Post|{Authorization=Bearer ${data.jwtToken}}|||200||||||||
    |ApplyLoans|6|Get Offer|/personal-loan-mob/v1/loan-applications/${data.id}/offers|Get|{Authorization=Bearer ${data.jwtToken}}|||200||||data[0].id||||
    |ApplyLoans|7|Select Offer|/personal-loan-mob/v1/loan-applications/${data.id}/offers/${data[0].id}/confirm|Post|{Authorization=Bearer ${data.jwtToken}}|||200||||||||
    |ApplyLoans|8|Upload Document|/personal-loan-mob/v1/loans/${data.id}/docs/batch|Post|{Authorization=Bearer ${data.jwtToken}}||body/UploadProofFiles.json|200||||||||
