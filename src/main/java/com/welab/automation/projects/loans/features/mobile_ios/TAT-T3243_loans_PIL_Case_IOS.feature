Feature: Loans PIL Case

  Background: Login
    Given Open loans WeLab App

  @VBL-T2951
  Scenario: Reg_Loan_PIL_001 PIL ETB Happy Flow IOS fill information
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_001_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page
      | screenShotName  | Reg_Loans_001_IOS_02 |
      | screenShotName1 | Reg_Loans_001_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sample               |
      | screenShotName      | Reg_Loans_001_IOS_04 |
      | screenShotName1     | Reg_Loans_001_IOS_05 |
      | screenShotName2     | Reg_Loans_001_IOS_06 |
      | screenShotName3     | Reg_Loans_001_IOS_07 |
      | screenShotName4     | Reg_Loans_001_IOS_08 |
      | screenShotName5     | Reg_Loans_001_IOS_09 |
      | screenShotName6     | Reg_Loans_001_IOS_10 |
      | screenShotName7     | Reg_Loans_001_IOS_11 |
      | screenShotName8     | Reg_Loans_001_IOS_12 |
      | screenShotName9     | Reg_Loans_001_IOS_13 |
      | screenShotName10    | Reg_Loans_001_IOS_14 |
      | screenShotName11    | Reg_Loans_001_IOS_15 |
    Then know more about you IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&08        |
      | screenShotName  | Reg_Loans_001_IOS_16 |
      | screenShotName1 | Reg_Loans_001_IOS_17 |
      | screenShotName2 | Reg_Loans_001_IOS_18 |
      | screenShotName3 | Reg_Loans_001_IOS_18 |
      | screenShotName4 | Reg_Loans_001_IOS_19 |
      | screenShotName5 | Reg_Loans_001_IOS_20 |
      | screenShotName6 | Reg_Loans_001_IOS_21 |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_001_IOS_22 |
      | screenShotName1 | Reg_Loans_001_IOS_23 |
      | screenShotName2 | Reg_Loans_001_IOS_24 |
      | screenShotName3 | Reg_Loans_001_IOS_25 |
    Then I share hkid

  @VBL-T2951
  Scenario: Reg_Loan_PIL_001 PIL ETB Happy Flow IOS upload files
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_001_IOS_01 |
    Then upload files
      | screenShotName   | Reg_Loans_001_IOS_26 |
      | screenShotName1  | Reg_Loans_001_IOS_27 |
      | screenShotName2  | Reg_Loans_001_IOS_28 |
      | screenShotName3  | Reg_Loans_001_IOS_29 |
      | screenShotName4  | Reg_Loans_001_IOS_30 |
      | screenShotName5  | Reg_Loans_001_IOS_31 |
      | screenShotName6  | Reg_Loans_001_IOS_32 |
      | screenShotName7  | Reg_Loans_001_IOS_33 |
      | screenShotName8  | Reg_Loans_001_IOS_34 |
      | screenShotName9  | Reg_Loans_001_IOS_35 |
      | screenShotName10 | Reg_Loans_001_IOS_36 |

  @VBL-T2952
  Scenario: Reg_Loan_PIL_002 PIL Calculator and KFS page IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_002_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page
      | screenShotName  | Reg_Loans_002_IOS_02 |
      | screenShotName1 | Reg_Loans_002_IOS_03 |

  @VBL-T2967
  Scenario: Reg_Loan_PIL_003 NPP skip document upload IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_003_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page And Set Loans Amount
      | Amount          | 30000                |
      | screenShotName  | Reg_Loans_003_IOS_02 |
      | screenShotName1 | Reg_Loans_003_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 40000                |
      | incomeType          | employed             |
      | Building            | sampleRisk&08        |
      | screenShotName      | Reg_Loans_003_IOS_04 |
      | screenShotName1     | Reg_Loans_003_IOS_05 |
      | screenShotName2     | Reg_Loans_003_IOS_06 |
      | screenShotName3     | Reg_Loans_003_IOS_07 |
      | screenShotName4     | Reg_Loans_003_IOS_08 |
      | screenShotName5     | Reg_Loans_003_IOS_09 |
      | screenShotName6     | Reg_Loans_003_IOS_10 |
      | screenShotName7     | Reg_Loans_003_IOS_11 |
      | screenShotName8     | Reg_Loans_003_IOS_12 |
      | screenShotName9     | Reg_Loans_003_IOS_13 |
      | screenShotName10    | Reg_Loans_003_IOS_14 |
      | screenShotName11    | Reg_Loans_003_IOS_15 |
    Then know more about you IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&82        |
      | screenShotName  | Reg_Loans_003_IOS_16 |
      | screenShotName1 | Reg_Loans_003_IOS_17 |
      | screenShotName2 | Reg_Loans_003_IOS_18 |
      | screenShotName3 | Reg_Loans_003_IOS_18 |
      | screenShotName4 | Reg_Loans_003_IOS_19 |
      | screenShotName5 | Reg_Loans_003_IOS_20 |
      | screenShotName6 | Reg_Loans_003_IOS_21 |
    Then check The Page After When Skip Upload Picture Submit Application IOS
      | screenShotName  | Reg_Loans_003_IOS_22 |
      | screenShotName1 | Reg_Loans_003_IOS_23 |
    Then I share hkid

  @VBL-T2970
  Scenario: Reg_Loan_PIL_004 PIL purpose of loan Credit card loan repayment IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_004_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page
      | screenShotName  | Reg_Loans_004_IOS_02 |
      | screenShotName1 | Reg_Loans_004_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sample               |
      | screenShotName      | Reg_Loans_004_IOS_04 |
      | screenShotName1     | Reg_Loans_004_IOS_05 |
      | screenShotName2     | Reg_Loans_004_IOS_06 |
      | screenShotName3     | Reg_Loans_004_IOS_07 |
      | screenShotName4     | Reg_Loans_004_IOS_08 |
      | screenShotName5     | Reg_Loans_004_IOS_09 |
      | screenShotName6     | Reg_Loans_004_IOS_10 |
      | screenShotName7     | Reg_Loans_004_IOS_11 |
      | screenShotName8     | Reg_Loans_004_IOS_12 |
      | screenShotName9     | Reg_Loans_004_IOS_13 |
      | screenShotName10    | Reg_Loans_004_IOS_14 |
      | screenShotName11    | Reg_Loans_004_IOS_15 |
    Then know more about you IOS
      | Purpose         | CreditCardOrLoanRepayment |
      | Building        | sampleRisk&08             |
      | screenShotName  | Reg_Loans_004_IOS_16      |
      | screenShotName1 | Reg_Loans_004_IOS_17      |
      | screenShotName2 | Reg_Loans_004_IOS_18      |
      | screenShotName3 | Reg_Loans_004_IOS_18      |
      | screenShotName4 | Reg_Loans_004_IOS_19      |
      | screenShotName5 | Reg_Loans_004_IOS_20      |
      | screenShotName6 | Reg_Loans_004_IOS_21      |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_004_IOS_22 |
      | screenShotName1 | Reg_Loans_004_IOS_23 |
      | screenShotName2 | Reg_Loans_004_IOS_24 |
      | screenShotName3 | Reg_Loans_004_IOS_25 |
    Then I share hkid

  @VBL-T2970
  Scenario: Reg_Loan_PIL_004 PIL purpose of loan Credit card loan repayment IOS upload
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_004_IOS_01 |
    Then upload files with Credit card Personal loan proof
      | screenShotName   | Reg_Loans_004_IOS_26 |
      | screenShotName1  | Reg_Loans_004_IOS_27 |
      | screenShotName2  | Reg_Loans_004_IOS_28 |
      | screenShotName3  | Reg_Loans_004_IOS_29 |
      | screenShotName4  | Reg_Loans_004_IOS_30 |
      | screenShotName5  | Reg_Loans_004_IOS_31 |
      | screenShotName6  | Reg_Loans_004_IOS_32 |
      | screenShotName7  | Reg_Loans_004_IOS_33 |
      | screenShotName8  | Reg_Loans_004_IOS_34 |
      | screenShotName9  | Reg_Loans_004_IOS_35 |
      | screenShotName10 | Reg_Loans_004_IOS_36 |


  @VBL-T2968
  Scenario: Reg_Loan_PIL_004 loans ekyc fill information set Building sampleQA221 IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_005_IOS_01 |
    Then go To Loans PIL Page
    Then check KFS page
      | screenShotName  | Reg_Loans_005_IOS_02 |
      | screenShotName1 | Reg_Loans_005_IOS_03 |
    Then fill about your job IOS
      | monthlyIncomeAmount | 50000                |
      | incomeType          | employed             |
      | Building            | sampleQA&221         |
      | screenShotName      | Reg_Loans_005_IOS_04 |
      | screenShotName1     | Reg_Loans_005_IOS_05 |
      | screenShotName2     | Reg_Loans_005_IOS_06 |
      | screenShotName3     | Reg_Loans_005_IOS_07 |
      | screenShotName4     | Reg_Loans_005_IOS_08 |
      | screenShotName5     | Reg_Loans_005_IOS_09 |
      | screenShotName6     | Reg_Loans_005_IOS_10 |
      | screenShotName7     | Reg_Loans_005_IOS_11 |
      | screenShotName8     | Reg_Loans_005_IOS_12 |
      | screenShotName9     | Reg_Loans_005_IOS_13 |
      | screenShotName10    | Reg_Loans_005_IOS_14 |
      | screenShotName11    | Reg_Loans_005_IOS_15 |
    Then know more about you IOS
      | Purpose         | buyCar               |
      | Building        | sampleRisk&08        |
      | screenShotName  | Reg_Loans_005_IOS_16 |
      | screenShotName1 | Reg_Loans_005_IOS_17 |
      | screenShotName2 | Reg_Loans_005_IOS_18 |
      | screenShotName3 | Reg_Loans_005_IOS_18 |
      | screenShotName4 | Reg_Loans_005_IOS_19 |
      | screenShotName5 | Reg_Loans_005_IOS_20 |
      | screenShotName6 | Reg_Loans_005_IOS_21 |
    Then check The Page After Submit Application IOS
      | screenShotName  | Reg_Loans_005_IOS_22 |
      | screenShotName1 | Reg_Loans_005_IOS_23 |
      | screenShotName2 | Reg_Loans_005_IOS_24 |
      | screenShotName3 | Reg_Loans_005_IOS_25 |
    Then I share hkid

  @VBL-T2968
  Scenario: Reg_Loan_004 Loan ekyc upload files IOS
    And skip Root ios
    When Login with user and password from properties
    Then check Unexpected Element
    Then I get loans page
      | screenShotName | Reg_Loans_005_IOS_01 |
    Then upload files with Ekyc
      | screenShotName   | Reg_Loans_005_IOS_26 |
      | screenShotName1  | Reg_Loans_005_IOS_27 |
      | screenShotName2  | Reg_Loans_005_IOS_28 |
      | screenShotName3  | Reg_Loans_005_IOS_29 |
      | screenShotName4  | Reg_Loans_005_IOS_30 |
      | screenShotName5  | Reg_Loans_005_IOS_31 |
      | screenShotName6  | Reg_Loans_005_IOS_32 |
      | screenShotName7  | Reg_Loans_005_IOS_33 |
      | screenShotName8  | Reg_Loans_005_IOS_34 |
      | screenShotName9  | Reg_Loans_005_IOS_35 |
      | screenShotName10 | Reg_Loans_005_IOS_36 |