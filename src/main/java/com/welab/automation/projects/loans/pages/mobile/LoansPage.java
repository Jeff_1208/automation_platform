package com.welab.automation.projects.loans.pages.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.base.RestAPI;
import com.welab.automation.framework.utils.TimeUtils;
import com.welab.automation.framework.utils.ZephyrScaleUtils;
import com.welab.automation.framework.utils.api.SendRequest;
import com.welab.automation.framework.utils.entity.api.ConfigUtils;
import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class LoansPage extends AppiumBasePage {

    protected String pageName = "Loans Page";
    GoalSettingPage goalSettingPage;
    CommonPage commonPage;
    private static Response response;
    public static String HkId;
    public LoansPage() {
        super.pageName = this.pageName;
        goalSettingPage = new GoalSettingPage();
        commonPage = new CommonPage();
    }


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Personal Loan']"),
            @AndroidBy(xpath = "//*[@text='私人分期貸款']")
    })
    private MobileElement personalLoan;

    @AndroidFindAll({
            @AndroidBy(accessibility = "Loan, tab, 4 of 5"),
            @AndroidBy(accessibility = "貸款, tab, 4 of 5"),
            @AndroidBy(xpath = "//*[@text='Loan']"),
            @AndroidBy(xpath = "//*[@text='loan']"),
            @AndroidBy(xpath = "//*[@content-desc=\"Loan, tab, 4 of 5\"]"),
            @AndroidBy(xpath = "//*[@text='貸款']")
    })
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"貸款, tab, 4 of 5\"")
    private MobileElement loanBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Personal Instalment Loan']"),
            @AndroidBy(xpath = "//*[@text='私人分期貸款']")
    })
    private MobileElement personalInstalmenBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Card Debt Consolidation Loan']"),
    })
    private MobileElement dcPageBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Get extra cash instantly!']"),
    })
    private MobileElement rfPageBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='How we help you repay credit cards & loans']"),
    })
    private MobileElement dcTenorPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='How much do you want to borrow?']"),
    })
    private MobileElement rfTenorPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.EditText")
    })
    private MobileElement loanAmountInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Apply Now']"),
            @AndroidBy(xpath = "//*[@text='立即申請']")
    })
    private MobileElement applyNow;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[1]")
    private MobileElement tenorStart;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[11]")
    private MobileElement tenorEnd;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]")
    private List<MobileElement> tenorList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Key Facts Statements']"),
            @AndroidBy(xpath = "//*[@text='產品資料概要']")
    })
    private MobileElement keyFactsStatement;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'read and agree')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'我已閱讀及同意')]")
    })
    private MobileElement readAgree;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='About your job']"),
            @AndroidBy(xpath = "//*[@text='有關你嘅工作']")
    })
    private MobileElement aboutYourJob;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Company name']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='公司名稱']/../android.widget.EditText")
    })
    private MobileElement companyName;

    @AndroidFindAll({
            @AndroidBy(
                    xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText"),
    })
    private MobileElement companyNameInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='行業']"),
            @AndroidBy(xpath = "//*[@text='Industry']")
    })
    private MobileElement industry;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Business nature']"),
            @AndroidBy(xpath = "//*[@text='業務性質']")
    })
    private MobileElement businessNature;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Job title']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='職位名稱']/../android.widget.EditText")
    })
    private MobileElement jobTitle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText")
    })
    private MobileElement jobTitleInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='職位']"),
            @AndroidBy(xpath = "//*[@text='Seniority']")
    })
    private MobileElement seniority;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='現職公司任職年期']"),
            @AndroidBy(xpath = "//*[@text='Year(s) of service with present employer']")
    })
    private MobileElement yearsOfService;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Year(s) of industry']"),
            @AndroidBy(xpath = "//*[@text='行業年資']")
    })
    private MobileElement yearsOfIndustry;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Type of income']"),
            @AndroidBy(xpath = "//*[@text='收入來源']")
    })
    private MobileElement typeOfIncome;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Monthly income']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='每月收入']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='Rental income']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='租金收入']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='Latest three months average monthly income']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='最近三個月平均收入']/../android.widget.EditText")
    })
    private MobileElement monthlyIncome;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Next']"),
            @AndroidBy(xpath = "//*[@text='下一步']")
    })
    private MobileElement next;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    private List<MobileElement> optionsFromDownList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Done']"),
            @AndroidBy(xpath = "//*[@text='完成']")
    })
    private MobileElement done;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
    private List<MobileElement> typeOfIncomeList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Autopay']"),
            @AndroidBy(xpath = "//*[@text='自動轉帳']")
    })
    private MobileElement AutoPay;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView"),
            @AndroidBy(xpath = "//*[@text='Office address']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='公司地址']/following-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//*[@text='District']")
    })
    private MobileElement District;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Street']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='街道名稱']/../android.widget.EditText")
    })
    private MobileElement Street;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Building']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='大廈']/../android.widget.EditText")
    })
    private MobileElement Building;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Floor')]/../android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'樓層')]/../android.widget.EditText")
    })
    private MobileElement Floor;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Room']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='單位']/../android.widget.EditText")
    })
    private MobileElement Room;

    @AndroidFindAll({
            @AndroidBy(xpath ="//android.widget.TextView[contains(@text,'Office phone number')]/../android.widget.EditText"),
            @AndroidBy(xpath ="//android.widget.TextView[contains(@text,'公司電話號碼')]/../android.widget.EditText")
    })
    private MobileElement OfficePhoneNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Know more about you']"),
            @AndroidBy(xpath = "//*[@text='更多有關你嘅資料']")
    })
    private MobileElement knowMoreAboutYou;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Education level']"),
            @AndroidBy(xpath = "//*[@text='教育程度']"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView")
    })
    private MobileElement educationLevel;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Residential status']"),
            @AndroidBy(xpath = "//*[@text='住宅狀況']")
    })
    private MobileElement residentialStatus;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Home phone number']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='住宅電話號碼']/../android.widget.EditText")
    })
    private MobileElement homePhoneNumber;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Purpose of loan')]"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'貸款用途')]")
    })
    private MobileElement purposeOfLoan;


    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Promotion code']/../android.widget.EditText"),
    })
    private MobileElement promotionCode;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Monthly mortgage instalment']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='每月按揭供款額']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='Monthly rental']/../android.widget.EditText"),
            @AndroidBy(xpath = "//*[@text='每月租金']/../android.widget.EditText")
    })
    private MobileElement monthlyMortgageInstalment ;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Review and confirm']"),
            @AndroidBy(xpath ="//*[@text='覆核及確認']")
    })
    private MobileElement reviewAndconfirm ;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'I confirm I've (a) read the summary page')]/preceding-sibling::android.view.ViewGroup[1]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'我確認我已')]/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'I confirm')]/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup")
    })
    private MobileElement circle ;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Building / Block')]/../android.widget.EditText"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'大廈名稱 / 座數')]/../android.widget.EditText")
    })
    private MobileElement buildingBlockInput;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement confirmInresidentialInformation;

    @AndroidFindAll({
            @AndroidBy(xpath ="//android.widget.TextView[contains(@text,'Confirm')]"),
            @AndroidBy(xpath ="//android.widget.TextView[contains(@text,'確定')]"),
            @AndroidBy(xpath ="//*[@text='Confirm']"),
    })
    private MobileElement  confirmBtn;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Submit application']"),
            @AndroidBy(xpath ="//*[@text='遞交申請']")
    })
    private MobileElement  submitApplication ;


    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Tailor-made plan']"),
    })
    private MobileElement  thePlanPage ;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Document upload']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='上載文件']")
    })
    private MobileElement documentUpload;

    String listOption="//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup";

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']"),
            @AndroidBy(xpath = " //android.widget.TextView[@text='WeLab Bank 私人貸款']")
    })
    private MobileElement personalLoanTitle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Check it now']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Check it Now']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='立即查看']")
    })
    private MobileElement checkItNowBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[2]"),
    })
    private MobileElement residentialInformationEdit;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'By pressing the')]"),
            @AndroidBy(xpath = "//*[contains(@text,'透過按下')]")})
    private MobileElement byPressingText;

    //Congratulation! Your loan application has been preliminarily approved
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'Congratulation')]"),
    })
    private MobileElement Congratulation;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Approval letter details']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='貸款確認書 (PDF)']")
    })
    private MobileElement approvalLetterDetails;

    @AndroidFindAll({@AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")})
    private MobileElement returnBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Repayment schedule']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='供款時間表']")
    })
    private MobileElement repaymentSchedule;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Terms and Conditions']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='條款及細則']")
    })
    private MobileElement termsAndConditions;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Key Facts Statements']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='產品資料概要']")
    })
    private MobileElement keyFactsStatements;

    @AndroidFindBy(
            xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
    private MobileElement eyeBtn;

    @AndroidFindBy(
            xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]")
    private MobileElement rocketButton;

    @AndroidFindAll({
            @AndroidBy(
                    xpath =
                            "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement http;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]"),
    })
    private MobileElement getHttp;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Dev Panel']"),
    })
    private MobileElement devPanel;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Upload']"),
            @AndroidBy(xpath = "//*[@text='Upload']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='上載']")
    })
    private MobileElement uploadBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Preparing']"),
    })
    private MobileElement uploadOkPagePreparing;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Income proof']"),
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[4]")
    })
    private MobileElement IncomeProof;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Sample']"),
    })
    private MobileElement sampleBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[8]/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Sample']/../following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement addPhotoBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Sample']/../following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement addPhotoBtn2;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Latest 1 month bank statement showing the loan account number']"),
    })
    private MobileElement addPhotoBtn3Page;
    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Latest 1 month bank statement showing the loan account number']/following-sibling::android.view.ViewGroup/android.view.ViewGroup"),
    })
    private MobileElement addPhotoBtn3;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Photo library']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='圖庫']")
    })
    private MobileElement photoLibrary;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='知道喇']")
    })
    private MobileElement gotItBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='允许']")
    })
    private MobileElement allowButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//com.google.android.documentsui:id/icon_mime_lg")
    })
    private MobileElement firstPhoto;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Take photo']")
    })
    private MobileElement takePhoto;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc=\"拍照\"]")
    })
    private MobileElement Photograph;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc=\"Done\"]"),
            @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc=\"确定\"]")
    })
    private MobileElement rightSign;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='No thanks']\n"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='No, thanks']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='唔需要']")
    })
    private MobileElement noThanksBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Residential address proof']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='住址證明']")
    })
    private MobileElement residentialAddressProof;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Credit card/Personal loan proof']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='住址證明']")
    })
    private MobileElement creditCardPersonalLoanProof;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Done']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='完成']")
    })
    private MobileElement doneBtn;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"),
    })
    private MobileElement applyButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ImageView"),
    })
    private MobileElement modify;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.EditText"),
    })
    private MobileElement input;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView"),
    })
    private MobileElement purpose;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Agriculture, Forestry & Fishing']"),
            @AndroidBy(xpath = "//*[@text='農業、林業及漁業']")
    })
    private MobileElement industryOption;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Agriculture']"),
            @AndroidBy(xpath = "//*[@text='Fitness Centre / Sports Centre']"),
            @AndroidBy(xpath = "//*[@text='農業']")
    })
    private MobileElement businessNatureOption;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Employed - Regular income']"),
            @AndroidBy(xpath = "//*[@text='受僱 - 固定收入']")
    })
    private MobileElement typeOfIncomeOption;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Aberdeen']")
    })
    private MobileElement districtOption;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Bachelor, Higher Diploma or Diploma']"),
            @AndroidBy(xpath = "//*[starts-with(@text, 'Bachelor')]"),
            @AndroidBy(xpath = "//*[@text='學士、文憑及高級文憑']")
    })
    private MobileElement educationLevelOption;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Bachelor, Higher Diploma or Diploma']"),
    })
    private List<MobileElement> educationLevelOptionList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Self-Owned (Without Mortgage)']"),
    })
    private List<MobileElement> residentialStatusOptionList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Car purchase']"),
            @AndroidBy(xpath = "//*[@text='買車']")
    })
    private List<MobileElement> purposeOfLoanOptionList;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Self-Owned (Without Mortgage)']"),
            @AndroidBy(xpath = "//*[@text='自置（沒有按揭貸款）']")
    })
    private MobileElement residentialStatusOption;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Car purchase']"),
            @AndroidBy(xpath = "//*[@text='買車']")
    })
    private MobileElement purposeOfLoanOption;

    @AndroidFindAll({
            @AndroidBy(xpath = " //android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.widget.TextView")
    })
    private MobileElement tenorInitial;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.widget.TextView")
    })
    private MobileElement tenorInitialSamsungS20;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'By pressing the')]/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'透過按下')]/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[6]/android.view.ViewGroup")
    })
    private MobileElement byPressingBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Agree and drawdown']"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='同意及提取貸款']")
    })
    private MobileElement agreeAnDrawdownBtn;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[starts-with(@text, 'We are working on your loan agreement as fast as we can')]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='We are working on your loan agreement as fast as we can. As always, you may contact our Customer Support hotline 24/7 at (852) 3898 6988.']"),
    })
    private MobileElement workingOnYourloanAgreementPage;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='My Loan']"),
    })
    private MobileElement myLoanItem;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Cancel']"),
    })
    private MobileElement CancelButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Please deposit into Core Account for your WeLab Bank Personal Loan repayment now.']"),
    })
    private MobileElement myLoanWithDeliquentMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Total outstanding (HKD)']"),
    })
    private MobileElement TotalOutstanding;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Monthly repayment']//..//android.widget.TextView[3]"),
    })
    private MobileElement monthlyRepayment;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Late charge']//..//android.widget.TextView[5]"),
    })
    private MobileElement lateCharge;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Total outstanding (HKD)']//..//android.widget.TextView[2]"),
    })
    private MobileElement loanAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Loan Detail']"),
    })
    private MobileElement myLoanDetail;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']//../android.view.ViewGroup[2]"),
    })
    private MobileElement repayingTabOPenButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']"),
    })
    private MobileElement PersonalLoanPage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']//../android.view.ViewGroup[4]"),
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']//../android.view.ViewGroup[4]/android.widget.TextView"),
    })
    private MobileElement secondRepaymentAmountOPenButton;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']//../android.widget.TextView[4]"),
    })
    private MobileElement firstRepaymentAmount;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Personal Loan']//../android.widget.TextView[8]"),
    })
    private MobileElement secondRepaymentAmount;


    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'By using a promotion code and participating in a specific campaign')]"),
    })
    private MobileElement PromotionCodeMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Promotion code']"),
    })
    private MobileElement PromotionCode;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[@text='Promotion code']/follwing-sibling::android.widget.EditText[1]"),
    })
    private MobileElement PromotionCodeInput;

    @AndroidFindAll({
            //@AndroidBy(xpath = "//android.widget.TextView[@text='I confirm I've read and fully understand the reminders on Responsible Borrowing.']/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup"),  // valid
            //@AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]"),
    })
    private MobileElement confirmFullyUnderstandCircle;

    //I confirm I've (a) read the summary page and (b) read, understood, acknowledged, and agreed to be bound by the Welab Bank Personal Loan Terms and Conditions, the Customer Declaration, the Privacy Policy Statement and the Key Facts Statements and related promotion(s) Terms and Conditions.
    @AndroidFindAll({
            //@AndroidBy(xpath = "//android.widget.TextView[contains(@text,'I confirm I've (a) read the summary page')]/preceding-sibling::android.view.ViewGroup[1]/android.view.ViewGroup"),
            @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup"), // valid
            //@AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]"),
    })
    private MobileElement confirmSummaryPageCircle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'fully understand')]"),
    })
    private MobileElement confirmFullyUnderstandMessage;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'summary page')]"),
    })
    private MobileElement confirmSummaryPageMessage;

    @SneakyThrows
    public boolean goToLoansPage(String screenShotName) {
        Thread.sleep(20000);
        waitUntilElementVisible(loanBtn);
        clickElement(loanBtn);
        waitUntilElementVisible(personalLoan);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
        return verifyElementExist(personalLoan);
    }

    @SneakyThrows
    public void goToLoansPage2(String screenShotName) {
        Thread.sleep(10000);
        clickElement(loanBtn);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public boolean goToPersonalInstalmenPage(String screenShotName){
        Thread.sleep(2000);
        clickElement(personalInstalmenBtn);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        return verifyElementExist(personalInstalmenBtn);
    }

    @SneakyThrows
    public void iGotoDcPage(String screenShotName){
        clickElement(dcPageBtn);
        waitUntilElementVisible(dcTenorPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void iGotoRfPage(String screenShotName){
        Thread.sleep(3000);
        clickElement(rfPageBtn);
        waitUntilElementVisible(rfTenorPage);
        Thread.sleep(1000);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void setLoanAmountAndTenor(String amount, String tenor,String screenShotName) {
        clickElement(loanAmountInput);
        clearAndSendKeys(loanAmountInput,amount);
        inputEnter(loanAmountInput);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        setTenor(tenor);
    }

    @SneakyThrows
    public void setTenor(String tenor){
        int num = 0;
        if(isSamSungS20()){
            num =Integer.parseInt(tenorInitialSamsungS20.getText());
        }else{
            num =Integer.parseInt(tenorInitial.getText());
        }
        int ageIndex = Integer.parseInt(tenor);
        if(num!=ageIndex) {
            if (ageIndex < 6) {
                ageIndex = 6;
            }
            if (ageIndex % 3 == 1) {
                ageIndex += 1;
            } else if (ageIndex % 3 == 2) {
                ageIndex += 2;
            }
            if (ageIndex > 60) {
                ageIndex = 60;
            }
            Thread.sleep(5000);
            MobileElement targetTimeElementStart =
                    driver.findElement
                            (By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[" + num / 3 + "]"));
            MobileElement targetTimeElement =
                    driver.findElement
                            (By.xpath("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[" + ageIndex / 3 + "]"));
            goalSettingPage.setAge(targetTimeElementStart, targetTimeElement);
        }
    }

    @SneakyThrows
    public boolean canSeeKeyFactsStatementsPage() {
        Thread.sleep(2000);
        scrollUpToFindElement(applyNow,3, 3);
        clickElement(applyNow);
        Thread.sleep(5000);
        return verifyElementExist(keyFactsStatement);
    }

    @SneakyThrows
    public void readKeyFactsStatementsPageAndAgree(String screenShotName) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
//        scrollUpEntireToFindElement(readAgree,16,2);
        for (int i = 0; i < 20; i++) {
            scrollUpFast(100);
        }
        Thread.sleep(5000);
        waitUntilElementClickable(readAgree);
        clickElement(readAgree);
    }

    @SneakyThrows
    public boolean canSeeAboutYourJobPage(String screenShotName) {
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        return verifyElementExist(aboutYourJob);
    }

    @SneakyThrows
    public void fillInTheInfoInTheAboutYourJobPage(Map<String, String> data) {
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName"));
        clickElement(companyName);
        clearAndSendKeys(companyNameInput,data.get("companyName"));
        inputEnter(companyNameInput);
        selectIndustry();
        selectBusinessNature();
        fillSeniorityAndJobTitle(data.get("jobTitleName"));
        selectYearOfService();
        selectTypeOfIncome();
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName1"));

        scrollUp();
        //scrollUp();  //samsung need scroll one more time
        Thread.sleep(5000);
        clickElement(monthlyIncome);
        andSendKeys(monthlyIncome,data.get("monthlyIncomeAmount"));
        inputEnter(monthlyIncome);
        Thread.sleep(5000);
        selectSalaryMethod();
        scrollUp();
        selectDistrict();
        fillAddress(data.get("Street"),data.get("Building"),data.get("Floor"),data.get("Room"),data.get("OfficePhoneNumber"));
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName2"));
    }


    public void fillAddress(String streetName,String BuildingName,String FloorName,String RoomName,String OfficePhone){
        clickElement(Street);
        clearAndSendKeys(Street,streetName);
        inputEnter(Street);
        scrollUp();
        clickElement(Building);
        clearAndSendKeys(Building,BuildingName);
        inputEnter(Building);
        clickElement(Floor);
        clearAndSendKeys(Floor,FloorName);
        inputEnter(Floor);
        clickElement(Room);
        clearAndSendKeys(Room,RoomName);
        inputEnter(Room);
        clickElement(OfficePhoneNumber);
        clearAndSendKeys(OfficePhoneNumber,OfficePhone);
        inputEnter(OfficePhoneNumber);
    }

    @SneakyThrows
    public void selectInAboutYourJob(){
        Thread.sleep(5000);
        int size = optionsFromDownList.size();
        int number = rand.nextInt(size)+1;
        Thread.sleep(5000);
        driver.findElement(By.xpath(listOption+"["+number+"]")).click();
    }

    Random rand = new Random();

    @SneakyThrows
    private void selectIndustry() {
        Thread.sleep(5000);
        clickElement(industry);
        Thread.sleep(2000);
        clickElement(industryOption);

    }

    @SneakyThrows
    public void selectBusinessNature(){
        Thread.sleep(5000);
        clickElement(businessNature);
        Thread.sleep(2000);
        clickElement(businessNatureOption);
    }

    @SneakyThrows
    private void fillSeniorityAndJobTitle(String jobTitleName) {
        Thread.sleep(5000);
        clickElement(jobTitle);
        clearAndSendKeys(jobTitle,jobTitleName);
        inputEnter(jobTitle);
    }

    @SneakyThrows
    private void selectYearOfService() {
        clickElement(yearsOfService);
        Thread.sleep(2000);
        scrollLeft();
        clickElement(done);
    }

    @SneakyThrows
    private void selectTypeOfIncome() {
        Thread.sleep(5000);
        clickElement(typeOfIncome);
        Thread.sleep(2000);
        clickElement(typeOfIncomeOption);
    }

    @SneakyThrows
    private void selectSalaryMethod() {
        Thread.sleep(5000);
        clickElement(AutoPay);
        scrollUp();
    }

    @SneakyThrows
    private void selectDistrict() {
        Thread.sleep(5000);
        clickElement(District);
        Thread.sleep(2000);
        clickElement(districtOption);
    }

    @SneakyThrows
    public boolean gotoKonwMoreAboutYouPage(String screenShotName) {
        Thread.sleep(2000);
        clickElement(next);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        return verifyElementExist(knowMoreAboutYou);
    }

    public void selectEducation(){
        int size  = educationLevelOptionList.size();
        clickElement(educationLevelOptionList.get(size-1));
    }

    public void selectResidentialStatus(){
        int size  = residentialStatusOptionList.size();
        clickElement(residentialStatusOptionList.get(size-1));
    }
    public void selectPurposeOfLoan(){
        int size  = purposeOfLoanOptionList.size();
        clickElement(purposeOfLoanOptionList.get(size-1));
    }


    @SneakyThrows
    public void fillInTheInfoInThePageKnowMoreAboutYour(Map<String, String> data) {
        fillInfoForKnowMorePage(data);
        //clickElement(purposeOfLoanOption);
        selectPurposeOfLoan();
        Thread.sleep(2000);
        scrollUpToFindElement(next,2,2);
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName1"));
    }

    @SneakyThrows
    public void fillInfoForKnowMorePage(Map<String, String> data){
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName"));
        clickElement(educationLevel);
        Thread.sleep(2000);
        //clickElement(educationLevelOption);
        selectEducation();
        Thread.sleep(2000);
        clickElement(residentialStatus);
        Thread.sleep(2000);
        //clickElement(residentialStatusOption);
        selectResidentialStatus();

        Thread.sleep(5000);
        clickElement(homePhoneNumber);
        andSendKeys(homePhoneNumber,data.get("homePhoneNumber"));
        inputEnter(homePhoneNumber);
        Thread.sleep(5000);
        clickElement(purposeOfLoan);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void fillInTheInfoInThePageKnowMoreAboutYourForDcFlow(Map<String, String> data){
        fillInfoForKnowMorePage(data);
        Thread.sleep(2000);
        scrollUpToFindElement(next,2,2);
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName1"));
    }

    @SneakyThrows
    public boolean gotoReviewAndConfirmPage(String screenShotName) {
        scrollUp();
        scrollUp();
        Thread.sleep(2000);
        clickElement(next);
        Thread.sleep(5000);
        takeScreenshot(screenShotName);
        return verifyElementExist(reviewAndconfirm);
    }

    public void clickFirstCycle(){
        int yPercent = getYPercentByElement(confirmFirstCircle);
        clickByLocationByPercent(8, yPercent);
    }

    public void clickSecondCycle(){
        int yPercent = getYPercentByElement(confirmSecondCircle);
        clickByLocationByPercent(8, yPercent);
    }

    @SneakyThrows
    public void DebugForCircle(){
        //Need Comment step from  I fill in the info in the About your job Page (Include)
        // to I goto Review and confirm Page (Include)
        if(isShow(next,3)){
            clickElement(next);
        }
        Thread.sleep(1000);
        if(isShow(next,3)){
            clickElement(next);
        }
        Thread.sleep(1000);
    }

    public void clickFirstCircle(){
        //clickElement(confirmFullyUnderstandCircle);
        int y = getYPercentByElement(confirmFullyUnderstandMessage);
        if(isSamSungS20()||isXiaomiA3()){
            clickByLocationByPercent2(9.5, y);
        }else{
            clickByLocationByPercent2(9.5, y-1);
        }
    }

    public void clickSecondCircle(){
        //clickElement(confirmSummaryPageCircle);
        int y = getYPercentByElement(confirmSummaryPageMessage);
        clickByLocationByPercent2(9.5, y);
    }

    @SneakyThrows
    public void changeTheInfoInThePageReviewAndConfirm(Map<String,String> data) {
        //DebugForCircle();  // just for debug when infomation is fill
        changeResidentialInformation(data.get("screenShotName"));
        //scrollUpToFindElement(submitApplication,8,2);
        for (int i = 0; i < 8; i++) {
            scrollUpFast();
        }
        Thread.sleep(3000);
        clickFirstCircle();
        Thread.sleep(1000);
        clickSecondCircle();
        Thread.sleep(1000);
        waitUntilElementClickable(submitApplication);
        takeScreenshot(data.get("screenShotName1"));
    }

    @SneakyThrows
    public void changeResidentialInformation(String screenShotName){
        waitUntilElementVisible(residentialInformationEdit);
        clickElement(residentialInformationEdit);
        Thread.sleep(2000);
        clickElement(buildingBlockInput);
        clearAndSendKeys(buildingBlockInput,"sampleRisk08");
        inputEnter(buildingBlockInput);
        Thread.sleep(2000);
        takeScreenshot(screenShotName);
        clickElement(confirmInresidentialInformation);
    }

    @SneakyThrows
    public void gotoThePlanPage(String screenShotName, String screenShotName1) {
        scrollUp();
        Thread.sleep(1000);
        clickElement(submitApplication);
        //waitUntilElementVisible(MeetYourPlan);
        Thread.sleep(10*000);
        takeScreenshot(screenShotName);
        scrollUp();
        scrollUp();
        if(isShow(Confirm, 3)){
            clickElement(Confirm);
        }
        if(isShow(documentUploadPage, 3)){
            Thread.sleep(1000);
            takeScreenshot(screenShotName1);
        }
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='I confirm I've read and fully understand the reminders on Responsible Borrowing.']"),
    })
    private MobileElement confirmFirstCircle;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[contains(@text,'I confirm I've (a) read the summary page')]"),
    })
    private MobileElement confirmSecondCircle;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Meet your plan']"),
    })
    private MobileElement  MeetYourPlan ;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Confirm']"),
    })
    private MobileElement  Confirm ;

    @AndroidFindAll({
            @AndroidBy(xpath ="//*[@text='Document upload']"),
    })
    private MobileElement  documentUploadPage ;

    @SneakyThrows
    public void goToLoanPage(String screenShotName) {
        Thread.sleep(5*1000);
        waitUntilElementVisible(loanBtn);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
        clickElement(loanBtn);
        Thread.sleep(2000);
    }

    public void clickCheckItNowBtn(String screenShotName1){
        waitUntilElementVisible(personalLoanTitle);
        if (screenShotName1!=null){
            takeScreenshot(screenShotName1);
        }
        clickElement(checkItNowBtn);
    }

    @SneakyThrows
    public void checkCongratulationPageBeforeUploadFiles(){
        if(isShow(checkItNowBtn,2)){
            clickElement(checkItNowBtn);
            Thread.sleep(2000);
            scrollUp();
            scrollUp();
            if(isShow(confirmBtn,2)){
                clickElement(confirmBtn);
            }
            if(isShow(next,2)){
                clickElement(next);
            }
        }

    }

    @SneakyThrows
    public void clickConfirmBtn(String screenShotName2, String screenShotName3){
        Thread.sleep(2*1000);
        if (screenShotName2!=null){
            takeScreenshot(screenShotName2);
        }
        scrollUpToFindElement(confirmBtn,10,30);
        if (screenShotName3!=null){
            takeScreenshot(screenShotName3);
        }
        clickElement(confirmBtn);
    }

    @SneakyThrows
    public void goToLoanPageAfterSubmitApplication(String screenShotName){
        Thread.sleep(5*1000);
        waitUntilElementVisible(loanBtn);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
        clickElement(loanBtn);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void clickApprovalLetterDetail(String screenShotName2, String screenShotName3){
        scrollUpToFindElement(byPressingText,10,30);
        if (screenShotName2!=null){
            takeScreenshot(screenShotName2);
        }
        clickElement(approvalLetterDetails);
        Thread.sleep(2*1000);
        if (screenShotName3!=null){
            takeScreenshot(screenShotName3);
        }
    }

    @SneakyThrows
    public void returnBtn(){
        Thread.sleep(1*1000);
        clickElement(returnBtn);
    }

    @SneakyThrows
    public void clickRepaymentSchedule(String screenShotName4){
        waitUntilElementVisible(repaymentSchedule);
        clickElement(repaymentSchedule);
        Thread.sleep(2*1000);
        if (screenShotName4!=null){
            takeScreenshot(screenShotName4);
        }
    }

    @SneakyThrows
    public void clickTermsAndConditions(String screenShotName5){
        waitUntilElementVisible(termsAndConditions);
        clickElement(termsAndConditions);
        Thread.sleep(2*1000);
        if (screenShotName5!=null){
            takeScreenshot(screenShotName5);
        }
    }

    @SneakyThrows
    public void clickKeyFactsStatements(String screenShotName6){
        waitUntilElementVisible(keyFactsStatements);
        clickElement(keyFactsStatements);
        Thread.sleep(2*1000);
        if (screenShotName6!=null){
            takeScreenshot(screenShotName6);
        }
    }

    public void iShareHkid() {
        String mobileType =  System.getProperty("mobile");
        GlobalVar.GLOBAL_VARIABLES.put("TEST_ACCOUNT_SPECIFIED", GlobalVar.GLOBAL_VARIABLES.get(mobileType+".userName"));
        GlobalVar.GLOBAL_VARIABLES.put("TEST_PASSWORD_SPECIFIED", GlobalVar.GLOBAL_VARIABLES.get(mobileType+".password"));
        GlobalVar.HEADERS.put("Authorization", "Bearer HoGwvptb2beNfWCWJ2Vds42jcZRJ");
        GlobalVar.HEADERS.put("Cookie", "cookiesession1=678A3E0E4ABCDEFGHIJKLMNOPQRS494C");
        GlobalVar.HEADERS.put("Content-Type","application/json;charset=utf-8");
        GlobalVar.HEADERS.put("Client-Id","AHNxFv0FE61r8zfv8kemGwL06LNxS6aM");
        GlobalVar.HEADERS.put("Client-Secret","fA52Bm6NkSGGPFPp");
        GlobalVar.HEADERS.put("Accept-Encoding","gzip,deflate,br");
        SignatureUtil signatureUtil = new SignatureUtil();
        signatureUtil.getPublicKeyReq();
        int offset = signatureUtil.getOffset();
        signatureUtil.createSignReq(offset);
        SendRequest SendRequest = new SendRequest();
        String url="/v1/loan-applications";
        String method="Get";
        try {
            response = SendRequest.sendRequest(url, method, "", "", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        String hkid = response.jsonPath().getString("data.id[0]");
        GlobalVar.GLOBAL_VARIABLES.put("hkid", hkid);
        System.out.println("hkid: "+hkid);
        ZephyrScaleUtils zephyrScaleUtils = new ZephyrScaleUtils();
        zephyrScaleUtils.storeLoansHkid();
        zephyrScaleUtils.updateForHkidInProperties();
    }



    @SneakyThrows
    public void getHKId(){
        waitUntilElementClickable(eyeBtn);
        clickElement(eyeBtn);
        waitUntilElementVisible(rocketButton);
        clickElement(rocketButton);
        waitUntilElementVisible(http);
        clickElement(http);
        Thread.sleep(3*1000);
        String getUrl = getElementText(getHttp);
        int indexOf = getUrl.indexOf("loan-applications/");
        String HKId = getUrl.substring(indexOf);
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(HKId);
        HkId =m.replaceAll("").trim();
    }

    @SneakyThrows
    public void clickDevPanel(){
        waitUntilElementVisible(devPanel);
        clickElement(devPanel);
        Thread.sleep(2*1000);
        clickElement(devPanel);
    }

    @SneakyThrows
    public void clickUploadBtn(String screenShotName14){
        Thread.sleep(2*1000);
        waitUntilElementVisible(uploadBtn);
        Thread.sleep(1000);
        if (screenShotName14!=null) {
            takeScreenshot(screenShotName14);
        }
        clickElement(uploadBtn);
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='Check it now']"),
    })
    private MobileElement checkItNow;

    public void checkUploadFileLastPage(String screenShotName){

    }



    public void clickIncomeProof(String screenShotName02){
        waitUntilElementVisible(documentUpload);
        if (screenShotName02!=null) {
            takeScreenshot(screenShotName02);
        }
        clickElement(IncomeProof);
    }

    public void clickAddPhoto(String screenShotName03){
        waitUntilElementVisible(sampleBtn);
        if (screenShotName03!=null) {
            takeScreenshot(screenShotName03);
        }
        clickElement(addPhotoBtn);
    }
    public void clickAddPhoto2(String screenShotName10){
        waitUntilElementVisible(sampleBtn);
        if (screenShotName10!=null) {
            takeScreenshot(screenShotName10);
        }
        clickElement(addPhotoBtn2);
    }
    public void clickAddPhoto3(String screenShotName){
        waitUntilElementVisible(addPhotoBtn3Page);
        if (screenShotName!=null) {
            takeScreenshot(screenShotName);
        }
        clickElement(addPhotoBtn3);
    }
    @SneakyThrows
    public void choosePhotoLibrary(String screenShotName04, String screenShotName05, String screenShotName06){
        waitUntilElementVisible(photoLibrary);
        if (screenShotName04!=null) {
            takeScreenshot(screenShotName04);
        }
        clickElement(photoLibrary);
        if (verifyElementExist(gotItBtn)){
            takeScreenshot(screenShotName05);
            clickElement(gotItBtn);
        }
        if(isShow(allowButton,2)){
            clickElement(allowButton);
        }
        Thread.sleep(1000);
        if (screenShotName06!=null) {
            takeScreenshot(screenShotName06);
        }
        //clickElement(firstPhoto);
        //clickByLocationByPercent(20,35);
        //clickByLocationByPercent(15,52);// for xiaomi A3
//        Thread.sleep(2000);
//        if (GlobalVar.IS_USE_OPENCV) {
//            clickByPicture2("src/main/resources/images/loans/incomeProof.jpg",50,50);
//        } else {
//            clickByLocationByPercent(374, 740);
//        }
    }

    @SneakyThrows
    public void choosePhotoLibrary1(String screenShotName, String screenShotName1, String screenShotName2){
        choosePhotoLibrary(screenShotName, screenShotName1, screenShotName2);
        //click this picutre will go to upload page
        clickByLocationByPercent(15,52);
        Thread.sleep(8000);
    }
    @SneakyThrows
    public void choosePhotoLibrary2(String screenShotName, String screenShotName1, String screenShotName2){
        choosePhotoLibrary(screenShotName, screenShotName1, screenShotName2);
        //click this picutre will go to upload page
        clickByLocationByPercent(15,52);// for xiaomi A3
        Thread.sleep(8000);
    }

    @SneakyThrows
    public void selectTakePhoto(String screenShotName4, String screenShotName5){
        waitUntilElementVisible(takePhoto);
        if (screenShotName4!=null){
            takeScreenshot(screenShotName4);
        }
        clickElement(takePhoto);
        if (verifyElementExist(gotItBtn)){
            takeScreenshot(screenShotName5);
            clickElement(gotItBtn);
        }
        clickElement(Photograph);
        Thread.sleep(2*1000);
        clickElement(rightSign);
        Thread.sleep(4*1000);
    }

    public void selectNoThanksBtn(String screenShotName08){
        waitUntilElementVisible(noThanksBtn);
        if (screenShotName08!=null){
            takeScreenshot(screenShotName08);
        }
        clickElement(noThanksBtn);
    }

    public void clickResidentialAddressProof(String screenShotName09){
        waitUntilElementVisible(residentialAddressProof);
        if (screenShotName09!=null){
            takeScreenshot(screenShotName09);
        }
        clickElement(residentialAddressProof);
    }

    public void clickCreditCardPersonalLoanProof(String screenShotName){
        waitUntilElementVisible(creditCardPersonalLoanProof);
        if (screenShotName!=null){
            takeScreenshot(screenShotName);
        }
        clickElement(creditCardPersonalLoanProof);
    }

    @SneakyThrows
    public void clickDoneBtn(String screenShotName, String screenShotName15){
        waitUntilElementVisible(uploadBtn);
        takeScreenshot(screenShotName);
        clickElement(uploadBtn);
        Thread.sleep(5000);
        waitUntilElementVisible(uploadOkPagePreparing);
        if (screenShotName15!=null){
            takeScreenshot(screenShotName15);
        }
    }

    @SneakyThrows
    public void clicked(Map<String,String>data){
        Thread.sleep(1000);
        clickElement(applyButton);
        Thread.sleep(5000);
    }
    @SneakyThrows
    public void modifyButton(Map<String,String>data){
        Thread.sleep(1000);
        clickElement(modify);
        Thread.sleep(1000);
        clearAndSendKeys(input,data.get("money"));
        Thread.sleep(1000);
        hideAndroidKeyboard();
        Thread.sleep(5000);
        clickElement(purposeOfLoan);
        selectInAboutYourJob();
        Thread.sleep(1000);
    }
    public void hideAndroidKeyboard(){
        try {
            if (System.getProperty("mobile").contains("android")) hideKeyboard();
        }catch (Exception e){
        }
    }
    @SneakyThrows
    public void clickApplyNow(Map<String,String>data){
        Thread.sleep(1000);
        clickElement(applyNow);
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName"));

    }
    @SneakyThrows
    public void clickconfirm(Map<String,String>data){
        readKeyFactsStatementsPageAndAgree(data.get("screenShotName"));
//        scrollUp();
        takeScreenshot(data.get("screenShotName2"));
        scrollUpToFindElement(submitApplication,3,2);
        Thread.sleep(2000);
        clickElement(circle);
        Thread.sleep(2000);
        takeScreenshot(data.get("screenShotName3"));
        clickElement(submitApplication);
        Thread.sleep(5000);
        takeScreenshot(data.get("screenShotName4"));
    }

    @SneakyThrows
    public void clickByPressingBtn(String screenShotName7){
        scrollUp();
        Thread.sleep(2000);
        clickElement(byPressingBtn);
        Thread.sleep(1*1000);
        if (screenShotName7!=null){
            takeScreenshot(screenShotName7);
        }
    }

    @SneakyThrows
    public void clickAgreeAndDrawdownBtn(String screenShotName8){
        waitUntilElementVisible(agreeAnDrawdownBtn);
        clickElement(agreeAnDrawdownBtn);
        Thread.sleep(1*1000);
        if (screenShotName8!=null){
            takeScreenshot(screenShotName8);
        }
        Thread.sleep(1*1000);
        sendMsk();
        Thread.sleep(1000);
        if(isShow(workingOnYourloanAgreementPage,5)){
            takeScreenshot(screenShotName8);
            clickElement(gotItBtn);
        }
    }

    @SneakyThrows
    public void checkMyLoans(String screenShotName){
        waitUntilElementVisible(myLoanItem);
        clickElement(myLoanItem);
        Thread.sleep(3000);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
    }

    @SneakyThrows
    public void openMyLoan(){
        waitUntilElementVisible(myLoanItem);
        clickElement(myLoanItem);
        Thread.sleep(1*1000);
        if(isShow(CancelButton,5)){
            clickElement(CancelButton);
        }
    }

    @SneakyThrows
    public void checkMyLoanWithDeliquent(String screenShotName, String screenShotName1){
        waitUntilElementVisible(TotalOutstanding);
        takeScreenshot(screenShotName);
        String Amount = loanAmount.getText();
        System.out.println("Amount: " + Amount);
        String repaymentAmount = firstRepaymentAmount.getText();
        clickElement(repayingTabOPenButton);
        checkRepaymentAmount(screenShotName1, repaymentAmount);
    }

    @SneakyThrows
    public void checkSecondRepaymentAmount(String screenShotName, String screenShotName1){
        waitUntilElementVisible(TotalOutstanding);
        takeScreenshot(screenShotName);
        String totalAmount = loanAmount.getText();
        logger.info("totalAmount: "+totalAmount);
        String repaymentAmount = secondRepaymentAmount.getText();
        clickElement(secondRepaymentAmountOPenButton);
        checkRepaymentAmount(screenShotName1, repaymentAmount);
    }

    private void checkRepaymentAmount(String screenShotName1, String repaymentAmount) throws InterruptedException {
        Thread.sleep(2000);
        takeScreenshot(screenShotName1);
        String monthlyRepaymentAmount = monthlyRepayment.getText();
        String lateChargetAmount = lateCharge.getText();
        logger.info("repaymentAmount: "+repaymentAmount);
        logger.info("monthlyRepaymentAmount: "+monthlyRepaymentAmount);
        logger.info("lateChargetAmount: "+lateChargetAmount);
        assertThat(checkAmount(repaymentAmount, monthlyRepaymentAmount, lateChargetAmount)).isTrue();
    }

    public boolean checkAmount(String total, String item1, String item2){
        BigDecimal b1 = plus_num(amountStringFormat(item1), amountStringFormat(item2));
        BigDecimal b2 = new BigDecimal(amountStringFormat(total));
        if(b1.equals(b2)) return true;
        return false;
    }

    public BigDecimal plus_num(String a, String b) {
        BigDecimal b1 = new BigDecimal(a);
        BigDecimal b2 = new BigDecimal(b);
        return b1.add(b2);
    }

    public String amountStringFormat(String s){
        s = s.replace("HKD", "");
        return s.replace(",", "").trim();
    }

    @SneakyThrows
    public void inputPromotionCode(Map<String,String>data){
        scrollUpToFindElement(PromotionCodeMessage,3,3);
        //clickElement(PromotionCode);
        //Thread.sleep(1000);
        //clearAndSendKeys(PromotionCodeInput,data.get("promotionCode"));
        Thread.sleep(1000);
    }

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Purpose of loan')]/.."),
            @AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Purpose of loan')]/../../android.view.ViewGroup[2]"),
    })
    private MobileElement rfPurpose;

    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='How much do you want to borrow?']/../android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ImageView"),
    })
    private MobileElement loanRfAmountEdit;
    @AndroidFindAll({
            @AndroidBy(xpath = "//*[@text='How much do you want to borrow?']/../android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.EditText"),
    })
    private MobileElement loanRfAmountInput;

    @SneakyThrows
    public void setRfTenorAndPurpose(String amount, String tenor,String screenShotName){
        clickElement(loanRfAmountEdit);
        clearAndSendKeys(loanRfAmountInput,amount);
        inputEnter(loanRfAmountInput);
        takeScreenshot(screenShotName);
        Thread.sleep(2000);
        //setTenor(tenor);
        clickElement(rfPurpose);
        Thread.sleep(1000);
        clickElement(purposeOfLoanOption);
        takeScreenshot(screenShotName);
    }
}
