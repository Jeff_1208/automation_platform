Feature: Loans Application Deliquent

  Background: Login
    Given Open loans WeLab App
    And skip Root ios

  @VBL-T2991
  Scenario: Reg_Loan_Deliquent_001 Loan 逾期 -deliquent -本金和逾期费没还
    When Login with user and password
      | user     | stress24028  |
      | password | abcD$123456  |
    Then I get loans page
      | screenShotName | Reg_Loan_Deliquent_001_IOS_01 |
    Then open my loans ios
      | screenShotName | Reg_Loan_Deliquent_001_IOS_02 |
    Then check My Loans Second Repayment With Deliquent IOS
      | screenShotName | Reg_Loan_Deliquent_001_IOS_03 |

  @VBL-T2992
  Scenario: Reg_Loan_Deliquent_002 Loan 逾期  -deliquent  -本金还了，逾期费没还
    When Login with user and password
      | user     | stress24028  |
      | password | abcD$123456  |
    Then I get loans page
      | screenShotName | Reg_Loan_Deliquent_002_IOS_01 |
    Then open my loans ios
      | screenShotName | Reg_Loan_Deliquent_002_IOS_02 |
    Then check My Loans With Deliquent IOS
      | screenShotName | Reg_Loan_Deliquent_002_IOS_03 |

  @VBL-T2993
  Scenario: Reg_Loan_Deliquent_003 Loan 逾期  -deliquent  -本金没还，逾期费还了
    When Login with user and password
      | user     | stress27170  |
      | password | abcD$123456  |
    Then I get loans page
      | screenShotName | Reg_Loan_Deliquent_003_IOS_01 |
    Then open my loans ios
      | screenShotName | Reg_Loan_Deliquent_003_IOS_02 |
    Then check My Loans With Deliquent IOS
      | screenShotName | Reg_Loan_Deliquent_003_IOS_03 |

  @VBL-T2994
  Scenario: Reg_Loan_Deliquent_004 Loan 逾期  - nab  -本金还部分
    When Login with user and password
      | user     | stress24046  |
      | password | abcD$123456  |
    Then I get loans page
      | screenShotName | Reg_Loan_Deliquent_004_IOS_01 |
    Then open my loans ios
      | screenShotName | Reg_Loan_Deliquent_004_IOS_02 |
    Then check My Loans With Deliquent IOS
      | screenShotName | Reg_Loan_Deliquent_004_IOS_03 |
