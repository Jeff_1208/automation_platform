package com.welab.automation.projects.loans.pages.sake2;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Sake2LoginPage extends WebBasePage {
  private String pageName = "Sake2 login page";
  public Sake2LoginPage() {
    super.pageName = this.pageName;
  }

  @FindBy(how = How.ID, using = "username")
  private WebElement usernameEditBox;

  @FindBy(how = How.ID, using = "password")
  private WebElement passwordEditBox;

  @FindBy(how = How.XPATH, using = "//*[@id=\"navbarSupportedContent\"]/ul/li")
  private WebElement loginTopBtn;

  @FindBy(how = How.ID, using = "kc-login")
  private WebElement loginBtn;

  @SneakyThrows
  public void loginSake2(String username, String password) {
    waitUntilElementVisible(loginTopBtn);
    Thread.sleep(2000);
    clickElement(loginTopBtn);
    clearAndSendKeys(usernameEditBox, username);
    clearAndSendKeys(passwordEditBox, password);
    clickElement(loginBtn);
  }

  public void closeWebDriver(){
    if(driver!=null) {
      try{
        driver.close();
      }catch(Exception ex) {
        ex.printStackTrace();
      }
    }
  }

}
