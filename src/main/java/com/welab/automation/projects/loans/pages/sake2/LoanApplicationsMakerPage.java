package com.welab.automation.projects.loans.pages.sake2;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class LoanApplicationsMakerPage extends WebBasePage {
    String HKID ;
    @FindBy(how = How.XPATH, using = "//*[@id=\"navbarSupportedContent\"]/ul/li/div/button")
    private WebElement logOut;

    @FindBy(how = How.XPATH, using = "//*[@id=\"navbarDropdown\"]")
    private WebElement navbarDropdown;

    @FindBy(how = How.XPATH, using = "//*[@id=\"navbarSupportedContent\"]/ul/li")
    private WebElement login;

    @FindBy(how = How.XPATH, using = "//*[@id=\"username\"]")
    private WebElement user;

    @FindBy(how = How.XPATH, using = "//*[@id=\"password\"]")
    private WebElement pwd;

    @FindBy(how = How.XPATH, using = "//*[@id=\"kc-login\"]")
    private WebElement kcLogin;

    @FindBy(how = How.XPATH, using = "//*[@id=\"search\"]")
    private WebElement inputSearch;

    @FindBy(how = How.XPATH, using = "//button[text()='Search all Loans']")
    private WebElement search;

    @FindBy(how = How.XPATH, using = "//*[@class=\"btn btn-light btn-sm mr-1 btn-outline btn btn-secondary\"][1]")
    private WebElement updateAssign;

    @FindBy(how = How.XPATH, using = "//*[@name=\"assignee\"]")
    private WebElement assignee;

    @FindBy(how = How.XPATH, using = "//*[@name=\"assignee\"]/option")
    private List<WebElement> assigneeOption;

    @FindBy(how = How.XPATH, using = "//button[text()='Update']")
    private WebElement btnSuccess;

    @FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
    private WebElement btnCancel;

    @FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[12]/span")
    private WebElement afterChange;

    @FindBy(how = How.XPATH, using = "//*[@class=\"detail\"]")
    private WebElement detail;

    @FindBy(how = How.XPATH, using = "//*[@class=\"table table-hover undefined\"]/tbody/tr")
    private List<WebElement> tableHover;

    @FindBy(how = How.XPATH, using = "//*[@aria-label=\"Go to next page\"]")
    private WebElement nextPage;

    @FindBy(how = How.XPATH, using = "//*[@class=\"pagination\"]/li[7]")
    private WebElement pageNumber;

    @FindBy(how = How.XPATH, using = "//*[@class=\"page-link undefined\"]")
    private WebElement pagelink;

    @FindBy(how = How.XPATH, using = "//*[@name=\"incomeProofVerificationStatus\"]")
    private List<WebElement> makerPass;

    @FindBy(how = How.XPATH, using = "//*[@name=\"status\"]")
    private WebElement status;

    @FindBy(how = How.XPATH, using = "//strong[text() = 'Status:']/../div/b/span")
    private WebElement statusContent;

    @FindBy(how = How.XPATH, using = "//*[@class=\"css-19bqh2r\"]")
    private WebElement Reasonbut;

    @FindBy(how = How.XPATH, using = "//*[@class=\" css-tlfecz-indicatorContainer\"][1]")
    private WebElement Reasonclear;

    @FindBy(how = How.XPATH, using = "//*[@class=\" css-1wa3eu0-placeholder\"]")
    private WebElement Select;

    @FindBy(how = How.XPATH, using = "//div[@data-testid='loanInfo']//div[text() = 'Quick Edit']")
    private WebElement quickEdit;

    @FindBy(how = How.XPATH, using = "//*[text()='A1. Approved']")
    private WebElement Approved;

    @FindBy(how = How.XPATH, using = "//div[@data-testid='residentialInfo']//label[text() = 'Pass']/input")
    private WebElement residentialInfoPass;

    @FindBy(how = How.XPATH, using = "//div[@data-testid='workInfo']//label[text() = 'Pass']/input")
    private WebElement workInfoPass;

    @FindBy(how = How.XPATH, using = "//div[@data-testid='pubicData']//label[text() = 'Pass']/input")
    private WebElement pubicDataPass;

    @FindBy(how = How.XPATH, using = "//label[text()='Pass']")
    private List<WebElement> Passbut;

    @FindBy(how = How.XPATH, using = "//*[@class=\"errors col-md-12 text-danger\"]")
    private List<WebElement> esg;

    @FindBy(how = How.XPATH, using = "//*[@name=\"tuMobileCheckingResult\"]")
    private WebElement TU;

    //@FindBy(how = How.XPATH, using = "//button[text()='Save']")
    @FindBy(how = How.XPATH, using = "//div[text()='Personal Info']/..//button[text()='Save']")
    private WebElement Save;


    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/nav/div[1]/span")
    private WebElement orderId;

    @FindBy(how = How.XPATH, using = "//li[@class=\"list-group-item\"]//input[@class=\"form-check-input\"]")
    private WebElement Valid;

    @FindBy(how = How.XPATH, using = "//li[@class=\"list-group-item\"]//input[@class=\"form-check-input\"]")
    private List<WebElement> ValidList;


    @FindBy(how = How.XPATH, using = "//*[@name=\"requestedAmount\"]")
    private WebElement loanAmount;

    @FindBy(how = How.XPATH, using = "//*[@class=\"errors col-md-12 text-danger\"]")
    private WebElement errors;


    @SneakyThrows
    public void clickLogin() {
        clickElement(navbarDropdown);
        Thread.sleep(2000);
        clickElement(logOut);

    }

    @SneakyThrows
    public void clickOrderDetail() {
        clickElement(detail);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public void login(String username,String password) {
        Thread.sleep(1000);
        clickElement(login);
        Thread.sleep(1000);
        andSendKeys(user,username);
        Thread.sleep(1000);
        andSendKeys(pwd,password);
        Thread.sleep(1000);
        clickElement(kcLogin);
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void searchButton() {
        Thread.sleep(1000);
        clickElement(inputSearch);
        andSendKeys(inputSearch,GlobalVar.GLOBAL_VARIABLES.get("orderId"));
        clickElement(search);
        Thread.sleep(1000);
    }


    @SneakyThrows
    public boolean clickUpdateAssign(String selectValue) {
        clickElement(updateAssign);
        Thread.sleep(1000);
        Select select = new Select(driver.findElement(By.name("assignee")));
        Thread.sleep(1000);
        select.selectByIndex(1);
        Thread.sleep(1000);
        select.selectByVisibleText(selectValue);
        clickElement(btnSuccess);

        Thread.sleep(3000);
        boolean str=false;
        if (selectValue.contains(afterChange.getText())){
            str=true;
        }
        return str;

    }

    @SneakyThrows
    public void HKid(String HKid,String screenShotName) {
        clickElement(detail);
        Thread.sleep(1000);
        try {
            String winHandleBefore = driver.getWindowHandle();
            Set<String> winHandles = driver.getWindowHandles();
            Iterator<String> it = winHandles.iterator();
            while (it.hasNext()) {
                String win = it.next();
                if (!win.equals(winHandleBefore)) {
                    driver.switchTo().window(win);
                    logger.info("Switch Window From " + winHandleBefore + " to " + win);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String str=driver.getCurrentUrl();
        int index=str.indexOf("/loan_");
        String substring= String.valueOf(str.subSequence(0,index));
        str=substring+"/loan_application/"+HKid+"/overview";
        driver.get(str);
        Thread.sleep(2000);
        HKID=getElementText(orderId);
        System.out.println("HKid: "+HKid);
        System.out.println("orderId: "+HKID);
        if(HKID.length()<5){
            assertThat(false);
        }
        GlobalVar.GLOBAL_VARIABLES.put("orderId",HKID);
        takeScreenshot(screenShotName);
    }

    @SneakyThrows
    public void teraversalQuery(){
        String name=getElementText(navbarDropdown);
        for (int i=0;i<100;i++) {
            for (int a=1;a<=tableHover.size();a++) {
              String SubmittedAt=driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[3]")).getText();
                HKID=driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[1]/span")).getText();
                String LastUpdate=driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[3]")).getText();
                String Status=driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[10]")).getText();
                String CurrentAssignee=driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[12]")).getText();
                if (!SubmittedAt.equals("")){
                    if (Status.equals("Cancelled")&&name.contains(CurrentAssignee)){
                        logger.info("Use the "+HKID+" of the");
                        GlobalVar.GLOBAL_VARIABLES.put("orderId",HKID);
                        driver.findElement(By.xpath("//*[@class=\"table table-hover undefined\"]/tbody/tr["+a+"]/td[11]/a")).click();
                        Set<String> winHandels=driver.getWindowHandles();
                        List<String> it = new ArrayList<String>(winHandels);
                        driver.switchTo().window(it.get(1));
                        return;
                    }
                }
            }
            ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
            String str=pageNumber.getText();
            if (str.equals(pagelink.getText())){
                return;
            }
            if (!str.equals(pagelink.getText())){
                clickElement(nextPage);
                Thread.sleep(1000);
            }
        }
    }

    @SneakyThrows
    public void approved(Map<String,String> data){
        screenshot(data.get("screenShotName"));
        Thread.sleep(1000);
        scrollToTop(pageName);
        Thread.sleep(1000);
        status(data.get("status"));
        Reason();
        clickElement(btnSuccess);
        Thread.sleep(3000);
        screenshot(data.get("screenShotName1"));
    }
    @SneakyThrows
    public void status(String str){
        scrollToElement(quickEdit);
        Thread.sleep(1000);
        driver.executeScript("arguments[0].click();",quickEdit);
        Thread.sleep(1000);
        selectByVisibleText(status,str);
        Thread.sleep(1000);
//        clearAndSendKeys(loanAmount,"100000");
    }
    @SneakyThrows
    public void Reason(){
        Thread.sleep(1000);
        //scrollToElement(Save);  // add for scroll to select reason
        clickElement(Reasonclear);
        Thread.sleep(1000);
        clickElement(Reasonbut);
        Thread.sleep(1000);
        clickElement(Approved);
        Thread.sleep(1000);
        Select select = new Select(driver.findElement(By.name("requestedTenorPeriod")));
        select.selectByIndex(1);
        select.selectByVisibleText("60 months");
        Thread.sleep(1000);
    }

    private boolean isAttribtuePresent(WebElement element, String attribute) {
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null){
                result = true;
            }
        } catch (Exception e) {}

        return result;
    }

    @SneakyThrows
    public void makerOverview1(){
        for (WebElement element: Passbut){
            Thread.sleep(1000);
            scrollToElement(element);
            Thread.sleep(1000);
            if (isAttribtuePresent(element,"checked")==false) {
                driver.executeScript("arguments[0].click();", element);
                Thread.sleep(1000);
            }
        }
        Valid();
    }
    @SneakyThrows
    public void makerOverview(){
        driver.executeScript("window.scrollTo(0, 400)");
        Thread.sleep(1000);
        clickElement(residentialInfoPass);
        Thread.sleep(5000);
        driver.executeScript("window.scrollTo(400, 800)");
        Thread.sleep(1000);
        clickElement(workInfoPass);
        Thread.sleep(5000);
        driver.executeScript("window.scrollTo(800, 1200)");
        Thread.sleep(1000);
        clickElement(pubicDataPass);
        Thread.sleep(5000);
        Valid();
    }

    @SneakyThrows
    public void checker(Map<String,String> data){
        screenshot(data.get("screenShotName"));
        status("Pending Checker");
        clickElement(btnSuccess);
        Thread.sleep(5000);
        screenshot(data.get("screenShotName1"));
        assertThat(checkStatus("Pending Checker")).isTrue();
        try {
            String winHandleBefore = driver.getWindowHandle();
            Set<String> winHandles = driver.getWindowHandles();
            driver.close();
            Iterator<String> it = winHandles.iterator();
            while (it.hasNext()) {
                String win = it.next();
                if (!win.equals(winHandleBefore)) {
                    driver.switchTo().window(win);
                    logger.info("Switch Window From " + winHandleBefore + " to " + win);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @SneakyThrows
    public void TU(){
        Thread.sleep(5000);
        if (ElementExist(By.xpath("//button[text()='Save']"))){
            scrollToElement(Save);
            logger.info("Find the element" + ElementExist(By.xpath("//button[text()='Save']")) + " to ");
            Select select = new Select(driver.findElement(By.name("tuMobileCheckingResult")));
            Thread.sleep(2000);
            select.selectByIndex(1);
            Thread.sleep(2000);
            select.selectByVisibleText("Pass");
            Thread.sleep(2000);
            driver.executeScript("arguments[0].click();",Save);
        }
        Thread.sleep(6*1000); //wait page fresh
    }

    @SneakyThrows
    public void Valid(){
        Thread.sleep(1000);
        scrollToElement(Valid);
        if (ElementExist(By.xpath("//li[@class=\"list-group-item\"]//input[@class=\"form-check-input\"]"))){
            int i=0;
            for (WebElement element:ValidList){
                if (i%2==0){
                    driver.executeScript("arguments[0].click();",element);
                }
                i++;
            }
            Thread.sleep(1000);
        }
    }
    public boolean ElementExist(By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @SneakyThrows
    public void SetTuToPass(){
        Select select = new Select(driver.findElement(By.name("tuMobileCheckingResult")));
        Thread.sleep(2000);
        select.selectByIndex(1);
        Thread.sleep(2000);
        select.selectByVisibleText("Pass");
        Thread.sleep(2000);
        //driver.executeScript("arguments[0].click();",Save);
        clickElement(Save);
        Thread.sleep(3000);  //Wait page for refresh
    }

    @SneakyThrows
    public void setInfomationPassAndValid(Map<String,String> data){
        logger.info("Set Infomation PASS and Proof Valid!!!!!!!!!!!!");
        scrollToTop(pageName);
        Thread.sleep(1000);
        driver.executeScript("window.scrollTo(0, 400)");
        driver.executeScript("window.scrollTo(400, 800)");
        Thread.sleep(1000);
        clickElement(residentialInfoPass);
        Thread.sleep(1000);
        takeScreenshot(data.get("screenShotName"));

        driver.executeScript("window.scrollTo(800, 1200)");
        Thread.sleep(1000);
        clickElement(workInfoPass);
        Thread.sleep(1000);
        takeScreenshot(data.get("screenShotName1"));

        driver.executeScript("window.scrollTo(1200, 1600)");
        Thread.sleep(1000);
        clickElement(pubicDataPass);
        Thread.sleep(1000);
        takeScreenshot(data.get("screenShotName2"));

        driver.executeScript("window.scrollTo(1600, 2000)");
        Thread.sleep(1000);

        if (ElementExist(By.xpath("//li[@class=\"list-group-item\"]//input[@class=\"form-check-input\"]"))){
            int i=0;
            for (WebElement element:ValidList){
                if (i%2==0){
                    driver.executeScript("arguments[0].click();",element);
                }
                i++;
            }
            Thread.sleep(1000);
        }

        takeScreenshot(data.get("screenShotName3"));
        Thread.sleep(1000);
    }

    @SneakyThrows
    public void makerApproved(Map<String,String> data){

        setInfomationPassAndValid(data);

        logger.info("start to set TU to PASS!!!!!!!!!!!!");
        scrollToTop(pageName);
        Thread.sleep(1000);
        driver.executeScript("window.scrollTo(0, 400)");
        SetTuToPass();
        takeScreenshot(data.get("screenShotName4"));
        Thread.sleep(1000);

        logger.info("start to checker!!!!!!!!!!!!");

        setStatusAndReason();

        if(!checkStatus("Pending Checker")){
            clickElement(btnCancel);
            Thread.sleep(1000);
            SetTuToPass();
            setStatusAndReason();
        }

        assertThat(checkStatus("Pending Checker")).isTrue();
        Thread.sleep(3000);
        screenshot(data.get("screenShotName5"));

        try {
            String winHandleBefore = driver.getWindowHandle();
            Set<String> winHandles = driver.getWindowHandles();
            driver.close();
            Iterator<String> it = winHandles.iterator();
            while (it.hasNext()) {
                String win = it.next();
                if (!win.equals(winHandleBefore)) {
                    driver.switchTo().window(win);
                    logger.info("Switch Window From " + winHandleBefore + " to " + win);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SneakyThrows
    public void setStatusAndReason(){
        status("Pending Checker");

        //set approved
        clickElement(Reasonclear);
        Thread.sleep(1000);
        clickElement(Reasonbut);
        Thread.sleep(1000);
        clickElement(Approved);
        Thread.sleep(1000);

        clickElement(btnSuccess);
        Thread.sleep(5000);
    }

    @SneakyThrows
    public boolean checkStatus(String exceptStatus){
        try {
            Thread.sleep(1000);
            String status = getElementText(statusContent);
            if(status.contains(exceptStatus)){
                logger.info("status as same as the exceptStatus!!!!!!!!!!");
                return true;
            }else{
                logger.info("The status is: "+status+"!!!!!! not match the exceptStatus!!!!!!!!!!");
                return false;
            }
        }catch (Exception e){
            return false;
        }

    }

}
