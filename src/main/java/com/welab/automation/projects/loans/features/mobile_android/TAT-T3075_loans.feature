Feature: Loans Application

  Background: Login
    Given Open loans WeLab App
    And Enable skip Root Loans
    #And close web driver
    When Login with user and password from properties
    Then check Unexpected Element

  @loans @VBL-T3106
  Scenario: Reg_Loan_001 Loan- Landing page and apply loan
    Then I goto loansPage
      | screenShotName               | Reg_Loan_001_Android_01 |
    And I goto Personal Instalmen Page
      | screenShotName               | Reg_Loan_001_Android_02 |
    Then I set Loan Amount and Tenor
      | amount                       | 300000                  |
      | tenor                        | 36                      |
      | screenShotName               | Reg_Loan_001_Android_03 |
    Then I can see Key Facts Statements Page
    And I read Key Facts Statements Page and agree
      | screenShotName               | Reg_Loan_001_Android_04 |
    Then I can see About your job Page
      | screenShotName               | Reg_Loan_001_Android_05 |
    And I fill in the info in the About your job Page
      | screenShotName               | Reg_Loan_001_Android_06 |
      | companyName                  |   Welab                 |
      | jobTitleName                 |   qaTester              |
      | screenShotName1              | Reg_Loan_001_Android_07 |
      | monthlyIncomeAmount          |   50000                 |
      | Street                       |   Mystreet              |
      | Building                     |   sampleRisk08          |
      | Floor                        |   12                    |
      | Room                         |   06                    |
      | OfficePhoneNumber            |   23456666             |
      | screenShotName2              | Reg_Loan_001_Android_08 |
      | screenShotName3              | Reg_Loan_001_Android_09 |
    Then I goto Konw more about you Page
      | screenShotName               | Reg_Loan_001_Android_10 |
    Then I fill in the info in the page Know more about your
      | screenShotName               | Reg_Loan_001_Android_11 |
      | monthlyMortgageInstalment    |   12345                 |
      | homePhoneNumber              |   23455555           |
      | screenShotName1              | Reg_Loan_001_Android_12 |
    Then input promotion code
      | promotionCode                | 123456                  |
    And I goto Review and confirm Page
      | screenShotName               | Reg_Loan_001_Android_13 |
    Then change the info in the page Review and confirm
      | screenShotName               | Reg_Loan_001_Android_14 |
      | screenShotName1              | Reg_Loan_001_Android_15 |
    And I goto The Plan page
      | screenShotName               | Reg_Loan_001_Android_16 |
      | screenShotName1              | Reg_Loan_001_Android_17 |
    Then I share hkid

  @loans @VBL-T3107  @PIL1
  Scenario: Reg_Loan_002 upload document-income proof
    Then I goto loansPage2
      | screenShotName   | Reg_Loan_002_Android_01 |
#    And I get HKId
#    And I click Dev Panel
    And check Congratulation Page Before Upload Files
    And I click upload button
      | screenShotName01 | Reg_Loan_002_Android_02 |
    And I select income proof
      | screenShotName02 | Reg_Loan_002_Android_03 |
    And I click add photo
      | screenShotName03 | Reg_Loan_002_Android_04 |
    And I select photo library1
#    And I select take photo
      | screenShotName04 | Reg_Loan_002_Android_05 |
      | screenShotName05 | Reg_Loan_002_Android_06 |
      | screenShotName06 | Reg_Loan_002_Android_07 |
    And I click upload button
      | screenShotName07 | Reg_Loan_002_Android_08 |
    And I select no thanks
      | screenShotName08 | Reg_Loan_002_Android_09 |
    And I select residential address proof
      | screenShotName09 | Reg_Loan_002_Android_10 |
    And I click add photo2
      | screenShotName10 | Reg_Loan_002_Android_11 |
    And I select photo library2
#    And I select take photo
      | screenShotName04 | Reg_Loan_002_Android_12 |
      | screenShotName05 | Reg_Loan_002_Android_13 |
      | screenShotName06 | Reg_Loan_002_Android_14 |
    And I click upload button
      | screenShotName01 | Reg_Loan_002_Android_15 |
    And I click done button
      | screenShotName | Reg_Loan_002_Android_16 |
      | screenShotName01 | Reg_Loan_002_Android_17 |

  Scenario: Reg_Loan_005 select offer
    Then I go to loans Page after submit application
      | screenShotName  | Reg_Loan_005_Android_01 |
    And I click check it now button
      | screenShotName1 | Reg_Loan_005_Android_02 |
    And I click confirm button in the plan page
      | screenShotName2 | Reg_Loan_005_Android_03 |
      | screenShotName3 | Reg_Loan_005_Android_04 |

  Scenario: Reg_Loan_004 agreement generate successfully
    Then I go to loans Page after submit application
      | screenShotName  | Reg_Loan_004_Android_01 |
    And I click check it now button
      | screenShotName1 | Reg_Loan_004_Android_02 |
    And I click approval letter details
      | screenShotName2 | Reg_Loan_004_Android_03 |
      | screenShotName3 | Reg_Loan_004_Android_04 |
    And I return to the approval letter page
    And I click repayment schedule
      | screenShotName4 | Reg_Loan_004_Android_05 |
    And I return to the approval letter page
    And I click terms and conditions
      | screenShotName5 | Reg_Loan_004_Android_06 |
    And I return to the approval letter page
    And I click key facts statements
      | screenShotName6 | Reg_Loan_004_Android_07 |
    And I return to the approval letter page
    And I click by pressing button
      | screenShotName7 | Reg_Loan_004_Android_08 |
    And I click agree and drawdown button
      | screenShotName8 | Reg_Loan_004_Android_09 |

  Scenario: Reg_Loan_001 Loan- Landing page and apply loan
    Then I go loansPage
      | screenShotName               | Reg_Loan_001_Android_1 |
    Then I clicked the Apply button
      | screenShotName               | Reg_Loan_001_Android_2 |
    Then Enter amount and select purpose
      | money               | 50000 |
    Then Click the Apply button
      | screenShotName               | Reg_Loan_001_Android_3 |
    Then Click the confirm button
      | screenShotName               | Reg_Loan_001_Android_4 |
      | screenShotName2              | Reg_Loan_001_Android_5 |
      | screenShotName3              | Reg_Loan_001_Android_6 |
      | screenShotName4              | Reg_Loan_001_Android_7 |


  @DC  @DC1
  Scenario: Reg_Loan_0011 Loans DC Happy Flow
    Then I goto loansPage
      | screenShotName               | Reg_Loan_0011_Android_01 |
    And I goto DC Page
      | screenShotName               | Reg_Loan_0011_Android_02 |
    Then I set Loan Amount and Tenor
      | amount                       | 300000                  |
      | tenor                        | 36                      |
      | screenShotName               | Reg_Loan_0011_Android_03 |
    Then I can see Key Facts Statements Page
    And I read Key Facts Statements Page and agree
      | screenShotName               | Reg_Loan_0011_Android_04 |
    Then I can see About your job Page
      | screenShotName               | Reg_Loan_0011_Android_05 |
    And I fill in the info in the About your job Page
      | screenShotName               | Reg_Loan_0011_Android_06 |
      | companyName                  |   Welab                 |
      | jobTitleName                 |   qaTester              |
      | screenShotName1              | Reg_Loan_0011_Android_07 |
      | monthlyIncomeAmount          |   50000                 |
      | Street                       |   Mystreet              |
      | Building                     |   sampleRisk08          |
      | Floor                        |   12                    |
      | Room                         |   06                    |
      | OfficePhoneNumber            |   23456666             |
      | screenShotName2              | Reg_Loan_0011_Android_08 |
      | screenShotName3              | Reg_Loan_0011_Android_09 |
    Then I goto Konw more about you Page
      | screenShotName               | Reg_Loan_0011_Android_10 |
    Then fill in the info in the page Know more about your for DC flow
      | screenShotName               | Reg_Loan_0011_Android_11 |
      | monthlyMortgageInstalment    |   12345                 |
      | homePhoneNumber              |   23455555           |
      | screenShotName1              | Reg_Loan_0011_Android_12 |
    Then input promotion code
      | promotionCode                | 123456                  |
    And I goto Review and confirm Page
      | screenShotName               | Reg_Loan_0011_Android_13 |
    Then change the info in the page Review and confirm
      | screenShotName               | Reg_Loan_0011_Android_14 |
      | screenShotName1              | Reg_Loan_0011_Android_15 |
    And I goto The Plan page
      | screenShotName               | Reg_Loan_0011_Android_16 |
      | screenShotName1              | Reg_Loan_0011_Android_17 |
    Then I share hkid

  @DC @DC2
  Scenario: Reg_Loan_0012 upload document-income proof for DC flow
    Then I goto loansPage2
      | screenShotName   | Reg_Loan_0012_Android_01 |
    And check Congratulation Page Before Upload Files
    And I click upload button
      | screenShotName01 | Reg_Loan_0012_Android_02 |
    And I select income proof
      | screenShotName02 | Reg_Loan_0012_Android_03 |
    And I click add photo
      | screenShotName03 | Reg_Loan_0012_Android_04 |
    And I select photo library1
      | screenShotName04 | Reg_Loan_0012_Android_05 |
      | screenShotName05 | Reg_Loan_0012_Android_06 |
      | screenShotName06 | Reg_Loan_0012_Android_07 |
    And I click upload button
      | screenShotName07 | Reg_Loan_0012_Android_08 |
    And I select no thanks
      | screenShotName08 | Reg_Loan_0012_Android_09 |
    And I select residential address proof
      | screenShotName09 | Reg_Loan_0012_Android_10 |
    And I click add photo2
      | screenShotName10 | Reg_Loan_0012_Android_11 |
    And I select photo library2
      | screenShotName04 | Reg_Loan_0012_Android_12 |
      | screenShotName05 | Reg_Loan_0012_Android_13 |
      | screenShotName06 | Reg_Loan_0012_Android_14 |
    And I click upload button
      | screenShotName01 | Reg_Loan_0012_Android_15 |
    And I select Credit card Personal loan proof
      | screenShotName | Reg_Loan_0012_Android_16 |
    And I click add photo3
      | screenShotName | Reg_Loan_0012_Android_17 |
    And I select photo library1
      | screenShotName04 | Reg_Loan_0012_Android_18 |
      | screenShotName05 | Reg_Loan_0012_Android_19 |
      | screenShotName06 | Reg_Loan_0012_Android_20 |
    And I click upload button
      | screenShotName01 | Reg_Loan_0012_Android_21 |
    And I click done button
      | screenShotName | Reg_Loan_0012_Android_22 |
      | screenShotName01 | Reg_Loan_0012_Android_23 |

  @RF  @RF1
  Scenario: Reg_Loan_0021 Loans RF Happy Flow
    Then I goto loansPage
      | screenShotName               | Reg_Loan_0021_Android_01 |
    And I goto RF Page
      | screenShotName               | Reg_Loan_0021_Android_02 |
    Then set Rf Tenor And Purpose
      | amount                       | 60000                  |
      | tenor                        | 12                      |
      | screenShotName               | Reg_Loan_0021_Android_03 |
    Then I can see Key Facts Statements Page
    And I read Key Facts Statements Page and agree
      | screenShotName               | Reg_Loan_0021_Android_04 |
    Then change the info in the page Review and confirm
      | screenShotName               | Reg_Loan_0021_Android_06 |
      | screenShotName1              | Reg_Loan_0021_Android_07 |
    And I goto The Plan page
      | screenShotName               | Reg_Loan_0021_Android_08 |
      | screenShotName1              | Reg_Loan_0021_Android_09 |
    Then I share hkid

  @RF  @RF2
  Scenario: Reg_Loan_0022 Loans RF Happy Flow upload proof
    Then I goto loansPage2
      | screenShotName   | Reg_Loan_0022_Android_01 |
    And check Congratulation Page Before Upload Files
    And I click upload button
      | screenShotName01 | Reg_Loan_0022_Android_02 |
    And I select income proof
      | screenShotName02 | Reg_Loan_0022_Android_03 |
    And I click add photo
      | screenShotName03 | Reg_Loan_0022_Android_04 |
    And I select photo library1
      | screenShotName04 | Reg_Loan_0022_Android_05 |
      | screenShotName05 | Reg_Loan_0022_Android_06 |
      | screenShotName06 | Reg_Loan_0022_Android_07 |
    And I click upload button
      | screenShotName07 | Reg_Loan_0022_Android_08 |
    And I select no thanks
      | screenShotName08 | Reg_Loan_0022_Android_09 |
    And I select residential address proof
      | screenShotName09 | Reg_Loan_0022_Android_10 |
    And I click add photo2
      | screenShotName10 | Reg_Loan_0022_Android_11 |
    And I select photo library2
      | screenShotName04 | Reg_Loan_0022_Android_12 |
      | screenShotName05 | Reg_Loan_0022_Android_13 |
      | screenShotName06 | Reg_Loan_0022_Android_14 |
    And I click upload button
      | screenShotName01 | Reg_Loan_0022_Android_15 |
    And I click done button
      | screenShotName | Reg_Loan_0022_Android_16 |
      | screenShotName01 | Reg_Loan_0022_Android_17 |
