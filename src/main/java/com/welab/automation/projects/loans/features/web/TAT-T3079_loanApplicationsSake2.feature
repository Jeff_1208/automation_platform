Feature:  Loan Applications Sake2

  Background: Approved by maker and checker

  @sake2 @VBL-T3125  @PIL @DC
  Scenario: Handle the loan in sake2 step1 with maker approved
        # And close app driver
    Given Open the URL https://ploan-portal-sta2.sta-wlab.net/
    And Enter the user name and password and log in
      | username | maker1 |
      | password | Aa123456 |
    And Select the corresponding data or query by ID
      | screenShotName               | Reg_maker_001_Android_01 |
    And maker Approved
      | screenShotName               | Reg_maker_001_Android_02 |
      | screenShotName1              | Reg_maker_001_Android_03 |
      | screenShotName2              | Reg_maker_001_Android_04 |
      | screenShotName3              | Reg_maker_001_Android_05 |
      | screenShotName4              | Reg_maker_001_Android_06 |
      | screenShotName5              | Reg_maker_001_Android_07 |

  @sake2 @VBL-T3147  @PIL @DC
  Scenario: Handle the loan in sake2 step2 with checker approved
        # And close app driver
    Given Open the URL https://ploan-portal-sta2.sta-wlab.net/
    And Enter the user name and password and log in
      | username | checker1 |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName               | Reg_Checker_001_Android_01 |
    And  Checker update assign successfully
      | upAssigneeName  | checker1 checker1 (Checker)     |
      | statusSelect    | Pending Approval                |
      |  riskName      | levelb1 levelb1 (Credit Checker B) |
#      | riskName        | CreditB Tech (Credit Checker B) |
      | screenShotName  | Reg_Checker_001_Android_02      |
      | screenShotName1 | Reg_Checker_001_Android_03      |
      | screenShotName2 | Reg_Checker_001_Android_04      |
      | screenShotName3 | Reg_Checker_001_Android_05      |
      | screenShotName4 | Reg_Checker_001_Android_06      |
      | screenShotName5 | Reg_Checker_001_Android_07      |
      | screenShotName6 | Reg_Checker_001_Android_08      |

  @sake2  @VBL-T3148 @PIL @DC
  Scenario: Handle the loan in sake2 step3 with levelb1 approved
        # And close app driver
    Given Open the URL https://ploan-portal-sta2.sta-wlab.net/
    And Enter the user name and password and log in
      | username | levelb1 |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName | Reg_Levelb_001_Android_01 |
    And  Click the tab 'credit info' and  the button 'Approve'
      | loanStatus      | Approve                   |
      | screenShotName  | Reg_Levelb_001_Android_02 |
      | screenShotName1 | Reg_Levelb_001_Android_03 |
    Then refresh Web Page
    Then Input the application id and click the button "search all loan"
      | screenShotName | Reg_Levelb_001_Android_04 |

  @sake2 @DC  @VBL-T3252
  Scenario: Handle the loan in sake2 step4 with leveld1 approved
        # And close app driver
    Given Open the URL https://ploan-portal-sta2.sta-wlab.net/
    And Enter the user name and password and log in
      | username | checker1 |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName | Reg_Leveld_001_Android_01 |
    Then update Assign To Account
      | riskName       | CreditD Tech (Credit Checker D) |
      | screenShotName | Reg_Leveld_001_Android_02       |
    And Click the exit login button and click login
    And Enter the user name and password and log in
      | username | td       |
      | password | Aa123456 |
    Then Input the application id and click the button "search all loan"
      | screenShotName | Reg_Leveld_001_Android_03 |
    And  Click the tab 'credit info' and  the button 'Approve'
      | loanStatus      | Approve                   |
      | screenShotName  | Reg_Leveld_001_Android_04 |
      | screenShotName1 | Reg_Leveld_001_Android_05 |
    Then refresh Web Page
    Then Input the application id and click the button "search all loan"
      | screenShotName | Reg_Leveld_001_Android_06 |
    Then verify Last Status


  # 2022
#  Scenario: Handle the loan in sake2 with maker and checker
#        # And close app driver
#    Given Open the URL https://ploan-portal-sta2.sta-wlab.net/
#    And Enter the user name and password and log in
#      | username | maker1 |
#      | password | Aa123456 |
#    And Select the corresponding data or query by ID
#      | screenShotName               | Reg_maker_001_Android_01 |
#    And select pass in theNew mobile number in TU: in the area of Personal info and click Save
#    And select all  Maker review valid  in the area of documents
#      | status | Pending Review |
#      | screenShotName               | Reg_maker_001_Android_02 |
#      | screenShotName1               | Reg_maker_001_Android_03 |
#    When select Pass in the Address proof Income proof and Employment verification
#    And status change to pending checker reason add approved
#      | screenShotName               | Reg_maker_001_Android_04 |
#      | screenShotName1               | Reg_maker_001_Android_05 |
#    And Click the exit login button and click login
#    And Enter the user name and password and log in
#      | username | checker1 |
#      | password | Aa123456 |
#    Then Input the application id and click the button "search all loan"
#      | screenShotName               | Reg_Checker_001_Android_01 |
#    And  Checker update assign successfully
#      | upAssigneeName | checker1 checker1 (Checker) |
#      | statusSelect   | Pending Approval |
#      |  riskName      | levelb1 levelb1 (Credit Checker B) |
#      | screenShotName               | Reg_Checker_001_Android_02 |
#      | screenShotName1              | Reg_Checker_001_Android_03 |
#      | screenShotName2              | Reg_Checker_001_Android_04 |
#      | screenShotName3              | Reg_Checker_001_Android_05 |
#      | screenShotName4              | Reg_Checker_001_Android_06 |
#      | screenShotName5              | Reg_Checker_001_Android_07 |
#      | screenShotName6              | Reg_Checker_001_Android_08 |
#    And Click the exit login button and click login
#    And Enter the user name and password and log in
#      | username | levelb1  |
#      | password | Aa123456 |
#    Then Input the application id and click the button "search all loan"
#      | screenShotName               | Reg_Checker_001_Android_09 |
#    And  Click the tab 'credit info' and  the button 'Approve'
#      | loanStatus | Approve |
#      | screenShotName               | Reg_Checker_001_Android_10 |
#      | screenShotName1              | Reg_Checker_001_Android_11 |