package com.welab.automation.projects.loans.pages.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.time.Duration;

import static com.welab.automation.framework.utils.Utils.logFail;

public class CommonPage extends AppiumBasePage {

  private final String pageName = "Common Page";
  private final String upgradeText = "Got it";
  private final String cancelText = "Cancel";
  private final int customWait = 50;
  private final int toastWait = 5;
  private final int waitMillSeconds = 3000;


  public static final String skipRootString =
          "//android.widget.TextView[contains(@text,'Jailbreak')]/../android.widget.Switch";

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']/../android.widget.TextView[1]")
  private MobileElement devPanelBackBtn;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Login']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='登入']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
  })
  private MobileElement loginTxt;

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']")
  private MobileElement devPanelTitle;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Got it']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='我明白啦']")
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Got it'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '我明白啦'"),
  })
  private MobileElement upgradeOKBtn;

  @iOSXCUITFindBy(accessibility = "-btn-title")
  @AndroidFindBy(accessibility = "-btn")
  private MobileElement gotItBtn;

  @iOSXCUITFindBy(accessibility = "Select one you want to explore now!")
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Select one you want to explore now!']")
  private MobileElement exploreText;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[5]")
  private MobileElement eyeBtn;

  @AndroidFindBy(
          xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]")
  private MobileElement rocketButton;

  @AndroidFindBy(
          xpath = "//android.widget.TextView[contains(@text,'Root')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Root/Jailbreak detection\"])[2]")
  private MobileElement skipRoot;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'OCR')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过OCR扫描\"])[2]")
  private MobileElement skipORC;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Rebind Device')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Rebind Device\"])[2]")
  private MobileElement skipRebindDevice;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'是否跳过Rebind Device Authentication')]/../android.widget.Switch")
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"是否跳过Rebind Device Authentication\"])[2]")
  private MobileElement skipRootRebindDeviceAuthentication;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @AndroidFindBy(accessibility = "GoFlexi, tab, 4 of 5")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoFlexi, tab, 4 of 5'")
  private MobileElement loanTab;


  @AndroidFindBy(
          xpath = "//android.widget.TextView[contains(@text, 'Hi,')]/../../android.view.ViewGroup[3]")
  @iOSXCUITFindBy(
          xpath =
                  "//XCUIElementTypeStaticText[starts-with(@label, 'Hi,')]/../../../../XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
  private MobileElement MyAccountBtn;

  @AndroidFindBy(xpath = "//*[@text='Learn more']")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Learn more']")
  private MobileElement learnMoreBtn;

  @AndroidFindBy(xpath = "//*[@text='Skip to open an account']")
  @iOSXCUITFindBy(accessibility = "skipTipText")
  private MobileElement skipToOpenAccountBtn;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  private MobileElement welcomeMutFundTitle;

  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  private MobileElement welcomeMutFundBtn;

  @AndroidFindBy(xpath = "//*[@text='Update and verify personal info']")
  @iOSXCUITFindBy(accessibility = "PROCESS_GUIDE-title")
  private MobileElement updateVerifyPersonalInfoTip;

  @AndroidFindBy(xpath = "//*[@text='Next']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement nextBtn;

  // used for exception showing up.
  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement exceptionCancelBtn;

  @AndroidFindBy(xpath = "//*[@text='Open an account']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'openAccountBtn-title'")
  private MobileElement openAccountBtn;

  @Getter
  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@content-desc='btnClose']/android.view.ViewGroup[1]")
  })
  @iOSXCUITFindBy(xpath = "//*[@name='btnClose']/XCUIElementTypeOther[1]")
  private MobileElement xIcon;

  @AndroidFindBy(accessibility = "btnBack")
  @iOSXCUITFindBy(xpath = "//*[@name='btnBack']/XCUIElementTypeOther[1]")
  private MobileElement backIcon;

  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement cancelBtn;

  @AndroidFindBy(
          xpath =
                  "//android.widget.TextView[@text='Check your order status, client risk profiling, promotion, tutorial, and more']/..")
  @iOSXCUITFindBy(
          iOSNsPredicate =
                  "name == 'Wealth Centre Check your order status, client risk profiling, promotion, tutorial, and more'")
  private MobileElement wealthCenterMenu;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-title']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement thanksTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-desc']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-desc'")
  private MobileElement pendingDesc;

  @AndroidFindBy(xpath = "//*[@text='EN']")
  private MobileElement EN;
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm']")
  private MobileElement confirmBtn;
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='繁體中文']")
  private MobileElement chineseBtn;
  @iOSXCUITFindBy(iOSNsPredicate = "label == \"流動保安編碼認證\" AND name == \"text\"")
  private MobileElement MSKpage;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"0\"]")
  private MobileElement softKeyNumber0;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"1\"]")
  private MobileElement softKeyNumber1;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"2\"]")
  private MobileElement softKeyNumber2;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"3\"]")
  private MobileElement softKeyNumber3;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"4\"]")
  private MobileElement softKeyNumber4;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"5\"]")
  private MobileElement softKeyNumber5;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"6\"]")
  private MobileElement softKeyNumber6;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"7\"]")
  private MobileElement softKeyNumber7;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"8\"]")
  private MobileElement softKeyNumber8;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"9\"]")
  private MobileElement softKeyNumber9;
  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeKey[@name=\"删除\"]"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeKey[@name=\"delete\"]")
  })
  private MobileElement softKeyDelete;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"more\"]")
  private MobileElement softKeyMore;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Return\"]")
  private MobileElement softKeyReturn;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"shift\"]")
  private MobileElement softKeyShift;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"space\"]")
  private MobileElement softKeySpace;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"A\"]")
  private MobileElement softKeyA;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"B\"]")
  private MobileElement softKeyB;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"C\"]")
  private MobileElement softKeyC;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"D\"]")
  private MobileElement softKeyD;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"E\"]")
  private MobileElement softKeyE;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"F\"]")
  private MobileElement softKeyF;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"G\"]")
  private MobileElement softKeyG;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"H\"]")
  private MobileElement softKeyH;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"I\"]")
  private MobileElement softKeyI;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"J\"]")
  private MobileElement softKeyJ;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"K\"]")
  private MobileElement softKeyK;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"L\"]")
  private MobileElement softKeyL;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"M\"]")
  private MobileElement softKeyM;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"N\"]")
  private MobileElement softKeyN;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"O\"]")
  private MobileElement softKeyO;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"P\"]")
  private MobileElement softKeyP;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"Q\"]")
  private MobileElement softKeyQ;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"R\"]")
  private MobileElement softKeyR;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"S\"]")
  private MobileElement softKeyS;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"T\"]")
  private MobileElement softKeyT;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"U\"]")
  private MobileElement softKeyU;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"V\"]")
  private MobileElement softKeyV;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"W\"]")
  private MobileElement softKeyW;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"X\"]")
  private MobileElement softKeyX;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"Y\"]")
  private MobileElement softKeyY;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"Z\"]")
  private MobileElement softKeyZ;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"a\"]")
  private MobileElement softKeya;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"b\"]")
  private MobileElement softKeyb;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"c\"]")
  private MobileElement softKeyc;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"d\"]")
  private MobileElement softKeyd;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"e\"]")
  private MobileElement softKeye;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"f\"]")
  private MobileElement softKeyf;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"g\"]")
  private MobileElement softKeyg;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"h\"]")
  private MobileElement softKeyh;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"i\"]")
  private MobileElement softKeyi;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"j\"]")
  private MobileElement softKeyj;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"k\"]")
  private MobileElement softKeyk;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"l\"]")
  private MobileElement softKeyl;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"m\"]")
  private MobileElement softKeym;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"n\"]")
  private MobileElement softKeyn;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"o\"]")
  private MobileElement softKeyo;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"p\"]")
  private MobileElement softKeyp;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"q\"]")
  private MobileElement softKeyq;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"r\"]")
  private MobileElement softKeyr;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"s\"]")
  private MobileElement softKeys;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"t\"]")
  private MobileElement softKeyt;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"u\"]")
  private MobileElement softKeyu;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"v\"]")
  private MobileElement softKeyv;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"w\"]")
  private MobileElement softKeyw;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"x\"]")
  private MobileElement softKeyx;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"y\"]")
  private MobileElement softKeyy;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeKey[@name=\"z\"]")
  private MobileElement softKeyz;
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"麻煩等陣再試多次\"])[1]")
  private MobileElement pleaseTryAgainMessage;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"取消\"]")
  private MobileElement tryAgainCancelBtn;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text = 'Enter your mobile security key']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text = '請輸入6位數字嘅流動保安編碼。']"),
  })
  private MobileElement enterKey;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text = 'Forgot mobile security key?']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text = '忘記流動保安編碼？']"),
  })
  private MobileElement forgotKey;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Maybe later']"),
  })
  private MobileElement maybeLaterButton;

  public CommonPage() {
    super.pageName = this.pageName;
  }

  public void checkTryAgainMessage() {
    if (isShow(pleaseTryAgainMessage, 2)) {
      clickElement(tryAgainCancelBtn);
    }
  }

  public void clickShift() {
    clickElement(softKeyShift);
  }

  public static int setSleepTime(int... sleepTime) {
    int actualSleepTime = 100;
    if (sleepTime.length != 0) {
      actualSleepTime = sleepTime[0];
    }
    return actualSleepTime;
  }

  public void changeKeyboardToLower(){
    if(isShow(softKeyB, 1)){
      clickElement(softKeyShift);
    }
  }

  @SneakyThrows
  public void clickSoftKeyByNumberString(String data, int... sleepTime) {
    int actualSleepTime = setSleepTime(sleepTime);
    changeKeyboardToLower();
    for (int i = 0; i < data.length(); i++) {
      String v = data.charAt(i) + "";
      Thread.sleep(actualSleepTime);
      switch (v) {
        case "0":
          clickElement(softKeyNumber0);
          break;
        case "1":
          clickElement(softKeyNumber1);
          break;
        case "2":
          clickElement(softKeyNumber2);
          break;
        case "3":
          clickElement(softKeyNumber3);
          break;
        case "4":
          clickElement(softKeyNumber4);
          break;
        case "5":
          clickElement(softKeyNumber5);
          break;
        case "6":
          clickElement(softKeyNumber6);
          break;
        case "7":
          clickElement(softKeyNumber7);
          break;
        case "8":
          clickElement(softKeyNumber8);
          break;
        case "9":
          clickElement(softKeyNumber9);
          break;

        case "a":
          clickElement(softKeya);
          break;
        case "b":
          clickElement(softKeyb);
          break;
        case "c":
          clickElement(softKeyc);
          break;
        case "d":
          clickElement(softKeyd);
          break;
        case "e":
          clickElement(softKeye);
          break;
        case "f":
          clickElement(softKeyf);
          break;
        case "g":
          clickElement(softKeyg);
          break;
        case "h":
          clickElement(softKeyh);
          break;
        case "i":
          clickElement(softKeyi);
          break;
        case "j":
          clickElement(softKeyj);
          break;
        case "k":
          clickElement(softKeyk);
          break;
        case "l":
          clickElement(softKeyl);
          break;
        case "m":
          clickElement(softKeym);
          break;
        case "n":
          clickElement(softKeyn);
          break;
        case "o":
          clickElement(softKeyo);
          break;
        case "p":
          clickElement(softKeyp);
          break;
        case "q":
          clickElement(softKeyq);
          break;
        case "r":
          clickElement(softKeyr);
          break;
        case "s":
          clickElement(softKeys);
          break;
        case "t":
          clickElement(softKeyt);
          break;
        case "u":
          clickElement(softKeyu);
          break;
        case "v":
          clickElement(softKeyv);
          break;
        case "w":
          clickElement(softKeyw);
          break;
        case "x":
          clickElement(softKeyx);
          break;
        case "y":
          clickElement(softKeyy);
          break;
        case "z":
          clickElement(softKeyz);
          break;

        case "A":
          clickElement(softKeyShift);
          clickElement(softKeyA);
          break;
        case "B":
          clickElement(softKeyShift);
          clickElement(softKeyB);
          break;
        case "C":
          clickElement(softKeyShift);
          clickElement(softKeyC);
          break;
        case "D":
          clickElement(softKeyShift);
          clickElement(softKeyD);
          break;
        case "E":
          clickElement(softKeyShift);
          clickElement(softKeyE);
          break;
        case "F":
          clickElement(softKeyShift);
          clickElement(softKeyF);
          break;
        case "G":
          clickElement(softKeyShift);
          clickElement(softKeyG);
          break;
        case "H":
          clickElement(softKeyShift);
          clickElement(softKeyH);
          break;
        case "I":
          clickElement(softKeyShift);
          clickElement(softKeyI);
          break;
        case "J":
          clickElement(softKeyShift);
          clickElement(softKeyJ);
          break;
        case "K":
          clickElement(softKeyShift);
          clickElement(softKeyK);
          break;
        case "L":
          clickElement(softKeyShift);
          clickElement(softKeyL);
          break;
        case "M":
          clickElement(softKeyShift);
          clickElement(softKeyM);
          break;
        case "N":
          clickElement(softKeyShift);
          clickElement(softKeyN);
          break;
        case "O":
          clickElement(softKeyShift);
          clickElement(softKeyO);
          break;
        case "P":
          clickElement(softKeyShift);
          clickElement(softKeyP);
          break;
        case "Q":
          clickElement(softKeyShift);
          clickElement(softKeyQ);
          break;
        case "R":
          clickElement(softKeyShift);
          clickElement(softKeyR);
          break;
        case "S":
          clickElement(softKeyShift);
          clickElement(softKeyS);
          break;
        case "T":
          clickElement(softKeyShift);
          clickElement(softKeyT);
          break;
        case "U":
          clickElement(softKeyShift);
          clickElement(softKeyU);
          break;
        case "V":
          clickElement(softKeyShift);
          clickElement(softKeyV);
          break;
        case "W":
          clickElement(softKeyShift);
          clickElement(softKeyW);
          break;
        case "X":
          clickElement(softKeyShift);
          clickElement(softKeyX);
          break;
        case "Y":
          clickElement(softKeyShift);
          clickElement(softKeyY);
          break;
        case "Z":
          clickElement(softKeyShift);
          clickElement(softKeyZ);
          break;
        case " ":
          clickElement(softKeySpace);
          break;
        case "&":
          clickElement(softKeyMore);
          break;

        default:
          break;
      }

    }
  }


  public void clickMskFisrtInput() {
    if(isIphoneXR()){
      clickByLocationByPercent(10, 20.9);
    }else{
      clickByLocationByPercent(10, 28.2);
    }
  }

  public void waitMskPage() {
    waitUntilElementVisible(MSKpage);
  }

  public void inputMSKByString(String value, int... sleepTime) {
    if (!isShow(softKeyNumber1, 2)) {
      clickMskFisrtInput();
    }
    clickSoftKeyByNumberString(value, sleepTime);
  }

  public void inputMSK() {
    String mskPassword = GlobalVar.GLOBAL_VARIABLES.get("mskPassword");
    inputMSKByString(mskPassword);
  }

  public void inputDeleteByNumber(int number) {
    for (int i = 0; i < number; i++) {
      clickElement(softKeyDelete);
    }
  }

  public void clickSoftKeyMore() {
    clickElement(softKeyMore);
  }

  public void clickSoftKeyReturn() {
    if(isShow(softKeyReturn, 1)){
      clickElement(softKeyReturn);
    }
  }

  public void checkAndclickSoftKeyReturn(){
    if(isShow(softKeyReturn, 2)){
      clickSoftKeyReturn();
    }
  }

  public void inputLimitNumber() {
    clickElement(softKeyNumber1);
    clickElement(softKeyNumber0);
    clickElement(softKeyNumber0);
    clickElement(softKeyNumber0);
  }

  public boolean isAndroid() {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      return true;
    }
    return false;
  }

  public boolean isIOS() {
    if (System.getProperty("mobile").equalsIgnoreCase("ios")) {
      return true;
    }
    return false;
  }

  public boolean oneOfArrayItemInStr(String str, String[] array) {
    for (int i = 0; i < array.length; i++) {
      if (str.contains(array[i])) return true;
    }
    return false;
  }

  public void enableSkipRoot() throws InterruptedException {
    By skipRootBy = By.xpath(skipRootString);
    Thread.sleep(3000);
    waitUntilElementClickable(eyeBtn);
    clickElement(eyeBtn);
    waitUntilElementVisible(rocketButton);
    clickElement(rocketButton);
    waitUntilElementVisible(devPanelTitle);
    scrollUpToFindElement(skipRootBy, 6, 3);
    Thread.sleep(200);
    //setSwithToChecked(skipORC);
    setSwithToChecked(skipRebindDevice);
    //setSwithToChecked(skipRoot);
    clickElement(devPanelBackBtn);
  }

  @SneakyThrows
  public void enableSkipRootRebindDeviceAuthenticationIOS() throws InterruptedException {
    clickByPicture2("src/main/resources/images/skip/eye.png",50,50);
    Thread.sleep(2000);
    clickByPicture2("src/main/resources/images/skip/rocket.png",50,50);
    scrollUp();
    if(!isIphoneXR()){
      scrollUp();
      scrollUp();
    }
    Thread.sleep(5000);
    scrollByLocation(88,99,getElementCenterY(skipRootRebindDeviceAuthentication));
    Thread.sleep(1000);
    clickByLocationByPercent(9,6);
  }

  public void enableSkipRootRebindDeviceAuthentication() throws InterruptedException {
    By skipRootBy = By.xpath(skipRootString);
    Thread.sleep(3000);
    waitUntilElementClickable(eyeBtn);
    clickElement(eyeBtn);
    waitUntilElementVisible(rocketButton);
    clickElement(rocketButton);
    waitUntilElementVisible(devPanelTitle);
    scrollUpToFindElement(skipRootBy, 6, 3);
    Thread.sleep(200);
    setSwithToChecked(skipRootRebindDeviceAuthentication);
    clickElement(devPanelBackBtn);
  }

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='Your Core Account has become dormant.']"),
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"你嘅核心賬戶已處於「不動賬戶」狀態。\" AND name == \"text\""),
          @iOSXCUITBy(xpath = "//*[contains(@value ,'你嘅核心賬戶已處於「不動賬戶」狀態"),
  })
  private MobileElement YourCoreAccountHasBecomeDormant;

  @AndroidFindAll({
          @AndroidBy(xpath = "//android.widget.TextView[@text='OK']"),
          @AndroidBy(xpath = "//android.widget.TextView[@text='知道']"),
  })
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"知道\"]")
  private MobileElement dormantOkButton;

  @SneakyThrows
  public void checkDormantMessage(){
    if(isShow(YourCoreAccountHasBecomeDormant,10)){
      Thread.sleep(1000);
      clickElement(dormantOkButton);
    }
  }

  @AndroidFindAll({
          @AndroidBy(id = "com.android.systemui:id/button2"),
          @AndroidBy(xpath = "//*[@text='CANCEL']"),
          @AndroidBy(xpath = "//*[@text='取消']"),
  })
  @iOSXCUITFindBy(xpath = "(//XCUIElementTypeButton[@name=\"取消\"])[2]")
  private MobileElement cancelForFingerprint;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Next']"),
          @AndroidBy(xpath = "//*[@text='下一步']"),
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"下一步\" AND name == \"text\""),
          @iOSXCUITBy(iOSNsPredicate = "label == \"下一步\""),
  })
  private MobileElement bindDeviceAuthenticationNextButton;

  @AndroidFindAll({
          @AndroidBy(xpath = "//*[@text='Continue']"),
          @AndroidBy(xpath = "//*[@text='繼續']"),
  })
  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"繼續\" AND name == \"text\""),
          @iOSXCUITBy(iOSNsPredicate = "label == \"繼續\""),
  })
  private MobileElement bindDeviceAuthenticationContinueButton;

  @SneakyThrows
  public void bindDeviceAuthenticationIOS(){
    if(isShow(bindDeviceAuthenticationNextButton)){
      Thread.sleep(1000);
      clickElement(bindDeviceAuthenticationNextButton);
      Thread.sleep(2000);
      inputMSKByString("123456");
      Thread.sleep(200);
      inputMSKByString("123456");
      waitUntilElementVisible(cancelForFingerprint);
      clickElement(cancelForFingerprint);
      Thread.sleep(1*1000);
      waitUntilElementVisible(bindDeviceAuthenticationContinueButton);
      clickElement(bindDeviceAuthenticationContinueButton);
      Thread.sleep(10*1000);
    }
  }
  @SneakyThrows
  public void bindDeviceAuthentication(){
    if(isShow(bindDeviceAuthenticationNextButton)) {
      Thread.sleep(3000);
      clickElement(bindDeviceAuthenticationNextButton);
      Thread.sleep(3000);
      sendMsk();
      Thread.sleep(1000);
      sendMsk();
      waitUntilElementVisible(cancelForFingerprint);
      Thread.sleep(1000);
      clickElement(cancelForFingerprint);
      //clickByLocationByPercent(50,77); //click cancel button for Fingerprint  SAMSUNG S20
      Thread.sleep(5 * 1000);
      if (isShow(bindDeviceAuthenticationContinueButton, 5)) {
        clickElement(bindDeviceAuthenticationContinueButton);
      }
      //clickByLocationByPercent(50,56); //click continue button for Fingerprint
      Thread.sleep(10 * 1000);
    }
    checkDormantMessage();
  }

  @SneakyThrows
  public void moveEyeBtnToTopRight() {
    Thread.sleep(5000);
    int width = driver.manage().window().getSize().width;
    TouchAction action = new TouchAction(driver);
    action
            .press(PointOption.point(eyeBtn.getCenter().getX(), eyeBtn.getCenter().getY()))
            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
            .moveTo(PointOption.point(width, 0))
            .release()
            .perform();
  }

  public void checkLogin() {
    if (isShow(loginTxt, 5)) {
      clickElement(loginTxt);
    }
  }

  public void checkUpgrade() {
    if (isShow(upgradeOKBtn, 2)) {
      clickElement(upgradeOKBtn);
    }
  }

  public void clickContactWarningCommon() {
    if (isElementDisplayed(contactWarningCancel, cancelText, 3)) {
      waitUntilElementClickable(contactWarningCancel);
      clickElement(contactWarningCancel);
    }
  }

  public void gotoLoan() throws InterruptedException {
    waitUntilElementVisible(loanTab);
    loanTab.click();
    Thread.sleep(waitMillSeconds);
  }


  // GoalsNSettings: A_01_Landing to CA_01_SideMenu
  public void gotoMyAccount() {
    waitUntilElementVisible(MyAccountBtn);
    clickElement(MyAccountBtn);
    clickContactWarningCommon();
  }

  // GoalsNSettings: CA_01_SideMenu to CB_01_WealthCentre
  public void gotoWealthCenter() {
    waitUntilElementClickable(wealthCenterMenu);
    clickElement(wealthCenterMenu);
  }

  @SneakyThrows
  public void skipGuidance() {
    waitUntilElementClickable(gotItBtn);
    Thread.sleep(200);
    clickElement(gotItBtn);
    waitUntilElementVisible(exploreText);
    clickXIcon();
  }

  // This is a workaround to go to versify personal information page
  // should not use this method if account generation script is ready.
  public void skipToOpenAccount() {
    waitUntilElementVisible(learnMoreBtn);
    clickElement(learnMoreBtn);
    waitUntilElementVisible(skipToOpenAccountBtn);
    clickElement(skipToOpenAccountBtn);
    waitUntilElementVisible(welcomeMutFundTitle);
    scrollUp();
    clickElement(welcomeMutFundBtn);
    waitUntilElementVisible(updateVerifyPersonalInfoTip);
    // if no delay, the "welcome mutual fund" might be generated again after click "Next"
    // sleep 200 ms to avoid this.
    try {
      Thread.sleep(500);
    } catch (Exception e) {
      logFail(e.getMessage(), pageName);
    }
    clickElement(nextBtn);
    // currently, exception might pop up during the step: Verify personal information.
    // check and cancel the exception page to continue
    checkException();
  }

  public void checkException() {

    waitUntilElementVisible(contactWarningCancel);
    clickElement(contactWarningCancel);
  }

  public void openAccount() {
    waitUntilElementClickable(openAccountBtn);
    clickElement(openAccountBtn);
  }

  public void clickXIcon() {
    waitUntilElementClickable(xIcon);
    clickElement(xIcon);
  }

  @SneakyThrows
  public void clickBackIcon() {
    Thread.sleep(toastWait);
    clickElement(backIcon);
    Thread.sleep(1000 * 3);
  }

  // check the upload page is loaded
  public boolean checkUploadElement() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  /**
   * A_06_Pending: Waiting for uploaded documents approval, the account not opened
   *
   * @return
   */
  public String getPendingApprovalDesc() {
    try {
      Thread.sleep(1000 * 2);
      waitUntilElementVisible(thanksTitle);
      WebElement pendingTxt = waitUntilElementVisible(pendingDesc);
      return pendingTxt.getText();
    } catch (Exception e) {
      return "";
    }
  }


  @SneakyThrows
  public boolean checkWealthLanding() {
    Thread.sleep(1000 * 3);
    if (isElementDisplayed(openAccountBtn) != null) {
      return true;
    }
    return false;
  }


  public boolean checkGuidanceElementShowing() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  @SneakyThrows
  public boolean changeLanguageToCH() {
    if (verifyElementExist(EN)) {
      waitUntilElementClickable(EN);
      clickElement(EN);
    } else {
      return true;
    }
    waitUntilElementClickable(chineseBtn);
    clickElement(chineseBtn);
    clickElement(confirmBtn);
    Thread.sleep(5000);
    boolean flag = verifyContainsText(chineseBtn, "繁體中文", false);
    return flag;
  }

  public void androidInputEnter() {
    if (System.getProperty("mobile").equals("android")) {
      driver.getKeyboard().pressKey(Keys.ENTER);
      logger.info("ADB input ENTER on page:{}!", pageName);
    }
  }

  public void moveEyeBtnToMiddleRight() {
    final int width = driver.manage().window().getSize().width;
    final int height = driver.manage().window().getSize().height;
    TouchAction action = new TouchAction(driver);
    action.press(PointOption.point(width * 88 / 100, height * 26 / 100))
            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
            .moveTo(PointOption.point(width, height / 2))
            .release()
            .perform();
    action.press(PointOption.point(width * 92 / 100, height * 22 / 100))
            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
            .moveTo(PointOption.point(width, height / 2))
            .release()
            .perform();
  }

  public void waiteSendMsk() {
    if (isShow(enterKey, 5) && isShow(forgotKey, 5)) {
      sendMsk();
    }
  }

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"WeLab Bank STAGE\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther")
  private MobileElement eye; //没有展开
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"WeLab Bank STAGE\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]")
  private MobileElement eye1; //已经展开
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"WeLab Bank STAGE\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]")
  private MobileElement live;
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"WeLab Bank STAGE\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]")
  private MobileElement rocket;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"更新應用程式\"")
  private MobileElement liveUpdatePage;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"更新應用程式 請確保你至少有150MB的儲存空間以及已連接網絡來更新應用程式。 立即更新\"])[4]/XCUIElementTypeOther[5]/XCUIElementTypeOther")
  private MobileElement eyeLocationOnLiveUpdatePage;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"更新應用程式 請確保你至少有150MB的儲存空間以及已連接網絡來更新應用程式。 立即更新\"])[4]/XCUIElementTypeOther[2]/XCUIElementTypeOther")
  private MobileElement liveUpdateCircleButtonLocationOnLiveUpdatePage;

  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "label == \"取消升级\" AND name == \"text\""),
          @iOSXCUITBy(iOSNsPredicate = "label == \"取消升级\""),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"取消升级\"]"),
  })
  private MobileElement liveUpdateCancelButton;

  @iOSXCUITFindBy(iOSNsPredicate = "//XCUIElementTypeOther[@name=\"Live Update\"])[1]/XCUIElementTypeOther[2]")
  private MobileElement liveUpdateCloseButton;

  @iOSXCUITFindAll({
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"開戶\"]/../XCUIElementTypeStaticText"),
          @iOSXCUITBy(xpath = "//XCUIElementTypeOther[@name=\"Open account\"]/../XCUIElementTypeStaticText[1]"),
  })
  private MobileElement appVersion;

  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Login'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '登入'"),
  })
  private MobileElement loginButton;

  @iOSXCUITFindAll({
          @iOSXCUITBy(iOSNsPredicate = "name == 'Username'"),
          @iOSXCUITBy(iOSNsPredicate = "name == '用戶名稱'"),
  })
  private MobileElement userNameTxt;

  public void clickLiveCircleButton(){
    String appVersionString =getAppVersion();
    By by  = By.xpath(getXpathString(appVersionString,"2"));
    clickElement(findMobileElement(by));
  }

  public void clickEyeCircleButton(){
    String appVersionString =getAppVersion();
    By by  = By.xpath(getXpathString(appVersionString,"5"));
    clickElement(findMobileElement(by));
  }

  public String getXpathString(String appVersionString, String index){
    //最后一个中括号2表示第一个图标，第二个图标飞机需要用3，火箭图标用4，眼睛图标用5
    String LiveCircleButtonXpathString ="(//XCUIElementTypeOther[@name=\"繁體中文 你好 用戶名稱 忘記用戶名稱 密碼 忘記密碼 登入 開戶 "+appVersionString+" 系統維護通知 更多 水平滚动条, 1页\"])[4]/XCUIElementTypeOther["+index+"]/XCUIElementTypeOther";
    return LiveCircleButtonXpathString;
  }

  @SneakyThrows
  public String getAppVersion(){
    if(!isShow(userNameTxt, 3)){
      clickElement(loginButton);
    }
    Thread.sleep(1000);
    return appVersion.getText().trim();
  }

  @SneakyThrows
  public void skipLiveUpdateIos() {
    if(isShow(liveUpdatePage, 5)){
      clickElement(eyeLocationOnLiveUpdatePage);
      clickElement(liveUpdateCircleButtonLocationOnLiveUpdatePage);
      clickElement(liveUpdateCancelButton);
      if(isShow(liveUpdateCloseButton,2)){
        clickElement(liveUpdateCloseButton);
      }
      Thread.sleep(5000);
    }else {
      if(isIphoneSE()){
        clickEyeCircleButton();
        Thread.sleep(1000);
        clickLiveCircleButton();
        Thread.sleep(1000);
      }else{
        clickElement(eye);
        Thread.sleep(1000);
        clickElement(live);
        Thread.sleep(1000);
      }
      clickElement(liveUpdateCancelButton);
      if (isShow(liveUpdateCloseButton, 2)) {
        clickElement(liveUpdateCloseButton);
      }
    }
  }


  @SneakyThrows
  public void skipRootIos() throws InterruptedException {
    clickByPicture2("src/main/resources/images/skip/eye.png",50,50);
    Thread.sleep(1000);
    clickByPicture2("src/main/resources/images/skip/rocket.png",50,50);
    scrollUp();
    if(!isIphoneXR()){
      scrollUp();
      scrollUp();
    }
    Thread.sleep(2000);
    //scrollUpToFindElement(skipRebindDevice,5);
    /*
    clickByPicture("src/main/resources/images/skip/skipOCR.png",110,90);
    clickByPicture("src/main/resources/images/skip/skipRebind.png",115,50);
    clickByPicture("src/main/resources/images/skip/skipROOT.png",110,50);
     */
    //scrollByLocation(88,99,getElementCenterY(skipORC));
    //Thread.sleep(1000);
    scrollByLocation(88,99,getElementCenterY(skipRebindDevice));
    Thread.sleep(1000);
    //scrollByLocation(88,99,getElementCenterY(skipRoot));
    //Thread.sleep(1000);
    clickByLocationByPercent(9,6);
    //clickByPicture2("src/main/resources/images/skip/back.png",50,30);
  }


  public void checkTouchID(){
    if(isShow(maybeLaterButton,5)){
      clickElement(maybeLaterButton);
    }
  }
}
