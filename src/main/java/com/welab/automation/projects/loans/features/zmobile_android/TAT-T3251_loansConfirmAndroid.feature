Feature: Loans Confirm

  Background: Login
    Given Open loans WeLab App
#    And close web driver

  @loans @VBL-T3109 @jeff2
  Scenario: Reg_Loan_004 agreement generate successfully
    And Enable Skip Root Rebind Device Authentication
    And get app version loans
    When Login with user and password from properties
    And bind Device Authentication
    Then check Unexpected Element
    Then I go to loans Page after submit application
      | screenShotName  | Reg_Loan_004_Android_01 |
    And I click check it now button
      | screenShotName1 | Reg_Loan_004_Android_02 |
    And I click approval letter details
      | screenShotName2 | Reg_Loan_004_Android_03 |
      | screenShotName3 | Reg_Loan_004_Android_04 |
    And I return to the approval letter page
    And I click repayment schedule
      | screenShotName4 | Reg_Loan_004_Android_05 |
    And I return to the approval letter page
    And I click terms and conditions
      | screenShotName5 | Reg_Loan_004_Android_06 |
    And I return to the approval letter page
    And I click key facts statements
      | screenShotName6 | Reg_Loan_004_Android_07 |
    And I return to the approval letter page
    And I click by pressing button
      | screenShotName7 | Reg_Loan_004_Android_08 |
    And I click agree and drawdown button
      | screenShotName8 | Reg_Loan_004_Android_09 |
