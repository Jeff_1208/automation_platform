@Android @iOS
Feature: Review CRPQ

  Background: Login
    Given Open WeLab App
    And Enable skip Root


# todo, create crpq profile firstly by post request(or app auto), so as to check the actual answers are same as expected.
  Scenario: Review CRPQ questions from side menu
    Given Check upgrade page appears
    And Login with user and password
      | user     | testsit057 |
      | password | Aa123321   |
    And I can see the LoggedIn page
    And goto My Account page
    And I goto Wealth Center
    And I Review My Risk Profile
    Then The Answer of "Can you tell me why you" should be
      | Preserve my capital and earn deposit rate |
    And The Answer of "How do you see risk-return" should be
      | low risk to achieve low returns |
    And The Answer of "I can accept if my investment value fluctuates between" should be
      | -30% and 30% |
    And The Answer of "How much of your total liquid assets" should be
      | Less than 5% |
    And The Answer of "To meet my financial needs in the next 12 months" should be
      | Sell up to 75% |
    And The Answer of "If your investment value dropped by 50%" should be
      | Sell more than 50% of your investment |
    And The Answer of "How many months of expenses have you put aside" should be
      | 0 to less than 3 months |

