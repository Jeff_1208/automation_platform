
package com.welab.automation.projects.wealth.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      //
      "src/main/java/com/welab/automation/projects/wealth/features/mobile/ios/IAOApplicationStatus_ios.feature"
    },
    glue = {
      "com/welab/automation/projects/wealth/steps/mobile",
      "com/welab/automation/projects/wealth/steps/web"
    },
    tags = "@debug",
    monochrome = true)
public class AdminPortalTestRunner_ios extends TestRunner {}
