@iOS
Feature: Add edit button to input investment value for Set my Target for goal setting

  Background: Login
    Given Open WeLab App
#    And Enable skip Root
    Given Check upgrade page appears
    And Login with user and password
      | user     | test112  |
      | password | Aa123321 |
    And I can see the LoggedIn page

  @WELATCOE-729
  Scenario: user input amount for one time exceeds the max value (300k) - my target
    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target with name
      |name| targetValue | time |  oneTimeValue|   monthlyValue|
      |729 |             | 60   |              |               |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
    |name|
    |729 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-729
  Scenario: user input amount for monthly exceeds the max value (30k) - my target
    When I go to Wealth Page
    When I open the goal setting
    |name|
    |729 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      |              |   3000001  |
    Then I see the exceed max amount error message

  @WELATCOE-733
  Scenario: user input amount for one time investment exceeds the max value (300k) - investment habit
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit with name
      |name| oneTimeValue|   monthlyValue|  month  |
      |733 |             |               |    60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
      |name|
      |733 |
    And I edit investment plan for investing habit
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-733
  Scenario: user input amount for monthly investment exceeds the max value (30k) - investment habit
    When I go to Wealth Page
    When I open the goal setting
      |name|
      |733 |
    And I edit investment plan for investing habit
      | oneTimeValue |monthlyValue|
      |              |   30000001 |
    Then I see the exceed max amount error message

  @WELATCOE-737
  Scenario: user input amount for one time investment exceeds the max value (300k) - Achieve Financial Freedom
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom with name
      | name  | age | monthlyExpenses | otherExpenses   |  oneTimeValue | monthlyValue |
      |  737  | 60  |      5000       | ,,              |               |              |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
      |name|
      |737 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-737
  Scenario: user input amount for monthly investment exceeds the max value (30k) - Achieve Financial Freedom
    When I go to Wealth Page
    When I open the goal setting
      |name|
      |737 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      |              |   30000001 |
    Then I see the exceed max amount error message

