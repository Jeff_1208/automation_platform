package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.projects.wealth.pages.iao.ForeignExchangePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.ui.Sleeper;
import org.picocontainer.annotations.Inject;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

public class ForeignExchangeSteps {

  @Inject ForeignExchangePage foreignExchangePage;

  SoftAssertions softAssert = new SoftAssertions();


  @Given("I go to Foreign Exchange Page")
  public void openFXPage() {
    foreignExchangePage.clickHomeForeignExchange();
  }

  @And("I enter amount {}")
  public void iEnterAmount(String amount) {
    foreignExchangePage.enterAmount(amount);
  }

  @And("I check slide button disabled")
  public void iCheckSlideBtn() {
    Boolean status = foreignExchangePage.slideToConfirmStatus();
    assertThat(status).isFalse();
  }

  @And("I check WealthBalance")
  public void iCheckWealthBalance() throws InterruptedException {
    Boolean status = foreignExchangePage.checkWealthBalance();
    assertThat(status).isTrue();
  }

  @And("I check TotalBalanceArrow")
  public void iCheckTotalBalanceArrow() throws InterruptedException {
    Boolean status = foreignExchangePage.checkTotalBalanceArrow();
    assertThat(status).isTrue();
  }

  @And("I click TotalBalanceArrow")
  public void iClickTotalBalanceArrow() {
    foreignExchangePage.clickTotalBalanceArrow();
  }

  @And("I check Success Done")
  public void iCheckSuccesssDone() throws InterruptedException {
    Boolean status = foreignExchangePage.checkSuccesssDone();
    assertThat(status).isTrue();
  }

  @And("I click Success Done")
  public void iClickSuccesssDone() {
    foreignExchangePage.clickSuccesssDone();
  }

  @And("I click slide button")
  public void iClickSlideBtn() {
    foreignExchangePage.clickImportantNotes();
  }

  @And("I slide confirm button")
  public void iSlideConfirmBtn() {
    foreignExchangePage.setSlideToConfirm();
  }

  @And("I check review button displayed")
  public void iCheckReviewBtn() throws InterruptedException {
    Boolean status = foreignExchangePage.reviewBtnstatus();
    assertThat(status).isTrue();
  }

  @And("I check the Foreign Exchange Show")
  public void iCheckgetForeignExchangeShow() {
    Boolean foreignExchangeStatus = foreignExchangePage.getForeignExchangeStatus();
    assertThat(foreignExchangeStatus).isFalse();
  }

  @And("I check the Foreign Exchange Not Show")
  public void iCheckgetForeignExchangeNotShow() {
    Boolean foreignExchangeStatus = foreignExchangePage.getForeignExchangeStatus();
    assertThat(foreignExchangeStatus).isFalse();
  }

  @And("I check the Add Money Show")
  public void iCheckgetAddMoneyShow() {
    Boolean foreignExchangeStatus = foreignExchangePage.getAddMoneyStatus();
    assertThat(foreignExchangeStatus).isTrue();
  }
  @And("I entered the foreign exchange page")
  public void iEnteredTheForeignExchangePage() {
    foreignExchangePage.clickForeignExchangeBtn();
  }
  @And("The entered amount is greater than the available balance")
  public void theEnteredAmountIsGreaterThanTheAvailableBalance() {
  foreignExchangePage.inputYouConvert();
  }
  @And("I saw the prompt")
  public void iSawThePrompt() {
    Boolean GreaterThanAvailable =foreignExchangePage.prompt();
    assertThat(GreaterThanAvailable).isTrue();
  }
  @And("I check review button displayed for {} mins")
  public void iCheckReviewButtonDisplayedForNumberMins (String time) throws ParseException, InterruptedException {
    assertThat(foreignExchangePage.enRouteInput("",time)).isTrue();
  }
  @And("I clicked the review button")
  public void iClickedTheReviewButton(){
    foreignExchangePage.clickReview();
  }
  @And("I want to enter the ([^\"]\\S*) while waiting for ([^\"]\\S*) minutes")
  public void iWantToEnterTheAmountWhileWaitingForNumberMinutes(String  amount ,String time)
          throws ParseException, InterruptedException {
    assertThat(foreignExchangePage.enRouteInput(amount,time)).isTrue();
  }
  @And("I choose the ([^\"]\\S*)$")
  public void iChooseTheCurrency(String currency) {
    foreignExchangePage.clickDropDownBox(currency);
  }
  @And("I enter the ([^\"]\\S*)$")
  public void iEnterTheMoney(String money) {
    foreignExchangePage.enterAmount(money);
  }
  @And("I submitted my foreign exchange order")
  public void iSubmittedMyForeignExchangeOrder() {
    foreignExchangePage.setSlideToConfirm();
  }
  @And("I saw the submitted order")
  public void iSawTheSubmittedOrder() {
    Boolean SubmittedOrder=foreignExchangePage.transactionStatus();
    assertThat(SubmittedOrder).isTrue();
  }
  @And("I saw the order with successful conversion")
  public void iSawTheOrderWithSuccessfulConversion(){
    assertThat(foreignExchangePage.whether()).isTrue();
  }
  @And("I went to the balance details to check the converted amount")
  public void iWentToTheDalanceBetailsToCheckTheConvertedAmount(){
    foreignExchangePage.clickBalanceDetails();
  }
  @And("I clicked USB balance")
  public void iClickeUsdBalance(){
    foreignExchangePage.clickBalance();
  }
  @And("I saw the order information")
  public void iSawTheOrderInformation(){
    foreignExchangePage.balanceUsd();
  }
}
