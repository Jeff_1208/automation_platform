package com.welab.automation.projects.wealth.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;

public class LoginPage extends AppiumBasePage {

  private String pageName = "Login Page";
  private static final String cancelText = "Cancel";
  private int customWait = 60;
  private int cancelMessageWait = 5;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Login'")
  private MobileElement loginTxt;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Got it']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Got it'")
  private MobileElement gotIt;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Username']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Username'")
  private MobileElement userNameTxt;

  @AndroidFindBy(accessibility = "username-input")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Username'")
  private MobileElement userNameInput;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Password']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Password'")
  private MobileElement passwordTxt;

  @AndroidFindBy(accessibility = "password-input")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Password'")
  private MobileElement passwordInput;

  @AndroidFindBy(xpath = "//*[@text='Welcome back']")
  private MobileElement welcomeBack;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Login'")
  private MobileElement loginButton;

  @AndroidFindBy(accessibility = "GoWealth, tab, 5 of 5")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoWealth, tab, 5 of 5'")
  private MobileElement wealthTab;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
  private MobileElement notification;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement next;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start Testing'")
  private MobileElement start;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Ask App not to Track'")
  private MobileElement noTracking;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Total balance')]")
  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Total balance')]")
  private MobileElement totalBalance;

  public LoginPage() {
    super.pageName = pageName;
    if (System.getProperty("mobile").equalsIgnoreCase("cloudios"))
      ((AppiumDriver<MobileElement>) BaseDriver.getMobileDriver()).activateApp("welab.bank.sit");
  }

  public void clickContactWarningLogin() throws InterruptedException {
    if (isElementDisplayed(contactWarningCancel, cancelText, cancelMessageWait)) {
      waitUntilElementClickable(contactWarningCancel);
      contactWarningCancel.click();
    }
    if (isElementVisible(wealthTab, customWait)) {
      Thread.sleep(1500);
      if (isElementDisplayed(contactWarningCancel, cancelText, cancelMessageWait)) {
        waitUntilElementClickable(contactWarningCancel);
        contactWarningCancel.click();
      }
    }
  }

  public String getLoggedInPageText() {
    // if (isElementDisplayed(debitCard).isDisplayed()){
    if (isElementDisplayed(totalBalance) != null) {
      return totalBalance.getText();
    }
    ;
    return "Can't find total balance element.";
  }

  public void openWealthPage() {
    waitUntilElementClickable(wealthTab);
    wealthTab.click();
  }

  public void loginWithCredential(String user, String password) {
    logger.info("Login with credential: " + user + ", " + password);
    clickElement(userNameTxt);
    clearAndSendKeys(userNameInput, user);
    if (System.getProperty("mobile").contains("android")) hideKeyboard();
    clickElement(passwordTxt);
    clearAndSendKeys(passwordInput, password);
    if (System.getProperty("mobile").contains("android")) hideKeyboard();
    clickElement(loginButton);
  }

  @SneakyThrows
  public String getLoginIconText() {
    if (System.getProperty("mobile").contains("ios")) {
      Thread.sleep(1000 * 5);
    }
    waitUntilElementVisible(loginTxt);
    return loginTxt.getText();
  }

  public String getLoginButtonText() {
    waitUntilElementVisible(loginButton);

    return totalBalance.getText();
  }
}
