package com.welab.automation.projects.wealth.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = {
                //
                //"src/main/java/com/welab/automation/projects/wealth/features/web/VerifyPagination.feature",
                "src/main/java/com/welab/automation/projects/wealth/features/web/ApplicationStatus.feature"
        },
        glue = {
                //"com/welab/automation/projects/wealth/steps/mobile",
                "com/welab/automation/projects/wealth/steps/web"
        },
        tags = "@WELATCOE-582",
        monochrome = true)


public class AdminPortalTestRunner_web extends TestRunner {

}
