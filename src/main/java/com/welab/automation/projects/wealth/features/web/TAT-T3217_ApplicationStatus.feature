Feature: Maker or Checker check status alteration

  Background: There are submitted document records


  @WELATCOE-579
  Scenario: Agree application by Checker which approved by Maker

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is agree decision of maker
    And checker agreed record in not Maker or Checker's approval list



  @WELATCOE-580
  Scenario: Disagree application by Checker which approved by Maker

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is disagree decision of maker
    And checker disagreed record in Maker but not Checker is approval list


  @WELATCOE-582
  Scenario: Agree application by Checker which rejected by Maker

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is agree decision of maker
    And checker agreed record in not Maker or Checker's approval list

  @WELATCOE-581
  Scenario: Submit application which is already submitted by another Checker

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check 'View' button of any maker record and click 'Agree' or 'Disagree', input remark
    Given Open a new TAB
    Then repeat click agree,input remark and click submit button
    And Switch back Checker A, click 'Submit' button

  @WELATCOE-583
  Scenario: Disagree application by Checker which rejected by Maker

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is disagree decision of maker
    And checker disagreed record in Maker but not Checker is approval list


  @WELATCOE-584
  Scenario: Maker/Checker reject again when Checker disagree Maker's decision the first time

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application
    And logout admin portal
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject pass T24ID the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is agree decision of maker
    And checker agreed record in not Maker or Checker's approval list

  @WELATCOE-585
  Scenario: Maker/Checker approve again when Checker disagree Maker's decision the first time

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application
    And logout admin portal
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And pass T24ID approve the application
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And check uploaded documents and Checker is agree decision of maker
    And checker agreed record in not Maker or Checker's approval list


  @WELATCOE-587 @AppNotCompleted
  Scenario Outline: Verify investment account is not opened if uploaded document is rejected
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Examples:
      | username |
      | test107  |

  @WELATCOE-588 @AppNotDebugging
  Scenario Outline: Verify investment account is opened if uploaded document is approved
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>c
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Examples:
      | username |
      | test107  |


