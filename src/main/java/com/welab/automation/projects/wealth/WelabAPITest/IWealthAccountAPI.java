package com.welab.automation.projects.wealth.WelabAPITest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.Get;
import com.welab.automation.framework.common.annotation.Server;
import io.restassured.response.Response;

@Server(GlobalVar.WEALTH_PATH)
public interface IWealthAccountAPI {
    @Get(path="/account",description="Fetch Basic Account Status")
    Response getAccountStatus();
}
