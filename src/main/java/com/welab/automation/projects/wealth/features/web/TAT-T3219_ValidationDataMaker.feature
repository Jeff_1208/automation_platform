Feature: wealth
  Background: Open wealth.portal
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/

    @WELATCOE-572
  Scenario Outline: Approve application which has documents to be validated by Maker
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I click on the Approval List
    And I click on the maker
    And I Click on the first row of data
    And My approved order <time> <result>
    And I found the approved order
    Examples:
      | time       |result|
      | 2032-2-17  |Valid|

  @573
  Scenario Outline: Reject application which has documents to be validated by Maker
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I click on the Approval List
    And I click on the maker
    And I Click on the first row of data
    And I approve rejected orders <time> <result>
    And I found the approved order
    Examples:
      | time       |result|
      | 2032-2-17  | Invalid|


  @571
  Scenario:  Verify Maker's detail view page
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I goto approval maker
    And I Click on the first row of data
    And I verified the display data of the audit page

    @WELATCOE-570
  Scenario: Verify sorting on Maker's approval list columns
      And login keycloak
        | username | ops1 |
        | password | ops1 |
      And I click on the Approval List
      And I verify header of Checker's approval list page
      And I Click on the first row of data
      And I verify the columns of Checker's approval list table

  @WELATCOE-575
  Scenario:Verify maked approval records in Checker's approval list
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I goto approval maker
    And verify header of Maker's approval list page
    And I verify the columns of Maker's approval list table
    And I click on the Approval List
    And I verify header of Checker's approval list page
    And I open the approved order

  @WELATCOE-574
  Scenario Outline: Submit application which is already submitted by another Maker
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I click on the Approval List
    And I click on the maker
    And I Click on the first row of data
    And I operated in the current browser and entered the <time> <result>
    And I opened multiple browsers to operate the same order <time> <result>
    Examples:
      | time       |result|
      | 2032-2-17  |Valid|

