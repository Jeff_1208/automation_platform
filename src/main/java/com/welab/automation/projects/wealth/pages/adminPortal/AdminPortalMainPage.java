package com.welab.automation.projects.wealth.pages.adminPortal;

import com.welab.automation.framework.base.WebBasePage;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class AdminPortalMainPage extends WebBasePage {

  private String pageName = "admin portal main page";

  @FindBy(how = How.CSS, using = "li[class='menu-item ng-tns-c134-1 ng-star-inserted']")
  private WebElement approvalListBtn;

  @FindBy(how = How.CSS, using = "div[class='user-name ng-star-inserted']")
  private WebElement accountBtn;

  @FindBy(how = How.XPATH, using = "//span[text()='Log out']")
  private WebElement logoutBtn;

  @FindBy(
          how = How.CSS,
          using = "li[class='menu-item ng-tns-c134-2 ng-tns-c134-1 ng-star-inserted']")
  private WebElement makerBtn;

  @FindBy(
          how = How.CSS,
          using = "li[class='menu-item ng-tns-c134-3 ng-tns-c134-1 ng-star-inserted']")
  private WebElement checkerBtn;

  @FindBy(
          how = How.XPATH,
          using = "//span[contains(text(),\"Investment Account Summary\")]"
  )
  private WebElement summaryMenu;

  @Getter
  @FindBy(
          how = How.XPATH,
          using = "//div/nb-card/nb-card-header"
  )
  private WebElement confirmIntoSummary;

  @FindBy(
          how = How.XPATH,
          using = "//tr[@class='ng-star-inserted']"
  )
  private List<WebElement> defaultListLink;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li/a"
  )
  private List<WebElement> paginationBottomList;


  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[1]/a"
  )
  private WebElement previousButton;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[2]/a"
  )
  private WebElement firstPage;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[7]/a"
  )
  private WebElement threePointIndex;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[6]/a"
  )
  private WebElement fifthPage;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[8]/a"
  )
  private WebElement forEachPage;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[2]/a/span"
  )
  private WebElement firstCurrentPage;

  @FindBy(
          how = How.XPATH,
          using = "//ngb-pagination/ul/li[9]/a"
  )
  private WebElement nextButton;

  @FindBy(how = How.CSS,
          using = "li[class='menu-item ng-tns-c134-4 ng-star-inserted']")
  private WebElement SummaryBtn;


@FindBy(
        how = How.XPATH,
        using = "//ngb-pagination/ul/li[3]/a"
)
private WebElement secondPage;

  public AdminPortalMainPage() {
    super.pageName = this.pageName;
  }

  @SneakyThrows
  public void gotoApprovalMaker() {
    clickElement(approvalListBtn);
    waitUntilElementClickable(makerBtn);
    clickElement(makerBtn);
  }

  @SneakyThrows
  public void gotoApprovalChecker() {
    clickElement(approvalListBtn);
    waitUntilElementClickable(checkerBtn);
    clickElement(checkerBtn);
  }

  public void logout() {
    waitUntilElementClickable(accountBtn);
    clickElement(accountBtn);
    waitUntilElementClickable(logoutBtn);
    clickElement(logoutBtn);
  }

  public void clickSummaryMenu() {
    waitUntilElementClickable(summaryMenu);
    clickElement(summaryMenu);
  }

  @SneakyThrows
  public boolean checkDefaultPagination() {
    Thread.sleep(3 * 1000);
    int defaultList = defaultListLink.size();
    return defaultList == 20;
  }

  public boolean checkFirstPreviousPage() {
    waitUntilElementClickable(firstPage);
    clickElement(firstPage);
    return isElementClickable(By.xpath("//ngb-pagination/ul/li[1]/a"));
  }

  @SneakyThrows
  public boolean checkPreviousPage() {
    int paginationBottomNumber = paginationBottomList.size() - 1;
    int defaultList = 0;
    for (int i = 3; i <= paginationBottomNumber; i++) {
      if (i != 5 && i != 6 && i != 7) {
        clickElement(driver.findElement(By.xpath("//ngb-pagination/ul/li[" + i + "]/a")));
        Thread.sleep(2 * 1000);
        clickElement(previousButton);
        Thread.sleep(2 * 1000);
        defaultList = defaultListLink.size();
        if (!verifyElementExist(driver.findElement(By.xpath("//ngb-pagination/ul/li[" + (i - 1) + "]/a/span"))) || defaultList != 20) {
          break;
        }
      }
    }
    return verifyElementExist(driver.findElement(By.xpath("//ngb-pagination/ul/li[" + (paginationBottomNumber - 1) + "]/a/span"))) &&  defaultList == 20;
  }

  @SneakyThrows
  public boolean checkNextButton() {
    clickElement(firstPage);
    Thread.sleep(2*1000);
    int paginationBottomNumber = paginationBottomList.size() - 1;
    for (int i = 2; i <= paginationBottomNumber; i++) {
      if (i != 5 && i != 6 && i != 7) {
        if (isElementClickable(By.xpath("//ngb-pagination/ul/li[9]/a"))){
          clickElement(nextButton);
          Thread.sleep(2*1000);
          if (i == 8) {
            break;
          }
          if (!verifyElementExist(driver.findElement(By.xpath("//ngb-pagination/ul/li[" + (i + 1) + "]/a/span")))) {
            break;
          }
        }else
          break;
      }
    }
    return verifyElementExist(driver.findElement(By.xpath("//ngb-pagination/ul/li[" + paginationBottomNumber + "]/a/span")));
  }

  @SneakyThrows
  public boolean checkSpecificPage() {
    clickElement(firstPage);
    Thread.sleep(2*1000);
    clickElement(fifthPage);
    int defaultList = 0;
    int paginationBottomNumber = paginationBottomList.size() ;
    for (int i = 4; i < paginationBottomNumber; i++) {
        clickElement(forEachPage);
        Thread.sleep(2*1000);
        defaultList = defaultListLink.size();
        if (!verifyElementNotExist(By.xpath("//nagination/ul/li["+ (8-i) +"]/a/span")) && defaultList != 20) {
          break;
      }
    }
    return verifyElementNotExist(By.xpath("//ngb-pagination/ul/li[4]/a/span")) && defaultList == 20;
  }

  public boolean checkThreeDots() {
    clickElement(firstPage);
    String lastPageText = forEachPage.getText();
    int lastPageNumber = Integer.parseInt(lastPageText);
    if (lastPageNumber >=10) {
      return threePointIndex.getText().equals("...");
    }
    return false;
  }
  public void gotoSummaryPage(){
    waitUntilElementClickable(SummaryBtn);
    clickElement(SummaryBtn);
  }
  @SneakyThrows
  public boolean checkApprovalSpecificPage() {
    clickElement(firstPage);
    clickElement(secondPage);
    boolean flag = !isElementClickable(By.xpath("//span[contains(text(),\\\"(current)\\\")]"),4);
    return flag&&verifyContainsText(secondPage,"2",true);
  }
  public boolean verifyThreeDots() {
    clickElement(firstPage);
    if (paginationBottomList.size() >=10) {
      return threePointIndex.getText().equals("...");
    }else {
      return !verifyContainsText(threePointIndex,"...",true);
    }
  }
}