package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import com.welab.automation.projects.wealth.WelabAPITest.VerifyUiAndApiGoalSetting;
import com.welab.automation.projects.wealth.pages.LoginPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingOrderPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingToolTipsPage;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import org.assertj.core.api.SoftAssertions;
import org.picocontainer.annotations.Inject;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import static com.welab.automation.framework.GlobalVar.TARGET_SCREENSHOTS_PATH;
import static com.welab.automation.framework.utils.comparison.model.ImageComparisonState.MATCH;
import static org.assertj.core.api.Assertions.assertThat;


public class VerifyUiAndApiGoalSettingSteps {

    @Inject LoginPage loginPage;
    @Inject GoalSettingPage goalSettingPage;
    @Inject GoalSettingToolTipsPage goalSettingToolTipsPage;
    @Inject GoalSettingOrderPage goalSettingOrderPage;

    SoftAssertions softAssert = new SoftAssertions();
    private static final String quitWarningString = "Are you sure you want to quit?";
    private static final String reviewAndConfirm = "Review and confirm order";
    private static final String wealthTitle = "Wealth";
    private Response response;
    private boolean isUploadFile = false;

    @When("^I click Build My Investing Habit and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void modifyInvestingHabitGoal(String oneTimeValue, String monthlyValue, String month ,String name, String password)
            throws Exception {
        goalSettingPage.setInvestingHabitGoal(oneTimeValue, monthlyValue, month);
        assertThat(goalSettingPage.getBIMAgeSliderText())
                .isEqualTo(goalSettingPage.formatInvestTime(month));
        goalSettingPage.saveGoal();
        assertThat(goalSettingPage.getText()).isEqualTo(true);
        String successTex=goalSettingPage.getSuccessRate();
        Map<String, String> apiGetYearAge= VerifyUiAndApiGoalSetting.getYearAge(name,password);

        DecimalFormat myformat=new java.text.DecimalFormat("0");
        int age= Integer.parseInt(apiGetYearAge.get("age"));
        age=(Integer.parseInt(month)-age)*12;
        BigDecimal numAge = new BigDecimal(age);
        BigDecimal numTimeValue = new BigDecimal(oneTimeValue);
        BigDecimal numMonthlyValue = new BigDecimal(monthlyValue);
        BigDecimal result = numMonthlyValue.multiply(numAge);
        BigDecimal target = numTimeValue.add(result);
        Map<String, String> valueExpression =VerifyUiAndApiGoalSetting.getSuccessRate(name,password,month,oneTimeValue,monthlyValue, myformat.format(target));
        Boolean successRate=goalSettingPage.Comfortable(valueExpression.get("successRate"),successTex);
        assertThat(successRate).isEqualTo(true);
    }
    @When("^I click Achieve Financial Freedom set and ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void addSetFinancialFreedomGoal(String age, String monthlyExpenses, String otherExpenses,String name,String password)
            throws Exception {
        goalSettingPage.setFinancialFreedomGoal(age);
        assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
        goalSettingPage.clickContinue();
        goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
        goalSettingPage.calculateTargetWealth();
        String monthlyPayment= goalSettingPage.getMonthlyInvestment();
        String downPayments = goalSettingPage.getInvestmentPlan();
        String TargetWealth=goalSettingPage.geTargetWealth();
        String strtrim =TargetWealth.replaceAll("[，。,、_—— ^|~！@#￥;；：=<><><>:【》‘“”、】\"\"《\\?？：。，：{}%……&\\ A-Za-z]","").trim();
        BigDecimal result = new BigDecimal(strtrim);
        String str=TargetWealth.replaceAll("[，。,、._—— ^|~！@#￥;；：=<><><>:【》‘“”、】\"\"《\\?？：。，：{}%……&\\ 0-9]","").trim();
        if (str!=null) {
            if (str.equals("M")) {
                BigDecimal num = new BigDecimal(1000000);
                BigDecimal strtNum = new BigDecimal(strtrim);
                result = num.multiply(strtNum);
            }else if (str.equals("K")) {
                BigDecimal num = new BigDecimal(1000);
                BigDecimal num22 = new BigDecimal(strtrim);
                result = num.multiply(num22);
            }
        }
        DecimalFormat myformat=new java.text.DecimalFormat("0");
        String strResult = myformat.format(result);
        String   downPaymentsTrim=downPayments.replaceAll("[，。,、_—— ^|~！@#￥;；：=<><><>:【》‘“”、】\"\"《\\?？：。，：{}%……&\\ A-Za-z]","").trim();
        monthlyPayment=monthlyPayment.replaceAll("[，。,、_—— ^|~！@#￥;；：=<><><>:【》‘“”、】\"\"《\\?？：。，：{}%……&\\ A-Za-z]","").trim();
        goalSettingPage.saveGoal();
        goalSettingPage.clickYesBtn();
        String successTex=goalSettingPage.getSuccessRate();
        Map<String, String> valueExpression =VerifyUiAndApiGoalSetting.getSuccessRate(name,password,age,downPaymentsTrim,monthlyPayment, strResult);
        Boolean successRate=goalSettingPage.Comfortable(valueExpression.get("successRate"),successTex);
        assertThat(successRate).isEqualTo(true);
    }
    @When("^I click set target and set value ([^\"]\\S*) and ([^\"]\\S*) ([^\"]\\S*) ([^\"]\\S*)$")
    public void addTargetGoal(String targetValue, String time, String name, String password)
            throws Exception {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
        goalSettingPage.setTargetGoal(targetValue, time);
        assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
        assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
        goalSettingPage.clickContinue();
        String downPayments = goalSettingPage.getInvestmentPlan();
        String monthlyPayment= goalSettingPage.getMonthlyInvestment();
        goalSettingPage.saveGoal();
        downPayments=downPayments.replaceAll("[，。,、_—— ^|~！@#￥;；：=<><><>:【》‘“”、】\"\"《\\?？：。，：{}%……&\\ A-Za-z]","").trim();
        String successTex=goalSettingPage.getSuccessRate();
        Map<String, String> valueExpression =VerifyUiAndApiGoalSetting.getSuccessRate(name,password,time,downPayments,monthlyPayment,targetValue);
        Boolean successRate=goalSettingPage.Comfortable(valueExpression.get("successRate"),successTex);
        assertThat(successRate).isEqualTo(true);

    }

}
