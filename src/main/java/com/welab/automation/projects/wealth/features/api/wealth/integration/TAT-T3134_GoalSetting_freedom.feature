
Feature: GoalSetting_freedom
  Scenario Outline: <caseName>
    Given test description: GoalSetting_freedom "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|
    |GoalSetting_freedom|1|Get wealth balance|/v1/wealth/wealth-balance-data|Get||||200||{'data.currency':'equals(HKD)'}||data.currency||test019:Aa123321||
    |GoalSetting_freedom|2|Get the goal categories by language|/v1/wealth/goal-categories-templates|Get||||200||{'data.categoryStatus':'equals(ACTIVE)'}||||||
    |GoalSetting_freedom|3|Get max-horizon|/v1/wealth/max-horizon|Get||||200||{'data':'isNotNull'}||||||
    |GoalSetting_freedom|4|Tenor-horizon|/v1/wealth/tenor-to-horizon|Get||tenor=50||200||{'data':'isNotNull'}||||||
    |GoalSetting_freedom|5|Grid-finder|/v1/wealth/grid-finder|Get||target=788000&horizon=217&initInvt=500000&monthlySaving=700||200||{'data.defaultMonthlySaving':'isNotNull'}||data.defaultInitInvt,data.defaultMonthlySaving,data.defaultHorizon,data.target||||
    |GoalSetting_freedom|6|Get Matching portfolio|/v1/wealth/matching-portfolio-service|Get||initInvt=${data.defaultInitInvt}&monthlySaving=${data.defaultMonthlySaving}&horizon=${data.defaultHorizon}&target=${data.target}&minSuccessRate=40||200||{'data.portfolioCode':'equals(PTF000000003)','data.portfolioName':'contains(Conservative)'}||data.portfolioCode||||
    |GoalSetting_freedom|7|Create client goal|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_integration.json|200||{'data.id':'isNotNull','data.portfolioCode':'equals(${data.portfolioCode})'}||data.id||||
    |GoalSetting_freedom|8|Get model portfolio details by mpId|/v1/wealth/model-portfolio-details/${data.portfolioCode}|Get||||200||{'data.recommendFunds.productCode[0]':'isNotNull'}||data.recommendFunds.productCode[0]||||
    |GoalSetting_freedom|9|Get Projection|/v1/wealth/model-portfolio-projection|Get||portfolioCode=${data.portfolioCode}&initInvt=${data.defaultInitInvt}&monthlySaving=${data.defaultMonthlySaving}&horizon=${data.defaultHorizon}&upper=100&lower=20&section=20||200||{'data':'arraySizeEquals(20)'}||||||
    |GoalSetting_freedom|10|Get fund detail with product code and language|/v1/wealth/fund-details/${data.recommendFunds.productCode[0]}|Get|{Accept-Language=en-US}|||200||{'data.productCode':'${data.recommendFunds.productCode[0]}'}||||||
    |GoalSetting_freedom|11|Get settlement-date|/v1/wealth/settlement-date/${data.portfolioCode}|Get||||200||{'data':'isNotNull'}||||||
    |GoalSetting_freedom|12|Get subscription dates|/v1/wealth/subscription-date|Get|{Accept-Language=en-US}|tenor=28||200||{'data.monthlyOrderDateOrdinal':'isNotNull'}||||||
    |GoalSetting_freedom|13|Get model portfolio fee by mpId|/v1/wealth/model-portfolio-details/${data.portfolioCode}/fee|Get||||200||{'data':'isNotNull'}||||||
    |GoalSetting_freedom|14|Get portfolio docs by portfolio code|/v1/wealth/portfolio-docs/${data.portfolioCode}|Get|{Accept-Language=en-US}|||200||{'data.productCode[0]':'isNotNull'}||||||
    |GoalSetting_freedom|15|Update boost up|/v1/wealth/client-goals/${data.id}/boost-up|Post|||Welab/body/Update_boost_up.json|200||{'data.goalCcyCode':'${data.currency}','data.portfolioCode':'equals(${data.portfolioCode})'}||data.id||||
    |GoalSetting_freedom|16|Order placement|/v1/wealth/order-placement|Post|{Accept-Language=en-US}||Welab/body/Order_placement.json|200||{'data.success':'equals(true)'}||data.referenceNumber||||
    |GoalSetting_freedom|17|Get standing instructions|/v1/wealth/standing-instructions|Get|{Accept-Language=en-US}|||200||{'data':'contains(${data.portfolioCode})'}||||||
    |GoalSetting_freedom|18|Get Client orders|/v1/wealth/client-orders|Get|{Accept-Language=en-US}|clientGoalId=${data.id}||200||{'data':'contains(${data.referenceNumber})'}||||||
