Feature: verify APP UI when user complete application and maker&checker took different action on admin portal

  Background: Open Admin Portal
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/

  @WELATCOE-586
  Scenario Outline: BasePage Demo
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application again of <username>
    And click submit button
    And logout admin portal
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application again of <username>
    And verify only show last Maker or Checker record
    And click submit button
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application again of <username>
    And verify only show last Maker or Checker record
    And click submit button
    Examples:
      | username |
      | test117  |
