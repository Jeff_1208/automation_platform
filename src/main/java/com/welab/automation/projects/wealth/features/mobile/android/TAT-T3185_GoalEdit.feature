Feature: Goal edit  different trigger or switch states to check the order

  Background: Login with correct user and password
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And Login with user and password
      | user     | test188  |
      | password | Aa123321 |
    And I can see the LoggedIn page

  @WELATCOE-613
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Set My Targe
    When I go to Wealth Page
    And  I click the order of goal  <targeName> with the status of on track or off track
    And  I click edit Button and update  monthly investment plan <targeValue>
    Then I click View updated recommendation
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeValue |
      | ReachA    | 10500      |


  @WELATCOE-615
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Build my investing Habit
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click Build My Investment Habit edit Button and update  monthly investment plan <targeTime>
    Then I click View updated recommendation
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeTime |
      | BuildB    | 48        |


  @WELATCOE-617
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Achieve Financial Freedom
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click edit Button and update  monthly investment plan <targeValue>
    Then I click View updated recommendation
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeValue |
      | AchieveC  | 82500      |


  @WELATCOE-619
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $ no change in monthly investment and horizon - Set My Target
    When I go to Wealth Page
    And  I click the order of goal  <targeName> with the status of on track or off track
    And  I click edit Button
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review my goal detail page <targeName>
    Examples:
      | targeName |
      | ReachA    |

  @WELATCOE-621
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $ no change in monthly investment and horizon - Build my investing Habit
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click edit Button
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review my goal detail page <targeName>
    Examples:
      | targeName |
      | BuildB    |

  @WELATCOE-623
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $  change in monthly investment and horizon - Achieve Financial Freedom
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click edit Button
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review my goal detail page <targeName>
    Examples:
      | targeName |
      | AchieveC  |


  @WELATCOE-626
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $  change in monthly investment and horizon - Set My Target
    When I go to Wealth Page
    And  I click the order of goal  <targeName> with the status of on track or off track
    And  I click edit Button and update  monthly investment plan <targeValue>
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeValue |
      | ReachA    | 10500      |


  @WELATCOE-628
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $  change in monthly investment and horizon - Build my investing Habit
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click Build My Investment Habit edit Button and update  monthly investment plan <targeTime>
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeTime |
      | BuildB    | 48        |


  @WELATCOE-630
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $  change in monthly investment and horizon - Achieve Financial Freedom
    When I go to Wealth Page
    And  I click the order of goal <targeName> with the status of on track or off track
    And  I click edit Button and update  monthly investment plan <targeValue>
    Then I click View updated recommendation
    And  I click keep existing one and confirm it
    And  I review and confirm order util look next button
    Examples:
      | targeName | targeValue |
      | AchieveC  | 82500      |

  @WELATCOE-607
  Scenario Outline: No new portfolio is recommended when goal parameters fall outside feasible range
    When I go to Wealth Page
    And I click Goal of Achieve Financial
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click confirm button and slide to confirm order
    Examples:
      | monthlyValue |
      | 20000        |

  @WELATCOE-662
  Scenario Outline: No  portfolio change when no rebalancing and no change in monthly investment - Set My Target
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit button at Goal Page
    And I click View update recommendation
    And I click YES button
    And I click OK button
    Examples:
      | targetName |
      | ReachBBF   |
#  not reusable
  @WELATCOE-605
  Scenario Outline: No new portfolio is recommended when goal parameters fall outside feasible range - Set My Target (Case 2a)
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    Examples:
      | targetName | monthlyValue |
      | ReachBBF   | 50000        |

  @WELATCOE-664
  Scenario Outline:No  portfolio change when no rebalancing and no change in monthly investment - Achieve Financial Freedom (Case 7a)
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit button at Goal Page
    And I click View update recommendation
    And I click YES button
    And I click OK button
    Examples:
      | targetName |
      | AchieveGGC |
#  not reusable
  @WELATCOE-668
  Scenario Outline:No  portfolio change when no rebalancing but there are any changes in monthly investment - Set My Target (Case 7b)
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click OK button
    Examples:
      | targetName | monthlyValue |
      | ReachMyBBF | 2600         |


  @WELATCOE-676
  Scenario Outline:Sell part of available balance_Set My Target
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value to over range
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName | SellValue |
      | ReachA   | 100       |


  @WELATCOE-707.1
  Scenario Outline: View Processing of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  Verify orders by dates from newest to oldest
    And  Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance
    And  I click any fund to verify fund documents
    Examples:
      | orderStatus |
      | Processing  |

  @WELATCOE-707.2
  Scenario Outline: View Ontrack of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  Verify orders by dates from newest to oldest
    And  Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance
    And  I click any fund to verify fund documents
    Examples:
      | orderStatus |
      | Ontrack     |

  @WELATCOE-707.3
  Scenario Outline: View Offtrack of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  Verify orders by dates from newest to oldest
    And  Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance
    And  I click any fund to verify fund documents
    Examples:
      | orderStatus |
      | Offtrack    |

  @WELATCOE-707.4
  Scenario Outline: View Achieved of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  Verify orders by dates from newest to oldest
    And  Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance
    And  I click any fund to verify fund documents
    Examples:
      | orderStatus |
      | Achieved    |

  @WELATCOE-707.5
  Scenario Outline: View Completed of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  Verify orders by dates from newest to oldest
    And  Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance
    And  I click any fund to verify fund documents
    Examples:
      | orderStatus |
      | Completed   |

  @WELATCOE-672
  Scenario Outline: No portfolio change when no rebalancing but have changes in monthly investment and/or horizon- Build My Investing Habit
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click OK button
    Examples:
      | targetName | monthlyValue |
      | BuildCCF   | 5000         |


  @WELATCOE-709.1
  Scenario Outline: View On track of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    And  I move eyeBtn to top right corner
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled

    Examples:
      | orderStatus | monthlyValue |
      | Ontrack     | 0            |

  @WELATCOE-709.2
  Scenario Outline: View Off track of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    And  I move eyeBtn to top right corner
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled

    Examples:
      | orderStatus | monthlyValue |
      | Offtrack    | 0            |

  @WELATCOE-709.3
  Scenario Outline: View Achieved of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    And  I move eyeBtn to top right corner
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled

    Examples:
      | orderStatus | monthlyValue |
      | Achieved    | 0            |

  @WELATCOE-709.4
  Scenario Outline: View Completed of order status
    When I go to Wealth Page
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    And  I move eyeBtn to top right corner
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled

    Examples:
      | orderStatus | monthlyValue |
      | Completed   | 0            |

  @WELATCOE-678
  Scenario Outline:Sell part of available balance_Build My investment Habit
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value to over range
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName   | SellValue |
      | AchieveTTC | 100       |

  @WELATCOE-680
  Scenario Outline:Sell part of available balance_Achieve Financial Freedom
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value to over range
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName | SellValue |
      | AC       | 100       |

  @WELATCOE-683
  Scenario Outline:Sell all the available balance_Set My Target
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName |
      | ReachBBF |

  @WELATCOE-685
  Scenario Outline:Sell all the available balance_Build My investment Habit
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName |
      | Build685 |

  @WELATCOE-687
  Scenario Outline:Sell all the available balance_Achieve Financial Freedom
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName   |
      | Achieve687 |

  @WELATCOE-670
  Scenario Outline:Goal Edit_No portfolio change when no rebalancing but there are any changes in monthly investment - Achieve Financial Freedom (Case 7b)
    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click OK button
    Examples:
      | targetName | monthlyValue |
      | AchieveAA  | 65000        |


  @WELATCOE-711
  Scenario Outline: Goal Edit_Edit the Target & Horizon when previous age <= current age <= retire age - Achieve Financial Freedom
    When I pass interface get current age <user> and <pwd>
    Examples:
      | user    | pwd      |
      | test101 | Aa123321 |

  @WELATCOE-711
  Scenario Outline: Goal Edit_Edit the Target & Horizon when previous age <= current age <= retire age - Achieve Financial Freedom

    When I go to Wealth Page
    And  I click the order of goal <targetName> with the status of on track or off track
    And  I get one time investment value
    And  I click edit button at Goal Page
    Then I click  the button to re-edit the target
    When I verify Achieve Financial Freedom and set new <age>, <monthlyExpenses> and <otherExpenses>
    And  I  retieve monthly investment value
    And  I click View update recommendation

    Examples:
      | targetName | age | monthlyExpenses | otherExpenses        |
      | AchieveC   | 60  | 50000           | Budget,Modest,Deluxe |

  @WELATCOE-695
  Scenario Outline:Sell overflow_Reach My Target
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName |
      | ReachA   |

  @WELATCOE-697
  Scenario Outline:Sell overflow_Build My investment Habit
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName |
      | BuildB   |

  @WELATCOE-699
  Scenario Outline:Sell overflow_Achieve Financial Freedom
    When I go to Wealth Page
    And I click goal name with <GoalName>
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Sell All Button and cancel button
    And I click Sell All Button
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I only can see Target wealth
    Examples:
      | GoalName |
      | AchieveC |

