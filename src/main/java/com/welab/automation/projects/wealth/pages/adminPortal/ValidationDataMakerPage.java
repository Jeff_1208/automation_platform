package com.welab.automation.projects.wealth.pages.adminPortal;


import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.framework.driver.*;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ValidationDataMakerPage extends WebBasePage {
    private static final Logger logger = LoggerFactory.getLogger(ValidationDataMakerPage.class);
    private static RemoteWebDriver webDriver;
    private static RemoteWebDriver mobileDriver;
    private static URL url;
    List<String> listMaker = new ArrayList<>();

    @FindBy(how = How.XPATH, using = "//th[text()=' Maker ']")
    private WebElement  makerBtn;

    @FindBy(how = How.XPATH, using = "//button[@class=\"btn-success operationBtn green\"]")
    private List<WebElement>  success ;

    @FindBy(how = How.XPATH, using = "//button[@class='btn-success operationBtn green select-green']")
    private List<WebElement>  successSelect ;

    @FindBy(how = How.XPATH, using = "//button[@class=\"btn-danger operationBtn red select-red\"]")
    private List<WebElement>  danger ;

    @FindBy(how = How.ID, using = "username")
    private WebElement usernameEditBox;

    @FindBy(how = How.XPATH, using = "//tr[@class='headline']/th")
    private List<WebElement> headline;
    @FindBy(how = How.XPATH, using = "//tr[@class='headline']/th")
    private WebElement headLine;

    @FindBy(how = How.XPATH, using = "//a[@class=\"page-link ng-star-inserted\"]")
    private  List<WebElement>linkinserted;

    @FindBy(how = How.ID, using = "password")
    private WebElement passwordEditBox;

    @FindBy(how = How.XPATH,using = "//a[@class=\"button-link\"]")
    private List<WebElement> button;

    @FindBy(how = How.XPATH, using = "//*[@id=\"navbarSupportedContent\"]/ul/li")
    private WebElement loginTopBtn;

    @FindBy(how = How.ID, using = "kc-login")
    private WebElement loginBtn;

    @FindBy(how = How.XPATH, using = "//*[@title=\"Approval List\"]")
    private WebElement ApprovalList;

    @FindBy(how = How.XPATH, using = "//*[@href=\"/approval-list/checker\"]")
    private WebElement checker;

    @FindBy(how = How.XPATH, using = "//*[@href=\"/approval-list/maker\"]")
    private WebElement maker;

    @Getter
    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[3]")
    private List<WebElement> makerList;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[7]")
    private List<WebElement>  userList;
    @Getter
    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[4]")
    private List<WebElement>  EnglishNameList;
    @Getter
    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[8]")
    private List<WebElement>  dateList;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"headline\"]/th[7]")
    private WebElement  userBUT;

    @Getter
    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[2]")
    private List<WebElement>  T24custid ;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"][2]/th[1]")
    private WebElement View;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"][1]/th[1]")
    private WebElement firstView;

    @FindBy(how = How.XPATH, using = "//th[contains(text(), \" Submit datetime\")]")
    private WebElement SubmitDatetime;

    @FindBy(how = How.XPATH, using = "//*[contains(text(), \"Aceptar\")]")
    private WebElement aceptar;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), \"Number of files to be validated: \")]")
    private WebElement validatedNumber;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),\"Approve\")]")
    private WebElement  approveBut ;

    @FindBy(how = How.XPATH, using = "//input[@placeholder=\"mandatory if reject\"]")
    private WebElement  inputApprove ;

    @FindBy(how = How.XPATH, using = " //div[@class=\"doc-item ng-star-inserted\"][3]/div/form/table[2]/tr[1]/td[1]/button")
    private WebElement  validBut;

    @FindBy(how = How.XPATH, using = "//div[@class=\"doc-item ng-star-inserted\"][3]/div/form/table[2]/tr[1]/td[3]/div/div/input")
    private WebElement  issueDate;

    @FindBy(how = How.XPATH, using = "//button[@type=\"button\"]")
    private WebElement  Submit;

    @FindBy(how = How.XPATH, using = "//div[@class=\"doc-item ng-star-inserted\"]")
    private List<WebElement> item;

    @FindBy(how = How.XPATH,using = "//span[@class=\"menu-title ng-tns-c134-3\"]")
    private WebElement checkerBtn;

    @FindBy(how = How.TAG_NAME, using = "th")
    private List<WebElement> tableHeaders;

    @FindBy(how = How.CSS, using = "[aria-label=Next]")
    private WebElement nextBtn;

    // used to check if it is the last page.
    @FindBy(how = How.CSS, using = "li[class='page-item ng-star-inserted disabled']")
    private WebElement disabledNextBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='View']")
    private WebElement viewActionBtn;

    @FindBy(how = How.XPATH, using = "//button[text()=' Approve ']")
    private WebElement approveBtn;

    @FindBy(how = How.XPATH, using = "//button[text()=' Reject ']")
    private WebElement rejectBtn;

    @FindBy(how = How.CSS, using = "input[placeholder='mandatory if reject']")
    private WebElement makerRemarkEditBox;

    @FindBy(how = How.XPATH, using = "//button[text()=' Agree ']")
    private WebElement agreeBtn;

    @FindBy(how = How.XPATH, using = "//button[text()=' Disagree ']")
    private WebElement disagreeBtn;

    @FindBy(how = How.CSS, using = "input[placeholder='comment']")
    private WebElement checkerRemarkEditBox;

    // need to use list?
    @FindBy(how = How.XPATH, using = "//button[text()='Valid']")
    private WebElement validBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='Invalid']")
    private List<WebElement> invalidBtns;

    @FindBy(how = How.CSS, using = "input[nbinput]")
    private List<WebElement> rejectReasonEditBoxes;

    @FindBy(how = How.NAME, using = "dp")
    private WebElement issueDateEditBox;

    @FindBy(how = How.XPATH, using = "//button[text()='Submit']")
    private WebElement submitBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='< Back']")
    private WebElement backBtn;

    @FindBy(how = How.XPATH, using = "//table[@class='operationPanel']/tr[1]/td/button")
    private List<WebElement> refuseBtn;

    @FindBy(how = How.XPATH, using = "//div/nb-card/nb-card-header")
    private WebElement approvalListHeader;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"][1]/th[2]")
    private WebElement firstT24ID;
    @Getter
    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"][1]/th[4]")
    private WebElement firstName;
    @FindBy(how = How.XPATH, using = "//detail-record-info/div/div[1]/div[1]")
    private WebElement PersonalINFO;

    @SneakyThrows
    public void login(String username, String password) {
//    waitUntilElementVisible(loginTopBtn);
        Thread.sleep(2000);
        clearAndSendKeys(usernameEditBox, username);
        clearAndSendKeys(passwordEditBox, password);
        clickElement(loginBtn);
        Thread.sleep(2000);
    }

    public void ApprovalList(){
        clickElement(ApprovalList);
    }
    public void maker(){
        clickElement(maker);

    }
    public void clickSubmitDatetime(){
        clickElement(SubmitDatetime);

    }
    @SneakyThrows
    public void makerList() throws InterruptedException {
        Thread.sleep(30000);
        listMaker.clear();
        for(WebElement row : makerList){
            listMaker.add(row.getText());
        }
    }

    @SneakyThrows
    public void getUserList() throws InterruptedException {
        Thread.sleep(30000);
        listMaker.clear();
        Thread.sleep(30000);
        int a=linkinserted.size();
        for (int b=0;b<a;b++) {
            for(WebElement row : userList){
                listMaker.add(row.getText());
            }
        }
    }
    public void clickedMaker() throws InterruptedException {
        clickElement(userBUT);
        Thread.sleep(30000);
    }
    public void clickedMakerBTN() throws InterruptedException {
        clickElement(makerBtn);
        Thread.sleep(30000);
        clickElement(makerBtn);
        Thread.sleep(30000);
    }
    @SneakyThrows
    public boolean verificationUserList() throws InterruptedException {
        boolean flag=true;
        List<String> list = new ArrayList<>();
        int a=0;
        for(int i=1;i<userList.size();i++){
            a=i-1;
            if (i!=1) {
                if (userList.get(i).getText() !=userList.get(a).getText()){
                    if (list.contains(list.add(userList.get(a).getText()))){
                        return flag=false;
                    }else if (!list.contains(list.add(userList.get(a).getText()))){
                        list.add(userList.get(i).getText());
                        list.add(userList.get(a).getText());
                    }
                }
            }
        }
        return flag;
    }
    @SneakyThrows
    public boolean T24id(){
        boolean flag=false;
        Thread.sleep(30000);
        int a=linkinserted.size();
        for (int b=0;b<a;b++) {
            List<String> list = new ArrayList<>();
            for(WebElement row : T24custid){
                list.add(row.getText());
            }
            if (list.contains(listMaker.get(0))) {
                return flag=true;
            }
            if(list.contains(listMaker.get(0))==false) {
                clickElement(nextBtn);
                Thread.sleep(10000);
            }
            list.clear();
        }
        return flag;
    }
    public int getMaximumLists(){
        for (int b=1;b<=linkinserted.size();b++) {
            if (b==linkinserted.size()) {
                return Integer.parseInt(linkinserted.get(b).getText());
            }
        }
        return 1;
    }


    @SneakyThrows
    public boolean listBefore() throws ParseException, InterruptedException {
        Thread.sleep(3000);
        boolean than=true;
        for (int i = 0; i < listMaker.size(); i++) {
            int a=i;
            if (i!=0){
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy, h:m:s aa", Locale.ENGLISH);
                Date date = dateFormat.parse(listMaker.get(i));
                Date datestr = dateFormat.parse(listMaker.get(a-1));
                if (date.before(datestr)){
                    return than=false;
                }
            }
        }
        return than;
    }
    public boolean listAfter() throws ParseException, InterruptedException {
        Thread.sleep(3000);
        boolean than=true;
        for (int i = 0; i < listMaker.size(); i++) {
            int a=i;
            if (i!=0){
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy, h:m:s aa", Locale.ENGLISH);
                Date date = dateFormat.parse(listMaker.get(i));
                Date datestr = dateFormat.parse(listMaker.get(a-1));
                if (date.after(datestr)){
                    return than=false;
                }
            }
        }
        return than;
    }

    public void clickList() throws InterruptedException {
        listMaker.clear();
        listMaker.add(T24custid.get(1).getText());
        clickElement(View);
        Thread.sleep(3000);
    }
    public void clickApproveBut() throws InterruptedException {
        Thread.sleep(3000);
        clickElement(approveBut);
        if (inputApprove.getAttribute("value")==""){
            andSendKeys(inputApprove,"automation Approve");
        }

    }

    public void choseDate(String date,String Valid) throws InterruptedException {
        String numberText=validatedNumber.getText();
        numberText=numberText.replaceAll("[，。,、._—— ^|~！@#￥;；：=<><><>:【》‘“”、】《?？：。，：{}%……& a-z A-Z]","").trim();
        String[] str=numberText.split("/");
        clickAgree(date,driver, Integer.parseInt(str[1]),Valid);
    }

    public void clickAgree(String date,RemoteWebDriver webDriver,int frequency,String Valid) throws InterruptedException {
        int c = 1;
        for (int b = 0; b < frequency; b++) {
            int a = c + 1;
            String text = webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[1]/td[1]/button")).getAttribute("class");
            String invalidText = webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[1]/button")).getAttribute("class");
            if (text.equals("btn-success operationBtn green") && invalidText.equals("btn-danger operationBtn red")) {
                if (Valid.equals(webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[1]/button")).getText())) {
                    webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[1]/button")).click();
                    String time = webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[3]/input")).getAttribute("value");
                    if (time != "") {
                        webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[3]/input")).sendKeys(date);
                    }
                }
                if (Valid.equals(webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[1]/td[1]/button")).getText())) {
                    webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[1]/td[1]/button")).click();
                    String time = webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[1]/td[3]/div/div/input")).getAttribute("value");
                    if (time != "") {
                        webDriver.findElement(By.xpath("//div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[1]/td[3]/div/div/input")).sendKeys(date);
                    }
                }
            }
            if (c != frequency) {
                WebElement slideDownward = webDriver.findElementByXPath(" //div[@class='doc-item ng-star-inserted'][" + c + "]/div/form/table[2]/tr[2]/td[1]/button");
                ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView();", slideDownward);
            } else if (c == frequency) {
                return;
            }
        }
        ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(document.body.scrollHeight,0)");
    }

    public void reasonsForRfusal(String date) throws InterruptedException {
        List<WebElement>textSzie=refuseBtn;
        for ( WebElement row : textSzie){
            int c=1;
            String text=row.getAttribute("class");
            String invalidText=row.getAttribute("class");
            if (text.equals("btn-success operationBtn green") || text.equals("btn-success operationBtn green") & invalidText.equals("btn-danger operationBtn red")) {
                row.click();
                String time = webDriver.findElement(By.xpath("//nb-card-body[@card-padding=\"20rem\"]/div/doc-review-list/div/div[" + c + "]/div/form/table[2]/tr[1]/td[3]/div/div/input")).getAttribute("value");
                if (time != "") {
                    webDriver.findElement(By.xpath("//nb-card-body[@card-padding=\"20rem\"]/div/doc-review-list/div/div["+c+"]/div/form/table[2]/tr[2]/td[3]/input")).sendKeys(date);
                }
            }
            if (c!=textSzie.size()) {
                WebElement slideDownward= webDriver.findElementByXPath(" //div[@class='doc-item ng-star-inserted']["+c+"]");
                ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView();", date);
            }
            c++;
        }
        ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(document.body.scrollHeight,0)");
    }
    @SneakyThrows
    public void clickedSubmit() throws InterruptedException {
        scrollToTop("Submit");
        if (item.size()!=0) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollTo(0, 0)");
            driver.executeScript("document.querySelector('body > ngx-app > ngx-pages > ngx-layout > nb-layout > div > div > div > div > div > nb-layout-column > approval-detail-content > div > nb-card > nb-card-header > div.header > div.ng-star-inserted > button').removeAttribute('disabled')");
        }
        clickElement(submitBtn);
        Thread.sleep(3000);
        Alert altElement = driver.switchTo().alert();
        acceptAlert(altElement);
    }

    @SneakyThrows
    public boolean clickedReopen(String date,String Valid) throws InterruptedException {
        scrollToTop("Submit");
        if (item.size()!=0) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollTo(0, 0)");
            driver.executeScript("document.querySelector('body > ngx-app > ngx-pages > ngx-layout > nb-layout > div > div > div > div > div > nb-layout-column > approval-detail-content > div > nb-card > nb-card-header > div.header > div.ng-star-inserted > button').removeAttribute('disabled')");
        }
        initDrive();
        Thread.sleep(3000);
        webDriver.get(driver.getCurrentUrl());
        Thread.sleep(3000);
        webDriver.findElement(By.id("username")).sendKeys("ops1");
        webDriver.findElement(By.id("password")).sendKeys("ops1");
        webDriver.findElement(By.id("kc-login")).click();
        Thread.sleep(3000);
        webDriver.findElement(By.xpath("//button[contains(text(),\"Approve\")]")).click();
        webDriver.findElement(By.xpath("//input[@placeholder=\"mandatory if reject\"]")).sendKeys("automation Approve");
        clickElement(submitBtn);
        Thread.sleep(3000);
        Alert altElement = driver.switchTo().alert();
        acceptAlert(altElement);
        Thread.sleep(3000);
        List<WebElement> WebElementList= webDriver.findElements(By.xpath("//div[@class=\"doc-item ng-star-inserted\"]"));
        if (WebElementList.size()!=0) {
            clickAgree(date,webDriver,WebElementList.size(),Valid);
            JavascriptExecutor js = (JavascriptExecutor) webDriver;
            js.executeScript("window.scrollTo(0, 0)");
            webDriver.executeScript("document.querySelector('body > ngx-app > ngx-pages > ngx-layout > nb-layout > div > div > div > div > div > nb-layout-column > approval-detail-content > div > nb-card > nb-card-header > div.header > div.ng-star-inserted > button').removeAttribute('disabled')");
        }
        webDriver.findElement(By.xpath("//button[text()='Submit']")).click();
        Thread.sleep(3000);
        boolean falg=false;
        String text=webDriver.findElement(By.xpath("//nb-alert[@class='error-message status-danger ng-star-inserted']")).getText();
        if (text.equals("This case is already submitted by someone else.")){
            return falg=true;
        }
        return falg;
    }


    public void clickCheckerBtn(){
        clickElement(checkerBtn);
    }
    public boolean contrast(){
        List<String> list = new ArrayList<>();
        boolean ratio=false;
        for(WebElement row : makerList){
            list.add(row.getText());
        }
        if (list.contains(listMaker)){
            ratio=true;
            return ratio;
        }
        return ratio;
    }
    public void clickrejectBtn() throws InterruptedException {
        Thread.sleep(3000);
        clickElement(rejectBtn);
        andSendKeys(inputApprove,"automation Approve");
    }

    public boolean verifyMakerPageList() {
        List<String> Head = Arrays.asList(headLine.getText().split(" "));
        List<String> HeadName = new ArrayList<>();
        HeadName.add("Action");
        HeadName.add("T24 Cust. ID");
        HeadName.add("Submit datetime");
        HeadName.add("Eng Name");
        HeadName.add("Chi Name");
        HeadName.add("Mobile no.");
        HeadName.add("Maker");
        boolean flag = !Collections.disjoint(Head, HeadName);
        return flag;
    }

    @SneakyThrows
    public boolean verifyMakerList(){
        List<String> list = new ArrayList<>();
        String Action ="Action";
        String T24CustID ="T24 Cust. ID";
        String SubmitDatetime ="Submit datetime";
        String EnglishName ="English name";
        String ChineseName ="Chinese name";
        String MobileNo ="Mobile no.";
        String Maker ="Maker";
        boolean flag=false;
        list.add(Action);
        list.add(T24CustID);
        list.add(SubmitDatetime);
        list.add(EnglishName);
        list.add(ChineseName);
        list.add(MobileNo);
        list.add(Maker);
        Thread.sleep(20000);
        listMaker.clear();
        List<WebElement> Element=headline;
        for (int i=0;i<Element.size();i++){
            listMaker.add(Element.get(i).getText());
        }
        for (int i=0;i<list.size();i++){
            if (!list.contains(listMaker.get(i))){
                flag=true;
                return flag;
            }
        }
        return flag;
    }

    public boolean verifyViewBtn(){
        boolean flag = isElementClickable(By.xpath(""));

        return flag;
    }
    @SneakyThrows
    public boolean verifyViewDeta(){
        boolean flag=true;
        Thread.sleep(20000);
        if (!submitBtn.isEnabled()){
            return flag=false;
        }
        if (!backBtn.isEnabled()){
            return flag=false;
        }

        String numberText=validatedNumber.getText();
        numberText=numberText.replaceAll("[，。,、._—— ^|~！@#￥;；：=<><><>:【》‘“”、】《?？：。，：{}%……& a-z A-Z]","").trim();
        String[] str=numberText.split("/");
        if (str!=null) {
            String read = str[0];
            String unread = str[1];
            int notSeen=success.size();
            int Valid=successSelect.size();
            int invalid=danger.size();
            int notSeenNumber=Valid+invalid;
            if (notSeen!=Integer.parseInt(unread) || notSeenNumber!=Integer.parseInt(read)){
                return flag=false;
            }
            List<WebElement> Element=button;
            for (WebElement Element2:Element){
                Element2.click();
                Thread.sleep(1000);
                closeWindow();
                Thread.sleep(1000);
            }
        }
        return flag;
    }

    public void closeWindow() {
        try {
            String winHandleBefore = driver.getWindowHandle();
            Set<String> winHandles = driver.getWindowHandles();
            Iterator<String> it = winHandles.iterator();
            while (it.hasNext()) {
                String win = it.next();
                if (win.equals(winHandleBefore)) {
                    driver.switchTo().window(win);
                    logger.info("Switch Window From " + winHandleBefore + " to " + win);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RemoteWebDriver initDrive() throws IOException {
        DriverFactory driverFactory;
        setURL();
        String browserType = getBrowserType().toUpperCase();

        switch (browserType) {
            case "CHROME":
                driverFactory = new ChromeDriverFactory();
                break;
            case "FIREFOX":
                driverFactory = new FirefoxDriverFactory();
                break;
            case "SAFARI":
                driverFactory = new SafariDriverFactory();
                break;
            default:
                logger.error("Illegal Browser Type: " + browserType);
                throw new IllegalArgumentException("Illegal Browser Type: " + browserType);
        }
        webDriver = driverFactory.getDriver();
        logger.info("The {} web driver is started.", browserType);
        return webDriver;
    }

    private void setURL() throws IOException {
        String hub = GlobalVar.GLOBAL_VARIABLES.get("hub");
        url = new URL("http://" + hub + "/wd/hub");
    }
    private String getBrowserType() {
        String browser = System.getProperty("platform");
        if (browser == null) {
            return "chrome";
        }
        return browser;
    }

    public String getText(WebElement element ){
        String attributevalue=element.getAttribute("class");
        return attributevalue;
    }


    public boolean verifyMakerPageHeader(){
        boolean flag = verifyContainsText(approvalListHeader,"Approval List - Maker", false);
        return  flag;
    }

    public boolean verifyCheckerPageHeader(){
        boolean flag = verifyContainsText(approvalListHeader,"Approval List - Checker", false);
        return  flag;
    }

    public boolean verfiyViewCanClickAndViewDetail(){
        boolean flag = false;
        List<String> firstDetail = new ArrayList<>();
        firstDetail.add(firstT24ID.getText());
        firstDetail.add(firstName.getText());
        firstView.click();
        String INFO = PersonalINFO.getText();
        Iterator<String> it = firstDetail.iterator();
        while (it.hasNext()) {
            if (INFO.contains(it.next())) {
                flag = true;
            } else {
                flag = false;
            }
        }
        return flag;
    }

    @SneakyThrows
    public String verifyNameNotNull(List<WebElement> list){
        String ID =" ";
        for(WebElement row : list){
            if(!row.getText().equals("")){
                ID =row.getText();
                return ID;
            }
        }
        Thread.sleep(5000);
        System.out.println(ID+"id");
        return ID;
    }



}
