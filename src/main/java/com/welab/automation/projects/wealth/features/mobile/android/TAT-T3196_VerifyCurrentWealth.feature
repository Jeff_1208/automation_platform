Feature: Verify wealth landing Current Wealth is updated

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears
    And Login with user and password
      | user     | test192  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I move eyeBtn to top right corner
    And I go to Wealth Page

  @WELATCOE-701.1
  Scenario Outline: Current wealth is updated after Set My Target goal payment is submitted
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    When I click Add New Goals
    And I click add Reach My Target and set <targetValue> and <time>
    And I click the discover and view portfolio
    And I click buy button and slide to confirm order
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | targetValue | time |
      | 400000      | 50   |

  @WELATCOE-701.2
  Scenario Outline: Current wealth is updated after Build My Investing Habit goal payment is submitted
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click the discover and view portfolio
    And I click buy button and slide to confirm order
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 300000       | 4000         | 360   |



  @WELATCOE-701.3
  Scenario Outline: Current wealth is updated after Achieve Financial Freedom goal payment is submitted
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover and view portfolio
    And I click buy button and slide to confirm order
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 42  | 50000           | Budget,Modest,Deluxe |

  @WELATCOE-703.1
  Scenario: Verify Current wealth is updated after edit the Set My Target goal and submit new fund order
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    And I click Set My Target On track Button
    And I click edit Button and change monthlyInvestment
    And I click View update recommendation
    And I click View New Portfolio
    And I click buy button and slide to confirm order
    And I verify order condition is changed

  @WELATCOE-703.2
  Scenario Outline: Verify Current wealth is updated after edit the Build My Investing Habit goal and submit new fund order
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    And I click Build My Investing Habit On track Button
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click View New Portfolio
    And I click buy button and slide to confirm order
    And I verify order condition is changed
    Examples:
      | monthlyValue |
      | 50500        |

  @WELATCOE-703.3
  Scenario: Verify Current wealth is updated after edit the Achieve Financial Freedom goal and submit new fund order
    When I go to Wealth Page
    And I get Wealth Current Page Wealth
    And I click Achieve Financial Freedom On track Button
    And I click edit Button and change monthlyInvestment
    And I click View update recommendation
    And I click YES button
    And I click View New Portfolio
    And I click buy button and slide to confirm order
    And I verify order condition is changed

  @WELATCOE-705.1
  Scenario Outline:Current wealth is updated after sell Set My Target goal payment is submitted
    When I go to Wealth Page
    And I click Set My Target On track Button
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | SellValue |
      | 1000      |

  @WELATCOE-705.2
  Scenario Outline:Current wealth is updated after sell Build My Investment Habit goal payment is submitted
    When I go to Wealth Page
    And I click Build My Investing Habit On track Button
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | SellValue |
      | 1000      |

  @WELATCOE-705.3
  Scenario Outline:Current wealth is updated after sell Achieve Financial Freedom goal payment is submitted
    When I go to Wealth Page
    And  I click Achieve Financial Freedom On track Button
    And I click Sell button
    And I click Proceed button
    And I edit Sell Value <SellValue>
    And I Slide to confirm Sell And Click Done
    Then I can see Sell button will be disabled and goal is Processing
    And I click button back to WealthMainPage
    And I verify wealth page wealth update
    Examples:
      | SellValue |
      | 1000      |