Feature: verify APP UI when user complete application and maker&checker took different action on admin portal

  Background: IAO application submit
#    TODO: create new account

  # These accounts only can be used once, since the action cannot be revoked on admin portal.

  @WELATCOE-552
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'agree' & checker = 'agree'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see pending page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |
    Examples:
      | username |
      | test107  |

  @WELATCOE-554
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'agree' & checker = 'agree'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |
    Examples:
      | username |
      | xxxx     |

  @WELATCOE-556
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'agree' & checker = 'reject'
#     TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see pending page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |
    Examples:
      | username |
      | test109  |

  @WELATCOE-558
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'agree' & checker = 'reject'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |
    Examples:
      | username |
      | test117  |

  @WELATCOE-560
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'reject' & checker = 'reject'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |
    Examples:
      | username |
      | test119  |

  @WELATCOE-562
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'reject' & checker = 'reject'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |
    Examples:
      | username |
      | test113  |

  @WELATCOE-564 @debug
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'reject' & checker = 'agree'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can find document upload page
      | pageTitle | Document upload |
    Examples:
      | username |
      | test116  |

  @WELATCOE-566
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'reject' & checker = 'agree'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And login welab app with <username> and password
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Then I can see transition page
      | pageTitle | Upload document(s) |
    Examples:
      | username |
      | test115  |
