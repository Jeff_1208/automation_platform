Feature: Check Fund Documents

  Background: Login with correct user and password
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And Login with user and password
      | user     | test011  |
      | password | Aa123321 |
    And I can see the LoggedIn page

  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set My Target
    When I go to Wealth Page
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
    And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | targetValue | time |
      | 300000      | 50   |

  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set Investing Habit goal
    When I go to Wealth Page
    And I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
    And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set Financial Freedom goal
    When I go to Wealth Page
    And I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
    And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  Scenario Outline: Check Fund Document in Portfolio and Confirm Page_Set My Target
    When I go to Wealth Page
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I View Portfolio Fund List
    And I view Featured Fund
    And I start to check fund document in featured fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario Outline: Check Fund Document in Portfolio and Confirm Page_Set Investing Habit goal
    When I go to Wealth Page
    And I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I View Portfolio Fund List
    And I view Featured Fund
    And I start to check fund document in featured fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |


  Scenario Outline: Check Fund Document in Portfolio and Confirm Page_Set Financial Freedom goal
    When I go to Wealth Page
    And I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I View Portfolio Fund List
    And I view Featured Fund
    And I start to check fund document in featured fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |