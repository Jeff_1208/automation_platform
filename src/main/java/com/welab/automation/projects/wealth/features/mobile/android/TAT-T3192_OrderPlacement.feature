Feature: Order placement

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears
#    And Login with user and password
#      | user     | test006  |
#      | password | Aa123321 |
#    And I can see the LoggedIn page

  @WELATCOE-433
  Scenario: The processing about Order placement but quit in Review and confirm order page for Set My Target _ USD buy USD_Quit
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      | 3000000     | 60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-433
  Scenario: The processing about Order placement but quit in Review and confirm order page for Set My Target _ USD buy USD_Not now
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      | 3000000     | 60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-435
  Scenario: The processing about Order placement but quit in Review and confirm order page for Build my investment habit _ USD buy USD_Quit
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-435
  Scenario: The processing about Order placement but quit in Review and confirm order page for Build my investment habit _ USD buy USD_Not now
    Given Login with user and password
      | user     | test098  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-437
  Scenario: The processing about Order placement but quit in Review and confirm order page for Achieve Financial Freedom _ USD buy USD_Quit
    Given Login with user and password
      | user     | test098  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-437
  Scenario: The processing about Order placement but quit in Review and confirm order page for Achieve Financial Freedom _ USD buy USD_Not now
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-439
  Scenario:The complete processing about Order placement_Reach My Target _ USD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I goto wealth main page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      | 3000000     | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page


  @WELATCOE-441
  Scenario: The complete processing about Order placement_Build My Investing Habit _ USD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |
    And I click the discover
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-443
  Scenario: The complete processing about Order placement_Achieve Financial Freedom _ USD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 60  | 500           | No,No,No |
    And I click the discover
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page


  @WELATCOE-423
  Scenario: The complete processing about Order placement_Set My Target _ HKD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I goto wealth main page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      | 3000000     | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-425
  Scenario: The complete processing about Order placement_Build My Investing Habit _ HKD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page


  @WELATCOE-427
  Scenario: The complete processing about Order placement_Achieve Financial Freedom _ HKD buy USD
    Given Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     500        |  No,No,No |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page
  @WELATCOE-530
#    test006
  Scenario: Update the tradability ability status is No - Buy_Set My Target
    Given Login with user and password
      | user     | test092  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |             | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-530
  Scenario: Update the tradability ability status is No - Buy_Build My Investing Habit
    Given Login with user and password
      | user     | test092  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-530
  Scenario: Update the tradability ability status is No - Buy_Set Achieve Financial Freedom
    Given Login with user and password
      | user     | test092  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     5000        | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-530
#    test006
  Scenario: Update the tradability ability status is No TRADABLE_Set My Target
    Given Login with user and password
      | user     | test098  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |             | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-530
  Scenario: Update the tradability ability status is No TRADABLE_Build My Investing Habit
    Given Login with user and password
      | user     | test098  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-530
  Scenario: Update the tradability ability status is No TRADABLE_Set Achieve Financial Freedom
    Given Login with user and password
      | user     | test098  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     5000        | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

