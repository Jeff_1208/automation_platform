Feature: IAO process

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  Scenario: IAO full process
    Given Check upgrade page appears
    And Login with user and password
      | user     | test118  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
    #   upload document
    And I can see the upload files page
    And I review the sample files
    And I take photo to upload
    Then I finish upload document
    #   Do CRPQ
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process


  Scenario: Verify investment account open when personal info & non-CRPQ and CRPQ is completed but document is not uploaded
    And Login with user and password
      | user     | test112  |
      | password | Aa123321 |
    And I can see the LoggedIn page
#    And goto wealth main page
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
    And I Skip Upload Document
    And I Do CRPQ with "CRPQ"
    Then I can see Wealth Landing Page
    And I click Open Account button
#   upload document
    And I can see the upload files page
    And I review the sample files
    And I take photo to upload
    Then I finish upload document with CRPQ Done
    Then I finish account opening process


  Scenario: Verify investment account open when personal info & non-CRPQ and upload document is completed but CRPQ is not complete
    And Login with user and password
      | user     | test113  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
#   upload document
    And I can see the upload files page
    And I review the sample files
    And I take photo to upload
    Then I finish upload document
#   quit IAO process
    And I quit opening account
    Then I can see Wealth Landing Page
#   Do CRPQ
    And I click Open Account button
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process


  Scenario: Verify investment account open when personal info & non-CRPQ is completed but upload document and CRPQ is not completed
    Given Check upgrade page appears
    And Login with user and password
      | user     | test119  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
#        I quit IAO process
    And I quit and back to Open Account
    And I goto wealth main page
    And I click Open Account button
#    And I goto wealth main page
    #   upload document
    And I can see the upload files page
    And I review the sample files
    And I take photo to upload
    Then I finish upload document
#   Do CRPQ
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process
