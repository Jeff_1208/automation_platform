package com.welab.automation.projects.wealth.WelabAPITest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thoughtworks.xstream.mapper.Mapper.Null;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.HttpUtils;
import com.welab.automation.framework.utils.JsonUtil;
import com.welab.automation.framework.utils.api.SendRequest;
import com.welab.automation.framework.utils.api.SendRequestEx;
import com.welab.automation.framework.utils.entity.api.ConfigUtils;
import com.welab.automation.framework.utils.entity.api.TestStep;
import com.welab.automation.projects.demo.steps.api.DefaultStepsDefinitions;
import io.qameta.allure.Allure;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import java.io.File;
import java.io.FileInputStream;

import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class VerifyUiAndApiGoalSetting {
    Logger logger = LoggerFactory.getLogger("VerifyUiAndApiGoalSetting");
    private static Response response;


    public static Map<String, String> gridFinder(String name,String password,String tenor,String initInvt,String monthlySaving,String target ) throws Exception {
        Map<String, String> valueExpression = new HashMap<String, String>();
        String path = "/v1/wealth/grid-finder";

        Map<String, String> prepareData = new HashMap<String, String>();
        prepareData.put("TEST_ACCOUNT_SPECIFIED",name);
        prepareData.put("TEST_PASSWORD_SPECIFIED",password);
        String params="horizon="+Integer.parseInt(tenor)+"&initInvt="+Integer.parseInt(initInvt)+"&monthlySaving="+Integer.parseInt(monthlySaving)+"&target="+Integer.parseInt(target)+"&tenor="+Integer.parseInt(tenor)+"";
        String globalVariable="data.defaultInitInvt,data.defaultMonthlySaving,data.defaultHorizon,data.target";
        Map<String, String> result=yearOfBirthAge(path,params,valueExpression,prepareData,globalVariable);
        return result;
    }

    public static Map<String, String> getSuccessRate (String name,String password, String tenor, String initInvt, String monthlySaving, String target ) throws Exception {
        Map<String, String> valueExpression = new HashMap<String, String>();
        valueExpression.put("successRate","data.successRate");
        valueExpression.put("portfolioName","data.portfolioName");

        String path = "/v1/wealth/matching-portfolio-service";
        Map<String, String> prepareData = new HashMap<String, String>();
        prepareData.put("TEST_ACCOUNT_SPECIFIED",name);
        prepareData.put("TEST_PASSWORD_SPECIFIED",password);
        Map<String, String> expectedData=gridFinder(name,password,tenor,initInvt,monthlySaving,target);
        String params="initInvt=${data.defaultInitInvt}&monthlySaving=${data.defaultMonthlySaving}&horizon=${data.defaultHorizon}&target=${data.target}&minSuccessRate=40";

        String globalVariable="";
        Map<String, String> result=yearOfBirthAge(path,params,valueExpression,prepareData,globalVariable);
        return result;
    }


    public static Map<String, String> getYearAge(String name,String password ) throws Exception {
        Map<String, String> valueExpression = new HashMap<String, String>();
        valueExpression.put("dateOfBirth","data.hkidInfo.dateOfBirth");
        valueExpression.put("age","data.hkidInfo.age");
        String path = "/v1/wealth/customer/personal-info";

        Map<String, String> prepareData = new HashMap<String, String>();
        prepareData.put("TEST_ACCOUNT_SPECIFIED",name);
        String params="";
        prepareData.put("TEST_PASSWORD_SPECIFIED",password);

        String globalVariable="data.hkidInfo.age,data.hkidInfo.dateOfBirth";
        Map<String, String> result=yearOfBirthAge(path,params,valueExpression,prepareData,globalVariable);
        return result;
    }

    public static Map<String, String> yearOfBirthAge(String path,String params, Map<String, String> valueExpression,Map<String, String> prepareData,String globalVariable) throws Exception {
        Map<String, String> globalVars = ConfigUtils.getPropertiesToMap();
        GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
        GlobalVar.HEADERS.put("Content-Type","application/json");
        GlobalVar.HEADERS.put("Client-Id",GlobalVar.GLOBAL_VARIABLES.get("CLIENT_ID"));
        GlobalVar.HEADERS.put("Client-Secret",GlobalVar.GLOBAL_VARIABLES.get("CLIENT_SECRET"));
        GlobalVar.HEADERS.put("Accept-Encoding","gzip,deflate,br");
        if (!prepareData.isEmpty()){
            for(String key : prepareData.keySet()){
                GlobalVar.GLOBAL_VARIABLES.put(key,prepareData.get(key));
            }
        }
        String env = System.getProperty("env");
        if (env.equals("sit")){
        File file =new File("./src/main/resources/api_wealth.properties");
        if(!file.exists()) file.createNewFile();
        Properties proterties=new Properties();
        proterties.load(new FileInputStream(file));
        String host=(String) proterties.get("host");
        GlobalVar.GLOBAL_VARIABLES.put("host", host);
        }else if (!env.equals("sit")){

        }
        SendRequest SendRequest = new SendRequest();
        Map<String, String> Map = new HashMap<String, String>();
        response = SendRequest.sendRequest(path, "GET", "", params, "");
        if (valueExpression.size()!=0){
            for(String key : valueExpression.keySet()){
                String value = valueExpression.get(key);
                String parameter = response.jsonPath().get(value).toString();
                Map.put(key,parameter);
            }
        }
        DefaultStepsDefinitions DefaultStepsDefinitions=new DefaultStepsDefinitions();
        String byParameter="";
        VerifyUiAndApiGoalSetting.storeVariablesEx(byParameter,globalVariable);
        return Map;
    }

    @SneakyThrows
    public static void main(String[] args) {
        String path = "/v1/investment-accounts/list";
        File file =new File("./src/main/resources/api_portal.properties");
        if(!file.exists()) file.createNewFile();
        Properties proterties=new Properties();
        proterties.load(new FileInputStream(file));
        String host=(String) proterties.get("host");
        GlobalVar.GLOBAL_VARIABLES.put("host", host);
        Map<String,Object> params = new HashMap<>();
        params.put("order","createdDate");
        params.put("asc","false");
        params.put("t24ClientId","");
        params.put("fnzHeadAccountId","");
        params.put("createdDate","");
        params.put("accOpenDate","");
        params.put("pageNum","5");
        params.put("pageSize","60");
        String params2="order=createdDate&asc=false&t24ClientId=&fnzHeadAccountId=&createdDate=&accOpenDate=&pageNum=5&pageSize=20";
        HttpUtils httpUtils = new HttpUtils(host);
        TestStep testStep = new TestStep();
        testStep=SendRequestEx.CreateTestStep(path,"GET",params,null);
        Response response = httpUtils.request(testStep);
        List<String> listMaker = new ArrayList<>();
        List<Map<String, Object>> data = response.jsonPath().getList("data");
        for (int i=0;i<data.size();i++){
            String str=data.get(i).get("tradability").toString();
            String status=data.get(i).get("status").toString();
            if (str.equalsIgnoreCase("TRADABLE") && status.equalsIgnoreCase("ACTIVE")){
                listMaker.add(data.get(i).get("t24ClientId").toString());
            }
        }
    }
    public  static List<String>  getDataStatus(int page,int total) throws Exception {
        String path = "/v1/investment-accounts/list";
        File file =new File("./src/main/resources/api_portal.properties");
        if(!file.exists()) file.createNewFile();
        Properties proterties=new Properties();
        proterties.load(new FileInputStream(file));
        String host=(String) proterties.get("host");
        GlobalVar.GLOBAL_VARIABLES.put("host", host);
        Map<String,Object> params = new HashMap<>();
        params.put("order","createdDate");
        params.put("asc","false");
        params.put("t24ClientId","");
        params.put("fnzHeadAccountId","");
        params.put("createdDate","");
        params.put("accOpenDate","");
        params.put("pageNum",""+page+"");
        params.put("pageSize",""+total+"");
        String params2="order=createdDate&asc=false&t24ClientId=&fnzHeadAccountId=&createdDate=&accOpenDate=&pageNum=5&pageSize=20";
        HttpUtils httpUtils = new HttpUtils(host);
        TestStep testStep = new TestStep();
        testStep=SendRequestEx.CreateTestStep(path,"GET",params,null);
        Response response = httpUtils.request(testStep);
        List<String> listMaker = new ArrayList<>();
        List<Map<String, Object>> data = response.jsonPath().getList("data");
        for (int i=0;i<data.size();i++){
            String str=data.get(i).get("tradability").toString();
            String status=data.get(i).get("status").toString();
            if (str.equalsIgnoreCase("TRADABLE") && status.equalsIgnoreCase("ACTIVE")){
                listMaker.add(data.get(i).get("t24ClientId").toString());
            }
        }
        return listMaker;
    }


    public static void   storeVariablesEx(String byParameter, String variables) {
        if (!byParameter.isEmpty())
        {
            Map obj = JSON.parseObject(byParameter);
            Iterator iterator = obj.entrySet().iterator();
            for (; iterator.hasNext(); )
            {
                Map.Entry entry = (Map.Entry) iterator.next();
                String field = (String) entry.getKey();
                String value = (String) entry.getValue();
                if (!variables.equals(""))
                {
                    List<ArrayList> lst = response.jsonPath().getList(field);
                    JSONArray jsonArray = JsonUtil.listToJsonArray(lst);
                    for (int i = 0; i < jsonArray.size(); i++)
                    {
                        if (jsonArray.get(i).equals(value))
                        {
                            String result = response.jsonPath().getList(variables).get(i).toString();
                            GlobalVar.GLOBAL_VARIABLES.put(variables, result);
                            Allure.addAttachment("Set global variable ", variables+"="+ result);
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            if (!variables.equals("")) {
                String[] fields = variables.split(",");
                for (String key : fields) {
                    JsonPath jp = response.jsonPath();
                    try {
                        GlobalVar.GLOBAL_VARIABLES.put(key, jp.get(key).toString());
                        Allure.addAttachment("Set global variable ", key +"="+jp.get(key));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Allure.addAttachment("variables", "There didn't set global variable");
            }
        }
    }




}
