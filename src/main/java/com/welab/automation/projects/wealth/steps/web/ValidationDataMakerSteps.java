package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.demo.pages.web.DemoPage;
import com.welab.automation.projects.demo.steps.web.DemoFeature;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ApprovalPage;
import com.welab.automation.projects.wealth.pages.adminPortal.SummaryPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ValidationDataMakerPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.picocontainer.annotations.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidationDataMakerSteps extends WebBasePage{
    private final Logger logger = LoggerFactory.getLogger(com.welab.automation.projects.demo.steps.web.DemoFeature.class);
    @Inject
    AdminPortalMainPage adminPortalMainPage;
    @Inject
    ApprovalPage approvalPage;
    @Inject
    ValidationDataMakerPage ValidationDataMakerPage;
    @Inject
    SummaryPage summaryPage;


//        @Given("^Open the URL ([^\"]*)\\S*$")
//        public void openUrl(String url) {
//            try {
//                navigateTo(url);
//            } catch (Exception e) {
//                Assert.fail(e.toString());
//            }
//        }

    @And("I read the list")
    public void readList() throws InterruptedException, ParseException {
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listBefore()).isEqualTo(true);
    }
    @And("I click Submit datetime")
    public void submitDatetime() throws InterruptedException, ParseException {
        ValidationDataMakerPage.clickSubmitDatetime();
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listAfter()).isEqualTo(true);
    }

    @And("login keycloak")
    public void loginAsMaker(Map<String, String> data) {
        ValidationDataMakerPage.login(data.get("username"), data.get("password"));
    }
    @And("I click on the Approval List")
    public void Approval() throws IOException {
        adminPortalMainPage.gotoApprovalMaker();
    }
    @And("I click on the maker")
    public void maker() {
        ValidationDataMakerPage.maker();
    }


    @And("I Click on the first row of data")
    public void clickFirstData() throws InterruptedException, ParseException {
        ValidationDataMakerPage.clickList();
    }
    @And("I clicked the Agree button and half entered my opinion")
    public void clickApproveBut() throws InterruptedException, ParseException {
        ValidationDataMakerPage.clickApproveBut();
    }
    @Given("I goto approval maker")
    public void goto_approval_maker() throws IOException {
        adminPortalMainPage.gotoApprovalMaker();
    }

    @SneakyThrows
    @And("I clicked Maker Btn")
    public void clickedMakerBtn() throws InterruptedException {

        ValidationDataMakerPage.getUserList();
        assertThat(ValidationDataMakerPage.verificationUserList()).isEqualTo(true);
    }

    @And("I found the approved order")
    public void approvedOrder() throws InterruptedException {
        ValidationDataMakerPage.clickCheckerBtn();
        ValidationDataMakerPage.clickSubmitDatetime();
        assertThat(ValidationDataMakerPage.T24id()).isEqualTo(true);
    }

    @And("^I operated in the current browser and entered the ([^\"]\\S*) ([^\"]\\S*)$")
    public void currentBrowser (String date,String Valid) throws InterruptedException {
        ValidationDataMakerPage.clickApproveBut();
        ValidationDataMakerPage.clickApproveBut();
        ValidationDataMakerPage.choseDate(date,Valid);
    }

    @And("I opened multiple browsers to operate the same order ([^\"]\\S*) ([^\"]\\S*)$")
    public void openedBrowsers (String date,String Valid) throws InterruptedException {
        assertThat(ValidationDataMakerPage.clickedReopen( date,Valid)).isEqualTo(true);
    }

    @SneakyThrows
    @And("I open the approved order")
    public void openCheckerBtn() throws InterruptedException {
        ValidationDataMakerPage.clickCheckerBtn();
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listBefore()).isEqualTo(true);
        ValidationDataMakerPage.clickedMakerBTN();
        ValidationDataMakerPage.getUserList();
        assertThat(ValidationDataMakerPage.verificationUserList()).isEqualTo(true);
        ValidationDataMakerPage.clickSubmitDatetime();
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listAfter()).isEqualTo(true);
    }

    @SneakyThrows
    @And("^My approved order ([^\"]\\S*) ([^\"]\\S*)$")
    public void disagreeApplicationdecline(String date,String Valid) {
        ValidationDataMakerPage.clickApproveBut();
        ValidationDataMakerPage.choseDate(date,Valid);
        ValidationDataMakerPage.clickedSubmit();
    }

    @SneakyThrows
    @And("^I approve rejected orders ([^\"]\\S*)$")
    public void approveRejectedOrders(String time) {
        ValidationDataMakerPage.clickrejectBtn();
        ValidationDataMakerPage.reasonsForRfusal(time);
        ValidationDataMakerPage.clickedSubmit();
    }
    @SneakyThrows
    @And("^I approve rejected orders ([^\"]\\S*) ([^\"]\\S*)$")
    public void approveRejectedOrders(String date,String Valid) {
        ValidationDataMakerPage.clickrejectBtn();
        ValidationDataMakerPage.choseDate(date, Valid);
        ValidationDataMakerPage.clickedSubmit();
    }
    @And("verify header of Maker approval list page")
    public void verifyMakerHeader(){
        boolean flag = ValidationDataMakerPage.verifyMakerPageHeader();
        assertThat(flag).isTrue();
    }


    @And("verify header of Checker's approval list page")
    public void verifyCheckerPageHeader(){
        boolean flag = ValidationDataMakerPage.verifyCheckerPageHeader();
        assertThat(flag).isTrue();
    }
    @And("I verify header of Checker's approval list page")
    public void verifyMakerPageHeader(){
        ValidationDataMakerPage.clickCheckerBtn();
    }


    @And("I verify the columns of Maker's approval list table")
    public void verifyMakerList(){
        boolean flag = ValidationDataMakerPage.verifyMakerPageList();
        Assert.assertTrue(flag);
    }
    @And("verify the columns of Checker's approval list table")
    public void verifyMakerPageList(){
        boolean flag = ValidationDataMakerPage.verifyMakerPageList();
        Assert.assertTrue(flag);
    }


    @And("I click view button can see view button is enabled and detail view")
    public void verifyViewBtnAndDetailView(){
        assertThat(summaryPage.verifyViewBtnExist()).isTrue();
        assertThat(ValidationDataMakerPage.verfiyViewCanClickAndViewDetail()).isTrue();
    }
    @And("I verified the display data of the audit page")
    public void verifiedDisplayPafeData(){
        ValidationDataMakerPage.verifyViewDeta();
    }

    @SneakyThrows
    @And("verify default sorting of checker page")
    public void verifyDefaultSort(){
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listBefore()).isEqualTo(true);
    }

    @SneakyThrows
    @And("verify sort by Submit datetime")
    public void verifySortBySubTime(){
        ValidationDataMakerPage.clickSubmitDatetime();
        ValidationDataMakerPage.makerList();
        assertThat(ValidationDataMakerPage.listAfter()).isEqualTo(true);
    }

    @SneakyThrows
    @And("verify sort by Maker column")
    public void verifySortByMaker(){
        ValidationDataMakerPage.clickedMakerBTN();
        ValidationDataMakerPage.getUserList();
        assertThat(ValidationDataMakerPage.verificationUserList()).isEqualTo(true);
    }

    @And("^I approve the application of ([^\"]\\S*)")
    public void approveTheApplicationOfUsername(String user) {
        Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
        assertThat(targetApplication).isNotNull();
        approvalPage.approveApplication(targetApplication);
    }
}
