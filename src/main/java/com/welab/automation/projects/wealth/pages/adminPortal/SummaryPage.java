package com.welab.automation.projects.wealth.pages.adminPortal;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.picocontainer.annotations.Inject;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.util.*;

public class SummaryPage extends WebBasePage {
  private final String pageName = "Summary page";
  @Inject ValidationDataMakerPage validationDataMakerPage;

  @FindBy(how = How.CLASS_NAME, using = "headline")
  private WebElement summaryHeadline;

  @FindBy(how = How.XPATH, using = "//option[text()='<T24 Customer ID>']")
  private WebElement T24IDoption;

  @FindBy(how = How.XPATH, using = "//option[text()='<FNZ head account no>']")
  private WebElement FNZoption;

  @FindBy(how = How.XPATH, using = "//option[text()='<Account open date>']")
  private WebElement Dateoption;

  @FindBy(how = How.CSS, using = "li[class='page-item ng-star-inserted disabled']")
  private WebElement disabledNextBtn;

  @FindBy(how = How.XPATH, using = "//button[text()='View']")
  private WebElement viewActionBtn;

  @FindBy(how = How.TAG_NAME, using = "th")
  private List<WebElement> tableHeaders;

  @FindBy(how = How.CSS, using = "[aria-label=Next]")
  private WebElement nextBtn;

  @FindBy(how = How.XPATH, using = "//span[text()='Account Detail']")
  private WebElement AccountPageHeader;

  @FindBy(how = How.XPATH, using = "//button[text()='< Back']")
  private WebElement BackBtn;

  @FindBy(how = How.CLASS_NAME, using = "subContainer")
  private WebElement PersonalINFO;

  @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[2]")
  private List<WebElement> ListT24;

  @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[3]")
  private List<WebElement> ListFNZ;

  @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[6]")
  private List<WebElement> ListInvestment;

  @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[7]")
  private List<WebElement> ListTrad;

  @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[8]")
  private List<WebElement> ListopenDate;

  @FindBy(how = How.XPATH, using = "//button[text()='Reset']")
  private WebElement resetBtn;

  @FindBy(how = How.TAG_NAME, using = "input")
  private WebElement searchInput;
  @FindBy(how = How.XPATH, using = "//button[text()='Search']")
  private WebElement searchBtn;
  @FindBy(how = How.XPATH, using = "//th[2]/select")
  private WebElement selectBtn;
  @SneakyThrows
  public SummaryPage() {
    super.pageName = this.pageName;
  }

  private enum SummaryHeader {
    Action("Action"),
    T24ID("T24ID"),
    FNZhead("FNZhead a/c"),
    ENGLISH_NAME("English Name"),
    CHINESE_NAME("Chinese Name"),
    InvestmentStatus("Investment a/c status"),
    Tradability("Tradability"),
    openDate("a/c open date");

    public final String header;

    SummaryHeader(String header) {
      this.header = header;
    }
  }

  public boolean isIASummaryPage() {
    List<String> Haead = Arrays.asList(summaryHeadline.getText().split(" "));
    List<String> HaeadName = new ArrayList<>();
    HaeadName.add("Action");
    HaeadName.add("T24 Cust. ID");
    HaeadName.add("FNZ head a/c");
    HaeadName.add("Eng Name");
    HaeadName.add("Chi Name");
    HaeadName.add("Investment a/c status");
    HaeadName.add("Tradability");
    HaeadName.add("a/c open date");
    boolean flag = !Collections.disjoint(Haead, HaeadName);
    List<WebElement> Option = new ArrayList<>();
    Option.add(T24IDoption);
    Option.add(FNZoption);
    Option.add(Dateoption);
    return flag && verifyAllElementExist(Option);
  }

  private List<Map<String, WebElement>> filterSummaryFromTableHeaders() {
    List<Map<String, WebElement>> applications = new ArrayList<>();
    waitUntilElementVisible(viewActionBtn);

    for (int i = 0; i < tableHeaders.size(); i++) {
      Map<String, WebElement> application = new IdentityHashMap<>();
      if (tableHeaders.get(i).getText().equals("View")) {
        application.put(SummaryHeader.Action.header, tableHeaders.get(i++));
        application.put(SummaryHeader.T24ID.header, tableHeaders.get(i++));
        application.put(SummaryHeader.FNZhead.header, tableHeaders.get(i++));
        application.put(SummaryHeader.ENGLISH_NAME.header, tableHeaders.get(i++));
        application.put(SummaryHeader.CHINESE_NAME.header, tableHeaders.get(i++));
        application.put(SummaryHeader.InvestmentStatus.header, tableHeaders.get(i++));
        application.put(SummaryHeader.Tradability.header, tableHeaders.get(i++));
        application.put(SummaryHeader.openDate.header, tableHeaders.get(i));
        applications.add(application);
      }
    }
    return applications;
  }

  @SneakyThrows
  public Map<String, WebElement> findSummary(String username) {
    while (true) {
      List<Map<String, WebElement>> applications = filterSummaryFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());
      Map<String, WebElement> targetApplication = findSummaryInCurrentPage(username, applications);

      if (targetApplication == null) {
        if (verifyElementExist(disabledNextBtn)) {
          return null;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 3); // wait for loading.
      } else {
        return targetApplication;
      }
    }
  }

  public Map<String, WebElement> findSummaryInCurrentPage(
      String username, List<Map<String, WebElement>> applications) {
    for (Map<String, WebElement> application : applications) {
      String EnglishName = getElementText(application.get(SummaryHeader.ENGLISH_NAME.header));
      String user = EnglishName.split(" ")[1];
      if (user.equals(username)) {
        return application;
      }
    }
    return null;
  }

  public boolean isSummaryDetailPageHeader() {
    List<WebElement> page = new ArrayList<>();
    page.add(AccountPageHeader);
    page.add(BackBtn);
    String INFO = PersonalINFO.getText();
    List<String> Title = new ArrayList<>();
    boolean flag = false;
    Title.add("Submission");
    Title.add("T24 Customer ID");
    Title.add("FNZ Head");
    Title.add("English Name");
    Title.add("Chinese Name");
    Title.add("Investment Account");
    Iterator<String> it = Title.iterator();
    while (it.hasNext()) {
      if (INFO.contains(it.next())) {
        flag = true;
      } else {
        flag = false;
      }
    }
    return verifyAllElementExist(page) && flag;
  }

  public void clickViewNamed(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(SummaryHeader.Action.header));
  }

  public boolean isAccountDetailDisplayed() {
    List<WebElement> page = new ArrayList<>();
    page.add(AccountPageHeader);
    page.add(BackBtn);
    return verifyAllElementExist(page);
  }

  @SneakyThrows
  public boolean sortColumn(String Column) {
    Map<String, WebElement> Head = new IdentityHashMap<>();
    for (int i = 0; i < tableHeaders.size(); i++) {
      if (tableHeaders.get(i).getText().equals("Action")) {
        Head.put(SummaryHeader.Action.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.T24ID.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.FNZhead.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.ENGLISH_NAME.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.CHINESE_NAME.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.InvestmentStatus.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.Tradability.header, tableHeaders.get(i++));
        Head.put(SummaryHeader.openDate.header, tableHeaders.get(i));
      }
    }
    boolean flag = false;
    List<String> listMaker = new ArrayList<>();
    switch (Column) {
      case "T24ID":
        clickElement(Head.get("T24ID"));
        clickElement(Head.get("T24ID"));
        Thread.sleep(2 * 1000);
        listMaker.clear();
        for (WebElement row : ListT24) {
          listMaker.add(row.getText());
        }
        for (int i = 0; i < listMaker.size(); i++) {
          int a = i;
          if (i != 0) {
            long ID = Long.parseLong(listMaker.get(i));
            long ID2 = Long.parseLong(listMaker.get(a - 1));
            if (ID2 > ID) {
              flag = true;
            }
          }
        }
        break;
      case "FNZ":
        clickElement(Head.get("FNZhead a/c"));
        clickElement(Head.get("FNZhead a/c"));
        Thread.sleep(2 * 1000);
        clickElement(Head.get("FNZhead a/c"));
        Thread.sleep(2 * 1000);
        listMaker.clear();
        for (WebElement row : ListFNZ) {
          String FNZ = row.getText();
          String num = FNZ.substring(2);
          listMaker.add(num);
        }
        for (int i = 0; i < listMaker.size(); i++) {
          int a = i;
          if (i != 0) {
            int FNZ = Integer.parseInt(listMaker.get(i));
            int FNZ2 = Integer.parseInt(listMaker.get(a - 1));
            if (FNZ2 < FNZ) {
              flag = true;
            }
          }
        }
        break;
      case "staus":
        clickElement(Head.get("Investment a/c status"));
        clickElement(Head.get("Investment a/c status"));
        Thread.sleep(3 * 1000);
        listMaker.clear();
        for (WebElement row : ListInvestment) {
          listMaker.add(row.getText());
        }
        if (listMaker.contains("REJECTED")) {
          flag = true;
        }
        break;
      case "Trad":
        clickElement(Head.get("Tradability"));
        clickElement(Head.get("Tradability"));
        Thread.sleep(3 * 1000);
        listMaker.clear();
        for (WebElement row : ListTrad) {
          listMaker.add(row.getText());
        }
        if (listMaker.contains("TRADABLE")) {
          flag = true;
        }
        break;
      case "date":
        clickElement(Head.get("a/c open date"));
        clickElement(Head.get("a/c open date"));
        Thread.sleep(2 * 1000);
        clickElement(Head.get("a/c open date"));
        Thread.sleep(2 * 1000);
        listMaker.clear();
        for (WebElement row : ListopenDate) {
          listMaker.add(row.getText());
        }
        for (int i = 0; i < listMaker.size(); i++) {
          int a = i;
          if (i != 0) {
            SimpleDateFormat dateFormat =
                    new SimpleDateFormat("MMM d, yyyy, h:m:s aa", Locale.ENGLISH);
            Date datestr = dateFormat.parse(listMaker.get(i));
            Date date = dateFormat.parse(listMaker.get(a - 1));
            if (date.before(datestr)) {
              flag = true;
            }
          }
        }
        break;
    }
    return flag;
  }
  public void clickViewBtn() {
    waitUntilElementClickable(viewActionBtn);
    clickElement(viewActionBtn);
  }
  @SneakyThrows
  public boolean verifySearchByName(String name){
    clearAndSendKeys(searchInput,name);
    waitUntilElementClickable(searchBtn);
    searchBtn.click();
    boolean flagID =verifyViewBtnExist();
    return flagID;
  }


  @SneakyThrows
  public boolean verifyViewBtnExist(){
    Thread.sleep(1000);
    boolean flag = false;
    if(verifyElementExist(viewActionBtn)){
      flag=true;
    }
    return  flag;
  }

  @SneakyThrows
  public boolean verifySearchByCustomerID(String ID){
    clearInput(searchInput);
    waitUntilElementClickable(selectBtn);
    selectBtn.click();
    waitUntilElementClickable(T24IDoption);
    T24IDoption.click();
    clearAndSendKeys(searchInput,ID);
    searchBtn.click();
    boolean flag =verifyViewBtnExist();
    searchAll();
    return flag;
  }


  @SneakyThrows
  public boolean verifySearchByFNZ(String FNZ) {
    selectBtn.click();
    FNZoption.click();
    clearAndSendKeys(searchInput,FNZ);
    searchBtn.click();
    boolean flag =verifyViewBtnExist();
    return flag;
  }


  @SneakyThrows
  public boolean verifyResetBtn(){
    Thread.sleep(1000);
    waitUntilElementClickable(resetBtn);
    resetBtn.click();
    Thread.sleep(1000);
    String text =searchInput.getText();
    if(text.equals("")){
      return true;
    }
    return false;
  }

  @SneakyThrows
  public void searchAll(){
    resetBtn.click();
    searchBtn.click();
  }
  @SneakyThrows
  public boolean verifySearchByDateName(String Date){
    clearInput(searchInput);
    waitUntilElementClickable(selectBtn);
    selectBtn.click();
    Dateoption.click();
    clearAndSendKeys(searchInput,Date);
    searchBtn.click();
    boolean flag =verifyViewBtnExist();
    return flag;
  }
  @SneakyThrows
  public boolean verifySearchByDate(String Date){
    String DateText = Date.substring(0,12).replace(","," ");
    SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy", Locale.ENGLISH);
    Date date2 = sdf.parse(DateText);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(simpleDateFormat.format(date2), dft);
    String actuallyDate =localDate.toString();
    clearInput(searchInput);
    waitUntilElementClickable(selectBtn);
    selectBtn.click();
    Dateoption.click();
    clearAndSendKeys(searchInput,actuallyDate);
    searchBtn.click();
    boolean flag =verifyViewBtnExist();
    return flag;
  }


  public Map<String, WebElement> findSummaryApplicationInCurrentPage(
          String expectText,String type, List<Map<String, WebElement>> applications) throws ParseException {
    for (Map<String, WebElement> application : applications) {
      String actuallyText ="";
      if(type.equals("ID")){
        actuallyText = getElementText(application.get(SummaryHeader.T24ID.header));
      }else if(type.equals("FNZ")){
        actuallyText = getElementText(application.get(SummaryHeader.FNZhead.header));
      }else if(type.equals("openDate")){
        String actuallyTypeText = getElementText(application.get(SummaryHeader.openDate.header)).substring(0,12).replace(","," ");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy", Locale.ENGLISH);
        Date date2 = sdf.parse(actuallyTypeText);
        Date dateEx =sdf.parse(expectText.substring(0,12).replace(","," "));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(simpleDateFormat.format(date2), dft);
        LocalDate localDateEx = LocalDate.parse(simpleDateFormat.format(dateEx), dft);
        actuallyText =localDate.toString();
        expectText = localDateEx.toString();
      }else if(type.equals("EnglishName")){
        String EnglishName = getElementText(application.get(SummaryHeader.ENGLISH_NAME.header));
        actuallyText = EnglishName.split(" ")[1];
      }
      if (expectText.equals(actuallyText)) {
        return application;
      }
    }
    return null;
  }

  @SneakyThrows
  public Map<String, WebElement> findSummaryApplication(String expectText,String type) {
    while (true) {
      List<Map<String, WebElement>> applications = filterSummaryFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());
      Map<String, WebElement> targetApplication =
              findSummaryApplicationInCurrentPage(expectText,type, applications);

      if (targetApplication == null) {
        if (verifyElementExist(disabledNextBtn)) {
          return null;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 3); // wait for loading.
      } else {
        return targetApplication;
      }
    }
  }

}
