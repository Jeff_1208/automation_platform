@Android
Feature: Demo Foreign Exchange On WeLab app

  Background: Login
    Given Open WeLab App
    And Enable skip Root

  Scenario Outline: App return same horizon and portfolio_Build My investment Habit
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month> <user> <password>
    Examples:
      | oneTimeValue | monthlyValue | month |user    | password|
      | 30000        | 4000         | 60    |test194 | Aa123321|

  Scenario Outline: App return same horizon and portfolio_Achieve Financial Freedom
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom set and <age>, <monthlyExpenses> and <otherExpenses> <user> <password>

    Examples:
      | age | monthlyExpenses | otherExpenses        |user  | password  |
      | 40  | 500000          | Budget,Modest,Deluxe |test194 | Aa123321 |

  Scenario Outline: Set My Target goal  through reach my target picture, and finally exit with "quit and save"
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    When I go to Wealth Page
    When I click Add New Goals
    And I click set target and set value <targetValue> and <time> <user> <password>
    Examples:
      | targetValue | time |user  | password  |
      | 30000     | 30   |test194 | Aa123321 |
