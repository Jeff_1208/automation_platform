
package com.welab.automation.projects.wealth.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/mobile/ios/GoalEdit_iOS.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps/mobile"},
    tags = "@WELATCOE-710.2",
    monochrome = true)
public class MobileTestRunner_iOS extends TestRunner {}
