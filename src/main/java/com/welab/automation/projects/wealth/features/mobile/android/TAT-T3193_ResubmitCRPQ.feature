Feature: Goal edit  different trigger or switch states to check the order

  Background: Login with correct user and password
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And Login with user and password
      | user     | test130  |
      | password | Aa123321 |
    And I can see the LoggedIn page



  @WELATCOE-546
  Scenario: Change Options and submit CRPQ in transition page
    When I go to Wealth Page
    And I go to Review Investment Account Profile page
    And I get default CRPQ options
    And I click X to close the page
    And I click Open an account button
    And I verify step 1 is still done
    And I go to Review Investment Account Profile page
    And I verify options have not change
    And I change options
    And I click confirm button in transition page
    And I click by checking the box
    And I click confirm button
    And I click X to close the page
    And I click Open an account button
    And I verify submit data is update











