package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalLoginPage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ApprovalPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.picocontainer.annotations.Inject;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationStatusSteps extends WebBasePage {

    @Inject AdminPortalLoginPage adminPortalLoginPage;
    @Inject AdminPortalMainPage adminPortalMainPage;
    @Inject ApprovalPage approvalPage;

    @And("^checker agreed record in not Maker or Checker's approval list")
    public void checkApprovalList() {
        boolean checkerApplicationStatus = approvalPage.findCheckerApplicationList();
        assertThat(checkerApplicationStatus).isTrue();
        boolean makerApplicationStatus = approvalPage.findMakerApplicationList();
        assertThat(makerApplicationStatus).isTrue();
    }

    @And("^check uploaded documents and Checker is agree decision of maker")
    public void checkUpDocumentAndAgreeMakerDecision() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.agree(targetApplication);
        boolean notEditStatus = approvalPage.NotEditable();
        assertThat(notEditStatus).isTrue();
        approvalPage.checkFileAndSubmit();

    }

    @And("^check uploaded documents and Checker is disagree decision of maker")
    public void checkUpDocumentAndDisagreeMakerDecision() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.disagree(targetApplication);
        boolean notEditStatus = approvalPage.NotEditable();
        assertThat(notEditStatus).isTrue();
        approvalPage.checkFileAndSubmit();

    }


    @And("^checker disagreed record in Maker but not Checker is approval list")
    public void InMakerButNotChecker() {
        boolean checkerApplicationStatus = approvalPage.findCheckerApplicationList();
        assertThat(checkerApplicationStatus).isTrue();
        boolean makerApplicationStatus   = approvalPage.findMakerApplicationNotList();
        assertThat(makerApplicationStatus).isTrue();
    }

    @And("reject the application")
    public void makerRejectApplication() {
        Map<String, WebElement> targetApplication = approvalPage.findMakerNoOperation();
        assertThat(targetApplication).isNotNull();
        approvalPage.rejectApplication(targetApplication);
    }

    @And("approve the application")
    public void makerApproveApplication() {
        Map<String, WebElement> targetApplication = approvalPage.findMakerNoOperation();
        assertThat(targetApplication).isNotNull();
        approvalPage.approveApplication(targetApplication);
    }

    @And("check 'View' button of any maker record and click 'Agree' or 'Disagree', input remark")
    public void checkViewAndClickStatus() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.agree(targetApplication);
    }

    @Then("repeat click agree,input remark and click submit button")
    public void repeatCheckerOperation() {
        approvalPage.repeatOperation() ;
    }

    @And("Switch back Checker A, click 'Submit' button")
    public void backAndClickSubmitButton() {
        boolean errorMessageStatus = approvalPage.backAndVerifyError();
        assertThat(errorMessageStatus).isTrue();
    }

    @And("disagree the application")
    public void againDisagreeApplication() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.disagreeApplication(targetApplication);
    }

    @And("reject pass T24ID the application")
    public void againRejectApplication() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.rejectApplication(targetApplication);
    }

    @And("pass T24ID approve the application")
    public void passT24ApproveApplication() {
        Map<String, WebElement> targetApplication = approvalPage.findApplicationPassT24Id();
        assertThat(targetApplication).isNotNull();
        approvalPage.approveApplication(targetApplication);
    }
}
