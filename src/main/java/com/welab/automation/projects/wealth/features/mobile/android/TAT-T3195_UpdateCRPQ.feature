Feature: Update CRPQ

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  Scenario: Verify client risk profiling section from side menu for scenario of CRPQ not completion
    And Login with user and password
      | user     | testsit059 |
      | password | Aa123321   |
    And I can see the LoggedIn page
    And goto My Account page
    # only approved account can view wealth centre
    Then I Cannot View Wealth Centre
#    And I goto Wealth Center
#    And I view Risk Profile Result
#    When Starting CRPQ
#    And I Select Answer for Can you tell me why you're investing with us
#      | Preserve my capital and earn deposit rate |
#    And I Select Answer for How do you see risk-return
#      | moderate risk to achieve moderate returns |
#    And I Select Answer for I can accept if my investment value fluctuates between
#      | -20% and 20% |
#    And I Select Answer for How much of your total liquid assets
#      | Less than 5% |
#    And I Select Answer for To meet my financial needs in the next 12 months
#      | Sell up to 25% |
#    And I Select Answer for If your investment value dropped by 50%
#      | Invest more to take advantage of lower price |
#    And I Select Answer for How many months of expenses have you put aside
#      | 12 months or above |
#    And Confirm CRPQ answer

  Scenario Outline: CRPQ can be updated when Risk profile is valid and not yet reached the CRPQ trial limit
    # user will be logged out when submitting request, so request first and then log in app.
    # will fetch and store risk score
    Given I Submit CRPQ Answers with <user> <password> CRPQAnswer.json
    And Login with user and password
      | user     | <user>     |
      | password | <password> |
    And I can see the LoggedIn page
    And goto My Account page
    And I goto Wealth Center
    And I view Risk Profile Result
    Then I Check Original Risk Score
    And I Start to Update CRPQ Profile
    And I Select Answer for Can you tell me why you're investing with us
      | Preserve my capital and earn deposit rate |
    And I Select Answer for How do you see risk-return
      | moderate risk to achieve moderate returns |
    And I Select Answer for I can accept if my investment value fluctuates between
      | -20% and 20% |
    And I Select Answer for How much of your total liquid assets
      | Less than 5% |
    And I Select Answer for To meet my financial needs in the next 12 months
      | Sell up to 25% |
    And I Select Answer for If your investment value dropped by 50%
      | Invest more to take advantage of lower price |
    And I Select Answer for How many months of expenses have you put aside
      | 12 months or above |
    And Confirm CRPQ answer
    Then My risk score should be 2

    Examples:
      | user    | password |
      | test111 | Aa123321 |


  Scenario Outline: CRPQ cannot be updated when Risk profile is valid, but reached the CRPQ trial limit
    Given I Submit CRPQ Answers maximum times with <user> <password> CRPQAnswer.json
    And Login with user and password
      | user     | <user>     |
      | password | <password> |
    And I can see the LoggedIn page
    And goto My Account page
    And I goto Wealth Center
    And I view Risk Profile Result
    Then I find I can not Update Risk Profile again
    Examples:
      | user    | password |
      | test021 | Aa123321 |
