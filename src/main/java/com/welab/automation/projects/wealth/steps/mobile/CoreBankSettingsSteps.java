package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.projects.wealth.pages.iao.CoreBankSettingsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.picocontainer.annotations.Inject;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CoreBankSettingsSteps {
    @Inject CoreBankSettingsPage coreBankSettingsPage;

    @Given("update personal info")
    public void updatePersonalInfo(Map<String, String> data) {
        coreBankSettingsPage.updatePersonalInfo();
    }

    @And("update account info")
    public void updateAccountInfo(Map<String, String> data) {
        coreBankSettingsPage.updateEmailAddress(data.get("emailAddress"));
        coreBankSettingsPage.updatePhoneNumber(data.get("phoneNumber"));
    }

    @And("update email address")
    public void updateEmailAddress(Map<String, String> data) {
        coreBankSettingsPage.updateEmailAddress(data.get("emailAddress"));
    }

    @And("update phone number")
    public void updatePhoneNumber(Map<String, String> data) {
        coreBankSettingsPage.updatePhoneNumber(data.get("phoneNumber"));
    }

    @And("update residential address")
    public void updateResidential(Map<String, String> data) {
//        rStreet, rBlock, rFloor, rFlat, sameAsResidentialAddress, mStreet, mBlock, mFloor, mFlat
        coreBankSettingsPage.updateResidentialAddress(
                data.get("rDistrict"), data.get("rStreet"), data.get("rBlock"), data.get("rFloor"), data.get("rFlat"),
                Boolean.parseBoolean(data.get("sameAsResidentialAddress")),
                data.get("mDistrict"), data.get("mStreet"), data.get("mBlock"), data.get("mFloor"), data.get("mFlat"));
    }

    @And("update employment status")
    public void updateEmploymentStatus(Map<String, String> data) {
        coreBankSettingsPage.updateEmploymentInfo(data.get("employmentStatus"));
    }

    @And("update region")
    public void updateRegion(Map<String, String> data) {
        coreBankSettingsPage.updateRegion(data.get("region"));
    }

    @Then("I can find updated personal info")
    public void checkPersonalInfo(Map<String, String> data) {
        coreBankSettingsPage.backToTop();
        assertThat(coreBankSettingsPage.getDisplayedEmailAddress()).isEqualTo(data.get("emailAddress"));
        assertThat(coreBankSettingsPage.getDisplayedPhoneNumber()).isEqualTo(data.get("phoneNumber"));
        assertThat(coreBankSettingsPage.getDisplayedEmploymentStatus()).isEqualTo(data.get("employStatus"));
        assertThat(coreBankSettingsPage.getDisplayedRegion()).isEqualTo(data.get("region"));
    }

    @Then("I can find updated personal basic info")
    public void checkPersonalBasicInfo(Map<String, String> data) {
        //TODO
    }

    @Then("I can find updated email address")
    public void checkAccountInfo(Map<String, String> data) {
        assertThat(coreBankSettingsPage.getDisplayedEmailAddress()).isEqualTo(data.get("emailAddress"));
    }

    @Then("I can find updated phone number")
    public void checkPhoneNumber(Map<String, String> data) {
        assertThat(coreBankSettingsPage.getDisplayedPhoneNumber()).isEqualTo(data.get("phoneNumber"));
    }

    @Then("I can find updated employment status")
    public void checkEmploymentStatus(Map<String, String> data) {
        assertThat(coreBankSettingsPage.getDisplayedEmploymentStatus()).isEqualTo(data.get("employStatus"));
    }

    @Then("I can find updated region")
    public void checkRegion(Map<String, String> data) {
        assertThat(coreBankSettingsPage.getDisplayedRegion()).isEqualTo(data.get("region"));
    }
}
