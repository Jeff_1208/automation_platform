@WELATCOE-232
Feature: Verify tool tips function for goal setting

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test112|Aa123321|

  Scenario Outline: Verify tool tips function for goal setting from begin to model portfolio detail page.
    When I click Add New Goals
    And I Start To Set My Target with <targetValue> and <time>
    Then I check if tips are correct
    And I save my goal and goto Portfolio Recommendation
    Then I check if tips are correct
    And I View Portfolio
    Then I check if tips are correct
    And I View Portfolio Fund List
    Then I check if tips are correct
    And I view Featured Fund
    Then I check if tips are correct
    Then I finish checking all the tip tools
    Examples:
      | targetValue | time |
      | 1000000     | 50   |

  Scenario: Verify tool tips function for goal setting of building investing habit
#  included in other two scenarios


  Scenario Outline: Verify tool tips function for goal setting of financial freedom
    When I click Add New Goals
    And I Start To Set Goal to Achieve Financial Freedom by age <age>
    # have no idea why this tip icon cannot be clicked, so comment here.
    #Then I check if tips are correct
    And I calculate target wealth by <monthlyExpenses> and <otherExpenses>
    Then I check if tips are correct
    Then I finish checking all the tip tools
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |