Feature: Verify investment account is submitted but not opened

  Background: Open WeLab Application
    Given Open WeLab App
    And Enable skip Root


  Scenario: Verify investment account is submitted but not opened
    Given Check upgrade page appears
    And Login with user and password
      | user     | test008  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    When I click Open Account button
    Then I can see pending approval screen
