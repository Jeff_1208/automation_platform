Feature: check jumping flow

  Background: Login and proceed to Wealth verify personal information step
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears

  @WELATCOE-40
  Scenario:Goal setting_Build My Investment Habit Jump Flow: not showing guidance page
    Given Login with user and password
      | user     | test060  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    And I proceed to document upload step
    And exit upload step
    And I click Open Account button
    Then I can verify guidance page is not showing

  @WELATCOE-40
  Scenario:Goal setting_Build My Investment Habit Jump Flow: not showing guidance page
    Given Login with user and password
      | user     | test110  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    Given I proceed to portfolio recommendation page
    And I click Unlock the details button
    Then I can verify guidance page is not showing

  @WELATCOE-214
  Scenario: Goal setting_Build My Investment Habit Jump Flow to B_01_onboarding page
    Given Login with user and password
      | user     | Kimberlie2684529  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I skip guidance
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  @WELATCOE-214
  Scenario:Goal setting_Build My Investment Habit Jump Flow to E_00_upload page
    Given Login with user and password
      | user     | test110  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can find document upload page
      | pageTitle |  Upload document(s) |

  @WELATCOE-214
  Scenario: Goal setting_Build My Investment Habit Jump Flow to G_00_CRPQstart page
    Given Login with user and password
      | user     | Omar82766  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  @WELATCOE-214
  Scenario: Goal setting_Build My Investment Habit Jump Flow to A_06_Pending page
    Given Login with user and password
      | user     | test008  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  @WELATCOE-214
  Scenario: Goal setting_Build My Investment Habit Jump Flow to D_01b_transition page
    Given Login with user and password
      | user     | Tressa9784217  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can see transition page
      | pageTitle | Upload document(s) |

  @WELATCOE-212
  Scenario:Goal setting_Set My Target Jump Flow to B_01_onboarding page
    Given Login with user and password
      | user     | Kimberlie2684529  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    And I skip guidance
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  @WELATCOE-212
  Scenario:Goal setting_Set My Target Jump Flow to E_00_upload page
    Given Login with user and password
      | user     | test060  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can find document upload page
      | pageTitle | Document uploadent(s) |

  @WELATCOE-212
  Scenario: Goal setting_Set My Target Jump Flow to G_00_CRPQstart page
    Given Login with user and password
      | user     | Omar82766  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  @WELATCOE-212
  Scenario: Goal setting_Set My Target Jump Flow to A_06_Pending page
    Given Login with user and password
      | user     | test008  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    And I click X icon

    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  @WELATCOE-212
  Scenario: Goal setting_Set My Target Jump Flow to D_01b_transition page
    Given Login with user and password
      | user     | Tressa9784217  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    And I click X icon

    Then I can see transition page
      | pageTitle | Upload document(s) |
#  Jump to B_01_onboarding page
#  Jump to E_00_upload page
#  Jump to G_00_CRPQstart page
#  Jump to A_06_Pending page
#  Jump to D_01b_transition page

  @WELATCOE-215
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to B_01_onboarding page
    Given Login with user and password
      | user     | Kimberlie2684529  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I skip guidance
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  @WELATCOE-215
  Scenario:Goal setting_Achieve Financial Freedom Jump Flow to E_00_upload page
    Given Login with user and password
      | user     | test060  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  @WELATCOE-215
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to G_00_CRPQstart page
    Given Login with user and password
      | user     | Omar82766  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  @WELATCOE-215
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to A_06_Pending page
    Given Login with user and password
      | user     | test008  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  @WELATCOE-215
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to D_01b_transition page
    Given Login with user and password
      | user     | Tressa9784217  |
      | password | Aa123321 |
    When I can see the LoggedIn page
    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    And I click X icon
    And I click Open Account button
    Then I can see transition page
      | pageTitle | Upload document(s) |