@iOS
Feature: Demo Foreign Exchange On WeLab app

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  Scenario Outline: Select and change available FX currency pairs
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I entered the foreign exchange page
    And I choose the <currency>
    And I enter the <amount>
    And I submitted my foreign exchange order
    And I check Success Done
    Examples:
      | currency | amount |
      | USD      | 33     |

  Scenario: Verify 'Foreign Exchange' button is not shown on transfer page
    And Login with user and password
      | user     | test026  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I check the Foreign Exchange Not Show

  Scenario Outline: Verify exchange rate is expired
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I entered the foreign exchange page
    And I enter amount <amount>
    And I click slide button
    And I check review button displayed
    Examples:
      | amount            |
      | 1000       |

  Scenario Outline: Verify transaction is successful
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I entered the foreign exchange page
    And I enter amount <amount>
    And I slide confirm button
    And I check Success Done
    And I click Success Done
    And I check TotalBalanceArrow
    And I click TotalBalanceArrow
    And I check WealthBalance
    Examples:
      | amount   |
      | 1000     |



  Scenario Outline: Input amount exceeds available balance
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I entered the foreign exchange page
    And I enter amount <amount>
    And I saw the prompt
    And I check slide button disabled
    Examples:
      | amount            |
      | 99999999999       |

  Scenario: Verify 'Foreign Exchange' button is shown on transfer page
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I check the Foreign Exchange Show