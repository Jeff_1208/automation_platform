Feature: Goal Edit

#  Background: Login with correct user and password
#    Given Open WeLab App
#    And Enable skip Root
#    And Check upgrade page appears
#    And Login with user and password
#      | user     | test111  |
#      | password | Aa123321 |
#    And I can see the LoggedIn page
#    When I go to Wealth Page

  @700
  Scenario Outline: Fund redemption_Fund sell overflow_Achieve Financial Freedom
    And I click on track AchieveC
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Proceed button
    And I enter balance <balanceValue>
    And I click Next Button
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button
    Examples:
      | balanceValue |
      | 1000         |

  @698
  Scenario Outline: Fund redemption_Fund sell overflow_Build My investment Habit
    And I click on track buildB
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Proceed button
    And I enter balance <balanceValue>
    And I click Next Button
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button
    Examples:
      | balanceValue |
      | 1000         |



  @696
  Scenario Outline: Fund redemption_Sell all the available balance_Set My Target
    And I click on track reachA
    And I click SellBtn and cancel button
    And I click Sell button
    And I click Proceed button
    And I enter balance <balanceValue>
    And I click Next Button
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button
    Examples:
      | balanceValue |
      | 1000         |

  @684
  Scenario: Fund redemption_Sell all the available balance_Achieve Financial Freedom
    And I click on track reachA
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button only
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button

  @688
  Scenario: Fund redemption_Sell all the available balance_Achieve Financial Freedom
    And I click on track AchieveC
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button only
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button

  @686
  Scenario: Fund redemption_Sell all the available balance_Build My investment Habit
    And I click on track buildB
    And I click Sell button
    And I click Proceed button
    And I click Sell All Button and cancel button
    And I click Sell All Button only
    And I slide to confirm the order
    And I click Completed Done
    And I check detail edit button

  @620
  Scenario: Goal Edit_ Switch but keep existing one _ confirm $ no change in monthly investment and horizon - Set My Target (Case 4a)
    And I click on track reachA
    And I click on track Edit
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click popup confirm
    And I check detail edit button

  @629
  Scenario Outline: Goal Edit_ Switch but keep existing one _ confirm $ with change in monthly investment or horizon - Achieve Financial Freedom (Case 4b)
    And I click on track AchieveC
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click popup confirm
    And I check review confirm order page
    Examples:
      | monthlyValue |
      | 123000       |

  @622
  Scenario: Goal Edit_ Switch but keep existing one _ confirm $ no change in monthly investment and horizon - Achieve Financial Freedom (Case 4a)
    And I click on track AchieveC
    And I click on track Edit
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click popup confirm
    And I check review confirm order page

  @624
  Scenario: Goal Edit_ Switch but keep existing one _ confirm $ no change in monthly investment and horizon - Build My investing Habit (Case 4a)
    And I click on track buildB
    And I click on track Edit
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click popup confirm
    And I check review confirm order page

  @637
  Scenario Outline: Goal Edit_ Switch but keep existing one _ back - Build My investing Habit (Case 4c )
    And I click on track buildB
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click back to home button
    Examples:
      | monthlyValue |
      | 126000       |

  @614
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Set My Target(Case 3 - Switch)
    And I click on track reachA
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | monthlyValue |
      | 12500        |


  @610
  Scenario Outline: Goal Edit_Back to goal setting page when goal parameters fall outside feasible range - Set My Target (Case 2b)
    And I click on track reachA
    And I click on track Edit
    And I check seems like label
    And I click Back Set My Target Page and set <targetValue>, <time> and <monthlyValue>
    And I check view updated recommendation
    Examples:
      | targetValue | time | monthlyValue |
      | 3000000     | 50   | 55000        |

  @606
  Scenario Outline: Goal Edit_No new portfolio is recommended when goal parameters fall outside feasible range - Set My Target (Case 2a)
    And I click on track reachA
    And I click on track Edit
    And I check seems like label
    And I Set My Target Page and set <targetValue>, <time> and <monthlyValue>
    And I check successfully been edited label
    Examples:
      | targetValue | time | monthlyValue |
      | 3000000     | 50   | 55000        |

  @635
  Scenario Outline: Goal Edit_ Switch but keep existing one _ back - Achieve Financial Freedom (Case 4c )
    And I click on track AchieveC
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click View update recommendation
    And I click popup yes
    And I slide to right and click Keep existing one
    And I click back to home button
    Examples:
      | monthlyValue |
      | 120000       |

  @WELATCOE-633
  Scenario Outline: Goal Edit_ Switch but keep existing one _ back - Set My Target (Case 4c )
    And I click on track reachA
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click View update recommendation
    And I slide to right and click Keep existing one
    And I click back to home button
    Examples:
      | monthlyValue |
      | 8900         |

  @618
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Achieve Financial Freedom (Case 3 - Switch)
    And I click on track AchieveC
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click popup yes
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | monthlyValue |
      | 120000       |


  @616
  Scenario Outline: Goal Edit_ Trigger new portfolio recommendation for accept new portfolio - Build My investing Habit (Case 3 - Switch)
    And I click on track buildB
    And I click on track Edit
    And I change Set My Target Page and set <monthlyValue>
    And I click popup yes
    And  I click View New Portfolio and buy it
    And  I review and confirm order util look next button
    Examples:
      | monthlyValue |
      | 126000       |

  @612
  Scenario Outline: Goal Edit_Back to goal setting page when goal parameters fall outside feasible range - Achieve Financial Freedom (Case 2b)
    And I click on track AchieveC
    And I click on track Edit
    And I check seems like label
    And I click Back Set My Target Page and set <targetValue>, <time> and <monthlyValue>
    And I check view updated recommendation
    Examples:
      | targetValue | time | monthlyValue |
      | 3000000     | 50   | 55000        |

  @608
  Scenario Outline:  Goal Edit_No new portfolio is recommended when goal parameters fall outside feasible range - Achieve Financial Freedom (Case 2a)
    And I click on track AchieveC
    And I click on track Edit
    And I check seems like label
    And I Set My Target Page and set <targetValue>, <time> and <monthlyValue>
    And I check successfully been edited label
    Examples:
      | targetValue | time | monthlyValue |
      | 3000000     | 50   | 55000        |

  @WELATCOE-673
  Scenario: set account for WELATCOE-673
    Given I launch app with account
      | account | password |
      | test188 | Aa123321 |

  @WELATCOE-673
  Scenario Outline: No portfolio change when no rebalancing but have changes in monthly investment and/or horizon- Build My Investing Habit
    And I click goal name with <targetName>
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click OK button
    Examples:
      | targetName | monthlyValue |
      | Build685   | 5000         |

  @WELATCOE-710.1
  Scenario: set account
    Given I launch app with account
      | account | password |
      | test188 | Aa123321 |

  @WELATCOE-710.1
  Scenario Outline: View On track of order status
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled
    Examples:
      | orderStatus | monthlyValue |
      | Ontrack     | 0            |

  @WELATCOE-710.2
  Scenario: set account
    Given I launch app with account
      | account | password |
      | test006 | Aa123321 |

  @WELATCOE-710.2
  Scenario Outline: View Off track of order status
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled
    Examples:
      | orderStatus | monthlyValue |
      | Offtrack    | 0            |

  @WELATCOE-710.3
  Scenario: set account
    Given I launch app with account
      | account | password |
      | test096 | Aa123321 |

  @WELATCOE-710.3
  Scenario Outline: View Achieved of order status
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled
    Examples:
      | orderStatus | monthlyValue |
      | Achieved    | 0            |


# hard to prepare this kind of account. Manual testing is to modify the DB to prepare, it is not apply to automation testing
  @WELATCOE-710.4
  Scenario Outline: View Completed of order status
    And  I click the order all of goal with the status of <orderStatus>
    Then I click order status icon
    And  I click standing instruction tab and verify monthly subscription instructions
    Then I click Model portfolio monthly subscription instructions details
    And  I back goal edit page
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click YES button
    And I click confirm button and slide to confirm order
    And I click standing instruction tab and verify  instructions be cancelled
    Examples:
      | orderStatus | monthlyValue |
      | Completed   | 0            |