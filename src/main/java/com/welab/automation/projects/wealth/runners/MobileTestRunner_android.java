
package com.welab.automation.projects.wealth.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/mobile/android/JumpingFlow.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps/mobile"},
    monochrome = true)
public class MobileTestRunner_android extends TestRunner {}
