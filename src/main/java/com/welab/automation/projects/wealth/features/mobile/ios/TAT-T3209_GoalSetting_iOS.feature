@iOS
Feature: Demo Wealth Management On WeLab app


  Scenario: set account
    Given I launch app with account
      | account | password |
      | test111 | Aa123321 |

  @WELATCOE-400
  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Build My investment Habit
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 5
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 110000       | 9000         | 12    |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test194 | Aa123321 |

  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Build My investment Habit
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 2
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 110000       | 9000         | 12    |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test111 | Aa123321 |

  @WELATCOE-504
  Scenario Outline: App return same horizon and portfolio Set Financial Freedom goal
    When I click Add New Goals
    And I click only Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I check InitInvt And MonthlySaving
    And I click save goal
    And I check Label50_65
    And I check MPid
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

  @WELATCOE-501
  Scenario Outline: App return same horizon and portfolio_Build My investment Habit
    #When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I check Label50_65
    And I check MPid
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 110000       | 9000         | 13    |

  @WELATCOE-498
  Scenario Outline: App return same horizon and portfolio_Set My Target
    #When I go to Wealth Page
    When I click Add New Goals
    And I click only Set My Target and set <targetValue> and <time>
    And I check Goal Horizon
    And I click save goal
    And I check Label50_65
    And I check MPid
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test112 | Aa123321 |

  Scenario Outline: Set My Target goal  through set my goal picture, and finally exit with "no, continue"
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click not now to stay in review and confirm order page
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario Outline: Set My Target goal  through set my goal picture, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | targetValue | time |
      | 3000000     | 33   |


  Scenario Outline: Set my goal through goal setting link
    When I click learn more link to create a goal
    And I click Set My Target and set <targetValue> and <time>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click not now to stay in review and confirm order page
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario Outline: Set My Target goal  through picture based on investing habit, and finally exit with "no, continue"
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click not now to stay in review and confirm order page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Set My Target goal  through picture based on investing habit, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Set My Target goal  through picture based on financial freedom, and finally exit with "no, continue"
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click not now to stay in review and confirm order page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  Scenario Outline: Set My Target goal  through picture based on financial freedom, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
#    And I click the discover
    And I can view portfolio
    And I review and confirm order
    And I click X icon
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |


  Scenario Outline: Set My Target goal
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Set My Target
    And I can check the draft target
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test112 | Aa123321 |

  @WELATCOE-223
  Scenario Outline: Set Investing Habit goal
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Build My Investing Habit
    Then I can check the draft target
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |

  @WELATCOE-224
  Scenario Outline: Set Financial Freedom goal
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Financial Freedom
    And I can check the draft target
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

  @WELATCOE-231
  Scenario: Dynamic interval for horizon setting of Build My Investing Habit
    When I click Add New Goals
    And I Start to Build My Investing Habit
    Then I check horizon interval of invest years
      | move to horizon index | expected years in icon |
      | 59                    | 60 months              |
      | 60                    | 5.5 years              |
      | 61                    | 6 years                |
      | 68                    | 9.5 years              |
      | 69                    | 10 years               |
      | 70                    | 11 years               |
      | 93                    | 34 years               |

  @WELATCOE-728
  Scenario: Set my Target for goal setting - exceed max amount - target wealth
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      | 30000001          |              |              | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-728
  Scenario: Set my Target for goal setting - exceed max amount - one time
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      |                   | 30000001     |              | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-728
  Scenario: Set my Target for goal setting - exceed max amount - monthly
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      |                   |              | 3000001      | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-732
  Scenario: Build My investing Habit - exceed max amount - one time
    When I click Add New Goals
    And I Build My investing Habit with exceeded value
      | oneTimeValue | monthlyValue | month |
      | 30000001     |              | 60    |
    Then I see the exceed max amount error message

  @WELATCOE-732
  Scenario: Build My investing Habit - exceed max amount - monthly
    When I click Add New Goals
    And I Build My investing Habit with exceeded value
      | oneTimeValue | monthlyValue | month |
      |              | 3000001      | 60    |
    Then I see the exceed max amount error message

  @WELATCOE-736
  Scenario: Achieve Financial Freedom - exceed max amount - one time
    When I click Add New Goals
    And I Achieve Financial Freedom with exceeded value
      | age | monthlyExpenses | otherExpenses | oneTimeValue | monthlyValue |
      | 60  | 3000            | ,,            | 30000001     |              |
    Then I see the exceed max amount error message

  @WELATCOE-736
  Scenario: Achieve Financial Freedom - exceed max amount - monthly
    When I click Add New Goals
    And I Achieve Financial Freedom with exceeded value
      | age | monthlyExpenses | otherExpenses | oneTimeValue | monthlyValue |
      | 60  | 3000            | ,,            |              | 3000001      |
    Then I see the exceed max amount error message


  @WELATCOE-720
  Scenario Outline: Keep using user's original invest horizon for calculating when create "Achieve Financial Freedom" goal
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio and buy it
    And I click next button
    And I'm validating the confirmation page data
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |


  Scenario: set account
    Given I launch app with account
      | account | password |
      | test111 | Aa123321 |

  @WELATCOE-392
  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Set My Target
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 5
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test194 | Aa123321 |

  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Set My Target
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 2
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test111 | Aa123321 |

  @WELATCOE-402
  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Achieve Financial Freedom
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 5
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

  Scenario: set account
    Given I launch app with account
      | account | password |
      | test188 | Aa123321 |

  Scenario Outline: Verfiy different crr value(1-5) to recommendation different portfolio_Achieve Financial Freedom
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I can view portfolio tab
    Then I check different crr value 2
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

#  @WELATCOE-690 @WELATCOE-692 @WELATCOE-694
  Scenario: set account
    Given I launch app with account
      | account | password |
      | test188 | Aa123321 |

  @WELATCOE-690
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Set My Target
    Given I click goal name with <GoalName>
    And I get the fund names and put it into context
    And I click sell button
    And I click proceed button and set <targetValue>
    And I set selling portfolio name to context
    And I click next button
    And I verify the order data information with <targetValue>
    Examples:
      | targetValue | GoalName |
      | 30          | ReachCCF |

  @WELATCOE-692
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Build My investment Habit
    Given I click goal name with <GoalName>
    And I get the fund names and put it into context
    And I click sell button
    And I click proceed button and set <targetValue>
    And I set selling portfolio name to context
    And I click next button
    And I verify the order data information with <targetValue>
    Examples:
      | targetValue | GoalName |
      | 30          | BuildGGF |


  @WELATCOE-694
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Achieve Financial Freedom
    Given I click goal name with <GoalName>
    And I get the fund names and put it into context
    And I click sell button
    And I click proceed button and set <targetValue>
    And I set selling portfolio name to context
    And I click next button
    And I verify the order data information with <targetValue>
    Examples:
      | targetValue | GoalName |
      | 30          | AC       |

#  @WELATCOE-675.1 @WELATCOE-675.2 @WELATCOE-675.3
  Scenario: set account
    Given I launch app with account
      | account | password |
      | test096 | Aa123321 |

  @WELATCOE-675.1
  Scenario Outline: Verify delete the draft goal _ Reach My Target
    When I click Add New Goals
    And I click Reach My Target button
    And I set the Target name to <targetName>
    And I set <targetValue> and <time>
    And I back to wealth main page from Portfolio Recommend as SMT
    And I delete the draft target
    Then I find target <targetName> no longer exist
    Examples:
      | targetName     | targetValue | time |
      | WELATCOE-675.1 | 3000000     | 50   |

  @WELATCOE-675.2
  Scenario Outline: Set Investing Habit goal
    When I click Add New Goals
    And I click Build My Investing Habit
    And I set the Target name to <targetName>
    And I click save goal
    And I back to wealth main page from Portfolio Recommend as BIH
    And I delete the draft target
    Then I find target <targetName> no longer exist
    Examples:
      | targetName     |
      | WELATCOE-675.2 |

  @WELATCOE-675.3
  Scenario Outline: Set Financial Freedom goal
    When I click Add New Goals
    And I click Achieve Financial Freedom
    And I click save goal
    And I back to wealth main page from Portfolio Recommend as AFF
    And I delete the draft target
    Then I find target <targetName> no longer exist
    Examples:
      | targetName     |
      | WELATCOE-675.2 |
