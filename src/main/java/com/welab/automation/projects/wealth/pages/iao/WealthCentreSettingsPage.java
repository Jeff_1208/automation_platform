package com.welab.automation.projects.wealth.pages.iao;

import com.github.javafaker.Faker;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.By;

import static org.assertj.core.api.Assertions.assertThat;

public class WealthCentreSettingsPage extends AppiumBasePage {

  private String pageName = "Wealth centre settings page";

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Order status']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement orderStatus;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Client risk profiling result']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement clientRiskProfilingResult;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Review My Risk Profile']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement reviewMyRiskProfile;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Tutorial']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement tutorial;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Investment account profile']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement investmentAccountProfile;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Promotion']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement promotion;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Document upload centre']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement documentUploadCentre;

  @AndroidFindBy(accessibility = "UPDATE_CRPQ_RISK_RESULT-btn-title")
  @iOSXCUITFindBy(accessibility = "UPDATE_CRPQ_RISK_RESULT-btn-title")
  private MobileElement updateRiskProfile;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Confirm']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement confirm;

  @AndroidFindBy(accessibility = "UPDATE_CRPQ_RISK_RESULT-level")
  @iOSXCUITFindBy(accessibility = "UPDATE_CRPQ_RISK_RESULT-level")
  private MobileElement score;

  @AndroidFindAll(@AndroidBy(className = "android.widget.ScrollView"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement scrollView;

  public WealthCentreSettingsPage() {
    super.pageName = pageName;
  }

  private void openClientRiskProfilingResultPage() {
    clientRiskProfilingResult.click();
  }

  private void openReviewMyRiskProfilePage() {
    reviewMyRiskProfile.click();
  }

  public int updateRiskProfile() {
    openClientRiskProfilingResultPage();
    updateRiskProfile.click();
    String[] reasons = {
      "Preserve my capital and earn deposit rate",
      "Earn regular income in line with inflation",
      "Balance regular income and capital growth",
      "Grow my capital",
      "Achieve high capital growth"
    };
    Faker faker = new Faker();
    int i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] riskReturns = {
      "minimal risk to achieve minimal returns",
      "low risk to achieve low returns",
      "moderate risk to achieve moderate returns",
      "high risk to achieve high returns",
      "significantly higher risk to maximize returns"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] fluctuations = {
      "-10% and 10%",
      "-20% and 20%",
      "-30% and 30%",
      "-40% and 40%",
      "Lower than -40% and more than 40%"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] liquidAssets = {
      "Less than 5%",
      "5% to less than 15%",
      "15% to less than 35%",
      "35% to less than 50%",
      "50% or above"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] needs = {
      "Sell up to 75%",
      "Sell up to 50%",
      "Sell up to 25%",
      "Sell up to 10%",
      "Don't need to sell any to meet my financial needs"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] valueDrops = {
      "Sell more than 50% of your investment",
      "Sell less than 50% of your investment",
      "Continue to monitor with no immediate action",
      "Invest more to take advantage of lower price",
      "Invest significantly more to take advantage of lower price"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    String[] months = {
      "0 to less than 3 months",
      "3 to less than 6 months",
      "6 to less than 9 months",
      "9 to less than 12 months",
      "12 months or above"
    };
    i = faker.number().numberBetween(0, 4);
    scrollView.findElement(By.xpath("//*[@text='" + reasons[i] + "']")).click();
    confirm.click();
    assertThat(score).isNotNull();
    return Integer.parseInt(score.getText());
  }
}
