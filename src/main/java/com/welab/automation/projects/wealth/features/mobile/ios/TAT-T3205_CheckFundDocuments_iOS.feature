Feature: Check Fund Documents

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test011|Aa123321|

  @351
  Scenario Outline: Check Fund Document in Portfolio Investing Habit goal
    And I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I view first Featured Fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Check Fund Document in Portfolio Set My Target
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I view first Featured Fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | targetValue | time |
      | 1000000     | 60   |

  Scenario Outline: Check Fund Document in Portfolio Financial Freedom goal
    And I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I view first Featured Fund
    Then Verify fund document Prospectus on portfolio and confirm page
    And Verify fund document Factsheet on portfolio and confirm page
    And Verify fund document Product Key Facts on portfolio and confirm page
    And Verify fund document Annual Report on portfolio and confirm page
    And Verify fund document Semi Annual Report on portfolio and confirm page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 60  | 5000          | Budget,Modest,Deluxe |


  @350
  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set My Target
    And I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
   And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | targetValue | time |
      | 300000      | 60   |

  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set Investing Habit goal
    And I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
    And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 1000000      | 4000         | 360   |

  Scenario Outline: Check Fund Document in Product Order Review and Confirm Page_Set Financial Freedom goal
    And I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I can view portfolio
    And I complete order fund
    Then Verify fund document Prospectus on product order review and confirm page
    And Verify fund document Factsheet on product order review and confirm page
    And Verify fund document Product Key Facts on product order review and confirm page
    And Verify fund document Annual Report on product order review and confirm page
    And Verify fund document Semi Annual Report on product order review and confirm page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 60  | 5000          | Budget,Modest,Deluxe |

