package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.framework.utils.ImageComparison;
import com.welab.automation.framework.utils.comparison.model.ImageComparisonResult;
import com.welab.automation.projects.ScenarioContext;
import com.welab.automation.projects.wealth.entities.SellOrder;
import com.welab.automation.projects.wealth.pages.LoginPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingOrderPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingToolTipsPage;
import io.appium.java_client.MobileElement;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;
import lombok.SneakyThrows;
import org.assertj.core.api.SoftAssertions;
import org.picocontainer.annotations.Inject;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;

import static com.welab.automation.framework.GlobalVar.TARGET_SCREENSHOTS_PATH;
import static com.welab.automation.framework.utils.comparison.model.ImageComparisonState.MATCH;
import static org.assertj.core.api.Assertions.assertThat;

public class GoalSettingSteps {

  @Inject LoginPage loginPage;
  @Inject GoalSettingPage goalSettingPage;
  @Inject GoalSettingToolTipsPage goalSettingToolTipsPage;
  @Inject GoalSettingOrderPage goalSettingOrderPage;

  SoftAssertions softAssert = new SoftAssertions();
  private static final String quitWarningString = "Are you sure you want to quit?";
  private static final String reviewAndConfirm = "Review and confirm order";
  private static final String wealthTitle = "Wealth";

  private ScenarioContext context = ScenarioContext.getInstance();

  @Given("I go to Wealth Page")
  public void openWealthPage() {
    loginPage.openWealthPage();
  }

  @SneakyThrows
  @When("I click Add New Goals")
  public void addNewGoals() {
    // everytime it save the goal, there will be a draft. delete them to avoid too many drafts.
    Thread.sleep(1000 * 2);
    goalSettingPage.delDraft();
    goalSettingPage.addNewGoal();
  }

  @When("^I change Set My Target Page and set ([^\"]\\S*)$")
  public void editTargetGoalChangeOnTrack(String monthlyValue) {
    goalSettingPage.editMonthlyInvestment(monthlyValue);
    goalSettingPage.clickViewUpdatedRec();
  }

  @And("I click popup yes")
  public void iClickYes() {
    goalSettingPage.clickYespopup();
  }

  @And("I click popup confirm")
  public void iClickConfirm() {
    goalSettingPage.clickConfirmpopup();
  }

  @And("I check review confirm order page")
  public void iCheckReviewConfirm() {
    assertThat(goalSettingPage.checkReviewConfirmOrder()).isEqualTo(true);
  }

  @And("I check detail edit button")
  public void iCheckDetailEditBtn() {
    assertThat(goalSettingPage.checkDetailEditBtn()).isEqualTo(true);
  }

  @And("I click Completed Done")
  public void iClickCompletedDone() {
    goalSettingPage.clickDone();
  }

  @When("^I click Set My Target and set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addTargetGoal(String targetValue, String time) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.clickReachTarget();
    goalSettingPage.setTargetGoal(targetValue, time);
    if (null == targetValue) targetValue = "1000000";
    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    assertThat(goalSettingPage.getGoalAchieveAmount())
        .isEqualTo(nf.format(Long.valueOf(targetValue)));
    goalSettingPage.saveGoal();
  }

  @And("I click Reach My Target button")
  public void clickReachMyTargetButton() {
    goalSettingPage.clickReachTarget();
  }

  @And("^I set the Target name to ([^\"]\\S*)$")
  public void setNameOfRMT(String name) {
    goalSettingPage.editGoalName(name);
  }

  @Then("^I find target ([^\"]\\S*) no longer exist")
  public void findTargetNotExist(String targetName) {
    // This method only applies to iOS. iOS can retrieve all targets, however android can only
    // retrieve displayed targets.
    List<MobileElement> elements = goalSettingPage.getTargets();
    boolean targetExistence = false;

    for (MobileElement ele : elements) {
      if (goalSettingPage.getElementText(ele).contains(targetName)) {
        targetExistence = true;
        break;
      }
    }
    assertThat(targetExistence)
        .overridingErrorMessage(
            "expected %s not exist, but actual it still exists.", targetExistence)
        .isFalse();
  }

  @When("^I set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void setTargetAndtime(String targetValue, String time) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.setTargetGoal(targetValue, time);
    if (null == targetValue) targetValue = "1000000";
    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    assertThat(goalSettingPage.getGoalAchieveAmount())
        .isEqualTo(nf.format(Long.valueOf(targetValue)));
    goalSettingPage.saveGoal();
  }

  @When("^I click only Set My Target and set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addTargetGoalwithoutSave(String targetValue, String time) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);

    goalSettingPage.setTargetGoal(targetValue, time);

    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    assertThat(goalSettingPage.getGoalAchieveAmount())
        .isEqualTo(nf.format(Long.valueOf(targetValue)));
  }

  @Then("I click save goal")
  public void clickSaveGoal() {
    goalSettingPage.saveGoal();
  }

  @And("I check InitInvt And MonthlySaving")
  public void checkInitInvtAndMonthlySaving() {

    assertThat(goalSettingPage.verifyAFMData()).isEqualTo(true);
  }

  @Then("I can check the risk rate")
  public void checkRiskRate() {
    String riskRateTipContent = goalSettingPage.getRiskRateTips();
    assertThat(riskRateTipContent).isNotBlank();
  }

  @And("I close risk rate tips")
  public void closeRiskRateTips() {
    goalSettingPage.closeToolTips();
  }

  @Then("I can check the morningstar rate")
  public void checkMorningStarRate() {
    String morningstarTipContent = goalSettingPage.getMorningStarRateTips();
    assertThat(morningstarTipContent).isNotBlank();
  }

  @When("^I click Build My Investing Habit and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addInvestingHabitGoal(String oneTimeValue, String monthlyValue, String month)
      throws Exception {
    goalSettingPage.clickBuildHabit();
    goalSettingPage.setInvesting(oneTimeValue, monthlyValue, month);
    assertThat(goalSettingPage.getBIMAgeSliderText())
        .isEqualTo(goalSettingPage.formatInvestTime(month));
    goalSettingPage.saveGoal();
  }

  @And("^I click Build My Investing Habit")
  public void clickBMIBtn() {
    goalSettingPage.clickBuildHabit();
  }

  @When("^I click Achieve Financial Freedom and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addFinancialFreedomGoal(String age, String monthlyExpenses, String otherExpenses) {
    goalSettingPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    assertThat(goalSettingPage.getRetiredAgeText()).isEqualTo("Retire at " + age);
    //    Map<String, String> calculatedFFT = goalSettingPage.getFinancialFreedomTarget();
    //    assertThat(calculatedFFT).isEqualTo(); //todo
    goalSettingPage.getInvestmentPlan();
    goalSettingPage.getMonthlyInvestment();
    goalSettingPage.getOneTimeText();
    goalSettingPage.saveGoal();
  }

  @When("^I click only Achieve Financial Freedom and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addFinancialFreedomGoalwithoutSave(
      String age, String monthlyExpenses, String otherExpenses) {
    goalSettingPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    assertThat(goalSettingPage.getRetiredAgeText()).isEqualTo("Retire at " + age);
  }

  @When("I click Achieve Financial Freedom")
  public void clickAFFAndSetDefault() {
    goalSettingPage.clickAchieveFreedom();
    goalSettingPage.clickContinue();
    goalSettingPage.setDefaultMonthlyExpenses();
    goalSettingPage.calculateTargetWealth();
  }

  @Then("I can view portfolio")
  public void viewPortfolio() {
    goalSettingPage.viewPortfolioOverview();
  }

  @Then("I can view portfolio tab")
  public void viewPortfolioTab() {
    goalSettingPage.viewPortfolioTab();
  }

  @Then("I check different crr value 5")
  public void iCheckdCrrValue() {
    assertThat(goalSettingPage.checkCrrValue(5)).isEqualTo(true);
  }

  @Then("I check different crr value 2")
  public void iCheckdCrrValue2() {
    assertThat(goalSettingPage.checkCrrValue(2)).isEqualTo(true);
  }

  @Then("I can view portfolio and funds")
  public void viewPortfolioAndFunds() {
    goalSettingPage.viewPortfolio();
    goalSettingPage.viewFeatureFunds();
  }

  @And("I can check tool tips of ratings")
  public void checkToolTips() {
    String riskRateTipContent = goalSettingPage.getRiskRateTips();
    assertThat(riskRateTipContent).isNotBlank();
    goalSettingPage.closeToolTips();
    String morningstarTipContent = goalSettingPage.getMorningStarRateTips();
    assertThat(morningstarTipContent).isNotBlank();
    goalSettingPage.closeToolTips();
  }

  @And("I back to wealth main page from Financial Freedom")
  public void backToWealthMainFromFFT() {
    goalSettingPage.backToWealthMainPageFromFFTFund();
  }

  @And("I back to wealth main page from Build My Investing Habit")
  public void backToWealthMainFromBIH() {
    goalSettingPage.backToWealthMainPageFromBIHFund();
  }

  @And("I back to wealth main page from Set My Target")
  public void backToWealthMainFromSMT() {
    goalSettingPage.backToWealthMainPageFromSMTFund();
  }

  @And("I back to wealth main page from Portfolio Recommend as SMT")
  public void backToWealthMainFromPortfolioRecommendAsSMT() {
    goalSettingPage.backToWealthMainPageFromPortfolioRecommendAsSMT();
  }

  @And("I back to wealth main page from Portfolio Recommend as BIH")
  public void backToWealthMainFromPortfolioRecommendAsBIH() {
    goalSettingPage.backToWealthMainPageFromPortfolioRecommendAsBIH();
  }

  @And("I back to wealth main page from Portfolio Recommend as AFF")
  public void backToWealthMainFromPortfolioRecommend() {
    goalSettingPage.backToWealthMainPageFromPortfolioRecommendAsAFF();
  }

  @And("I can check the draft target")
  public void checkAndDelDraft() {
    // TODO assert target wealth.

    // remove draft to void too many draft.
    goalSettingPage.delDraft();
  }

  @Then("^I can view recommendation$")
  public void viewRecommendation() {
    // check one time value & monthly value
    boolean correct = goalSettingPage.viewRecommendation();
    assertThat(correct).isTrue();
  }


  //    @And("I can view portfolio")
  //    public void iCanViewPortfolio() {
  //      // check target value
  //      assertThat(goalSettingPage.viewPortfolio2()).isTrue();
  //    }

  @Then("^I check if tips are correct")
  public void checkTips() {
    Map<String, String> resultMap = goalSettingToolTipsPage.checkTips();
    softAssert
        .assertThat(Boolean.valueOf(resultMap.get("status")))
        .as(resultMap.get("message"))
        .isTrue();
  }

  @When("^I Start To Set My Target")
  public void startSetMyTarget(Map<String, String> data) throws Exception {
    goalSettingPage.startSetMyTarget(data.get("age"));
  }

  @And("^I Start To Set My Target with ([^\"]\\S*) and ([^\"]\\S*)")
  public void setMyTarget(String targetValue, String time) {
    goalSettingPage.setTargetGoal(targetValue, time);
    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.SetMyTarget);
  }

  @SneakyThrows
  @And("I save my goal and goto Portfolio Recommendation")
  public void saveMyGoal() { // String targetValue
    goalSettingPage.clickContinue();
    Thread.sleep(1000 * 3);
    goalSettingPage.saveGoal();

    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.Recommendation);
  }

  @And("I View Portfolio")
  public void viewPortfolioOverView() {
    goalSettingPage.viewPortfolioOverview();
    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.PortfolioOverView);
  }

  @And("I delete the draft target")
  public void delDraft() {
    goalSettingPage.delDraft();
  }

  @And("I View Portfolio Fund List")
  public void viewPortfolioFundList() {
    goalSettingPage.viewPortfolioTab();
    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.Portfolio);
  }

  @And("I view Featured Fund")
  public void viewFeatureFund() {
    goalSettingPage.viewFeatureFunds();
    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.FeaturedFund);
  }

  @And("I view first Featured Fund")
  public void viewFirstFeatureFund() {
    goalSettingPage.viewFirstFeatureFund();
  }

  @And("^I Start To Set Goal to Achieve Financial Freedom by age ([^\"]\\S*)")
  public void setGoalOfAchieveFinancialFreedom(String age) {
    goalSettingPage.setFinancialFreedomGoal(age);
    // assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.FinancialFreedom);
  }

  @And("^I calculate target wealth by ([^\"]\\S*) and ([^\"]\\S*)")
  public void saveGoalOfFinancialFreedom(String monthlyExpenses, String otherExpenses) {
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    //
    // GoalSettingToolTipsPage.setSubPage(GoalSettingToolTipsPage.PagesHaveTips.FinancialFreedomSave);
  }

  @And("I click View recommendation")
  public void clickViewRecommendation() {
    goalSettingPage.saveGoal();
  }

  @Then("I finish checking all the tip tools")
  public void finshCheckTipTools() {
    softAssert.assertAll();
  }

  @And("I click the discover")
  public void iClickTheDiscover() throws InterruptedException {
    goalSettingPage.clickDiscoverAndViewPortfolio();
  }



  @And("I click the portfolio  and  review Fund list")
  public void iReviewFundWeight() throws InterruptedException {
    int portfolioNumber = goalSettingPage.getPortNumber();
    String riskRate = goalSettingPage.getRiskRate();
    String[] getRiskRateString = riskRate.split("Rating ");
    int getRiskRateNumber = Integer.parseInt(getRiskRateString[1]);
    switch (getRiskRateNumber) {
      case 1:
        assertThat(portfolioNumber).isLessThanOrEqualTo(2);
        break;
      case 2:
        assertThat(portfolioNumber).isLessThanOrEqualTo(6);
        break;
      case 3:
        assertThat(portfolioNumber).isLessThanOrEqualTo(10);
        break;
      case 4:
      case 5:
        assertThat(portfolioNumber).isLessThanOrEqualTo(12);
        break;
    }
    boolean fundSumNumber = goalSettingPage.checkFundWeight();
    assertThat(fundSumNumber).isTrue();
  }

  @And("I review and confirm order")
  public void iReviewAndConfirmOrder() {
    String portfolioName = goalSettingPage.getPortfolioName();
    // Buy
    String actualPortfolioName = goalSettingPage.getJumpPagePortfolioName();
    assertThat(actualPortfolioName).startsWith(portfolioName);
    goalSettingPage.reviewOrder();
  }

  @And("I click X to back read my target")
  public void iClickXToBackReadMyTarget() {
    Boolean returnResult = goalSettingPage.clickXBackReachMyTarget();
    assertThat(returnResult).isTrue();
  }

  @And("I click X to close the review page")
  public void iClickXToCloseTheReviewPage() {
    String quitWarningStringActual = goalSettingPage.clickXGetQuitWarning();
    assertThat(quitWarningStringActual).isEqualTo(quitWarningString);
  }

  @When("I click learn more link to create a goal")
  public void iClickLearnMoreLinkToCreateAGoal() {
    goalSettingPage.delDraft();
    goalSettingPage.clickLearnMoreLinkToCreateGoal();
  }

  @And("I click not now to stay in review and confirm order page")
  public void iClickNoNowToStayInReviewAndConfirmOrderPage() {
    String reviewOrder = goalSettingPage.clickNoNow();
    assertThat(reviewOrder).isEqualTo(reviewAndConfirm);
  }

  @And("I click quit and save, then back to wealth page")
  public void iClickQuitAndSaveThenBackToWealthPage() {
    String reviewOrder = goalSettingPage.clickQuitAndSave();
    assertThat(reviewOrder).isEqualTo(wealthTitle);
  }

  @And("I check the new goal status is draft")
  public void iCheckTheNewGoalStatusIsPending() {
    Boolean goalStatus = goalSettingPage.getGoalStatus();
    assertThat(goalStatus).isTrue();
  }

  @When("I complete order fund")
  public void iCompleteOrderFund() {
    goalSettingOrderPage.orderPlacement();
  }

  @Then("^Verify fund document (.*) on (.*) and confirm page$")
  public void verifyFundDocumentOnProductOrderReviewAndConfirmPage(String docName, String name)
      throws InterruptedException {
    if (System.getProperty("mobile").equalsIgnoreCase("android")) {
      String pdfNo = goalSettingOrderPage.getFundDoc(docName);
      assertThat(pdfNo.contains("0 / 0")).isFalse();
    }
    if (System.getProperty("mobile").equalsIgnoreCase("ios")) {
      goalSettingOrderPage.getFundDoc2(docName);
      ImageComparison imageComparison =
          new ImageComparison(
                  "src/main/resources/expectedScreenShot/ios/" + docName + ".png",
                  TARGET_SCREENSHOTS_PATH + docName + ".png")
              .setAllowingPercentOfDifferentPixels(10);
      File file = new File("target/screenshots/" + docName + ".png");
      ImageComparisonResult imageComparisonResult =
          imageComparison.compareImages().writeResultTo(file);
      assertThat(imageComparison.getActual()).isNotNull();
      assertThat(imageComparison.getExpected()).isNotNull();
      Allure.step("compare result is match");
      assertThat(imageComparisonResult.getImageComparisonState()).isEqualTo(MATCH);
    }
  }

  @When("^I Start to Build My Investing Habit")
  public void startBuildInvestHabit() throws Exception {
    goalSettingPage.startBuildInvestHabit();
  }

  @Then("^I check horizon interval of invest years")
  public void checkInvestHabitYearsInterval(List<List<String>> paras) {
    boolean allResult = true;
    // the first line is not test data
    for (int i = 1; i < paras.size(); i++) {
      // if one test data is failed, continue testing with next data
      boolean result =
          goalSettingPage.checkInvestHabitYearsInterval(paras.get(i).get(0), paras.get(i).get(1));

      if (!result) {
        allResult = false;
      }
    }
    assertThat(allResult).isTrue();
  }

  @And("I start to check fund document in featured fund")
  public void scrollToFundDocument() {
    boolean result = goalSettingPage.scrollToLastDocumentInFeaturedFund();
    assertThat(result).isTrue();
  }

  @SneakyThrows
  @And("I setup Build My Investing Habit")
  public void iSetupBuildMyInvestingHabit(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String oneTimeValue = data.get(0).get("oneTimeValue");
    String monthlyValue = data.get(0).get("monthlyValue");
    String month = data.get(0).get("month");
    goalSettingPage.setInvestingHabitGoal(oneTimeValue, monthlyValue, month);
    assertThat(goalSettingPage.getBIMAgeSliderText())
        .isEqualTo(goalSettingPage.formatInvestTime(month));
    goalSettingPage.saveGoal();
  }

  @SneakyThrows
  @And("I setup Build My Investing Habit with name")
  public void iSetupBuildMyInvestingHabitWithName(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String name = data.get(0).get("name");
    String oneTimeValue = data.get(0).get("oneTimeValue");
    String monthlyValue = data.get(0).get("monthlyValue");
    String month = data.get(0).get("month");
    goalSettingPage.setInvestingHabitGoal(oneTimeValue, monthlyValue, month, name);
    assertThat(goalSettingPage.getBIMAgeSliderText())
        .isEqualTo(goalSettingPage.formatInvestTime(month));
    goalSettingPage.saveGoal();
  }

  @And("I check the portfolio and buy it")
  public void reachMyTarget() {
    goalSettingPage.InsufficientAmountWhenHkdBuyUsdProduct();
  }

  @And("I can see there is Insufficient amount in your account")
  public void investingHabitGoal() {
    goalSettingPage.InsufficientAmountInYourAccount();
  }

  @And("I setup my target")
  public void iSetupMyTarget(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String targetValue = data.get(0).get("targetValue");
    String time = data.get(0).get("time");
    addTargetGoal(targetValue, time);
  }

  @And("I setup my target with name")
  public void iSetupMyTargetWithName(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String name = data.get(0).get("name");
    String targetValue = data.get(0).get("targetValue");
    String time = data.get(0).get("time");
    String oneTime = data.get(0).get("oneTimeValue");
    String monthly = data.get(0).get("monthlyValue");
    goalSettingPage.addTargetGoalWithInvestmentPlan(targetValue, time, oneTime, monthly, name);
  }

  @And("The status of new goal setting is draft")
  public void theStatusOfNewGoalSettingIsDraft() {
    assertThat(goalSettingPage.getDraftBtns().size()).isGreaterThan(0);
  }

  @And("I click Goal of Set My Target Goal")
  public void iclickGoalofSetMyTargetGoal() {
    goalSettingPage.clickMyTargetGoal();
  }

  @And("I click Goal of Achieve Financial")
  public void iclickGoalofAchieveFinancial() {
    goalSettingPage.clickAFFGoal();
  }

  @And("I click Goal of Build My investing Habit")
  public void iclickGoalofBuildMyinvestingHabit() {
    goalSettingPage.clickBMIHGoal();
  }

  @When("I click Edit Button and set ([^\"]\\S*)$")
  public void editGoalName(String GoalName) {
    goalSettingPage.clickGoalNameEditButton();
    goalSettingPage.editGoalName(GoalName);
  }

  @And("I click button back to WealthMainPage")
  public void backToWealthMainPage() {
    goalSettingPage.backToWealthMainPageFromGoalPage();
  }

  @And("I can check changed Goal Name of Set My Target")
  public void goalNameHasbeenChanged() {
    boolean result = goalSettingPage.scrollToFindSetMyTargetName();
    assertThat(result).isTrue();
  }

  @When("^I click set my target and set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addtargetgoal(String targetValue, String time) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);

    goalSettingPage.setTargetGoal(targetValue, time);

    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
  }

  @And("I click monthly investment Edit Button and set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void changeWealthAndTime(String newtargetValue, String newtime) {
    goalSettingPage.GoalHorizon();
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);

    goalSettingPage.setTargetGoal(newtargetValue, newtime);

    assertThat(goalSettingPage.getTargetWealth())
        .isEqualTo(nf.format(Long.valueOf(newtargetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(newtime);
    goalSettingPage.clickContinue();
    goalSettingPage.saveGoal();
  }

  @And("I click Goal of Set My Target Goal Draft")
  public void iclickGoalofSetMyTargetGoalDraft() {
    goalSettingPage.clickMyTargetGoalDraft();
  }

  @And("I click on track reachA")
  public void iclickOnTrackReachA() {
    goalSettingPage.clickOnTrackReachA();
  }

  @And("I click on track buildB")
  public void iclickOnTrackBuildB() {
    goalSettingPage.clickOnTrackBuildB();
  }

  @And("I click on track AchieveC")
  public void iclicknTrackAchieveC() {
    goalSettingPage.clickOnTrackAchieveC();
  }

  @And("I click on track Edit")
  public void iclickOnTrackEdit() {
    goalSettingPage.clickOnTrackEdit();
  }

  @And("I click Goal of Achieve Financial Draft")
  public void iclickGoalofAchieveFinancialDraft() {
    goalSettingPage.clickAFFGoalDraft();
  }

  @And("I click Goal of Build My investing Habit Draft")
  public void iclickGoalofBuildMyinvestingHabitDraft() {
    goalSettingPage.clickBMIHGoalDraft();
  }

  @When(
      "^I click edit Button At Set My Target Page and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void editTargetGoal(String targetValue, String time, String monthlyValue) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.editSetMyTargetGoal(targetValue, time);
    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    goalSettingPage.editMonthlyInvestment(monthlyValue);
    goalSettingPage.saveGoal();
  }

  @And("I check seems like label")
  public void iCheckSeemsLikeLabel() {
    goalSettingPage.verifySeemLikeLabel();
  }

  @When("^I Set My Target Page and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void editTargetGoalonTrack(String targetValue, String time, String monthlyValue) {
    goalSettingPage.clickViewUpdatedRec();
    goalSettingPage.clickYespopup();
    goalSettingPage.clickConfirmpopup();
    goalSettingPage.clickNext();
    goalSettingPage.clickByChecing();
    goalSettingOrderPage.slideToConfirmOrder();
  }

  @When("^I click Back Set My Target Page and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void editTargetGoalonTrackBack(String targetValue, String time, String monthlyValue) {
    goalSettingPage.clickViewUpdatedRec();
    goalSettingPage.clickYespopup();
    goalSettingPage.clickBackpopup();
  }

  @And("I check successfully been edited label")
  public void iCheckBeenEditedLabel() {
    assertThat(goalSettingPage.verifyBeenEdited()).isTrue();
  }

  @And("I check view updated recommendation")
  public void iCheckViewUpdatedRecDisplay() {
    assertThat(goalSettingPage.checkViewUpdatedBtn()).isTrue();
  }

  @When(
      "^I click edit Button At Freedom Goal Page and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void editFreedomGoal(String age, String monthlyExpenses, String otherExpenses) {
    goalSettingPage.editFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    goalSettingPage.saveGoal();
  }

  @And("I back to wealth main page from edit Page")
  public void backToWealthMainFromeditPage() {
    goalSettingPage.backToWealthMainPageFromeditPage();
  }

  @When(
      "^I click edit Button At Build My Investing Habit and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void editInvestingHabitGoal(String oneTimeValue, String monthlyValue, String month)
      throws Exception {
    goalSettingPage.editInvestingHabitGoal(oneTimeValue, monthlyValue, month);
    assertThat(goalSettingPage.getBIMAgeSliderText())
        .isEqualTo(goalSettingPage.formatInvestTime(month));
    goalSettingPage.saveGoal();
  }

  @And("I check Goal Horizon")
  public void iCheckGoalHorizon() {
    assertThat(goalSettingPage.checkGoalHorizon()).isEqualTo(true);
  }

  @And("I check Label50_65")
  public void iCheckLabel50_65() {
    assertThat(goalSettingPage.checkLabel50_65()).isEqualTo(true);
  }

  @And("I check MPid")
  public void iCheckMP5() {
    assertThat(goalSettingPage.checkMP5()).isEqualTo(true);
  }

  @And("I click edit button at Goal Page")
  public void iclickeditbutton() {
    goalSettingPage.clickBtnAtGoalPage();
  }

  @When("I open the goal setting")
  public void iOpenTheGoalSetting(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String name = data.get(0).get("name");
    goalSettingPage.scrollToFindName(name);
  }

  @And("I edit investment plan")
  public void iEditInvestmentPlan(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String oneTime = data.get(0).get("oneTimeValue");
    String monthly = data.get(0).get("monthlyValue");
    goalSettingPage.setInvestmentPlan(oneTime, monthly);
  }

  @And("I edit investment plan for investing habit")
  public void iEditInvestmentPlanForInvestingHabit(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String oneTime = data.get(0).get("oneTimeValue");
    String monthly = data.get(0).get("monthlyValue");
    goalSettingPage.setInvestmentPlan(oneTime, monthly);
  }

  @Then("I see the exceed max amount error message")
  public void iSeeTheExceedMaxAmountErrorMessage() {
    assertThat(goalSettingPage.isExceedMaxAmountErrorMsgExist()).isTrue();
  }

  @And("I setup my target with exceeded value")
  public void iSetupMyTargetWithExceededValue(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String targetWealthValue = data.get(0).get("targetWealthValue");
    String oneTime = data.get(0).get("oneTimeValue");
    String monthly = data.get(0).get("monthlyValue");
    String time = data.get(0).get("time");
    boolean isNotExceedMaxValue = goalSettingPage.setTargetGoal(targetWealthValue, time);
    if (isNotExceedMaxValue) {
      goalSettingPage.clickContinue();
      isNotExceedMaxValue = goalSettingPage.setInvestmentPlan(oneTime, monthly);
    }
    assertThat(isNotExceedMaxValue).isFalse();
  }

  @And("I Build My investing Habit with exceeded value")
  public void iBuildMyInvestingHabitWithExceededValue(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String oneTimeValue = data.get(0).get("oneTimeValue");
    String monthlyValue = data.get(0).get("monthlyValue");
    String month = data.get(0).get("month");
    assertThat(goalSettingPage.setInvestingHabitGoal(oneTimeValue, monthlyValue, month)).isFalse();
  }

  @And("I Achieve Financial Freedom with exceeded value")
  public void iAchieveFinancialFreedomWithExceededValue(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String age = data.get(0).get("age");
    String monthlyExpenses = data.get(0).get("monthlyExpenses");
    String otherExpenses = data.get(0).get("otherExpenses");
    String oneTimeValue = data.get(0).get("oneTimeValue");
    String monthlyValue = data.get(0).get("monthlyValue");
    goalSettingPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    assertThat(goalSettingPage.setInvestmentPlan(oneTimeValue, monthlyValue)).isFalse();
  }

  @And("I delete and verify Set My target Draft")
  public void iDeleteAndVerifySetMyTarget() {
    goalSettingPage.iClickDeleteAndVerifySMTDraft();
    boolean SMTName = goalSettingPage.iVerifySMTRemoved();
    assertThat(SMTName).isTrue();
  }

  @And("I delete and verify Build My Investing Habit")
  public void iClickDeleteAndVerifyBMIHraft() {
    goalSettingPage.iClickDeleteAndVerifyBMIHDraft();
    boolean BMIHName = goalSettingPage.iVerifyBMIHRemoved();
    assertThat(BMIHName).isTrue();
  }

  @And("I delete and verify Achieve Financial Freedom")
  public void iClickDeleteAndVerifyAFFDraft() {
    goalSettingPage.iClickDeleteAndVerifyAFFDraft();
    boolean AFFName = goalSettingPage.iVerifyAFFemoved();
    assertThat(AFFName).isTrue();
  }

  @And("I click Goal of Build My Target Draft")
  public void iclickGoalofDraftSetMyTargetName() {
    goalSettingOrderPage.clickDraftSetMyTargetName();
  }

  @When("^I click edit Set My Target and set ([^\"]\\S*) and ([^\"]\\S*) ([^\"]\\S*)$")
  public void clickEitTargetGoal(String targetValue, String time, String str) {
    Calendar cal = Calendar.getInstance();
    int month = cal.get(Calendar.MONTH) + 1;
    List<String> list = new ArrayList<String>();
    int year = cal.get(Calendar.YEAR);
    String[] strArr = str.split("\\.");
    for (int i = 0; i < strArr.length; ++i) {
      list.add(strArr[i]);
    }

    int age = year - Integer.parseInt(list.get(0));
    int expect = (Integer.parseInt(time) - age - 1) + (Integer.parseInt(list.get(2)) - month);
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.clickTargetGoal(targetValue, time);
    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    assertThat(goalSettingPage.getGoalAchieveAmount())
        .isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.checkGoalHorizon()).isEqualTo(true);
    goalSettingPage.getInvestmentPlan();
    goalSettingPage.getMonthlyInvestment();
    goalSettingPage.getActualValue();
    String GoalHorizon = goalSettingPage.getGoalHorizon();
    assertThat(GoalHorizon.contains(String.valueOf(expect))).isTrue();
    goalSettingPage.saveGoal();
    Boolean textExists = goalSettingPage.getText();
    assertThat(textExists).isTrue();
  }

  @When("^I click Set My Target and verification set ([^\"]\\S*) and ([^\"]\\S*) ([^\"]\\S*)$")
  public void addTargetGoalCalculation(String targetValue, String time, String str) {
    Calendar cal = Calendar.getInstance();
    int month = cal.get(Calendar.MONTH) + 1;
    List<String> list = new ArrayList<String>();
    int year = cal.get(Calendar.YEAR);
    String[] strArr = str.split("\\.");
    for (int i = 0; i < strArr.length; ++i) {
      list.add(strArr[i]);
    }
    int age = year - Integer.parseInt(list.get(0));
    int expect = (Integer.parseInt(time) - age - 1) + (Integer.parseInt(list.get(2)) - month);
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.setTargetGoal(targetValue, time);
    assertThat(goalSettingPage.getTargetWealth()).isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.getSMTAge()).isEqualTo(time);
    goalSettingPage.clickContinue();
    assertThat(goalSettingPage.getGoalAchieveAmount())
        .isEqualTo(nf.format(Long.valueOf(targetValue)));
    assertThat(goalSettingPage.checkGoalHorizon()).isEqualTo(true);
    goalSettingPage.getActualValue();
    String GoalHorizon = goalSettingPage.getGoalHorizon();
    assertThat(GoalHorizon.contains(String.valueOf(expect))).isTrue();
    goalSettingPage.getInvestmentPlan();
    goalSettingPage.getMonthlyInvestment();
    goalSettingPage.getOneTimeText();
    goalSettingPage.saveGoal();
    Boolean Exists = goalSettingPage.getText();
    assertThat(Exists).isTrue();
  }

  @And("I'm validating the confirmation page data")
  public void validatingConfirmation() {
    assertThat(goalSettingPage.determineTheAmountOf()).isEqualTo(true);
  }

  @When("^I input Achieve Financial Freedom and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void inputFinancialFreedomGoal(String age, String monthlyExpenses, String otherExpenses) {
    goalSettingPage.setFinancialFreedom(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    assertThat(goalSettingPage.getRetiredAgeText()).isEqualTo("Retired at " + age);
    goalSettingPage.getInvestmentPlan();
    goalSettingPage.getMonthlyInvestment();
    goalSettingPage.getOneTimeText();
    goalSettingPage.saveGoal();
    assertThat(goalSettingPage.getText()).isEqualTo(true);
  }

  @When("^I click Build Investing Habit and set ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void InvestingHabitGoal(String oneTimeValue, String monthlyValue, String month)
      throws Exception {
    goalSettingPage.setInvestingHabitGoal(oneTimeValue, monthlyValue, month);
    assertThat(goalSettingPage.getBIMAgeSliderText())
        .isEqualTo(goalSettingPage.formatInvestTime(month));
    goalSettingPage.saveGoal();
    Boolean textExists = goalSettingPage.getText();
    assertThat(textExists).isTrue();
  }

  @And("I validate order confirmation page")
  public void validatehMyTarget() {
    goalSettingPage.pullDownToVerifyTheOriginalData();
  }

  @And("I get Wealth Current Page Wealth")
  public void iGetWealthPageWealth() {
    goalSettingPage.iGetWealthCurrentWealth();
  }

  @When("^I click add Reach My Target and set ([^\"]\\S*) and ([^\"]\\S*)$")
  public void addReachMyTargetGoal(String targetValue, String time) {
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.CHINA);
    goalSettingPage.setTargetGoal(targetValue, time);
    goalSettingPage.clickContinue();
    goalSettingPage.saveGoal();
  }

  @And("I click the discover and view portfolio")
  public void iClickTheDiscoverAndPorfolico() throws InterruptedException {
    goalSettingPage.clickDiscoverAndViewPortfolio();
  }

  @And("I click buy button and slide to confirm order")
  public void iClickConfirmAndSlideToConfirm() {
    goalSettingPage.iClickConfirmAndSlideToConfirm();
    goalSettingPage.clickElement(goalSettingPage.getDone());
  }

  @And("I verify wealth page wealth update")
  public void iVerifyCurrentWealthChange() {
    boolean CurrentWealth = goalSettingPage.iVerifyCurrentWealthUpdate();
    assertThat(CurrentWealth).isTrue();
  }

  @And("I click Build My Investing Habit On track Button")
  public void iClickBMIHOntrack() {
    goalSettingPage.iClickBMIHOntrack();
  }

  @And("I click Set My Target On track Button")
  public void iClickSMTOnTrackBtn() {
    goalSettingPage.iClickSMTOntrack();
  }

  @And("I click Achieve Financial Freedom On track Button")
  public void iCickAFFOnTrackBtn() {
    goalSettingPage.iClickAFFOntrack();
  }

  @And("I clicked my target goal with the status of on track")
  public void iClickedMyTargetOnTrack() {
    goalSettingPage.clickMyTargetOnTrack();
  }

  @And("I clicked Achieve Financial Freedom goal with the status of on track")
  public void iClickAchieveFinancialFreedomOnTrack() {
    goalSettingPage.clickAchieveFinancialFreedomOnTrack();
  }

  @And("I clicked Build My Investing Habit with the status of on track")
  public void clickBuildMyInvestingHabitOnTrack() {
    goalSettingPage.clickBuildMyInvestingHabitOnTrack();
  }

  @And("I click sell button")
  public void iClickSellButton() {
    goalSettingPage.clickSellBtn();
    goalSettingPage.clickProceedBtn();
  }

  @And("I click proceed button and set ([^\"]\\S*)$")
  public void setSellInputValue(String targeValue) {
    goalSettingPage.enterAmount(targeValue);
  }

  @And("I set selling portfolio name to context")
  public void getSellingPortfolioName() {
    goalSettingPage.setSellingPortfolioName2Context(context);
  }

  @And("I verifying the order data information")
  public void verifyingOrderDataInformation() {
    assertThat(goalSettingPage.verifyingOrderData()).isTrue();
  }

  @And("I get the fund names and put it into context")
  public void putFundNames2Context() {
    goalSettingPage.clickElement(goalSettingPage.getOnTrackBtn());
    ArrayList<String> fundNames = goalSettingPage.getPortfolioFundList();
    context.setScenarioData("fundList", fundNames);
    goalSettingPage.clickElement(goalSettingPage.getHeaderBackBtn());
  }

  @And("I verify the order data information with ([^\"]\\S*)$")
  public void verifyOrderDataInformation(String targetValue) {
    SellOrder sellOrder = goalSettingPage.getSellOrderInfo();

    // verify portfolioName and estimated sell amount.
    assertThat(sellOrder.getPortfolioName()).isEqualTo(context.getScenarioData("portfolioName"));
    assertThat(Double.parseDouble(sellOrder.getEstimatedSellAmount()))
        .isEqualTo(Double.parseDouble(targetValue));

    // verify fund names
    ArrayList<String> fundList = (ArrayList<String>) context.getScenarioData("fundList");
    for (String s1 : fundList) {
      System.out.println(s1);
    }
    for (SellOrder.Fund fund : sellOrder.getFunds()) {
      System.out.println(fund.getFundName());
    }

    assertThat(sellOrder.getFunds().size())
        .overridingErrorMessage(
            "expected %s funds, but actual %s funds", fundList.size(), sellOrder.getFunds().size())
        .isEqualTo(fundList.size());
    for (SellOrder.Fund fund : sellOrder.getFunds()) {
      assertThat(fund.getFundName()).isIn(fundList);
    }

    // checked with Dylan Wang, No need to check the accuracy of the decimal
  }

  @And("I click View New Portfolio")
  public void iClickViewNewPortfolio() {
    goalSettingPage.iClickViewNewPortfoloi();
  }

  @And("I verify order condition is changed")
  public void iVerifyOrderIsChanged(){
    Boolean ProcessingBtn = goalSettingOrderPage.verifyGoalIsProcessing();
    assertThat(ProcessingBtn).isTrue();
  }
}
