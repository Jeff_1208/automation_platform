package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth.entities.NoneCRPQ;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class NoneCRPQPage extends AppiumBasePage {

  private final HashMap<String, MobileElement> noneCRPQQuestionMap;
  private final Map<String, List<MobileElement>> noneCRPQAnswersMap;
  private final Map<String, MobileElement> noneCRPQVerifyMap;
  private String defaultDeclarations;
  private String defaultQualificationInformations;
  private String defaultInvestmentHorizons;
  private String defaultKnowledgeAndExperiences;
  private String defaultNetWorths;

  @Getter
  @AndroidFindBy(accessibility = "-btn-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Got it!'")
  private MobileElement gotItTab;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[3]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'btnGoalCard'")
  private MobileElement goalBasePicture;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Skip to open an account\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'skipTipText'")
  private MobileElement skipLink;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'WELCOME_TO_MUTUAL_FUND-title'")
  private MobileElement mutualFundTitle;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'WELCOME_TO_MUTUAL_FUND-btn'")
  private MobileElement mutualFundNextButton;

  @AndroidFindBy(accessibility = "PROCESS_GUIDE-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement processGuideNext;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Cancel\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @AndroidFindBy(xpath = "//*[@text=\"Verify personal information\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Verify personal information'")
  private MobileElement verifyInfo;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"PERSONAL_INFO-btn-title\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement personalInfoConfirmButton;

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text=\"Declaration – Employee of Intermediary\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Declaration – Employee of Intermediary'")
  private MobileElement employeeIntermediaryText;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_OTHER_INFO-question\"]/../android.view.ViewGroup/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'No'")
  private List<MobileElement> employeeIntermediaryAnswers;

  // qualification info
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Qualification information\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Qualification information'")
  private MobileElement qualificationInfo;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,\"What is your education\")]/../android.view.ViewGroup/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'University level or above'")
  private List<MobileElement> qualificationInfoAnswers;

  // investment Horizon
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Investment Horizon\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Investment Horizon'")
  private MobileElement investmentHorizon;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,\"What is your intended\")]/../android.view.ViewGroup/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Up to 1 year'")
  private List<MobileElement> investmentHorizonAnswers;

  // investment Knowledge
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text=\"Investment Product Knowledge and Experience\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Investment Product Knowledge and Experience'")
  private MobileElement investmentKnowledge;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,\"I have knowledge\")]/../android.view.ViewGroup/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Bonds, Bond Funds'")
  private List<MobileElement> investmentKnowledgeAnswers;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Next\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL-btn-title'")
  private MobileElement nextButton;

  // net Worth
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Net worth\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Net worth'")
  private MobileElement netWorth;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text,\"What is your total\")]/../android.view.ViewGroup/android.widget.TextView")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'HKD 1,000,001 - HKD 5,000,000'")
  private List<MobileElement> netWorthAnswers;

  // review profile
  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Review Investment account profile\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review Investment account profile'")
  private MobileElement reviewProfile;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-licensedStaff-desc-0\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL_REVIEW-licensedStaff-desc-0'")
  private MobileElement answerLicensedStaffDeclaration;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-0-desc-0\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL_REVIEW-question-0-desc-0'")
  private MobileElement answerQualificationInformation;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-1-desc-0\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL_REVIEW-question-1-desc-0'")
  private MobileElement answerInvestmentHorizon;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-2-desc-0\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL_REVIEW-question-2-desc-0'")
  private MobileElement answerInvestmentProductKnowledge;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-3-desc-0\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'NON_CRPQ_PERSONAL_REVIEW-question-3-desc-0'")
  private MobileElement answerNetWorth;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-btn\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Confirm'")
  private MobileElement reviewAccountConfirm;

  @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"Terms & Conditions\")]")
  @iOSXCUITFindBy(
      iOSNsPredicate =
          "name == 'Terms & Conditions, Important Notes, Risk Disclosures and other documents'")
  private MobileElement importantNote;

  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[starts-with(@name, 'By checking the box')]/../XCUIElementTypeOther")
  private MobileElement confirmDisclosure;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Confirm\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Confirm'")
  private MobileElement disclosureConfirm;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"PROCESS_GUIDE-desc\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-desc'")
  private MobileElement verifyUpdated;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Next\"]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement verifyNextButton;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-guide-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'WELCOME_TO_MUTUAL_FUND-guide-title'")
  private MobileElement welcomeTitle;

  @Getter
  @AndroidFindAll({
          @AndroidBy(accessibility = "PROCESS_GUIDE-title"),
          @AndroidBy(accessibility = "CRPQ-txt-desc"),
          @AndroidBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView")
  })
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement progressGuideTitle;

  @AndroidFindBy(accessibility = "btnBack")
  private MobileElement backIcon;
  @AndroidFindBy(xpath = "//*[@text='Open an account']")
  private MobileElement openAnAccountBtn;
  @AndroidFindBy(
          xpath =
                  "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup")
  private MobileElement byCheckingBtn;
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement defaultDeclaration;
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement defaultQualificationInformation;
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement defaultInvestmentHorizon;
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement defaultKnowledgeAndExperience;
  @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.widget.TextView[3]")
  private MobileElement defaultNetWorth;
  @AndroidFindBy(xpath = "//*[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-2-btn-toggle\"]/../android.view.ViewGroup")
  private MobileElement multipleChoiceEdit;
  @AndroidFindBy(
          xpath =
                  "//android.view.ViewGroup[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-1\"]")
  private MobileElement multipleChoiceSecond;
  @AndroidFindBy(
          xpath =
                  "//android.view.ViewGroup[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-6\"]")
  private MobileElement multipleChoicesSeventh;
  @AndroidFindBy(xpath = "//*[contains(@text,'We will automatically')]")
  private MobileElement weWillAutomatically;
  @AndroidFindBy(xpath = "//*[@text='OK']")
  private MobileElement OkBtn;
  @AndroidFindBy(xpath = "//*[contains(@text,'Thank you for')]")
  private MobileElement thankYouTxt;
  @AndroidFindBy(xpath = "/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup")
  private MobileElement doneBtn;
  @AndroidFindBy(xpath = "//android.view.ViewGroup[5]/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement closeBtn;
  @AndroidFindBy(xpath = "//*[contains(@text,'By clicking')]")
  private MobileElement byClickingText;
  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"NON_CRPQ_PERSONAL_REVIEW-btn\"]")
  private MobileElement confirmBtn;

  public static final String pageName = "None CRPQ Page";
  public static final String goalBaseStr = "Our goal-based";
  public static final String goalBaseIOSStr = "Goal-based";
  public static final String confirmStr = "Confirm";
  public static final String cancelText = "Cancel";
  public static final String nextText = "Next";
  public static final String skipText = "Skip to open an account";
  public static final String investmentKnowledgeStr = "Investment Product Knowledge and Experience";
  public static final String terms = "Terms and Conditions";
  public static final String fee = "Fees Table";
  public static final int customWait = 2;
  public static final int toastWait = 5;
  public static final String popToastStr = "//*[contains(@text,“Application has been submitted.”)]";

  public static HashMap<String, HashMap<String, String>> noneCRPQMap;
  public static HashMap<String, String> employeeOfIntermediaryMap;

  public NoneCRPQPage() {
    super.pageName = pageName;
    this.noneCRPQQuestionMap = (HashMap<String, MobileElement>) NoneCRPQPageQuestionMap();
    this.noneCRPQAnswersMap = NoneCRPQPageAnswersMap();
    this.noneCRPQVerifyMap = NoneCRPQVerifyResultMap();
  }

  public Map<String, MobileElement> NoneCRPQPageQuestionMap() {
    Map<String, MobileElement> questionsMap = new HashMap<>();
    questionsMap.put("Declaration – Employee of Intermediary", employeeIntermediaryText);
    questionsMap.put("Qualification information", qualificationInfo);
    questionsMap.put("Investment Horizon", investmentHorizon);
    questionsMap.put("Investment Product Knowledge and Experience", investmentKnowledge);
    questionsMap.put("Net worth", netWorth);
    return questionsMap;
  }

  public Map<String, List<MobileElement>> NoneCRPQPageAnswersMap() {
    Map<String, List<MobileElement>> answersMap = new HashMap<>();
    answersMap.put("Declaration – Employee of Intermediary", employeeIntermediaryAnswers);
    answersMap.put("Qualification information", qualificationInfoAnswers);
    answersMap.put("Investment Horizon", investmentHorizonAnswers);
    answersMap.put("Investment Product Knowledge and Experience", investmentKnowledgeAnswers);
    answersMap.put("Net worth", netWorthAnswers);
    return answersMap;
  }

  public Map<String, MobileElement> NoneCRPQVerifyResultMap() {
    Map<String, MobileElement> verifyMap = new LinkedHashMap<>();
    verifyMap.put("Declaration – Employee of Intermediary", answerLicensedStaffDeclaration);
    verifyMap.put("Qualification information", answerQualificationInformation);
    verifyMap.put("Investment Horizon", answerInvestmentHorizon);
    verifyMap.put("Investment Product Knowledge and Experience", answerInvestmentProductKnowledge);
    verifyMap.put("Net worth", answerNetWorth);
    return verifyMap;
  }

  public void VerifyInfoText() {
    clickElement(personalInfoConfirmButton);
  }

  // A_02_Welcome A_03_Selection A_04a_Goal
  public Boolean stepsBeforeNoneCRQP() throws InterruptedException {
    Boolean beforeNoneCRPQTag = true;
    clickElement(gotItTab);
    if (null != isElementDisplayed(goalBasePicture)) {
      waitUntilElementClickable(goalBasePicture);
      clickElement(goalBasePicture);
    } else {
      beforeNoneCRPQTag = false;
    }
    if (isElementDisplayed(skipLink, skipText)) {
      waitUntilElementClickable(skipLink);
      clickElement(skipLink);
    } else {
      beforeNoneCRPQTag = false;
    }
    return beforeNoneCRPQTag;
  }

  // C_01a_infoReview
  public void clickPersonalInfoConfirm() {
    waitUntilElementClickable(personalInfoConfirmButton);
    clickElement(personalInfoConfirmButton);
  }

  public void selectNoneCRPQOptions() {
    scrollUp();
    clickElement(mutualFundNextButton);
    clickElement(processGuideNext);
  }

  public void clickNextButton() {
    clickElement(processGuideNext);
  }

  // C_03b_nonCRPQ1 C_03b_nonCRPQ2 C_03b_nonCRPQ3 C_03b_nonCRPQ4 C_03b_nonCRPQ5
  public Boolean chooseNoneCRQP(String pageTitle, String selectedOpt) {
    List<MobileElement> answersList = this.noneCRPQAnswersMap.get(pageTitle);
    try {
      String questionTitle = getQuestionTitle(pageTitle);
      if (!questionTitle.equals(investmentKnowledgeStr)) {
        clickAnswerFromList(answersList, selectedOpt);
      } else {
        Thread.sleep(500);
        clickAnswerFromList(answersList, selectedOpt);
        nextButton.click();
      }
      return true;

    } catch (NullPointerException | InterruptedException e) {
      return false;
    }
  }

  private void clickAnswerFromList(List<MobileElement> answerList, String answer) {
    for (MobileElement answerEle : answerList) {
      waitUntilElementClickable(answerEle);
      if (answerEle.getText().equals(answer)) {
        answerEle.click();
        break;
      }
    }
  }

  public String getQuestionTitle(String pageTitle) {
    String actualText = "";
    MobileElement questionElement = this.noneCRPQQuestionMap.get(pageTitle);
    if (isElementVisible(questionElement)) actualText = questionElement.getText();
    return actualText;
  }

  public void checkOptionElement(String pageTitle, String selectedOpt) {
    String optionString = noneCRPQMap.get(pageTitle.trim()).get(selectedOpt.trim());
    By optionLocator = By.xpath(optionString);
    MobileElement optionElement = (MobileElement) isElementByClickable(optionLocator);
    optionElement.click();
  }

  public void clickCancelButton() {
    if (isElementDisplayed(contactWarningCancel, cancelText, customWait)) {
      waitUntilElementClickable(contactWarningCancel);
      contactWarningCancel.click();
    }
  }

  public String confirmReviewProfile() {
    clickElement(reviewAccountConfirm);
    if (waitUntilElementVisible(importantNote) != null)
      ;
    return importantNote.getText();
  }

  @SneakyThrows
  public String confirmRiskDisclosure() {
    String updatedStr = "";
    clickElement(confirmDisclosure);
    waitUntilElementClickable(disclosureConfirm);
    clickElement(disclosureConfirm);
    Thread.sleep(1000 * 5);
    if (isElementVisible(verifyUpdated, customWait)) updatedStr = verifyUpdated.getText();
    updatedStr = verifyUpdated.getText();
    return updatedStr;
  }

  // C_08_nonCRPQreview
  public String getText(MobileElement ele) {
    MobileElement element = scrollUpToFindElement(ele, 5, customWait);
    return element.getText();
  }

  // C_08_nonCRPQreview
  public String getProfileAnswer(String question, NoneCRPQ questionsEntity)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    String questionRawStr = question.replace(" ", "").replace("–", "").toLowerCase();
    String firstLetter = questionRawStr.substring(0, 1).toUpperCase();
    String questionKeyMethod = "get" + firstLetter + questionRawStr.substring(1);
    Method method = questionsEntity.getClass().getMethod(questionKeyMethod, new Class[] {});
    method.setAccessible(true);
    Object expectedAnswer = method.invoke(questionsEntity, new Object[] {});
    return (String) expectedAnswer;
  }

  public void clickNextDone() throws InterruptedException {
    Thread.sleep(toastWait);
    clickNotification(250);
    clickElement(verifyNextButton);
  }

  public boolean isWelcomeTitleDisplayed() {
    return welcomeTitle.isDisplayed();
  }

  public boolean isOnParticularPage(String text) {
    String label = null;
    try {
      label = progressGuideTitle.getText().trim();
      logger.info("I'm on " + label);
    } catch (Exception ex) {
      if (System.getProperty("mobile").contains("ios")) {
        if (text.contains("'"))
          return driver
                  .findElement(
                          By.xpath(
                                  "//XCUIElementTypeStaticText[starts-with(@label, '"
                                          + text.split("'")[0]
                                          + "')]"))
                  .isDisplayed();
        else
          return driver
                  .findElement(
                          By.xpath("//XCUIElementTypeStaticText[starts-with(@label, '" + text + "')]"))
                  .isDisplayed();
      }
    }
    return label.equalsIgnoreCase(text);
  }

  public void clickConfirmButton() {
    clickElement(disclosureConfirm);
  }

  @SneakyThrows
  public void iGoToRIAPPage(){
    for (int i=1; i<4; i++){
      clickElement(backIcon);
      Thread.sleep(1000 * 3);
    }
  }

  public void iClickOpenAnAccountBtn(){
    waitUntilElementVisible(openAnAccountBtn);
    clickElement(openAnAccountBtn);
  }

  public boolean verifyStep1IsStillDone(){
    if(verifyElementExist(doneBtn)){
      return true;
    }
    return false;
  }

  public void iClickByCheckingBtn(){
    scrollUpToFindElement(byCheckingBtn,8,3);
    clickElement(byCheckingBtn);
  }

  public void iGetDefaultDeclaration(){
    defaultDeclarations = getElementText(defaultDeclaration);
  }

  public void iGetDefaultQualificationInformation(){
    defaultQualificationInformations = getElementText(defaultQualificationInformation);
  }

  public void iGetDefaultInvestmentHorizon(){
    defaultInvestmentHorizons = getElementText(defaultInvestmentHorizon);
  }

  public void iGetDefaultKnowledgeAndExperience(){
    scrollUpToFindElement(byClickingText,8,3);
    defaultKnowledgeAndExperiences = getElementText(defaultKnowledgeAndExperience);
  }

  public void iGetDefaultNetWorth(){
    defaultNetWorths = getElementText(defaultNetWorth);
  }

  public boolean iVerifyDeclarationNotChange(){
    if (getElementText(defaultDeclaration).equals(defaultDeclarations)){
      return true;
    }
    return false;
  }

  public boolean iVerifyQualificationInformationNotChange(){
    if (getElementText(defaultQualificationInformation).equals(defaultQualificationInformations)){
      return true;
    }
    return false;
  }

  public boolean iVerifyInvestmentHorizonNotChange(){
    if (getElementText(defaultInvestmentHorizon).equals(defaultInvestmentHorizons)){
      return true;
    }
    return false;
  }

  public boolean iVerifyKnowledgeAndExperienceNotChange(){
    scrollUpToFindElement(byClickingText,8,3);
    if (getElementText(defaultKnowledgeAndExperience).equals(defaultKnowledgeAndExperiences)){
      return true;
    }
    return false;
  }

  public boolean iVerifyNetWorthNotChange(){
    if (getElementText(defaultNetWorth).equals(defaultNetWorths)){
      return true;
    }
    return false;
  }

  public void iChangeOptions() {
    clickElement(multipleChoiceEdit);
    scrollUpToFindElement(weWillAutomatically, 8, 3);
    clickElement(multipleChoiceSecond);
    clickElement(multipleChoicesSeventh);
    scrollDownToFindElement(OkBtn,8,3);
    clickElement(OkBtn);
  }

  public boolean iVerifySubmitIsUpdate(){
    if (verifyElementExist(thankYouTxt)){
      return true;
    }
    return false;
  }

  public void iClosePage(){
    clickElement(closeBtn);
  }

  public void iClickConfirmBtn(){
    clickElement(confirmBtn);
  }
}
