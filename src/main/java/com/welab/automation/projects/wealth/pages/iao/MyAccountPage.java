package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;

public class MyAccountPage extends AppiumBasePage {
  private String pageName = "My Account Page";

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='\\uF16B']"))
  @iOSXCUITFindAll(@iOSXCUITBy())
  private MobileElement myAccountIcon;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='\\uF104']"))
  @iOSXCUITFindAll(@iOSXCUITBy())
  private MobileElement back;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Settings']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement settings;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Wealth Centre']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement wealthCentre;

  public MyAccountPage() {
    super.pageName = pageName;
  }

  private void openMyAccount() {
    myAccountIcon.click();
  }

  public CoreBankSettingsPage openCoreBankSettingPage() {
    openMyAccount();
    settings.click();
    return new CoreBankSettingsPage();
  }

  public WealthCentreSettingsPage openWealthCentreSettingPage() {
    openMyAccount();
    settings.click();
    return new WealthCentreSettingsPage();
  }
}
