
package com.welab.automation.projects.wealth.runners;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.api.FeatureCreator;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.util.Map;


public class TestScenariosGenerationInt {
    FeatureCreator featureCreator;
    @BeforeSuite
    private void generateFeatureAllCases() throws IOException {
        String filepath="";
        String env = System.getProperty("env")+"/integration";
        featureCreator = new FeatureCreator(env);
        Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
        GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
        if(!env.isEmpty())
            filepath = GlobalVar.TEST_DATA_FILE_PATH+env+"/";
        else
            filepath = GlobalVar.TEST_DATA_FILE_PATH;
        featureCreator.generatFeatureAllCases(filepath,"templateDefault.feature");
    }
}