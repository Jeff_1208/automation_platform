Feature: check portfolio  When take different crr value(1-5)

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  @WELATCOE-2838
  Scenario Outline: Achieve Financial Freedom    crr 1  to recommendation different portfolio
    When Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list


    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 600000          | Budget,Modest,Deluxe |

  @WELATCOE-2838
  Scenario Outline: Achieve Financial Freedom    crr 2  to recommendation different portfolio
    When Login with user and password
      | user     | test111  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list


    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 64  | 600000          | Budget,Modest,Deluxe |


  @WELATCOE-2838
  Scenario Outline: Achieve Financial Freedom    crr 3  to recommendation different portfolio
    When Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list


    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 64  | 600000          | Budget,Modest,Deluxe |

  @WELATCOE-2838
  Scenario Outline: Achieve Financial Freedom    crr4  to recommendation different portfolio
    When Login with user and password
      | user     | test112  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list


    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 64  | 600000          | Budget,Modest,Deluxe |

  @WELATCOE-2838
  Scenario Outline: Achieve Financial Freedom    crr 5  to recommendation different portfolio
    When Login with user and password
      | user     | test006  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list


    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 60  | 600000          | Budget,Modest,Deluxe |


  @WELATCOE-399
  Scenario Outline: Build My Investing Habit   crr 1  to recommendation different portfolio
    When Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         |  288  |


  @WELATCOE-399
  Scenario Outline: Build My Investing Habit   crr 2  to recommendation different portfolio
    When Login with user and password
      | user     | test111  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | oneTimeValue | monthlyValue  | month |
      | 90000        | 10000         |  288  |


  @WELATCOE-399
  Scenario Outline: Build My Investing Habit   crr 3  to recommendation different portfolio
    When Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         |  288  |

  @WELATCOE-399
  Scenario Outline: Build My Investing Habit   crr 4  to recommendation different portfolio
    When Login with user and password
      | user     | test112  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 99000        | 14000         |  288  |

  @WELATCOE-399
  Scenario Outline: Build My Investing Habit   crr 5  to recommendation different portfolio
    When Login with user and password
      | user     | test190  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | oneTimeValue | monthlyValue  | month |
      | 90000        | 10000         |  288  |


  @WELATCOE-393
  Scenario Outline: React my target   crr 1  to recommendation different portfolio
    When Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | targetValue | time |
      | 3000000     | 35   |

  @WELATCOE-393
  Scenario Outline: React my target   crr 2  to recommendation different portfolio
    When Login with user and password
      | user     | test192  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | targetValue | time |
      | 3000000     | 50  |

  @WELATCOE-393
  Scenario Outline: React my target   crr 3  to recommendation different portfolio
    When Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | targetValue | time |
      | 9000000     | 40   |

  @WELATCOE-393
  Scenario Outline: React my target   crr 4  to recommendation different portfolio
    When Login with user and password
      | user     | test112  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | targetValue | time |
      | 6000000     | 39   |

  @WELATCOE-393
  Scenario Outline: React my target   crr 5  to recommendation different portfolio
    When Login with user and password
      | user     | test190  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click View Portfolio
    And I click the portfolio  and  review Fund list
    Examples:
      | targetValue | time |
      | 5000000     | 45   |

