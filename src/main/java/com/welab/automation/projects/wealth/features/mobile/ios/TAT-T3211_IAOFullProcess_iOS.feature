@iOS
Feature: IAO process

#  ******************* A NEW ACCOUNT IS REQUIRED HERE *************************
  Scenario: set account
    Given I launch app with account
      |account|password|
      |testXXX|Aa123321|

  Scenario: IAO full process
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
    #   upload document
    And I can see the upload files page
    And I review the sample files
    And I add photo from photo library
    Then I finish upload document
    #   Do CRPQ
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test182|Aa123321|

  Scenario: Verify investment account is submitted but not opened
    Given I can see pending approval screen
    When I click X icon
    Then I can see Wealth Landing Page

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test180|Aa123321|

  Scenario: Verify investment account open when personal info & non-CRPQ and CRPQ is completed but document is not uploaded
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
    And I Skip Upload Document
    And I Do CRPQ with "CRPQ"
    Then I can see Wealth Landing Page
    And I click Open Account button
#   upload document
    And I can see the upload files page
    And I review the sample files
    And I add photo from photo library
    Then I finish upload document with CRPQ Done
    Then I finish account opening process

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test186|Aa123321|

  Scenario: Verify investment account open when personal info & non-CRPQ and upload document is completed but CRPQ is not complete
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
#   upload document
    And I can see the upload files page
    And I review the sample files
    And I add photo from photo library
    Then I finish upload document
#   quit IAO process
    And I quit opening account
    Then I can see Wealth Landing Page
#   Do CRPQ
    And I click Open Account button
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test187|Aa123321|

  Scenario: Verify investment account open when personal info & non-CRPQ is completed but upload document and CRPQ is not completed
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I review investment account profile
    And I confirm the Investment account profile
#        I quit IAO process
    And I quit and back to Open Account
    And I click Open Account button
    #   upload document
    And I can see the upload files page
    And I review the sample files
    And I add photo from photo library
    Then I finish upload document
#   Do CRPQ
    And I Do CRPQ with "CRPQ"
    Then I finish account opening process
