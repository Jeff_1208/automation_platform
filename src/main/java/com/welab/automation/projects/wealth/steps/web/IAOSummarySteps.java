package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalLoginPage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.SummaryPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ValidationDataMakerPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.picocontainer.annotations.Inject;
import org.testng.Assert;


import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class IAOSummarySteps extends WebBasePage {

  @Inject AdminPortalLoginPage adminPortalLoginPage;
  @Inject AdminPortalMainPage adminPortalMainPage;
  @Inject SummaryPage summaryPage;
  @Inject
  ValidationDataMakerPage validationDataMakerPage;

  @And("goto Investment Account Summary page")
  public void gotoSummaryPage() {
    adminPortalMainPage.gotoSummaryPage();
    assertThat(summaryPage.isIASummaryPage()).isTrue();
  }
  @And("I goto Investment Account Summary page")
  public void gotoSummarypage() {
    adminPortalMainPage.gotoSummaryPage();
  }

  @And("^I click View button at SummaryPage named ([^\"]\\S*)$")
  public void clickViewNameAtSummaryPage(String user) {
    Map<String, WebElement> targetApplication = summaryPage.findSummary(user);
    assertThat(targetApplication).isNotNull();
    summaryPage.clickViewNamed(targetApplication);
    boolean pageTop = summaryPage.isSummaryDetailPageHeader();
    assertThat(pageTop).isTrue();
  }

  @And("^verify search by Name out of scope")
  public void verifySearchByName(){
    String name = validationDataMakerPage.verifyNameNotNull(validationDataMakerPage.getEnglishNameList());
    boolean flagId = summaryPage.verifySearchByName(name);
    assertThat(flagId).isFalse();
    boolean flagFNZ = summaryPage.verifySearchByFNZ(name);
    assertThat(flagFNZ).isFalse();
    boolean flagDate =summaryPage.verifySearchByDateName(name);
    assertThat(flagDate).isFalse();
  }



  @And("verify search by T24 CustomerID")
  public void verifySearchByCustomerID(){
    summaryPage.searchAll();
    String ID =validationDataMakerPage.verifyNameNotNull(validationDataMakerPage.getT24custid());
    boolean flagID =summaryPage.verifySearchByCustomerID(ID);
    assertThat(flagID).isTrue();
    Map<String, WebElement> targetApplication = summaryPage.findSummaryApplication(ID,"ID");
    assertThat(targetApplication).isNotNull();
  }


  @And("verify search by FNZ head account")
  public void verifySearchByFNZ() {
    String FNZ=validationDataMakerPage.verifyNameNotNull(validationDataMakerPage.getMakerList());
    boolean flagFNZ = summaryPage.verifySearchByFNZ(FNZ);
    assertThat(flagFNZ).isTrue();
    Map<String, WebElement> targetApplication = summaryPage.findSummaryApplication(FNZ,"FNZ");
    assertThat(targetApplication).isNotNull();
  }

  @And("verify search by Account open date")
  public void verifySearchByDate(){
    summaryPage.searchAll();
    String Date=validationDataMakerPage.verifyNameNotNull(validationDataMakerPage.getDateList());
    boolean dateFlag = summaryPage.verifySearchByDate(Date);
    assertThat(dateFlag).isTrue();
    Map<String, WebElement> targetApplication = summaryPage.findSummaryApplication(Date,"openDate");
    assertThat(targetApplication).isNotNull();
  }

  @And("verify reset button")
  public void verifyResetBtn(){
    boolean flag =summaryPage.verifyResetBtn();
    assertThat(flag).isTrue();
  }

  @And("Sort by T24 Customer ID column")
  public void sortByT24() {
    boolean sortColumn = summaryPage.sortColumn("T24ID");
    assertThat(sortColumn).isTrue();
  }
  @And("Sort by FNZ head account column")
  public void sortByFNZ() {
    boolean sortColumn = summaryPage.sortColumn("FNZ");
    assertThat(sortColumn).isTrue();
  }
  @And("Sort by Investment account status column")
  public void sortByInvestment() {
    boolean sortColumn = summaryPage.sortColumn("staus");
    assertThat(sortColumn).isTrue();
  }
  @And("Sort by Tradability column")
  public void sortByTrad() {
    boolean sortColumn = summaryPage.sortColumn("Trad");
    assertThat(sortColumn).isTrue();
  }
  @And("Sort by Account open date column")
  public void sortByDate() {
    boolean sortColumn = summaryPage.sortColumn("date");
    assertThat(sortColumn).isTrue();
  }
  @And("I click view button")
  public void clickViewBtn() {
    summaryPage.clickViewBtn();
  }
}
