Feature: verify Maker Page

  Background: verify Maker Page

  @WELATCOE-568
  Scenario: Verify Maker page
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I goto approval maker
    And verify header of Maker's approval list page
    And I verify the columns of Maker's approval list table
#    And I verify the pending approval list on Maker's approval list table
    And I click view button can see view button is enabled and detail view






  @WELATCOE-577
  Scenario: Verify search opened accounts
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval checker
    And verify default sorting of checker page
    And verify sort by Submit datetime
    And verify sort by Maker column


