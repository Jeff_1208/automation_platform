@iOS
Feature: Order placement on iOS

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test106|Aa123321|

  @WELATCOE-424
  Scenario: The complete processing about Order placement_Set My Target _ HKD buy USD_Error message
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |        | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the error message

  @WELATCOE-426
  Scenario: The complete processing about Order placement_Build My Investing Habit _ HKD buy USD_Error message
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the error message

  @WELATCOE-428
  Scenario: The complete processing about Order placement_Set Achieve Financial Freedom _ HKD buy USD_Error message
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the error message

  Scenario: set account
    Given I launch app with account
      |account|password|
      |testsit057|Aa123321|

  @WELATCOE-432
  Scenario: Insufficient amount when HKD buy USD product _ HKD buy USD
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    Then I can see the insufficient amount error message
    And I click back to home button
    And I can see Wealth Landing Page

  @WELATCOE-432
  Scenario: Insufficient amount when HKD buy USD product_Build My Investing Habit _ HKD buy USD
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    Then I can see the insufficient amount error message
    And I click back to home button
    And I can see Wealth Landing Page

  @WELATCOE-432
  Scenario: Insufficient amount when HKD buy USD product_Achieve Financial Freedom _ HKD buy USD
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    Then I can see the insufficient amount error message
    And I click back to home button
    And I can see Wealth Landing Page

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test112|Aa123321|

  @WELATCOE-230
  Scenario: The processing about Order placement but quit in Review and confirm order page for Achieve Financial Freedom _ HKD buy USD_Quit
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I check Subscription Fee
    And I check Starting From
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-229
  Scenario: The processing about Order placement but quit in Review and confirm order page for Build my investment habit _ HKD buy USD_Quit
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I check Subscription Fee
    And I check Starting From
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-228
  Scenario: The processing about Order placement but quit in Review and confirm order page for Set My Target _ HKD buy USD_Quit
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I check Subscription Fee
    And I check Starting From
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-434
  Scenario: The processing about Order placement but quit in Review and confirm order page for Set My Target _ USD buy USD_Quit
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-434
  Scenario: The processing about Order placement but quit in Review and confirm order page for Set My Target _ USD buy USD_Not now
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-436
  Scenario: The processing about Order placement but quit in Review and confirm order page for Build my investment habit _ USD buy USD_Quit
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-436
  Scenario: The processing about Order placement but quit in Review and confirm order page for Build my investment habit _ USD buy USD_Not now
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-438
  Scenario: The processing about Order placement but quit in Review and confirm order page for Achieve Financial Freedom _ USD buy USD_Quit
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | ,, |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process
    Then I can see Wealth Landing Page
    And The status of new goal setting is draft

  @WELATCOE-438
  Scenario: The processing about Order placement but quit in Review and confirm order page for Achieve Financial Freedom _ USD buy USD_Not now
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | ,, |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | USD      |
    And I quit the ordering process but go back
    Then I still stay on review and confirm order page

  @WELATCOE-424
#    test006
  Scenario: The complete processing about Order placement_Set My Target _ HKD buy USD_successfully
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-426
  Scenario: The complete processing about Order placement_Build My Investing Habit _ HKD buy USD_successfully
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-428
  Scenario: The complete processing about Order placement_Set Achieve Financial Freedom _ HKD buy USD_successfully
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     5000        | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-440
  Scenario:The complete processing about Order placement_Reach My Target _ USD buy USD
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |      | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-442
  Scenario: The complete processing about Order placement_Build My Investing Habit _ USD buy USD
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page

  @WELATCOE-444
  Scenario Outline: The complete processing about Order placement_Achieve Financial Freedom _ USD buy USD
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I buy the fund
      | currency |
      | USD      |
    And I slide to confirm the order
    Then I can see the congratulation page
    Examples:
      | age | monthlyExpenses | otherExpenses |
      | 65  | 500             | No,No,No      |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test092|Aa123321|

  @WELATCOE-531
#    test006
  Scenario: Update the tradability ability status is No - Buy_Set My Target
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |             | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-531
  Scenario: Update the tradability ability status is No - Buy_Build My Investing Habit
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-531
  Scenario: Update the tradability ability status is No - Buy_Set Achieve Financial Freedom
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     5000        | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test098|Aa123321|

  @WELATCOE-531
#    test006
  Scenario: Update the tradability ability status is No TRADABLE_Set My Target
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target
      | targetValue | time |
      |             | 60   |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-531
  Scenario: Update the tradability ability status is No TRADABLE_Build My Investing Habit
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit
      | oneTimeValue | monthlyValue | month |
      |         |          | 60    |
#    And I click the discover
    And I can view portfolio
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

  @WELATCOE-531
  Scenario: Update the tradability ability status is No TRADABLE_Set Achieve Financial Freedom
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  |     5000        | ,, |
    And I click the discover
    And I buy the fund
      | currency |
      | HKD      |
    And I slide to confirm the order
    Then I can see the submit error message

