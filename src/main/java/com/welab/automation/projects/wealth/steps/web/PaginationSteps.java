package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalLoginPage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ApprovalPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.picocontainer.annotations.Inject;

import static org.assertj.core.api.Assertions.assertThat;

public class PaginationSteps extends WebBasePage {

    @Inject AdminPortalLoginPage adminPortalLoginPage;
    @Inject AdminPortalMainPage adminPortalMainPage;
    @Inject ApprovalPage approvalPage;

    @When("I click navigate from side menu: Investment Account Summary")
    public void iClickSummaryMenu() {
        adminPortalMainPage.clickSummaryMenu();
        waitUntilElementClickable(adminPortalMainPage.getConfirmIntoSummary());
        assertThat(adminPortalMainPage.getConfirmIntoSummary().getText()).isEqualTo("Investment Account Summary");
    }

    @And("I check default pagination")
    public void icheckDefaultPagination(){
        boolean defalitPaginationNumber = adminPortalMainPage.checkDefaultPagination();
        assertThat(defalitPaginationNumber).isTrue();
    }

    @And("I check pagination at the bottom of the page")
    public void iCheckPaginationBottom(){
        boolean checkFirstPreviousStatus = adminPortalMainPage.checkFirstPreviousPage();
        assertThat(checkFirstPreviousStatus).isTrue();
        boolean checkCurrentPage = adminPortalMainPage.checkPreviousPage();
        assertThat(checkCurrentPage).isTrue();
        boolean checkNextStatus = adminPortalMainPage.checkNextButton();
        assertThat(checkNextStatus).isTrue();
        boolean checkSpecific = adminPortalMainPage.checkSpecificPage();
        assertThat(checkSpecific).isTrue();
        boolean checkThreeDotsStatus = adminPortalMainPage.checkThreeDots();
        assertThat(checkThreeDotsStatus).isTrue();
    }
    @And("I check pagination at the bottom of the Approval page")
    public void iCheckPaginationBottomAtApprovalpage(){
        boolean checkFirstPreviousStatus = adminPortalMainPage.checkFirstPreviousPage();
        assertThat(checkFirstPreviousStatus).isTrue();
        boolean checkCurrentPage = adminPortalMainPage.checkPreviousPage();
        assertThat(checkCurrentPage).isTrue();
        boolean checkNextStatus = adminPortalMainPage.checkNextButton();
        assertThat(checkNextStatus).isTrue();
        boolean checkSpecific = adminPortalMainPage.checkApprovalSpecificPage();
        assertThat(checkSpecific).isTrue();
        boolean checkThreeDotsStatus = adminPortalMainPage.verifyThreeDots();
        assertThat(checkThreeDotsStatus).isTrue();
    }
}





