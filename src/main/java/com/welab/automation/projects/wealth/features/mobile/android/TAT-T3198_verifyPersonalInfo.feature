Feature: update personal information

  Background: Login and proceed to Wealth verify personal information step
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And Login with user and password
      | user     | test219  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I goto wealth main page

#  Scenario: update personal basic information
#    Given I go to Verify personal info
#    And update personal info
#      | name | test |
#    Then I can find updated personal basic info
#      | name | 大文 |
#
#  Scenario: update account information
#    Given I go to Verify personal info
#    And update email address
#      | emailAddress | etest@welabtest.co |
#    Then I can find updated email address
#      | emailAddress | e*********@welabtest.co |
#
#  Scenario: update account information
#    Given I go to Verify personal info
#    And update phone number
#      | phoneNumber | 98072447 |
#    Then I can find updated phone number
#      | phoneNumber | +852 9810 **** |
#
#  Scenario: update residential address
#    Given I go to Verify personal info
#    And update residential address
#      | rDistrict | Z |
#      | rStreet | A |
#      | rBlock | B |
#      | rFloor | C |
#      | rFlat | D |
#      | sameAsResidentialAddress | true |
#    # TODO
#
#  Scenario: update employment status
#    Given I go to Verify personal info
#    And update employment status
#      | employmentStatus | employed |
#    Then I can find updated employment status
#      | employStatus | Employed |
#
#  Scenario: update region
#    Given I go to Verify personal info
#    And update region
#      | region | Canada |
#    Then I can find updated region
#      | region | none |

  Scenario: update personal information success
    Given I skip guidance
    Given I go to Verify personal info
    And I move eyeBtn to top right corner
    And update personal info
      | name | test |
    And update account info
      | emailAddress | etest@welabtest.co |
      | phoneNumber  | 98072447           |
    And update residential address
      | rDistrict                | Z    |
      | rStreet                  | A    |
      | rBlock                   | B    |
      | rFloor                   | C    |
      | rFlat                    | D    |
      | sameAsResidentialAddress | true |
    And update employment status
      | employmentStatus | employed |
    And update region
      | region | Canada |
    Then I can find updated personal info
      | emailAddress | e******@welabtest.co |
      | phoneNumber  | +816 3360 ****       |
      | employStatus | Employed             |
      | region       | Hong Kong SAR        |



