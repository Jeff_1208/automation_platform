package com.welab.automation.projects.wealth.pages.adminPortal;

import com.welab.automation.framework.base.WebBasePage;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.*;

public class ApprovalPage extends WebBasePage {
  private final String pageName = "approval page";
  String t24Id = null;
  String parentHandle = null;

  @FindBy(how = How.TAG_NAME, using = "th")
  private List<WebElement> tableHeaders;

  @FindBy(how = How.CSS, using = "[aria-label=Next]")
  private WebElement nextBtn;

  // used to check if it is the last page.
  @FindBy(how = How.CSS, using = "li[class='page-item ng-star-inserted disabled']")
  private WebElement disabledNextBtn;

  @FindBy(how = How.XPATH, using = "//button[text()='View']")
  private WebElement viewActionBtn;

  @FindBy(how = How.XPATH, using = "//button[text()=' Approve ']")
  private WebElement approveBtn;

  @FindBy(how = How.XPATH, using = "//button[text()=' Reject ']")
  private WebElement rejectBtn;

//  @FindBy(how = How.CSS, using = "input[placeholder='mandatory if reject']")
  @FindBy(how = How.XPATH, using = "//input[@class='form-control operationInput wideCol']")
  private WebElement makerRemarkEditBox;


  @FindBy(how = How.XPATH, using = "//button[text()=' Agree ']")
  private WebElement agreeBtn;

  @FindBy(how = How.XPATH, using = "//button[text()=' Disagree ']")
  private WebElement disagreeBtn;

  @FindBy(how = How.CSS, using = "input[placeholder='comment']")
  private WebElement checkerRemarkEditBox;

  // need to use list?
  @FindBy(how = How.XPATH, using = "//button[text()='Valid']")
  private WebElement validBtn;

  @FindBy(how = How.XPATH, using = "//button[text()='Valid']")
  private List<WebElement> validBtns;

  @FindBy(how = How.XPATH, using = "//button[text()='Invalid']")
  private List<WebElement> invalidBtns;

  @FindBy(how = How.CSS, using = "input[nbinput]")
  private List<WebElement> rejectReasonEditBoxes;

  @FindBy(how = How.NAME, using = "dp")
  private WebElement issueDateEditBox;

  @FindBy(how = How.NAME, using = "dp")
  private List<WebElement> issueDateEditBoxs;

  @FindBy(how = How.XPATH, using = "//button[text()='Submit']")
  private WebElement submitBtn;

  @FindBy(
          how = How.CSS,
          using = "li[class='menu-item ng-tns-c134-2 ng-tns-c134-1 ng-star-inserted']")
  private WebElement makerBtn;

  @FindBy(how = How.XPATH,using = "//ngb-pagination/ul/li[2]")
  private WebElement noFirstPage;

  @FindBy(how = How.XPATH, using = "//span[text()='Detail Record view as Checker']")
  private WebElement CheckerPageHeader;

  @FindBy(how = How.XPATH, using = "//button[text()='< Back']")
  private WebElement BackBtn;

  @FindBy(how = How.XPATH, using = "//td[text()='Consent letter:']")
  private WebElement ConsentLetter;

  @FindBy(how = How.CLASS_NAME, using = "subContainer")
  private WebElement PersonalINFO;

  @FindBy(how = How.XPATH, using = "//button[text()='Invalid']")
  private WebElement invalidBtn;

  @FindBy(how = How.XPATH, using = "//button[text()='Valid']")
  private WebElement ValidBtn;

  @FindBy(how = How.XPATH, using = "//span[contains(text(),'Total number of files:')]")
  private WebElement TotalNum;

  @FindBy(how = How.XPATH, using = "//h4[text()='Document Review']")
  private WebElement DocTitle;

  @FindBy(how = How.XPATH, using = "//input[@value='ADDRESS_PROOF']")
  private WebElement AddressProof;

  @FindBy(how = How.XPATH, using = "//input[@value='CONSENT_LETTER']")
  private WebElement ConsentLetterInput;

  @FindBy(how = How.XPATH,using = "//a[text()=' View File']")
  private List<WebElement> viewFiles;

  @FindBy(how = How.CSS,using = "div.ng-star-inserted > nb-alert")
  private WebElement errorMessage;

  public ApprovalPage() {
    super.pageName = this.pageName;
  }

  private enum ApplicationHeader {
    Action("Action"),
    T24ID("T24ID"),
    SUBMIT_DATETIME("Submit datetime"),
    ENGLISH_NAME("English Name"),
    CHINESE_NAME("Chinese Name"),
    MOBILE_NO("Mobile Number"),
    MAKER("Maker");

    public final String header;

    ApplicationHeader(String header) {
      this.header = header;
    }
  }

  // not all th are the application's element, need to filter.
  private List<Map<String, WebElement>> filterApplicationFromTableHeaders() {
    List<Map<String, WebElement>> applications = new ArrayList<>();

    waitUntilElementVisible(viewActionBtn);

    for (int i = 0; i < tableHeaders.size(); i++) {
      Map<String, WebElement> application = new IdentityHashMap<>();
      if (tableHeaders.get(i).getText().equals("View")) {
        application.put(ApplicationHeader.Action.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.T24ID.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.SUBMIT_DATETIME.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.ENGLISH_NAME.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.CHINESE_NAME.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.MOBILE_NO.header, tableHeaders.get(i++));
        application.put(ApplicationHeader.MAKER.header, tableHeaders.get(i));
        applications.add(application);
      }
    }

    return applications;
  }

  public Map<String, WebElement> findApplicationInCurrentPage(
      String username, List<Map<String, WebElement>> applications) {
    for (Map<String, WebElement> application : applications) {
      String EnglishName = getElementText(application.get(ApplicationHeader.ENGLISH_NAME.header));
      if (EnglishName == null) {
        continue;
      }
      String user = EnglishName.split(" ")[1];
      if (user.equals(username)) {
        return application;
      }
    }
    return null;
  }

  @SneakyThrows
  public Map<String, WebElement> findApplication(String username) {
    while (true) {
      List<Map<String, WebElement>> applications = filterApplicationFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());
      Map<String, WebElement> targetApplication =
          findApplicationInCurrentPage(username, applications);

      if (targetApplication == null) {
        if (verifyElementExist(disabledNextBtn)) {
          return null;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 3); // wait for loading.
      } else {
        return targetApplication;
      }
    }
  }

  @SneakyThrows
  public void approveApplication(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(approveBtn);
    if (verifyElementExist(validBtn)) {
      for (WebElement validBtn : validBtns) {
        clickElement(validBtn);
      }
      for (WebElement issueDateEditBox : issueDateEditBoxs) {
        clickElement(issueDateEditBox);
        inputEnter(driver.switchTo().activeElement());
      }
    }
    Thread.sleep(500);
    //    clickElement(submitBtn);
    clickElementWithAlert(submitBtn, 3);
  }

  @SneakyThrows
  public void rejectApplication(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(rejectBtn);
    clickElement(makerRemarkEditBox);
    clearAndSendKeys(makerRemarkEditBox, "WELATCOE-562 test scenario");
    if (verifyElementExist(validBtn)) {
      for (WebElement invalidBtn : invalidBtns) {
        clickElement(invalidBtn);
      }
    }
    if (verifyElementExist(validBtn)) {
      for (WebElement rejectReasonEditBox : rejectReasonEditBoxes) {
      clickElement(rejectReasonEditBox);
      clearAndSendKeys(rejectReasonEditBox, "WELATCOE-562 test scenario");
      }
    }
    //    clickElement(submitBtn);
    clickElementWithAlert(submitBtn, 3);
    // wait for alert pop up.
    //    Thread.sleep(1000);
    //    driver.switchTo().alert().accept();
  }

  @SneakyThrows
  public void agreeApplication(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(agreeBtn);
    clickElementWithAlert(submitBtn, 3);
  }

  @SneakyThrows
  public void disagreeApplication(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(disagreeBtn);
    clickElement(checkerRemarkEditBox);
    clearAndSendKeys(checkerRemarkEditBox, "WELATCOE-562 test scenario");
    Thread.sleep(500);
    clickElementWithAlert(submitBtn, 3);
  }

  public boolean NotEditable() {
    return !isElementClickable(By.xpath("//button[text()=' Approve ']"),2)
            && !isElementClickable(By.xpath("//button[text()=' Reject ']"),2);
  }

  @SneakyThrows
  public boolean findCheckerApplicationList() {
    Thread.sleep(3*1000);
    while (true) {
      List<Map<String, WebElement>> applications = filterApplicationFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());
      Map<String, WebElement> targetApplication =
              checkPassT24ID(t24Id,applications);

      if (targetApplication == null) {
        if ( applications.size()< 20 ) {
          return true;
        }else if (applications.size()==20 && !verifyElementExist(noFirstPage)) {
          return true;
        } else if (verifyElementExist(disabledNextBtn)) {
          return true;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 3); // wait for loading.
      } else {
        return false;
      }
    }
  }

  public boolean findMakerApplicationList() {
    clickElement(makerBtn);
    return findCheckerApplicationList();
  }


  public void clickViewNamed(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
  }

  @Override
  public List<String> getSelectedOptions(WebElement element) {
    return super.getSelectedOptions(element);
  }
  public List<String> getPersonalINFO(Map<String, WebElement> targetApplication){
    List<String> INFO = new ArrayList<>();
    INFO.add(targetApplication.get(ApplicationHeader.T24ID.header).getText());
    INFO.add(targetApplication.get(ApplicationHeader.ENGLISH_NAME.header).getText());
    INFO.add(targetApplication.get(ApplicationHeader.CHINESE_NAME.header).getText());
    INFO.add(targetApplication.get(ApplicationHeader.MOBILE_NO.header).getText());
    return INFO;
  }
  public boolean isCheckerTopDetailPage() {
    List<WebElement>page = new ArrayList<>();
    page.add(CheckerPageHeader);
    page.add(BackBtn);
    page.add(submitBtn);
    page.add(ConsentLetter);
    page.add(approveBtn);
    page.add(rejectBtn);
    page.add(agreeBtn);
    page.add(disagreeBtn);
    return verifyAllElementExist(page);
  }
  public boolean isPersonalINFO(List<String> INFO) {
    String INFO2 = PersonalINFO.getText();
    boolean flag = false;
    Iterator<String> it = INFO.iterator();
    while (it.hasNext()) {
      INFO2.contains(it.next());
      flag = true;
    }
    return flag;
  }
  public boolean isCheckerBottomDetailPage() {
    scrollToBottom("Wealth");
    boolean flag = true;
    List<WebElement> page = new ArrayList<>();
    page.add(invalidBtn);
    page.add(ValidBtn);
    page.add(TotalNum);
    boolean flag1= verifyAllElementExist(page)&&isElementClickable(By.linkText("View File"));
    flag= !AddressProof.isEnabled() && !ConsentLetterInput.isEnabled();
    return flag&&flag1;
  }

  public boolean findMakerApplicationNotList() {
    clickElement(makerBtn);
    return !findCheckerApplicationList();
  }

  @SneakyThrows
  public void disagree(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(disagreeBtn);
    clickElement(checkerRemarkEditBox);
    clearAndSendKeys(checkerRemarkEditBox, "WELATCOE-580 test scenario");
    Thread.sleep(500);

  }

  @SneakyThrows
  public void agree(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(agreeBtn);
    Thread.sleep(500);

  }

  @SneakyThrows
  public Map<String, WebElement> findMakerNoOperation() {
    while (true) {
      List<Map<String, WebElement>> applications = filterApplicationFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());

      Map<String, WebElement> targetApplication =
                makerNoOperation(applications);

      if (targetApplication == null) {
        if (verifyElementExist(disabledNextBtn)) {
          return null;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 2); // wait for loading.
      } else {
        return targetApplication;
      }
    }
  }

  public Map<String, WebElement> makerNoOperation(
          List<Map<String, WebElement>> applications) {
    for (Map<String, WebElement> application : applications) {
      String makerValue = getElementText(application.get(ApplicationHeader.MAKER.header));
      String T24Id      = getElementText(application.get(ApplicationHeader.T24ID.header));
      if (makerValue.equals("") && !T24Id.equals("8000058520")) {
          t24Id = getElementText(application.get(ApplicationHeader.T24ID.header));
          return application;
      }

    }
    return null;
  }

  @SneakyThrows
  public Map<String, WebElement> findApplicationPassT24Id() {
    while (true) {
      List<Map<String, WebElement>> applications = filterApplicationFromTableHeaders();
      logger.info("current page applications amount: {}", applications.size());
      Map<String, WebElement> targetApplication =
              checkPassT24ID(t24Id,applications);

      if (targetApplication == null) {
        if (verifyElementExist(disabledNextBtn)) {
          return null;
        }
        clickElement(nextBtn);
        Thread.sleep(1000 * 3); // wait for loading.
      } else {
        return targetApplication;
      }
    }
  }

  public Map<String, WebElement> checkPassT24ID(
          String t24Id, List<Map<String, WebElement>> applications) {
    for (Map<String, WebElement> application : applications) {
      String T24Id  = getElementText(application.get(ApplicationHeader.T24ID.header));
      if (T24Id == null){
        continue;
      }
      if ( T24Id.equals(t24Id) ) {
        return application;
      }
    }
    return null;
  }

  @SneakyThrows
  public void checkFileAndSubmit() {
    if (verifyElementExist(validBtn)) {
      for (WebElement viewFile : viewFiles) {
        clickElement(viewFile);
        Thread.sleep(4*1000);
        returnBeforeWindow();
      }
    }
    clickElementWithAlert(submitBtn, 3);
  }

  @SneakyThrows
  public void disagreeApplicationAgain(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(disagreeBtn);
    clickElement(checkerRemarkEditBox);
    clearAndSendKeys(checkerRemarkEditBox, "WELATCOE-586");
    Thread.sleep(500);
  }

  public void submitButton() {
    clickElementWithAlert(submitBtn, 3);
  }

  @SneakyThrows
  public void approveApplicationAgain(Map<String, WebElement> targetApplication) {
    clickElement(targetApplication.get(ApplicationHeader.Action.header));
    clickElement(approveBtn);
    clickElement(makerRemarkEditBox);
    clearAndSendKeys(makerRemarkEditBox, "WELATCOE-586");
    Thread.sleep(500);
  }

  public boolean verifyOnlyShowLastRecord() {
    if (approveBtn.isEnabled()) {
      if (checkerRemarkEditBox.getAttribute("value").equals("WELATCOE-586")) {
        return true;
      } else {
        return false;
      }
    } else {
      if (makerRemarkEditBox.getAttribute("value").equals("WELATCOE-586")) {
        return true;
      } else {
        return false;
      }
    }
  }

  @SneakyThrows
  public boolean openNewTab() {
    parentHandle = driver.getWindowHandle();
    String checkDetailUrl = driver.getCurrentUrl() ;
    String openNewTabUrl = "window.open(" + '"' + checkDetailUrl + '"' + ")";
    driver.executeScript(openNewTabUrl);
    logger.info("open new tab success {}",openNewTabUrl);
    Thread.sleep(5*1000);
    String newCheckDetailUrl = driver.getCurrentUrl() ;
    return checkDetailUrl.equals(newCheckDetailUrl);
  }

  public void repeatOperation() {
    Set<String> allHandles = driver.getWindowHandles();
    for (String oneOfHandles : allHandles) {
      if (!oneOfHandles.equals(parentHandle)) {
        driver.switchTo().window(oneOfHandles);
      }
    }
    clickElement(agreeBtn);
    clearAndSendKeys(checkerRemarkEditBox,"WELATCOE-581 test scenario");
    clickElementWithAlert(submitBtn);

  }

  @SneakyThrows
  public boolean backAndVerifyError() {
    Set<String> allHandles = driver.getWindowHandles();
    for (String oneOfHandles : allHandles) {
      if (oneOfHandles.equals(parentHandle)) {
        driver.switchTo().window(oneOfHandles);
      }
    }
    Thread.sleep(3*1000);
    clickElement(submitBtn);
    String errorMessageText = errorMessage.getText();
    Thread.sleep(2*1000);
    return verifyElementExist(errorMessage)
            && errorMessageText.equals("This case is already submitted by someone else.");
  }
}
