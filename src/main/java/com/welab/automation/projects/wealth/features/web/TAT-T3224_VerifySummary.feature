Feature: verify Summary Page

  Background: verify Summary Page

  @WELATCOE-589
  Scenario: Verify Investment Account Summary page
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto Investment Account Summary page

  @WELATCOE-593
  Scenario Outline: Verify account detail view
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto Investment Account Summary page
    And I click View button at SummaryPage named <username>
    Examples:
      | username       |
      | Antonio8172525 |

  @WELATCOE-590
  Scenario: Verify search opened accounts
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And I goto Investment Account Summary page
    And verify search by Name out of scope
    And verify search by T24 CustomerID
    And verify search by FNZ head account
    And verify search by Account open date
    And verify reset button

  @WELATCOE-592
  Scenario: Verify sorting on opened accounts table
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto Investment Account Summary page
    And Sort by T24 Customer ID column
    And Sort by FNZ head account column
    And Sort by Investment account status column
    And Sort by Tradability column
    And Sort by Account open date column

