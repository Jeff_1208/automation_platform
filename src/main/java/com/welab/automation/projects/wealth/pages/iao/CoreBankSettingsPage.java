package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.By;

import static com.welab.automation.framework.utils.Utils.logFail;

public class CoreBankSettingsPage extends AppiumBasePage {

  private String pageName = "Core bank settings page";

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Personal information']"))
  @iOSXCUITFindAll(@iOSXCUITBy)
  private MobileElement personalInformation;

  public void openPersonalInfoPage() {}

  public void updatePersonalInfoPage() {}

  @AndroidFindBy(xpath = "//*[@text='Verify personal information']")
  private MobileElement verifyPersonalInformationTitle;

  /** basic info, name and birth. need to verify ID card to update * */
  @AndroidFindBy(xpath = "//*[@text='Personal information']/../android.view.ViewGroup[1]")
  private MobileElement editPersonalInfoBtn;

  @AndroidFindBy(xpath = "//*[@text='Verify your identity']")
  private MobileElement verifyIdentityTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='HKID verification']")
  private MobileElement HKIDVerificationTitle;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Security Authentication']/../../android.view.ViewGroup[2]")
  private MobileElement HKIDVerificationBackBtn;

  @AndroidFindBy(xpath = "//*[@text='Next']")
  private MobileElement nextBtn;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@content-desc='PERSONAL_INFO-info-item-info'][3]")
  private MobileElement displayedUsername;

  /** Account info, username, Email, Phone * */
  @AndroidFindBy(
      xpath = "(//android.widget.TextView[@content-desc='PERSONAL_INFO-info-item-info'])[5]")
  private MobileElement displayedEmailAddress;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc='PERSONAL_INFO-info-item-btn'][1]")
  private MobileElement editEmailBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Email']/../android.widget.EditText")
  private MobileElement emailEditBox;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Verify your new email address']/../../android.view.ViewGroup[2]")
  private MobileElement emailVerificationBackBtn;

  @AndroidFindBy(
      xpath = "(//android.widget.TextView[@content-desc='PERSONAL_INFO-info-item-info'])[6]")
  private MobileElement displayedPhone;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc='PERSONAL_INFO-info-item-btn'][2]")
  private MobileElement editPhoneBtn;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Mobile number']/../android.widget.EditText")
  private MobileElement phoneNumberEditBox;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Please enter a new mobile number']/../android.view.ViewGroup")
  private MobileElement phoneUpdateBackBtn;

  /** Residential Address. * */
  // fail to find by content-desc.
  //    @AndroidFindBy(xpath =
  // "//android.view.ViewGroup[@content-desc='PERSONAL_INFO-info-item-btn'][3]")
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Residential Address']/../android.view.ViewGroup")
  private MobileElement editResidentialAddressBtn;

  // 'r' stands for Residential Address
  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "(//android.widget.TextView[@text='Residential address']/../android.view.ViewGroup//android.widget.EditText)[1]"),
    @AndroidBy(xpath = "//android.widget.TextView[@text='District']/../android.widget.EditText")
  })
  private MobileElement rDistrictEditBox;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "(//android.widget.TextView[@text='Residential address']/../android.view.ViewGroup//android.widget.EditText)[2]"),
    @AndroidBy(
        xpath = "//android.widget.TextView[@text='Estate / Street']/../android.widget.EditText")
  })
  private MobileElement rStreetEditBox;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "(//android.widget.TextView[@text='Residential address']/../android.view.ViewGroup//android.widget.EditText)[3]"),
    @AndroidBy(
        xpath = "//android.widget.TextView[@text='Building / Block']/../android.widget.EditText")
  })
  private MobileElement rBlockEditBox;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "(//android.widget.TextView[@text='Residential address']/../android.view.ViewGroup//android.widget.EditText)[4]"),
    @AndroidBy(xpath = "//android.widget.TextView[@text='Floor']/../android.widget.EditText")
  })
  private MobileElement rFloorEditBox;

  @AndroidFindAll({
    @AndroidBy(
        xpath =
            "(//android.widget.TextView[@text='Residential address']/../android.view.ViewGroup//android.widget.EditText)[5]"),
    @AndroidBy(xpath = "//android.widget.TextView[@text='Flat / room']/../android.widget.EditText")
  })
  private MobileElement rFlatEditBox;

  // 'm' stands for Mailing Address
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Same as residential address']/../android.view.ViewGroup[12]") // 15?
  private MobileElement mSameAsRAddressCheckBox;

  /** mail address editBox cannot be located by this way, need to use unique accessibility ID */
  // FIX: locate the mail address editBox
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='District'][2]/../android.widget.EditText")
  private MobileElement mDistrictEditBox;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Estate / Street'][2]/../android.widget.EditText")
  private MobileElement mStreetEditBox;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Building / Block'][2]/../android.widget.EditText")
  private MobileElement mBlockEditBox;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='FloorStreet'][2]/../android.widget.EditText")
  private MobileElement mFloorEditBox;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Flat / room']/../android.widget.EditText")
  private MobileElement mFlatEditBox;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Change address']/../android.view.ViewGroup")
  private MobileElement residentialBackBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Confirm']/../android.view.ViewGroup")
  private MobileElement confirmBtn;

  /** employment information * */
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Employment status']/../android.widget.TextView[3]")
  private MobileElement displayedEmploymentStatus;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Employment information']/../android.view.ViewGroup")
  private MobileElement editEmploymentStatusBtn;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Employment status']/../android.widget.EditText")
  private MobileElement employmentStatusEditBox;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Employment information']/../../../../android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
  private MobileElement employmentBackBtn;

  /** Nationality and region information * */
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Nationality / region']/../android.widget.TextView[2]")
  private MobileElement displayedRegion;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text='Nationality / region']/../android.view.ViewGroup")
  private MobileElement editRegionBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Nationality / region (you can select more than one nationality)']/../android.view.ViewGroup/android.widget.EditText")
  private MobileElement regionEditBox;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Edit nationality/region/tax jurisdiction']/../android.view.ViewGroup")
  private MobileElement regionBackBtn;

  public CoreBankSettingsPage() {
    super.pageName = pageName;
  }

  // update personal info need NFC feature to detect the ID Card
  // seems NFC function will let the app crash on emulator
  public void updatePersonalInfo() {
    // app crashes when detecting the ID card. exit before detecting.
    clickElement(editPersonalInfoBtn);

    waitUntilElementVisible(verifyIdentityTitle);
    clickElement(nextBtn);
    waitUntilElementVisible(HKIDVerificationTitle);
    /* TODO: find workaround to detect the ID card? */
    // wiat 5 seconds for demo. app will crash if proceed to detect the ID card.
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }
    clickElement(HKIDVerificationBackBtn);
  }

  public void updateEmailAddress(String newEmailAddress) {
    // doesn't receive the verification code, exit at entering the code
    waitUntilElementVisible(editEmailBtn);
    clickElement(editEmailBtn);
    waitUntilElementVisible(emailEditBox);
    // don't know why clearAndSendKeys() will not fill the editBox.
    //        clearAndSendKeys(emailEditBox, newEmailAddress);
    emailEditBox.sendKeys(newEmailAddress);
    hideKeyboard();
    waitUntilElementClickable(nextBtn);
    clickElement(nextBtn);
    // TODO: receive the code by email and finish update.
    //        waitUntilElementVisible(emailVerificationBackBtn);
    // sleep 5 seconds for demo
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }
    clickElement(emailVerificationBackBtn);
  }

  public String getDisplayedEmailAddress() {
    return getElementText(displayedEmailAddress);
  }

  public void updatePhoneNumber(String newPhoneNumber) {
    clickElement(editPhoneBtn);
    waitUntilElementClickable(phoneNumberEditBox);
    phoneNumberEditBox.sendKeys(newPhoneNumber);
    hideKeyboard();
    //        clearAndSendKeys(phoneNumberEditBox, newPhoneNumber);
    /* TODO: must use HK number to move on. */
    // sleep 5 seconds for demo
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }
    // exit here
    clickElement(phoneUpdateBackBtn);
  }

  public String getDisplayedPhoneNumber() {
    return getElementText(displayedPhone);
  }

  public void updateResidentialAddress(
      String rDistrict,
      String rStreet,
      String rBlock,
      String rFloor,
      String rFlat,
      boolean sameAsResidentialAddress,
      String mDistrict,
      String mStreet,
      String mBlock,
      String mFloor,
      String mFlat) {
    waitUntilElementVisible(verifyPersonalInformationTitle);
    scrollUpToFindElement(editResidentialAddressBtn, 10);
    clickElement(editResidentialAddressBtn);
    /* TODO: street need to be selected from a list, but the app fail to retrieve the list now. */
    //        clearAndSendKeys(rDistrictEditBox, rDistrict);
    // selectDistrict()
    //        clearAndSendKeys(rBlockEditBox, rBlock);
    //        clearAndSendKeys(rFloorEditBox, rFloor);
    //        clearAndSendKeys(rFlatEditBox, rFlat);
    rStreetEditBox.sendKeys(rStreet);
    rBlockEditBox.sendKeys(rBlock);
    rFloorEditBox.sendKeys(rFloor);
    rFlatEditBox.sendKeys(rFlat);

    if (!sameAsResidentialAddress) {
      scrollUp();
      //            clearAndSendKeys(mDistrictEditBox, mDistrict);
      // selectDistrict();
      //            clearAndSendKeys(mBlockEditBox, mBlock);
      //            clearAndSendKeys(mFloorEditBox, mFloor);
      //            clearAndSendKeys(mFlatEditBox, mFlat);
      mStreetEditBox.sendKeys(rStreet);
      mBlockEditBox.sendKeys(rBlock);
      mFloorEditBox.sendKeys(rFloor);
      mFlatEditBox.sendKeys(rFlat);
    } else {
      clickElement(mSameAsRAddressCheckBox);
    }
    // sleep 5 seconds for demo
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }

    // need to input street to continue. exit here.
    clickElement(residentialBackBtn);
    waitUntilElementVisible(verifyPersonalInformationTitle);
  }

  public String getDisplayedResidentialAddress() {
    // TODO: Residential address is not displayed. need to find workaround to get.
    return "";
  }

  public void updateEmploymentInfo(String employmentStatus) {
    scrollUpToFindElement(
        By.xpath(
            "//android.widget.TextView[@text='Employment information']/../android.view.ViewGroup"),
        10);
    clickElement(editEmploymentStatusBtn);
    // employment status need to be selected from a list, and the app fail to retrieve the list
    // appium inspector fail to capture the status selector.
    /* TODO: select the status */
    // sleep 5 seconds for demo
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }
    // need to input street to continue. exit here.
    clickElement(employmentBackBtn);
  }

  public String getDisplayedEmploymentStatus() {
    scrollUpToFindElement(
        By.xpath(
            "//android.widget.TextView[@text='Employment status']/../android.widget.TextView[3]"),
        10);
    return getElementText(displayedEmploymentStatus);
  }

  public void updateRegion(String region) {
    scrollUpToFindElement(
        By.xpath(
            "//android.widget.TextView[@text='Nationality / region']/../android.view.ViewGroup"),
        10);
    try {
      Thread.sleep(1000 * 5);
    } catch (Exception e) {
      logFail(e.getMessage());
    }
    // will pop out system problem when entering the update page.
    /* TODO */
  }

  public String getDisplayedRegion() {
    scrollUpToFindElement(
        By.xpath(
            "//android.widget.TextView[@text='Nationality / region']/../android.widget.TextView[2]"),
        10);
    return getElementText(displayedRegion);
  }

  public void backToTop() {
    scrollDown();
    scrollDown();
    scrollDown();
  }
}
