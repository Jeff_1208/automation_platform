package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForeignExchangePage extends AppiumBasePage {
  private final String pageName = "Foreign Exchange Page";

  @Getter
  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Transfer']"))
  @iOSXCUITFindBy(accessibility = "Transfer, tab, 3 of 5")
  private MobileElement homeForeignExchangeBtn;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='Add money']"))
  @iOSXCUITFindBy(accessibility = "Add money")
  private MobileElement  addMoneyBtn;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Foreign exchange']"))
  @iOSXCUITFindBy(accessibility = "Foreign exchange")
  private MobileElement foreignExchangeBtn;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='You convert']"))
  @iOSXCUITFindBy(iOSNsPredicate = "value == \"You convert\" AND type == \"XCUIElementTypeTextField\"")
  private MobileElement youConvert;

  @iOSXCUITFindBy(iOSNsPredicate = "value == \"You get\" AND type == \"XCUIElementTypeTextField\"")
  private MobileElement youGet;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Review']"))
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review'")
  private MobileElement reviewBtn;

  @iOSXCUITFindBy(iOSNsPredicate = "label == \"Important notes\"")
  private MobileElement mImportantNotes;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"getSelector\"]")
  private MobileElement youGetSelector;

  @iOSXCUITFindBy(
          xpath = "//*[starts-with(@name, 'Available amount')]")
  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[contains(@text,'Available amount: HKD')]"))
  private MobileElement availableAmount;

  @AndroidFindAll(
      @AndroidBy(
          xpath = "//android.widget.TextView[contains(@text,'Amount exceeds available balance')]"))
  @iOSXCUITFindBy(
      iOSNsPredicate =
          "label == \"Amount exceeds available balance\" AND name == \"Amount exceeds available balance\" AND value == \"Amount exceeds available balance\"")
  private MobileElement errorMessage;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[3]")
  private MobileElement slideToConfirmTxt;

  @iOSXCUITFindBy(
          xpath = "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
  private MobileElement slideToConfirmArrow;

  @iOSXCUITFindBy(
      xpath =
          "(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
  private MobileElement slideToConfirmBtn;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Done']"))
  @iOSXCUITFindBy(
          xpath =
                  "(//XCUIElementTypeOther[@name=\"Done\"])[2]")
  private MobileElement successDone;

  @iOSXCUITFindBy(
      xpath = "(//XCUIElementTypeStaticText[@name=\"text\"])[4]/../../XCUIElementTypeOther[3]")
  private MobileElement totalBalanceArrow;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Please review and confirm.']"))
  private MobileElement exchangeRatePrompt;

  @iOSXCUITFindBy(
          xpath = "//*[starts-with(@name, 'Wealth Balance')]")
  private MobileElement mWealthBalance;

  @iOSXCUITFindBy(iOSNsPredicate = "name == 'HKD'")
  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='HKD']"))
  private MobileElement HKD;

  @iOSXCUITFindBy(iOSNsPredicate = "name == 'USD'")
  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='USD']"))
  private MobileElement USD;

  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"convertSelector\"]")
  @AndroidFindAll(
      @AndroidBy(
          xpath =
              "//android.view.ViewGroup[@content-desc=\"convertSelector\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"))
  private MobileElement topBropDownBox;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.FrameLayout[1]/../android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup"))
  private MobileElement slideButton;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='Transaction in progress']"))
  private MobileElement transactionInProgress;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.EditText[@text='Your transaction is being processed. We'll notify you when it is completed.']"))
  private MobileElement transactionTips;

  @AndroidFindAll(@AndroidBy(xpath = "//android.widget.TextView[@text='Review']"))
  private MobileElement Done;

  @AndroidFindAll(@AndroidBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup"))
  private MobileElement accountBalance;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"USD Balance\"]")
  private MobileElement balance;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"alance(USD)\"]")
  private MobileElement balanceUsd;



  public ForeignExchangePage() {
    super.pageName = this.pageName;
  }

  public void clickHomeForeignExchange() {
    clickElement(homeForeignExchangeBtn);
  }

  public Boolean checkWealthBalance()  {
    return verifyElementExist(mWealthBalance);
  }

  public Boolean checkTotalBalanceArrow()  {
    return verifyElementExist(totalBalanceArrow);
  }

  public void clickTotalBalanceArrow() {
    clickElement(totalBalanceArrow);
  }

  public void clickImportantNotes() {
    clickElement(mImportantNotes);
  }

  public Boolean checkSuccesssDone() throws InterruptedException {
    return isElementVisible(successDone,120000);
  }

  public void clickSuccesssDone() {
    clickElement(successDone);
  }

  public void clickSlideToConfirm() {
    clickElement(slideToConfirmBtn);
  }

  public void enterAmount(String amount) {
    andSendKeys(youConvert,amount);
  }

  public void setSlideToConfirm() {
    if (System.getProperty("mobile").contains("android")) {
      List<String> yNow = getAttribute(slideButton);
      swipeDown(Integer.parseInt(yNow.get(0)), Integer.parseInt(yNow.get(1)),
              Integer.parseInt(yNow.get(2)), Integer.parseInt(yNow.get(3)));
    }
    else if (System.getProperty("mobile").contains("ios")){
      int widthArrow= getIOSAttributePoints(slideToConfirmArrow,"width")/2;
      int witdthTxt= getIOSAttributePoints(slideToConfirmTxt,"width");
      int btnX=getIOSAttributePoints(slideToConfirmBtn,"x");
      int btnY=getIOSAttributePoints(slideToConfirmBtn,"y");
      swipeDown(btnX,btnY,btnX+widthArrow+witdthTxt,btnY);
    }
  }

  public Boolean getForeignExchangeStatus() {
    return isElementNotVisible(foreignExchangeBtn,3);
  }
  public Boolean getAddMoneyStatus() {
    return isElementVisible(addMoneyBtn,1);
  }
  public void clickForeignExchangeBtn() {
    clickElement(foreignExchangeBtn);
  }
  public void inputYouConvert() {
    String money=getElementText(availableAmount);
    String regEx="[^0-9]";
    Pattern p = Pattern.compile(regEx);
    Matcher m = p.matcher(money);
    money= m.replaceAll("").trim();
    andSendKeys(youConvert,money);
  }
  public Boolean prompt() {
    return verifyElementExist(errorMessage);
  }

  public Boolean slideToConfirmStatus() {
    return isElementClickable(By.xpath("(//XCUIElementTypeOther[@name=\"Slide to Confirm\"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeO￼ther[2]"));
  }

  public Boolean reviewBtnstatus() {
    return isElementVisible(reviewBtn,600);
  }

  public void swipeDown(int xstart, int ystart, int xend, int yend) {
    TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
    PointOption pointOption1 = PointOption.point(xstart, ystart);
    PointOption pointOption2 = PointOption.point(xend, yend);
    Duration duration = Duration.ofMillis(1000);
    WaitOptions waitOptions = WaitOptions.waitOptions(duration);
    touchAction.press(pointOption1).waitAction(waitOptions).moveTo(pointOption2).release()
            .perform();
  }

  public int getIOSAttributePoints(WebElement element,String aName){
    WebElement seekBar = element;
    String rect=seekBar.getAttribute("rect");
    if(aName=="y"){
      String strx= rect.split(",")[0].toString().split(":")[1];
      return (Integer.parseInt(strx));
    }
    else if(aName=="x"){
      String stry= rect.split(",")[1].toString().split(":")[1];
      return (Integer.parseInt(stry));
    }
    else if (aName.toLowerCase()=="width"){
      String strx= rect.split(",")[2].toString().split(":")[1];
      return (Integer.parseInt(strx));
    }
    return 0;
  }
  public void clickReview(){
    clickElement(reviewBtn);
  }

  public Boolean enRouteInput(String amount,String time) throws ParseException, InterruptedException {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date now = new Date();
    Calendar nowTime = Calendar.getInstance();
    nowTime.add(Calendar.MINUTE, Integer.parseInt(time));
    nowTime.add(Calendar.MINUTE, Integer.parseInt(String.valueOf(Integer.parseInt(time)/2)));
    Date halfwayTime= sdf.parse(sdf.format(nowTime.getTime()));
    Date endTime= sdf.parse(sdf.format(nowTime.getTime()));
    for (int i=0;i<1;){
      SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date now1 = new Date();
      Date startTime= sdf1.parse(sdf.format(now1));
      if (compareDate(startTime,halfwayTime)==1 && amount!=""){
        andSendKeys(youConvert,amount);
      }
      i=compareDate(startTime,endTime);
      if (isElementVisible(exchangeRatePrompt,1)==true){
        return verifyElementExist(reviewBtn);
      }
      Thread.sleep(60000);
    }
    return false;
  }
  public static int compareDate(Date now, Date validity) {
    if(validity != null){
      try {
        if (now.getTime() > validity.getTime())
          return 1;
        else if (now.getTime() < validity.getTime())
          return 0;
        else
          return 0;
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    return 0;
  }
  public void clickDropDownBox(String currency) {
    clickElement(availableAmount);
    clickElement(topBropDownBox);
    String actualValue=getElementText(availableAmount);
    if (actualValue!=currency){
      if(currency.equalsIgnoreCase("USD")){
        clickElement(USD);
      }else if(currency.equalsIgnoreCase("HKD")){
        clickElement(HKD);
      }
    }
  }
  public List<String> getAttribute(WebElement element){
    WebElement seekBar = element;
    String Now=seekBar.getAttribute("bounds");
    return test(Now);
  }

  public static List<String> test(String Now) {
    String result = Now.replaceAll("[-+.^:,]","");
    List<String> lst = new ArrayList<String>();
    String quStr=Now.substring(Now.indexOf("[")+1,Now.indexOf("]"));
    int index = Now.indexOf("]");
    String after1 = Now.substring(index + 1);
    String[] yNow =quStr.split(",");
    lst.add(yNow[0]);
    lst.add(yNow[1]);
    String after2=after1.substring(after1.indexOf("[")+1,after1.indexOf("]"));
    String[] xStart =after2.split(",");
    lst.add(xStart[0]);
    lst.add(xStart[1]);
    return lst;
  }
  public Boolean transactionStatus() {
    if (isElementVisible(transactionInProgress,1)==true && isElementVisible(transactionTips,1)==true){
      return true;
    }else {
      return false;
    }
  }
  public boolean whether(){
    return isElementVisible(Done,1);
  }
  public void  clickBalanceDetails(){
    waitUntilElementVisible(accountBalance,1);
    clickElement(accountBalance);
  }
  public void  clickBalance(){
    waitUntilElementVisible(balance,1);
    if (isElementVisible(balance)==true){
      clickElement(accountBalance);
    }

  }
  public Boolean balanceUsd(){
    waitUntilElementVisible(balanceUsd,1);
    return isElementVisible(balanceUsd,1);
  }


}
