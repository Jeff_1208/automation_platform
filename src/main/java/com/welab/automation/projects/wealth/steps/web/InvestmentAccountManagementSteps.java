package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.InvestmentAccountManagementPage;
import io.cucumber.java.en.And;
import org.picocontainer.annotations.Inject;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class InvestmentAccountManagementSteps {
    @Inject
    InvestmentAccountManagementPage InvestmentAccountManagementPage;

    @And("I clicked on investment account management")
    public void Approval() throws IOException {
        InvestmentAccountManagementPage.clickAccountManagement();
    }
    @And("I click on account management the maker")
    public void maker() {
        InvestmentAccountManagementPage.clickaccountManagementMaker();
    }
    @And("I click on edit")
    public void clickEdit() {
        InvestmentAccountManagementPage.clickeditBtn();
    }
    @And("Change investment account status")
    public void editAccountStatus() {
        InvestmentAccountManagementPage.clickmakerRemark();
    }
    @And("Get the data of the checker")
    public void getCheckerData() {
        InvestmentAccountManagementPage.clickChecks();
        InvestmentAccountManagementPage.getCheckList();
    }

    @And("I find the data I need to click on on the maker page")
    public void clickDataMaker() {
        InvestmentAccountManagementPage.clickDate();
    }
    @And("I find the data I need to click on on the maker page ([^\"]\\S*) ([^\"]\\S*)$")
    public void clickData(String status,String Tradability) {
        InvestmentAccountManagementPage.clickDate1(status,Tradability);

    }
    @And("Click the corresponding status ([^\"]\\S*) ([^\"]\\S*)$")
    public void clickDataStatus(String status,String Tradability) {
        InvestmentAccountManagementPage.clickAccountStatus(status,Tradability);
    }
    @And("Click the submit button")
    public void clickSubmitButton() {
        InvestmentAccountManagementPage.clickeSubmitBtn();
    }
    @And("Enter the investment summary interface ([^\"]\\S*)$")
    public void clickInvestment(String Tradability) {
        assertThat(InvestmentAccountManagementPage.clickInvestmentBtn(Tradability)).isEqualTo(true);
    }
}
