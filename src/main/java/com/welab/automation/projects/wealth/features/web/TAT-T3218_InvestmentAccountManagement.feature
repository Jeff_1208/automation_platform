Feature: wealth
  Background: Open wealth.portal
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/

  @WELATCOE-572
  Scenario Outline: Approve application which has documents to be validated by Maker
    And login keycloak
      | username | ops1 |
      | password | ops1 |
    And I clicked on investment account management
    And Get the data of the checker
    And I click on account management the maker
    And I find the data I need to click on on the maker page <status> <Tradability>
    And I find the data I need to click on on the maker page
    And I click on edit
    And Change investment account status
    And Click the corresponding status <status> <Tradability>
    And Click the submit button
    And Enter the investment summary interface <Tradability>
    Examples:
      | status |Tradability|
      |  Active|No-Sell    |
