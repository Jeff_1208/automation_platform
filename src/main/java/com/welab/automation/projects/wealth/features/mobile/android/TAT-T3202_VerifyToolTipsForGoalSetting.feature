Feature: Verify tool tips function for goal setting

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  Scenario Outline: Verify tool tips function for goal setting from begin to model portfolio detail page.
    And Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I Start To Set My Target with <targetValue> and <time>
    And I move eyeBtn to top right corner
    Then I check if tips are correct
    And I save my goal and goto Portfolio Recommendation
    Then I check if tips are correct
    And I View Portfolio
    Then I check if tips are correct
    And I View Portfolio Fund List
    Then I check if tips are correct
    And I view Featured Fund
    Then I check if tips are correct
    Then I finish checking all the tip tools
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario Outline: Verify tool tips function for goal setting of financial freedom
    And Login with user and password
      | user     | test096  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    When I go to Wealth Page
    When I click Add New Goals
    And I Start To Set Goal to Achieve Financial Freedom by age <age>
    # have no idea why this tip icon cannot be clicked, so comment here.
    #Then I check if tips are correct
    And I calculate target wealth by <monthlyExpenses> and <otherExpenses>
    And I move eyeBtn to top right corner
    Then I check if tips are correct
    Then I finish checking all the tip tools
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |