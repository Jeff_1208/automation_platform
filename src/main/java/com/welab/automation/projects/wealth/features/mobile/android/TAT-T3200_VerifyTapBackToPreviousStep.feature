Feature: Verify tap back to previous step

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears


  Scenario: Verify tap back to previous step when non-CRPQ
    And Login with user and password
      | user     | test211  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And Click Next on Mutual Fund Services Welcome page
#    click back on update and verify personal info page
    When I click back icon
    Then I'm on mutual fund services welcome page
    And I begin to check personal information
#    click back on verify personal information page
    When I click back icon
    Then I'm on particular page
      | Update and verify personal info |
    And I click next button
    And I click next button
#    click back on declaration - employee of intermediary page
    When I click back icon
    And I'm on particular page
      | Verify personal information |
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                               | answer |
      | Declaration – Employee of Intermediary | No     |
#    click back on qualification information page
    And I click back icon
    And I'm on particular page
      | Declaration – Employee of Intermediary |
    And I select nonCRPQ answer
      | question                               | answer                    |
      | Declaration – Employee of Intermediary | No                        |
      | Qualification information              | University level or above |
#    click back on investment horizon page
    And I click back icon
    And I'm on particular page
      | Qualification information |
    And I select nonCRPQ answer
      | question                  | answer                    |
      | Qualification information | University level or above |
      | Investment Horizon        | Up to 1 year              |
#    click back on investment product knowledge and experience page
    And I click back icon
    And I'm on particular page
      | Investment Horizon |
    And I select nonCRPQ answer
      | question                                    | answer            |
      | Investment Horizon                          | Up to 1 year      |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds |
#    click back on net worth page
    And I click back icon
    And I'm on particular page
      | Investment Product Knowledge and Experience |
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    And I click confirm on nonCRPQ review page
#    click back on terms and conditions page
    And I click back icon
    And I'm on particular page
      | Review investment account profile |
  ###comment this because currently the account cannot be clear
#    And I click confirm on nonCRPQ review page
#    And I confirm the Investment account profile
##    click back on your application is being processed page
#    And I click back icon
#    And I'm on particular page
#      | Your application is being processed |
#    And I click next button
#    And I can see document transition page
#    And I click next on document transition page

  Scenario: Verify tap back to previous step when CRPQ
    And Login with user and password
      | user     | test186  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    Given I goto wealth main page
        # F_01c_transition
    And Finished uploading document
    And Starting CRPQ
#    click back on first crpq question page
    And I click back icon
    And I'm on CRPQ start page
    And Starting CRPQ
    And I Select Answer for Can you tell me why you're investing with us?
      | Preserve my capital and earn deposit rate |
#    click back on 2nd crpq question page
    And I click back icon
    And I'm on particular page
      | Can you tell me why you're investing with us? |
    And I Select Answer for Can you tell me why you're investing with us?
      | Preserve my capital and earn deposit rate |
    And I Select Answer for How do you see risk-return? I'm willing to take
      | moderate risk to achieve moderate returns |
#    click back on 3rd crpq question page
    And I click back icon
    And I'm on particular page
      | How do you see risk-return? I'm willing to take |
    And I Select Answer for How do you see risk-return? I'm willing to take
      | moderate risk to achieve moderate returns |
    And I Select Answer for I can accept if my investment value fluctuates between
      | -20% and 20% |
#    click back on 4th crpq question page
    And I click back icon
    And I'm on particular page
      | I can accept if my investment value fluctuates between _________ over 5 years. |
    And I Select Answer for I can accept if my investment value fluctuates between
      | -20% and 20% |
    And I Select Answer for How much of your total liquid assets
      | Less than 5% |
#    click back on 5th crpq question page
    And I click back icon
    And I'm on particular page
      | How much of your total liquid assets* can you set aside for savings and investments? |
    And I Select Answer for How much of your total liquid assets
      | Less than 5% |
    And I Select Answer for To meet my financial needs in the next 12 months
      | Sell up to 25% |
#    click back on 6th crpq question page
    And I click back icon
    And I'm on particular page
      | To meet my financial needs in the next 12 months, at most, I will ____________ of investments. |
    And I Select Answer for To meet my financial needs in the next 12 months
      | Sell up to 25% |
    And I Select Answer for If your investment value dropped by 50% over the past 6 months, how would you react?
      | Invest more to take advantage of lower price |
#    click back on 7th crpq question page
    And I click back icon
    And I'm on particular page
      | If your investment value dropped by 50% over the past 6 months, how would you react? |
    And I Select Answer for If your investment value dropped by 50% over the past 6 months, how would you react?
      | Invest more to take advantage of lower price |
    And I Select Answer for How many months of expenses have you put aside, and can easily accessed to convert to cash*?
      | 12 months or above |
#    click back on review page
    And I click back icon
    And I'm on particular page
      | How many months of expenses have you put aside, and can easily accessed to convert to cash*? |
    And I Select Answer for How many months of expenses have you put aside, and can easily accessed to convert to cash*?
      | 12 months or above |

      ###comment this because currently the account cannot be clear
#    And I click confirm button
##    click back on score page
#    And I click back icon
#    And I'm on particular page
#      | Confirm |
#    And I click confirm button
#    And I click next button
##    click back on congratulations page
#    And I click back icon
#    And I'm on particular page
#      | Your Customer Risk Rating is |
#    And I click next button
#    And I click next button

  @WELATCOE-542
  Scenario: Verify tap back to App home page
    And Login with user and password
      | user     | Arielle4316744  |
      | password | Aa123321        |
    And I prepare none-CRPQ through wealth tab
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I answer the none-CRPQ questions at "none-CRPQ"
    And I click back icon
    Then I can see App homepage