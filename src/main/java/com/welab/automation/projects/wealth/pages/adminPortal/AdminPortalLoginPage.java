package com.welab.automation.projects.wealth.pages.adminPortal;

import com.welab.automation.framework.base.WebBasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdminPortalLoginPage extends WebBasePage {

  private String pageName = "admin portal login page";

  @FindBy(how = How.ID, using = "username")
  private WebElement usernameEditBox;

  @FindBy(how = How.ID, using = "password")
  private WebElement passwordEditBox;

  @FindBy(how = How.ID, using = "kc-login")
  private WebElement loginBtn;

  public AdminPortalLoginPage() {
    super.pageName = this.pageName;
  }

  public void loginAdminPortal(String username, String password) {
    clearAndSendKeys(usernameEditBox, username);
    clearAndSendKeys(passwordEditBox, password);
    clickElement(loginBtn);
  }
}
