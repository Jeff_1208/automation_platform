package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import com.welab.automation.projects.ScenarioContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.welab.automation.framework.utils.TimeUtils.getCurrentTimeAsString;

public class MobileHooks extends AppiumBasePage {
  private static final Logger logger = LoggerFactory.getLogger(MobileHooks.class);
  BaseDriver baseDriver;
  boolean recordVideo = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("recordVideo"));

  @Before
  public void beforeScenario(Scenario scenario) throws IOException {
    logger.info("* Start running scenario: {}", scenario.getName());
    this.baseDriver = new BaseDriver();
    this.baseDriver.initMobileDriver();
    if (recordVideo) {
      startRecording();
    }
  }

  @After
  public void afterScenario(Scenario scenario) {
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    String screenshotVideoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      stopRecording(screenshotVideoName);
    }

    if (scenario.isFailed()) {
      // Take screenshot to report
      final byte[] screenshot =
          ((TakesScreenshot) BaseDriver.getMobileDriver()).getScreenshotAs(OutputType.BYTES);
      scenario.attach(screenshot, "image/png", screenshotVideoName);
      // Take screenshots to project folder
      takeScreenshot(screenshotVideoName);
    }
    logger.info("* End running scenario: {}", scenario.getName());
    BaseDriver.closeMobileDriver();
    ScenarioContext.getInstance().clearScenarioData();
  }
}
