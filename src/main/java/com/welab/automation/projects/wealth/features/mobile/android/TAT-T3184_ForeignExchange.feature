@Android
Feature: Demo Foreign Exchange On WeLab app

  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears

  Scenario: Verify 'Foreign Exchange' button is not shown on transfer page
    Given Check upgrade page appears
    And Login with user and password
      | user     | test026  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I check the Foreign Exchange Not Show

  Scenario: Verify 'Foreign Exchange' button is shown on transfer page
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I check the Foreign Exchange Show

  Scenario: Verify that my input exceeds the available balance
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And The entered amount is greater than the available balance
    And I saw the prompt

  Scenario Outline: Verify exchange rate is expired
    Given Check upgrade page appears
    And Login with user and password
      | user     | test101  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I entered the foreign exchange page
    And I enter amount <amount>
    And I check review button displayed for <number> mins
    And I clicked the review button
    And I want to enter the <amount> while waiting for <number> minutes
    Examples:
      | amount    |number|
      | 20      | 10     |

  Scenario Outline: Select and change available FX currency pairs
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I choose the <currency>
    And I enter the <amount>
    And I submitted my foreign exchange order
    And I check Success Done
    Examples:
      | currency | amount |
      | USB      | 33     |

  Scenario Outline: Verify transaction is successful
    Given Check upgrade page appears
    And Login with user and password
      | user     | test194  |
      | password | Aa123321 |
    And I go to Foreign Exchange Page
    And I check the Add Money Show
    And I enter the <amount>
    And I submitted my foreign exchange order
    And I saw the submitted order
    And I saw the order with successful conversion
    And I went to the balance details to check the converted amount
    And I clicked USB balance
    And I saw the order information
    Examples:
      | amount |
      | 33     |

