@iOS
Feature: Add edit button to input investment value for Set my Target for goal setting

  Scenario: set account
    Given I launch app with account
    |account|password|
    |test112|Aa123321|

  @WELATCOE-730
  Scenario: user input amount for one time exceeds the max value (300k) - my target
    When I click Add New Goals
    And I setup my target with name
      |name| targetValue | time |  oneTimeValue|   monthlyValue|
      |730 |             | 60   |              |               |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
    |name|
    |730 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-730
  Scenario: user input amount for monthly exceeds the max value (30k) - my target
    When I open the goal setting
    |name|
    |730 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      |              |   3000001  |
    Then I see the exceed max amount error message

  @WELATCOE-734
  Scenario: user input amount for one time investment exceeds the max value (300k) - investment habit
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Build My Investing Habit with name
      |name| oneTimeValue|   monthlyValue|  month  |
      |734 |             |               |    60   |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
      |name|
      |734 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-734
  Scenario: user input amount for monthly investment exceeds the max value (30k) - investment habit
#    When I go to Wealth Page
    When I open the goal setting
      |name|
      |734 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      |              |   30000001 |
    Then I see the exceed max amount error message

  @WELATCOE-738
  Scenario: user input amount for one time investment exceeds the max value (300k) - investment habit
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup Achieve Financial Freedom with name
      | name  | age | monthlyExpenses | otherExpenses   |  oneTimeValue | monthlyValue |
      |  738  | 60  |      5000       | ,,              |               |              |
    And I click the discover
    And I buy the fund without confirm
      | currency |
      | HKD      |
    And I quit the ordering process
    And The status of new goal setting is draft
    When I open the goal setting
      |name|
      |738 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      | 30000001     |            |
    Then I see the exceed max amount error message

  @WELATCOE-738
  Scenario: user input amount for monthly investment exceeds the max value (30k) - investment habit
#    When I go to Wealth Page
    When I open the goal setting
      |name|
      |738 |
    And I edit investment plan
      | oneTimeValue |monthlyValue|
      |              |   30000001 |
    Then I see the exceed max amount error message

