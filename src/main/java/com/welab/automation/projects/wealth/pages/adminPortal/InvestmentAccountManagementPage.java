package com.welab.automation.projects.wealth.pages.adminPortal;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.wealth.WelabAPITest.VerifyUiAndApiGoalSetting;
import lombok.SneakyThrows;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

public class InvestmentAccountManagementPage  extends WebBasePage {

    List<String> listMaker = new ArrayList<>();

    @FindBy(how = How.XPATH, using = "//span[@class=\"menu-title ng-tns-c134-8\"]")
    private WebElement accountManagement;

    @FindBy(how = How.XPATH, using = "//*[@href=\"/investment-account-managment/maker\"]")
    private WebElement accountManagementMaker;

    @FindBy(how = How.XPATH, using = "//*[@href=\"/investment-account-managment/checker\"]")
    private WebElement accountManagementChecks;

    @FindBy(how = How.XPATH, using = "//button[text()='Edit']")
    private WebElement editBtn;

    @FindBy(how = How.XPATH, using = "//th[text()=' Maker ']")
    private WebElement  makerBtn;

    @FindBy(how = How.XPATH, using = "//button[text()=' Active ']")
    private WebElement activeBtn;

    @FindBy(how = How.XPATH, using = "//button[text()=' Inactive ']")
    private WebElement inactiveBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='Maker's Remark']")
    private WebElement makerRemark;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[2]")
    private List<WebElement>  t24Id;

    @FindBy(how = How.XPATH, using = "//a[@class=\"page-link ng-star-inserted\"]")
    private List<WebElement> linkinserted;

    @FindBy(how = How.CSS, using = "[aria-label=Next]")
    private WebElement nextBtn;

    @FindBy(how = How.XPATH, using = "//div[@class=\"operationPanel\"][1]/table/tr[1]/td/button")
    private List<WebElement> accountStatus;

    @FindBy(how = How.XPATH, using = "//div[@class=\"operationPanel\"][1]/table/tr[2]/td/button")
    private List<WebElement>  tradability;

    @FindBy(how = How.XPATH, using = "//input[@class=\"ng-star-inserted\"]")
    private WebElement inputBox;

    @FindBy(how = How.XPATH, using = "//button[text()='Search']")
    private WebElement search;

    @FindBy(how = How.XPATH, using = "//tr[@class=\"ng-star-inserted\"]/th[1]")
    private WebElement ViewBut;

    @FindBy(how = How.XPATH, using = "//button[text()='Submit']")
    private WebElement submitBtn;

    @FindBy(how = How.XPATH, using = "//span[text()='Investment Account Summary']")
    private WebElement Summarytn;

    @FindBy(how = How.XPATH, using = "//div[@class='subContainer'][3]/tr[2]/td[2]")
    private WebElement subContainerText;

    @FindBy(how = How.XPATH, using = "//button[text()='< Back']")
    private WebElement backBut;

    public void clickAccountManagement(){
        clickElement(accountManagement);
    }
    @SneakyThrows
    public void clickaccountManagementMaker(){
        clickElement(accountManagementMaker);
        Thread.sleep(7000);
    }
    @SneakyThrows
    public void clickeditBtn(){
        Thread.sleep(20000);
        clickElement(editBtn);
    }
    public void clickmakerRemark(){
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
    }
    @SneakyThrows
    public void clickChecks(){
        Thread.sleep(2000);
        clickElement(accountManagementChecks);
    }

    @SneakyThrows
    public void getCheckList(){
        Thread.sleep(20000);
        for (int a=1;a<=linkinserted.size();a++){
            for(WebElement row : t24Id){
                listMaker.add(row.getText());
            }
            if (a==linkinserted.size()){
                clickElement(nextBtn);
                Thread.sleep(7000);
            }
        }
    }
    @SneakyThrows
    public void clickDate(){
        Thread.sleep(20000);
        int link= linkinserted.size()-1;
        int  linkstr= Integer.parseInt(linkinserted.get(link).getText());
        int total=60;
        int page=1;
        if (linkstr!=1) {
          total = linkstr*10;

        }
        List<String> data= VerifyUiAndApiGoalSetting.getDataStatus(page,total);
        for (int a=0;a<data.size();a++){
            String id=data.get(a);
            if (listMaker.contains(id)){
                andSendKeys(inputBox,id);
                clickElement(search);
                listMaker.clear();
                listMaker.add(id);
                Thread.sleep(2000);
                clickElement(ViewBut);
                break;
            }
        }
    }
    @SneakyThrows
    public void clickDate1(String status,String Tradability){
        Thread.sleep(20000);
        int link= linkinserted.size()-1;
        int  linkstr= Integer.parseInt(linkinserted.get(link).getText());
        int total=60;
        int page=1;
        if (linkstr!=1) {
            total = linkstr*10;

        }
        List<String> data= VerifyUiAndApiGoalSetting.getDataStatus(page,total);
        for (int a=0;a<data.size();a++){
            String id=data.get(a);
            if (listMaker.contains(id)){
                andSendKeys(inputBox,id);
                clickElement(search);
                listMaker.add(id);
                Thread.sleep(2000);
                clickElement(ViewBut);
                clickeditBtn();
                Thread.sleep(2000);
                clickmakerRemark();
                Thread.sleep(2000);
                clickAccountStatus(status,Tradability);
                Thread.sleep(2000);
                scrollToTop("Submit");
                Thread.sleep(2000);
                clickElement(submitBtn);
                Thread.sleep(2000);
                String str=driver.switchTo().alert().getText();
                Alert altElement = driver.switchTo().alert();
                acceptAlert(altElement);
                if (str.equals("success")){
                    break;
                }else if (!str.equals("success")){
                    clickElement(backBut);
                    Thread.sleep(2000);
                }

            }
        }
    }
    @SneakyThrows
    public void clickAccountStatus(String status, String Tradability){
        List<String> list = new ArrayList<>();
        for(int i=0;i<accountStatus.size();i++){
            if (accountStatus.get(i).getAttribute("class")!=null) {
                if (accountStatus.get(i).getAttribute("class").equals("btn operationBtn green") || accountStatus.get(i).getAttribute("class").equals("btn operationBtn red")) {
                    if (accountStatus.get(i).getText().equals(status)) {
                        accountStatus.get(i).click();
                        break;
                    }
                }
            }
        }
        for(int i=0;i<tradability.size();i++){
            if (tradability.get(i).getAttribute("class")!=null) {
                if (tradability.get(i).getAttribute("class").equals("btn operationBtn green") || tradability.get(i).getAttribute("class").equals("btn operationBtn red")) {
                    if (tradability.get(i).getText().equals(Tradability)) {
                        tradability.get(i).click();
                        break;
                    }
                }
            }
        }
    }



    @SneakyThrows
    public boolean clickeSubmitBtn(){
        scrollToTop("Submit");
        Thread.sleep(2000);
        clickElement(submitBtn);
        String str=driver.switchTo().alert().getText();
        boolean fla=false;
        if (str.equals("success")){
            fla=true;
        }
        return fla;
    }
    @SneakyThrows
    public boolean clickInvestmentBtn(String tradabilityTexe){
        Thread.sleep(20000);
        clickElement(Summarytn);
        andSendKeys(inputBox,listMaker.get(0));
        clickElement(search);
        Thread.sleep(2000);
        clickElement(ViewBut);
        Thread.sleep(2000);
       String str=getElementText(subContainerText);
       boolean subContainer=false;
       if (str.equalsIgnoreCase(tradabilityTexe)){
           return subContainer=true;
       }
       return subContainer;
    }
}
