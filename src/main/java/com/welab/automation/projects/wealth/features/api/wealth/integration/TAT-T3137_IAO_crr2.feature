Feature: IAO_crr2
  Scenario Outline: <caseName>
    Given test description: IAO_crr2 "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|
    |IAO_crr2|1|Fetch Basic Account Status|/v1/wealth/account|Get||||200||||data.mutualFundServicesAccount||test208:Aa123321||
    |IAO_crr2|2|Fetch Personal Information|/v1/wealth/customer/personal-info|Get||||200||{'data.contact.chineseName':'equals(大文)'}||||||
    |IAO_crr2|3|Fetch Non-CRPQ Questions|/v1/wealth/questionnaires/non-crpq|Get||||200||{'data.questions.questionKey[1]':'equals(INH01)'}||||||
    |IAO_crr2|4|Submit IAO Request|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer.json|200||{'data.vcValue':'equals(Not VC)'}||||||
    |IAO_crr2|5|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=V2VsYWIgQ2xvdWQg.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||||
    |IAO_crr2|6|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||||
    |IAO_crr2|7|Fetch CRPQ Questions|/v1/wealth/questionnaires/crpq|Get||||200||{'data.questions.questionKey[0]':'equals(RAI01)','data.questions.questionKey[0]':'equals(RAI01)'}||||||
    |IAO_crr2|8|Submit CRPQ Answers|/v1/wealth/questionnaires/answers|Post|||Welab/body/CRPQAnswer_crr2.json|200||{'data.crrDescription':'equals(Conservative)'}||||||
    |IAO_crr2|9|Fetch CRPQ Questions & Answer|/v1/wealth/questionnaires/answers/crpq|Get||||200||{'data.questions.options[0].optionKey[0]':'equals(RAI01a)'}||||||
    |IAO_crr2|10|Fetch Risk Profile Result|/v1/wealth/risk-profiles|Get||||200||{'data.questionnaireName':'contains(CRPQ_V1)'}||||||
    |IAO_crr2|11|Fetch IAO Application status|/v1/wealth/investment-account-status|Get||||200||{'data.crr':'equals(2)'}||data.accOpeningStatus||||
