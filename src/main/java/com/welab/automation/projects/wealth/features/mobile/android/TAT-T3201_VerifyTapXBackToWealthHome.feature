Feature: Verify tap X back to wealth home page

  Background: Login with correct user and password
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears

  Scenario: Verify tap X back to wealth home page_nonCRPQ process
    And Login with user and password
      | user     | test015  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I prepare none-CRPQ through wealth tab
      # Click X on A_03_Selection
    And I click Got it on Welcome page
    When I click X icon
    Then I'm on wealth home page

      # Click X on A_04a_Goal
    Given goto loan main page
    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    When I click X icon
    Then I'm on wealth home page

      # Click X on A_04b_Goal
    Given goto loan main page
    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    And I scroll left on GoalBased Description page
    When I click X icon
    Then I'm on wealth home page

     # Click X on A_04c_Goal
    Given goto loan main page
    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    And I scroll left on GoalBased Description page
    And I scroll left on GoalBased Description page
    When I click X icon
    Then I'm on wealth home page
#      # B_01_onboarding, cannot working now, reported a bug
#    Given goto loan main page
#    And I goto wealth main page
#    And I click Got it on Welcome page
#    And I click GoalBased on Select GoalBased page
#    And I click Skip to open an account
#    When I click X icon
#    Then I'm on wealth home page

      # B_03_getStart
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And Click Next on Mutual Fund Services Welcome page
    When I click X icon
    Then I'm on wealth home page

      # bk/C_01_infoReview
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_03_non_CRPQ1
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_04_non_CRPQ2
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                   | answer |
      | Licensed Staff Declaration | No     |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_05_non_CRPQ3
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                   | answer                    |
      | Licensed Staff Declaration | No                        |
      | Qualification information  | University level or above |
    When I click X on CRPQ page
    Then I'm on wealth home page

        # C_06_non_CRPQ4
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                   | answer                    |
      | Licensed Staff Declaration | No                        |
      | Qualification information  | University level or above |
      | Investment Horizon         | Up to 1 year              |
    When I click X on CRPQ page
    Then I'm on wealth home page

        # C_07_non_CRPQ5
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                    |
      | Licensed Staff Declaration                  | No                        |
      | Qualification information                   | University level or above |
      | Investment Horizon                          | Up to 1 year              |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds         |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_08_non_CRPQReview
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Licensed Staff Declaration                  | No                            |
      | Qualification information                   | University level or above     |
      | Investment Horizon                          | Up to 1 year                  |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_09_notes
    Given goto loan main page
    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Licensed Staff Declaration                  | No                            |
      | Qualification information                   | University level or above     |
      | Investment Horizon                          | Up to 1 year                  |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    And I click confirm on nonCRPQ review page
    When I click X on CRPQ page
    Then I'm on wealth home page

  Scenario: Verify tap X back to wealth home page_CRPQ process
    And Login with user and password
      | user     | testsit059 |
      | password | Aa123321   |
    And I can see the LoggedIn page
    # F_01c_transition
    Given I goto wealth main page
    When I click X icon
    Then I'm on wealth home page

    # G_01_CRPQinfo
    Given goto loan main page
    And I goto wealth main page
    And Finished uploading document
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_02_CRPQ1
    Given goto loan main page
    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_03_CRPQ02
    Given goto loan main page
    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    And I Select Answer for Can you tell me why you're investing with us
      | Preserve my capital and earn deposit rate |
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_09_CRPQoverview
    Given goto loan main page
    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    And I Select Answer for Can you tell me why you're investing with us
      | Preserve my capital and earn deposit rate |
    And I Select Answer for How do you see risk-return
      | moderate risk to achieve moderate returns |
    And I Select Answer for I can accept if my investment value fluctuates between
      | -20% and 20% |
    And I Select Answer for How much of your total liquid assets
      | Less than 5% |
    And I Select Answer for To meet my financial needs in the next 12 months
      | Sell up to 25% |
    And I Select Answer for If your investment value dropped by 50%
      | Invest more to take advantage of lower price |
    And I Select Answer for How many months of expenses have you put aside
      | 12 months or above |
    When I click X on CRPQ page
    Then I'm on wealth home page

  Scenario: Verify tap X back to wealth home page_upload doc
    And Login with user and password
      | user     | test010  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    # D_01a_transition
    And I goto wealth main page
    And I can see document transition page
    When I click X icon
    Then I'm on wealth home page

    # E_01_upload
    Given goto loan main page
    And I goto wealth main page
    And I can see document transition page
    And I click next on document transition page
    When I click X on CRPQ page
    Then I'm on wealth home page

