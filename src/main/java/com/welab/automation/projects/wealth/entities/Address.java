package com.welab.automation.projects.wealth.entities;

import lombok.Getter;
import lombok.Setter;

public class Address {
    @Getter
    @Setter
    private String district;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String block;
    @Getter
    @Setter
    private int floor;
    @Getter
    @Setter
    private int room;
    @Getter
    @Setter
    private boolean isSameAsResidentialAddress;
}
