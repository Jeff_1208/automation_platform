package com.welab.automation.projects.wealth.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalLoginPage;
import com.welab.automation.projects.wealth.pages.adminPortal.AdminPortalMainPage;
import com.welab.automation.projects.wealth.pages.adminPortal.ApprovalPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebElement;
import org.picocontainer.annotations.Inject;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class IAOApprovalSteps extends WebBasePage {

  @Inject AdminPortalLoginPage adminPortalLoginPage;
  @Inject AdminPortalMainPage adminPortalMainPage;
  @Inject ApprovalPage approvalPage;

  @Given("^Open the URL ([^\"]\\S*)$")
  public void openUrl(String url) {
    try {
      navigateTo(url);
    } catch (Exception e) {
      logger.error(e.toString());
    }
  }

  @And("login admin portal")
  public void loginAsMaker(Map<String, String> data) {
    adminPortalLoginPage.loginAdminPortal(data.get("username"), data.get("password"));
  }

  @And("goto approval maker")
  public void gotoApprovalMaker() {
    adminPortalMainPage.gotoApprovalMaker();
  }

  @And("logout admin portal")
  public void logoutAdminPortal() {
    adminPortalMainPage.logout();
  }

  @And("goto approval checker")
  public void gotoApprovalChecker() {
    adminPortalMainPage.gotoApprovalChecker();
  }

  //  @And("^find the ([^\"]\\S*)'s application$")
  //  public void findApplication(String user) {
  //    approvalPage.findApplication(user);
  //  }

  @And("^approve the application of ([^\"]\\S*)$")
  public void approveApplication(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.approveApplication(targetApplication);
  }

  @And("^agree the application of ([^\"]\\S*)")
  public void agreeApplication(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.agreeApplication(targetApplication);
  }

  @And("^reject the application of ([^\"]\\S*)")
  public void rejectApplication(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.rejectApplication(targetApplication);
  }

  @And("^disagree the application of ([^\"]\\S*)")
  public void disagreeApplication(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.disagreeApplication(targetApplication);
  }

  @And("^check maker is decision is not editable ([^\"]\\S*)")
  public void checkNotEditable(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.agreeApplication(targetApplication);
    boolean notEditStatus = approvalPage.NotEditable();
    assertThat(notEditStatus).isTrue();
  }

  @And("^I click View button at approvalPage named ([^\"]\\S*)$")
  public void clickViewName(String user) {
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    List<String>INFO = approvalPage.getPersonalINFO(targetApplication);
    assertThat(targetApplication).isNotNull();
    approvalPage.clickViewNamed(targetApplication);
    boolean pageTop = approvalPage.isCheckerTopDetailPage();
    assertThat(pageTop).isTrue();
    boolean PersonalINFO = approvalPage.isPersonalINFO(INFO);
    assertThat(PersonalINFO).isTrue();
    boolean pageBottom = approvalPage.isCheckerBottomDetailPage();
    assertThat(pageBottom).isTrue();
  }

  @And("^disagree the application again of ([^\"]\\S*)")
  public void disagreeApplicationAgain(String user){
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.disagreeApplicationAgain(targetApplication);
  }

  @And("click submit button")
  public void submitButton(){
    approvalPage.submitButton();
  }

  @And("^approve the application again of ([^\"]\\S*)")
  public void approveApplicationAgain(String user){
    Map<String, WebElement> targetApplication = approvalPage.findApplication(user);
    assertThat(targetApplication).isNotNull();
    approvalPage.approveApplicationAgain(targetApplication);
  }

  @And("verify only show last Maker or Checker record")
  public void verifyOnlyShowCheckerLastRecord(){
    boolean lastRecord = approvalPage.verifyOnlyShowLastRecord();
    assertThat(lastRecord).isTrue();
  }

  @Given("Open a new TAB")
  public void openNewTab() {
     boolean  urlIsEqual = approvalPage.openNewTab();
     assertThat(urlIsEqual).isTrue();
  }

}


