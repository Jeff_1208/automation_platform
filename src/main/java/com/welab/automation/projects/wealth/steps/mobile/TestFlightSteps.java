package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.projects.wealth.pages.TestFlightPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;

import static org.assertj.core.api.Assertions.assertThat;

public class TestFlightSteps {

    private boolean isNew = false;
    private TestFlightPage testFlightPage = new TestFlightPage();

    @Given("I click the testflight app icon")
    public void iClickTheTestflightAppIcon() {
        if (System.getProperty("mobile").contains("cloud")) {
            testFlightPage.initApp();
        }
    }

    @When("I click Open button on app")
    public void iClickOpenButtonOnApp() {
        testFlightPage.installOrOpenApp();
    }

    @SneakyThrows
    @Then("I see the Welab app launched")
    public void iSeeTheWelabAppLaunched() {
        assertThat(testFlightPage.launchApp()).isNotNull();
    }
}
