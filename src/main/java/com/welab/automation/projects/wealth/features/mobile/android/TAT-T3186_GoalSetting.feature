Feature: Demo Wealth Management On WeLab app

#  Currently, wealth SDK is built to test on iOS, and it will auto login.
#  So need to comment out Background when testing on iOS.
  Background: Login
    Given Open WeLab App
    And Enable skip Root
    Given Check upgrade page appears
    And Login with user and password
      | user     | test006  |
      | password | Aa123321 |
    And I can see the LoggedIn page
    And I move eyeBtn to top right corner
    And I go to Wealth Page

  Scenario Outline: Set My Target goal  through reach my target picture, and finally exit with "Not now"
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click not now to stay in review and confirm order page
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario Outline: Set My Target goal  through reach my target picture, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario Outline: Set my goal through goal setting link
    When I click learn more link to create a goal
    And I click Set My Target and set <targetValue> and <time>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click not now to stay in review and confirm order page
    Examples:
      | targetValue | time |
      | 3000000     | 33   |

  Scenario Outline: Set My Target goal  through picture based on investing habit, and finally exit with "Not now"
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click not now to stay in review and confirm order page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Set My Target goal  through picture based on investing habit, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  Scenario Outline: Set My Target goal  through picture based on financial freedom, and finally exit with "no, continue"
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click not now to stay in review and confirm order page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  Scenario Outline: Set My Target goal  through picture based on financial freedom, and finally exit with "quit and save"
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  @WELATCOE-202 @WELATCOE-220 @debug
  Scenario Outline: Set My Target goal
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Set My Target
    And I can check the draft target
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  @WELATCOE-206 @WELATCOE-223 @debug
  Scenario Outline: Set Investing Habit goal
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Build My Investing Habit
    Then I can check the draft target
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |

  @WELATCOE-207 @WELATCOE-224 @debug
  Scenario Outline: Set Financial Freedom goal
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    Then I can view portfolio and funds
    And I can check tool tips of ratings
    And I back to wealth main page from Financial Freedom
    And I can check the draft target
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

  @WELATCOE-219
  Scenario: Dynamic interval for horizon setting of Build My Investing Habit
    When I click Add New Goals
    And I Start to Build My Investing Habit
    Then I check horizon interval of invest years
      | move to horizon index | expected years in icon |
      | 59                    | 60 months              |
      | 60                    | 5.5 years              |
      | 61                    | 6 years                |
      | 68                    | 9.5 years              |
      | 69                    | 10 years               |
      | 70                    | 11 years               |
      | 93                    | 34 years               |

  Scenario Outline: Create a new Rearch My Target goal with Insufficient amount
    When I click Add New Goals
    And I click Set My Target and set <targetValue> and <time>
    And I check the portfolio and buy it
    Then I can see there is Insufficient amount in your account
    Examples:
      | targetValue | time |
      | 3000000     | 50   |

  Scenario Outline: Create a new Build My Investing Habit goal with Insufficient amount
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I check the portfolio and buy it
    Then I can see there is Insufficient amount in your account
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |

  Scenario Outline:  Create a new Achieve Financial Freedom goal with Insufficient amount
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I check the portfolio and buy it
    Then I can see there is Insufficient amount in your account
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |


  @WELATCOE-218
  Scenario Outline: A-Set My Target goal  through picture based on financial freedom, and finally exit and verify  with   "Not now"
    When I go to Wealth Page
    When I click learn more link to create a goal
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click not now to stay in review and confirm order page

    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 64  | 600000          | Budget,Modest,Deluxe |


  @WELATCOE-218
  Scenario Outline: B-Set My Target goal  through picture based on financial freedom, and finally exit and verify  with "quit and save"
    When I go to Wealth Page
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I click X to close the review page
    And I click quit and save, then back to wealth page
    And I check the new goal status is draft
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  #TODO,559,601,603,Edit should be executed after creating a goal,but account can't make more goals now
  @WELATCOE-559
  Scenario Outline: Change Goal Name of Set My Target Goal
    When I go to Wealth Page
    And I click Goal of Set My Target Goal
    When I click Edit Button and set <goalName>
    And I click button back to WealthMainPage
    And I can check changed Goal Name of Set My Target
    Examples:
      | goalName                |
      | ReachMyTargetAAAAAAAAAA |

  @WELATCOE-601
  Scenario Outline: Change Goal Name of Achieve Financial
    When I go to Wealth Page
    And I click Goal of Achieve Financial
    When I click Edit Button and set <goalName>
    And I click button back to WealthMainPage
    And I can check changed Goal Name of Set My Target
    Examples:
      | goalName                |
      | AchieveFinancialBBBBBBB |

  @WELATCOE-603
  Scenario Outline: Change Goal Name of Build My investing Habit
    When I go to Wealth Page
    And I click Goal of Build My investing Habit
    When I click Edit Button and set <goalName>
    And I click button back to WealthMainPage
    And I can check changed Goal Name of Set My Target
    Examples:
      | goalName                    |
      | BuildMyinvestingHaitCCCCCCC |

  @WELATCOE-492
  Scenario Outline: Edit Goal Target and Horizon of Set My Target
    When I click Add New Goals
    And I click set my target and set <targetValue> and <time>
    And I click monthly investment Edit Button and set <newtargetValue> and <newtime>
    And I click the discover
    Examples:
      | targetValue | time | newtargetValue | newtime |
      | 300000      | 45   | 5000000        | 50      |

  @WELATCOE-494
  Scenario Outline: Edit draft goal of Set My Target Goal Draft
    When I go to Wealth Page
    And I click Goal of Set My Target Goal Draft
    And I click edit Button At Set My Target Page and set <targetValue>, <time> and <monthlyValue>
    And I back to wealth main page from edit Page
    Examples:
      | targetValue | time | monthlyValue |
      | 3000000     | 50   | 2600         |

  @WELATCOE-494
  Scenario Outline: Edit draft goal of Achieve Financial Draft
    When I go to Wealth Page
    And I click Goal of Achieve Financial Draft
    And I click edit Button At Freedom Goal Page and set <age>, <monthlyExpenses> and <otherExpenses>
    And I back to wealth main page from edit Page
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000            | Budget,Modest,Deluxe |

  @WELATCOE-494
  Scenario Outline: Edit draft goal of Build My investing Habit Draft
    When I go to Wealth Page
    And I click Goal of Build My investing Habit Draft
    And I click edit Button At Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I back to wealth main page from edit Page
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |

  @WELATCOE-632
  Scenario: Edit but keep existing one of Build My investing Habit
    When I go to Wealth Page
    And I click Goal of Build My investing Habit
    And I click edit button at Goal Page
    And I click View update recommendation
    And I slide to right and click Keep existing one
    And I click back to home button

  @WELATCOE-634
  Scenario Outline: Edit but keep existing one of Achieve Financial
    When I go to Wealth Page
    And I click Goal of Achieve Financial
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I slide to right and click Keep existing one
    And I click back to home button
    Examples:
      | monthlyValue |
      | 142000       |

  @WELATCOE-636
  Scenario Outline: Edit but keep existing one of My Target Goal
    When I go to Wealth Page
    And I click Goal of Set My Target Goal
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I slide to right and click Keep existing one
    And I click back to home button
    Examples:
      | monthlyValue |
      | 50000        |

  @WELATCOE-609
  Scenario Outline: back when outside feasible range of My Target Goal
    When I go to Wealth Page
    And I click Goal of Set My Target Goal
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click back to home button
    Examples:
      | monthlyValue |
      | 50000        |

  @WELATCOE-611
  Scenario Outline: back when outside feasible range of Achieve Financial
    When I go to Wealth Page
    And I click Goal of Achieve Financial
    And I click edit Button and set <monthlyValue> monthly investment plan
    And I click View update recommendation
    And I click back to home button
    Examples:
      | monthlyValue |
      | 20000        |

  @WELATCOE-666
  Scenario: No change in monthly investment and horizon of Build My Investing Habit On track
    When I go to Wealth Page
    And I click Build My Investing Habit On track Button
    And I click edit Button
    And I click View recommendation
    And I click YES button

  @WELATCOE-727
  Scenario: Set my Target for goal setting - exceed max amount - target wealth
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      | 30000001          |              |              | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-727
  Scenario: Set my Target for goal setting - exceed max amount - one time
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      |                   | 30000001     |              | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-727
  Scenario: Set my Target for goal setting - exceed max amount - monthly
#    When I go to Wealth Page
    When I click Add New Goals
    And I setup my target with exceeded value
      | targetWealthValue | oneTimeValue | monthlyValue | time |
      |                   |              | 3000001      | 60   |
    Then I see the exceed max amount error message

  @WELATCOE-731
  Scenario: Build My investing Habit - exceed max amount - one time
#    When I go to Wealth Page
    When I click Add New Goals
    And I Build My investing Habit with exceeded value
      | oneTimeValue | monthlyValue | month |
      | 30000001     |              | 60    |
    Then I see the exceed max amount error message

  @WELATCOE-731
  Scenario: Build My investing Habit - exceed max amount - monthly
#    When I go to Wealth Page
    When I click Add New Goals
    And I Build My investing Habit with exceeded value
      | oneTimeValue | monthlyValue | month |
      |              | 3000001      | 60    |
    Then I see the exceed max amount error message

  @WELATCOE-735
  Scenario: Achieve Financial Freedom - exceed max amount - one time
#    When I go to Wealth Page
    When I click Add New Goals
    And I Achieve Financial Freedom with exceeded value
      | age | monthlyExpenses | otherExpenses | oneTimeValue | monthlyValue |
      | 60  | 3000            | ,,            | 30000001     |              |
    Then I see the exceed max amount error message

  @WELATCOE-735
  Scenario: Achieve Financial Freedom - exceed max amount - monthly
#    When I go to Wealth Page
    When I click Add New Goals
    And I Achieve Financial Freedom with exceeded value
      | age | monthlyExpenses | otherExpenses | oneTimeValue | monthlyValue |
      | 60  | 3000            | ,,            |              | 3000001      |
    Then I see the exceed max amount error message

  @WELATCOE-674
  Scenario: Click Draft Delete and verify draft removed from goal list(Prepare three accounts:Set My target DraftAA, Build My Investing HabitBB, Achieve Financial FreedomCC)
    When I go to Wealth Page
    And I delete and verify Set My target Draft
    And I delete and verify Build My Investing Habit
    And I delete and verify Achieve Financial Freedom

  Scenario Outline: Set My Target goal  through reach my target picture, and finally exit with "Not now"
    And I click Goal of Build My Target Draft
    When I click edit Button
    And I click edit Set My Target and set <targetValue> and <time> <year>
    And I click the discover
    And I review and confirm order
    Examples:
      | targetValue | time | year |
      | 3000000     | 50   | year |

  Scenario Outline: Set My Target goal  through reach my target picture, and finally exit with "Not now"
    When I click Add New Goals
    And I click Set My Target and verification set <targetValue> and <time> <year>
    And I click the discover
    And I review and confirm order
    And I'm validating the confirmation page data
    Examples:
      | targetValue | time | year |
      | 3000000     | 50   | year |


  Scenario Outline: Keep using user's original invest horizon for calculating when create "Achieve Financial Freedom" goal
    When I click Add New Goals
    And I click Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I'm validating the confirmation page data
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  Scenario Outline: Keep using user's original invest horizon for calculating when edit "Achieve Financial Freedom" goal
    And I click Goal of Achieve Financial Draft
    When I click edit Button
    And I input Achieve Financial Freedom and set <age>, <monthlyExpenses> and <otherExpenses>
    And I click the discover
    And I review and confirm order
    And I'm validating the confirmation page data
    Examples:
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 500000          | Budget,Modest,Deluxe |

  Scenario Outline: Keep using user's original invest horizon for calculating when edit "Build My Investing Habit" goal
    And I click Goal of Build My investing Habit Draft
    And I click edit Button
    And I click Build Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I validate order confirmation page
    And I'm validating the confirmation page data
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 60    |
#
  Scenario Outline: Keep using user's original invest horizon for calculating  when create "Build My Investing Habit" goal
    When I click Add New Goals
    And I click Build My Investing Habit and set <oneTimeValue>, <monthlyValue> and <month>
    And I click the discover
    And I review and confirm order
    And I'm validating the confirmation page data
    Examples:
      | oneTimeValue | monthlyValue | month |
      | 30000        | 4000         | 360   |

  @WELATCOE-689
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Set My Target
    And I clicked my target goal with the status of on track
    And I click sell button
    And I click proceed button and set <targeValue>
    And I click next button
    And I verifying the order data information
    Examples:
      | targeValue |
      | 30         |

  @WELATCOE-693
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Achieve Financial Freedom
    When I go to Wealth Page
    And I clicked Achieve Financial Freedom goal with the status of on track
    And I click sell button
    And I click proceed button and set <targeValue>
    And I click next button
    Examples:
      | targeValue |
      | 30         |

  @WELATCOE-691
  Scenario Outline: Fund redemption_Show all Funds in Sell Order List_Build My investment Habit
    And I clicked Build My Investing Habit with the status of on track
    And I click sell button
    And I click proceed button and set <targeValue>
    And I click next button
    Examples:
      | targeValue |
      | 30         |