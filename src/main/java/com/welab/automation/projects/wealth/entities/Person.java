package com.welab.automation.projects.wealth.entities;

import lombok.Getter;
import lombok.Setter;

public class Person {
    @Getter
    @Setter
    private String chineseName;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private String birthday;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String phone;
}
