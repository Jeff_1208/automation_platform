package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.projects.wealth.pages.CommonPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingOrderPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderPlacementSteps {

  private GoalSettingOrderPage goalSettingOrderPage = new GoalSettingOrderPage();
  private CommonPage commonPage = new CommonPage();
  private static int oneTimeInvestmentValue;

  @And("I buy the fund without confirm")
  public void iBuyTheFundWithoutConfirm(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String currency = data.get(0).get("currency");
    String portfolioName = goalSettingOrderPage.getPortfolioName();
    // Buy
    String actualPortfolioName = goalSettingOrderPage.getJumpPagePortfolioName();
    assertThat(actualPortfolioName).startsWith(portfolioName);
    goalSettingOrderPage.selectCurrency(currency);
    goalSettingOrderPage.getNextButton().click();
  }

  @And("I buy the fund")
  public void iBuyTheFund(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String currency = data.get(0).get("currency");
    String portfolioName = goalSettingOrderPage.getPortfolioName();
    // Buy
    String actualPortfolioName = goalSettingOrderPage.getJumpPagePortfolioName();
    assertThat(actualPortfolioName).startsWith(portfolioName);
    goalSettingOrderPage.selectCurrency(currency);
    goalSettingOrderPage.reviewOrder();
  }

  @And("I quit the ordering process but go back")
  public void iQuitTheOrderingProcessButGoBack() {
    commonPage.clickXIcon();
    goalSettingOrderPage.getNotNow().click();
  }

  @Then("I still stay on review and confirm order page")
  public void iStillStayOnReviewAndConfirmOrderPage() {
    assertThat(goalSettingOrderPage.getReviewAndConfirmOrder().isDisplayed()).isEqualTo(true);
  }

  @And("I slide to confirm the order")
  public void iSlideToConfirmTheOrder() {
    goalSettingOrderPage.slideToConfirmOrder();
  }

  @Then("I can see the error message")
  public void iCanSeeTheErrorMessage() {
    String msg = goalSettingOrderPage.getErrorMsg();
    goalSettingOrderPage.clickElement(goalSettingOrderPage.getBack());
    assertThat(msg).as("Error: " + msg).isNotEmpty();
  }

  @Then("I can see the insufficient amount error message")
  public void iCanSeeTheSpecificErrorMessage() {
    String msg = goalSettingOrderPage.getErrorMsg();
    assertThat(msg).as("Error: " + msg).isEqualTo("There is insufficient amount in your account");
  }

  @Then("I can see the submit error message")
  public void iCanSeeTheSubmitErrorMessage() {
    String msg = goalSettingOrderPage.getErrorMsg();
    assertThat(msg).as("Error: " + msg).startsWith("Submit error");
    goalSettingOrderPage.getBack().click();
  }

  @Then("I can see the congratulation page")
  public void iCanSeeTheCongratulationPage() {
    goalSettingOrderPage.waitUntilElementVisible(goalSettingOrderPage.getCongratulation(), 30);
    goalSettingOrderPage.getReferenceNumber();
    goalSettingOrderPage.clickElement(goalSettingOrderPage.getDone());
    goalSettingOrderPage.waitUntilElementClickable(goalSettingOrderPage.getProcessing());
    goalSettingOrderPage.getHeaderBackBtn().click();
  }

  @And("I quit the ordering process")
  public void iQuitTheOrderingProcess() {
    commonPage.clickXIcon();
    goalSettingOrderPage.getQuit().click();
  }

  @And("I review the order")
  public void iReviewTheOrder() {
    goalSettingOrderPage.clickNext();
  }

  @And("I check Subscription Fee")
  public void iCheckSubscriptionFee() {
    Boolean txtStatus = goalSettingOrderPage.getSubscriptionFeeStatus();
    assertThat(txtStatus).isTrue();
  }

  @And("I check Starting From")
  public void iCheckStartingFrom() {
    Boolean txtStatus = goalSettingOrderPage.getStartingFrom();
    assertThat(txtStatus).isTrue();
  }

  @And("I click back to home button")
  public void iClickBackToHomeButton() {
    goalSettingOrderPage.getBack().click();
  }

  @Then("I click View update recommendation")
  public void iClickUpRecommendation() {
    goalSettingOrderPage.clickUpRecommen();
  }

  @Then("I slide to right and click Keep existing one")
  public void iClickKeepexistingone() {
    goalSettingOrderPage.slideToclickKeepone();
  }

  @And("^I click edit Button and set ([^\"]\\S*) monthly investment plan$")
  public void iClickEditButtonAndSetMonthly(String monthlyValue) {
    goalSettingOrderPage.clickEditButton();
    goalSettingOrderPage.updateMonlyInvest(monthlyValue);
  }

  @And("^I click the order of goal ([^\"]\\S*) with the status of on track or off track$")
  public void iClickOrder(String targeName) {
    if (targeName.contains("Reach")) {
      goalSettingOrderPage.clickReachGoal(targeName);
      assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
    } else if (targeName.contains("Build")) {
      goalSettingOrderPage.clickBuild(targeName);
      assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
    } else if (targeName.contains("Achieve")) {
      goalSettingOrderPage.clickAchieveGoal(targeName);
      assertThat(goalSettingOrderPage.getEditGoalNameText().getText()).isEqualTo(targeName);
    }
  }

  @And("^I click edit Button and update  monthly investment plan ([^\"]\\S*)$")
  public void iClickEditButtonAndSetTargeValue(String targeValue) {
    goalSettingOrderPage.clickEditButton();
    goalSettingOrderPage.updateMonlyInvest(targeValue);
  }

  @And(
      "^I click Build My Investment Habit edit Button and update  monthly investment plan ([^\"]\\S*)$")
  public void iclickBuildButtonAndSetTime(String time) throws Exception {
    goalSettingOrderPage.clickEditButton();
    goalSettingOrderPage.setMyInvestingHabitGoal(time);
  }

  @Then("I click View updated recommendation")
  public void iClickUpRecommendationNew() {
    goalSettingOrderPage.clickUpRecommen();
  }

  @And("I click View New Portfolio and buy it")
  public void iClickViewNewPortfolio() {
    goalSettingOrderPage.clickViewNewPortlioAndBuy();
  }

  @And("I click View Portfolio")
  public void iClickViewPortfolio() {
    goalSettingOrderPage.clickViewPortlio();
  }

  @And("I click buy button")
  public void iClickBuyBtn() {
    goalSettingOrderPage.clickBuyBtn();
  }

  @And("I review and confirm order util look next button")
  public void iReviewAndConfirmOrder() {
    goalSettingOrderPage.reviewNewOrder();
  }

  @And("I click keep existing one and confirm it")
  public void iKeepExistingOneAndConfirm() {
    goalSettingOrderPage.clickKeepExistingOneAndConfirm();
  }

  @And("I click confirm button and slide to confirm order")
  public void iClickConfirmAndSlideToConfirm() {
    goalSettingOrderPage.clickConfirmAndSlideToConfirm();
    goalSettingOrderPage.clickElement(goalSettingOrderPage.getDone());
  }

  @And("I click edit Button")
  public void iclickEditButton() {
    goalSettingOrderPage.clickEditButton();
  }

  @And("^I review my goal detail page ([^\"]\\S*)$")
  public void iReviewGoalDetail(String targeName) {
    goalSettingOrderPage.waitAndReviewGoalName(targeName);
  }

  @And("I click OK button")
  public void iClickOK() {
    goalSettingOrderPage.clickOKButton();
  }

  @And("I click YES button")
  public void iClickYES() {
    goalSettingOrderPage.clickYESButton();
  }

  @And("I click Sell button")
  public void iClickSell() {
    goalSettingOrderPage.clickSellButton();
  }

  @And("I edit Sell Value ([^\"]\\S*)$")
  public void ieditSellValue(String SellValue) {
    goalSettingOrderPage.clearSellInputEdit();
    String name = goalSettingOrderPage.getPortfolioNameText().getText();
    String balance = goalSettingOrderPage.getAvailableBalance().getText();
    goalSettingOrderPage.editSellValue(SellValue);
    assertThat(name).isEqualTo(goalSettingOrderPage.getPortfolioNameText().getText());
    assertThat(balance).isEqualTo(goalSettingOrderPage.getAvailableBalance().getText());
    goalSettingOrderPage.clickSellNextBtn();
  }

  @And("I Slide to confirm Sell And Click Done")
  public void iSlideToConfirmSell() {
    goalSettingOrderPage.SlideToConfirmSellOrder();
    goalSettingOrderPage.clickElement(goalSettingOrderPage.getDone());
  }

  @And("^I click the order all of goal with the status of ([^\"]\\S*)$")
  public void iclickProcessingOrder(String orderStatus) {
    switch (orderStatus) {
      case "Processing":
        goalSettingOrderPage.clickProcessingOrder();
        assertThat(goalSettingOrderPage.getProcessing().getText()).isEqualTo("Processing");
        break;
      case "Ontrack":
        goalSettingOrderPage.clickOnTrackOrder();
        break;
      case "Offtrack":
        goalSettingOrderPage.clickOffTrackOrder();
        break;
      case "Achieved":
        goalSettingOrderPage.clickAchievedOrder();
        break;
      case "Completed":
        goalSettingOrderPage.clickCompletedOrder();
        break;
    }
  }

  @Then("I click order status icon")
  public void iclickOrderStatus() {
    goalSettingOrderPage.clickOrderStatus();
    assertThat(goalSettingOrderPage.getOrderStatusPage().getText()).isEqualTo("Order status");
  }

  @And("Verify orders by dates from newest to oldest")
  public void verifyTimeNewestToOldest() {
    boolean isTimeOrderTure = goalSettingOrderPage.verifyGoalTime();
    assertThat(isTimeOrderTure).isTrue();
  }

  @And(
      "Verify only support model portfolio transactions,showing three status,only shows 1 reference number, 1 balance")
  public void iVerifyMoPoStatusReBa() {
    boolean orderOtherStrTure = goalSettingOrderPage.verifyMoPoStatus();
    assertThat(orderOtherStrTure).isTrue();
  }

  @And("I click any fund to verify fund documents")
  public void iClickFundAndVerifyDocuments() {
    goalSettingOrderPage.clickFundAndVerifyDocuments();
    assertThat(goalSettingOrderPage.getDownloadSuccess().getText()).isEqualTo("Download Completed");
  }

  @And("I click standing instruction tab and verify monthly subscription instructions")
  public void iClickStandInstruction() {
    goalSettingOrderPage.clickStandInstruction();
    assertThat(goalSettingOrderPage.getEachMonthInstruction().getText())
        .isEqualTo("Transaction on every  of each month");
  }

  @Then("I click Model portfolio monthly subscription instructions details")
  public void iClickInstructionsDetails() {
    goalSettingOrderPage.clickInstructionsDetails();
  }

  @And("I back goal edit page")
  public void iClickBackBtn() {
    goalSettingOrderPage.clickBackBtn();
  }

  @And("I click standing instruction tab and verify  instructions be cancelled")
  public void iclickDoneBtnAndVerifyCancel() {
    // goalSettingOrderPage.clickDoneBtn();
    goalSettingOrderPage.clickOrderStatus();
    assertThat(goalSettingOrderPage.clickStandInstruction()).isTrue();
  }

  @And("^I click goal name with ([^\"]\\S*)$")
  public void iClickGoalNameWith(String goalname) {
    goalSettingOrderPage.clickGoalNameWith(goalname);
  }

  @And("I click Sell All Button")
  public void iclickSellAllButton() {
    String name = goalSettingOrderPage.getPortfolioNameText().getText();
    String balance = goalSettingOrderPage.getAvailableBalance().getText();
    goalSettingOrderPage.clickSellAllnConfirm();
    assertThat(name).isEqualTo(goalSettingOrderPage.getPortfolioNameText().getText());
    assertThat(balance).isEqualTo(goalSettingOrderPage.getAvailableBalance().getText());
    goalSettingOrderPage.clickSellNextBtn();
  }

  @And("I click Sell All Button only")
  public void iclickSellAllButtonOnly() {
    goalSettingOrderPage.clickSellAllnConfirm();
    goalSettingOrderPage.clickSellNextBtn();
  }

  @And("I click Next Button")
  public void iclickNextButton() {
    goalSettingOrderPage.clickSellNextBtn();
  }

  @When("^I enter balance ([^\"]\\S*)$")
  public void iEnterBalance(String bValue) {
    goalSettingOrderPage.enterBalance(bValue);
  }

  @Then("I click  the button to re-edit the target")
  public void iClickReeditButon() {
    goalSettingOrderPage.clickAgainGoal();
  }

  @And("^I pass interface get current age ([^\"]\\S*) and ([^\"]\\S*)$")
  public void getCurrentAge(String user, String password) throws Exception {
    goalSettingOrderPage.judgeCurrentAge(user, password);
  }

  @When("^I verify Achieve Financial Freedom and set new ([^\"]\\S*), ([^\"]\\S*) and ([^\"]\\S*)$")
  public void verifyFinancialFreedomGoal(String age, String monthlyExpenses, String otherExpenses)
      throws Exception {
    goalSettingOrderPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingOrderPage.getFFTAgeSliderText()).isEqualTo(age);
    assertThat(goalSettingOrderPage.judgeCurAgeLessThanReAge(age)).isTrue();
    goalSettingOrderPage.clickContinue();
    goalSettingOrderPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    String totalMonthly = goalSettingOrderPage.getTotalMonthly();
    goalSettingOrderPage.calculateTargetWealth();
    assertThat(goalSettingOrderPage.getRetiredAgeLink()).isEqualTo("Retire at " + age);
    String editPageTotalMonthly = goalSettingOrderPage.getEditPageTotalMonthly();
    assertThat(totalMonthly).isEqualTo(editPageTotalMonthly);
    goalSettingOrderPage.getCurrentWealth();
    assertThat(oneTimeInvestmentValue).isEqualTo(goalSettingOrderPage.getCurrentWealth());
  }

  @And("I get one time investment value")
  public int getOneTimeInvestmentValue() {
    oneTimeInvestmentValue = goalSettingOrderPage.oneTimeInvestmentValue();
    return oneTimeInvestmentValue;
  }

  @And("I  retieve monthly investment value")
  public void retieveFeasible() {
    goalSettingOrderPage.retieveFeasibleMonthlyValue();
  }

  @And("I click SellBtn and cancel button")
  public void iClickSellBtn() {
    goalSettingOrderPage.clickSellButton();
    commonPage.clickCancelBtn();
  }

  @And("I click Sell All Button and cancel button")
  public void clickSellAllBtnAndCancenBtn() {
    goalSettingOrderPage.clickSellAllButton();
    commonPage.clickCancelBtn();
  }

  @And("I click SellBtn and Proceed button")
  public void clickSellBtnAndProceedBtn() {
    goalSettingOrderPage.clickSellButton();
    goalSettingOrderPage.clickProceedButton();
  }

  @Then("I can see Sell button will be disabled and goal is Processing")
  public void icanseeSellbutonwillbedisabled() {
    Boolean SellBtn = goalSettingOrderPage.SellBtnUnClickable();
    assertThat(SellBtn).isFalse();
    Boolean ProcessingBtn = goalSettingOrderPage.verifyGoalIsProcessing();
    assertThat(ProcessingBtn).isTrue();
  }

  @And("I only can see Target wealth")
  public void onlySeeTargetWealth() {
    Boolean probability = goalSettingOrderPage.verifyExistProbability();
    assertThat(probability).isFalse();
    Boolean targetWealth = goalSettingOrderPage.verifyExistTargetWealth();
    assertThat(targetWealth).isTrue();
    Boolean goalHorizon = goalSettingOrderPage.verifyExistGoalHorizon();
    assertThat(goalHorizon).isFalse();
  }

  @And("I click Proceed button")
  public void iClickProceed() {
    goalSettingOrderPage.clickProceedButton();
  }

  @And("I edit Sell Value to over range")
  public void ieditSellValuetoOverRange() {
    goalSettingOrderPage.editSellValueToOverRange();
    goalSettingOrderPage.verifyNextBtnUnClickable();
  }

  @And("I click edit Button and change monthlyInvestment")
  public void iClickEditBtnAndChangeMonthlyValue(){
    goalSettingOrderPage.clickEditButton();
    goalSettingOrderPage.iChangeMonthlyInvestment();
  }
}
