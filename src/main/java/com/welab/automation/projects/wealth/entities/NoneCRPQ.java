package com.welab.automation.projects.wealth.entities;

import lombok.Getter;
import lombok.Setter;

public class NoneCRPQ {

  @Getter @Setter private String declarationemployeeofintermediary;

  @Getter @Setter private String qualificationinformation;

  @Getter @Setter private String investmenthorizon;

  @Getter @Setter private String investmentproductknowledgeandexperience;

  @Getter @Setter private String networth;
}
