package com.welab.automation.projects.wealth.WelabAPITest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.javafaker.Faker;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.JsonUtil;
import com.welab.automation.framework.utils.entity.api.HttpUtils;
import com.welab.automation.framework.utils.entity.api.TestStep;
import com.welab.automation.framework.utils.enums.HttpType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Map;

public class AccountCreator {
  private static final Logger logger = LoggerFactory.getLogger(AccountCreator.class);

  private String createAccountHost = "https://api-sit.dev-wlab.net/onboarding-mob";
  private String createAccountPath = "/v1/dummy-accounts/all-create";

  /** To call the API to generate an account. */
  public JSONObject create() {
    TestStep testStep = new TestStep();
    testStep.setType(HttpType.POST);
    testStep.setPath(createAccountPath);

    GlobalVar.HEADERS.put("Accept-Encoding", "gzip, deflate, br");
    GlobalVar.HEADERS.put("Content-Type", "application/json;charset=utf-8");
    String objBody = null;
    JSONObject account = new JSONObject();
    try {
      String jsonFilePath =
          "src/main/resources/api/testData/input/Welab/accountGeneration/create_acc_template.json";
      JSONObject jsonObject = JsonUtil.extractDataJson(jsonFilePath);
      formatBody(jsonObject);
      JSONObject milestoneRequest = jsonObject.getJSONObject("milestoneRequest");
      account.put("username", milestoneRequest.getString("username"));
      account.put("password", milestoneRequest.getString("password"));
      account.put("useStatus", "false");
      objBody = jsonObject.toJSONString();
      logger.info(
          "creating {}/{}",
          milestoneRequest.getString("username"),
          milestoneRequest.getString("password"));
    } catch (Exception e) {
      logger.error("failed to post questionnaires with error {}", e.toString());
    }

    testStep.setBody(objBody);

    logger.info("headers: {}", testStep.getHeaders());

    HttpUtils httpUtils = new HttpUtils(createAccountHost);
    Response response = httpUtils.request(testStep);
    logger.info("welab response: {}", response.getBody().asString());
    return account;
  }

  private JSONObject formatBody(JSONObject bodyTemplate) {
    Faker faker = new Faker();
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    String firstName = faker.name().firstName() + faker.number().numberBetween(0, 10000000);
    String emailAddress = firstName + "@welab.co";
    String mobileNo = String.valueOf(faker.number().numberBetween(100000000000L, 199999999999L));
    String dateOfBirth = dateFormatter.format(faker.date().birthday());
    String hkidCardNumber = "TA" + mobileNo + "(" + faker.number().digit() + ")";
    String cardHolderName = firstName + "," + faker.name().lastName();

    updateJson(bodyTemplate, "email", emailAddress);
    updateJson(bodyTemplate, "mobile", mobileNo);
    updateJson(bodyTemplate, "username", firstName);
    updateJson(bodyTemplate, "dateofBirth", dateOfBirth);
    updateJson(bodyTemplate, "cardHolderName", cardHolderName);
    updateJson(bodyTemplate, "hkidCardNumber", hkidCardNumber);
    updateJson(bodyTemplate, "firstRegistrationDate", dateOfBirth);
    updateJson(bodyTemplate, "taxIdentificationNumber", hkidCardNumber);
    return bodyTemplate;
  }

  /**
   * replace all keys with newValue
   *
   * @param jsonObject The Json object to manipulate with.
   * @param key target key
   * @param newValue new value
   */
  private static void updateJson(JSONObject jsonObject, String key, String newValue) {
    for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
      Object element = entry.getValue();
      if (element instanceof JSONArray) {
        parseJsonArray((JSONArray) element, key, newValue);
      } else if (element instanceof JSONObject) {
        updateJson((JSONObject) element, key, newValue);
      } else {
        if (key.equals(entry.getKey())) {
          jsonObject.put(entry.getKey(), newValue);
        }
      }
    }
  }

  private static void parseJsonArray(JSONArray asJsonArray, String key, String newValue) {
    for (Object element : asJsonArray) {
      if (element instanceof JSONArray) {
        parseJsonArray((JSONArray) element, key, newValue);
      } else if (element instanceof JSONObject) {
        updateJson((JSONObject) element, key, newValue);
      }
    }
  }
}
