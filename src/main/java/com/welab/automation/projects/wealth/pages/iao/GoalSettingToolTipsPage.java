package com.welab.automation.projects.wealth.pages.iao;

import com.csvreader.CsvReader;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.*;

import static com.welab.automation.framework.utils.Utils.logFail;

public class GoalSettingToolTipsPage extends AppiumBasePage {
  private final String pageName = "Goal Setting Tool Tips Page";

  // expected tips text are stored in this file
  private final String tipsFile = "goal_setting_tips.csv";
  private final Logger logger = LoggerFactory.getLogger(GoalSettingToolTipsPage.class);

  // pages contains tool tips
  public enum PagesHaveTips {
    // there is relationship between csv "pages" column and UI pages.
    SetMyTarget("SetMyTarget"), // GB1_01_Goal1_Setting1
    Recommendation("Recommendation"), // GC1_01_Recommendation
    PortfolioOverView("PortfolioOverView"), // GD_01_Overview,
    Portfolio("Portfolio"), // GD_03_Portfolio
    FeaturedFund("FeaturedFund"), // FE_01c_Fund1
    BuildInvestHabit("BuildInvestHabit"), // GB3_02_Goal4_Setting2
    FinancialFreedom("FinancialFreedom"); // GB2_01_Goal2_Setting1

    @Getter @Setter private String pageDesc;

    PagesHaveTips(String pageDesc) {
      this.pageDesc = pageDesc;
    }
  }

  @Getter @Setter private static PagesHaveTips subPage;

  private HashMap<String, HashMap<String, ArrayList<String>>> TipsFromCsv;
  // Relationship of csv tips and UI elements
  private Map<String, MobileElement> tipIconElement;

  /* ========================   tips icons elements start ============================= */
  // GB1_01_Goal1_Setting1, tips of When do you want to achieve your goal? (age)
  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[2]/android.widget.TextView[@text=\\\"Reach My Target\\\"]/../android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(xpath = "//*[@name='When do you want to achieve your goal? (age)']/following-sibling::XCUIElementTypeOther[1]")
  private static MobileElement GoalDetailQuestionTipsEle;

  // GC1_01_Recommendation, tips of Portfolio Recommendation
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Portfolio Recommendation']/following-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(xpath = "//*[@name='Portfolio Recommendation']/following-sibling::XCUIElementTypeOther[1]")
  private static MobileElement PortfolioRecommendTipsEle;

  // GC1_01_Recommendation to GC_03_planRemark
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Investment Plan']/following-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Investment Plan']/following-sibling::XCUIElementTypeOther[1]")
  private static MobileElement RecommendPortfolioInfoTipsEle;

  // GD_01_Overview to GD_02a_PortfolioRemark
  // todo how to locate this element?  previous text "Conservative IV 6" and next text "(USD)" may
  // be dynamic
  @AndroidFindBy(
      xpath =
          "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(
      xpath = "//*[@name='(USD)']/preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther")
  private static MobileElement PortfolioDetailTipsEle;

  // GD_01_Overview to GD_02_OverviewRemark
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Expected wealth']/following-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(
      xpath = "//*[starts-with(@name,'Expected wealth')]/following-sibling::XCUIElementTypeOther[1]")
  private static MobileElement PortfolioFigureRemarkTipsEle;

  // GD_03b_Portfolio to GD_03c_popUp
  // 2021.11.15 Content is coming soon
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@text, 'Risk Rating')]/following-sibling::android.view.ViewGroup[1]")
  @iOSXCUITFindBy(
      xpath = "//*[starts-with(@name,'Risk Rating')]/XCUIElementTypeOther")
  private static MobileElement PortfolioTips1Ele;

  // FE_01c_Fund1 to FE_03a_popUp
  @AndroidFindBy(accessibility = "FEATURED_FUND-btn-tipForRiskRating-1")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'FEATURED_FUND-btn-tipForRiskRating-1'")
  private static MobileElement FeaturedFundTipsEle;

  // FE_01c_Fund1 to FE_03b_MorningStar
  @AndroidFindBy(accessibility = "FEATURED_FUND-btn-tipForMorningStarRating")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'FEATURED_FUND-btn-tipForMorningStarRating'")
  private static MobileElement FeaturedFundMorningStarTipsEle;

  // GB3_02_Goal4_Setting2
  //  static String HabitTips1 = "How long do you want to be investing for?";

  // GB2_01_Goal2_Setting1 tip of "When do you want to retire? (age)"
  //  @AndroidFindBy(
  //      xpath =
  //          "//android.widget.TextView[contains(@text, 'When do you want to
  // retire')]/following-sibling::android.view.ViewGroup[2]")
  @AndroidFindBy(
      xpath = "\\t//android.view.ViewGroup[@content-desc=\\\"questionIcon\\\"]/android.view.ViewGroup/android.view.ViewGroup\\n")
  @iOSXCUITFindBy(xpath = "//*[@name='When do you want to retire? (age)']/XCUIElementTypeOther")
  private static MobileElement achieveFinancialFreedomTipsEle;

  // GB2_03_Goal2_Setting3 to GB2_04_popUp
  // 2021.11.29, 'Total monthly expenses post retirement' has content-desc "questionIcon"
  // Content is coming soon
  @AndroidFindBy(accessibility = "questionIcon")
  private static MobileElement questionIcon;

  /* ========================   tips icons elements end ============================= */

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Tips']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Tips'")
  private static MobileElement toolTips;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Tips']/../android.view.ViewGroup")
  @iOSXCUITFindBy(
          xpath =
                  "//XCUIElementTypeStaticText[@name='Tips']/following-sibling::XCUIElementTypeOther[1]")
  private static MobileElement closeToolTipsBtn;

  // 2021.11.18, version 180, this tip not have 'Tool tips' title, so use absolute xpath
  @AndroidFindBy(
      xpath =
          "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(xpath = "//*[@name='Expected Wealth']/../../../../../preceding-sibling::XCUIElementTypeOther[1]/XCUIElementTypeOther")
  private MobileElement PortfolioOverViewTips2CloseBtn;

  // 2021.11.18, version 180, this tip title is "Fund Weight Tip", so use absolute xpath
  @AndroidFindBy(
      xpath =
          "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(xpath = "//*[@name='Tips']/XCUIElementTypeOther")
  private MobileElement PortfolioTips1CloseBtn;

  // multiple text views on tip tools page
  @AndroidFindBy(
      xpath = "//android.widget.TextView[@text = 'Tips']/..//android.widget.TextView")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Tips']/..//XCUIElementTypeStaticText")
  private List<MobileElement> ToolTipsTexts;


  @AndroidFindBy(
          xpath = "//android.widget.TextView[@text = \"Wealth projection\"]\n")
  private MobileElement wealthProjection;



  public GoalSettingToolTipsPage() {
    super.pageName = pageName;
  }

  private void setTipIconElement() {
    // key is same as "tips" column in csv file.
    tipIconElement = new HashMap<>();
    tipIconElement.put("When do you want to achieve your goal? (age)", GoalDetailQuestionTipsEle);
    tipIconElement.put("Portfolio Recommendation", PortfolioRecommendTipsEle);
    tipIconElement.put("Investment Plan", RecommendPortfolioInfoTipsEle);
    tipIconElement.put("PortfolioRemark", PortfolioDetailTipsEle);
    tipIconElement.put("Expected wealth", PortfolioFigureRemarkTipsEle);
    tipIconElement.put("Risk Rating", PortfolioTips1Ele);
    tipIconElement.put("Risk rate", FeaturedFundTipsEle);
    tipIconElement.put("MorningStar rating", FeaturedFundMorningStarTipsEle);
    tipIconElement.put("Target wealth", achieveFinancialFreedomTipsEle);
  }

  /**
   * the structure is like: { page1: { tip1: [text1, text2, ...]}, tip2: [text1, text2, ...]},
   * page2: { tip1: [text1, text2, ...]}, tip2: [text1, text2, ...]}
   *
   * @throws IOException
   */
  public void parseCsvFile() throws IOException {

    TipsFromCsv = new HashMap<>();

    String filePath = Paths.get(GlobalVar.APP_TEST_DATA_PATH, tipsFile).toRealPath().toString();
    CsvReader reader = new CsvReader(filePath, ',', StandardCharsets.UTF_8);
    reader.readHeaders();

    while (reader.readRecord()) {
      String[] row = reader.getValues();
      System.out.println(Arrays.toString(row));
      if (TipsFromCsv.containsKey(row[0])) {
        if (TipsFromCsv.get(row[0]).containsKey(row[1])) {
          //          HashMap<String, String> text = ;
          TipsFromCsv.get(row[0]).get(row[1]).add(row[2]); // add text, row[2]
        } else {
          ArrayList<String> TextsInOneTip = new ArrayList<>();
          TextsInOneTip.add(row[2]); // row[2] is English text.
          TipsFromCsv.get(row[0]).put(row[1], TextsInOneTip); // add tip
        }
      } else {
        HashMap<String, ArrayList<String>> tipsInOnePage = new HashMap<>();
        ArrayList<String> TextsInOneTip = new ArrayList<>();
        TextsInOneTip.add(row[2]); // row[2] is English text.
        tipsInOnePage.put(row[1], TextsInOneTip);
        TipsFromCsv.put(row[0], tipsInOnePage); // add page
      }
    }

    System.out.println(TipsFromCsv);
  }

  public void closeToolTips(MobileElement... closeBtn) {
    if (closeBtn.length <= 0) {
      waitUntilElementClickable(closeToolTipsBtn);
      clickElement(closeToolTipsBtn);
    } else {
      waitUntilElementClickable(closeBtn[0]);
      clickElement(closeBtn[0]);
    }
  }

  // tips
  @SneakyThrows
  public Map<String, String> checkTips() {
    PagesHaveTips currPage = getSubPage();
    if (TipsFromCsv == null) {
      parseCsvFile();
    }
    if (tipIconElement == null) {
      setTipIconElement();
    }

    return checkTipsOnPage(currPage);
  }

  // for demo
  private void takeTimeToViewTips() {
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void clickTipsIcon(MobileElement tipsIconEle) {
    // click tip icon
    waitUntilElementClickable(tipsIconEle);
    clickElement(tipsIconEle);
    // takeTimeToViewTips();
  }

  private Map<String, String> checkTipsOnPage(PagesHaveTips subPage) {
    Map<String, String> resultMap = new HashMap<>();
    boolean result = true;
    resultMap.put("status", "true");
    resultMap.put("message", "");
    String pageDesc = subPage.getPageDesc();
    logger.info("checkTipsOnPage {}, {}", subPage, subPage.getPageDesc());

    if (!TipsFromCsv.containsKey(pageDesc)) {
      logFail("failed to get page tips info from csv, page: " + pageDesc, pageName);
      resultMap.put("status", "false");
      resultMap.put("message", "failed to get page tips info from csv, page: " + pageDesc);
      return resultMap;
    }

    // get tips in page
    for (Map.Entry<String, ArrayList<String>> eachTipInThisPage :
        TipsFromCsv.get(pageDesc).entrySet()) {
      String tipsDesc = eachTipInThisPage.getKey();
      ArrayList<String> expTipsTexts = eachTipInThisPage.getValue();

      if (tipsDesc.equals("Expected wealth")) {
        clickElement(wealthProjection);
        scrollUp();
      }

      MobileElement tipsIconEle = tipIconElement.get(tipsDesc);

      if (tipsIconEle != null) {
        clickTipsIcon(tipsIconEle);
      } else {
        logFail("failed to get tips icon element of " + tipsDesc, pageName);
        result = false;
        resultMap.put("status", "false");
        resultMap.put("message", "failed to get tips icon element of " + tipsDesc);
        continue; // check next tip tools in this page
      }

      // check whether the first text view is title "tool tips".
      if (isElementDisplayed(toolTips, 5) == null) {
        // if "tool Tips" is not show in tips title, considered to be a bug.
        logFail("not contain title 'tip tools' in tip " + tipsDesc, pageName);
        closeToolTipsByPage(tipsIconEle);
        result = false;
        resultMap.put("status", "false");
        resultMap.put("message", "not contain title 'tip tools' in tip " + tipsDesc);
        continue; // check next tip tools in this page
      }
      for (int i = 0; i < expTipsTexts.size(); i++) {
        // the first text view is title "tool tips", not check here.
        if (i < ToolTipsTexts.size() - 1) {
          String actualText = getElementText(ToolTipsTexts.get(i + 1));
          if (!actualText.replaceAll("\\p{C}", "").trim().equals(expTipsTexts.get(i).replaceAll("\\p{C}", "").trim())) {
            String errMsg =
                String.format(
                    "tip: %s\nexpected text: %s\n,diffs are %s\n",
                    eachTipInThisPage.getKey(),
                    expTipsTexts.get(i),
                    StringUtils.difference(expTipsTexts.get(i), actualText));
            logFail(errMsg, pageName);
            result = false;
            resultMap.put("status", "false");
            resultMap.put("message", errMsg);
          }
        } else {
          // todo, scroll to make the textview visible.
          logger.info("will check text {} of page {} later", expTipsTexts.get(i), pageDesc);
        }
      }

      // check tip texts, one text is related to a textview
      //      for (String expectedText : eachTipInThisPage.getValue()) {
      // expected text
      //        String xpathExp = String.format("//android.widget.TextView[@text = '%s']",
      // expectedText);
      //
      //        MobileElement textEle = scrollUpToFindElement(By.xpath(xpathExp), 3, 5);
      //        if (textEle == null) {
      //          logFail(
      //              String.format("tips %s doesn't have content %s", tipsDesc, expectedText),
      // pageName);
      //          result = false;
      //        } else {
      //          System.out.printf("tips %s have content %s", tipsDesc, expectedText);
      //        }
      //      }

      // close tool tips after checking a tool tip
      // some tool tips close button is located by specific xpath
      closeToolTipsByPage(tipsIconEle);
    }

    return resultMap;
  }

  /**
   * close tool tips after checking a tool tip, some tool tips close button is located by specific
   * xpath
   *
   * @param tipsIconEle
   */
  private void closeToolTipsByPage(MobileElement tipsIconEle) {
    if (tipsIconEle == PortfolioFigureRemarkTipsEle) {
      closeToolTips(PortfolioOverViewTips2CloseBtn);
    } else if (tipsIconEle == PortfolioTips1Ele) {
      closeToolTips(PortfolioTips1CloseBtn);
    } else {
      closeToolTips();
    }
  }
}
