package com.welab.automation.projects.wealth.pages;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.picocontainer.annotations.Inject;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import static com.welab.automation.framework.utils.Utils.logFail;

public class CommonPage extends AppiumBasePage {

  private String pageName = "Common Page";
  private String upgradeText = "Got it";
  private String cancelText = "Cancel";
  private int customWait = 50;
  private int toastWait = 5;
  private int waitMillSeconds = 3000;
  private String defaultInvestmentHorizon;
  private String defaultKnowledgeAndExperience;

  @Inject GoalSettingPage goalSettingPage;

  public static final String skipRootString =
      "//android.widget.TextView[contains(@text,'Jailbreak')]/../android.widget.Switch";

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']/../android.widget.TextView[1]")
  private MobileElement devPanelBackBtn;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Login'")
  private MobileElement loginTxt;

  @AndroidFindBy(xpath = "//*[@text='Dev Panel']")
  private MobileElement devPanelTitle;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Got it']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Got it'")
  private MobileElement upgradeOKBtn;

  @iOSXCUITFindBy(accessibility = "-btn-title")
  @AndroidFindBy(accessibility = "-btn")
  private MobileElement gotItBtn;

  @iOSXCUITFindBy(accessibility = "Select one you want to explore now!")
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Select one you want to explore now!']")
  private MobileElement exploreText;

  @iOSXCUITFindBy(accessibility = "btnGoalCard")
  @AndroidFindBy(accessibility = "btnGoalCard")
  private MobileElement goalCardBtn;

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
  private MobileElement eyeBtn;

  @AndroidFindBy(
      xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]")
  private MobileElement rocketButton;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text,'Root')]/../android.widget.Switch")
  private MobileElement skipRoot;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cancel']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Cancel'")
  private MobileElement contactWarningCancel;

  @Getter
  @AndroidFindBy(accessibility = "GoFlexi, tab, 4 of 5")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'GoFlexi, tab, 4 of 5'")
  private MobileElement loanTab;

  @Getter
  @AndroidFindBy(accessibility = "Wealth, tab, 5 of 5")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Wealth, tab, 5 of 5'")
  private MobileElement wealthTab;

  @AndroidFindBy(
      xpath = "//android.widget.TextView[contains(@text, 'Hi,')]/../../android.view.ViewGroup[3]")
  @iOSXCUITFindBy(
      xpath =
          "//XCUIElementTypeStaticText[starts-with(@label, 'Hi,')]/../../../../XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
  private MobileElement MyAccountBtn;

  @AndroidFindBy(xpath = "//*[@text='Learn more']")
  @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Learn more']")
  private MobileElement learnMoreBtn;

  @AndroidFindBy(xpath = "//*[@text='Skip to open an account']")
  @iOSXCUITFindBy(accessibility = "skipTipText")
  private MobileElement skipToOpenAccountBtn;

  @Getter
  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-title")
  private MobileElement welcomeMutFundTitle;

  @AndroidFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  @iOSXCUITFindBy(accessibility = "WELCOME_TO_MUTUAL_FUND-btn")
  private MobileElement welcomeMutFundBtn;

  @AndroidFindBy(xpath = "//*[@text='Update and verify personal info']")
  @iOSXCUITFindBy(accessibility = "PROCESS_GUIDE-title")
  private MobileElement updateVerifyPersonalInfoTip;

  @AndroidFindBy(xpath = "//*[@text='Next']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement nextBtn;

  // used for exception showing up.
  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement exceptionCancelBtn;

  @AndroidFindBy(xpath = "//*[@text='Open an account']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'openAccountBtn-title'")
  private MobileElement openAccountBtn;

  @Getter
  @AndroidFindAll({@AndroidBy(xpath = "//*[@content-desc='btnClose']/android.view.ViewGroup[1]")})
  @iOSXCUITFindBy(xpath = "//*[@name='btnClose']/XCUIElementTypeOther[1]")
  private MobileElement xIcon;

  @AndroidFindBy(accessibility = "btnBack")
  @iOSXCUITFindBy(xpath = "//*[@name='btnBack']/XCUIElementTypeOther[1]")
  private MobileElement backIcon;

  @iOSXCUITFindBy(xpath = "//*[starts-with(@name, 'Cancel')]")
  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement cancelBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='Check your order status, client risk profiling, promotion, tutorial, and more']/..")
  @iOSXCUITFindBy(
      iOSNsPredicate =
          "name == 'Wealth Centre Check your order status, client risk profiling, promotion, tutorial, and more'")
  private MobileElement wealthCenterMenu;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-title']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement thanksTitle;

  @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='PROCESS_GUIDE-desc']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-desc'")
  private MobileElement pendingDesc;

  @AndroidFindBy(xpath = "//*[@text='Wealth Centre']")
  private MobileElement wealthCentre;

  @AndroidFindBy(xpath = "//*[@text='Review investment account profile']")
  private MobileElement reviewInvestmentAccountProfile;

  @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnBackContainer\"]")
  private MobileElement backBtn;

  @AndroidFindBy(
      xpath =
          "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-btn-toggle\"]/../android.view.ViewGroup")
  private MobileElement investmentHorizonEditBtn;

  @AndroidFindBy(
      xpath =
          "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-radio-btn-1\"]/android.view.ViewGroup")
  private MobileElement upTo3Years;

  @AndroidFindBy(
      xpath =
          "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-btn-toggle\"]/../android.view.ViewGroup")
  private MobileElement multipleChoiceEdit;

  @AndroidFindBy(
      xpath =
          "//android.view.ViewGroup[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-0\"]/android.view.ViewGroup")
  private MobileElement multipleChoiceFirst;

  @AndroidFindBy(
      xpath =
          "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-1\"]/android.view.ViewGroup")
  private MobileElement multipleChoiceSecond;

  @AndroidFindBy(
      xpath =
          "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-radio-btn-6\"]/android.view.ViewGroup")
  private MobileElement multipleChoicesSeventh;

  @AndroidFindBy(xpath = "//*[contains(@text,'We will automatically')]")
  private MobileElement weWillAutomatically;

  @AndroidFindBy(
      xpath = "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-btn-confirm\"]")
  private MobileElement OKBtn;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-desc-0")
  private MobileElement defaultHorizon;

  @AndroidFindBy(xpath = "//*[@text='Net worth']")
  private MobileElement netWorth;

  @AndroidFindBy(xpath = "//*[@content-desc=\"UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-desc-0\"]")
  private MobileElement knowledgeAndExperience;

  @AndroidFindBy(xpath = "//*[@text='Transaction']")
  private MobileElement transaction;

  @AndroidFindBy(xpath = "//*[@text='Customer Risk Profiling Questionnaire']")
  private MobileElement clientRiskProfiling;

  @AndroidFindBy(xpath = "//*[@text='Community']")
  private MobileElement community;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-0-subTitle")
  private MobileElement question1;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-1-subTitle")
  private MobileElement question2;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-2-subTitle")
  private MobileElement question3;

  @AndroidFindBy(accessibility = "UPDATE_NON_CRPQ_PERSONAL_REVIEW-question-3-subTitle")
  private MobileElement question4;

  public CommonPage() {
    super.pageName = this.pageName;
  }

  public void enableSkipRoot() throws InterruptedException {
    By skipRootBy = By.xpath(skipRootString);
    Thread.sleep(3000);
    waitUntilElementClickable(eyeBtn);
    clickElement(eyeBtn);
    waitUntilElementVisible(rocketButton);
    clickElement(rocketButton);
    scrollUpToFindElement(skipRootBy, 6, 3);
    skipRoot.click();
    clickElement(devPanelBackBtn);
  }

  public void moveEyeBtnToTopRight() {
    int width = driver.manage().window().getSize().width;
    TouchAction action = new TouchAction(driver);
    action
        .press(PointOption.point(eyeBtn.getCenter().getX(), eyeBtn.getCenter().getY()))
        .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
        .moveTo(PointOption.point(width, 0))
        .release()
        .perform();
  }

  public void checkUpgrade() {
    clickElement(loginTxt);
    if (isElementDisplayed(upgradeOKBtn, upgradeText)) clickElement(upgradeOKBtn);
  }

  public void clickContactWarningLogin() throws InterruptedException {
    Thread.sleep(3000);
    if (isElementDisplayed(wealthTab, customWait).isDisplayed()) {
      Thread.sleep(3000);
      if (isElementDisplayed(contactWarningCancel, cancelText)) {
        waitUntilElementClickable(contactWarningCancel);
        contactWarningCancel.click();
      }
    }
  }

  public void clickContactWarningCommon() {
    if (isElementDisplayed(contactWarningCancel, cancelText, 3)) {
      waitUntilElementClickable(contactWarningCancel);
      clickElement(contactWarningCancel);
    }
  }

  public void gotoLoan() throws InterruptedException {
    waitUntilElementVisible(loanTab);
    loanTab.click();
    Thread.sleep(waitMillSeconds);
  }

  public void gotoWealth() throws InterruptedException {
    waitUntilElementVisible(wealthTab);
    wealthTab.click();
    Thread.sleep(waitMillSeconds);
  }

  // GoalsNSettings: A_01_Landing to CA_01_SideMenu
  public void gotoMyAccount() {
    waitUntilElementVisible(MyAccountBtn);
    clickElement(MyAccountBtn);
    clickContactWarningCommon();
  }

  // GoalsNSettings: CA_01_SideMenu to CB_01_WealthCentre
  public void gotoWealthCenter() {
    waitUntilElementClickable(wealthCenterMenu);
    clickElement(wealthCenterMenu);
  }

  @SneakyThrows
  public void skipGuidance() {
    if (verifyElementExist(gotItBtn)) {
      waitUntilElementClickable(gotItBtn);
      Thread.sleep(200);
      clickElement(gotItBtn);
      waitUntilElementVisible(goalCardBtn);
      clickXIcon();
    }
  }

  // This is a workaround to go to versify personal information page
  // should not use this method if account generation script is ready.
  public void skipToOpenAccount() {
    waitUntilElementVisible(learnMoreBtn);
    clickElement(learnMoreBtn);
    waitUntilElementVisible(skipToOpenAccountBtn);
    clickElement(skipToOpenAccountBtn);
    waitUntilElementVisible(welcomeMutFundTitle);
    scrollUp();
    clickElement(welcomeMutFundBtn);
    waitUntilElementVisible(updateVerifyPersonalInfoTip);
    // if no delay, the "welcome mutual fund" might be generated again after click "Next"
    // sleep 200 ms to avoid this.
    try {
      Thread.sleep(500);
    } catch (Exception e) {
      logFail(e.getMessage(), pageName);
    }
    clickElement(nextBtn);
    // currently, exception might pop up during the step: Verify personal information.
    // check and cancel the exception page to continue
    checkException();
  }

  public void checkException() {
    if (verifyElementExist(contactWarningCancel)) {
      waitUntilElementVisible(contactWarningCancel);
      clickElement(contactWarningCancel);
    }
  }

  public void openAccount() {
    waitUntilElementClickable(openAccountBtn);
    clickElement(openAccountBtn);
  }

  public void clickXIcon() {
    waitUntilElementClickable(xIcon);
    clickElement(xIcon);
  }

  @SneakyThrows
  public void clickBackIcon() {
    Thread.sleep(toastWait);
    clickElement(backIcon);
    Thread.sleep(1000 * 3);
  }

  // check the upload page is loaded
  public boolean checkUploadElement() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  /**
   * A_06_Pending: Waiting for uploaded documents approval, the account not opened
   *
   * @return
   */
  public String getPendingApprovalDesc() {
    try {
      Thread.sleep(1000 * 2);
      waitUntilElementVisible(thanksTitle);
      WebElement pendingTxt = waitUntilElementVisible(pendingDesc);
      return pendingTxt.getText();
    } catch (Exception e) {
      return "";
    }
  }

  // onboarding: H_02_Landing
  @SneakyThrows
  public boolean checkWealthLanding() {
    Thread.sleep(1000 * 3);
    if (isElementDisplayed(openAccountBtn) != null) {
      return true;
    }
    return false;
  }

  public boolean checkNewGoal() {
    return isElementDisplayed(goalSettingPage.getAddNewGoalsLink(), "Add New Goals");
  }

  @SneakyThrows
  public boolean checkWealthCentreDisabled() {
    waitUntilElementVisible(wealthCenterMenu);
    clickElement(wealthCenterMenu);
    Thread.sleep(3000);
    // wealthCenterMenu is still displayed after click this menu.
    return isElementVisible(wealthCenterMenu);
  }

  public boolean checkGuidanceElementShowing() {
    waitUntilElementVisible(xIcon);
    return verifyElementExist(welcomeMutFundTitle);
  }

  public void changeAccount(String account, String password) throws IOException {
    List<String> list;
    try {
      logger.info("switched account");
      File file = new File(GlobalVar.RN_ACCOUNT_CONFIG_FILE);
      list = FileUtils.readLines(file, "UTF-8");
      for (int i = 0; i < list.size(); i++) {
        String line = list.get(i);
        if (line.trim().startsWith("TEST_ACCOUNT")) {
          String[] arr = line.split("'");
          if (!account.equals(arr[1])) {

            String str = line.replace(arr[1], account);
            list.remove(i);
            list.add(i, str);
          }
        }
        if (line.trim().startsWith("TEST_PASSWORD")) {
          String[] arr = line.split("'");
          if (!arr[1].equals(password)) {
            String str = line.replace(arr[1], password);
            list.remove(i);
            list.add(i, str);
          }
        }
      }
      FileUtils.writeLines(file, "UTF-8", list, false);
      logger.info("switched account to {}", account);
    } catch (IOException e) {
      logger.error("write file failed");
    }
  }

  public void clickCancelBtn() {
    waitUntilElementClickable(cancelBtn);
    clickElement(cancelBtn);
  }

  public void iClickWealthCentre() {
    waitUntilElementVisible(wealthCentre);
    clickElement(wealthCentre);
  }

  public void iClickReviewInvestmentAccountProfile() {
    clickElement(reviewInvestmentAccountProfile);
  }

  public void iClickSingleChoiceEditButton() {
    waitUntilElementVisible(investmentHorizonEditBtn);
    clickElement(investmentHorizonEditBtn);
  }

  public void iGetDefaultInvestmentHorizon() {
    defaultInvestmentHorizon = getElementText(defaultHorizon);
  }

  public void iGetDefaultKnowledgeAndExperience() {
    defaultKnowledgeAndExperience = getElementText(knowledgeAndExperience);
  }

  public boolean iVerifyInvestmentHorizonUpdate() {
    if (getElementText(defaultHorizon).equals("Up to 3 years")) {
      return true;
    }
    return false;
  }

  public boolean iVerifyInvestmentHorizonNotChange() {
    if (getElementText(defaultHorizon).equals(defaultInvestmentHorizon)) {
      return true;
    }
    return false;
  }

  public boolean iVerifyKnowledgeAndExperienceNotChange() {
    if (getElementText(knowledgeAndExperience).equals(defaultKnowledgeAndExperience)) {
      return true;
    }
    return false;
  }

  public void iChooseUpTo3Years() {
    waitUntilElementVisible(upTo3Years);
    clickElement(upTo3Years);
  }

  public void iClickMutipleChoiceEditButton() {
    clickElement(multipleChoiceEdit);
    scrollUpToFindElement(weWillAutomatically, 8, 3);
    clickElement(multipleChoiceFirst);
    clickElement(multipleChoiceSecond);
    clickElement(multipleChoicesSeventh);
  }

  public void iClickOKButton() {
    scrollDownToFindElement(OKBtn, 8, 2);
    clickElement(OKBtn);
  }

  @SneakyThrows
  public boolean iCanSeeMutipleChoice() {
    scrollUpToFindElement(netWorth, 10, 3);
    Thread.sleep(2 * 1000);
    if ((verifyElementExist(multipleChoiceFirst) && verifyElementExist(multipleChoiceSecond))
        || (verifyElementExist(multipleChoiceFirst) && verifyElementExist(multipleChoicesSeventh))
        || (verifyElementExist(multipleChoiceSecond)
            && verifyElementExist(multipleChoicesSeventh))) {
      return true;
    }
    return false;
  }

  public boolean iCanSeeThreesessionDivided() {
    if (verifyElementExist(transaction)
        && verifyElementExist(clientRiskProfiling)
        && verifyElementExist(community)) {
      return true;
    }
    return false;
  }

  @SneakyThrows
  public boolean iReviewCRPQQuestion() {
    Thread.sleep(2 * 1000);
    if ((getElementText(question1).equals("What is your education level?"))
        && (getElementText(question2).equals("What is your intended investment horizon*?"))
        && (getElementText(question3)
            .equals("I have knowledge and/or experience in the last 3 years in this product:"))
        && (getElementText(question4).equals("What is your total net worth*?"))) {
      return true;
    }
    return false;
  }
}
