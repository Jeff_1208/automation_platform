package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.projects.wealth.entities.Address;
import com.welab.automation.projects.wealth.entities.Employment;
import com.welab.automation.projects.wealth.entities.Person;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;

import java.util.List;

/** Verify personal information page during opening account */
public class VerifyPersonalInfoPage extends AppiumBasePage {

  private String pageName = "Verify personal information page";

  @AndroidFindAll(
      @AndroidBy(xpath = "//*[@text='Personal information']/../android.widget.TextView"))
  private List<MobileElement> personalInfos;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Account information']/../android.widget.TextView"))
  private List<MobileElement> accountInfos;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Mail Address']/../android.view.ViewGroup"))
  private MobileElement mailAddress;

  @AndroidFindAll(
      @AndroidBy(xpath = "//*[@text='Employment information']/../android.widget.TextView"))
  private List<MobileElement> employmentInfos;

  @AndroidFindAll(
      @AndroidBy(xpath = "//*[@text='Nationality / region']/../android.widget.TextView"))
  private List<MobileElement> nationality;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Next']"))
  private MobileElement next;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='District']"))
  private MobileElement district;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Estate / Street']"))
  private MobileElement street;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Building / Block']"))
  private MobileElement block;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Floor']"))
  private MobileElement floor;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Flat / Room']"))
  private MobileElement room;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Same as residential address']"))
  private MobileElement sameAsResidentialAddress;

  @AndroidFindAll(@AndroidBy(xpath = "//*[@text='Confirm']"))
  private MobileElement confirm;

  public VerifyPersonalInfoPage() {
    super.pageName = pageName;
  }

  public Person getPersonalInfo() {
    Person person = new Person();
    // ChineseName
    person.setChineseName(personalInfos.get(2).getText().trim());
    // lastname
    person.setLastName(personalInfos.get(3).getText().split(",")[0].trim());
    // firstName
    person.setFirstName(personalInfos.get(3).getText().split(",")[1].trim());
    // birthday
    person.setBirthday(personalInfos.get(5).getText().trim());
    return person;
  }

  public Employment getEmploymentInfo() {
    scrollUp();
    Employment employment = new Employment();
    // status
    employment.setStatus(employmentInfos.get(2).getText().trim());
    // industry
    employment.setIndustry(employmentInfos.get(4).getText().trim());
    // business nature
    employment.setBusinessNature(employmentInfos.get(6).getText().trim());
    // monthly income
    employment.setStatus(employmentInfos.get(8).getText().trim());
    // source of wealth
    employment.setStatus(employmentInfos.get(10).getText().trim());
    // source of funds
    employment.setStatus(employmentInfos.get(12).getText().trim());
    return employment;
  }

  public void updateMailAddress(Address address) {
    String district = address.getDistrict();
    String street = address.getStreet();
    String block = address.getBlock();
    String floor = String.valueOf(address.getFloor());
    String room = String.valueOf(address.getRoom());
    scrollUp();
    clickElement(mailAddress);
  }

  public void confirm() {
    clickElement(confirm);
  }
}
