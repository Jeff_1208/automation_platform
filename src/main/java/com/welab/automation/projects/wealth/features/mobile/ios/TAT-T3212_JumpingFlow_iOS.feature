Feature: check Guidance Page appearance

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test110|Aa123321|

  @WELATCOE-39
  Scenario: Verify guidance page is not displayed from jump flow
#    Given I proceed to document upload step
    And I click X icon
    And I click Open Account button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  @WELATCOE-39
  Scenario: Verify guidance page is not displayed from jump flow
    Given I proceed to portfolio recommendation page
    And I click Unlock the details button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test060|Aa123321|

  @WELATCOE-225
  Scenario: Goal setting_Build My Investment Habit Jump Flow to B_01_onboarding page
    And I skip guidance
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test110|Aa123321|

  @WELATCOE-225
  Scenario:Goal setting_Build My Investment Habit Jump Flow to E_00_upload page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  @WELATCOE-225
  Scenario: set account
    Given I launch app with account
      |account|password|
      |test174|Aa123321|

  @WELATCOE-225
  Scenario: Goal setting_Build My Investment Habit Jump Flow to G_00_CRPQstart page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  @WELATCOE-225
  Scenario: set account
    Given I launch app with account
      |account|password|
      |test008|Aa123321|

  @WELATCOE-225
  Scenario: Goal setting_Build My Investment Habit Jump Flow to A_06_Pending page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  @WELATCOE-225
  Scenario: set account
    Given I launch app with account
      |account|password|
      |testsit054|Aa123321|

  @WELATCOE-225
  Scenario: Goal setting_Build My Investment Habit Jump Flow to D_01b_transition page
    And I click X icon
    And I click Add New Goals
    And I Start to Build My Investing Habit
    And I click View recommendation
    And I click Unlock the details button
    Then I can see transition page
      | pageTitle | Upload document(s) |

  @WELATCOE-226
  Scenario:set account
    Given I launch app with account
      |account|password|
      |test060|Aa123321|

  @WELATCOE-226
  Scenario:Goal setting_Set My Target Jump Flow to B_01_onboarding page
    And I skip guidance
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  @WELATCOE-226
  Scenario:set account
    Given I launch app with account
      |account|password|
      |test110|Aa123321|

  @WELATCOE-226
  Scenario:Goal setting_Set My Target Jump Flow to E_00_upload page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test174|Aa123321|

  @WELATCOE-226
  Scenario: Goal setting_Set My Target Jump Flow to G_00_CRPQstart page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test008|Aa123321|

  @WELATCOE-226
  Scenario: Goal setting_Set My Target Jump Flow to A_06_Pending page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |testsit054|Aa123321|

  @WELATCOE-226 @debug
  Scenario: Goal setting_Set My Target Jump Flow to D_01b_transition page
    And I click X icon
    And I click Add New Goals
    And I Start To Set My Target
      | age | 50 |
    And I save my goal and goto Portfolio Recommendation
    And I click Unlock the details button
    Then I can see transition page
      | pageTitle | Upload document(s) |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test060|Aa123321|

  @WELATCOE-227
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to B_01_onboarding page
    And I skip guidance
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    Then I can see Welcome to Wealth Management Services
      | pageTitle | Welcome to Wealth Management Services |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test110|Aa123321|

  @WELATCOE-227
  Scenario:Goal setting_Achieve Financial Freedom Jump Flow to E_00_upload page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    Then I can find document upload page
      | pageTitle | Upload document(s) |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test174|Aa123321|

  @WELATCOE-227
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to G_00_CRPQstart page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test008|Aa123321|

  @WELATCOE-227
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to A_06_Pending page
#    Given Login with user and password
#      | user     | test008  |
#      | password | Aa123321 |
#    When I can see the LoggedIn page
#    And I goto wealth main page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |

  Scenario: set account
    Given I launch app with account
      |account|password|
      |testsit054|Aa123321|

  @WELATCOE-227
  Scenario: Goal setting_Achieve Financial Freedom Jump Flow to D_01b_transition page
    And I click X icon
    And I click Add New Goals
    And I setup Achieve Financial Freedom
      | age | monthlyExpenses | otherExpenses        |
      | 40  | 5000          | Budget,Modest,Deluxe |
    And I click Unlock the details button
    Then I can see transition page
      | pageTitle | Upload document(s) |