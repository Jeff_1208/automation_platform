Feature: Update and check Investment account profile

  Background: Login with correct user and password
    Given Open WeLab App
    And Enable skip Root
    And Check upgrade page appears
    And Login with user and password
      | user     | test192  |
      | password | Aa123321 |
    And I can see the LoggedIn page

  @WELATCOE597
  Scenario: Update Investment account profile
    And goto My Account page
    And I click Wealth Center
    And I click Review investment account profile
    And I get default options
    And I click back icon
    And I click Review investment account profile
    And I click X icon
    And I click Review investment account profile
    And I click Investment Horizon edit Button
    And I choose Up to 3 years
    And I verify Investment Horizon update in Review investment account profile page
    And I click multiple choice edit button and choose options
    And I click X icon
    And I click Review investment account profile
    And I click multiple choice edit button and choose options
    And I click OK Button
    And I can see multiple choice display in Review investment account profile
    And I click back icon
    And I click Review investment account profile
    And I verify options have not changed

  @WELATCOE595
  Scenario: Review nonCRPQ question from side menu
    And goto My Account page
    And I click Wealth Center
    Then I can see three sessions divided
    And I click Review investment account profile
    And I review CRPQ questions








