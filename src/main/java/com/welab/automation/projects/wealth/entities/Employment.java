package com.welab.automation.projects.wealth.entities;

import lombok.Getter;
import lombok.Setter;

public class Employment {
    @Getter
    @Setter
    private String status;
    @Getter
    @Setter
    private String industry;
    @Getter
    @Setter
    private String businessNature;
    @Getter
    @Setter
    private String monthlyIncome;
    @Getter
    @Setter
    private String sourceOfWealth;
    @Getter
    @Setter
    private String sourceOfFunds;
}
