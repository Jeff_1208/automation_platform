Feature: Verify pagination

  Background: Prepare enough opened accounts to verify pagination


  @WELATCOE-591
  Scenario: Verify pagination on investment account summary page

      Given Open the URL https://wealth-portal-sit.dev-wlab.net/
      And login admin portal
        | username | ops1 |
        | password | ops1 |
      When I click navigate from side menu: Investment Account Summary
      And  I check default pagination
      And  I check pagination at the bottom of the page
  @WELATCOE-569
  Scenario: Verify pagination on Maker's approval list page

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    When goto approval checker
    And  I check default pagination
    And  I check pagination at the bottom of the Approval page
  @WELATCOE-576
  Scenario: Verify pagination on Checker's approval list page

    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    When goto approval maker
    And  I check default pagination
    And  I check pagination at the bottom of the Approval page
