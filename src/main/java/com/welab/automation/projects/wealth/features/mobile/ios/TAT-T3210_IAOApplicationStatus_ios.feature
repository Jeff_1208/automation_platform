Feature: verify APP UI when user complete application and maker&checker took different action on admin portal

  Background: IAO application submit
#    TODO: create new account

  # These accounts only can be used once, since the action cannot be revoked on admin portal.

  @WELATCOE-553
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'agree' & checker = 'agree'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |

    Then I can see pending page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |
    Examples:
      | username |
      | test231  |


  @WELATCOE-555
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'agree' & checker = 'agree'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test215  |

  @WELATCOE-555
  Scenario: Verify APP UI of task WELATCOE-555
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |


  @WELATCOE-557
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'agree' & checker = 'reject'
#     TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
        # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test217  |

  @WELATCOE-557
  Scenario: Verify APP UI of task WELATCOE-557
    Then I can see pending page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |


  @WELATCOE-559
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'agree' & checker = 'reject'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And approve the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test216  |

  @WELATCOE-559
  Scenario: Verify APP UI of task WELATCOE-559
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |


  @WELATCOE-561
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'reject' & checker = 'reject'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test212  |

  @WELATCOE-561
  Scenario: Verify APP UI of task WELATCOE-561
    Then I can see CRPQStart page
      | pageTitle | Complete Customer Risk Profiling Questionnaire |


  @WELATCOE-563
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'reject' & checker = 'reject'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And disagree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test118  |

  @WELATCOE-563
  Scenario: Verify APP UI of task WELATCOE-563
    Then I can see pending page
      | pageTitle | Thank you for completing the application. |


  @WELATCOE-565
  Scenario Outline: Verify APP UI when client completes step 1&2 and maker = 'reject' & checker = 'agree'
    # TODO: IAO steps 1&2
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test110  |

  @WELATCOE-565
  Scenario: Verify APP UI of task WELATCOE-565
    Then I can find document upload page
      | pageTitle | Document upload |


  @WELATCOE-567
  Scenario Outline: Verify APP UI when client completes step 1&2&3 and maker = 'reject' & checker = 'agree'
    # TODO: IAO steps 1&2&3
    Given Open the URL https://wealth-portal-sit.dev-wlab.net/
    And login admin portal
      | username | ops1 |
      | password | ops1 |
    And goto approval maker
    And reject the application of <username>
    And logout admin portal
    And login admin portal
      | username | ops2 |
      | password | ops2 |
    And goto approval checker
    And agree the application of <username>
    # change account for wealthSDK on iOS
    And I update wealth SDK account with <username> and password
      | password | Aa123321 |
    Examples:
      | username |
      | test213  |

  @WELATCOE-567
  Scenario: Verify APP UI of task WELATCOE-567
    Then I can see transition page
      | pageTitle | Upload document(s) |
