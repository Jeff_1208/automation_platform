package com.welab.automation.projects.wealth.pages.iao;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.welab.automation.framework.utils.Utils.logFail;
import static com.welab.automation.framework.utils.Utils.logPass;

/**
 * IAO's step 3, My Account--->Wealth Centre----> Update CRPQ risk profile and Account--->Wealth
 * Centre----> Review My Risk Profile are handled in this page.
 */
public class CRPQPage extends AppiumBasePage {
  private final Logger logger = LoggerFactory.getLogger(CRPQPage.class);
  public String pageName = "CRPQ Page";

  String questionnaire = "Questionnaire";

  @AndroidFindBy(accessibility = "PROCESS_GUIDE-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Next'")
  private MobileElement NextBtn;

  @Getter
  @WithTimeout(time = 2, chronoUnit = ChronoUnit.SECONDS)
  @AndroidFindBy(accessibility = "PROCESS_GUIDE-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-title'")
  private MobileElement GuideTitle;

  @AndroidFindBy(accessibility = "PROCESS_GUIDE-desc")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'PROCESS_GUIDE-desc'")
  private MobileElement GuideDesc;

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup")
  private List<MobileElement> stepPromptElements;

  @AndroidFindBy(accessibility = "CRPQ_INTRODUCE-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Start'")
  private MobileElement StartBtn;

  @AndroidFindBy(accessibility = "CRPQ_INTRODUCE-title")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ_INTRODUCE-title'")
  private MobileElement CRPQIntroBtn;

  @WithTimeout(time = 2, chronoUnit = ChronoUnit.SECONDS)
  @AndroidFindBy(accessibility = "CRPQ-txt-desc")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ-txt-desc'")
  private MobileElement CRPQTxtDesc;

  @AndroidFindBy(accessibility = "CRPQ_CONFIRM-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ_CONFIRM-btn'")
  private MobileElement CRPQConfirmBtn;

  @AndroidFindBy(accessibility = "CRPQ_RISK_RESULT-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ_RISK_RESULT-btn'")
  private MobileElement CRPQRiskResultBtn;

  @AndroidFindBy(accessibility = "CRPQ_RISK_RESULT-level")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ_RISK_RESULT-level'")
  private MobileElement CRPQResultLevel;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Review customer risk profile']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Client risk profiling result'")
  private MobileElement CRPQResultMenu;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='View record']")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Review My Risk Profile'")
  private MobileElement ReviewCRPQMenu;

  @AndroidFindBy(
          xpath =
                  "//android.widget.TextView[starts-with(@text, 'Your Customer Risk Rating is')]/../android.widget.TextView[2]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'UPDATE_CRPQ_RISK_RESULT-level'")
  private MobileElement UpdateCRPQResultLevel;

  @AndroidFindBy(accessibility = "UPDATE_CRPQ_RISK_RESULT-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name=='UPDATE_CRPQ_RISK_RESULT-btn'")
  private MobileElement UpdateCRPQBtn;

  @AndroidFindBy(accessibility = "UPDATE_CRPQ-txt-desc")
  @iOSXCUITFindBy(iOSNsPredicate = "name=='UPDATE_CRPQ-txt-desc'")
  private MobileElement UpdateCRPQTxtDesc;

  @AndroidFindBy(accessibility = "UPDATE_CRPQ_CONFIRM-btn")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'UPDATE_CRPQ_CONFIRM-btn'")
  private MobileElement UpdateCRPQConfirmBtn;

  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[contains(@content-desc, 'CRPQ_RESULT_REVIEW-question-title')]")
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'CRPQ_RESULT_REVIEW-question-title-RAT01'")
  private MobileElement ReviewCRPQTxtDesc;

  @Getter
  @AndroidFindBy(
      xpath = "//android.view.ViewGroup[5]/android.view.ViewGroup/android.view.ViewGroup")
  @iOSXCUITFindBy(xpath = "//*[@name='btnBackContainer']/../XCUIElementTypeOther[2]")
  private MobileElement xBtnG0CRPQ;

  public CRPQPage() {
    super.pageName = pageName;
  }

  // onboarding: F_01c_transition or G_00_CRPQstart
  public boolean clickTransitionPage2() {
    waitUntilElementVisible(NextBtn);
    // use getElementText may get the guide title in previous page, so use isElementDisplayed
    // instead.
    if (isElementDisplayed(GuideTitle, questionnaire)) {
      clickElement(NextBtn);
      return true;
    }
    return false;
  }

  // onboarding: F_01c_transition or G_00_CRPQstart
  public boolean seeCRPQWelcomePage() {
    waitUntilElementVisible(NextBtn);
    // use getElementText may get the guide title in previous page, so use isElementDisplayed
    // instead.
    if (isElementDisplayed(GuideTitle, questionnaire)) {
      return true;
    }
    return false;
  }

  // onboarding: G_01_CRPQinfo
  public void startCRPQ() {
    UpdatePageName("CRPQ");
    waitUntilElementVisible(StartBtn);
    String elementText = getElementText(CRPQIntroBtn);
    clickElement(StartBtn);
  }

  public boolean isCRPQIntroductionDisplayed() {
    UpdatePageName("CRPQ");
    return waitUntilElementVisible(CRPQIntroBtn).isDisplayed();
  }

  public boolean isCRPQParticularQuestionDisplayed(String text) {
    UpdatePageName("CRPQ");
    return waitUntilElementVisible(CRPQTxtDesc).getText().equalsIgnoreCase(text);
  }

  // onboarding: G_02_CRPQ1 ~ G_08_CRPQ7
  @SneakyThrows
  public boolean selectOption(String questionDesc, String selectedAnswerTxt) {
    // wait util question visible when switching question pages.
    MobileElement ele = CRPQTxtDesc;
    if (pageName.toLowerCase().contains("update")) {
      ele = UpdateCRPQTxtDesc;
    }
    if (!ele.getText().startsWith(questionDesc)) {
      logFail("Cannot get CRPQ question " + questionDesc, pageName);
      return false;
    }

    String byString = null;
    if (System.getProperty("mobile").contains("android"))
      byString = String.format("//android.widget.TextView[@text='%s']", selectedAnswerTxt);
    if (System.getProperty("mobile").contains("ios"))
      byString = String.format("//*[@name='%s']", selectedAnswerTxt);
    // sleep to avoid StaleElementReferenceException
    try {
      Thread.sleep(350);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    MobileElement optEle = scrollUpToFindElement(By.xpath(byString), 5);
    clickElement(optEle);
    Thread.sleep(1000 * 2);
    logPass("Succeeded to select Answer" + selectedAnswerTxt, pageName);
    return true;
  }

  // onboarding: G_09_CRPQoverview, G_10_CRPQresult
  public void confirmCRPQAnser() {
    MobileElement ele = CRPQConfirmBtn;
    if (pageName.toLowerCase().contains("update")) {
      ele = UpdateCRPQConfirmBtn;
    }
    waitUntilElementVisible(ele);
    // once confirm, there is no account for testing, so comment here.
    clickElement(ele);
    //        try {
    //            Thread.sleep(4000);
    //        } catch (InterruptedException e) {
    //            logger.error("failed to pause 100 millis " + e);
    //        }
  }

  // onboarding: G_10_CRPQresult to H_01_transition/H_01a_transition/H_01b_transition,
  // H_01_transition/H_01a_transition/H_01b_transition to H_02_Landing
  public boolean finishCRPQ(boolean isSkipUploadDoc) {
    clickElement(CRPQRiskResultBtn);

    String titleTxt;
    if (isSkipUploadDoc) {
      titleTxt = "Hope you have a better understanding";
    } else {
      titleTxt = "Congratulations!";
    }

    if (isElementDisplayed(GuideTitle, titleTxt)) {
      clickElement(NextBtn);
      return true;
    }
    return false;
  }

  // GoalsNSettings: CB_01_WealthCentre
  public void viewCRPQResult() {
    UpdatePageName("Update CRPQ");
    waitUntilElementVisible(CRPQResultMenu);
    clickElement(CRPQResultMenu);
  }

  // GoalsNSettings: CD_01_CRPQ1
  // onboarding: G_10_CRPQresult
  public String getCRPQScore() {
    MobileElement ele = CRPQResultLevel;
    if (pageName.toLowerCase().contains("update")) {
      ele = UpdateCRPQResultLevel;
    }
    waitUntilElementVisible(ele);
    System.out.println(ele.getText());
    System.out.println(ele.getAttribute("label"));
    return getElementText(ele);
  }

  private void UpdatePageName(String pageName) {
    if (!this.pageName.equals(pageName)) {
      this.pageName = pageName;
    }
  }

  // GoalsNSettings: CD_01_CRPQ1
  public void startUpdateCRPQ() {
    UpdatePageName("Update CRPQ");
    clickElement(UpdateCRPQBtn);
  }

  // GoalsNSettings: CB_01_WealthCentre
  public void startReviewCRPQ() {
    UpdatePageName("View CRPQ");
    waitUntilElementVisible(ReviewCRPQMenu);
    clickElement(ReviewCRPQMenu);
  }

  // GoalsNSettings: CE_01_ReviewCRPQ1 ~ CE_07_ReviewCRPQ7
  @SneakyThrows
  public boolean verifyAnswer(String questionDesc, String expAnswer) {
    if (System.getProperty("mobile").contains("ios")) {
      Thread.sleep(1000 * 3);
      MobileElement element =
          driver.findElement(
              By.xpath("//XCUIElementTypeStaticText[starts-with(@label, '" + questionDesc + "')]"));
      if (!element.isDisplayed()) {
        logFail("Cannot get CRPQ question " + questionDesc, pageName);
        return false;
      }
    }
    if (System.getProperty("mobile").contains("android")) {
      if (!isElementDisplayed(ReviewCRPQTxtDesc, questionDesc)) {
        logFail("Cannot get CRPQ question " + questionDesc, pageName);
        return false;
      }
    }
    // todo, verify answer is same as that in submitting CRPQ answers.
    String byString = String.format("//android.widget.TextView[@text='%s']", expAnswer);
    if (System.getProperty("mobile").contains("ios"))
      byString = String.format("//*[@name='%s']", expAnswer);
    MobileElement ele = scrollUpToFindElement(By.xpath(byString), 5);

    swipeRightToLeft();
    return true;
  }

  // GoalsNSettings: CD_01a_CRPQ2
  public boolean verifyCannotUpdateCRPQ() {
    String tips = "You have exceeded your monthly update limit"; // how about traditional Chinese?
    waitUntilElementClickable(UpdateCRPQBtn);

    boolean checkTipsRslt = false;
    if (System.getProperty("mobile").contains("android")) {
      checkTipsRslt =
          verifyElementExist(
              By.xpath("//android.widget.TextView[contains(@text, '" + tips + "')]"));
    } else if (System.getProperty("mobile").contains("ios")) {
      checkTipsRslt =
          verifyElementExist(
              By.xpath(
                  "//*[@name='You have exceeded your attempt limit. Please try again later.']"));
    }

    clickElement(UpdateCRPQBtn);
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    // update risk profile button is grey, and clicking it will not navigate to next page.
    WebElement updateBtnDisplayed = isElementDisplayed(UpdateCRPQBtn);
    //  UpdateCRPQBtn.isEnabled(),  the button is still enabled.
    return updateBtnDisplayed != null && checkTipsRslt;
  }
}
