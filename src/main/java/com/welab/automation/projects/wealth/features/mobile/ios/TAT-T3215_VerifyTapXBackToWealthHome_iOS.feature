@iOS @WELATCOE-15
Feature: Verify tap X back to wealth home page

  Scenario: set account
    Given I launch app with account
      |account|password|
      |test060|Aa123321|

  Scenario: Verify tap X back to wealth home on welcome page
      # Click X on A_03_Selection
    And I click Got it on Welcome page
    When I click X icon
    Then I'm on wealth home page

      # Click X on A_04a_Goal
  Scenario: Verify tap X back to wealth home on goal based page
#    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    When I click X icon
    Then I'm on wealth home page

      # Click X on A_04b_Goal
  Scenario: Verify tap X back to wealth home on goal base description page
#    Given goto loan main page
#    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    And I scroll left on GoalBased Description page
    When I click X icon
    Then I'm on wealth home page

     # Click X on A_04c_Goal
  Scenario: Verify tap X back to wealth home on goal base description 2nd page
#    Given goto loan main page
#    And I goto wealth main page
    And I click Got it on Welcome page
    And I click GoalBased on Select GoalBased page
    And I scroll left on GoalBased Description page
    And I scroll left on GoalBased Description page
    When I click X icon
    Then I'm on wealth home page
#      # B_01_onboarding, cannot working now, reported a bug
#    Given goto loan main page
#    And I goto wealth main page
#    And I click Got it on Welcome page
#    And I click GoalBased on Select GoalBased page
#    And I click Skip to open an account
#    When I click X on on-boarding page
#    Then I'm on wealth home page

      # B_03_getStart
  Scenario: Verify tap X back to wealth home on mutual fund services welcome page
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And Click Next on Mutual Fund Services Welcome page
    When I click X icon
    Then I'm on wealth home page

      # bk/C_01_infoReview
  Scenario: Verify tap X back to wealth home on check personal information page
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_03_non_CRPQ1
  Scenario: Verify tap X back to wealth home on confirm personal info page
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_04_non_CRPQ2
  Scenario: Verify tap X back to wealth home on select nonCRPQ 1st question
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                   | answer |
      | Declaration – Employee of Intermediary | No     |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_05_non_CRPQ3
  Scenario: Verify tap X back to wealth home on select nonCRPQ 2nd question
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                | answer                    |
      | Declaration – Employee of Intermediary  | No                        |
      | Qualification information               | University level or above |
    When I click X on CRPQ page
    Then I'm on wealth home page

        # C_06_non_CRPQ4
  Scenario: Verify tap X back to wealth home on select nonCRPQ 3rd question
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                | answer                    |
      | Declaration – Employee of Intermediary  | No                        |
      | Qualification information               | University level or above |
      | Investment Horizon                      | Up to 1 year              |
    When I click X on CRPQ page
    Then I'm on wealth home page

        # C_07_non_CRPQ5
  Scenario: Verify tap X back to wealth home on select nonCRPQ 4th question
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                    |
      | Declaration – Employee of Intermediary      | No                        |
      | Qualification information                   | University level or above |
      | Investment Horizon                          | Up to 1 year              |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds         |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_08_non_CRPQReview
  Scenario: Verify tap X back to wealth home on select nonCRPQ 5th question
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Declaration – Employee of Intermediary      | No                            |
      | Qualification information                   | University level or above     |
      | Investment Horizon                          | Up to 1 year                  |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    When I click X on CRPQ page
    Then I'm on wealth home page

      # C_09_notes
  Scenario: Verify tap X back to wealth home on confirm nonCRPQ page
#    Given goto loan main page
#    And I goto wealth main page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Declaration – Employee of Intermediary      | No                            |
      | Qualification information                   | University level or above     |
      | Investment Horizon                          | Up to 1 year                  |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    And I click confirm on nonCRPQ review page
    When I click X on CRPQ page
    Then I'm on wealth home page

    # D_01_transition
  Scenario: Verify tap X back to wealth home on updated and verified personal info page
#    And Login with user and password
#      | user     | test010  |
#      | password | Aa123321 |
#    And I can see the LoggedIn page
    And I begin none-CRPQ questionnaire process
    And I begin to check personal information
    And I confirm the personal information
    And I select nonCRPQ answer
      | question                                    | answer                        |
      | Declaration – Employee of Intermediary      | No                            |
      | Qualification information                   | University level or above     |
      | Investment Horizon                          | Up to 1 year                  |
      | Investment Product Knowledge and Experience | Bonds, Bond Funds             |
      | Net worth                                   | HKD 1,000,001 - HKD 5,000,000 |
    And I click confirm on nonCRPQ review page
    And I confirm the Investment account profile
    When I click X icon
    Then I'm on wealth home page

    #D_01a_transitin
  Scenario: Verify tap X back to wealth home on document upload page
#    And I goto wealth main page
    And I can see document transition page
    When I click X icon
    Then I'm on wealth home page

    # E_01_upload
  Scenario: Verify tap X back to wealth home on upload documents page
#    Given goto loan main page
#    And I goto wealth main page
    And I can see document transition page
    And I click next on document transition page
    When I click X on CRPQ page
    Then I'm on wealth home page

    #F_02_transition
  Scenario: Verify tap X back to wealth home on uploaded successfully page
    And I can see document transition page
    And I click next on document transition page
    And I go to address proof page
    And I add photo from photo library
    When I click X icon
    Then I'm on wealth home page

    #F_01c_transition
  Scenario: Verify tap X back to wealth home on CRPQ welcome page
    And I can see CRPQ welcome page
    When I click X icon
    Then I'm on wealth home page


    # G_01_CRPQinfo
  Scenario: Verify tap X back to wealth home on CRPQ info page
#    Given goto loan main page
#    And I goto wealth main page
    And Finished uploading document
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_02_CRPQ1
  Scenario: Verify tap X back to wealth home on CRPQ 1st page
#    Given goto loan main page
#    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_03_CRPQ02
  Scenario: Verify tap X back to wealth home on CRPQ 2nd page
#    Given goto loan main page
#    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    And I Select Answer for Can you tell me why you're investing with us
      | Preserve my capital and earn deposit rate |
    When I click X on CRPQ page
    Then I'm on wealth home page

    # G_09_CRPQoverview
  Scenario: Verify tap X back to wealth home on CRPQ overview page
#    Given goto loan main page
#    And I goto wealth main page
    And Finished uploading document
    And Starting CRPQ
    And I Select Answer for Can you tell me why you're investing with us
      | Preserve my capital and earn deposit rate |
    And I Select Answer for How do you see risk-return
      | moderate risk to achieve moderate returns |
    And I Select Answer for I can accept if my investment value fluctuates between
      | -20% and 20% |
    And I Select Answer for How much of your total liquid assets
      | Less than 5% |
    And I Select Answer for To meet my financial needs in the next 12 months
      | Sell up to 25% |
    And I Select Answer for If your investment value dropped by 50%
      | Invest more to take advantage of lower price |
    And I Select Answer for How many months of expenses have you put aside
      | 12 months or above |
    When I click X on CRPQ page
    Then I'm on wealth home page


