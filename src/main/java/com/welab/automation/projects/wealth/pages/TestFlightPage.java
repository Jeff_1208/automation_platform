package com.welab.automation.projects.wealth.pages;

import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import lombok.Getter;
import lombok.SneakyThrows;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class TestFlightPage extends AppiumBasePage {

  private String pageName = "Test flight";
  private static final Logger logger = LoggerFactory.getLogger(TestFlightPage.class);

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Allow'")
  private MobileElement allow;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Continue'")
  private MobileElement continueBtn;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name BEGINSWITH 'WeLab Bank SIT'")
  private MobileElement welab;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'Not Now'")
  private MobileElement notNow;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'OPEN'")
  private MobileElement open;

  @Getter
  @iOSXCUITFindBy(iOSNsPredicate = "name == 'INSTALL'")
  private MobileElement install;

  private boolean isNew;
  private AppiumDriver driver = ((AppiumDriver<MobileElement>) BaseDriver.getMobileDriver());

  public TestFlightPage() {
    super.pageName = pageName;
  }

  public void initApp() {
    //        try {
    //            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    //            logger.info("trying to click Allow button on mobile cloud...");
    //            allow.click();
    //        } catch (NoSuchElementException ex) {
    //        }
    //        try {
    //            logger.info("trying to click Continue button on mobile cloud...");
    //            continueBtn.click();
    //        } catch (NoSuchElementException ex) {
    //            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    //        }
    try {
      new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(welab));
    } catch (Exception ex) {
      scrollUp();
      new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(welab));
    }
  }

  @SneakyThrows
  public void installOrOpenApp() {
    String platform = System.getProperty("mobile");
    if (platform.contains("cloud")) {
      welab.click();
      //            try {
      //                logger.info("trying to click Not Now button on mobile cloud...");
      //                notNow.click();
      //            } catch (NoSuchElementException ex) {
      //                logger.warn("Not Now button didn't exist.");
      //            }
    }
    try {
      driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
      boolean isEnabled = open.isEnabled();
    } catch (NoSuchElementException ex) {
      driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
      install.click();
      logger.info("Install Welab app...");
      isNew = true;
      new WebDriverWait(driver, 3600).until(ExpectedConditions.visibilityOf(open));
    }
    open.click();
    logger.info("Open Welab app...");
  }

  @SneakyThrows
  public MobileElement launchApp() {
    LoginPage loginPage = new LoginPage();
    if (isNew) {
      loginPage.waitUntilElementClickable(loginPage.getNotification(), 10);
      loginPage.getNotification().click();
      loginPage.getNext().click();
      loginPage.getStart().click();
      loginPage.getNext().click();
      loginPage.getNoTracking().click();
    } else {
      Thread.sleep(1000 * 3);
      loginPage.waitUntilElementClickable(loginPage.getLoginTxt(), 10);
    }
    return loginPage.getLoginTxt();
  }
}
