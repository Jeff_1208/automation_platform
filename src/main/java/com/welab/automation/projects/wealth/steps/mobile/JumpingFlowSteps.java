package com.welab.automation.projects.wealth.steps.mobile;

import com.welab.automation.projects.wealth.pages.CommonPage;
import com.welab.automation.projects.wealth.pages.iao.CRPQPage;
import com.welab.automation.projects.wealth.pages.iao.GoalSettingPage;
import com.welab.automation.projects.wealth.pages.iao.UploadFilesPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.SneakyThrows;
import org.picocontainer.annotations.Inject;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class JumpingFlowSteps {
  @Inject CommonPage commonPage;
  @Inject GoalSettingPage goalSettingPage;
  @Inject UploadFilesPage uploadFilesPage;
  @Inject CRPQPage crpqPage;

  @Given("I proceed to document upload step")
  public void proceedToUpload() {
    // Because the account is limited now. choose an account which already in upload step to demo.
    // TODO: fix this method when account generation/reset script is ready.
    //        jumpFlowPage.openAccount();
  }

  @And("exit upload step")
  @And("I quit opening account")
  @And("I quit and back to Open Account")
  public void quitTheWizard() throws InterruptedException {
    commonPage.clickXIcon();
  }

  @And("I click Open Account button")
  public void openAccount() {
    commonPage.openAccount();
  }

  @Then("I can find document upload page")
  public void verifyUploadPage(Map<String, String> data) {
    uploadFilesPage.waitUntilElementVisible(uploadFilesPage.getDocumentUpLoadTitle(), 60);
    String titleText = uploadFilesPage.getElementText(uploadFilesPage.getDocumentUpLoadTitle());
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
    if (System.getProperty("mobile").equals("android")) {
      // currently, xpath cannot identify the order of the ticks, so use the amounts to verify.
      // need to fix after accessibility ID is added.
      assertThat(uploadFilesPage.getStepPromptElements().size()).isEqualTo(6);
    } else if (System.getProperty("mobile").equals("ios")) {
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement1()))
          .isEqualTo(true);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement2()))
          .isEqualTo(false);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement3()))
          .isEqualTo(false);
      assertThat(uploadFilesPage.getProcessGuideAfterStep1Group1Elements().size()).isEqualTo(2);
      assertThat(uploadFilesPage.getProcessGuideAfterStep1Group2Elements().size()).isEqualTo(1);
      assertThat(uploadFilesPage.getProcessGuideAfterStep1Group3Elements().size()).isEqualTo(1);
    }
  }

  @SneakyThrows
  @Then("I can see CRPQStart page")
  public void verifyCRPQStartPage(Map<String, String> data) {
    Thread.sleep(1000 * 3);
    String titleText = crpqPage.getElementText(crpqPage.getGuideTitle());
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
    if (System.getProperty("mobile").equals("android")) {
      assertThat(crpqPage.getStepPromptElements().size()).isEqualTo(7);
    } else if (System.getProperty("mobile").equals("ios")) {
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement1()))
          .isEqualTo(true);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement2()))
          .isEqualTo(true);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement3()))
          .isEqualTo(false);
      assertThat(uploadFilesPage.getProcessGuideAfterStep12Group1Elements().size()).isEqualTo(2);
      assertThat(uploadFilesPage.getProcessGuideAfterStep12Group2Elements().size()).isEqualTo(2);
      assertThat(uploadFilesPage.getProcessGuideAfterStep12Group3Elements().size()).isEqualTo(1);
    }
  }

  @Then("I can see transition page")
  public void verifyTransitionPage(Map<String, String> data) {
    uploadFilesPage.waitUntilElementVisible(uploadFilesPage.getDocumentUpLoadTitle());
    String titleText = uploadFilesPage.getElementText(uploadFilesPage.getDocumentUpLoadTitle());
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
    // currently, xpath cannot identify the order of the ticks, so use the amounts to verify.
    // need to fix after accessibility ID is added.
    if (System.getProperty("mobile").equals("android")) {
      assertThat(uploadFilesPage.getStepPromptElements().size()).isEqualTo(7);
    } else if (System.getProperty("mobile").equals("ios")) {
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement1()))
          .isEqualTo(true);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement2()))
          .isEqualTo(false);
      assertThat(uploadFilesPage.verifyElementExist(uploadFilesPage.getStepPromptElement3()))
          .isEqualTo(true);
      assertThat(uploadFilesPage.getProcessGuideAfterStep13Group1Elements().size()).isEqualTo(2);
      assertThat(uploadFilesPage.getProcessGuideAfterStep13Group2Elements().size()).isEqualTo(1);
      assertThat(uploadFilesPage.getProcessGuideAfterStep13Group3Elements().size()).isEqualTo(2);
    }
  }

  @Then("I can verify guidance page is not showing")
  public void checkGuidancePage() {
    // guidance page should not be displayed and should display upload page.
    assertThat(commonPage.checkGuidanceElementShowing()).isEqualTo(false);
  }

  @Given("I proceed to portfolio recommendation page")
  public void proceedToPortfolioRecommendation() throws InterruptedException {
    commonPage.clickXIcon();
    goalSettingPage.addNewGoal();
    goalSettingPage.setDefaultTarget();
  }

  @And("I click Unlock the details button")
  public void unlockDetails() {
    goalSettingPage.unlockDetails();
  }

  @Then("I can see Welcome to Wealth Management Services")
  public void checkMutualFundWelcomePage(Map<String, String> data) {
    String titleText = crpqPage.getElementText(commonPage.getWelcomeMutFundTitle());
    assertThat(titleText).isEqualTo(data.get("pageTitle"));
  }

  @And("I setup Achieve Financial Freedom")
  public void iSetupAchieveFinancialFreedom(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String age = data.get(0).get("age");
    String monthlyExpenses = data.get(0).get("monthlyExpenses");
    String otherExpenses = data.get(0).get("otherExpenses");
    goalSettingPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.calculateTargetWealth();
    goalSettingPage.saveGoal();
  }

  @And("I setup Achieve Financial Freedom with name")
  public void iSetupAchieveFinancialFreedomWithName(DataTable dataTable) {
    List<Map<String, String>> data = dataTable.asMaps();
    String name = data.get(0).get("name");
    String age = data.get(0).get("age");
    String monthlyExpenses = data.get(0).get("monthlyExpenses");
    String otherExpenses = data.get(0).get("otherExpenses");
    String oneTimeValue = data.get(0).get("oneTimeValue");
    String monthlyValue = data.get(0).get("monthlyValue");
    goalSettingPage.setFinancialFreedomGoal(age);
    assertThat(goalSettingPage.getFFTAgeSliderText()).isEqualTo(age);
    goalSettingPage.clickGoalNameEditButton();
    goalSettingPage.editGoalSettingName(name);
    goalSettingPage.clickContinue();
    goalSettingPage.setMonthlyExpenses(monthlyExpenses, otherExpenses);
    goalSettingPage.setInvestmentPlan(oneTimeValue, monthlyValue);
    goalSettingPage.calculateTargetWealth();
    goalSettingPage.saveGoal();
  }
}
