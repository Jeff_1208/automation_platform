
package com.welab.automation.projects.demo.runners;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeSuite;

import java.util.Map;

@CucumberOptions(
    features = {"src/main/java/com/welab/automation/projects/demo/features/mobile"},
    glue = {"com/welab/automation/projects/demo/steps/mobile"})
public class MobileFarmAPPRunner extends AbstractTestNGCucumberTests {
  @BeforeSuite
  public void beforeSuite() {
    Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
    GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
  }
}
