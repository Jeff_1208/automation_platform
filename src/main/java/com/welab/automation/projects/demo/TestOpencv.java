package com.welab.automation.projects.demo;

import com.welab.automation.framework.GlobalVar;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.touch.offset.PointOption;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
//import org.sikuli.script.Match;
//import org.sikuli.script.Region;
//import org.sikuli.script.Screen;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static com.welab.automation.framework.GlobalVar.TARGET_SCREENSHOTS_PATH;
import static com.welab.automation.framework.utils.Utils.logFail;
import static com.welab.automation.framework.utils.Utils.logPass;
import static io.appium.java_client.service.local.flags.GeneralServerFlag.LOG_LEVEL;
import static io.appium.java_client.service.local.flags.GeneralServerFlag.SESSION_OVERRIDE;

public class TestOpencv {
    public static final String WIN_DRIVER_PATH = "D:/Program Files/nodejs/node.exe";
    public static final String WIN_APPIUM_PATH = "C:/Users/jeff.xie/AppData/Roaming/npm/node_modules/appium/build/lib/main.js";
    public static final String MAC_DRIVER_PATH = "/usr/local/bin/node";
    public static final String MAC_APPIUM_PATH = "/usr/local/lib/node_modules/appium/build/lib/main.js";
    int width;
    int height;
    AndroidDriver driver;

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        TestOpencv testOpenCV = new TestOpencv();
        testOpenCV.startTest();
    }

    public DesiredCapabilities setAndroidDesiredCapabilities(){
        DesiredCapabilities devices  = new DesiredCapabilities();
        devices.setCapability("platformName", "Android");
        devices.setCapability("deviceName","emulator-5554");
        devices.setCapability("platformVersion", "10");
        devices.setCapability("appPackage","com.android.settings");
        devices.setCapability("appActivity","com.android.settings.Settings");
        devices.setCapability("automation","uiautomator2");
        devices.setCapability("unicodeKeyboard", true);
        devices.setCapability("resetKeyboard",true);
        return devices;
    }

    public void getWindowWidthAbdHeight(){
        width = driver.manage().window().getSize().width;
        height = driver.manage().window().getSize().height;
    }

    @SneakyThrows
    public void startTest()  throws URISyntaxException, IOException {
        //需要打开appium server，并启动模拟器
        //driver= new AndroidDriver(new URL("http://localhost:4723/wd/hub"),setAndroidDesiredCapabilities());

        //不需要打开appium桌面版
        AppiumDriverLocalService service ;
        if (isWindows()) {
            service =startAppiumService(WIN_DRIVER_PATH,WIN_APPIUM_PATH);
        } else {
            service =startAppiumService(MAC_DRIVER_PATH,MAC_APPIUM_PATH);
        }

        driver = new AndroidDriver(service.getUrl(), setAndroidDesiredCapabilities());
        driver.unlockDevice(); //解锁android手机

        Thread.sleep(10000);
        String picturePath="src/main/resources/images/Connect.png";
        clickByPicture2(picturePath,50,50);
        Thread.sleep(5000);
        System.out.println("pass");
    }

    @SneakyThrows
    public void clickByPicture(String picturePath,int xPercent,int yPercent){
        takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
        Thread.sleep(1000);
        String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv.py";
        HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
        int x =map.get("xPer");
        int y =map.get("yPer");
        clickByLocationByPercent(x,y);
    }

    @SneakyThrows
    public void clickByPicture2(String picturePath,int xPercent,int yPercent){
        takeScreenshot(GlobalVar.CURRENT_PAGE_PICTURE_NAME);
        Thread.sleep(1000);
        String pythonCodePath ="src/main/java/com/welab/automation/framework/base/get_location_by_opencv2.py";
        HashMap<String, Integer> map=run_opencv(pythonCodePath,picturePath,GlobalVar.CURRENT_PAGE_PICTURE_PATH,xPercent,yPercent);
        int x =map.get("xPer");
        int y =map.get("yPer");
        clickByLocationByPercent(x,y);
    }

    public static HashMap<String, Integer> run_opencv(String pythonCodePath,String picturePath, String PagePicturePath, int xPercent, int yPercent) {
        HashMap<String, Integer> location = new HashMap<>();
        try {
            String python_execute_path ="";
            if (isWindows()) {
                python_execute_path ="python "+pythonCodePath;
            } else {
                python_execute_path ="python3 "+pythonCodePath;
            }
            String cmds = String.format("%s %s %s %s %s", python_execute_path,picturePath,PagePicturePath,xPercent+"",yPercent+"");
            System.out.println("Executing python script for picture location.");
            System.out.println(cmds);
            Process pcs = Runtime.getRuntime().exec(cmds);
            pcs.waitFor();
            Thread.sleep(1000);

            String result = null;
            BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
            BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
            String lineStr = null;
            while ((lineStr = br.readLine()) != null) {
                result = lineStr;//Python 代码中print的数据就是返回值
                System.out.println(result);
                if(lineStr.contains("xLocation")) {
                    int x =  Integer.parseInt(lineStr.split(":")[1].trim());
                    location.put("x", x);
                }
                if(lineStr.contains("yLocation")) {
                    int x =  Integer.parseInt(lineStr.split(":")[1].trim());
                    location.put("y", x);
                }
                if(lineStr.contains("x_loc_percent")) {
                    int x =  Integer.parseInt(lineStr.split(":")[1].trim());
                    location.put("xPer", x);
                }
                if(lineStr.contains("y_loc_percent")) {
                    int x =  Integer.parseInt(lineStr.split(":")[1].trim());
                    location.put("yPer", x);
                }
            }
            br.close();
            in.close();
            System.out.println(location.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    public void clickByLocationByPercent(int xPercent, int yPercent) {
        final int width = driver.manage().window().getSize().width;
        final int height = driver.manage().window().getSize().height;
        int x = width * xPercent / 100;
        int y = height * yPercent / 100;
        clickByLocation(x, y);
    }

    public void clickByLocation(int x, int y) {
        TouchAction touchAction = new TouchAction(driver);
        PointOption pointOption = PointOption.point(x, y);
        touchAction.press(pointOption).release().perform();
    }

    public static boolean isWindows(){
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            return true;
        } else {
            return false;
        }
    }

    public void takeScreenshot(String screenshotName) {
        TakesScreenshot driver = this.driver;
        File screenFile = driver.getScreenshotAs(OutputType.FILE);
        File file = new File(TARGET_SCREENSHOTS_PATH + screenshotName + ".png");
        try {
            FileUtils.copyFile(screenFile, file);
        } catch (IOException e) {
            logFail("Failed to save screenshot: " + e.getLocalizedMessage());
        }
    }

    public  void clickMobileElementByPicture(String picPath) throws URISyntaxException, IOException {
        File file = new File(picPath);
        String base64String = Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
        By by= MobileBy.image(base64String);
        findMobileElement(by).click();
    }

    public MobileElement findMobileElement(By by) {
        return (MobileElement) driver.findElement(by);
    }

    private static AppiumDriverLocalService startAppiumService(String driverPath, String appiumPath) {
        AtomicInteger port = new AtomicInteger();
        AppiumDriverLocalService service = null;
        service = new AppiumServiceBuilder()
                .usingAnyFreePort()
                .withIPAddress("0.0.0.0")
                .withArgument(SESSION_OVERRIDE)
                .withArgument(LOG_LEVEL, "error")
                .usingDriverExecutable(new File(driverPath))
                .withAppiumJS(new File(appiumPath))
                .build();
        Optional.ofNullable(service).ifPresent(s -> {
            s.start();
            port.set(s.getUrl().getPort());
        });
        AppiumDriverLocalService appiumDriverLocalService = service;
        return service;
    }

}
