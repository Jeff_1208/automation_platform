package com.welab.automation.projects.demo.WelabAPITest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.*;
import io.restassured.response.Response;

@Server(GlobalVar.WEALTH_PATH)
public interface IWealthAccountAPI {
    @Get(path="/account",description="Fetch Basic Account Status")
    Response getAccountStatus();
}
