Feature: Login
  Just a test

  # A Background allows you to add some context to the scenarios that follow it.
  # It can contain one or more Given steps,
  # which are run before each scenario, but after any Before hooks.
#  Background: This is Background
#    Given It is Black

  Background: Open Baidu.com
    Given Open the URL https://www.baidu.com

  @browser:chrome
  Scenario Outline: BasePage Demo
    Given She clicks text box and type <keyword>
    When  She clicks search button
    Then  She can see <result> on the page
    Examples:
      | keyword  | result   |
      | selenium | selenium |
      | java     | java     |

#  @browser:chrome
#  Scenario Outline: Test Automation
#    Given I am <name>
#    Then My age is <age>
#    Examples:
#      | name | age |
#      | Ben  | 18  |
#      | Anna | 23  |
#
#  # String text or datatable in step line
#  Scenario: Hello World
#    # https://github.com/cucumber/cucumber/tree/master/datatable
#    Given This is a apple
#      | name | age |
#      | ben  | 18  |
#    Then That is a banana
#    """
#    Hello world
#    """

