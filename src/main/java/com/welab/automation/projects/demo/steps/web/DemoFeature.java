package com.welab.automation.projects.demo.steps.web;

import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.projects.demo.pages.web.DemoPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class DemoFeature extends WebBasePage {
  private final Logger logger = LoggerFactory.getLogger(DemoFeature.class);
  DemoPage demoPage;

  public DemoFeature() {
    demoPage = new DemoPage();
  }

  // Cucumber expression match with single-quoted or double-quoted strings in feature file
  // Given It is "Black" for {string} and Given It is Black for word
  // https://cucumber.io/docs/cucumber/cucumber-expressions/
  @Given("It is {word}")
  public void black(String text) {
    logger.info("It is " + text);
  }

  //  @Given("^This is a apple$")
  //  public void apple(List<Map<String, String>> data) throws InterruptedException {
  //    Allure.attachment("Hello", "My Friend");
  //    logger.info("This is a apple: " + data.toString());
  //    this.baseDriver.getWebDriver().get("http://www.google.com");
  //    Thread.sleep(2000);
  //    this.baseDriver.takeScreenshot("This is a apple");
  //  }

  //  @Then("^That is a banana$")
  //  public void banana(String text) {
  //    this.baseDriver.getScenario().attach("HelloBanana", "", "Title");
  //    logger.info("That is a banana: " + text);
  //  }

  //  @Given("^I am ([^\"]\\S*)$")
  //  public void name(String name) {
  //    try {
  //      logger.info("I am : " + name);
  //      this.baseDriver.getWebDriver().get("http://www.facebook.com");
  //      //      Assert.fail("Just failed the step");
  //    } catch (Throwable e) {
  //      Allure.step("Verify 1 and 2", Status.FAILED);
  //      throw e;
  //    }
  //  }

  @Then("^My age is ([^\"]\\S*)$")
  public void age(int age) {
    logger.info("I am : " + age);
  }

  // BasePage test
  @Given("^Open the URL ([^\"]*)\\S*$")
  public void openUrl(String url) {
    try {
      navigateTo(url);
    } catch (Exception e) {
      Assert.fail(e.toString());
    }
  }

  @Given("^She clicks text box and type ([^\"]\\S*)$")
  public void sendKeyword(String keyword) {
    try {
      demoPage.inputSearchTxt(keyword);
    } catch (Exception e) {
      Assert.fail(e.toString());
    }
  }

  @When("She clicks search button")
  public void submit() {
    try {
      demoPage.searchTxt();
    } catch (Exception e) {
      Assert.fail(e.toString());
    }
  }

  @Then("^She can see ([^\"]\\S*) on the page")
  public void verifyResult(String expectText) {
    try {
      boolean isCorrect = demoPage.verifySearchResult(expectText);
      Assert.assertTrue(isCorrect);
    } catch (Exception e) {
      Assert.fail(e.toString());
    }
  }
}
