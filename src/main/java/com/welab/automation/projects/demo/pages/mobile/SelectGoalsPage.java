package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class SelectGoalsPage extends AppiumBasePage {
  private String pageName = "Select Goals Page";

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Build My Investing Habit']")
  public MobileElement habitTxt;

  public void selectGoals(MobileElement element) {
    clickElement(element);
  }
}
