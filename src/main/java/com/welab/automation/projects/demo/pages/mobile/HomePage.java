package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class HomePage extends AppiumBasePage {
  private String pageName = "Home Page";

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
  private MobileElement loginBtn;

  public void waitOnHomePage() {
    waitUntilElementVisible(loginBtn);
  }
}
