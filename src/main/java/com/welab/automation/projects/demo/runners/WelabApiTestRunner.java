package com.welab.automation.projects.demo.runners;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.framework.utils.entity.api.SignatureUtil;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.util.Map;

@CucumberOptions(
        features = {"src/main/java/com/welab/automation/projects/demo/features/api/Welab"},
        glue = {"com/welab/automation/projects/demo/steps/api"})
public class WelabApiTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();


    }
}