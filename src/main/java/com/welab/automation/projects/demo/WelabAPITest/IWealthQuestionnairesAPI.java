package com.welab.automation.projects.demo.WelabAPITest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.Body;
import com.welab.automation.framework.common.annotation.Get;
import com.welab.automation.framework.common.annotation.Post;
import com.welab.automation.framework.common.annotation.Server;
import com.welab.automation.framework.common.annotation.Headers;
import io.restassured.response.Response;

@Server(GlobalVar.WEALTH_PATH)
public interface IWealthQuestionnairesAPI {
    @Get(path = "/questionnaires/crpq", description = "Fetch CRPQ Questions")
    @Headers(key={"Accept-Language"}, value={"en-US"})
    Response getQuestionnariesCRPQ();

    @Post(path = "/questionnaires/answers", description = "submit questionnaires answers")
    Response postQuestionnaries(@Body String body);
}
