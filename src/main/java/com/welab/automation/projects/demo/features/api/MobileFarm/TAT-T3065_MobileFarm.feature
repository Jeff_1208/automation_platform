
Feature: MobileFarm
  Scenario Outline: <caseName>
    Given test description: MobileFarm "<caseNo>" "<caseName>" "<isFile>"
    When send <method> request to <url> with <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|
    |MobileFarm|1|Find android device|/automation/api/device/android?web=chrome&version=7|Get|||200|||{'desiredCapabilities.deviceName':'HUAWEI BAH3-L09'}|desiredCapabilities.udid||
    |MobileFarm|3|Get device info|/automation/api/device/${desiredCapabilities.udid}|Get|||200||{'status':'equals(available)'}||||
    |MobileFarm|4|Start use device|/automation/api/device/${desiredCapabilities.udid}|Post|||200||||||
    |MobileFarm|5|upload file to mobilefarm|/automation/api/v1/spaces/artifacts/0|Post||src/test/resources/app/android/VodQA.apk|200||||id|1|
    |MobileFarm|6|Install file to device|/automation/api/storage/install/${desiredCapabilities.udid}/${id}|Get|||201||||||
    |MobileFarm|7|Stop use device|/automation/api/device/${desiredCapabilities.udid}|Delete|||200||||||
    |MobileFarm|8|delete uploaded file|/automation/api/v1/spaces/artifacts/0/${id}|Delete|||200||||||
