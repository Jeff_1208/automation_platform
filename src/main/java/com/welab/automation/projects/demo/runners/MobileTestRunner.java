
package com.welab.automation.projects.demo.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/demo/features/mobile/MobileApp.feature"
    },
    glue = {
            "com/welab/automation/projects/demo/steps/mobile",
            "com/welab/automation/projects/demo/steps/web"
    },
    monochrome = true)
public class MobileTestRunner extends TestRunner {}
