package com.welab.automation.projects.demo;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;

import static com.welab.automation.framework.utils.FileUtil.copyFile;

public class TestWebDemo extends WebBasePage {

    public static void main(String[] args) {
        TestWebDemo testWebDemo = new TestWebDemo();
        testWebDemo.test2();
    }
    @FindBy(how = How.ID, using = "kw")
    private WebElement searchTxt;
    @FindBy(how = How.ID, using = "su")
    private WebElement searchBtn;
    @FindBy(how = How.CSS, using = "div#content_left>div[class*='result']")
    private List<WebElement> resultLink;

    public void inputSearchTxt(String searchStr) {
        clearAndSendKeys(searchTxt, searchStr);
    }
    public void searchTxt() {
        clickElement(searchBtn);
    }

    @SneakyThrows
    public void webUiTest(){
        WebDriverManager.chromedriver().setup();
        String sourceDrivePath = WebDriverManager.chromedriver().getDownloadedDriverPath();
        String targetDrivePath =
                System.getProperty("user.dir") + "/src/main/resources/driver/chromedriver.exe";
        copyFile(sourceDrivePath, targetDrivePath);
        ChromeOptions chromeOptions = new ChromeOptions();
        boolean headlessFlag = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("headless"));
        chromeOptions.setHeadless(headlessFlag);
        chromeOptions.addArguments("--ignore-certificate-errors");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("--lang=es-US");
        RemoteWebDriver driver = new RemoteWebDriver(BaseDriver.getURL(), chromeOptions);
        driver.get("https://www.baidu.com");
        Thread.sleep(5000);
        driver.findElementById("kw").sendKeys("Java");
        driver.findElementById("su").click();

        Thread.sleep(10000);

    }

    @SneakyThrows
    public void test2() {
        WebDriverManager.chromedriver().setup();
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.baidu.com/");
        //百度输入框
        WebElement kw = driver.findElement(By.id("kw"));
        kw.sendKeys("selenium入门学习");

        //搜索框的结果：selenium入门学习ABCtre
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.SHIFT).sendKeys(kw, "abc")
                .keyUp(Keys.SHIFT).sendKeys(kw, "tre")
                .perform();
        //【百度一下】 按钮
        WebElement su = driver.findElement(By.id("su"));
        su.click();
        //这句等价于上面的两句 == 回车即查询
        //kw.sendKeys(Keys.ENTER);
        //将查询结果打印出来
        WebElement searchResultContentElement = driver.findElement(By.id("content_left"));
        String searchResultContent = searchResultContentElement.getText();
//        Console.log(searchResultContent);
        //关闭浏览器
        driver.quit();
    }
}
