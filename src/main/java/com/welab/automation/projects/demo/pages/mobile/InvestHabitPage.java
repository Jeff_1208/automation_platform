package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class InvestHabitPage extends AppiumBasePage {
  private String pageName = "Set Goals Page";

  @Getter
  @AndroidFindBy(
      xpath =
          "//*[@text='One time investment today']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  public MobileElement oneTimeInvestment;

  @Getter
  @AndroidFindBy(
      xpath =
          "//*[@text='Monthly investment']/following-sibling::android.view.ViewGroup[1]//android.widget.EditText")
  public MobileElement monthlyInvestment;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='View recommendation & save my goal']")
  private MobileElement saveGoalBtn;

  public InvestHabitPage() {
    super.pageName = pageName;
  }

  public void setGoals(String oneTimeValue, String monthlyValue) {
    clearAndSendKeys(oneTimeInvestment, oneTimeValue);
    inputEnter(oneTimeInvestment);
    clearAndSendKeys(monthlyInvestment, monthlyValue);
    inputEnter(monthlyInvestment);
    clickElement(saveGoalBtn);
  }
}
