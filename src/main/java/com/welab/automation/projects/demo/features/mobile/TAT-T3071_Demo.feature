Feature: A Simple Demo


  Scenario Outline: Test login with username and password
    Given Launch App
    When Input username and password
      | username | <username> |
      | password | <password> |
    And Click Login button
    Then Verify login success
    Examples:
      | username | password |
      | admin    | admin    |
      | admin    | 1        |


  Scenario: Test scroll up
    Given Launch App
    When Input username and password
      | username | admin |
      | password | admin |
    And Click Login button
    Then Verify login success
    And Scroll up on the screen
    And Verify 'wheelPicker' on the screen


  Scenario: Test verify notification
    Given Launch App
    When Open notification
    Then Verify notification 'Appium' exist



