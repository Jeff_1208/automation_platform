package com.welab.automation.projects.demo.steps.web;

import com.welab.automation.projects.demo.pages.web.APLoginPage;
import io.cucumber.java.en.Given;
import org.testng.Assert;

public class WebSteps {
  APLoginPage apLoginPage;

  public WebSteps() {
    apLoginPage = new APLoginPage();
  }

  @Given("Open WeLab Admin Portal")
  public void openWeLabBrowser() {
    apLoginPage.openWeLabHome("https://www.baidu.com");
  }

  @Given("Click text box and type ([^\"]\\S*)$")
  public void sendKeyword(String keyword) {
    try {
      apLoginPage.inputSearchTxt(keyword);
    } catch (Exception e) {
      Assert.fail(e.toString());
    }
  }
}
