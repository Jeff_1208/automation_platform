package com.test.demo;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import static java.lang.String.format;
import static java.time.Duration.ofMinutes;
import static java.time.Duration.ofSeconds;
import static java.util.Optional.ofNullable;
import static org.openqa.selenium.By.className;


    public class AppiumDemo {

        private static final String PROJECT_NAME = "welab";

        private static final String API_KEY = "HQJFXdeRUNDovexdFLFLx38Ea6KzcrL8hTdxCObhSkT05Ew9ULMs19ncm9PSB0j334eu2QsiwwE5uOmJNcjRayd21jZlkszKZXHCX2lN3D2b8+KENYz6QpD9qQTjOjJE8hb5veD1seEE+Q4lFU+/X2zsy4HDaOm5wxvyFT3XxbQWgqwug2O9OYxb4Ozh7jV2OdpZjgkLQ96X8dkcZSyoExBUEqtQ7eACU3LfQicECMu5hqxn5LoEMPYIPru7Mmj+kaYn40nndPQO4XQ0TfCvCU7yLmTAP/sPX3a4TkbXDDUsgiWdgxGt9OToCMwf9bKBa8y2M0IapZhgekOPM+IWR5hgf2NrLNor3bNYMxLg70ANOd1yfJyc7SGOIf950KhjCgUM44FAK6F1qQkBXV7YP/+/2fUCOlQJkspXQZ7y9js6aHQRMhOsXonr8GyzsAmDPPSxc8H9URzJ8onZEFwr5IiK0OY6eWX1Pd3hmg8MVjORIpmhCqoEvmzfUJI0+Jma47cn1xPhwIKqCeTHKDdJAscSzG7hJ9DDLQmi2eyFuxjm3wRr0QIJvseby8fSps1p/r9vRhU1CB60SVNskQa5ypCtxPkWUA71ngS64F0rBdu55QhMU0S21//hTOrnMGLTFEJLviJFFUHwPdF5nl+J41/NCFOgg4+lXB77Ytx5p0/Mc5GPrPwMVBgMcLQc11JF7uzAh27ml15YtPy7W166spZQOBE4Eu/PXJN7UH+BtwPh+vwG43Kmty46X383uC0Cy2kJ9tuDp1nGtFCVjMB2dWfbILZnPnVXwlc0yv40Gu9z6kXuwR4oKKZt0VHpY4WTQFUgUeKzX5bxiiFdOqhwzaJ1t2WEC7AVS2oMuTZJ+D+dxIde7WDQGnOAwcnhK3MsuG1PV/qzpj37TQhrphSk2FE";
        private static final String APPIUM_HUB = "mobilecloud.test.com";
        private static final String PLATFORM_NAME = "Android";
        private static final String PLATFORM_VERSION = "9";
        private static final String BROWSER_NAME = "chrome";
        private static final String DEVICE_NAME = "HUAWEI ELE-L29";

        private final DesiredCapabilities capabilities;

        private AppiumDriver driver = null;

        public AppiumDemo() {
            capabilities = new DesiredCapabilities();
            capabilities.setCapability("platformName", PLATFORM_NAME);
            capabilities.setCapability("platformVersion", PLATFORM_VERSION);
            capabilities.setCapability("browserName", BROWSER_NAME);
            capabilities.setCapability("deviceName", DEVICE_NAME);
        }

        @BeforeMethod
        public void before() throws Exception {
            String key = URLEncoder.encode(API_KEY, StandardCharsets.UTF_8.name());
            driver = new AndroidDriver(
                    new URL(format("https://%s:%s@%s/wd/hub", PROJECT_NAME, key, APPIUM_HUB)), capabilities);

            // For devices with low performance
            driver.manage().timeouts()
                    .pageLoadTimeout(5, TimeUnit.MINUTES)
                    .implicitlyWait(90, TimeUnit.SECONDS);
        }

        @Test
        public void demoTest() {
            final String testUrl = "https://www.test.com/";

//            Assert.assertEquals(format("Focus is not on '%s'", BROWSER_NAME), driver.isBrowser());

            driver.get(testUrl);

            new FluentWait<>(driver).withMessage("Page was not loaded")
                    .pollingEvery(ofSeconds(1))
                    .withTimeout(ofMinutes(1))
                    .until(driver -> driver.findElements(className("header__logo")).size() > 0);

            Assert.assertEquals("Current url is incorrect", testUrl, driver.getCurrentUrl());
            Assert.assertEquals("Page title is incorrect", "test | Enterprise Software Development, Design & Consulting", driver.getTitle());
        }

        @AfterMethod
        public void after() {
            ofNullable(driver).ifPresent(RemoteWebDriver::quit);
        }

}
