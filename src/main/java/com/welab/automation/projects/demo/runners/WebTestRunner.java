

package com.welab.automation.projects.demo.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {"src/main/java/com/welab/automation/projects/demo/features/web/baidu"},
    glue = {"com/welab/automation/projects/demo/steps/web"},
    tags = "",
    monochrome = true)
public class WebTestRunner extends TestRunner {}
