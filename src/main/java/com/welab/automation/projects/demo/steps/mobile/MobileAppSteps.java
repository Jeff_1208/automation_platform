package com.welab.automation.projects.demo.steps.mobile;

import com.welab.automation.projects.demo.pages.mobile.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class MobileAppSteps {
  LoginPage loginPage;
  HomePage homePage;
  DevPanelPage devpanelPage;
  LoggedInPage loggedInPage;
  WealthPage wealthPage;
  SelectGoalsPage selectGoalsPage;
  InvestHabitPage investHabitPage;
  RecommendationPage recommendationPage;

  public MobileAppSteps() {
    loginPage = new LoginPage();
    homePage = new HomePage();
    devpanelPage = new DevPanelPage();
    loggedInPage = new LoggedInPage();
    wealthPage = new WealthPage();
    selectGoalsPage = new SelectGoalsPage();
    investHabitPage = new InvestHabitPage();
    recommendationPage = new RecommendationPage();
  }

//  @Given("Open WeLab App")
//  public void openWeLabApp() throws Exception {
//    homePage.waitOnHomePage();
//  }

//  @And("Enable skip Root")
//  public void enableSkipRoot() {
//    try {
//      if (System.getProperty("platform").equalsIgnoreCase("android")) {
//        Thread.sleep(5000);
//        devpanelPage.openDevPanel();
//        devpanelPage.enableSkipRoot();
//        devpanelPage.backToHomePage();
//      }
//    } catch (Exception e) {
//
//    }
//  }

//  @Then("I can see the LoggedIn page")
//  public void verifyOnLoggedInPage() {
//    loggedInPage.verifyOnLoggedInPage();
//  }
//
//  @Given("I go to Wealth Page")
//  public void openWealthPage() {
//    loggedInPage.openWealthPage();
//    wealthPage.verifyOnWealthPage();
//  }
//
//  @When("^I Click Add Goals and set ([^\"]\\S*) and ([^\"]\\S*) goals$")
//  public void addMyNewGoals(String oneTime, String monthly) {
//    wealthPage.addNewGoals();
//    selectGoalsPage.selectGoals(selectGoalsPage.habitTxt);
//    investHabitPage.setGoals(oneTime, monthly);
//  }
//
//  @Then("^I can find my ([^\"]\\S*) and ([^\"]\\S*) goals$")
//  public void verifyMyGoal(String oneTime, String monthly) {
//    boolean correct = recommendationPage.verifyGoals(oneTime, monthly);
//    Assert.assertTrue(correct);
//  }
}
