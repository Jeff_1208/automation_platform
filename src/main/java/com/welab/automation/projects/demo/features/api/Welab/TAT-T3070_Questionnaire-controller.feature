

Feature: Questionnaire-controller
  Scenario Outline: <caseName>
    Given test description: Questionnaire-controller "<caseNo>" "<caseName>" "<isFile>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|
    |Questionnaire-controller|1|Fetch language zh-HK CRPQ Questions|/v1/wealth/questionnaires/crpq|Get|{Accept-Language=zh-HK}|||200||{'data.questions.description':'contains(話我哋)'}||||
    |Questionnaire-controller|2|Fetch language zh-HK non-CRPQ Questions|/v1/wealth/questionnaires/non-crpq|Get|{Accept-Language=zh-HK}|||200||{'data.questions.description':'contains(嘅教育)'}||||
    |Questionnaire-controller|3|Fetch language en-US CRPQ Questions|/v1/wealth/questionnaires/crpq|Get|{Accept-Language=en-US}|||200||{'data.questions.description':'contains(Can you tell)'}||||
    |Questionnaire-controller|4|Fetch language en-US nonCRPQ Questions|/v1/wealth/questionnaires/non-crpq|Get|{Accept-Language=en-US}|||200||{'data.questions.description':'contains(your education)'}||||
    |Questionnaire-controller|5|Fetch language zh-HK CRPQ Questions & Answer|/v1/wealth/questionnaires/answers/crpq|Get|{Accept-Language=zh-HK}|||200||{'data.questions.description':'contains(話我哋)'}||||
    |Questionnaire-controller|6|Fetch language zh-HK non-CRPQ Questions & Answer|/v1/wealth/questionnaires/answers/non-crpq|Get|{Accept-Language=zh-HK}|||200||{'data.questions.description':'contains(你嘅教育)'}||||
    |Questionnaire-controller|7|Fetch language en-US CRPQ Questions & Answer|/v1/wealth/questionnaires/answers/crpq|Get|{Accept-Language=en-US}|||200||{'data.questions.description':'contains(Can you tell)'}||||
    |Questionnaire-controller|8|Fetch language en-US non-CRPQ Questions & Answer|/v1/wealth/questionnaires/answers/non-crpq|Get|{Accept-Language=en-US}|||200||{'data.questions.description':'contains(education)'}||||
    |Questionnaire-controller|9|Fetch language CRPQ Questions & Answer incorrect language code|/v1/wealth/questionnaires/answers/crpq|Get|{Accept-Language=en-HC}|||200||{'data.questions.description[0]':'isNull'}||||
    |Questionnaire-controller|10|Fetch language CRPQ Questions incorrect language code|/v1/wealth/questionnaires/crpq|Get|{Accept-Language=en-HC}|||200||{'data.questions.description[0]':'isNull'}||||
    |Questionnaire-controller|11|Fetch language CRPQ Questions & Answer incorrect type|/v1/wealth/questionnaires/answers/crpq1|Get|{Accept-Language=en-US}|||500||||||
    |Questionnaire-controller|12|Fetch language CRPQ Questions incorrect type|/v1/wealth/questionnaires/crpq1|Get|{Accept-Language=en-US}|||500||||||
