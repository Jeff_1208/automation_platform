package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class RecommendationPage extends AppiumBasePage {
  private String pageName = "Recommendation Page";

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Portfolio Recommendation']")
  private MobileElement recommendationTitle;

  @Getter
  @AndroidFindBy(
      xpath = "//*[@text='One time investment']/following-sibling::android.widget.TextView[1]")
  public MobileElement oneTimeInvestment;

  @Getter
  @AndroidFindBy(
      xpath = "//*[@text='Monthly investment']/following-sibling::android.widget.TextView[1]")
  public MobileElement monthlyInvestment;

  public RecommendationPage() {
    super.pageName = pageName;
  }

  public boolean verifyGoals(String oneTimeValue, String monthlyValue) {
    waitUntilElementVisible(recommendationTitle);
    String actualOneTimeValue = getElementText(oneTimeInvestment);
    String actualMonthlyValue = getElementText(monthlyInvestment);
    if (actualOneTimeValue.equals("HKD " + oneTimeValue)
        && actualMonthlyValue.equals("HKD " + monthlyValue)) {
      return true;
    } else {
      return false;
    }
  }
}
