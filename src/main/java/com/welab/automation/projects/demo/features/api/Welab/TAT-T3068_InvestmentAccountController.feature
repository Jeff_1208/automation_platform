
Feature: InvestmentAccountController
  Scenario Outline: <caseName>
    Given test description: InvestmentAccountController "<caseNo>" "<caseName>" "<isFile>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|
    |InvestmentAccountController|1|Save non-CRPQ answers 1|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer.json|200||{'data.vcValue':'equals(Not VC)'}||||
    |InvestmentAccountController|2|Save non-CRPQ answers 2|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer1.json|200||{'data.vcValue':'equals(Not VC)'}||||
    |InvestmentAccountController|3|Save non-CRPQ answers 3|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer2.json|200||{'data.vcValue':'equals(Not VC)'}||||
    |InvestmentAccountController|5|Save non-CRPQ answers 5|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer_not_complete_all.json|500||||||
    |InvestmentAccountController|6|Save non-CRPQ answers 6|/v1/wealth/investment-instruction|Post|||Welab/body/NonCRPQAnswer_type_crpq.json|500||||||
    |InvestmentAccountController|7|Get currencies dicts|/v1/wealth/currencies-dicts|Get||||200||{'data':'arraySizeEquals(6)'}||||
    |InvestmentAccountController|8|Get investment account status for current customer|/v1/wealth/investment-account-status|Get||||200||{'data.status':'equals(PENDING)'}||||
    |InvestmentAccountController|9|Get the customer risk profile for current customer|/v1/wealth/risk-profiles|Get||||200||{'data.questionnaireType':'equals(Crpq)'}||||
