package com.welab.automation.projects.demo.runners;

import com.welab.automation.framework.utils.ImageComparison;
import com.welab.automation.framework.utils.comparison.model.ImageComparisonResult;
import com.welab.automation.framework.utils.comparison.model.Rectangle;
import com.welab.automation.projects.TestRunner;
import io.qameta.allure.Allure;
import io.qameta.allure.Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.welab.automation.framework.utils.ImageComparisonUtil.readImageFromResources;
import static com.welab.automation.framework.utils.comparison.model.ImageComparisonState.MATCH;
import static com.welab.automation.framework.utils.comparison.model.ImageComparisonState.MISMATCH;

@Feature("Image comparison Test")
public class ImageComparisonTest {
    private Logger logger = LoggerFactory.getLogger(ImageComparisonTest.class);

    @Test(description = "compare image normally")
    public void compareImageNormally() throws RuntimeException {
        Allure.description("compare image normally");
        File file = new File("images/result-Normal.png");
        Allure.step("give two files with same png format");
        ImageComparison imageComparison = new ImageComparison("images1/1.png", "images2/1.png");
        ImageComparisonResult imageComparisonResult = imageComparison.compareImages().writeResultTo(file);
        try {
            Allure.addAttachment("normal compare result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }

        Assert.assertNotNull(imageComparison.getActual());
        Assert.assertNotNull(imageComparison.getExpected());

        Allure.step("compare result is match");
        Assert.assertEquals(MATCH, imageComparisonResult.getImageComparisonState());
    }

    @Test(description = "compare image with more different")
    public void compareImageMoreDifferent() throws RuntimeException, FileNotFoundException {
        Allure.description("compare image with more different");
        File file = new File("images/result-MoreDifferent.png");
        Allure.step("give two files with more different");
        ImageComparison imageComparison = new ImageComparison("images1/1.png", "images2/2.png");
        ImageComparisonResult imageComparisonResult = imageComparison.compareImages().writeResultTo(file);
        try {
            Allure.addAttachment("more different result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }
        Assert.assertNotNull(imageComparison.getActual());
        Assert.assertNotNull(imageComparison.getExpected());
        Allure.step("compare result is Mismatch");
        Assert.assertEquals(MISMATCH, imageComparisonResult.getImageComparisonState());
    }

    @Test(description = "compare images with allowing less than percent different")
    public void compareImagesAllowLessThanPercentDifference() {
        Allure.description("compare images with allowing less than percent different");

        File file = new File("images/result-PercentDifference.png");
        Allure.step("give two files with part different");
        ImageComparison imageComparison = new ImageComparison("images1/2.png", "images2/2.png")
                .setAllowingPercentOfDifferentPixels(0);

        Allure.step("compare result with 100% same");
        ImageComparisonResult imageComparisonResult = imageComparison.compareImages().writeResultTo(file);
        try {
            Allure.addAttachment("compare with less than result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }
        Assert.assertEquals(MISMATCH, imageComparisonResult.getImageComparisonState());
        Allure.description("compare result is Mismatch");

        Allure.step("compare result with 98% same");
        imageComparisonResult = imageComparison.setAllowingPercentOfDifferentPixels(2).compareImages();

        Allure.step("compare result is match");
        Assert.assertEquals(MATCH, imageComparisonResult.getImageComparisonState());

    }

    @Test(description = "compare imaged with rectangle width")
    public void compareImagesWithRectangleLineWidth() {
        Allure.description("compare imaged with rectangle width");
        Allure.step("give two files with part different");
        BufferedImage expected = readImageFromResources("images1/3.png");
        BufferedImage actual = readImageFromResources("images2/3.png");

        File file = new File("images/result-imagesWithRectangle.png");
        ImageComparison imageComparison = new ImageComparison(expected, actual, file)
                .setRectangleLineWidth(10);

        ImageComparisonResult imageComparisonResult = imageComparison.compareImages();
        try {
            Allure.addAttachment("compare with rectangle result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }
        Allure.step("compare result is Mismatch");
        Assert.assertEquals(MISMATCH, imageComparisonResult.getImageComparisonState());

        Allure.step("compare result is rectangle width equal 10");
        Assert.assertEquals(10, imageComparison.getRectangleLineWidth());
    }

    @Test(description = "compare images with ignore exclude areas")
    public void compareImagesIgnorExcludedAreas() {
        Allure.description("compare images with ignore exclude areas");

        Allure.step("give two files with part different");
        BufferedImage expected = readImageFromResources("images1/4.png");
        BufferedImage actual = readImageFromResources("images2/4.png");
        List<Rectangle> excludedRect = new ArrayList<>();
        excludedRect.add(new Rectangle(100, 100, 1500, 2500));

        ImageComparison imageComparison = new ImageComparison(expected, actual).setExcludedAreas(excludedRect).setRectangleLineWidth(20);
        File file = new File("images/result-imagesIgnoreExcludedAreas.png");
        ImageComparisonResult imageComparisonResult = imageComparison.compareImages()
                .writeResultTo(file);
        try {
            Allure.addAttachment("images Ignore Excluded Areas result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }
        Allure.description("compare result is Mismatch");
        Assert.assertEquals(imageComparisonResult.getImageComparisonState(), MISMATCH);
    }

    @Test(description = "comare images with jpg format")
    public void compareImagesWithJPG() {
        Allure.description("comare images with jpg format");

        Allure.step("give two files with jpg format");
        BufferedImage expected = readImageFromResources("images1/5.jpg");
        BufferedImage actual = readImageFromResources("images2/5.jpg");

        File file = new File("images/result-compareImagesWithJPG.png");

        ImageComparisonResult imageComparisonResult = new ImageComparison(expected, actual)
                .setRectangleLineWidth(10)
                .compareImages().writeResultTo(file);
        try {
            Allure.addAttachment("compare jpg result", new FileInputStream(file));
        } catch (Exception ex) {
            logger.error("save result to allure report error");
        }
        Allure.step("compare result is Mismatch");
        Assert.assertEquals(MISMATCH, imageComparisonResult.getImageComparisonState());

    }
}
