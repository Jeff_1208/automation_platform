
Feature: InvestmentController
  Scenario Outline: <caseName>
    Given test description: InvestmentController "<caseNo>" "<caseName>" "<isFile>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|
    |InvestmentController|1|Create client goal-cant|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_cant.json|500||||||
    |InvestmentController|2|Create client goal-bigInit|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_bigInit.json|500||||||
    |InvestmentController|3|Create client goal-bigdata|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_bigdata.json|200||{'data.calcTargetWealth':'equals(2000000000)'}||data.id,data.portfolioCode||
    |InvestmentController|4|Create client goal-longTenor|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_longTenor.json|200||{'data.calcTenor':'equals(600)'}||||
    |InvestmentController|5|Create client goal-usd|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_USD.json|200||{'data.goalCcyCode':'equals(USD)'}||||
    |InvestmentController|6|Create client goal-0|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_0.json|200||{'data.calcMonthlyInvt':'equals(0)'}||||
    |InvestmentController|7|Create client goal-500|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_500.json|500||||||
    |InvestmentController|8|Create client goal-wrongCcyCode|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal_wrongCcyCode.json|500||||||
    |InvestmentController|9|Create client goal|/v1/wealth/client-goals|Post|||Welab/body/Create_client_goal.json|200||{'data.portfolioCode':'equals(PTF000000001)'}||||
    |InvestmentController|10|Create client goal-401|/v1/wealth/client-goals|Post|{Authorization=xxxx}||Welab/body/Create_client_goal_0.json|401||||||
    |InvestmentController|11|Get settlement-date|/v1/wealth/settlement-date/${data.portfolioCode}|Get||||200||{'data':'contains(th)'}||||
    |InvestmentController|12|Get settlement-date_PTF000000003|/v1/wealth/settlement-date/PTF000000003|Get||||200||{'data':'contains(th)'}||||
    |InvestmentController|13|Get settlement-date_500|/v1/wealth/settlement-date/|Get||||500||||||
    |InvestmentController|14|Get settlement-date_401|/v1/wealth/settlement-date/PTF000000003|Get|{Authorization=xxxx}|||401||||||
    |InvestmentController|15|Get subscription dates-600-en|/v1/wealth/subscription-date|Get|{Accept-Language=en-US}|tenor=60000||200||{'data.monthlyOrderDateOrdinal':'equals(10th)'}||||
    |InvestmentController|16|Get subscription dates-12-en|/v1/wealth/subscription-date|Get|{Accept-Language=en-US}|tenor=12||200||{'data.monthlyOrderDateOrdinal':'equals(10th)'}||||
    |InvestmentController|17|Get subscription dates-0-hk|/v1/wealth/subscription-date|Get|{Accept-Language=zh-HK}|tenor=0||200||{'data.monthlyOrderDateOrdinal':'equals(10)'}||||
    |InvestmentController|18|Get subscription dates-mince|/v1/wealth/subscription-date|Get||tenor=-12||200||{'data.monthlyOrderDateOrdinal':'equals(10th)'}||||
    |InvestmentController|19|Get subscription dates-500|/v1/wealth/subscription-date|Get||tenor=z||500||||||
    |InvestmentController|20|Get subscription dates-401|/v1/wealth/subscription-date|Get|{Authorization=xxxx}|tenor=1||401||||||
    |InvestmentController|21|Client Goal Name|/v1/wealth/client-goals/${data.id}/goal-name|Put||goalName=test_goalName||200||{'data.goalName':'contains(test_goalName)'}||||
    |InvestmentController|22|Client Goal Name-401|/v1/wealth/client-goals/${data.id}/goal-name|Put|{Authorization=xxxx}|goalName=test_goalName||401||||||
    |InvestmentController|23|Client Goal Name-400|/v1/wealth/client-goals/${data.id}/goal-name|Put|{Signature=xxxx}|goalName=test_goalName||400||||||
    |InvestmentController|24|Client Goal Name-500|/v1/wealth/client-goals/xxx/goal-name|Put||goalName=test_goalName||500||||||
    |InvestmentController|25|Client Goal Name-specialChars|/v1/wealth/client-goals/${data.id}/goal-name|Put||goalName=123_!.*()~||200||{'data.goalName':'contains(123_!.*()~)'}||||
    |InvestmentController|26|Client Goal Name-shortName|/v1/wealth/client-goals/${data.id}/goal-name|Put||goalName=a||200||{'data.goalName':'contains(a)'}||||
    |InvestmentController|27|Client Goal Name-longName|/v1/wealth/client-goals/${data.id}/goal-name|Put||goalName=aabcdefghijklmnopqrstuvwxyz1234567890||200||{'data.goalName':'contains(aabcdefghijklmnopqrstuvwxyz1234567890)'}||||
    |InvestmentController|28|Client Goal Name-chineseName|/v1/wealth/client-goals/${data.id}/goal-name|Put||goalName=测试關||200||{'data.goalName':'contains(测试關)'}||||
