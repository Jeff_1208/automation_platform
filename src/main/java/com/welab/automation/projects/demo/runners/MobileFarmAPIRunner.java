
package com.welab.automation.projects.demo.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {"src/main/java/com/welab/automation/projects/demo/features/api/MobileFarm"},
    glue = {"com/welab/automation/projects/demo/steps/api"})
public class MobileFarmAPIRunner extends TestRunner {}
