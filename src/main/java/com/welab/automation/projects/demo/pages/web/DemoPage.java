package com.welab.automation.projects.demo.pages.web;

import com.welab.automation.framework.base.WebBasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DemoPage extends WebBasePage {
  private static final Logger logger = LoggerFactory.getLogger(DemoPage.class);

  private String pageName = "Demo Page";

  @FindBy(how = How.ID, using = "kw")
  private WebElement searchTxt;

  @FindBy(how = How.ID, using = "su")
  private WebElement searchBtn;

  @FindBy(how = How.CSS, using = "div#content_left>div[class*='result']")
  private List<WebElement> resultLink;

  public void inputSearchTxt(String searchStr) {
    clearAndSendKeys(searchTxt, searchStr);
  }

  public void searchTxt() {
    clickElement(searchBtn);
  }

  public boolean verifySearchResult(String resultStr) throws InterruptedException {
    waitForPageLoadComplete(resultStr + "_百度搜索", 10);
    return verifyContainsText(resultLink.get(0), resultStr, true);
  }
}
