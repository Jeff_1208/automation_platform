
Feature: Document-controller
  Scenario Outline: <caseName>
    Given test description: Document-controller "<caseNo>" "<caseName>" "<isFile>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|
    |Document-controller|1|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=V2VsYWIgQ2xvdWQg.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|2|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|3|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=1212V2VsYWIgQ2xvdWQg.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|4|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|5|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=1212V2VsYWIgQ2xvdWQg.png||200||{'data.docName':'contains(png)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|6|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|7|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=1212V2VsYWIgQ2xvdWQg.jpeg||200||{'data.docName':'contains(jpeg)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|8|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|9|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=1212V2VsYWIgQ2xvdWQg.csv||200||{'data.docName':'contains(csv)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|10|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|11|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=1212V2VsYWIgQ2xvdWQg.docx||200||{'data.docName':'contains(docx)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|12|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|13|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=测试關.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|14|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|15|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=a.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|16|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|17|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|18|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|19|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=abcdefghijklmn_#*()-~.lall.pdf||200||{'data.docName':'contains(pdf)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|20|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|21|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=nofileType||200||{'data.docName':'contains(documents)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|22|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile.json|200||{'data':'1'}||||
    |Document-controller|23|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=a.pdf||200||{'data.docName':'contains(documents)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|24|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile_incorrectID.json|500||||||
    |Document-controller|25|Submit File / pre upload|/v1/wealth/documents/pre-upload|Get||docType=ADDRESS_PROOF&fileName=a.pdf||200||{'data.docName':'contains(documents)'}||data.invtAccDocId,data.publicKey||
    |Document-controller|26|Submit File  upload|/v1/wealth/documents/upload|Post|||Welab/body/submitUploadFile_incorrect.json|500||||||
