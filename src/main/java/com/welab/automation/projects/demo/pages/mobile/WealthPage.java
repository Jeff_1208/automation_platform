package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class WealthPage extends AppiumBasePage {
  private String pageName = "Wealth Page";

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Wealth']")
  private MobileElement wealthTitle;

  @Getter
  @AndroidFindBy(
      xpath = "(//android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup)[2]")
  private MobileElement closeIcon;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Got it']")
  private MobileElement gotIt;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Add New Goals']")
  private MobileElement addNewGoalsTxt;

  //  public void verifyOnWealthPage() {
  //    try {
  //      waitUntilElementVisible(pageName, gotIt);
  //      if (isElementDisplayed(gotIt)) {
  //        clickElement(pageName, gotIt);
  //      }
  //      waitUntilElementVisible(pageName, closeIcon);
  //      if (isElementDisplayed(closeIcon)) {
  //        clickElement(pageName, closeIcon);
  //      }
  //      if (isElementDisplayed(wealthTitle)) {
  //        logPass("Wealth page opens successfully");
  //      } else {
  //        logFail("Failed to open Wealth page");
  //      }
  //    } catch (Exception e) {
  //      Assert.fail("Failed on verifyOnWealthPage");
  //    }
  //  }

  public void addNewGoals() {
    waitUntilElementClickable(addNewGoalsTxt);
    clickElement(addNewGoalsTxt);
  }
}
