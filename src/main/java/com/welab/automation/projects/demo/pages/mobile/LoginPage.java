package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.testng.Assert;

public class LoginPage extends AppiumBasePage {
  private static String pageName = "LoginPage";

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
  private MobileElement loginTxt;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Got it']")
  private MobileElement upgradeOKBtn;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Username']")
  private MobileElement userNameTxt;

  @Getter
  @AndroidFindBy(accessibility = "username-input")
  private MobileElement userNameInput;

  @Getter
  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Password']")
  private MobileElement passwordTxt;

  @Getter
  @AndroidFindBy(accessibility = "password-input")
  private MobileElement passwordInput;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Welcome back']")
  private MobileElement welcomeBack;

  @Getter
  @AndroidFindBy(
      xpath = "//android.view.ViewGroup[@content-desc='login-button']/android.widget.TextView")
  private MobileElement loginBtn;

//  public void loginWithCredential(String user, String password) {
//    try {
//      logger.info("Login with credential: " + user + ", " + password);
//      waitUntilElementVisible(pageName, loginTxt, 5);
//      clickElement(pageName, loginTxt);
//      if (isElementDisplayed(upgradeOKBtn)) upgradeOKBtn.click();
//      waitUntilElementVisible(pageName, userNameTxt, 5).click();
//      clearAndSendKeys(pageName, userNameInput, user);
//      hideKeyboard();
//      clickElement(pageName, passwordTxt);
//      clearAndSendKeys(pageName, passwordInput, password);
//      hideKeyboard();
//      clickElement(pageName, loginBtn);
//    } catch (Exception e) {
//      Assert.fail("Failed on loginWithCredential");
//    }
//  }
}
