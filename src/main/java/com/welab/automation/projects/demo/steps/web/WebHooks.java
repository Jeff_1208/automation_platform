

package com.welab.automation.projects.demo.steps.web;

import atu.testrecorder.ATUTestRecorder;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.WebBasePage;
import com.welab.automation.framework.driver.BaseDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.welab.automation.framework.utils.TimeUtils.getCurrentTimeAsString;

public class WebHooks extends WebBasePage {
  private static final Logger logger = LoggerFactory.getLogger(WebHooks.class);
  ATUTestRecorder recorder;
  boolean recordVideo = Boolean.parseBoolean(GlobalVar.GLOBAL_VARIABLES.get("recordVideo"));
  String videoName;

  @Before
  public void beforeScenario(Scenario scenario) throws IOException {
    logger.info("* Start running scenario: {}", scenario.getName());
    BaseDriver baseDriver = new BaseDriver();
    baseDriver.initDriver();
    String scenarioName = scenario.getName().replaceAll(" ", "_");
    videoName = scenarioName + "_" + getCurrentTimeAsString();
    if (recordVideo) {
      recorder = startRecording(recorder, videoName);
    }
  }

  @After
  public void afterScenario(Scenario scenario) {
    if (recordVideo) {
      stopRecording(recorder, videoName);
    }

    if (scenario.isFailed()) {
      // Take screenshot to report
      final byte[] screenshot =
          ((TakesScreenshot) BaseDriver.getWebDriver()).getScreenshotAs(OutputType.BYTES);
      scenario.attach(screenshot, "image/png", "");
      // Take screenshots to project folder
      String scenarioName = scenario.getName().replaceAll(" ", "_");
      String screenshotName = scenarioName + "_" + getCurrentTimeAsString();
      takeScreenshot(screenshotName);
    }

    BaseDriver.closeDriver();
    logger.info("* End running scenario: {}", scenario.getName());
  }
}
