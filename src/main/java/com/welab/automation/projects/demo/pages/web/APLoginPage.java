package com.welab.automation.projects.demo.pages.web;

import com.welab.automation.framework.base.WebBasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class APLoginPage extends WebBasePage {
  private String pageName = "HomePage";

  @FindBy(how = How.ID, using = "kw")
  private WebElement searchTxt;

  public APLoginPage() {
    super.pageName = pageName;
  }

  public void openWeLabHome(String url) {
    navigateTo(url);
  }

  public void inputSearchTxt(String searchStr) {
    clearAndSendKeys(searchTxt, searchStr);
  }
}
