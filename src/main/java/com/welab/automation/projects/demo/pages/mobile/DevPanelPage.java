package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.testng.Assert;

public class DevPanelPage extends AppiumBasePage {
  private String pageName = "Dev Panel";

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[4]")
  private MobileElement eyeBtn;

  @Getter
  @AndroidFindBy(
      xpath = "//android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]")
  private MobileElement airplaneBtn;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Dev Panel']")
  private MobileElement devPanelTitle;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Dev Panel']/../android.widget.TextView[1]")
  private MobileElement backHomeBtn;

  @Getter
  @AndroidFindBy(
      xpath =
          "//android.widget.TextView[@text='是否跳过Root/Jailbreak detection']/../android.widget.Switch")
  private MobileElement skipRoot;

  public DevPanelPage() {
    super.pageName = pageName;
  }

  public void openDevPanel() {
    waitUntilElementVisible(eyeBtn);
    clickElement(eyeBtn);
    waitUntilElementVisible(airplaneBtn);
    clickElement(airplaneBtn);
    Assert.assertTrue(devPanelTitle.isDisplayed());
  }

  public void enableSkipRoot() {
    scrollUp();
    scrollUp();
    skipRoot.click();
  }

  public void backToHomePage() {
    backHomeBtn.click();
  }
}
