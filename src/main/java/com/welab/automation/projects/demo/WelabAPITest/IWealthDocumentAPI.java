package com.welab.automation.projects.demo.WelabAPITest;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.common.annotation.*;
import io.restassured.response.Response;

@Server(GlobalVar.WEALTH_PATH)
public interface IWealthDocumentAPI {
    @Get(path="/documents/pre-upload",description="Submit file pre upload")
    Response getDocumentsPreUpload(@Param("docType") String docType,
                                   @Param("fileName") String filename);

    @Post(path="/documents/upload",description = "submit file")
    Response postDocumentsUpload(@Body String body);
}
