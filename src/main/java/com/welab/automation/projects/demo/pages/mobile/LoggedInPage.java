package com.welab.automation.projects.demo.pages.mobile;

import com.welab.automation.framework.base.AppiumBasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

public class LoggedInPage extends AppiumBasePage {
  private String pageName = "LoggedIn Page";

  @Getter
  @AndroidFindBy(accessibility = "TabBarItemWealthStack, tab, 5 of 5")
  private MobileElement wealthIcon;

  @Getter
  @AndroidFindBy(xpath = "//*[@text='Cancel']")
  private MobileElement cancelBtn;

  //  public void verifyOnLoggedInPage() {
  //    try {
  //      waitUntilElementClickable(pageName, cancelBtn, 60);
  //      Thread.sleep(3000);
  //      if (isElementDisplayed(cancelBtn)) clickElement(pageName, cancelBtn);
  //      waitUntilElementVisible(pageName, wealthIcon);
  //      if (isElementDisplayed(wealthIcon)) {
  //        logPass("Login successfully");
  //      } else {
  //        logFail("Failed to login");
  //      }
  //    } catch (Exception e) {
  //      Assert.fail("Failed on verifyOnLoggedInPage");
  //    }
  //  }

  public void openWealthPage() {
    waitUntilElementClickable(wealthIcon);
    clickElement(wealthIcon);
  }
}
