

package com.welab.automation.projects;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.ZephyrScaleUtils;
import com.welab.automation.framework.utils.app.CompressPicture;
import com.welab.automation.framework.utils.app.GenerateExcelReport;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static com.welab.automation.framework.GlobalVar.*;
import static com.welab.automation.framework.utils.FileUtil.copyDirectory;
import static com.welab.automation.framework.utils.FileUtil.copyFile;
import static com.welab.automation.framework.utils.Utils.clearTestReport;
import static com.welab.automation.framework.utils.Utils.generateAllureReport;

public class TestRunner extends AbstractTestNGCucumberTests {
  private static final Logger logger = LoggerFactory.getLogger(TestRunner.class);

  static ZephyrScaleUtils zephyrScaleUtils = new ZephyrScaleUtils();

  @BeforeSuite(alwaysRun = true)
  public void setUp() throws Exception {
    Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
    GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
    clearTestReport(50);
    File target = new File(TARGET_SCREENSHOTS_PATH);
    if (!target.exists()) {target.mkdirs();}
    File target1 = new File(TEST_REPORT_PATH);
    if (!target1.exists()) {target1.mkdirs();}
    //startSelenium();
    //zephyrScaleUtils.startAndroidThread();
    zephyrScaleUtils.getUserAndPasswordFromTxtAndPutInGlobalVar();
    zephyrScaleUtils.putPhoneType();
  }

  public void startSelenium() throws IOException {
    if(GlobalVar.GLOBAL_VARIABLES.get("startSelenium")!=null){
      if(GlobalVar.GLOBAL_VARIABLES.get("startSelenium").contains("true")) {
        try {
          String cmds = "java -jar ./src/main/resources/driver/selenium-server-standalone-3.141.59.jar";
          Runtime.getRuntime().exec(cmds);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  @AfterSuite(alwaysRun = true)
  public static void tearDown() throws IOException {
    LocalDateTime now = LocalDateTime.now();
    String path = DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm-ss").format(now);
    generateAllureReport(path);
    copyFile(TARGET_LOG_FILE, TEST_REPORT_PATH + path + "/logs.txt");
    File screenshotsFolder = new File(TARGET_SCREENSHOTS_PATH);
    if (screenshotsFolder.exists()) {
      copyDirectory(TARGET_SCREENSHOTS_PATH, TEST_REPORT_PATH + path + "/screenshots");
      if(System.getProperty("mobile") != null) {
        CompressPicture compressPicture = new CompressPicture();
        String flag = System.getProperty("mobile").toUpperCase();
        double compressPercent = 0.2f;
        if (flag.contains("IOS")) compressPercent = 0.4f;
        String compressScreenShotsPath = compressPicture.changeByDir(TEST_REPORT_PATH + path + "/screenshots", compressPercent);
        GenerateExcelReport generateExcelReport = new GenerateExcelReport();
        generateExcelReport.create_result_report(compressScreenShotsPath, flag);
      }
    }
    File videoFolder = new File(TARGET_RECORDING_PATH);
    if (videoFolder.exists()) {
      copyDirectory(TARGET_RECORDING_PATH, TEST_REPORT_PATH + path + "/recordings");
    }

    File target = new File(TEST_REPORT_ZEPHYRSCALE_PATH);
    if (!target.exists()) {target.mkdirs();}
    copyFile(TEST_REPORT_PATH + path + "/data/suites.csv", TEST_REPORT_ZEPHYRSCALE_PATH + "/suites.csv");
    GLOBAL_VARIABLES.put("TestReportPath", TEST_REPORT_PATH + path);
    zephyrScaleUtils.storeAppVersion();
    zephyrScaleUtils.endAndroidThread();

  }

}
