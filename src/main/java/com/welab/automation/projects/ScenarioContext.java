package com.welab.automation.projects;

import java.util.HashMap;
import java.util.Map;

public class ScenarioContext {
  private static Map<String, Object> scenarioData;
  private static ScenarioContext context;

  public static ScenarioContext getInstance() {
    if (null == context) {
      context = new ScenarioContext();
      scenarioData = new HashMap<>();
    }
    return context;
  }

  public void setScenarioData(String key, Object value) {
    scenarioData.put(key, value);
  }

  public Object getScenarioData(String key) {
    return scenarioData.get(key);
  }

  public Boolean isContains(String key) {
    return scenarioData.containsKey(key);
  }

  public void clearScenarioData() {
    scenarioData = new HashMap<>();
  }
}
