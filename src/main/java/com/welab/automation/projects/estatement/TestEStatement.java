package com.welab.automation.projects.estatement;

import com.welab.automation.framework.utils.entity.estatement.Estatement;
import com.welab.automation.framework.utils.estatement.MainUtils;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.welab.automation.framework.utils.Utils.generateAllureReport;

public class TestEStatement {

  @BeforeClass
  public void setUp() {
    System.out.println("hello start");
  }

  String path = "D:/Project/e-Statement/estatement_pdf/2021_10_25_estatement";

  @DataProvider(name = "data")
  public Object[][] Results() {
    MainUtils m = new MainUtils();
    ArrayList<Estatement> list = m.run_all_estatement(path);
    Object[][] datas = new Object[list.size()][4];
    for (int i = 0; i < list.size(); i++) {
      Estatement e = list.get(i);
      datas[i][0] = e.getCustomer_id();
      datas[i][1] = e.isResult();
      datas[i][2] = e.getComment();
      datas[i][3] = e.getAll_fail_Ref_list();
    }
    return datas;
  }

  @Test(dataProvider = "data")
  public void test_eStatement(
      String customerid, boolean result, String comment, List<String> fail_refs_list) {
    // Allure.step("Test eStatement "+customerid);
    Allure.description("Test eStatement " + customerid);
    AllureLifecycle lifecycle = Allure.getLifecycle();
    lifecycle.updateTestCase(testResult -> testResult.setName(customerid));
    if (!result) {
      for (int i = 0; i < fail_refs_list.size(); i++) {
        String ref = fail_refs_list.get(i);
        if (ref.startsWith("FT") || ref.startsWith("AAAC")) {
          String pic_name = customerid + "_" + fail_refs_list.get(i) + ".png";
          String pic_abs_path = path + "/Image/" + pic_name;
          File file = new File(pic_abs_path);
          try {
            Allure.addAttachment(pic_name, new FileInputStream(file));
          } catch (FileNotFoundException e) {
            e.printStackTrace();
          }
        }
      }
    }
    Assert.assertTrue(result);
    // Assert.assertEquals(result, true);
  }

  @AfterClass
  public static void tearDown() throws IOException {
    LocalDateTime now = LocalDateTime.now();
    String path = DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm-ss").format(now);
    System.setProperty("env", "sit");
    System.setProperty("platform", "statement");
    generateAllureReport(path);
  }
}
