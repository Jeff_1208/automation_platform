package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth2/features/mobile/ios/TAT-T3246_ForeignExchangeIOS.feature"
    },
    glue = {"com/welab/automation/projects/wealth2/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3246_ForeignExchangeIOSTestRunner extends TestRunner {}      
      
    