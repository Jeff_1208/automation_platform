package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/api/wealth/stage/Integration/TAT-T3181_IAO_crr4.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3181_IAO_crr4TestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
