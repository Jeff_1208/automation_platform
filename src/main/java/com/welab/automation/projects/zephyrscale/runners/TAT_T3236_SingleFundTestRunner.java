package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile/android/TAT-T3236_SingleFund.feature"
    },
    glue = {"com/welab/automation/projects/wealth2/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3236_SingleFundTestRunner extends TestRunner {}
      
    