package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile_android/TAT-T3235_LoansDeliquent.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3235_LoansDeliquentTestRunner extends TestRunner {}
      
    