package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/zmobile_android/TAT-T3251_loansConfirmAndroid.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3251_loansConfirmAndroidTestRunner extends TestRunner {}      
      
    