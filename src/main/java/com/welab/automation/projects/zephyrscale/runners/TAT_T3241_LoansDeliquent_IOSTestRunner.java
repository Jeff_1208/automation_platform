package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile_ios/TAT-T3241_LoansDeliquent_IOS.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3241_LoansDeliquent_IOSTestRunner extends TestRunner {}      
      
    