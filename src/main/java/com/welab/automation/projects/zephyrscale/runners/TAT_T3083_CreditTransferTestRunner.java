package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/payment/features/web/TAT-T3083_CreditTransfer.feature"
    },
    glue = {"com/welab/automation/projects/payment/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3083_CreditTransferTestRunner extends TestRunner {}      
      
    