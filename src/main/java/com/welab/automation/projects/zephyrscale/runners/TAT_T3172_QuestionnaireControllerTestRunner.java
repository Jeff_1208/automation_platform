package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/api/wealth/stage/Functions/TAT-T3172_QuestionnaireController.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3172_QuestionnaireControllerTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
