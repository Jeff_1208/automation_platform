package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth2/features/mobile/android/TAT-T3227_IAO.feature"
    },
    glue = {"com/welab/automation/projects/wealth2/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3227_IAOTestRunner extends TestRunner {}      
      
    