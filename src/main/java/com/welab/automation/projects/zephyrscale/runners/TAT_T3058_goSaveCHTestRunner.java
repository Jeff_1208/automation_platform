package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/mobile_CH_android/TAT-T3058_goSaveCH.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3058_goSaveCHTestRunner extends TestRunner {}      
      
    