package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/api/wealth/sit/functions/TAT-T3152_TranslationController.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3152_TranslationControllerTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
