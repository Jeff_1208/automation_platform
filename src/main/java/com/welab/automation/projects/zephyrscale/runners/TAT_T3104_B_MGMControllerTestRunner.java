package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/sit/features/api/sit/Integration/TAT-T3104_B_MGMController.feature"
    },
    glue = {"com/welab/automation/projects/all/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3104_B_MGMControllerTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
