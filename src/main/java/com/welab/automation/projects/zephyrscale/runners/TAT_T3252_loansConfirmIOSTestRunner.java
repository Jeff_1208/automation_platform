package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/zmobile_ios/TAT-T3252_loansConfirmIOS.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "@VBL-T3114",
    monochrome = true)
public class TAT_T3252_loansConfirmIOSTestRunner extends TestRunner {}      
      
    
