package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/web/TAT-T3240_GosavePortal.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps/web"},
    tags = "",
    monochrome = true)
public class TAT_T3240_GosavePortalTestRunner extends TestRunner {}      
      
    