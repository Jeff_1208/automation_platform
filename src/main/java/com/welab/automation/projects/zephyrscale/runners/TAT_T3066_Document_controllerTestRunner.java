package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/demo/features/api/Welab/TAT-T3066_Document_controller.feature"
    },
    glue = {"com/welab/automation/projects/demo/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3066_Document_controllerTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
