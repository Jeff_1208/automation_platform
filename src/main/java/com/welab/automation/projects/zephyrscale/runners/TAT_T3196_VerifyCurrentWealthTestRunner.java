package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/mobile/android/TAT-T3196_VerifyCurrentWealth.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3196_VerifyCurrentWealthTestRunner extends TestRunner {}      
      
    