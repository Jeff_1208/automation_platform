package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/web/TAT-T3218_InvestmentAccountManagement.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3218_InvestmentAccountManagementTestRunner extends TestRunner {}      
      
    