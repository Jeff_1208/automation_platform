package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features2/ios/TAT-T3064_login_05.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3064_login_05TestRunner extends TestRunner {}      
      
    