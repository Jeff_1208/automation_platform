package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile_android/TAT-T3075_loans.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3075_loansTestRunner extends TestRunner {}      
      
    