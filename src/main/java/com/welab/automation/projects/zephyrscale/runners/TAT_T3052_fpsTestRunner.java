package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/mobile_android/TAT-T3052_fps.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3052_fpsTestRunner extends TestRunner {}      
      
    