package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/payment/features/GPS/TAT-T3082_runGPS.feature"
    },
    glue = {"com/welab/automation/projects/payment/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3082_runGPSTestRunner extends TestRunner {}      
      
    