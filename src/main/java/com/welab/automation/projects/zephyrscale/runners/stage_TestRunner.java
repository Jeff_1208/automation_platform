package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/"
    },
    glue = {"com/welab/automation/projects/channel/steps"},
    tags = "",
    monochrome = true)

public class stage_TestRunner extends TestRunner {}
