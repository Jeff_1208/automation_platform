package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/demo/features/mobile/TAT-T3071_Demo.feature"
    },
    glue = {"com/welab/automation/projects/demo/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3071_DemoTestRunner extends TestRunner {}      
      
    