package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/web/TAT-T3077_loanApplicationsChecker.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3077_loanApplicationsCheckerTestRunner extends TestRunner {}      
      
    