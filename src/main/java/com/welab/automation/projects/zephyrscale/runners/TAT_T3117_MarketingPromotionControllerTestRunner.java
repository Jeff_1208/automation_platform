package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/stage/features/api/stage/Functions/TAT-T3117_MarketingPromotionController.feature"
    },
    glue = {"com/welab/automation/projects/all/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3117_MarketingPromotionControllerTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
