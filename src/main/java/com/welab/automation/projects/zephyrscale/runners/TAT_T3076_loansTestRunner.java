package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile_ios/TAT-T3076_loans.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "@VBL-T3111 or @VBL-T3112",
    monochrome = true)
public class TAT_T3076_loansTestRunner extends TestRunner {}      
      
    
