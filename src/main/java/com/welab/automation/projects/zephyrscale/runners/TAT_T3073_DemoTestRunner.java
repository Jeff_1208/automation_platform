package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/demo/features/web/baidu/TAT-T3073_Demo.feature"
    },
    glue = {"com/welab/automation/projects/demo/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3073_DemoTestRunner extends TestRunner {}      
      
    