package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/mobile_ios/TAT-T3244_loans_RF_Case_IOS.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3244_loans_RF_Case_IOSTestRunner extends TestRunner {}      
      
    