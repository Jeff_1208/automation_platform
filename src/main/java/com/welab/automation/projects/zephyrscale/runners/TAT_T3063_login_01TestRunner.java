package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features2/ios/TAT-T3063_login_01.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3063_login_01TestRunner extends TestRunner {}      
      
    