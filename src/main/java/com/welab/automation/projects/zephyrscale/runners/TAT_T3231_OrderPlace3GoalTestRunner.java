package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth2/features/mobile/android/TAT-T3231_OrderPlace3Goal.feature"
    },
    glue = {"com/welab/automation/projects/wealth2/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3231_OrderPlace3GoalTestRunner extends TestRunner {}      
      
    