package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth2/features/mobile/android/TAT-T3245_wealthCentre.feature"
    },
    glue = {"com/welab/automation/projects/wealth2/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3245_wealthCentreTestRunner extends TestRunner {}      
      
    