package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/web/TAT-T3079_loanApplicationsSake2.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/web"},
    tags = "@VBL-T3125 or @VBL-T3147 or @VBL-T3148",
    monochrome = true)
public class TAT_T3079_loanApplicationsSake2TestRunner extends TestRunner {}
