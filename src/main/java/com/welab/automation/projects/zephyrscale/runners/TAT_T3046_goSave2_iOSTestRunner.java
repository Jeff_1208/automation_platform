package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/channel/features/ios/TAT-T3046_goSave2_iOS.feature"
    },
    glue = {"com/welab/automation/projects/channel/steps/mobile"},
    tags = "",
    monochrome = true)
public class TAT_T3046_goSave2_iOSTestRunner extends TestRunner {}      
      
    