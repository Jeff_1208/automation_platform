package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.framework.utils.api.BaseRunner;
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/api/loans/functions/TAT-T3074_ApplyLoans.feature"
    },
    glue = {"com/welab/automation/projects/loans/steps/api"},
    tags = "",
    monochrome = true)

public class TAT_T3074_ApplyLoansTestRunner extends TestRunner {
    @BeforeClass
    public void BeforeClass() {
        BaseRunner baseRunner = new BaseRunner();
        baseRunner.generateHeaderForWealth();
    }
}
