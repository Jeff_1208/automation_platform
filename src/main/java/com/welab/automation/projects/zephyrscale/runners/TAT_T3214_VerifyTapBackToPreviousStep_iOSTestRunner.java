package com.welab.automation.projects.zephyrscale.runners;

import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/wealth/features/mobile/ios/TAT-T3214_VerifyTapBackToPreviousStep_iOS.feature"
    },
    glue = {"com/welab/automation/projects/wealth/steps"},
    tags = "",
    monochrome = true)
public class TAT_T3214_VerifyTapBackToPreviousStep_iOSTestRunner extends TestRunner {}      
      
    