package com.welab.automation.projects.zephyrscale.runners;
    
import com.welab.automation.projects.TestRunner;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = {
      "src/main/java/com/welab/automation/projects/loans/features/"
    },
    glue = {"com/welab/automation/projects/loans/steps"},
    tags = "",
    monochrome = true)

public class loans_TestRunner extends TestRunner {}
