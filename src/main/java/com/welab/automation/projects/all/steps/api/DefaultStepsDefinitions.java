
package com.welab.automation.projects.all.steps.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.db.DBSingleton;
import com.welab.automation.framework.utils.FileUtil;
import com.welab.automation.framework.utils.JsonUtil;
import com.welab.automation.framework.utils.TestDataReader;
import com.welab.automation.framework.utils.api.Matchers;
import com.welab.automation.framework.utils.api.SendRequest;
import com.welab.automation.framework.utils.api.SendRequestEx;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.welab.automation.framework.db.DBUtil.*;

@Feature("Welab API Automation")
public class DefaultStepsDefinitions {

  private static Logger logger =
      LoggerFactory.getLogger(DefaultStepsDefinitions.class);
  private Response response;
  DBSingleton dbSingleton;
  Connection connection;
  private boolean isUploadFile = false;

  @Given("test description: {} {string} {string} {string} {string} {string}")
  public void printCaseDescription(
      String module,
      String caseNo,
      String caseDesc,
      String isFile,
      String account,
      String timeout) {
    logger.info("Executing test case [" + module + " - " + caseNo + " - " + caseDesc + "]......");
    // Clear last cases data
    // GlobalVar.GLOBAL_VARIABLES.replace("TEST_ACCOUNT_SPECIFIED", "");
    // GlobalVar.GLOBAL_VARIABLES.replace("TEST_PASSWORD_SPECIFIED", "");
    if (!isFile.isEmpty()) isUploadFile = true;
    if (!account.isEmpty()) {
      String[] userAccount = account.split(":");
      GlobalVar.GLOBAL_VARIABLES.replace("TEST_ACCOUNT_SPECIFIED", userAccount[0]);
      GlobalVar.GLOBAL_VARIABLES.replace("TEST_PASSWORD_SPECIFIED", userAccount[1]);
    }
    if (!timeout.isEmpty())
      // to do
      ;
  }

  @Step("and Given parameters When Sending Request")
  @When("send {} request to {} with {} {} {} and {}")
  public void sendRequest(String method, String url, String headers, String params, String jsonFile,String sql)
      throws Exception {
    String content = "";
    String getLoansSake2Token = "/auth/realms/welab-loan-operations-portal/protocol/openid-connect/token";
    String loansSake2Url = "/api/v1/loan-applications";
    content = params.isEmpty() ? content : content + " params: " + params;
    content = jsonFile.isEmpty() ? content : content + " jsonFile: " + jsonFile;
    Allure.addAttachment("Send " + method + " Request to " + url, content);
    if ( !sql.equals("") ) {
      if (sql.contains("from otp")) {
        Thread.sleep(3*1000);
        dbSingleton = DBSingleton.getInstance();
        connection = dbSingleton.getConnection();
        queryOtp(connection,sql);
      }
    }
    if (isUploadFile) {
      String host = (null == TestDataReader.getHost()) ? "" : TestDataReader.getHost();
      GlobalVar.HEADERS.put("X-File-Name", "VodQA.apk");
      GlobalVar.HEADERS.put("X-Content-Type", "application/zip");
      GlobalVar.HEADERS.put("X-Alias", "v1.0.0");
      GlobalVar.HEADERS.put("Content-Type", "multipart/form-data");
      SendRequestEx sendRequestEx = new SendRequestEx(host);
      GlobalVar.HEADERS.put(
          "Authorization",
          "Bearer 0vsbTctD4S8guX5+98WARQGXeJBsq8JvK2CXryGLFsHrumvGcJNGr6d1hDBC0nQ5lB41/0Wa3WNHBw8yCO10Cte7JKXSwvJ1gVvLckflEuAI73h8Y0BOM8FUWdkL4PfHuazcDrEdnqP2cUTuCE6bE4hhl3bR0tBVIqpU/mgkHLzz+gkZFgZx+OxySH31YPZhkSN54i5bzT6iYLEC+MY9R53X0qQXKFE9pH5a1YbirtuxH5r3H1VYm/WcFUuLEtMmhBrF+pSUeVzH+DMu0R9V8g90OFSXLxOcctkTXTdtOm/Io9S9Wn2bLa9c+Hle7siediC5MzebmFMQ9CPFBzhZ3Azq1OCy8KNIbrZENO6d0FLOCwbB5wNx2g1VjH6yPiYdaZoWR9ukt7Xbag0EumlxjPauEytpvbWa3FqwQPTWAyjb7yNKvKP6kQwZFc5k2J+146Yqd3HD3DvLpgWJfHiVfy5rzWHODAjwywml4PLc+cH/AOIarn4ar97Bdn36VOOnWTcz4Likx1QSdJqdJugxj7a6mW+zsQu0rJabrs6gP9cwiuu+n1znPh4L9TWgF3D4z63p8YuWSX3KLJ/hQdOC1HcPxLT3MZWAMLUcHVGvR8kZncQLA7NM20aN/uukBqJUaQsudCAem7hhmYyGiQY8Ab6cpmt5l7MkUSKC6+QHlolKudE1ePDvMM6IucHHPcsXol7cRa2sk3YoqO7MDOccUdV0RPO67VobCSnIXyQ7UNHydpw9d7hyZsI/DWztPbV41MhCR71eU5pbDTxL+iovBeKkv1DhkZ5yCPlJM3PWdQw02VPJQprISBMyL1VM6uSkxmwuoZ4gE6HLYuCGUuXU5wSoJ20kfMh3QOtkyeN49xASFFFMunN2ESwLNI5m2r3zGpfuhxkCmsE26g4UTAE87Xc");
      response = sendRequestEx.SendRequest(url, "Post", null, "", jsonFile, true);
    }else if(url.equals(getLoansSake2Token) ||  url.contains(loansSake2Url)) {
      GlobalVar.HEADERS.clear();
      response = new SendRequest().sendRequestLoans(url, method, headers, params, jsonFile);
    }else {
      response = new SendRequest().sendRequest(url, method, headers, params, jsonFile);
    }

  }

  @Then("the response http code equals to {int}")
  public void validHttpCode(int code) {
    Allure.addAttachment("response line", response.getStatusLine());
    response.then().statusCode(code);
  }

  @And("the response equals {}")
  public void validAllResponse(String responseBody) {
    Allure.addAttachment("response content", response.prettyPrint());
    if (!responseBody.equals("")) {
      String expectedResponse =
          FileUtil.readFileWithWrap(GlobalVar.RESPONSE_JSON_FILE_PATH + responseBody);
      response.then().assertThat().equals(expectedResponse);
    }
  }

  @And("the fields in response equal to {}")
  public void validResponseField(String expectedResults) throws IOException {
    Allure.addAttachment("response content", response.prettyPrint());
    if (!expectedResults.equals("")) {
      Map obj = JSON.parseObject(expectedResults);
      Iterator iterator = obj.entrySet().iterator();
      for (; iterator.hasNext(); ) {
        Map.Entry entry = (Map.Entry) iterator.next();
        String field = (String) entry.getKey();
        String value = (String) entry.getValue();
        Matchers.checkField(response.jsonPath(), field, value);
      }
    }
  }

  @And("store data to global variables {string}")
  public void storeVariables(String variables) {
    if (!variables.equals("")) {
      String[] fields = variables.split(",");
      for (String key : fields) {
        JsonPath jp = response.jsonPath();
        try {
          GlobalVar.GLOBAL_VARIABLES.put(key, jp.get(key).toString());
          logger.info("Set global variable [{} = {}]", key, jp.get(key).toString());
          Allure.addAttachment("Set global variable [" + key + "=" + jp.get(key) + "]", "");
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } else {
      Allure.addAttachment("variables", "There didn't set global variable");
    }
  }

  @And("store data to global variables {} {string}")
  public void storeVariablesEx(String byParameter, String variables) {
    if (!byParameter.isEmpty()) {
      Map obj = JSON.parseObject(byParameter);
      Iterator iterator = obj.entrySet().iterator();
      for (; iterator.hasNext(); ) {
        Map.Entry entry = (Map.Entry) iterator.next();
        String field = (String) entry.getKey();
        String value = (String) entry.getValue();
        if (!variables.equals("")) {
          List<ArrayList> lst = response.jsonPath().getList(field);
          JSONArray jsonArray = JsonUtil.listToJsonArray(lst);
          for (int i = 0; i < jsonArray.size(); i++) {
            if (jsonArray.get(i).equals(value)) {
              String result = response.jsonPath().getList(variables).get(i).toString();
              GlobalVar.GLOBAL_VARIABLES.put(variables, result.toString());
              logger.info("Set global variable [{} = {}]", variables, result);
              Allure.addAttachment("Set global variable ", variables + "=" + result);
              break;
            }
          }
        }
      }
    } else {
      if (!variables.equals("")) {
        String[] fields = variables.split(",");
        for (String key : fields) {
          JsonPath jp = response.jsonPath();
          try {
            GlobalVar.GLOBAL_VARIABLES.put(key, jp.get(key).toString().replace(".0", ""));
            logger.info("Set global variable [{} = {}]", key, jp.get(key));
            Allure.addAttachment("Set global variable ", key + "=" + jp.get(key));
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      } else {
        Allure.addAttachment("variables", "There didn't set global variable");
      }
    }
  }
}
