

Feature: A1PersonalLoanController
  Scenario Outline: <caseName>
    Given test description: A1PersonalLoanController "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
    |A1PersonalLoanController|1|App client static copy display -v1 version|/v1/loans/static-text|Get||||200|||||||||
    |A1PersonalLoanController|2|Get dicts types|/v1/loans/dicts/types|Get||||200||{'data.dicts.LOAN_PURPOSE[0].name':'equals(Credit card/Personal Loan repayment)'}|||||||
    |A1PersonalLoanController|3|Query the current user for all loan info|/v1/loan-applications|Get||||200||||data.id,data.applicationId|||||
    |A1PersonalLoanController|4|Get loan application config|/v1/loans/config|Get||productCode=PIL||200||{'data.tenorMax':'equals(60)','data.tenorMin':'equals(6)'}|||||||
    |A1PersonalLoanController|5|Get summary of my loan application|/v1/loan-applications/me/summary|Get||||200||{'data.newApplicationEligibilityCheck.isEligible':'isNotNull'}|||||||
    |A1PersonalLoanController|6|New loan application info|/v1/loan-applications|Post|||Welab/body/New_Loan_Application_Info.json|200||{'data.requestedTenorPeriod':'equals(36)'}||data.id,data.applicationId|||||
    |A1PersonalLoanController|7|Get ab-test list info|/v1/loan-applications/${data.id}/ab-test/list|Get||||200|||||||||
    |A1PersonalLoanController|8|Save ab-test info|/v1/loan-applications/${data.id}/ab-test|Post|||Welab/body/Save_AbTest.json|200|||||||||
    |A1PersonalLoanController|9|Query individual loan info by loan id|/v1/loan-applications/${data.id}|Get||||200||{'data.applicationId':'${data.applicationId}'}||data.applicationId|||||
    |A1PersonalLoanController|10|Update loan application info|/v1/loan-applications/${data.id}|Put|||Welab/body/Update_Loan_Application_Info.json|200||{'data':'isNull'}|||||||
    |A1PersonalLoanController|11|Update loan application info-1|/v1/loan-applications/${data.id}|Put|||Welab/body/Update_Loan_Application_Info_1.json|200||{'data':'isNull'}|||||||
    |A1PersonalLoanController|12|Confirms the submission of a loan application|/v1/loan-applications/${data.id}/submit|Post||||200||{'data.applicationId':'${data.applicationId}'}|||||||
    |A1PersonalLoanController|13|Query individual loan info by loan id|/v1/loan-applications/${data.id}|Get||||200||{'data.applicationId':'${data.applicationId}'}||data.applicationId|||||
    |A1PersonalLoanController|14|Get loan applications agreements|/v1/loan-applications/${data.id}/agreements|Get||||200||{'data.loanApplicationId':'equals(${data.id})'}|||||||
    |A1PersonalLoanController|15|Get offers list|/v1/loan-applications/${data.id}/offers|Get||||200||{'data[0].tenor':'equals(36)'}||data[0].id|||||
    |A1PersonalLoanController|16|Confirm offers|/v1/loan-applications/${data.id}/offers/${data[0].id}/confirm|Post||||200||{'data.tenor':'equals(36)'}|||||||
    |A1PersonalLoanController|17|Get document upload list i18n|/v1/loans/${data.id}/docs-upload-list|Get||||200|||||||||
    |A1PersonalLoanController|18|Batch upload docs|/v1/loans/${data.id}/docs/batch|Post|||Welab/body/Batch_Upload_Docs.json|200||{'data.docId':'equals(0)'}|||||||
    |A1PersonalLoanController|21|Check available balance|/v1/loans/me/checkAvailableBalance|Get||||200||{'data.isAvailable':'equals(true)'}|||||||
    |A1PersonalLoanController|22|Get products pricing-tables|/v1/loans/products/pricing-tables|Get||||200||{'data.priceTables[0].minAmount':'equals(100000)'}|||||||
    |A1PersonalLoanController|23|Get debt consolidation details|/v1/loan-applications/${data.id}/debt-consolidation-details|Get||||200|||||||||
    |A1PersonalLoanController|24|Documents list|/v1/loans/${data.id}/docs-list|Get||||200||{'data.incomeProof.en-US.title':'equals(Income proof)'}|||||||
    |A1PersonalLoanController|25|Additional documents|/v1/loans/${data.id}/docs-additional|Get||||200|||||||||
    |A1PersonalLoanController|26|Get loan application agreement pdf|/v1/loans/${data.id}/docs-agreement|Get||docType=incomeProof||200|||||||||
    |A1PersonalLoanController|27|Get docs|/v1/loans/${data.id}/docs|Get||docType=incomeProof||200|||||||||
    |A1PersonalLoanController|28|Get upload path and keys|/v1/loans/docs/keys|Get||fileName=proof-6c3afba2-5ed3-4eab-b5b5-3887d5b99e56.jpg||200||{'data.name':'contains(proof-6c3afba2-5ed3-4eab-b5b5-3887d5b99e56)'}|||||||
    |A1PersonalLoanController|29|Get maker token|/auth/realms/welab-loan-operations-portal/protocol/openid-connect/token|Post|Welab/headers/maker-token.json|username=maker1&password=Aa123456&&grant_type=password&client_id=personal-loan-prtl||200||{'access_token':'isNotNull'}||access_token|||||
    |A1PersonalLoanController|30|Patch section=address_proof|/api/v1/loan-applications/${data.id}|Patch|Welab/headers/loans_sake2_header.json|section=address_proof|body/addressproof_verfication.json|200|||||||||
    |A1PersonalLoanController|31|Patch section=income_proof|/api/v1/loan-applications/${data.id}|Patch|Welab/headers/loans_sake2_header.json|section=income_proof|body/income_proof_verf.json|200|||||||||
    |A1PersonalLoanController|32|Patch section=employment_verification|/api/v1/loan-applications/${data.id}|Patch|Welab/headers/loans_sake2_header.json|section=employment_verification|body/employment_verification.json|200|||||||||
    |A1PersonalLoanController|33|Get loans_id documents|/api/v1/loan-applications/${data.id}/documents|Get|Welab/headers/loans_sake2_header.json|||200||||data[0].id|||||
    |A1PersonalLoanController|34|Maker Review|/api/v1/loan-applications/${data.id}/documents/${data[0].id}|Patch|Welab/headers/loans_sake2_header.json||body/maker_review.json|200|||||||||
    |A1PersonalLoanController|35|Change to pending checker|/api/v1/loan-applications/${data.id}|Patch|Welab/headers/loans_sake2_header.json|section=loan_info|body/change_pending_checker.json|200|||||||||
    |A1PersonalLoanController|36|Get checker token|/auth/realms/welab-loan-operations-portal/protocol/openid-connect/token|Post|Welab/headers/maker-token.json|username=checker1&password=Aa123456&&grant_type=password&client_id=personal-loan-prtl||200||{'access_token':'isNotNull'}||access_token|||||
    |A1PersonalLoanController|37|Checker Review|/api/v1/loan-applications/${data.id}/documents/${data[0].id}|Patch|Welab/headers/loans_sake2_header.json||body/checker_review.json|200|||||||||
    |A1PersonalLoanController|38|Change to pending approval|/api/v1/loan-applications/${data.id}|Patch|Welab/headers/loans_sake2_header.json|section=loan_info|body/change_pending_approval.json|200|||||||||
    |A1PersonalLoanController|39|Assign application to levelb1|/api/v1/loan-applications/${data.id}/assign-user|Patch|Welab/headers/loans_sake2_header.json||body/assgin_appli_to_level_b1.json|200|||||||||
    |A1PersonalLoanController|40|Get levelb1 token|/auth/realms/welab-loan-operations-portal/protocol/openid-connect/token|Post|Welab/headers/maker-token.json|username=levelb1&password=Aa123456&&grant_type=password&client_id=personal-loan-prtl||200||{'access_token':'isNotNull'}||access_token|||||
    |A1PersonalLoanController|41|Get approval id|/api/v1/loan-applications/${data.id}/approval|Get|Welab/headers/maker-token.json|||200||||data[0].id|||||
    |A1PersonalLoanController|42|Change to approved|/api/v1/loan-applications/${data.id}/approve/${data[0].id}|Patch|Welab/headers/maker-token.json|||200|||||||||
    |A1PersonalLoanController|43|Agree and drawdown|/v1/loan-applications/${data.id}/sign|Post||||200|||||||||
    |A1PersonalLoanController|44|Get loan applications schedule|/v1/loan-applications/${data.id}/schedule|Get||||200||{'data.breakEvenPeriod':'equals(36)'}||data[0].id|||||
    |A1PersonalLoanController|45|Get repaid loanapplications|/v1/loans/repaying|Get||||200||{'data[0].outstandingAmount':'equals(300000.0)'}||data[0].arrangementId|||||
    |A1PersonalLoanController|46|Get repaying loan details|/v1/loans/${data[0].arrangementId}/repaying|Get||||200||{'data.totalInstallments':'equals(36)'}|||||||
    |A1PersonalLoanController|47|Get loan details|/v1/loans/${data[0].arrangementId}|Get||||200||{'data.loanAmount':'equals(300000.0)'}|||||||
    |A1PersonalLoanController|48|Get loans schedule|/v1/loans/${data[0].arrangementId}/schedule|Get||||200||{'data.breakEvenPeriod':'equals(36)','data.accountStatus':'equals(Current)'}|||||||
    |A1PersonalLoanController|49|Get signed agreement document|/v1/loans/${data[0].arrangementId}/signed-agreement|Get||||200|||||||||
    |A1PersonalLoanController|50|Get refinancing pre info|/v1/loan-applications/${data[0].arrangementId}/rf-pre-info|Get||||200||{'data.preApprovalAmount':'isNull'}|||||||
    |A1PersonalLoanController|51|Get repaid loanapplications|/v1/loans/repaid|Get||||200|||||||||
    |A1PersonalLoanController|52|Get repaid loan details|/v1/loans/${data[0].arrangementId}/repaid|Get||||200||{'data.arrangementId':'${data[0].arrangementId}'}|||||||
    |A1PersonalLoanController|53|A/B test|/v1/loan-applications/${data.id}/runPlanTestRule|Post|||Welab/body/A_B_Test.json|200||{'data.arrangementId':'${data[0].arrangementId}'}|||||||
