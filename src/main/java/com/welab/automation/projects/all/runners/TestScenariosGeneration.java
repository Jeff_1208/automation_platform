
package com.welab.automation.projects.all.runners;

import com.welab.automation.framework.GlobalVar;
import com.welab.automation.framework.base.AppiumBasePage;
import com.welab.automation.framework.utils.PropertiesReader;
import com.welab.automation.framework.utils.api.FeatureCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.util.Map;


public class TestScenariosGeneration {
    protected static final Logger logger = LoggerFactory.getLogger(TestScenariosGeneration.class);
    FeatureCreator featureCreator;
    @BeforeSuite
    private void generateFeatureAllCases() throws IOException {
        String filepath="";
        logger.info(GlobalVar.GLOBAL_VARIABLES.get("FUNCTION_TYPE"));
        String env = System.getProperty("env")+"/"+GlobalVar.GLOBAL_VARIABLES.get("FUNCTION_TYPE");
        featureCreator = new FeatureCreator(env);
        Map<String, String> globalVars = PropertiesReader.getInstance().getAllProperties();
        GlobalVar.GLOBAL_VARIABLES.putAll(globalVars);
        if(!env.isEmpty())
            filepath = GlobalVar.TEST_DATA_FILE_PATH+env+"/";
        else
            filepath = GlobalVar.TEST_DATA_FILE_PATH;
        featureCreator.generatFeatureAllCases(filepath,"templateDefault.feature");
    }
}