Feature: <module>
  Scenario Outline: <caseName>
    Given test description: <module> "<caseNo>" "<caseName>" "<isFile>" "<account>" "<timeout>"
    When send <method> request to <url> with <headers> <requestParams> <requestBodyFileName> and <sql>
    Then the response http code equals to <httpCode>
    And the fields in response equal to <validations>
    And store data to global variables <byParameter> "<variables>"
    Examples:
    |module|caseNo|caseName|url|method|headers|requestParams|requestBodyFileName|httpCode|responseFileName|validations|byParameter|variables|isFile|account|timeout|sql|
