#!/bin/bash
zstoken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczovL3dlbGFiYmFuay5hdGxhc3NpYW4ubmV0IiwidXNlciI6eyJhY2NvdW50SWQiOiI2MzM1NGUzMWY1Njg2MTViZGM3ZWJlNjYifX0sImlzcyI6ImNvbS5rYW5vYWgudGVzdC1tYW5hZ2VyIiwic3ViIjoiZmYzNWI4MzctNTIyOS0zZmFiLTgwM2UtN2EyN2ExMGFlZGVjIiwiZXhwIjoxNzMyMjYxNTM3LCJpYXQiOjE3MDA3MjU1Mzd9.8e7RmOVsjEb8hr5fadqfcn9eiH-3GvNZ7En7sx0cLXo"

SHELL_FOLDER=$(cd "$(dirname "$0")";pwd)
echo "${SHELL_FOLDER}"

platform="mobile"
mobile_temp="android"  # default for android
if [ "$1" = "" ];then
  echo "Run Android Device"
else
  echo "Run IOS Device"
  mobile_temp='ios'
fi
mobile=`echo ${mobile_temp} | tr '[A-Z]' '[a-z]'`

# get test cases from test execution
echo "Enter test cycle key:"
read manualTestCycleKey_temp # in uppercase, e.g. "VBL", "ACBN"
manualTestCycleKey=`echo ${manualTestCycleKey_temp} | tr '[a-z]' '[A-Z]'`
manualProjectKey=${manualTestCycleKey%%-*}
echo "manualTestCycleKey: ${manualTestCycleKey}"
echo "manualProjectKey: ${manualProjectKey}"

##################################################################
# get test case
manualTestCycleName=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testcycles/${manualTestCycleKey}" -H "Content-Type: application/json" | jq -r -c '.name')
manualTestCycle=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testexecutions?testCycle=${manualTestCycleKey}&startAt=0&maxResults=1000" -H "Content-Type: application/json")
manualTestCycleArr=(${manualTestCycle//,/ })
echo "check get testcase costs time if there are many test executions"

autoTestCycleKey="${manualTestCycleKey}"
echo "autoTestCycleKey: ${autoTestCycleKey}"
echo "******************************"

manualTestCaseKeysFull=()
manualTestCaseKeysFullTemp=()
for ((i = 0 ; i < ${#manualTestCycleArr[@]} ; i++ ))
do
  if [[ ${manualTestCycleArr[$i]} = *"testcases"* ]]; then
    tempStr=${manualTestCycleArr[$i]#\"testCase\":{\"self\":\"https://api.zephyrscale.smartbear.com/v2/testcases/}}
    tempStr=${tempStr%%/*}
    manualTestCaseKeysFullTemp+=($tempStr)
  fi
done
manualTestCaseKeysFull+=($(echo "${manualTestCaseKeysFullTemp[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
echo "manualTestCaseKeysFull: ${manualTestCaseKeysFull[@]}"

##################################################################
## update -Denv
upperManualTestCycleKey=`echo ${manualTestCycleKey} | tr '[a-z]' '[A-Z]'`
if [[ ${upperManualTestCycleKey} = VBL ]]; then
  env=loans
else
  env=stage
fi
echo ${env}

##################################################################
## get xml path & runner path for each related automated test case
xmlLocations=()
xmlFolder="${SHELL_FOLDER}/src/main/resources/suite/zephyrscale"
runnerLocations=()
runnerFolder="${SHELL_FOLDER}/src/main/java/com/welab/automation/projects/zephyrscale/runners"
for ((i = 0 ; i < ${#manualTestCaseKeysFull[@]} ; i++ ))
do
  newPrefix=$(sed 's/-/_/g' <<< ${manualTestCaseKeysFull[$i]})
  newPrefix=$(sed 's/"//g' <<< $newPrefix)
  echo "newPrefix: ${newPrefix}"
  xmlLocations[$i]=$(find $xmlFolder -name "$env*")
  runnerLocations[$i]=$(find $runnerFolder -name "$env*")
done
echo ${xmlLocations[@]}
echo ${runnerLocations[@]}
echo "xmlLocations: ${xmlLocations[@]}"
echo "runnerLocations: ${runnerLocations[@]}"

##################################################################
# create a test cycle for manual test cases
currentDateTime=`date +"%Y-%m-%d %H:%M:%S"`
createManualTestCycleKey=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testcycles" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"name\":\"${manualTestCycleName}-${currentDateTime}\"}")
newManualTestCycleKey=$(jq -r -c '.key' <<< "$createManualTestCycleKey")

##################################################################
tags=()
for ((i = 0 ; i < ${#manualTestCaseKeysFull[@]} ; i++ ))
do

  ##################################################################
  ## update runner file
  #  tags_string=$(IFS=,; echo "${manualTestCaseKeysFull[*]}")
  #  echo "tags_string: ${tags_string}"
  #  i_tag=@$(sed 's/,/ or @/g' <<< ${tags_string})
  #  i_runner=$(sed "s/tags = \".*\",/tags = \"$i_tag\",/" ${runnerLocations[0]}) # update for all tag

  # just update one tag
  i_runner=$(sed "s/tags = \".*\",/tags = \"@${manualTestCaseKeysFull[$i]}\",/" ${runnerLocations[0]}) # update one tag
  echo "i_runner: ${i_runner}"
  echo "$i_runner" > ${runnerLocations[$i]}  # write content in file

  #####################################################
  echo mvn clean test -Denv=${env} -Dplatform=${platform} -Dmobile=${mobile} -DtestngXmlFile="${xmlLocations[0]}"
  echo "**************************************************************************************************"
  mvn clean test -Denv=${env} -Dplatform=${platform} -Dmobile=${mobile} -DtestngXmlFile="${xmlLocations[0]}"

  appVersionTxtFile=${SHELL_FOLDER}/TestReport/zephyrscale/appversion.txt
  appVersionTxt=`cat ${appVersionTxtFile} | tr '\r\n' "#"`
  appVersionTxt=$(sed 's/#/\<br\>/g' <<< $appVersionTxt)

  echo $appVersionTxt
  echo "-------------------------------------------------------------"

  statusName="Not Executed"
  if [[ $appVersionTxt = *"false"* ]]; then
  statusName="Fail"
  elif [[ $appVersionTxt = *"true"* ]]; then
  statusName="Pass"
  fi
  echo "statusName： ${statusName}"

  durationColumn=(`cat ${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv | cut -d ',' -f4 | sed 's/"Duration in ms"//g' | sed 's/"//g'`)
  sumOfDuration=0
  for ((j = 0 ; j < ${#durationColumn[@]} ; j++ ))
  do
    sumOfDuration=$(($sumOfDuration+${durationColumn[$j]}))
  done

  #upload result and create new test cycle
  echo curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"testCaseKey\":\"${manualTestCaseKeysFull[$i]}\",\"testCycleKey\":\"${newManualTestCycleKey}\",\"statusName\":\"${statusName}\",\"comment\":\"${appVersionTxt}\",\"executionTime\":\"${sumOfDuration}\"}"
  testExecution=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"testCaseKey\":\"${manualTestCaseKeysFull[$i]}\",\"testCycleKey\":\"${newManualTestCycleKey}\",\"statusName\":\"${statusName}\",\"comment\":\"${appVersionTxt}\",\"executionTime\":\"${sumOfDuration}\"}")
  #  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv # force delete suites.csv in case of old test results exist
  #  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/appversion.txt # force delete appversion.txt
done

