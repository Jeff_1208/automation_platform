#!/bin/bash

zstoken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczovL3dlbGFiYmFuay5hdGxhc3NpYW4ubmV0IiwidXNlciI6eyJhY2NvdW50SWQiOiI2MzM1NGUzMWY1Njg2MTViZGM3ZWJlNjYifX0sImlzcyI6ImNvbS5rYW5vYWgudGVzdC1tYW5hZ2VyIiwic3ViIjoiZmYzNWI4MzctNTIyOS0zZmFiLTgwM2UtN2EyN2ExMGFlZGVjIiwiZXhwIjoxNzMyMjYxNTM3LCJpYXQiOjE3MDA3MjU1Mzd9.8e7RmOVsjEb8hr5fadqfcn9eiH-3GvNZ7En7sx0cLXo"

SHELL_FOLDER=$(cd "$(dirname "$0")";pwd)
echo "${SHELL_FOLDER}"

#FILES="./zephyrScaleTestCase/*"
#for f in $FILES
#for f in ./zephyrScaleTestCase/*.feature ./zephyrScaleTestCase/**/*.feature
projects="${SHELL_FOLDER}/src/main/java/com/welab/automation/projects"
for f in $(find ${projects} -maxdepth 10 -type f -name "*.feature")
do
	fileName=$(basename $f)		#just file name ##fn=$(sed 's/.*\///' <<< $f)	alternative to filter filename from full path
	dirName=$(dirname $f)			#dir path
	partialName=${f#${projects}}
#	extension=${fileName##*.} #extension of file

  ##featureText=`cat $f`      #for checking if the "Feature:" line is commented
  featureText=`cat $f | tr '\r\n' "#"`
  featureText=$(sed 's/[^\x00-\x7F]//g' <<< $featureText)
  featureText=$(sed 's/	/`/g' <<< $featureText)
  featureText=$(sed 's/"/`/g' <<< $featureText)
  featureText=$(sed 's/\x27/`/g' <<< $featureText)
  featureText=$(sed 's/##/\<br\>/g' <<< $featureText)
  echo "${partialName}"
  echo "${fileName}"

	if ! [[ $fileName = *"TAT-T"* ]]; then # if fileName doesn't contain "TAT-T"
#		casename=${f##*/}
#	    echo "${casename}"
		#create test case, save test case key
		testCaseKey=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testcases" -H "Content-Type: application/json" -d "{\"projectKey\":\"TAT\",\"name\":\"${partialName}\"}" | jq --raw-output '.key')	# > choco install jq

		#create test script
		echo "${testCaseKey}"
		curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testcases/${testCaseKey}/testscript" -H "Content-Type: application/json" -d "{\"type\":\"plain\",\"text\":\"${featureText}\"}"

		#add test case key as prefix
		mv -f "$f" "${dirName}/${testCaseKey}_${fileName}"

		#write automated test case key and path to appendix
    appendix=`cat ${SHELL_FOLDER}/appendix.json`  #let initialize the content of appendix.json to be "[]" (omit the double quotes)
    # need to revise test case key, path
    # echo $(jq --null-input --argjson a "$appendix" '$a + [{"autoTestCaseKey": "'${testCaseKey}'", "manualTestCaseKey": "", "path": "'${dirName}/${testCaseKey}_${fileName}'"}]') > ./zephyrScaleTestCase/appendix.json
    appendix=$(sed 's~{}]~{\n"autoTestCaseKey": "'${testCaseKey}'",\n"manualTestCaseKey": "",\n"path": "'${dirName}'"/"'${testCaseKey}'"_"'${fileName}'"},\n{}]~' <<< $appendix)
    echo "$appendix" > ${SHELL_FOLDER}/appendix.json
	fi

done

appendixJson=`cat ${SHELL_FOLDER}/appendix.json`
appendixJson=$(sed 's/"\/"/\//g' <<< $appendixJson)
appendixJson=$(sed 's/"_"/_/g' <<< $appendixJson)
echo "$appendixJson" > ${SHELL_FOLDER}/appendix.json
echo `cat ${SHELL_FOLDER}/appendix.json`