#!/bin/bash

zstoken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczovL3dlbGFiYmFuay5hdGxhc3NpYW4ubmV0IiwidXNlciI6eyJhY2NvdW50SWQiOiI2MzM1NGUzMWY1Njg2MTViZGM3ZWJlNjYifX0sImlzcyI6ImNvbS5rYW5vYWgudGVzdC1tYW5hZ2VyIiwic3ViIjoiZmYzNWI4MzctNTIyOS0zZmFiLTgwM2UtN2EyN2ExMGFlZGVjIiwiZXhwIjoxNzMyMjYxNTM3LCJpYXQiOjE3MDA3MjU1Mzd9.8e7RmOVsjEb8hr5fadqfcn9eiH-3GvNZ7En7sx0cLXo"
SHELL_FOLDER=$(cd "$(dirname "$0")";pwd)
echo "${SHELL_FOLDER}"

# get test cases from test execution
echo "Enter test cycle key:"
read manualTestCycleKey # in uppercase, e.g. "VBL-R123", "ACBN-R456"
manualProjectKey=${manualTestCycleKey%%-*}

manualTestCycleName=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testcycles/${manualTestCycleKey}" -H "Content-Type: application/json" | jq -r -c '.name')
manualTestCycle=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testexecutions?testCycle=${manualTestCycleKey}&startAt=0&maxResults=1000" -H "Content-Type: application/json")
manualTestCycleArr=(${manualTestCycle//,/ })
echo "check get testcase costs time if there are many test executions"

manualTestCaseKeysFull=()
manualTestCaseKeysFullTemp=()
for ((i = 0 ; i < ${#manualTestCycleArr[@]} ; i++ ))
do
  if [[ ${manualTestCycleArr[$i]} = *"testcases"* ]]; then
    tempStr=${manualTestCycleArr[$i]#\"testCase\":{\"self\":\"https://api.zephyrscale.smartbear.com/v2/testcases/}}
    tempStr=${tempStr%%/*}
    manualTestCaseKeysFullTemp+=($tempStr)
  fi
done
manualTestCaseKeysFull+=($(echo "${manualTestCaseKeysFullTemp[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

automatedTestCaseKeysFull=()
for ((i = 0 ; i < ${#manualTestCaseKeysFull[@]} ; i++ ))
do
  automatedTestCaseKeyFull=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testcases/${manualTestCaseKeysFull[$i]}" -H "Content-Type: application/json" | jq -r -c '.labels')

#  automatedTestCaseKeyFull=$(sed 's/[^\x00-\x7F]//g' <<< $automatedTestCaseKeyFull) # comment for mac
  automatedTestCaseKeysFull+=(${automatedTestCaseKeyFull})
done
echo ${manualTestCaseKeysFull[@]}
#echo ${automatedTestCaseKeysFull[@]}

manualTestCaseKeys=()
automatedTestCaseKeys=()
for ((i = 0 ; i < ${#manualTestCaseKeysFull[@]} ; i++ ))
do
  if ! [[ ${#automatedTestCaseKeysFull[$i]} == 2 ]]; then
    manualTestCaseKeys+=(${manualTestCaseKeysFull[$i]})
    automatedTestCaseKeys+=(${automatedTestCaseKeysFull[$i]})
  fi
done
echo ${manualTestCaseKeys[@]}
echo ${automatedTestCaseKeys[@]}

#################################################################
## get manual to automated test cases relations from zephyr scale

echo "converting to array...."

transposeCasesToBeRun=()
for ((i = 0 ; i < ${#manualTestCaseKeys[@]} ; i++ ))
do
  temp=${automatedTestCaseKeys[$i]//[,]/ }
  temp=${temp//[\[\]]/}
  tempArr=($temp)

  for ((j = 0 ; j < ${#tempArr[@]} ; j++ ))
  do
    transposeCasesToBeRun+=({${tempArr[$j]}:${manualTestCaseKeys[$i]}})
#    transposeCasesToBeRun=(${transposeCasesToBeRun[@]} "{${tempArr[$j]}:${manualTestCaseKeys[$i]}}")
#    transposeCasesToBeRun["${tempArr[$j]}"]+=("${manualTestCaseKeys[$i]}")
#    transposeCasesToBeRun+=( {"name": "${manualTestCaseKeys[$i]}", "keys": ${tempArr[@]}} )
  done
#  arrayCasesToBeRun+=( {"name": "${manualTestCaseKeys[$i]}", "keys": ${tempArr[@]}} )
done
echo ${transposeCasesToBeRun[@]}

echo "splitting the array...."

transposeCasesToBeRunAuto=()
transposeCasesToBeRunManual=()
for ((i = 0 ; i < ${#transposeCasesToBeRun[@]} ; i++ ))
do
  tempT=${transposeCasesToBeRun[$i]//[\{\}]/}
  elArrT=(${tempT//:/ })

  transposeCasesToBeRunAuto=(${transposeCasesToBeRunAuto[@]} ${elArrT[0]})
  transposeCasesToBeRunManual=(${transposeCasesToBeRunManual[@]} ${elArrT[1]})
done
echo ${transposeCasesToBeRunAuto[@]}
echo ${transposeCasesToBeRunManual[@]}

echo "reducing the array...."

reducedCasesToBeRunAuto=($(echo "${transposeCasesToBeRunAuto[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
echo ${reducedCasesToBeRunAuto[@]}

reducedCasesToBeRunManual=()
for ((i = 0 ; i < ${#transposeCasesToBeRunAuto[@]} ; i++ ))
do
  for ((j = 0 ; j < ${#reducedCasesToBeRunAuto[@]} ; j++ ))
  do
    if [[ ${reducedCasesToBeRunAuto[$j]} == "${transposeCasesToBeRunAuto[$i]}" ]]; then
#      echo $i $j
      if [[ ${reducedCasesToBeRunManual[$j]} == "" ]]; then
        reducedCasesToBeRunManual[$j]+=${transposeCasesToBeRunManual[$i]}
      else
        reducedCasesToBeRunManual[$j]+=,${transposeCasesToBeRunManual[$i]}
      fi
    fi
  done
done
echo ${reducedCasesToBeRunManual[@]}

#reducedCasesToBeRun=()
#for ((i = 0 ; i < ${#reducedCasesToBeRunAuto[@]} ; i++ ))
#do
#  reducedCasesToBeRun[$i]={${reducedCasesToBeRunAuto[$i]}:${reducedCasesToBeRunManual[$i]}}
#done
#echo ${reducedCasesToBeRun[@]}
#################################################################

########################################################
## get feature path for each related automated test case
featureLocations=()
appendixJson=`cat ${SHELL_FOLDER}/appendix.json`
appendixJson=$(sed 's/"\/"/\//g' <<< $appendixJson)
appendixJson=$(sed 's/"_"/_/g' <<< $appendixJson)

for ((i = 0 ; i < ${#reducedCasesToBeRunAuto[@]} ; i++ ))
do
i_reducedCasesToBeRunAuto=${reducedCasesToBeRunAuto[$i]}
featureLocations[$i]=$(jq -r -c '.[] | select(.autoTestCaseKey == '$i_reducedCasesToBeRunAuto') | .path' <<< "$appendixJson")
done
echo ${featureLocations[@]}
########################################################

##################################################################
## get xml path & runner path for each related automated test case
xmlLocations=()
xmlFolder="${SHELL_FOLDER}/src/main/resources/suite/zephyrscale"
runnerLocations=()
runnerFolder="${SHELL_FOLDER}/src/main/java/com/welab/automation/projects/zephyrscale/runners"
for ((i = 0 ; i < ${#reducedCasesToBeRunAuto[@]} ; i++ ))
do
  newPrefix=$(sed 's/-/_/g' <<< ${reducedCasesToBeRunAuto[$i]})
  newPrefix=$(sed 's/"//g' <<< $newPrefix)

  xmlLocations[$i]=$(find $xmlFolder -name "$newPrefix*")
  runnerLocations[$i]=$(find $runnerFolder -name "$newPrefix*")
done
echo ${xmlLocations[@]}
echo ${runnerLocations[@]}
##################################################################

#####################
## update runner file
tags=()
for ((i = 0 ; i < ${#reducedCasesToBeRunManual[@]} ; i++ ))
do
  i_tag=@$(sed 's/,/ or @/g' <<< ${reducedCasesToBeRunManual[$i]})
  tags[$i]=$i_tag

  i_runner=$(sed "s/tags = \".*\",/tags = \"$i_tag\",/" ${runnerLocations[$i]})
  echo "$i_runner" > ${runnerLocations[$i]}
done
echo ${tags[@]}
#####################

#########################################
# create a test cycle for auto test cases
currentDateTime=`date +"%Y-%m-%d %H:%M:%S"`
createAutoTestCycleKey=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testcycles" -H "Content-Type: application/json" -d "{\"projectKey\":\"TAT\",\"name\":\"${manualTestCycleKey}-${manualTestCycleName}-${currentDateTime}\"}")
autoTestCycleKey=$(jq -r -c '.key' <<< "$createAutoTestCycleKey")
#########################################

####################
## update -Dplatform
platform=()
for ((i = 0 ; i < ${#reducedCasesToBeRunAuto[@]} ; i++ ))
do
  i_upperFeatureLocation=`echo ${featureLocations[$i]} | tr '[a-z]' '[A-Z]'`
  #if [[ ${featureLocations[$i]^^} = *"API"* ]]; then
  if [[ ${i_upperFeatureLocation} = *API* ]]; then
    platform[$i]=api
  elif [[ ${i_upperFeatureLocation} = *IOS* || ${i_upperFeatureLocation} = *ANDROID* || ${i_upperFeatureLocation} = *MOBILE* ]]; then
    platform[$i]=mobile
  else
    platform[$i]=chrome
  fi
done
echo ${platform[@]}
####################

###############
## update -Denv
upperManualProjectKey=`echo ${manualProjectKey} | tr '[a-z]' '[A-Z]'`
if [[ $upperManualProjectKey == "ACBN" ]]; then
  env=stage
elif [[ $upperManualProjectKey == "VBL" ]]; then
  env=loans
elif [[ $upperManualProjectKey == "GOS" ]]; then
  env=stage
elif [[ $upperManualProjectKey == "VBWMIS" ]]; then
  env=wealth
fi
echo ${env}
###############

#####################################################
## test Execution for TAT test cycle, get test result
for ((i = 0 ; i < ${#reducedCasesToBeRunAuto[@]} ; i++ ))
do
  echo execution: $i
  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv # force delete suites.csv in case of old test results exist
  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/appversion.txt # force delete appversion.txt

  i_upperFeatureLocation=`echo ${featureLocations[$i]} | tr '[a-z]' '[A-Z]'`
  if [[ ${platform[$i]} == *"chrome"* ]]; then
    echo mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -DtestngXmlFile="${xmlLocations[$i]}"
    echo "**************************************************************************************************"
    mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -DtestngXmlFile="${xmlLocations[$i]}"
  else
    if [[ ${i_upperFeatureLocation} = *"IOS"* ]]; then
      echo mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -Dmobile=ios -DtestngXmlFile="${xmlLocations[$i]}"
      echo "**************************************************************************************************"
      mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -Dmobile=ios -DtestngXmlFile="${xmlLocations[$i]}"

    else
      echo mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -Dmobile=android -DtestngXmlFile="${xmlLocations[$i]}"
      echo "**************************************************************************************************"
      mvn clean test -Denv=${env} -Dplatform=${platform[$i]} -Dmobile=android -DtestngXmlFile="${xmlLocations[$i]}"
    fi
  fi

  testResultCsvFile=${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv
  waitTime=10
  while : ; do
    [[ -f ${testResultCsvFile} || $waitTime == 0 ]] && break
    echo "Pausing until suites.csv exists."
    let waitTime--
    echo $waitTime
    sleep 1
  done

  appVersionTxtFile=${SHELL_FOLDER}/TestReport/zephyrscale/appversion.txt
  while : ; do
    [[ -f ${appVersionTxtFile} || $waitTime == 0 ]] && break
    echo "Pausing until appversion.txt exists."
    let waitTime--
    echo $waitTime
    sleep 1
  done

  testResultCsv=`cat ${testResultCsvFile}`
  echo $testResultCsv
  echo "-------------------------------------------------------------"
  echo "-------------------------------------------------------------"
  echo "-------------------------------------------------------------"

  statusName="Not Executed"
  if [[ $testResultCsv = *"failed"* ]]; then
    statusName="Fail"
  elif [[ $testResultCsv = *"broken"* ]]; then
    statusName="Fail"
  elif [[ $testResultCsv = *"passed"* ]]; then
    statusName="Pass"
  fi
  appVersionTxt=`cat ${appVersionTxtFile} | tr '\r\n' "#"`
  appVersionTxt=$(sed 's/#/\<br\>/g' <<< $appVersionTxt)

  durationColumn=(`cat ${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv | cut -d ',' -f4 | sed 's/"Duration in ms"//g' | sed 's/"//g'`)
  sumOfDuration=0
  for ((j = 0 ; j < ${#durationColumn[@]} ; j++ ))
  do
    sumOfDuration=$(($sumOfDuration+${durationColumn[$j]}))
  done

  # create test execution for auto test cases and the test cases would be automatically added into the test cycle
  echo curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"TAT\",\"testCaseKey\":\"$(sed 's/"//g' <<< ${reducedCasesToBeRunAuto[$i]})\",\"testCycleKey\":\"${autoTestCycleKey}\",\"statusName\":\"${statusName}\",\"comment\":\"${appVersionTxt}\",\"executionTime\":\"${sumOfDuration}\"}"
  testExecution=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"TAT\",\"testCaseKey\":\"$(sed 's/"//g' <<< ${reducedCasesToBeRunAuto[$i]})\",\"testCycleKey\":\"${autoTestCycleKey}\",\"statusName\":\"${statusName}\",\"comment\":\"${appVersionTxt}\",\"executionTime\":\"${sumOfDuration}\"}") # Not Executed / Pass / Fail / WIP / Blocked

  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/suites.csv # delete test result
  rm -f ${SHELL_FOLDER}/TestReport/zephyrscale/appversion.txt # delete appversion.txt
done

#####################################################

###########################################
# create a test cycle for manual test cases
if [[ $upperManualProjectKey == "ACBN" ]]; then
manualFolderId="9250320"
elif [[ $upperManualProjectKey == "VBL" ]]; then
manualFolderId="8410995"
elif [[ $upperManualProjectKey == "GOS" ]]; then
manualFolderId="10206779"
elif [[ $upperManualProjectKey == "VBWMIS" ]]; then
manualFolderId="9250352"
fi

createManualTestCycleKey=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testcycles" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"name\":\"${manualTestCycleKey}-${manualTestCycleName}-${currentDateTime}\",\"folderId\":\"${manualFolderId}\"}")
newManualTestCycleKey=$(jq -r -c '.key' <<< "$createManualTestCycleKey")
echo ${createManualTestCycleKey}
echo ${newManualTestCycleKey}
###########################################


########################################################
## test Execution for manual test cycle, get test result
for ((i = 0 ; i < ${#manualTestCaseKeys[@]} ; i++ ))
do
    temp=${automatedTestCaseKeys[$i]//[,]/ }
    temp=${temp//[\[\]]/}
    tempArr=($temp)
    echo ${tempArr[@]}

    i_testExecutionStatusArray=()
    i_executionTimeArray=()
    i_commentArray=()
    for ((j = 0 ; j < ${#tempArr[@]} ; j++ ))
    do
      echo $i,$j
      tempTestCaseKey=$(sed 's/"//g' <<< ${tempArr[$j]})
      testExecution=$(curl -k -H "Authorization: Bearer ${zstoken}" -X GET "https://api.zephyrscale.smartbear.com/v2/testexecutions?testCycle=${autoTestCycleKey}&testCase=${tempTestCaseKey}" -H "Content-Type: application/json")
#      testExecutionStatus=$(jq -r -c '.values[0].testExecutionStatus.id' <<< "$testExecution")
      executionTime=$(jq -r -c '.values[0].executionTime' <<< "$testExecution")
      comment=$(jq -r -c '.values[0].comment' <<< "$testExecution")
#      echo ${tempTestCaseKey}: ${testExecutionStatus}
#      if [[ ${testExecutionStatus} == "3432227" ]]; then
#        testExecutionStatus="Not Executed"
#      elif [[ ${testExecutionStatus} == "3432228" ]]; then
#        testExecutionStatus="WIP"
#      elif [[ ${testExecutionStatus} == "3432229" ]]; then
#        testExecutionStatus="Pass"
#      elif [[ ${testExecutionStatus} == "3432230" ]]; then
#        testExecutionStatus="Fail"
#      elif [[ ${testExecutionStatus} == "3432231" ]]; then
#        testExecutionStatus="Blocked"
#      fi
#
#      i_testExecutionStatusArray+=($testExecutionStatus)
      i_executionTimeArray+=($executionTime)
      i_commentArray+=(${tempTestCaseKey}: ${testExecutionStatus}\<br\>${comment}\<br\>)
    done

#    echo i_testExecutionStatusArray: ${i_testExecutionStatusArray[@]}
#    passCount=$(grep -o "Pass" <<< ${i_testExecutionStatusArray[*]} | wc -l)
#    echo passCount: $passCount
#    i_testExecutionStatus="Not Executed"
#    if [[ ${i_testExecutionStatusArray[*]} =~ "Fail" ]]; then
#      i_testExecutionStatus="Fail"
#    elif [[ $passCount -eq ${#i_testExecutionStatusArray[@]} ]]; then
#      i_testExecutionStatus="Pass"
#    else
#      i_testExecutionStatus="In Progress"
#    fi
#    echo i_testExecutionStatus: $i_testExecutionStatus

    i_executionTime=$((${i_executionTimeArray[@]/%/+}0))

    i_comment=$(IFS="" echo "${i_commentArray[*]}")

    if [[ $i_comment = *"${manualTestCaseKeys[$i]}=false"* ]]; then
      i_testExecutionStatus="Fail"
    elif [[ $i_comment = *"${manualTestCaseKeys[$i]}=true"* ]]; then
      i_testExecutionStatus="Pass"
    else
      i_testExecutionStatus="In Progress"
    fi

    echo curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"testCaseKey\":\"${manualTestCaseKeys[$i]}\",\"testCycleKey\":\"${newManualTestCycleKey}\",\"statusName\":\"${i_testExecutionStatus}\",\"comment\":\"${i_comment}\",\"executionTime\":\"${i_executionTime}\"}"
    manualTestExecution=$(curl -k -H "Authorization: Bearer ${zstoken}" -X POST "https://api.zephyrscale.smartbear.com/v2/testexecutions" -H "Content-Type: application/json" -d "{\"projectKey\":\"${manualProjectKey}\",\"testCaseKey\":\"${manualTestCaseKeys[$i]}\",\"testCycleKey\":\"${newManualTestCycleKey}\",\"statusName\":\"${i_testExecutionStatus}\",\"comment\":\"${i_comment}\",\"executionTime\":\"${i_executionTime}\"}") # Not Executed / Pass / Fail / WIP / Blocked
done
########################################################
